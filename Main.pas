unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniLabel, uniGUIBaseClasses, uniPanel, UMasterPage,
  Vcl.Menus, uniMainMenu, System.Actions, Vcl.ActnList, uniButton, uniBitBtn, uniSpeedButton, dbisamtb,
  System.DateUtils, UDMdatiAgeTitoli;

type
  TMainForm = class(TFMasterPage)
    UmnMainMenu: TUniMainMenu;
    actlstOperazioni: TActionList;
    actUscita: TAction;
    mnuFile: TUniMenuItem;
    mnuArchivi: TUniMenuItem;
    actClienti: TAction;
    actMandanti: TAction;
    actAgentDipCollab: TAction;
    actDatiAgenzia: TAction;
    mnuContratti: TUniMenuItem;
    mnuClienti: TUniMenuItem;
    mnuMandanti: TUniMenuItem;
    mnuAgentDipCollab: TUniMenuItem;
    mnuDatiAgenzia: TUniMenuItem;
    actContrattiInVigore: TAction;
    mnuContrattiInVigore: TUniMenuItem;
    mnuTitFogliCassa: TUniMenuItem;
    actTitoli: TAction;
    actFogliCassa: TAction;
    actEstremiIncassi: TAction;
    mnuTitoli: TUniMenuItem;
    mnuFogliCassa: TUniMenuItem;
    mnuEstremiIncassi: TUniMenuItem;
    mnuDocumenti: TUniMenuItem;
    actPolizzeDisponibili: TAction;
    actInviatiDaSLP: TAction;
    actPrintHouse: TAction;
    mnuPolizzeDisponibili: TUniMenuItem;
    mnuInviatiDaSLP: TUniMenuItem;
    mnuPrintHouse: TUniMenuItem;
    actUtenti: TAction;
    mnuServizi: TUniMenuItem;
    mnuUtenti: TUniMenuItem;
    mnuSLP: TUniMenuItem;
    mnuUscita: TUniMenuItem;
    pnlMenuButtons: TUniSimplePanel;
    btnUscita: TUniSpeedButton;
    btnClienti: TUniSpeedButton;
    btnStampaPolizze: TUniSpeedButton;
    actStampaPolizza: TAction;
    actPerfezionaPolizza: TAction;
    btnPerfezionaPolizza: TUniSpeedButton;
    btnTitoli: TUniSpeedButton;
    btnFogliCassa: TUniSpeedButton;
    btnEsci: TUniSpeedButton;
    actInviatiDaSLPOnLogin: TAction;
    Stampe1: TUniMenuItem;
    actStampe: TAction;
    mnuMessaggi: TUniMenuItem;
    actMessaggi: TAction;
    mnuStampaQuietanziamenti: TUniMenuItem;
    actStampaQuietanziamenti: TAction;
    mnuSimulaQuietanziamento: TUniMenuItem;
    actCalcolaQuietanziamentoSLP: TAction;
    mnuN1: TUniMenuItem;
    actSimulaQuietanzamenti: TAction;
    actCalcolaQuietanziamentoNoSLP: TAction;
    mnuCalcolaQuietanziamentoNoSLP: TUniMenuItem;
    mnuCalcolaQuietanziamentoSLP: TUniMenuItem;
    procedure UniFormCreate(Sender: TObject);
    procedure actUscitaExecute(Sender: TObject);
    procedure actClientiExecute(Sender: TObject);
    procedure actStampaPolizzaExecute(Sender: TObject);
    procedure actAgentDipCollabExecute(Sender: TObject);
    procedure actDatiAgenziaExecute(Sender: TObject);
    procedure actUtentiExecute(Sender: TObject);
    procedure actInviatiDaSLPOnLoginExecute(Sender: TObject);
    procedure UniFormAfterShow(Sender: TObject);
    procedure actInviatiDaSLPExecute(Sender: TObject);
    procedure actContrattiInVigoreExecute(Sender: TObject);
    procedure actFogliCassaExecute(Sender: TObject);
    procedure actTitoliExecute(Sender: TObject);
    procedure actPerfezionaPolizzaExecute(Sender: TObject);
    procedure UniFormScreenResize(Sender: TObject; AWidth, AHeight: Integer);
    procedure actPrintHouseExecute(Sender: TObject);
    procedure actMandantiExecute(Sender: TObject);
    procedure actPolizzeDisponibiliExecute(Sender: TObject);
    procedure actStampeExecute(Sender: TObject);
    procedure actMessaggiExecute(Sender: TObject);
    procedure actStampaQuietanziamentiExecute(Sender: TObject);
    procedure actEstremiIncassiExecute(Sender: TObject);
    procedure actSimulaQuietanzamentiExecute(Sender: TObject);
    procedure actCalcolaQuietanziamentoSLPExecute(Sender: TObject);
    procedure actCalcolaQuietanziamentoNoSLPExecute(Sender: TObject);
  private
    { Private declarations }
    procedure showFormStampe(isComingFromLogin: Boolean);
    procedure ProceedElab(Sender: TComponent; AResult: Integer);
    procedure ShowFormQuietanziamenti(ATipoQuietanziamento: TEnTipoQuietanziamento;
      ATipoCopagniaQuietanzamento: TEnTipoCopagniaQuietanzamento);
  public
    { Public declarations }
  end;

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication, UClienti, ServerModule, USceltaTipoPolizza, UCollaboratori,
  UAgenziaEdit, UUtenti, UUtenteEdit, UStampeSLP, UContratti, UFogliCassa, UTitoli, UPerfezionaPolizze,
  UPrinthouseSLP, UMandanti, UModuliDisponibili, UstampeStatistiche, UCodiciErroriPolizza, UAvvisoAgenzia,
  UElencoAvvisi, UStampaQuietanzeSLP, UDMDatiReport, UStoricoIncassi, USimulaQuietanziamento;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;

procedure TMainForm.actAgentDipCollabExecute(Sender: TObject);
begin
  // UniMainModule.archivio := 'ADC';   // agenti dipendenti collaboratori
  UniMainModule.archivio   := 'COL'; // agenti dipendenti collaboratori
  UniMainModule.operazione := 'VIS';

  if UniMainModule.aggiornaPosizione('COL', 'VIS', 'Agenti-Dipendenti-Collaboratori',False) then
  // if UniMainModule.VerificaAutorizzazione then
    TFCollaboratori.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actCalcolaQuietanziamentoNoSLPExecute(Sender: TObject);
begin
  ShowFormQuietanziamenti(tqQuietanzamento, tcNotSLP);
end;

procedure TMainForm.actCalcolaQuietanziamentoSLPExecute(Sender: TObject);
begin
  // ShowFormQuietanziamenti(tqQuietanzamento, tcSLP);

end;

procedure TMainForm.actClientiExecute(Sender: TObject);
begin
  if UniMainModule.aggiornaPosizione('CLI', 'VIS', 'Clienti',False) then
  // if UniMainModule.VerificaAutorizzazione then
    TFClienti.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actContrattiInVigoreExecute(Sender: TObject);
begin
  if UniMainModule.aggiornaPosizione('POL', 'VIS', 'Contratti',False) then
  // if UniMainModule.VerificaAutorizzazione then
    TFContratti.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actDatiAgenziaExecute(Sender: TObject);
begin
  if UniMainModule.aggiornaPosizione('AGE', 'MOD', 'Agenzia',False) then
  // if UniMainModule.VerificaAutorizzazione then
  begin
    UniMainModule.operazione := 'MOD';
    UniMainModule.DMdatiAge.PosizionaQuery(UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger);
    with TFAgenziaEdit.Create(UniApplication) do
    begin
      ShowModal;
    end;
  end
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actEstremiIncassiExecute(Sender: TObject);
begin
  if (UniMainModule.aggiornaPosizione('SIN', 'VIS', 'StoricoIncassi',False) and
      (not UniMainModule.DMdatiAge.is_utente_promoter)) then
  // if ((not UniMainModule.DMdatiAge.is_utente_promoter) and UniMainModule.VerificaAutorizzazione) then
    TFStoricoIncassi.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actFogliCassaExecute(Sender: TObject);
begin
  if (UniMainModule.aggiornaPosizione('FCA', 'VIS', 'FogliCassa',False) and
     (not UniMainModule.DMdatiAge.is_utente_promoter)) then
  // if ((not UniMainModule.DMdatiAge.is_utente_promoter) and UniMainModule.VerificaAutorizzazione) then
    TFfogliCassa.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actInviatiDaSLPExecute(Sender: TObject);
begin
  showFormStampe(False);
end;

procedure TMainForm.actInviatiDaSLPOnLoginExecute(Sender: TObject);
begin
  showFormStampe(True);
end;

procedure TMainForm.actMandantiExecute(Sender: TObject);
begin
  if UniMainModule.aggiornaPosizione('MAN', 'VIS', 'Mandanti',False) then
  // if UniMainModule.VerificaAutorizzazione then
    TFMandanti.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actMessaggiExecute(Sender: TObject);
begin
  // TFElencoAvvisi.Create(UniApplication).ImpostaParent(MainForm.pnlGlobal).ShowModal;
  TFElencoAvvisi.Create(UniApplication).ShowModal;
end;


procedure TMainForm.actPerfezionaPolizzaExecute(Sender: TObject);
begin
  if UniMainModule.aggiornaPosizione('PPP', 'VIS', 'Perfeziona Polizze',false) then
    TFperfezionaPolizze.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
{
  UniMainModule.aggiornaPosizione('PPP', 'VIS', 'Perfeziona Polizze');
  if UniMainModule.VerificaAutorizzazione then
    TFperfezionaPolizze.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
 }
end;

procedure TMainForm.actPolizzeDisponibiliExecute(Sender: TObject);
begin
  // mostra l'elenco delle polizze disponibili e della modulistica disponibile
  TFModuliDisponibili.ShowModuliDisponibili(UniApplication, True, False);
end;

procedure TMainForm.actPrintHouseExecute(Sender: TObject);
begin
  TFPrintHouseSLP.ShowPrintHousePolizza(UniApplication, '', True, False);
end;

procedure TMainForm.actSimulaQuietanzamentiExecute(Sender: TObject);
begin
  ShowFormQuietanziamenti(tqSimulazione, tcNone);
end;

procedure TMainForm.actStampaPolizzaExecute(Sender: TObject);
begin
  if UniMainModule.aggiornaPosizione('PPO', 'STA', 'PreparaPolizza',false) then
    TFSceltaTipoPolizza.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
end;

procedure TMainForm.actStampaQuietanziamentiExecute(Sender: TObject);
begin
  // UniMainModule.aggiornaPosizione('STS', 'VIS', 'Stampe');
  // if UniMainModule.VerificaAutorizzazione then
  if not UniMainModule.IsSubAgenzia then
    TFStampaQuietanzeSLP.Create(UniApplication).ShowModal
  else
    ShowMessage(MSG_STAMPA_QUIET_DA_SUBAGE);

  // else
  // ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;

procedure TMainForm.actStampeExecute(Sender: TObject);
begin
  // stampe e statistiche
  if UniMainModule.aggiornaPosizione('STS', 'VIS', 'Stampe',False) then
  // if UniMainModule.VerificaAutorizzazione then
    TFstampestatistiche.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
end;

procedure TMainForm.actTitoliExecute(Sender: TObject);
begin
  if UniMainModule.aggiornaPosizione('SCA', 'VIS', 'Titoli',False) then
  // if UniMainModule.VerificaAutorizzazione then
    TFTitoli.Create(UniApplication).ShowModal
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);

end;


procedure TMainForm.actUscitaExecute(Sender: TObject);
begin
  UniMainModule.traccia('END',0,'','Uscita '+DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente);
  UniMainModule.QelencoAgenzie.close;
  UniMainModule.DBaccessoBase.close;
  UniMainModule.DMdatiAge.SessionAgenzia.close;
  UniMainModule.DMdatiAge.DBagenti4web.close;
  close;
end;


procedure TMainForm.actUtentiExecute(Sender: TObject);
begin
  if UniMainModule.IsUserAdmin then
  begin
    UniMainModule.aggiornaPosizione('UTE', 'MOD', 'Utenti');
    // TFUtenti.Create(UniApplication).ImpostaParent(MainForm.pnlGlobal).ShowModal;
    TFUtenti.Create(UniApplication).ShowModal;
  end
  else
  begin
    if UniMainModule.aggiornaPosizione('UTE', 'MOD', 'Utenti',False) then
    // if UniMainModule.VerificaAutorizzazione then
    begin
      UniMainModule.operazione := 'MOD';
      UniMainModule.DMdatiAgeUtenti.PosizionaQuery(UniMainModule.utenteCodice);
      TFUtenteEdit.Create(UniApplication).ShowModal;
    end
    else
      ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
  end;

end;


procedure TMainForm.ProceedElab(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.DMDatiAgeAvvisi.hasMessages then
  begin
    ShowMessage(MSG_FOR_YOU);
  end;

  if UniMainModule.DMDatiReport.hasDocumentsToShow then
    showFormStampe(True);

end;

procedure TMainForm.ShowFormQuietanziamenti(ATipoQuietanziamento: TEnTipoQuietanziamento;
  ATipoCopagniaQuietanzamento: TEnTipoCopagniaQuietanzamento);
begin
  with TFSimulaQuietanziamento.Create(UniApplication) do
  begin
    TipoQuietanziamento := ATipoQuietanziamento;
    TipoCompagniaQuietanziamento := ATipoCopagniaQuietanzamento;
    Show;
  end;
end;

procedure TMainForm.UniFormAfterShow(Sender: TObject);
var
  AQyMessage: TDBISAMQuery;
begin

  if UniMainModule.DMDatiAgeAvvisi.hasStopMessage then
  begin
    UniApplication.Terminate(MSG_APP_DISABLED);
  end;

  if UniMainModule.DMDatiAgeAvvisi.hasCriticalMessages(AQyMessage) then
  begin
    TFAvviso.ShowAvviso(UniApplication, AQyMessage, ProceedElab);
  end
  else
    ProceedElab(nil, 0);

end;

procedure TMainForm.showFormStampe(isComingFromLogin: Boolean);
begin
  with TFStampeSLP.Create(UniApplication) do
  begin
    ComingFromLogin := isComingFromLogin;
    ShowModal;
  end;

end;

procedure TMainForm.UniFormCreate(Sender: TObject);
begin
  inherited;
  // FParametersList       := TStringList.Create;
  actInviatiDaSLP.Enabled := not UniMainModule.DMdatiAge.is_utente_promoter;
  UniMainModule.traccia('START',0,'','Ingresso '+DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente);
end;

procedure TMainForm.UniFormScreenResize(Sender: TObject; AWidth, AHeight: Integer);
begin
  inherited;
  WindowState := wsNormal;
  Width       := AWidth;
  Height      := AHeight;
end;

initialization

RegisterAppFormClass(TMainForm);

end.
