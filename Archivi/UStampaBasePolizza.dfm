﻿inherited FStampaBasePolizza: TFStampaBasePolizza
  ClientHeight = 661
  Caption = 'Preparazione polizza'
  OnClose = UniFormClose
  ScreenMask.Enabled = True
  ScreenMask.Target = Owner
  ExplicitWidth = 928
  ExplicitHeight = 700
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlEditButtons: TUniSimplePanel
    AlignmentControl = uniAlignmentServer
    inherited btnConferma: TUniSpeedButton
      ScreenMask.Enabled = True
      ScreenMask.Target = Owner
    end
    inherited btnAnnulla: TUniSpeedButton
      Top = 0
      Action = nil
      ExplicitTop = 0
    end
    object btnEsci: TUniSpeedButton
      Left = 581
      Top = 0
      Width = 106
      Height = 43
      Action = actUscita
      ParentFont = False
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 3
    end
    object btnRiprendiPolizza: TUniSpeedButton
      Left = 245
      Top = 0
      Width = 106
      Height = 43
      Action = actRiprendiPolizza
      ParentFont = False
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 4
    end
    object btnAnteprimaStampa: TUniSpeedButton
      Left = 357
      Top = 0
      Width = 106
      Height = 43
      Action = actAnteprimaStampa
      ParentFont = False
      ParentColor = False
      Color = clWindow
      ScreenMask.Enabled = True
      ScreenMask.Message = 'Attendere, generazione anteprima in corso ...'
      ScreenMask.Target = Owner
      LayoutConfig.Flex = 1
      TabOrder = 5
    end
    object btnAzzeraValori: TUniSpeedButton
      Left = 469
      Top = 0
      Width = 106
      Height = 43
      Action = actAzzeraValori
      ParentFont = False
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 6
    end
  end
  object pnlDatiSpecificiPolizza: TUniPanel [2]
    Left = 0
    Top = 307
    Width = 912
    Height = 354
    Hint = ''
    Align = alClient
    ParentFont = False
    TabOrder = 4
    Caption = ''
    object pcDatiEreditabili: TUniPageControl
      Left = 1
      Top = 1
      Width = 910
      Height = 352
      Hint = ''
      ActivePage = pgContraente
      Align = alClient
      TabOrder = 1
      object pgContraente: TUniTabSheet
        Hint = ''
        Caption = 'Contraente'
        ParentFont = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 256
        ExplicitHeight = 128
        object dbedtContraente: TUniDBEdit
          Left = 16
          Top = 40
          Width = 414
          Height = 22
          Hint = ''
          DataField = 'Contraente'
          DataSource = dsoEdit
          TabOrder = 0
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Contraente'
          FieldLabelWidth = 65
        end
        object btnSelCliente: TUniBitBtn
          Left = 436
          Top = 38
          Width = 25
          Height = 25
          ShowHint = True
          ParentShowHint = False
          Action = actSelezionaContraente
          ParentFont = False
          TabOrder = 1
          IconAlign = iaCenter
          Images = untvmglstIcons
          ImageIndex = 0
        end
        object dbedtCodFiscale: TUniDBEdit
          Left = 16
          Top = 72
          Width = 233
          Height = 22
          Hint = ''
          DataField = 'CodiceFiscale'
          DataSource = dsoEdit
          TabOrder = 2
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Codice fiscale'
          FieldLabelWidth = 65
        end
        object dbedtPIva: TUniDBNumberEdit
          Left = 436
          Top = 72
          Width = 280
          Height = 22
          Hint = ''
          DataField = 'PartitaIva'
          DataSource = dsoEdit
          TabOrder = 3
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Partita Iva'
          FieldLabelWidth = 60
          DecimalPrecision = 0
          DecimalSeparator = ','
        end
        object dbedtLocNascita: TUniDBEdit
          Left = 16
          Top = 104
          Width = 414
          Height = 22
          Hint = ''
          DataField = 'LocNascita'
          DataSource = dsoEdit
          TabOrder = 4
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Nato a'
          FieldLabelWidth = 65
        end
        object dtpckDataNascita: TUniDBDateTimePicker
          Left = 436
          Top = 104
          Width = 280
          Hint = ''
          DataField = 'DataNascita'
          DataSource = dsoEdit
          DateTime = 43749.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          ReadOnly = True
          TabOrder = 5
          Color = clYellow
          FieldLabel = 'Nato il'
          FieldLabelWidth = 60
        end
        object dbedtIndirizzo: TUniDBEdit
          Left = 16
          Top = 136
          Width = 414
          Height = 22
          Hint = ''
          DataField = 'INDIRIZZO'
          DataSource = dsoEdit
          TabOrder = 6
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Indirizzo'
          FieldLabelWidth = 65
        end
        object dbedtCap: TUniDBEdit
          Left = 436
          Top = 136
          Width = 121
          Height = 22
          Hint = ''
          DataField = 'CAP'
          DataSource = dsoEdit
          TabOrder = 7
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Cap'
          FieldLabelWidth = 60
        end
        object dbedtCitta: TUniDBEdit
          Left = 563
          Top = 136
          Width = 263
          Height = 22
          Hint = ''
          DataField = 'CITTA'
          DataSource = dsoEdit
          TabOrder = 8
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Citt'#224
          FieldLabelWidth = 35
        end
        object dbedtPROVINCIA: TUniDBEdit
          Left = 832
          Top = 136
          Width = 66
          Height = 22
          Hint = ''
          DataField = 'Provincia'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          TabOrder = 9
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Prov.'
          FieldLabelWidth = 30
        end
        object cbbLkProfessione: TUniDBLookupComboBox
          Left = 16
          Top = 168
          Width = 414
          Hint = ''
          ListField = 'DESCRIZ'
          ListSource = DMdatiAge.dsClientiProfessioni
          KeyField = 'COD_PROFESSIONE'
          ListFieldIndex = 0
          DataField = 'Professione'
          DataSource = dsoEdit
          TabOrder = 10
          ReadOnly = True
          Color = clYellow
          FieldLabel = 'Professione'
          FieldLabelWidth = 65
        end
        object dbedtDescrAttività: TUniDBEdit
          Left = 436
          Top = 168
          Width = 390
          Height = 22
          Hint = ''
          DataField = 'DescrizioneAttivita'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          TabOrder = 11
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Attivit'#224
          FieldLabelWidth = 60
          ReadOnlyMode = urmNotEditable
        end
        object cbbTelefono: TUniDBLookupComboBox
          Left = 16
          Top = 198
          Width = 414
          Hint = ''
          ListField = 'NUMERO'
          ListSource = DMdatiAgeClienti.dsTelefoni
          KeyField = 'NUMERO'
          ListFieldIndex = 0
          DataField = 'Telefono'
          DataSource = dsoEdit
          TabOrder = 12
          Color = clWindow
          FieldLabelWidth = 65
        end
        object cbbMail: TUniDBLookupComboBox
          Left = 436
          Top = 198
          Width = 390
          Hint = ''
          ListField = 'NUMERO'
          ListSource = DMdatiAgeClienti.dsMail
          KeyField = 'NUMERO'
          ListFieldIndex = 0
          DataField = 'EMail'
          DataSource = dsoEdit
          TabOrder = 13
          Color = clWindow
          FieldLabel = 'Mail'
          FieldLabelWidth = 60
        end
      end
    end
  end
  object pnlDatiFissi: TUniPanel [3]
    Left = 0
    Top = 97
    Width = 912
    Height = 105
    Hint = ''
    Align = alTop
    ParentFont = False
    TabOrder = 2
    BorderStyle = ubsFrameRaised
    Caption = ''
    object dbedtPolizza: TUniDBEdit
      Left = 21
      Top = 6
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'NPolizza'
      DataSource = dsoEdit
      ParentFont = False
      Font.Height = -19
      TabOrder = 1
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Polizza N.'
      FieldLabelAlign = laTop
    end
    object dbedtAgenzia: TUniDBEdit
      Left = 125
      Top = 6
      Width = 190
      Height = 41
      Hint = ''
      DataField = 'AgenziaNome'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      ParentFont = False
      TabOrder = 2
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Agenzia'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
    end
    object dbedtCodAge: TUniDBEdit
      Left = 321
      Top = 6
      Width = 55
      Height = 41
      Hint = ''
      DataField = 'Agenzia'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      ParentFont = False
      TabOrder = 3
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Cod.age'
      FieldLabelWidth = 40
      FieldLabelAlign = laTop
    end
    object dbedtSubAge: TUniDBEdit
      Left = 382
      Top = 6
      Width = 55
      Height = 41
      Hint = ''
      DataField = 'subAgenzia'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      ParentFont = False
      TabOrder = 4
      TabStop = False
      ReadOnly = True
      FieldLabel = 'Collaboratore'
      FieldLabelWidth = 50
      FieldLabelAlign = laTop
      OnDblClick = dbedtSubAgeDblClick
    end
    object dbedtSubPromoter: TUniDBEdit
      Left = 468
      Top = 6
      Width = 55
      Height = 41
      Hint = ''
      DataField = 'SubPromoterSigla'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      ParentFont = False
      TabOrder = 6
      TabStop = False
      ReadOnly = True
      FieldLabel = 'Sotto cod'
      FieldLabelWidth = 40
      FieldLabelAlign = laTop
      OnDblClick = dbedtSubPromoterDblClick
    end
    object dbedtSostPol: TUniDBEdit
      Left = 687
      Top = 6
      Width = 70
      Height = 41
      Hint = ''
      DataField = 'NumPolizzaSostituita'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 10
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Sost. Pol.'
      FieldLabelWidth = 50
      FieldLabelAlign = laTop
      OnChangeValue = dbedtSostPolChangeValue
    end
    object dtpckScadSostPol: TUniDBDateTimePicker
      Left = 805
      Top = 6
      Width = 95
      Height = 41
      Hint = ''
      DataField = 'ScadenzaPolizzaSostituita'
      DataSource = dsoEdit
      DateTime = 43748.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      ReadOnly = True
      TabOrder = 12
      ParentFont = False
      Color = clYellow
      FieldLabel = 'Scad. Sost. Pol.'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
    end
    object dtpckDataEffetto: TUniDBDateTimePicker
      Left = 21
      Top = 51
      Width = 90
      Height = 41
      Hint = ''
      DataField = 'Decorrenza'
      DataSource = dsoEdit
      DateTime = 43748.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 13
      ParentFont = False
      FieldLabel = 'Decorrenza'
      FieldLabelWidth = 50
      FieldLabelAlign = laTop
    end
    object dtpckDataScadenza: TUniDBDateTimePicker
      Left = 117
      Top = 51
      Width = 90
      Height = 41
      Hint = ''
      DataField = 'Scadenza'
      DataSource = dsoEdit
      DateTime = 43748.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 14
      ParentFont = False
      FieldLabel = 'Scadenza'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
      OnExit = dtpckDataScadenzaExit
    end
    object cbbFrazionamento: TUniDBComboBox
      Left = 242
      Top = 51
      Width = 84
      Height = 41
      Hint = ''
      DataField = 'Frazionamento'
      DataSource = dsoEdit
      Items.Strings = (
        'Annuale=A'
        'Semestrale=S'
        'Quadrimestrale=Q'
        'Trimestrale=T'
        '')
      ParentFont = False
      TabOrder = 15
      FieldLabel = 'Frazionamento'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
      IconItems = <>
    end
    object dbedtDurataAnni: TUniDBEdit
      Left = 333
      Top = 51
      Width = 63
      Height = 41
      Hint = ''
      DataField = 'DurataAnni'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 16
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Dur. anni'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
    end
    object dbedtDurataMesi: TUniDBEdit
      Left = 401
      Top = 51
      Width = 66
      Height = 41
      Hint = ''
      DataField = 'DurataMesi'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 17
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Dur. mesi'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
    end
    object dbedtRateoGG: TUniDBEdit
      Left = 473
      Top = 51
      Width = 70
      Height = 41
      Hint = ''
      DataField = 'DurataGiorni'
      DataSource = dsoEdit
      TabOrder = 18
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Rateo GG'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
    end
    object ckbxRatino: TUniDBCheckBox
      Left = 558
      Top = 71
      Width = 97
      Height = 17
      Hint = ''
      DataField = 'Ratino1Rata'
      DataSource = dsoEdit
      Caption = 'Ratino in 1 rata'
      TabOrder = 20
      ParentColor = False
      Color = clBtnFace
      FieldLabelWidth = 0
    end
    object dtpckScad1Rata: TUniDBDateTimePicker
      Left = 671
      Top = 51
      Width = 95
      Height = 41
      Hint = ''
      DataField = 'ScadenzaPrimaRata'
      DataSource = dsoEdit
      DateTime = 43748.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      ReadOnly = True
      TabOrder = 21
      Color = clYellow
      FieldLabel = 'Scad. 1a rata'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
    end
    object dtpckDataEmissione: TUniDBDateTimePicker
      Left = 805
      Top = 51
      Width = 95
      Height = 41
      Hint = ''
      DataField = 'DataEmissione'
      DataSource = dsoEdit
      DateTime = 43748.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 22
      FieldLabel = 'Emissione'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
    end
    object ckbxTacitoRinnovo: TUniDBCheckBox
      Left = 558
      Top = 53
      Width = 97
      Height = 17
      Hint = ''
      Visible = False
      DataField = 'TacitoRinnovo'
      DataSource = dsoEdit
      Caption = 'S.T.R.'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 19
      ParentColor = False
      Color = clBtnFace
    end
    object dbedtIntermediataDaSigla: TUniDBEdit
      Left = 556
      Top = 6
      Width = 90
      Height = 41
      Hint = ''
      DataField = 'intermediataDaSigla'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      ParentFont = False
      TabOrder = 8
      TabStop = False
      ReadOnly = True
      FieldLabel = 'Intermed. da'
      FieldLabelWidth = 80
      FieldLabelAlign = laTop
    end
    object btnSelezionaProduttore: TUniMenuButton
      Left = 441
      Top = 26
      Width = 25
      Height = 25
      Hint = ''
      DropdownMenu = unpmnSelProduttore
      Caption = ''
      TabOrder = 5
      Images = untvmglstIcons
      ImageIndex = 0
    end
    object btnSelezionaSubproduttore: TUniMenuButton
      Left = 526
      Top = 26
      Width = 25
      Height = 25
      Hint = ''
      DropdownMenu = unpmnSelSubCollaboratore
      Caption = ''
      TabOrder = 7
      Images = untvmglstIcons
      ImageIndex = 0
    end
    object btnIntermediataDa: TUniMenuButton
      Left = 649
      Top = 26
      Width = 25
      Height = 25
      Hint = ''
      DropdownMenu = unpmnIntermediataDa
      Caption = ''
      TabOrder = 9
      Images = untvmglstIcons
      ImageIndex = 0
    end
    object btnPolizzaSostituita: TUniMenuButton
      Left = 762
      Top = 26
      Width = 36
      Height = 25
      Hint = ''
      DropdownMenu = unpmnPolSost
      Caption = ''
      TabOrder = 11
      Images = untvmglstIcons
      ImageIndex = 5
    end
    object UniBBincAnno: TUniBitBtn
      Left = 210
      Top = 58
      Width = 21
      Height = 18
      Hint = ''
      Caption = '+'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 23
      IconAlign = iaCenter
      Images = untvmglstIcons
      OnClick = UniBBincAnnoClick
    end
    object UniBBdecAnno: TUniBitBtn
      Left = 210
      Top = 77
      Width = 21
      Height = 18
      Hint = ''
      Caption = '-'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 24
      IconAlign = iaCenter
      Images = untvmglstIcons
      OnClick = UniBBincAnnoClick
    end
  end
  object pnlPremiPolizza: TUniPanel [4]
    Left = 0
    Top = 202
    Width = 912
    Height = 105
    Hint = ''
    Align = alTop
    TabOrder = 3
    BorderStyle = ubsFrameRaised
    Caption = ''
    object cbbFrazionamento1: TUniDBComboBox
      Left = 21
      Top = 5
      Width = 90
      Height = 41
      Hint = ''
      DataField = 'Frazionamento'
      DataSource = dsoEdit
      Items.Strings = (
        'Annuale=A'
        'Bimestrale=B'
        'Mensile=M'
        'Quadrimestrale=Q'
        'Semestrale=S'
        'temporaneo=P'
        'Trimestrale=T'
        'Unico=U')
      ParentFont = False
      TabStop = False
      TabOrder = 1
      Color = clYellow
      FieldLabel = 'Frazionamento'
      FieldLabelWidth = 60
      FieldLabelAlign = laTop
      ReadOnly = True
      IconItems = <>
    end
    object dtpckRataFirmaFinoAl: TUniDBDateTimePicker
      Left = 21
      Top = 53
      Width = 90
      Height = 41
      Hint = ''
      DataField = 'ScadenzaPrimaRata'
      DataSource = dsoEdit
      DateTime = 43748.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      ReadOnly = True
      TabOrder = 10
      Color = clYellow
      FieldLabel = 'Rt Firma fino a'
      FieldLabelAlign = laTop
    end
    object dbedtPremioNetto: TUniDBNumberEdit
      Left = 119
      Top = 5
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'Netto1'
      DataSource = dsoEdit
      Alignment = taRightJustify
      TabOrder = 2
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Premio netto'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtIntFrazionamento: TUniDBNumberEdit
      Left = 303
      Top = 5
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'InteressiFrazionamento'
      DataSource = dsoEdit
      Alignment = taRightJustify
      TabOrder = 4
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Int.Frazionam.'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtImponibile1: TUniDBNumberEdit
      Left = 412
      Top = 5
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'Imponibile1'
      DataSource = dsoEdit
      TabOrder = 5
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Imponibile'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtImposte1: TUniDBNumberEdit
      Left = 518
      Top = 5
      Width = 80
      Height = 41
      Hint = ''
      DataField = 'Imposte1'
      DataSource = dsoEdit
      TabOrder = 6
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Imposte'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtTotale1: TUniDBNumberEdit
      Left = 605
      Top = 5
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'Totale1'
      DataSource = dsoEdit
      TabOrder = 7
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Tot. da pagare'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtNetto2: TUniDBNumberEdit
      Left = 119
      Top = 53
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'Netto2'
      DataSource = dsoEdit
      TabOrder = 11
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Premio netto'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtAccessori2: TUniDBNumberEdit
      Left = 226
      Top = 53
      Width = 70
      Height = 41
      Hint = ''
      DataField = 'Accessori2'
      DataSource = dsoEdit
      TabOrder = 12
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Accessori'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtRimbPolSost: TUniDBNumberEdit
      Left = 303
      Top = 53
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'RimborsoSost'
      DataSource = dsoEdit
      TabOrder = 13
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Rimb. pol. sost.'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtImponibile2: TUniDBNumberEdit
      Left = 412
      Top = 53
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'Imponibile2'
      DataSource = dsoEdit
      TabOrder = 14
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Imponibile'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtImposte2: TUniDBNumberEdit
      Left = 518
      Top = 53
      Width = 80
      Height = 41
      Hint = ''
      DataField = 'Imposte2'
      DataSource = dsoEdit
      TabOrder = 15
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Imposte'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtTotale2: TUniDBNumberEdit
      Left = 605
      Top = 53
      Width = 100
      Height = 41
      Hint = ''
      DataField = 'Totale2'
      DataSource = dsoEdit
      TabOrder = 16
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Totale'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object undbrdgrpProvvig: TUniDBRadioGroup
      Left = 714
      Top = 5
      Width = 104
      Height = 42
      Hint = ''
      DataField = 'Provvig'
      DataSource = dsoEdit
      Caption = 'Provvigioni'
      ParentFont = False
      TabOrder = 8
      Items.Strings = (
        'Ric'
        'Prec')
      Columns = 2
      Values.Strings = (
        'R'
        'P')
      OnClick = undbrdgrpProvvigClick
    end
    object dbedtPercAccessori: TUniDBNumberEdit
      Left = 714
      Top = 53
      Width = 104
      Height = 41
      Hint = ''
      DataField = 'PercAccessori'
      DataSource = dsoEdit
      TabOrder = 17
      TabStop = False
      Color = clYellow
      ReadOnly = True
      MaxValue = 15.000000000000000000
      FieldLabel = 'Accessori annui %'
      FieldLabelWidth = 150
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object dbedtPremioAccessori: TUniDBNumberEdit
      Left = 226
      Top = 5
      Width = 70
      Height = 41
      Hint = ''
      DataField = 'Accessori1'
      DataSource = dsoEdit
      Alignment = taRightJustify
      ParentFont = False
      TabOrder = 3
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Accessori'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object btnPiuNPercAccessori: TUniBitBtn
      Left = 824
      Top = 53
      Width = 21
      Height = 22
      Hint = ''
      Caption = '+'
      ParentFont = False
      TabOrder = 18
      IconAlign = iaCenter
      Images = untvmglstIcons
      OnClick = btnPiuNPercAccessoriClick
    end
    object btnMenoNPercAccessori: TUniBitBtn
      Left = 824
      Top = 75
      Width = 21
      Height = 22
      Hint = ''
      Caption = '-'
      ParentFont = False
      TabOrder = 19
      IconAlign = iaCenter
      Images = untvmglstIcons
      OnClick = btnPiuNPercAccessoriClick
    end
    object UniDBProvvigioni: TUniDBNumberEdit
      Left = 824
      Top = 5
      Width = 76
      Height = 41
      Hint = ''
      DataField = 'Provvigioni'
      DataSource = dsoEdit
      Alignment = taRightJustify
      ParentFont = False
      TabOrder = 9
      TabStop = False
      Color = clYellow
      ReadOnly = True
      FieldLabel = 'Provv.'
      FieldLabelAlign = laTop
      DecimalSeparator = ','
    end
    object BaccMax: TUniButton
      Left = 849
      Top = 52
      Width = 49
      Height = 20
      Hint = ''
      Caption = 'MAX'
      TabOrder = 20
      OnClick = BaccMaxClick
    end
    object BaccMin: TUniButton
      Left = 849
      Top = 76
      Width = 49
      Height = 20
      Hint = ''
      Caption = 'min'
      TabOrder = 21
      OnClick = BaccMaxClick
    end
  end
  object pnlHeadPolizza: TUniPanel [5]
    Left = 0
    Top = 69
    Width = 912
    Height = 28
    Hint = ''
    Align = alTop
    ParentFont = False
    TabOrder = 5
    Caption = ''
    object lblHeadPolizza: TUniLabel
      Left = 21
      Top = 5
      Width = 152
      Height = 13
      Hint = ''
      Caption = 'Stai preparando la polizza: '
      ParentFont = False
      Font.Color = clRed
      Font.Style = [fsBold]
      ParentColor = False
      Color = clBtnFace
      TabOrder = 1
    end
    object lblDescrPolizza: TUniLabel
      Left = 178
      Top = 5
      Width = 3
      Height = 13
      Hint = ''
      Caption = ''
      ParentFont = False
      Font.Color = clRed
      Font.Style = [fsBold]
      TabOrder = 2
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 872
    Top = 25
    inherited actConferma: TAction
      OnUpdate = actConfermaUpdate
    end
    object actUscita: TAction
      Category = 'EditOperation'
      Caption = 'Esci'
      OnExecute = actUscitaExecute
    end
    object actSelezionaCollaboratore: TAction
      Category = 'EditPolizza'
      Caption = 'Seleziona collaboratore'
      Hint = 'Seleziona collaboratore'
      OnExecute = actSelezionaCollaboratoreExecute
    end
    object actSelezionaContraente: TAction
      Category = 'EditPolizza'
      Hint = 'Seleziona contraente'
      OnExecute = actSelezionaContraenteExecute
    end
    object actRiprendiPolizza: TAction
      Category = 'EditOperation'
      Caption = 'Riprendi Polizza'
      OnExecute = actRiprendiPolizzaExecute
    end
    object actSelezionaPolizzaDaSostituire: TAction
      Category = 'EditPolizza'
      Caption = 'Seleziona Polizza da sostituire'
      Hint = 'Seleziona polizza da sostituire'
      OnExecute = actSelezionaPolizzaDaSostituireExecute
    end
    object actAnteprimaStampa: TAction
      Category = 'EditOperation'
      Caption = 'Anteprima Stampa'
      OnExecute = actAnteprimaStampaExecute
    end
    object actAzzeraValori: TAction
      Category = 'EditOperation'
      Caption = 'Azzera Valori'
      Enabled = False
      OnExecute = actAzzeraValoriExecute
    end
    object actSelezionaSubCollaboratore: TAction
      Category = 'EditPolizza'
      Caption = 'Seleziona subcollaboratore'
      Hint = 'Seleziona subcollaboratore'
      OnExecute = actSelezionaSubCollaboratoreExecute
    end
    object actIntermediataDa: TAction
      Category = 'EditPolizza'
      Caption = 'Seleziona interemediata da'
      Hint = 'Seleziona Intermediata da'
      OnExecute = actIntermediataDaExecute
    end
    object actTogliCollaboratore: TAction
      Category = 'EditPolizza'
      Caption = 'Togli collaboratore'
      OnExecute = actTogliCollaboratoreExecute
    end
    object actTogliSubcollaboratore: TAction
      Category = 'EditPolizza'
      Caption = 'Togli Subcollaboratore'
    end
    object actTogliIntermediataDa: TAction
      Category = 'EditPolizza'
      Caption = 'Togli Intermediata da'
      OnExecute = actTogliIntermediataDaExecute
    end
    object actTogliPolizzaSostituita: TAction
      Category = 'EditPolizza'
      Caption = 'Togli polizza sostituita'
      Enabled = False
      OnExecute = actTogliPolizzaSostituitaExecute
    end
    object actRicalcolaPremio: TAction
      Category = 'EditPolizza'
      Caption = 'Calcola rimborso'
      Enabled = False
      OnExecute = actRicalcolaPremioExecute
    end
  end
  inherited dsoEdit: TDataSource
    Left = 728
    Top = 27
  end
  inherited untvmglstIcons: TUniNativeImageList
    Width = 12
    Height = 12
    Left = 836
    Top = 22
    Images = {
      07000000FFFFFF1F0375050000FFD8FFE000104A464946000101010060006000
      00FFE202B04943435F50524F46494C45000101000002A06C636D73043000006D
      6E74725247422058595A2007E30004000C000C0037001D616373704150504C00
      00000000000000000000000000000000000000000000000000F6D60001000000
      00D32D6C636D7300000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000D646573630000012000
      0000406370727400000160000000367774707400000198000000146368616400
      0001AC0000002C7258595A000001D8000000146258595A000001EC0000001467
      58595A0000020000000014725452430000021400000020675452430000021400
      0000206254524300000214000000206368726D0000023400000024646D6E6400
      00025800000024646D64640000027C000000246D6C7563000000000000000100
      00000C656E5553000000240000001C00470049004D0050002000620075006900
      6C0074002D0069006E002000730052004700426D6C7563000000000000000100
      00000C656E55530000001A0000001C005000750062006C006900630020004400
      6F006D00610069006E000058595A20000000000000F6D6000100000000D32D73
      6633320000000000010C42000005DEFFFFF325000007930000FD90FFFFFBA1FF
      FFFDA2000003DC0000C06E58595A200000000000006FA0000038F50000039058
      595A20000000000000249F00000F840000B6C458595A20000000000000629700
      00B787000018D9706172610000000000030000000266660000F2A700000D5900
      0013D000000A5B6368726D00000000000300000000A3D70000547C00004CCD00
      00999A0000266700000F5C6D6C756300000000000000010000000C656E555300
      0000080000001C00470049004D00506D6C756300000000000000010000000C65
      6E5553000000080000001C0073005200470042FFDB0043000302020302020303
      030304030304050805050404050A070706080C0A0C0C0B0A0B0B0D0E12100D0E
      110E0B0B1016101113141515150C0F171816141812141514FFDB004301030404
      05040509050509140D0B0D141414141414141414141414141414141414141414
      1414141414141414141414141414141414141414141414141414141414FFC200
      11080010001003011100021101031101FFC40015000101000000000000000000
      00000000000607FFC40014010100000000000000000000000000000000FFDA00
      0C03010002100310000001BD030564FCFFC40018100003010100000000000000
      00000000000405060301FFDA0008010100010502CEA4021940D6B3A6D288165D
      C11ABB8058FF00FFC40014110100000000000000000000000000000020FFDA00
      08010301013F011FFFC40014110100000000000000000000000000000020FFDA
      0008010201013F011FFFC4002310000103030305010000000000000000000201
      0304001112052122132331416182FFDA0008010100063F0291019781D94D0DD0
      10D39AF2B8A7D4C6A78EA10523232A98108A8FE56FEE9D7343E835A8BF881BCF
      AAEC097B5BCEFBD347327C599172EEB645EBE71F35FFC4001C10000203010003
      000000000000000000000111002141513171E1FFDA0008010100013F21B97DEC
      EA0F47188AB0F0CC92F402F91472274ED08109E6FA846F405B5EC981403C5A9F
      FFDA000C03010002000300000010904FFFC40014110100000000000000000000
      000000000020FFDA0008010301013F101FFFC400141101000000000000000000
      00000000000020FFDA0008010201013F101FFFC4001A10010101000301000000
      0000000000000001112100315161FFDA0008010100013F102B6D55890C5A3298
      FBC279F0301AAC215277A29CECCB1F865EC1D98D6C1C65B6CD6C5C141109C4F3
      FFD9FFFFFF1F049D08000089504E470D0A1A0A0000000D494844520000001000
      000010080200000090916836000005257A5458745261772070726F66696C6520
      747970652065786966000078DA9D575B76EC280CFC6715B3044B201ECB11AF73
      6607B3FC296CC0B7DDE993E4DA6923CB2044950A3BA6FDF76F37FFE060F6CE38
      09D127EF0F1C2EB9C40A231ED7A1E7950E775EE7CDB18C17BFD90F182E8BD65E
      B7D14F7F839FE1D6E92F338EC22F7F042A7300E5D707A5CDF8714E407BC2F3B0
      744D70CCC0A6CE4096AF07E4662273804F31FCB9843EFB3B9D9E78FDCCB8382B
      ECC49138D785ADF2E183775CBD86C38B0F7C046F899BC54C1C83040B54DBF003
      5575A3552307425C5DBD654E5C6053104FBEF8145C487CD88A5FB427F85C6626
      33C3756F96C30544AF823911AED96413AE34966C8107EE15AD3BAFC4C323A76D
      71159B6620F08F34106C011A6F96BF3ABE82C2FC168B4F50984F58CCA27C29BA
      6DD1BBDF7C558C7975B18F1AF2BB7DF39F81481E0FEC9E9FFF9C39FB69F1AB1F
      C51CCC0B8AF1FEF55E63EFED5A9D3A0F94FC5CD45AE269A15F1EF09B7398C709
      BC219188769C0967045605955E21838CB35022267B74725449A9533BDB4200DC
      B0E3C6608119F8DBD3196D186C9CF5E3C6499D032AA98208B605AC5A7879E582
      CC30ADC1A59CB345CC5C095D99108C30E457A7F9ED8071F63E244F340B77FC80
      11F1D039D218CC8D2BBA8111EA135439015EE7F318BC5A3028E6843962817AE4
      2B4416BA6BCB9E445B8443491FD71643A1CE004355980AB990B1A0E0F0D02079
      3A02732002901104295267EB38830112E18A24D959EBC14DE4313586043ABBB2
      18BEFCD8ABC184403F01DC40EB20CB41551EBB421C4A132B4E44BC0489924421
      35077D7AEC5C63CF57132C7426C18710624841A38D2E4AF431C41853D4C4D851
      5C920441A6985252C59C8AC80AAD2B3AA866CED6649725FB1C72CC296B41F914
      57A4F8124A2CA968E56A2B36AAEA6BA8B1A6AA8D1A4AA9B926CDB7D0624B4D3B
      99CEDD766C2BDDF7D0634F5D376B3465FB3C7FC11A4DD6F8646A740C9B357843
      5821686C2782F2C536E289B1D781B141010A9A07674724E7783037383B124315
      C24852063795066360D03562E934B833277937737FCD9B01F44FDEF86F983383
      BABF60EE8DB72F59AB7ABE8AEDC9D050E100F5B0505F0F4D39E20FFBFA6B6B9E
      0EE66958C07059949C9093F13ABF5AB71C4164B9CCF2818A611DC773D4E70772
      3D200794D8442C43D7B3A1D86915CD8F81CFC8D4C1E0342BDEFDD3DCC1A4AC15
      F9D1609EBD449E8E01F065F554A7D31C9DCB03A61DC17D00F6ABD6BC8CECB266
      B72DA665EEF9774676593DDE19F1A3D75FB6EF817AB62BC156FCE36159ABB551
      5FD7677E04C00FDA3B90CD7E59FE09CACE83D613A8E3BBA5FDB66D7AAEDFC0AC
      E13BDE6F446CB32B71AAB3F26C5B951D750648CFB56C99B955E79B79BF0BDC8F
      02375785A759E0477E08E958955F64599DF236E558E18CD6ADAEB48556253C95
      D9596F8DB56546BB869852EEE1696F0F5EBED37E20D9197824639E29F8BC27A6
      76E7B0D239EA46DD956D96A066DA876E3D8BBF6B7C735CF49B2A308DF516E036
      5557BDEDDA00F17736796F2D8B1D038C3732F8FC5C0BDC6C74BE39F2F746F646
      B3F929CFDB9BEF4D34E49D8237BBD03632E9DE6DF2D656A1BD2CF137C81BEF6C
      F81B717FA90EDB747BFD35AD1920F79D8DD637C13F685E2CDF0B105DA2FD15CD
      9F5836CBAEF167347F62D97C47737D25F50BA95C1E736AE59E83B754F4966BD9
      263E1266C01B387FC2692E0EB67BBF043BBE4B6EA9E8C6F0A34496A1B798CBCA
      2A24F9B84F3D5FE86F95BD7B042D1B3FB7CDB7F7C2CE489E9F351B9BB2F1A83F
      7D65EF97FE6233B8B5F3897EAAF88E0FB2F1FFFEFF8ADD22C4B468D3FF000001
      85694343504943432070726F66696C65000028917D913D48C35014854FD34A45
      2A0E761071C8509D2C888A082E5A85225408B542AB0E262FFD83260D498A8BA3
      E05A70F067B1EAE0E2ACAB83AB2008FE80B8B83A29BA4889F7258516315E78BC
      8FF3EE39BC771F20342A4CB3426380A6DB663A9910B3B95531FC8A1002886006
      719959C69C24A5E05B5FF7D44B7517E759FE7D7F56AF9AB718101089679961DA
      C41BC4539BB6C1799F38CA4AB24A7C4E3C6AD205891FB9AE78FCC6B9E8B2C033
      A366263D4F1C25168B1DAC74302B991AF124714CD574CA17B21EAB9CB7386B95
      1A6BDD93BF3092D75796B94E6B08492C6209124428A8A18C0A6CC469D749B190
      A6F3848F7FD0F54BE452C8550623C702AAD020BB7EF03FF83D5BAB3031EE2545
      1240D78BE37C0C03E15DA059779CEF63C7699E00C167E04A6FFBAB0D60FA93F4
      7A5B8B1D017DDBC0C5755B53F680CB1D60E0C9904DD99582B4844201783FA36F
      CA01FDB740CF9A37B7D6394E1F800CCD2A75031C1C022345CA5EF7797777E7DC
      FEED69CDEF07957972B5ADC4EE37000000097048597300002E2300002E230178
      A53F760000000774494D4507E304010D0402938235520000004174455874436F
      6D6D656E740043524541544F523A2067642D6A7065672076312E302028757369
      6E6720494A47204A50454720763632292C207175616C697479203D2038300AB1
      8732A40000012D4944415428CF8D52BDCAC240109CBD5C9AF800DA5A9AD79158
      F86E621910B4B0F215446D432A2585103B8358C9CD596C38F2F9E9E737D5DEED
      CC2CFB2324D1C1E3F1288AC23907C05A9BA6A9B55644BCF700440424493AE7AE
      D76B55558BC522884564B95C5655D5348D738EA4F71E248FC7E3783C0E24FC82
      884C2693D3E944122477BB1DFE81C3E140D202180C06ABD5EAABA0DFEF8B76E3
      BDAFEBBA2CCB3FD8A3D148056DD3DBED168031268E639D4F144500E23836C600
      D8EFF7DAB4E9DACCE7F3F57A0D20CFF33CCF016C369BD96CD6E5D8EE2349925E
      AFA781EE27FC285E2BBC85AE2CCCD77CCA7D129BAFAE2F782FF8B46F11699BD6
      D95D2E173DBB109CCFE7BAAE95D096D53D344D936559F056B3603C9D4E6FB79B
      3285A4E6EEF77B7B5E3F1145D170384C92449F4F8146B936D968EB7300000000
      49454E44AE426082FFFFFF1F03750D0000FFD8FFE000104A4649460001010200
      2500250000FFE107FA45786966000049492A000800000005001A010500010000
      004A0000001B0105000100000052000000280103000100000003000000310102
      000C0000005A0000003201020014000000660000007A000000BD000000050000
      00BD0000000500000047494D5020322E39392E3100323031393A30343A303120
      31353A31353A3133000800000104000100000000010000010104000100000000
      0100000201030003000000E00000000301030001000000060000000601030001
      000000060000001501030001000000030000000102040001000000E600000002
      020400010000000C07000000000000080008000800FFD8FFE000104A46494600
      010100000100010000FFDB004300080606070605080707070909080A0C140D0C
      0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C3031
      3434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C
      2132323232323232323232323232323232323232323232323232323232323232
      32323232323232323232323232323232323232FFC00011080100010003012200
      021101031101FFC4001F00000105010101010101000000000000000001020304
      05060708090A0BFFC400B5100002010303020403050504040000017D01020300
      041105122131410613516107227114328191A1082342B1C11552D1F024336272
      82090A161718191A25262728292A3435363738393A434445464748494A535455
      565758595A636465666768696A737475767778797A838485868788898A929394
      95969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9
      CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4
      001F0100030101010101010101010000000000000102030405060708090A0BFF
      C400B51100020102040403040705040400010277000102031104052131061241
      510761711322328108144291A1B1C109233352F0156272D10A162434E125F117
      18191A262728292A35363738393A434445464748494A535455565758595A6364
      65666768696A737475767778797A82838485868788898A92939495969798999A
      A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
      D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C030100021103
      11003F00F7FA28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
      800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
      800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280390A28A2B9CE43
      3E8A28AE83AC28A28A0028A28A0028A28A00EFE8A28A0028A28A0028A28A0028
      A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A008E8A28AC8B
      384A28A2B520EFE8A28A0028A28A0028A28A008E8A28AC8B394A28A2A0E13AFA
      28A2BA0EB0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A28
      00A28A28023A28A2B22C28A28A00928A28AD480A28A2800A28A2800A28A28038
      0A28A2803BFA28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2
      800A28A2800A28A28023A28A2B22C28A28A00928A28AD480A28A2800A28A2800
      A28A280380A28A2803BFA28A2800A28A2800A28A2800A28A2800A28A2800A28A
      2800A28A2800A28A2800A28A28023A28A2B22CE128A28AD483BFA28A2800A28A
      2800A28A2800A28A280380A28A2803BFA28A2800A28A2800A28A2800A28A2800
      A28A2800A28A2800A28A2800A28A2800A28A2800A28A280380A28A2803BFA28A
      2800A28A2800A28A280390A28A2B9CE43ABA28A2ACEE24A28A2B52028A28A002
      8A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A0028A28A00E
      428A28AE7390EAE8A28AB3B8928A28AD480A28A2803C828A28AA03D7E8A28A90
      0A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A280
      0A28A2800A28A2800A28A280380A28A2800A28A28039FA28A2A80F5BA28A2B02
      C928A28AD480A28A2800A28A2800A28A2800A28A2800A28A2800A28A2800A28A
      2800A28A28023A28A2B22CE528A28A8384CFA28A2BA0EB3BFA28A28023A28A2B
      22C28A28A00928A28AD48380A28A2803428A28AE7390EBE8A28AE83AC28A28A0
      028A28A0028A28A0028A28A00E428A28AE7390CFA28A2BA0EB0A28A2803BBA28
      A2B22CE128A28AD483BFA28A2803CDA8A28AD0E136A8A28AE32CEAE8A28AB3B8
      E528A28A8384EAE8A28AB3B8E128A28AD480A28A2803BBA28A2B22C928A28AD4
      80A28A280380A28A2803BFA28A2800A28A2800A28A280390A28A2B9CE43ABA28
      A2ACEE388A28A2BDB3C93B7A28A2BC43D6384A28A2B520EFE8A28A0028A28A00
      28A28A0028A28A00E028A28A00EFE8A28A008E8A28AC8B384A28A2B520EFE8A2
      8A0028A28A0028A28A0028A28A00E028A28A00928A28AF58F30B945145782741
      9F45145741D677F4514500145145001451450014514500701451450077F45145
      0047451456459E49451456E41EB74514560592514515A9014514500145145007
      2145145739C867D14515D075851451401DDD1451591649451456A40514514011
      D145159167294514541C262D14515D841E9345145667705145140051451401C8
      514515CE7219F45145741D614514500145145001451450014514500145145001
      45145001451450014514500145145007774514564592514515A9014514500145
      1450014514500145145001451450014514500145145001451450014514500145
      14500145145001451450014514500145145001451450014514500145145007FF
      D9FFE202B04943435F50524F46494C45000101000002A06C636D73043000006D
      6E74725247422058595A2007E300040001000D000D002A616373704150504C00
      00000000000000000000000000000000000000000000000000F6D60001000000
      00D32D6C636D7300000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000D646573630000012000
      0000406370727400000160000000367774707400000198000000146368616400
      0001AC0000002C7258595A000001D8000000146258595A000001EC0000001467
      58595A0000020000000014725452430000021400000020675452430000021400
      0000206254524300000214000000206368726D0000023400000024646D6E6400
      00025800000024646D64640000027C000000246D6C7563000000000000000100
      00000C656E5553000000240000001C00470049004D0050002000620075006900
      6C0074002D0069006E002000730052004700426D6C7563000000000000000100
      00000C656E55530000001A0000001C005000750062006C006900630020004400
      6F006D00610069006E000058595A20000000000000F6D6000100000000D32D73
      6633320000000000010C42000005DEFFFFF325000007930000FD90FFFFFBA1FF
      FFFDA2000003DC0000C06E58595A200000000000006FA0000038F50000039058
      595A20000000000000249F00000F840000B6C458595A20000000000000629700
      00B787000018D9706172610000000000030000000266660000F2A700000D5900
      0013D000000A5B6368726D00000000000300000000A3D70000547C00004CCD00
      00999A0000266700000F5C6D6C756300000000000000010000000C656E555300
      0000080000001C00470049004D00506D6C756300000000000000010000000C65
      6E5553000000080000001C0073005200470042FFDB0043000302020302020303
      030304030304050805050404050A070706080C0A0C0C0B0A0B0B0D0E12100D0E
      110E0B0B1016101113141515150C0F171816141812141514FFDB004301030404
      05040509050509140D0B0D141414141414141414141414141414141414141414
      1414141414141414141414141414141414141414141414141414141414FFC200
      11080010001003011100021101031101FFC40017000003010000000000000000
      000000000002030508FFC4001601010101000000000000000000000000000100
      02FFDA000C03010002100310000001D4198525C347FFC4001910010003010100
      00000000000000000000010203041231FFDA0008010100010502B8173F9A7B6B
      A1911FFFC40017110100030000000000000000000000000010011141FFDA0008
      010301013F0126F0FFC400181100020300000000000000000000000000000110
      2131FFDA0008010201013F0185B623FFC4002110000300010207010000000000
      000000000102110012210341425161C1F0F1FFDA0008010100063F02404B0A7A
      4CE58F0D50D06F7EDEE3D1C467A6269ABE3D1C4D21C36D534C503F3B67FFC400
      1D1001000202020300000000000000000000011121004131617191B1FFDA0008
      010100013F2105847E41B4D57CC5208EAB25536DE0A327A9929C087B278D65D5
      E0AA84D815ECEB3FFFDA000C030100020003000000105D0FFFC4001A11000203
      0101000000000000000000000000210141F03181FFDA0008010301013F105640
      A9BED6387DDE1FFFC4001A110003010003000000000000000000000000011131
      416171FFDA0008010201013F107A84443A9CF9D08D69FFC40019100101010003
      0000000000000000000000011121003151FFDA0008010100013F1065D5D74912
      04D3A7BF1797A49FC002A2908DF3C9CEC201A8D4AB21D2A934BC19808076398E
      9116B1C1FFD9FFFFFF1F040905000089504E470D0A1A0A0000000D4948445200
      000020000000200806000000737A7AF40000000473424954080808087C086488
      000000097048597300000EC400000EC401952B0E1B000004AB494441545885BD
      964B68945714C77FF7FBE69BE93C4C08DA09D416076C4D176954DAE6515A1282
      C58ABA6BADE0A28BEE1A210E820F5C986084488D144AD14D2916C456454D415A
      EA222D91D0488363D4986122C6D49834C519539299CC7CF3DDDBC598711E79CC
      24A107EEE6DE73CEFF7FCE3DF79C2B2627276B344D3BA59472B14C69ED19FBB5
      E3A38A83C5D8D884104D4AA9F7960B0EF06F5C6EFAF0EC2DC7F5CF36EF2BD446
      534A194A295662814229D95CFF6DEF5705130056043C45202542D1FCFE999E82
      486852CA42C92E2E19244035D77CF3FBA94509AC54F44A29DE5CA550C9446A99
      714422EEFFA0E3DA822444381C3E0FEC5E6EF0009665119E8AA10930740D4313
      689A86CBE55A535A5AFA742E1B9B941221447A63646484B1B1B18241376CD840
      595919009AA6B1A6C45D14695BEE86C3E1C0ED2EDC89CD96E7A238022AAB70C0
      EBF5E2F57A8B7292EBA32802CB75B05CC9AB81FF9D006467201008100C0697EC
      D09D48D0505545C9AA55A89212D4EAD50BEA8BF1F1F1F34288F433B42C8B6432
      591C6A2482DED58571EB16F6A9A9AC23E576432CF6BD4C26BF5C7DE5CADD3909
      B0D43EA0147A57177A672724128BE95AC0D7658671505CBC9856D6A4944BEB7C
      52A29F3B8776E1022A1E5F5C1F7405FBC289C4CFA33B76A4477F5E0D4C4E4E12
      8944160DFE95BE3E8CEE6E1480A6A1FB7C58C3C3903B5B84489D8D8C80650134
      DA75FD2CF0093CEF0399AF201C0EF3E4C99305C11D1313ACBF7E3D455CD771F9
      FD18B5B598376F12EDE880D91AD2349C4D4DD8EBEB310301A2274ECC5ED5C713
      DBB7EFF65EBBF6435E067C3E1F3E9F6F4102FA9933202512B0373662D4D60260
      5457E3DABF9FE993275152E2DEBB177B7D7DEA6CD3268C6DDB885FBD3A9B9916
      053F16DF07A6A7211048939EB97F1FC7D414368F278B848AC5D2E000C9991962
      77EF225E045BF1F7D6ADB5457F48181A42259328295152C2F03091D656CC8CE7
      67AFA9C1D1D090051E696B8360306DA7A4C4D2B486BC0C3C78F080D1D1D17913
      F0DAE020AFE7B6EE6090484B0B652D2D18CF3391091E3E760C71E70EB95F1FA1
      D4FABC51565E5E8ECB35FF07D9333E3EF7EC0885480E0C605457676D5B434388
      8181396D2438F308783C1E3C3951648A2A2F4FA53E53348D92E6669C39E0008E
      CA4A4A0E1CE0597BFB8BD7F182C0536D5EA4F908AC5D9B5D174250E2F7E36C6C
      4CEB2463B1AC9A70D4D5517AE8104AD7B36C2DA5EEE465C0344DE2F1F8FC0CBC
      5EEC4E27B6681400D7962D79E0FFB4B6A2666678B9AD2D5D132FD5D5E1DCB993
      E9CB9753812825A74DF3973C02A15088870F1F2E9885F5EBD6F1C6BD7B004443
      213CB11836A793642CC6C4D1A3A8DBB70198387204EFF1E3181E0F5622417470
      307D7D26FCF45677F75FE2D1A34759D3B01091D128E6E1C3E8CF5BB6A8ACC4B5
      6B17D14B9750FDFDD9CA1515B8F7EC21DAD989EAEB4BD9436C3A91D8BCB1A727
      B8240200663088D5DE8E5EE4E8B640CD58D6E755376E7C07A0C5E3F1C452BE64
      4645059ADF8FE97020A52C6825A534A3A6F9C52C3880E8EFEFAF9252B60B219C
      45B300ACC78FD14F9FF6D89F3D7B5B87397BBA044C29FBE296D5F44E6F6F6FE6
      D98A7D06FFA8AEDE68E8FAA73AD46942BC8A52BA8471097F5A525E7EB7B7F7B7
      B9ECFE03BBCE5D0AC8C74C130000000049454E44AE426082FFFFFF1F04210900
      0089504E470D0A1A0A0000000D4948445200000020000000200806000000737A
      7AF40000000473424954080808087C086488000000097048597300000EC40000
      0EC401952B0E1B000008C3494441545885AD976B8C5E4519C77F33E7BCF7EBEE
      BBF74B77BB6D5968690B054B0A5483212008914B84881862146310E3050C3131
      4AE2374C8C21119084C8251283A8144154941245A014B6ADA5174AB7A57BA3BB
      7D77DFDDF776DE7399193F9C77B72D972E1F98643227739ECCF39F79FECFCCFF
      11DBB76FEFEBEFEF7F301E8F9F8331824FA309A1EBF5FACEE9E9E9EF6CDDBAB5
      742653BBBBBBFB8142A170CDA7E2F894964A25577B8DC63C70C7190118A3D7D5
      6AD54FDB3F0052CA7397B3B19DBA13C380901221400A89100221040881100027
      C7B0198C39391A639A5D9FFCD606A7D190CB0288C7E36E3A93C1F73C8C31A173
      2910A70281A571B199D033A6F96DB4C61889361A4B5AD8B68DD24A2F0B404889
      005A5A5AC8B7B47CAC61A55CA65AABE2B91E00896492133333A452295CD70520
      9E888382642A850A02A4B496F38F6D59E12EED48E48C86FBF7EFC3711C6AD52A
      0B0B0BECDE35C2EA356B705D97A1A15594CB65E6E666914272D3576EC1188314
      CB27952D84851DB1F17DFF8C861B366E64FAF8349EE7118946B8FA8BD7303D3D
      4D3C1E279E4890482428CDCD01A094C2B2AC4F06400A41341AC36D34989C9838
      493E9A713F857CF1448278220140C375C9E57318133AAC562AD89148C80DC0B2
      2C84F5094220A4444A492D30186CD0A08DA12D97A2EEFA044A634C38D79249D2
      F07CBC40618C416B4D3619C70F140DCF476B83D20AA10531293F44DC8F06D034
      3A3C3587E7078C1D2FD29ACF32DCD74EB15CA754AE2084441958D5D346A5EE22
      A560BE524369437B3E8D94924069C68F174926E2A413512E5E3B8094CB662152
      4233ED60E4E028B1A84D188570AE5A6FF0DF5DFB713D1F304BFF2666664F4BD1
      86EBB1E7D0514C7361D10CE3B200901281C07103D6AF19A4DEF0F0FC000334BC
      80D65C96B5AB079AB115B85E80E3FAB4E573B84DE27A7E0042B27178083F0896
      EE934FD2EC45B29DB3A2032F500CF77702D0D7D1426B3685D3F0387BA00B630C
      3D6D79AA6D2E957A03BA5AD15AD3D992C5F503166A755674E4D15AD39A490221
      999705B0C8F2D1F767A93B2E53C5390AF91C5E1030335F03633832394D2E93A6
      DEF098AF39080487C6A628E4B3CC556A80C00F14422B52510B2F22A8A148C4E3
      174D4E4EEEAA552AAF54CBE5C7376DDEBCF3C321204C354B4A5A72699C868B25
      43E452088AF36576BE7D084B0A4C73CEF13C5E7C6D1796253106221256B4A648
      76281E3BBE8DEBFF790FEB7F7F2B1BFF705BF4BA177F7CDE4313CFDF496772C7
      91C3879F7DF3CD37BB3F0020CC723F50B89ECFCADE2EFC66EA0581A23597E182
      756BF095060C7EA08845225C79F1267C5F11F81E035D597EF1CE137C61DB5DFC
      6B720419B3E92FF4D0D7DA8DB2E0AFC77670C5B33F140F8F3F7F6D5777E75BBB
      77EF5E7A25C5D8D8D868A150183A5EAAA074C8616D0C9D2D592A8E8BDF24A436
      868E7C867AC3C3F17C8C31B8AE4B2E13E1D6ED3FE3587586D6640EA4414A9002
      30A001ADA13356E040F12817B4AFE157177E6F6A7C6C7CD3962D5BA6ED452407
      C766B02C89E37AD41B1E1B867A98282E00506BB838AEC7BAC11EE6CA7594D678
      7EC039FD6DFCFC7F8F70CC99A133972569699216444533BF0D280397F46EE5DB
      E77E8D1FBD7A3F7F3BFA1A0FBFF76CCFD77BAEBA1FB85912DA11B12D4A0B155E
      DAB107DB92045A635B12C775F9CBCB3BB0A445A034520ABC20E0EFAFBC412952
      E28FC7B63394CFD01B37F4250C3D094357D2D09580AE246C6AEFE5BB1B6E2315
      8973EFE6DBE94867F8CDC16D3851FFCB2323236B6D302152AD49C4A20CF674A2
      74283402A5B12D8BE1C13EB4D6180C4A6BE2D108375CB699A78E6EE3D28E1C99
      88211D818465119536B6B0914262498B6F9C7B07512B8A31867B5FFF35834943
      472CC533EFBF246ECA5F7E8B4D53D9F416722060654F3B4A6B7A0A39E2D1084A
      6B06BADA504AD15DC851755CBC40D1954BF2D81B6F7169BB64CC11F4A7FBC018
      CA6E11A5EB28A3F8ECC08D74A7FA0178F4C07318F52EED095899128C96F790EE
      BB6E8B6D0083E1C442353C5E5F51773DD28918D3A54A48363FC0713D62119B52
      D5C10F14FD852C5ACDD0126B4708CDD54337538877F3C4DBF7D1F067694BF6F2
      B915D7027064618A178EFC8EE17C8642AC4ADA5678FEFB5896D527D11AA3C3A3
      F503C5AB7B0EE2FA0155C7C50F148EEBB17DE75E1A9E4FD569E0FAE155ABB426
      6187F7455B5C7156CB7ADA931DDC7EDE4F59D57236570E7D155B86CFF32F471E
      60435E62E9DD44990120694BA4105A6A164525446C9B7C26150ACEA6B8544AE3
      F97E5384B2E47CA1E6D091EC25D01E130BBB78E69D9FA08C2613CD72C3F00F18
      C885A9FED4BB2F9213FF46985D44C402F38D4902EDD395EA451B332E8DD61820
      158F10B5259B865752C824C8A7E3E452715A7369AEBFEC22DA72295AB3295AD2
      09D2F118524A865BB750AC4F61D01C38F1347F3A780F4A2B2256148005B7C673
      87EF23691D0754B80134D3B5E3AC2B6CC675DDFF486D0C5A29A4106197A146D4
      06A414D896C492610F091BDACC942A5CD6F7252A7E62E95A3D54DCC6D307EE26
      D001000FEE7D94D5A9B1D3EE7E63A0A6725CD4FE79552F959E94E1312BAA0D8F
      89132546DE39CA6CA54EA95267A1D6A0E105389E4FB15C63B65C65BE5A67BEEA
      F0E79777A2EB692EEDBF1D65E24B0E0ECF3DCF53FBBFCFBBF3E31C9CF92DF607
      3489A7335C357427966B3F76D6FAF5A3D22885520A636062BAC8E4CCEC29C546
      38CE2D544F9B2B57EBEC1B1D67CFE8249774DEC8DAF65B81D49293A3A57FF0D0
      5B37B23A5D3E6DE79A1C17F67E93CF142E3F3C3135751780387060FF68369B1B
      DA3B56444A89D77C94CEEA6B6766BE866E3E548EEBB1AABBC07CAD8117288226
      39FBDA72AC1BEA61F78917787DF211AADE7B60FC2529600C181123175BC325FD
      DFE2ECECD6C3C562F18AC1C1C1A30062DFBE7DA3D94C66484A89B42CA4944829
      C2124D9EAC8E3EAA325AAC88B43144A351B4DD60747E07E3E511669D31049242
      7290C1DC850C64362911441F9D3B72E4EE95E79F3FBFB88EED3ACE44D998D39C
      C9531DC3C72B1B63308475E0A216EC4C9CC7AAAEADB6655929C08B44227B3DCF
      7BA5542C3D3930D03EFAC125FE0FBD2A7F0ED8CD607E0000000049454E44AE42
      6082FFFFFF1F04A007000089504E470D0A1A0A0000000D494844520000004000
      0000400806000000AA6971DE0000001974455874536F6674776172650041646F
      626520496D616765526561647971C9653C000007424944415478DAEC5B4B6C54
      5518FECFB9339DBE298F56C0D254A840203C5CA834262E202C35184DDCB83046
      2326BAD3951B75C742E34EDD98485C1812892B631465638AA014447956684B5F
      334C1FC3741EBD8F738EFF3977DACE9D69A77798D3999BB437B999295CEE3DE7
      FBFFFFFBFEFFBB850821602D1F14D6F8B10EC05A072054EA2FFB6E7EF826A5E1
      4F00485B759725064088D347F67EFC694D0120C438891CB90D1754EDC01CC4F3
      3D3C6B0B006E5E10A88D4A882AA15E1A008E1890C57510985F95D0BA3AA24E2F
      D4A24AFA5C1A80826D0AB5300E06090325FAF8D3E1266E9FBA48789E566300DC
      44240BDF39776063730F74B63F0F21DA20FF4443EC054C266FC0F8541F661C47
      DEA10BF557730038E7B01816018C3368697A0241D8AD75118484607CFA127096
      C5EF01CA003708F91C60C058BC0F92E91155063A9840D67EC68C816D67B108E8
      C23D05042603BCCBB5EC34C413FFBAB8105D246860E40916145FC45B0422030A
      7729DCD8700E3A499A52F90C63A9F4AB710620E3937C00E4A2A8016DCDBB2064
      44B481309B19C3CC4A01A1745905AA4D0630D909098F0A74771C85273B5FD02A
      83930F6FC1DFFF7D038C990B2A2002910105FD98A484393309A9EC84BB500D8B
      94AC9F9E8B03138EBADD42E4832283F925209B9589C97E984CDC5200E858A2BC
      BBCD32F82CE601B5F21E434B2BCC8BA85E4668CE4AE622A54306049693E1B6C2
      82078B03F85269289867A13A0EBC231260A86810094406E49700C7CD37376E85
      AEC77AA12ED4E4EAB686563836750D1E4CDF503FCE3F2F1025C0B9C8DF3E0E2D
      36ECE878167A3A8F6B5DC4E6965DA80477C17232D80D18556C847DF401DE3A27
      301CBDE0B6ADD4672B2CC459BCEA7DA976CBA9C04C72084C5417AC03707291C7
      FB5725054829BDFDFEFCC94B98924F7BCBC251F2487C122096CDB1AECE1D3DB8
      D38F7C5B6B08BCC552C261195121DF605DC1D7C70E7DF5E523738028DAA8646B
      E6BB5191BC8157BE0DBEAC35D96CD960DA09FC778E8E00CBE06DC4F3110110C2
      538D22179DC6C826EC88EB7C31B550B692E0C4C7751875B09D94EE2CE78FCC01
      AC400598B0A17B6B2F1CE83901A150BD5F0EA057EE7E5ED25AE3DCC2A83FC4E7
      59158FD685D6DA4A7CBA722344F21C21E6407D640334D46F54E3AB2FAEC61A2A
      65AD393C9B8B3A0EC5345299B5260D1565ADF96FD0562C01E161EC30DC19FE0D
      DBE1EBB8D8902F1E9003D4864D64496B6DDBE65E08D17A6DD65A74E6320CC77E
      C1C0B1456B4D5F1F904319D3742A31E8BFCB6336B4B4F578AC35875998491DB0
      A575BF766B6DF8C1797CA6059468C880223F20D7A131E6A8E8FB7906E38EC75A
      63DC84AC3905F7C67F82E9E40066923E6B2D9595BE4246596BDCE73D5796C1BC
      7A929B8ED43541F7F6C3EA5370E1830319CCF101F5DDC64E6FCE9A51CB652C09
      13537FAE82B546CB6AD1CB32451DC78443BB5F82A7F6BE52D6C27EEDFF405848
      74963DEBB1D6E408ACD3F83028291BCDB2FC001014A293376168EC0276AD615F
      A68564FBAC35E3C8E8E7232BA7BFCDADFB201C6AD00642223508268EEAF9D65A
      E52A90B77F420D188DFD0313F19BAE0C8A95BBC054266E75EE6867F92C22F57E
      4FD70938B0F335E505E83AA2D357E18FEB9F61A6CAF70B861E0096221B49822B
      939F0DC9744C960D8AC996827617203B37030FD3F7DD856AB2D66633136A62F5
      586B9503209696DDE51B1F15E164FA819223E5F717BC5F9064351CFD1D49F0AA
      CA005D2A6022CFC81EC3B5D6B81600082993546C24CA643AAA08CE6BAD4191B5
      26B340BBB526BD4AA14905B00FA044F827146968CC62E4F3373F3F53144F948E
      766B4D6A3F55D69ACF36DD47069CC14EBE3D775DC93B5A765AA43253AC70F378
      98851920C971437327F43C7E1C22E1166DD6DA68EC228CC52F7BACB58A0078E7
      D5B3A7F0E354A59139FDE38B9E38D9CC849DDB8FC2FE9D2F6BCD808EB67DA804
      D771B24CE1A015AA1C005D072BCA0A0A0323E7C0B2326018755AFA00A902F1C4
      1D30CD5965AD31C18203002F6A990D984E0CC1E4CCBDDC58ADEFA032F244E821
      417D00F025EB5699AE5CE75BE650EE755EC000282441297D52019A1B3AB00422
      1A5E82B82498CAC6D5F85D4E7759250EE0053F5BB0BBEB383CB3FF7508871B2B
      9643D706133034DE077DD7BEC0EED3D26388AC4E0608D54A37D66F82A68676FF
      D69A0F185A9BB6A9649252ACC510D1C6010535494918AEDDF901EE47FF720D11
      4D2A20670F139585E2D0C685081000AC38C56D6E426CF236E87C09A65A61B979
      C683C5017C891A9775EF30DBB7B5E6CB10316487A1D110592D19949BAF8FB4C2
      9EEE5EF529B81E151889F5C354E29E6F2FA07A24E8A94701B63D07470EBE01CF
      1D7E4BEB73A45B75E6E77791076673435150390027CC91E815B83D780EA811D6
      6088A8E90746F19E969951EF20B80E3F60B55440FA8183A317E1FE447FCEBCD0
      A282CAB425C4C8D97522A82530BF5E1C581C5BEFAFDDE7E60A11B85678B9742C
      DB6FAAE059B50540FB3ECB95889ACB205DD300E0F11D9E5BF10C43F57EFF496E
      3C8DE7B7252F5AFFAFB36BFC5807601D80357EFC2FC000333B0EAEC8AAA50600
      00000049454E44AE426082FFFFFF1F04AD05000089504E470D0A1A0A0000000D
      4948445200000018000000180806000000E0773DF8000005744944415478DA85
      566B4C145714FE9665D9E5B53C1490978AA854AA2268623035126977B554B41A
      AD08496B636CDAAA3F4C53DBA48D6D8DC6A6DA6A6A1A6B2A681B14A92F624B77
      15A94505A49587C84344E5A5BC9665617767676777667A6620D5D28C9DE4E6DE
      B93373BE7BCEF79D73468509D7A6F2A1FD1101AA5DD25A109FEEF30220D2062F
      8AF29A97D634BC3CAD79111E1EBF5D5815F5EA447BAA09C64BB727F9AF2CE864
      21E0BF970428F063A8129028198708C12B225107340E7ACE5E5A17BB5E11607B
      95D51CE50743E6B4A07F19F68A63A7E56878BCE3F3F8BD5BBA27972ADA9D1871
      0B0DA5EBE31728026CBB6E31C5EA60CC48D0CB1E086498F33E3536CAF1B07122
      58AF00963C71D1F057ABC81B01771F327072625569CED4258A00EF5E1B3447A8
      79C3F2D9A118F508E86705F43134335E1A02184E80CBCDC34533E32610026469
      CDD198AEF1421FACBB6CCE9B6E5404D87AB5DFECE7721B82E242606178380984
      251069968CB2B2713AF9B8612FEDF3FC185BB1BC1B5151C1672FBF99A0CCC116
      73AF89B5388CCEC921203B0AA7E54939A2ACA8672516CF31088F093D7DF5EDC4
      4DCF05F83035CCF859C97D74F53965D9483CC80A92D702191E235DF48AB2AAA4
      FB40BD16D92F27E04AD348E1EF5B67E72902BCF54B8F79CF4B9106521EC5DF2B
      EF856B7CE4D941E1F0A1B7EDE495CAC787D443E1E9B1C035388AFEC478128080
      923AEBFF005CEC36EDC98832967433D8981008954A85F2762B664606E15A0F83
      DC243DEC047CD7E2C69C9ADB88EB2C276FDCA8EA0946CB3BDB70AE66B0E8FAFB
      7372140172CF7598F667C6C8008B427DE5BD6E2B8388B040D4F5BB903EC51FFE
      BE2AFC50D98BCFAB4F22A0FD1248C778D0E98539FF3A2E560F9CAFD891BCEEF9
      00CB638C5FD65AE11E76C1472DC26E71E2BD153370A279147194ADF52D56AC4A
      8B80E1E03E84D416CBDF55AA22D15A5886E23F9E5CAAFC607EB622C01B458F4C
      0756C41A592230BF7E006E3B8B9CB429080BD0A0D5C6A1A4610829D13ACC8B19
      CBF4D02305B0B93DE8C8D988613F2DCE94779756ED4ACD5204585FD86EFE3A6B
      AA4CB29D48F5A3A73EE36F48240711E10F6D6E04697D65922D2C0F9D9A04E112
      64928BCABACBAA3F4E7D451160DD8FF7CD87B2A7196E3CB2213331948CAB70A5
      7D1829B17A983B1DC82192475D1EB40C71F0F55363C1642D3474829B9D23E864
      553865EE2AAFF9242D5311606DFE3DD3E1B509C6E24E060BF56A79AFD7E642F4
      A4319253C9A0966ACFF7D77A302F65CA3FEF740C31B0896A149676DCF873F7A2
      A58A00AB8F359B8E6C98693CDC3802CB100BAF93CA36C362E76B8938523702AD
      97436BB305864511F0E80361A57009A4A29B951DC85B3D07A77F7D58F3D7178B
      172B02AC39DA64FA76E32CA3546FCEB55AE170B0D890128940AD1A5DA31C7EAE
      B76046B81673A383284780EA3E162D149EB484106828643F95DC6FA8DB9BAE5C
      AEB38E349A8FE62519EC44982048858C8746AD86CED747269DE178B9D005E834
      32A97E94E46A5244C300794BD95D70A1ADB161DF92F9CF07C84D321C6B736073
      6280BC77E6CE20329226A1FA09832C6A448124AD53E4495C7430CEDF1DC1DEA5
      11A87D3C823EAF1AF9C54DCD77BE5AF6A222C0CA6FEA4DC736271BBF6BB663AE
      7EAC06750D38903E331CB77A9C880DD650251570FB810D93E243D14419BF2629
      0837DA86312B4E8F82334DF71A0F2E7B4111C070A0D67C7CCB5C4319D59D1315
      7DE0EC2EAC4A0DC36A52CCA715FDB0F43AC10CD8B03B37994E0C5C6EB3C36175
      62F8F1305E3724E24471734BCBA18C644580853BCB4D451FA51B751A35188A39
      47BF0CD29A94496D9324EBE028446A487D5F4A342765FC638747FE7690BADEC9
      E3B79AEFE5672B8748BA52769495844406CBF544EE05137A82D4C19EED070209
      41FA6D11ECCE01321E35D1DEDFA4831F46013CBF9D0000000049454E44AE4260
      82}
  end
  object dsGaranzia: TDataSource
    Left = 760
    Top = 24
  end
  object dsAssicurato: TDataSource
    Left = 800
    Top = 24
  end
  object unpmnPolSost: TUniPopupMenu
    Images = untvmglstIcons
    Left = 688
    Top = 61
    object mnuSelezionaPolizzaDaSostituire: TUniMenuItem
      Action = actSelezionaPolizzaDaSostituire
      ImageIndex = 4
    end
    object mnuTogliPolizzaSostituita: TUniMenuItem
      Action = actTogliPolizzaSostituita
      ImageIndex = 3
    end
    object mnuRicalcolaPremio: TUniMenuItem
      Action = actRicalcolaPremio
      ImageIndex = 6
    end
  end
  object unpmnSelProduttore: TUniPopupMenu
    Images = untvmglstIcons
    Left = 352
    Top = 61
    object mnuSelezionaCollaboratore: TUniMenuItem
      Action = actSelezionaCollaboratore
      ImageIndex = 0
    end
    object mnuTogliCollaboratore: TUniMenuItem
      Action = actTogliCollaboratore
      ImageIndex = 1
    end
  end
  object unpmnSelSubCollaboratore: TUniPopupMenu
    Images = untvmglstIcons
    Left = 456
    Top = 69
    object UniMenuItem1: TUniMenuItem
      Action = actSelezionaSubCollaboratore
      ImageIndex = 0
    end
    object UniMenuItem2: TUniMenuItem
      Action = actTogliSubcollaboratore
      ImageIndex = 1
      OnClick = UniMenuItem2Click
    end
  end
  object unpmnIntermediataDa: TUniPopupMenu
    Images = untvmglstIcons
    Left = 584
    Top = 69
    object mnuIntermediataDa: TUniMenuItem
      Action = actIntermediataDa
      Caption = 'Seleziona intermediata da'
      ImageIndex = 0
    end
    object mnuTogliIntermediataDa: TUniMenuItem
      Action = actTogliIntermediataDa
      ImageIndex = 1
    end
  end
end
