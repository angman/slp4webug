
unit UPerfezionaPolizze;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniPanel,
  uniBasicGrid, uniDBGrid, uniButton, uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, UDMDatiAgePolizzePerf,
  UdmdatiAge,
  uniEdit, uniDBEdit, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox,
  uniRadioGroup, uniDBNavigator, uniScreenMask;

type
  TFperfezionaPolizze = class(TFMasterElenco)
    actPerfezionaPolizza: TAction;
    actCancellaPolizza: TAction;
    actVisualizza: TAction;
    cbbTipoEmiss: TUniDBLookupComboBox;
    cbbPromoter: TUniDBLookupComboBox;
    cbbStatoPolizza: TUniDBLookupComboBox;
    unmbrdtNPolizza: TUniNumberEdit;
    unedtContraente: TUniEdit;
    cbbSubAge: TUniDBLookupComboBox;
    unedtSubage: TUniEdit;
    UniScreenMask1: TUniScreenMask;
    UniCBstatoPolizze: TUniComboBox;
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn; const Value: Variant);
    procedure actPerfezionaPolizzaExecute(Sender: TObject);
    procedure actVisualizzaExecute(Sender: TObject);
    procedure grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
    procedure UniCBstatoPolizzeChange(Sender: TObject);
    procedure actCancellaPolizzaExecute(Sender: TObject);
  private
    { Private declarations }
    cod_polizza: Integer;

    function controlli_perf_pol(cod_polizza: Integer): Boolean;
    procedure perfeziona_polizza;
    procedure gridRefresh(Sender: TComponent; AResult: Integer);
    procedure cancellaPolizzaDaPerfezCallBack(Sender: TComponent; Res: Integer);
  public
    { Public declarations }
  end;

function FperfezionaPolizze: TFperfezionaPolizze;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UQueryElencoIntf, UCodiciErroriPolizza, uPrintHouseSLP, UconfermaDatiIncasso,
  ServerModule, libSLP;

function FperfezionaPolizze: TFperfezionaPolizze;
begin
  Result := TFperfezionaPolizze(UniMainModule.GetFormInstance(TFperfezionaPolizze));
end;


procedure TFperfezionaPolizze.actVisualizzaExecute(Sender: TObject);
var npol: string;
begin
  // visualizza la polizza corrente se esiste nel printhouse
  if UniMainModule.VerificaAutorizzazione('STP', 'VIS') then // and (UniCBstatoPolizze.ItemIndex<>5) then
  begin

     if UniCBstatoPolizze.ItemIndex=5 then
        npol:=UniMainModule.DMdatiAgePolizzePerf.Qpol_da_perfez.FieldByName('n_preventivo').AsString
     else
        npol:=UniMainModule.DMdatiAgePolizzePerf.Qpol_da_perfez.FieldByName('n_polizza').AsString;

     if UniMainModule.IsUtentePromoter then
        TFPrintHouseSLP.ShowPrintHousePolizza(uniApplication, npol, True, false)
     else
        TFPrintHouseSLP.ShowPrintHousePolizza(uniApplication, npol, True, true);
  end else ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
end;

procedure TFperfezionaPolizze.cancellaPolizzaDaPerfezCallBack(Sender: TComponent; Res: Integer);
begin
  case Res of
    mrYes:
      begin
         // procedi con la cancellazione effettiva
         UniMainModule.DMdatiAgePolizzePerf.cancellaPolizzaDaPerfez(grdElenco.DataSource.DataSet.FieldByName('cod_polizza').asInteger);
         grdElenco.Update;
      end;
    mrNo:
      ;
  end;
end;

function TFperfezionaPolizze.controlli_perf_pol(cod_polizza: Integer): Boolean;

  function controlla_date: Boolean;
  var
    differenza: Integer;
  begin
    Result := false;
    // portato da 31 a 36 giorni il 04/06/2009  (assisomma)
    // portato da 36 a 18 giorni per le polizze nuove indicazione di VZ (22/04/2011)
    if (length(alltrim(UniMainModule.DMdatiAgePolizzePerf.Qpolizza.FieldByName('n_polizza_sostituita').AsString)) >= 5)
    then
    begin
      UniMainModule.DMdatiAge.get_codice('PPOLGG_11', differenza);
      if differenza = 0 then
        differenza := 36;
    end
    else
    begin
      UniMainModule.DMdatiAge.get_codice('PPOLGG_10', differenza);
      if differenza = 0 then
        differenza := 19;
    end;
    if (trunc(date - UniMainModule.DMdatiAgePolizzePerf.Qpolizza.FieldByName('decorrenza').asDateTime) < differenza)
    then
      Result := True
    else
    begin
      showmessage
        ('La decorrenza della polizza � troppo vecchia. Richiedere autorizzazione all''incasso al proprio referente commerciale.');
      Result := false;
    end;
  end;

begin
  // esegui i controlli sui dati della polizza che si vuole perfezionare
  Result := false;
  if UniMainModule.DMdatiAgePolizzePerf.cerca_pol_da_perf(cod_polizza) then
  begin
    // 2) verifica che le data di decorrenza della polizza sia coerente con la data odierna
    if controlla_date then
    begin
      // 3) controlla che non sia gi� presente tra le polizze in portafoglio
      if not UniMainModule.DMdatiAge.FindPolizza(UniMainModule.DMdatiAgePolizzePerf.Qpolizza.FieldByName('n_polizza')
        .AsString) then
        Result := True
      else
        showmessage
          ('La polizza � gi� presente tra le polizze perfezionate. Avvisare il proprio referente commerciale.');

    end
    else
      showmessage
        ('la data di decorrenza della polizza � troppo vecchia rispetto alla data corrente. Perfezionamento impossibile.');

  end
  else
    showmessage('Polizza da perfezionare non trovata. Perfezionamento impossibile.');
end;


procedure TFperfezionaPolizze.actCancellaPolizzaExecute(Sender: TObject);
begin
  // cancella solo se si tratta di una:
  //  polizza da perfezionare
  //  emissione di direzione o
  //  prevetivo direzione
  // controlla che l'utente sia autorizza a perfezionare le polizze
  if UniMainModule.VerificaAutorizzazione('PPP', 'PER', false) and not(UniMainModule.UtenteSoloFacSimileSLP = 'S') then
    MessageDlg('Confermi la cancellazione dela tentata vendita/preventivo ?', mtConfirmation, mbYesNo, cancellaPolizzaDaPerfezCallBack)
  else
    showmessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
end;


procedure TFperfezionaPolizze.actPerfezionaPolizzaExecute(Sender: TObject);
begin
  inherited;
  // 1) verifica che l'utente abbia le autorizzazione per perfezionare la polizza
  // UniMainModule.aggiornaPosizione('PPP', 'PER', 'Incassa Polizza');
  if UniMainModule.VerificaAutorizzazione('PPP', 'PER', false) and not(UniMainModule.UtenteSoloFacSimileSLP = 'S') then
    perfeziona_polizza
  else
    showmessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
end;

procedure TFperfezionaPolizze.grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn;
  const Value: Variant);
begin
  dsGrid.DataSet.DisableControls;
  try
    dsGrid.DataSet.Filtered      := false;
    dsGrid.DataSet.FilterOptions := [foCaseInsensitive];
    if (Column.FieldName = 'SiglaPromoterLkup') or (Column.FieldName = 'SubAgeLkup') then
    begin
      if not VarIsNull(Value) and (VarToStr(Value) <> '') then
        self.DMMaster.EseguiQuery('N_POLIZZA', 0, '', 0, 0, Value)
      else
        self.DMMaster.EseguiQuery('N_POLIZZA');
    end
    else
      inherited;
  finally
    dsGrid.DataSet.EnableControls
  end;

end;

procedure TFperfezionaPolizze.grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
begin
  DMMaster.EseguiQuery(Column.FieldName, 0, '', UniCBstatoPolizze.ItemIndex, Integer(Direction), 0);
end;

procedure TFperfezionaPolizze.perfeziona_polizza;
begin

  cod_polizza := UniMainModule.DMdatiAgePolizzePerf.Qpol_da_perfez.FieldByName('cod_polizza').AsInteger;

  if controlli_perf_pol(cod_polizza) then
  begin
    if (UniMainModule.DMdatiAgePolizzePerf.cerca_assicurati(cod_polizza) and
      UniMainModule.DMdatiAgePolizzePerf.cerca_garanzie(cod_polizza)) then
    begin
      TFconfermaDatiIncasso.ShowConfermaDatiIncasso(uniApplication, cod_polizza, 'Polizza', gridRefresh);
      grdElenco.Update;
    end
    else
      showmessage('Errore nei dati degli assicurati o nelle garanzie. Caricamento impossibile.');
  end;
end;

procedure TFperfezionaPolizze.gridRefresh(Sender: TComponent; AResult: Integer);
begin
  grdElenco.Update;
  grdElenco.Refresh;
end;

procedure TFperfezionaPolizze.UniCBstatoPolizzeChange(Sender: TObject);
begin
  // inherited;
  DMMaster.EseguiQuery('N_POLIZZA', 0, '', UniCBstatoPolizze.ItemIndex, 0, 0);
  if UniCBstatoPolizze.ItemIndex in [1,2,3,5] then
     actPerfezionaPolizza.Enabled:=false
  else actPerfezionaPolizza.Enabled:=True;

  if UniCBstatoPolizze.ItemIndex=1 then
     actCancellaPolizza.Enabled:=False
  else actCancellaPolizza.Enabled:=true;

end;

procedure TFperfezionaPolizze.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  UniMainModule.DMdatiAge.QSelPromoter.Close;
end;

procedure TFperfezionaPolizze.UniFormCreate(Sender: TObject);
begin
  inherited;
  self.DMMaster  := (UniMainModule.DMdatiAgePolizzePerf as IQueryElenco);
  dsGrid.DataSet := UniMainModule.DMdatiAgePolizzePerf.Qpol_da_perfez;
  // il filtro va ripulito sempre ...
  dsGrid.DataSet.Filtered := False;
  dsGrid.DataSet.Filter   := '';
  UniMainModule.DMdatiAge.QSelPromoter.Open;
  self.DMMaster.EseguiQuery('N_POLIZZA');
  // EditPageClassName    := 'TFCollaboratoreEdit';
  if UniMainModule.IsUtentePromoter then
    grdElenco.ColumnByName('fatta_da').Width := 0;
  UniCBstatoPolizze.ItemIndex := 0;
  btnInsert.Action    := actPerfezionaPolizza;
  btnModify.Action    := actCancellaPolizza;
  btnSeleziona.Action := actVisualizza;

end;

end.
