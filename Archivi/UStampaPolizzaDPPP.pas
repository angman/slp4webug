unit UStampaPolizzaDPPP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, Data.DB, System.Actions, Vcl.ActnList,
  uniPageControl, uniEdit, uniDBEdit, uniButton, uniBitBtn, uniSpeedButton, uniLabel,
  uniGUIBaseClasses, uniPanel, uniImageList, uniRadioGroup, uniDBRadioGroup, uniCheckBox,
  uniDBCheckBox, uniMultiItem, uniComboBox, uniDBComboBox, uniDateTimePicker, uniDBDateTimePicker,
  uniDBLookupComboBox, uniMemo, uniSpinEdit, UdmdatiAge, unimDBEdit, System.Math,
  uniScreenMask, Vcl.Menus, uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaDPPP = class(TFStampaBasePolizza)
    pgAssicurato: TUniTabSheet;
    pcOggettoAssicurato: TUniPageControl;
    pgOggettoAssicurato: TUniTabSheet;
    pgFamiliari: TUniTabSheet;
    pgMassimaliGaranzie: TUniTabSheet;
    undbrdgrpTipoAssicurato: TUniDBRadioGroup;
    pnlpncontaier: TUniContainerPanel;
    pnlAssicurato: TUniPanel;
    pnlVeicolo: TUniPanel;
    dbedtDenominazione: TUniDBEdit;
    dbedtCodiceFiscale: TUniDBEdit;
    dbedtPatente: TUniDBEdit;
    dbedtRilasciataDa: TUniDBEdit;
    dtpckDataRilascioPat: TUniDBDateTimePicker;
    dtpckDataScadenzaPat: TUniDBDateTimePicker;
    cbbCategoriaPat: TUniDBComboBox;
    dbedtNome1: TUniDBEdit;
    dbedtPatente1: TUniDBEdit;
    dbedtCategoria1: TUniDBEdit;
    dbedtNome2: TUniDBEdit;
    dbedtPatente2: TUniDBEdit;
    dbedtCategoria2: TUniDBEdit;
    dbedtNome3: TUniDBEdit;
    dbedtPatente3: TUniDBEdit;
    dbedtCategoria3: TUniDBEdit;
    dbedtNome4: TUniDBEdit;
    dbedtPatente4: TUniDBEdit;
    dbedtCategoria4: TUniDBEdit;
    dbcblkupTipoVeicolo: TUniDBLookupComboBox;
    dbedtMarca: TUniDBEdit;
    dbedtModello: TUniDBEdit;
    dbedtTarga: TUniDBEdit;
    dbedtTelaio: TUniDBEdit;
    dbedtQlHp: TUniDBEdit;
    dtpckScadenzaRevisione: TUniDBDateTimePicker;
    cbbMassimali: TUniDBComboBox;
    ckbxRischioAccessorio: TUniDBCheckBox;
    cbbRimbSpesePatPunti: TUniDBComboBox;
    dbgrpForma: TUniDBRadioGroup;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    ckbxAppendiceControversie: TUniDBCheckBox;
    ckbxIncMaxDP: TUniDBCheckBox;
    ckbxbProblematicaRCA: TUniDBCheckBox;
    ckbxPerizieParte: TUniDBCheckBox;
    ckbxMancatoInterventoRCA: TUniDBCheckBox;
    ckbxProtezioneFamiglia: TUniDBCheckBox;
    ckbxEstEPatenti: TUniDBCheckBox;
    unmEstEPatente: TUniMemo;
    dsTipoVeicolo: TDataSource;
    dbedtNumeroAssicurati: TUniDBNumberEdit;
    procedure undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure dbgrpFormaChangeValue(Sender: TObject);
    procedure ckbxProtezioneFamigliaClick(Sender: TObject);
    procedure ckbxRischioAccessorioClick(Sender: TObject);
    procedure dbedtNumeroAssicuratiChange(Sender: TObject);
  private
    { Private declarations }
    procedure EnableDisableEstFamiglia(Enable: Boolean);
    procedure EnableDisableEstRischioAccessorio(Enable: Boolean);
    procedure RiallineaMaxGaranzie;
    procedure AzzeraEstensioni;
  protected

    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); override;

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;

  end;

function FStampaPolizzaDPPP: TFStampaPolizzaDPPP;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiPolizzaOM, UDMDatiTempPolizzaOM, UDMDatiTempPolizza, UDMDatiTempPolizzaDPPP;

function FStampaPolizzaDPPP: TFStampaPolizzaDPPP;
begin
  Result := TFStampaPolizzaDPPP(UniMainModule.GetFormInstance(TFStampaPolizzaDPPP));
end;

procedure TFStampaPolizzaDPPP.AzzeraEstensioni;
begin
   // azzera le estensioni eventualmente richiamate

   if ckbxRischioAccessorio.Checked then
   begin
      ckbxRischioAccessorio.Checked:=false;
      dsoEdit.DataSet.FieldByName('RimborsoSpesePatPunti').AsBoolean:=False;
      cbbRimbSpesePatPunti.ItemIndex:=-1;
      dsoEdit.DataSet.FieldByName('SiglaRimborsoSpesePatPunti').AsString:='';
   end;

end;

procedure TFStampaPolizzaDPPP.ckbxProtezioneFamigliaClick(Sender: TObject);
begin
  dbedtNumeroAssicurati.Enabled := ckbxProtezioneFamiglia.Checked;
  pgFamiliari.TabVisible        := ckbxProtezioneFamiglia.Checked;
  if not ckbxProtezioneFamiglia.Checked then
  begin
    pcOggettoAssicurato.ActivePage := pgAssicurato;
    dbedtNumeroAssicurati.Value    := 0;
    pgFamiliari.TabVisible         := false
  end
  else
  begin
    if Sender <> nil then
      dbedtNumeroAssicurati.Value := 1;
    pgFamiliari.TabVisible        := True;
  end;
  dbedtNumeroAssicuratiChange(nil);
end;

procedure TFStampaPolizzaDPPP.ckbxRischioAccessorioClick(Sender: TObject);
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
  if (not ckbxRischioAccessorio.Checked) then
    cbbRimbSpesePatPunti.Text := '';
end;

procedure TFStampaPolizzaDPPP.dbedtNumeroAssicuratiChange(Sender: TObject);
var
  i: Integer;

  procedure EnableDisableFamiliari(Enable: Boolean);
  begin
    TUniDBEdit(FindComponent('DBEdtNome' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtPatente' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtCategoria' + i.ToString)).Enabled := Enable;

  end;

begin
  for i := 1 to 4 do
    EnableDisableFamiliari(i <= dbedtNumeroAssicurati.Value);
end;

procedure TFStampaPolizzaDPPP.dbgrpFormaChangeValue(Sender: TObject);
var
  selectedForma: string;
begin
  selectedForma := dbgrpForma.Values[dbgrpForma.ItemIndex];

  if ((UniMainModule.DMDatiTempPolizza.fdmtblAssicuratoTipoVeicolo.AsString = 'AV') or
    (UniMainModule.DMDatiTempPolizza.fdmtblAssicuratoTipoVeicolo.AsString = 'CM') or
    (UniMainModule.DMDatiTempPolizza.fdmtblAssicuratoTipoVeicolo.AsString = 'MC')) and (selectedForma = 'B') and
    (undbrdgrpTipoAssicurato.ItemIndex = 1) then

  begin
    showmessage('Non � possibile usare la forma B con AutoVeicoli, CicloMotori o MotoCicli !!!');
    dbgrpForma.ItemIndex := 0;
    selectedForma        := 'A';
  end
  else
  begin

    cbbMassimali.DataField := '';
    try
      UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
        selectedForma);

    finally
      cbbMassimali.DataField := 'SiglaMassimale';
    end;
  end;
  EnableDisableEstFamiglia(dbgrpForma.ItemIndex = 0);
end;

procedure TFStampaPolizzaDPPP.EnableDisableEstFamiglia(Enable: Boolean);
begin
  // ckbxProtezioneFamiglia.Enabled := Enable;
  // dbedtNumeroAssicurati.Enabled  := False;
  // ckbxProtezioneFamiglia.Checked := False;

end;

procedure TFStampaPolizzaDPPP.EnableDisableEstRischioAccessorio(Enable: Boolean);
begin
  ckbxRischioAccessorio.Enabled := Enable;
  cbbRimbSpesePatPunti.Enabled  := Enable and ckbxRischioAccessorio.Checked;
end;

procedure TFStampaPolizzaDPPP.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  AzzeraEstensioni;
  inherited;
  RiallineaMaxGaranzie;
end;

procedure TFStampaPolizzaDPPP.RiallineaMaxGaranzie;
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
  ckbxProtezioneFamigliaClick(nil);
  undbrdgrpTipoAssicuratoChangeValue(nil);
end;

procedure TFStampaPolizzaDPPP.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited SetCliente(ASelectedItem, ASelectedDescription);
  // undbrdgrpTipoAssicurato.readonly  := UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta;
  undbrdgrpTipoAssicurato.ItemIndex := ifThen(UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta, 1, 0);
end;

procedure TFStampaPolizzaDPPP.undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
begin
  pnlAssicurato.visible := undbrdgrpTipoAssicurato.ItemIndex = 0;
  if not(dsAssicurato.DataSet.State in [dsInsert, dsEdit]) then dsAssicurato.DataSet.Edit;

  pnlVeicolo.visible     := undbrdgrpTipoAssicurato.ItemIndex = 1;
  pgFamiliari.TabVisible := undbrdgrpTipoAssicurato.ItemIndex = 0;
  // dbgrpForma.Enabled     := undbrdgrpTipoAssicurato.ItemIndex = 1;
  // dbgrpForma.ItemIndex   := undbrdgrpTipoAssicurato.ItemIndex;
  EnableDisableEstFamiglia(undbrdgrpTipoAssicurato.ItemIndex = 0);
  EnableDisableEstRischioAccessorio((undbrdgrpTipoAssicurato.ItemIndex = 0)); // and ckbxRischioAccessorio.Checked);

end;

procedure TFStampaPolizzaDPPP.UniFormCreate(Sender: TObject);
begin
  inherited;
  pcDatiEreditabili.ActivePage   := pgContraente;
  pcOggettoAssicurato.ActivePage := pgOggettoAssicurato;

  // IWFraAssicuratoPolizzaOM.OnAfterTipoAssicuratoChanged := UniMainModule.DMDatiTempPolizza.SetDatiAssicuratoDaContraente;

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;

  ckbxMancatoInterventoRCA.Enabled  := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICEDP');
  ckbxAppendiceControversie.Enabled := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICECC');
  // ckbxTacitoRinnovo.Enabled         := UniMainModule.DMdatiAge.TacitoRinnovoEnabled;
  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaForma.AsString);
  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  // TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.asstring :=
  TDMDatiTempPolizzaDPPP(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.AsString :=
    Copy(cbbMassimali.Text, 1, 3);
  // TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzaSiglaMassimaleChange(nil);
  dsTipoVeicolo.DataSet.Open;

  pnlAssicurato.visible := True;
  pnlVeicolo.visible    := false;
  pcOggettoAssicurato.Pages[1].Visible:=false;
  undbrdgrpTipoAssicurato.OnChangeValue := undbrdgrpTipoAssicuratoChangeValue;
end;

initialization

RegisterClass(TFStampaPolizzaDPPP);

end.
