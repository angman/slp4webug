unit UStampeSLP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses,
  uniURLFrame, uniSplitter, uniDBComboBox, uniDBLookupComboBox, uniMultiItem,
  uniComboBox;

type
  TFStampeSLP = class(TFMasterElenco)
    actVisualizzaDocumento: TAction;
    actEsportaDocumento: TAction;
    UniPanelFilter: TUniSimplePanel;
    UniCBtipoStampe: TUniComboBox;
    DStipoStampe: TDataSource;
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoFieldImageURL(const Column: TUniDBGridColumn; const AField: TField; var OutImageURL: string);
    procedure actVisualizzaDocumentoExecute(Sender: TObject);
    procedure actEsportaDocumentoExecute(Sender: TObject);
    procedure actChiudiExecute(Sender: TObject);
    procedure UniCBtipoStampeChange(Sender: TObject);
  private
    FComingFromLogin: Boolean;
    PDFFolderPath: string;
    PDFUrl: string;
    { Private declarations }
    function getDocumentFileName(ACodDocument: Integer): string;
    procedure SetComingFromLogin(const Value: Boolean);
    procedure caricaTipoStampe;
  public
    { Public declarations }
    property ComingFromLogin: Boolean write SetComingFromLogin;

  end;

function FStampeSLP: TFStampeSLP;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiReport, ServerModule, System.IOUtils, System.StrUtils, UStampePDFViewer;

function FStampeSLP: TFStampeSLP;
begin
  Result := TFStampeSLP(UniMainModule.GetFormInstance(TFStampeSLP));
end;

procedure TFStampeSLP.actChiudiExecute(Sender: TObject);
begin
  UniMainModule.DMDatiReport.QtipoStampe.close;
  inherited;
end;


procedure TFStampeSLP.actEsportaDocumentoExecute(Sender: TObject);
var
  afilename: string;
begin
  afilename := getDocumentFileName(dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
  if (afilename <> '') then // and FileExists(afilename) then
  begin
    UniSession.SendFile(PDFFolderPath + '\' + afilename);
    UniMainModule.DMDatiReport.UpdDataStampaDoc(dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
    TFile.delete(PDFFolderPath + '\' + afilename);
  end;
end;

procedure TFStampeSLP.actVisualizzaDocumentoExecute(Sender: TObject);
var
  afilename: string;
begin
  afilename := getDocumentFileName(dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
  if (afilename <> '') then // and FileExists(afilename) then
  begin
    Cursor := crHourGlass;
    try
      TFPDFViewer.ShowReport(UniApplication, afilename, PDFUrl, PDFFolderPath, False, True);
      UniMainModule.DMDatiReport.UpdDataVisualizzazioneDoc(dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
      dsGrid.DataSet.DisableControls;
      dsGrid.DataSet.Close;
      dsGrid.DataSet.Open;
      dsGrid.DataSet.EnableControls;
    finally
      Cursor := crDefault;
    end;
  end;
end;

procedure TFStampeSLP.caricaTipoStampe;
begin
   UniMainModule.DMDatiReport.QtipoStampe.First;
   while not UniMainModule.DMDatiReport.QtipoStampe.Eof do
   begin
      UniCBtipoStampe.Items.Add(UniMainModule.DMDatiReport.QtipoStampe.FieldByName('tipo_doc').AsString+'- '+
                 UniMainModule.DMDatiReport.QtipoStampe.FieldByName('descrizione').AsString);
      UniMainModule.DMDatiReport.QtipoStampe.Next;
   end;
end;

function TFStampeSLP.getDocumentFileName(ACodDocument: Integer): string;
begin

  Result := UniMainModule.DMDatiReport.SalvaTempDocument(PDFFolderPath, ACodDocument, not FComingFromLogin, tdDocHouse);

end;

procedure TFStampeSLP.SetComingFromLogin(const Value: Boolean);
begin
  FComingFromLogin := Value;
  UniMainModule.DMDatiReport.getDocuments(grdElenco.DataSource.DataSet, FComingFromLogin);
end;

procedure TFStampeSLP.grdElencoFieldImageURL(const Column: TUniDBGridColumn; const AField: TField;
  var OutImageURL: string);

var
  imagesPath: string;

begin
  imagesPath := TPath.Combine(UniServerModule.FilesFolderURL, 'Images/');

  if SameText(AField.FieldName, 'DATA_LETTURA') then
  begin
    if AField.IsNull then
      OutImageURL := imagesPath + 'flag_mark_red.png'
    else
      OutImageURL := imagesPath + 'flag_mark_green.png';
  end
  else
    inherited;
end;

procedure TFStampeSLP.UniCBtipoStampeChange(Sender: TObject);
var i: Integer;
begin
   i:=Pos('-',UniCBtipoStampe.Text);
   UniMainModule.DMDatiReport.openDocHouse(Copy(UniCBtipoStampe.Text,1,i-1));
end;


procedure TFStampeSLP.UniFormCreate(Sender: TObject);
begin
  inherited;
  PDFUrl            := UniServerModule.FilesFolderURL + 'PDF/';
  PDFFolderPath     := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  dsGrid.DataSet    := UniMainModule.DMDatiReport.QdocHouse;
  UniMainModule.DMDatiReport.QtipoStampe.Open;
  caricaTipoStampe;
  UniCBtipoStampe.ItemIndex:=-1;
  btnModify.Action  := actEsportaDocumento;
  btnModify.Enabled := UniMainModule.VerificaAutorizzazione('STD', 'STA');
  btnInsert.Action  := actVisualizzaDocumento;
  btnInsert.Enabled := UniMainModule.VerificaAutorizzazione('STD', 'VIS');
  UniMainModule.aggiornaPosizione('STD', 'VIS', 'Elenco documenti agenzia');
end;

end.
