unit UAgenziaEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, uniDBEdit, uniEdit, uniRadioGroup, uniDBRadioGroup,
  Data.DB, System.Actions, Vcl.ActnList, uniButton, uniBitBtn, uniSpeedButton, uniLabel,
  uniGUIBaseClasses, uniPanel, uniCheckBox, uniDBCheckBox, uniDateTimePicker, uniDBDateTimePicker,
  uniImageList;

type
  TFAgenziaEdit = class(TFMasterEdit)
    pnlEdit: TUniSimplePanel;
    undbrdgrpTipologia: TUniDBRadioGroup;
    dbedtNumChiusure: TUniDBNumberEdit;
    dbedtIntestazione: TUniDBEdit;
    ckbxStampaIntestazioneInRiquadro: TUniDBCheckBox;
    dbedtIndirizzo: TUniDBEdit;
    dbedtCap: TUniDBEdit;
    dbedtCitta: TUniDBEdit;
    dbedtProvincia: TUniDBEdit;
    dbedtCodFisc: TUniDBEdit;
    dbedtPIva: TUniDBNumberEdit;
    dbedtTelefono1: TUniDBEdit;
    dbedtTelefono2: TUniDBEdit;
    dbedtFax: TUniDBEdit;
    dbedtCellulare: TUniDBEdit;
    dbedtMail: TUniDBEdit;
    dbedtPec: TUniDBEdit;
    dbedtSito: TUniDBEdit;
    dbedtNRui: TUniDBEdit;
    dtpckDataIscrizRui: TUniDBDateTimePicker;
    dbedtPreponenti: TUniDBEdit;
    undbrdgrpContoSepFid: TUniDBRadioGroup;
    procedure undbrdgrpTipologiaClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FAgenziaEdit: TFAgenziaEdit;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function FAgenziaEdit: TFAgenziaEdit;
begin
  Result := TFAgenziaEdit(UniMainModule.GetFormInstance(TFAgenziaEdit));
end;

procedure TFAgenziaEdit.undbrdgrpTipologiaClick(Sender: TObject);
begin
  dbedtNRui.Enabled          := undbrdgrpTipologia.ItemIndex = 0;
  dtpckDataIscrizRui.Enabled := undbrdgrpTipologia.ItemIndex = 0;
  if undbrdgrpTipologia.ItemIndex = 1 then
  begin
    UniMainModule.DMdatiAge.ClearRuiData;
  end;
end;

procedure TFAgenziaEdit.UniFormCreate(Sender: TObject);
begin
  inherited;
  DSoEdit.DataSet := UniMainModule.DMdatiAge.QAgenziaEdit;
  DSoEdit.DataSet.Edit;

  undbrdgrpTipologiaClick(nil);
end;

end.
