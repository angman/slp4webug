inherited FAgenziaEdit: TFAgenziaEdit
  ClientHeight = 511
  ClientWidth = 628
  Caption = 'Editazione dati Agenzia'
  ExplicitWidth = 644
  ExplicitHeight = 550
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 628
    ExplicitWidth = 628
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 628
    ExplicitWidth = 628
  end
  object pnlEdit: TUniSimplePanel [2]
    Left = 0
    Top = 69
    Width = 628
    Height = 442
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    object undbrdgrpTipologia: TUniDBRadioGroup
      Left = 26
      Top = 6
      Width = 239
      Height = 40
      Hint = ''
      DataField = 'TIPO'
      DataSource = dsoEdit
      Caption = 'Tipologia'
      TabOrder = 1
      Items.Strings = (
        'Societ'#224
        'Ditta individuale')
      Columns = 2
      Values.Strings = (
        'S'
        'D')
      OnClick = undbrdgrpTipologiaClick
    end
    object dbedtNumChiusure: TUniDBNumberEdit
      Left = 402
      Top = 24
      Width = 201
      Height = 22
      Hint = ''
      DataField = 'NUMERO_CHIUSURE'
      DataSource = dsoEdit
      TabOrder = 2
      MaxValue = 3.000000000000000000
      ShowTrigger = True
      FieldLabel = 'Numero chiusure'
      DecimalPrecision = 0
      DecimalSeparator = ','
    end
    object dbedtIntestazione: TUniDBEdit
      Left = 26
      Top = 52
      Width = 577
      Height = 22
      Hint = ''
      DataField = 'DENOMINAZIONE'
      DataSource = dsoEdit
      TabOrder = 3
      FieldLabel = 'Intestazione'
      FieldLabelWidth = 80
    end
    object ckbxStampaIntestazioneInRiquadro: TUniDBCheckBox
      Left = 26
      Top = 80
      Width = 337
      Height = 17
      Hint = ''
      DataField = 'stampa_intestazione_in_polizza'
      DataSource = dsoEdit
      Caption = 'Stampa l'#39'intestazione nel riquadro agenzia/broker delle polizze'
      TabOrder = 4
      ParentColor = False
      Color = clBtnFace
    end
    object dbedtIndirizzo: TUniDBEdit
      Left = 26
      Top = 103
      Width = 577
      Height = 22
      Hint = ''
      DataField = 'INDIRIZZO'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      ParentFont = False
      TabOrder = 5
      FieldLabel = 'Indirizzo'
      FieldLabelWidth = 80
    end
    object dbedtCap: TUniDBEdit
      Left = 26
      Top = 131
      Width = 124
      Height = 22
      Hint = ''
      DataField = 'CAP'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 6
      FieldLabel = 'Cap'
      FieldLabelWidth = 80
    end
    object dbedtCitta: TUniDBEdit
      Left = 153
      Top = 131
      Width = 320
      Height = 22
      Hint = ''
      DataField = 'CITTA'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      TabOrder = 7
      FieldLabel = 'Citt'#224
      FieldLabelWidth = 30
    end
    object dbedtProvincia: TUniDBEdit
      Left = 479
      Top = 131
      Width = 124
      Height = 22
      Hint = ''
      DataField = 'PROVINCIA'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      TabOrder = 8
      FieldLabel = 'Provincia'
      FieldLabelWidth = 60
    end
    object dbedtCodFisc: TUniDBEdit
      Left = 242
      Top = 159
      Width = 231
      Height = 22
      Hint = ''
      DataField = 'C_FISCALE'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      TabOrder = 10
      FieldLabel = 'Codice fiscale'
    end
    object dbedtPIva: TUniDBNumberEdit
      Left = 26
      Top = 159
      Width = 210
      Height = 22
      Hint = ''
      DataField = 'P_IVA'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 9
      FieldLabel = 'P.Iva'
      FieldLabelWidth = 80
      DecimalPrecision = 0
      DecimalSeparator = ','
    end
    object dbedtTelefono1: TUniDBEdit
      Left = 26
      Top = 187
      Width = 210
      Height = 22
      Hint = ''
      DataField = 'TELEFONO1'
      DataSource = dsoEdit
      TabOrder = 11
      FieldLabel = 'Tel1'
      FieldLabelWidth = 80
    end
    object dbedtTelefono2: TUniDBEdit
      Left = 242
      Top = 187
      Width = 231
      Height = 22
      Hint = ''
      DataField = 'TELEFONO2'
      DataSource = dsoEdit
      TabOrder = 12
      FieldLabel = 'Tel2'
    end
    object dbedtFax: TUniDBEdit
      Left = 26
      Top = 215
      Width = 210
      Height = 22
      Hint = ''
      DataField = 'FAX'
      DataSource = dsoEdit
      TabOrder = 13
      FieldLabel = 'Fax'
      FieldLabelWidth = 80
    end
    object dbedtCellulare: TUniDBEdit
      Left = 242
      Top = 215
      Width = 231
      Height = 22
      Hint = ''
      DataField = 'CELLULARE'
      DataSource = dsoEdit
      TabOrder = 14
      FieldLabel = 'Cell'
    end
    object dbedtMail: TUniDBEdit
      Left = 26
      Top = 245
      Width = 447
      Height = 22
      Hint = ''
      DataField = 'E_MAIL'
      DataSource = dsoEdit
      TabOrder = 15
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  Ext.apply(s' +
          'ender,{allowBlank:false,vtype:'#39'email'#39',mstarget:'#39'side'#39'})'#13#10'}')
      InputType = 'email'
      FieldLabel = 'E-mail'
      FieldLabelWidth = 80
    end
    object dbedtPec: TUniDBEdit
      Left = 26
      Top = 273
      Width = 447
      Height = 22
      Hint = ''
      DataField = 'PEC'
      DataSource = dsoEdit
      TabOrder = 16
      ClientEvents.UniEvents.Strings = (
        
          'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'    Ext.apply' +
          '(sender,{allowBlank:false,vtype:'#39'email'#39',mstarget:'#39'side'#39'})'#13#10'}')
      InputType = 'email'
      FieldLabel = 'Pec'
      FieldLabelWidth = 80
    end
    object dbedtSito: TUniDBEdit
      Left = 26
      Top = 301
      Width = 447
      Height = 22
      Hint = ''
      DataField = 'SITO'
      DataSource = dsoEdit
      TabOrder = 17
      FieldLabel = 'Sito'
      FieldLabelWidth = 80
    end
    object dbedtNRui: TUniDBEdit
      Left = 26
      Top = 329
      Width = 210
      Height = 22
      Hint = ''
      DataField = 'N_RUI'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 18
      FieldLabel = 'N. Iscrizione Rui societ'#224
      FieldLabelWidth = 80
    end
    object dtpckDataIscrizRui: TUniDBDateTimePicker
      Left = 242
      Top = 329
      Width = 231
      Hint = ''
      DataField = 'DATA_ISCRIZIONE_RUI'
      DataSource = dsoEdit
      DateTime = 43749.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 19
      ParentFont = False
      FieldLabel = 'Data iscrizione RUI societ'#224
    end
    object dbedtPreponenti: TUniDBEdit
      Left = 26
      Top = 357
      Width = 447
      Height = 22
      Hint = ''
      DataField = 'PREPONENTI'
      DataSource = dsoEdit
      TabOrder = 20
      FieldLabel = 'Imprese Preponenti'
      FieldLabelWidth = 80
    end
    object undbrdgrpContoSepFid: TUniDBRadioGroup
      Left = 26
      Top = 387
      Width = 255
      Height = 40
      Hint = ''
      DataField = 'contosep_fidej'
      DataSource = dsoEdit
      Caption = 'L'#39'intermediario ha'
      TabOrder = 21
      Items.Strings = (
        'Conto separato'
        'Fidejussione')
      Columns = 2
      Values.Strings = (
        'C'
        'F')
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 400
    Top = 41
  end
  inherited dsoEdit: TDataSource
    Left = 352
  end
end
