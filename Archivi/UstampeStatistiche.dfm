inherited FstampeStatistiche: TFstampeStatistiche
  ClientHeight = 583
  ClientWidth = 647
  Caption = 'Stampe / Statistiche'
  OnClose = UniFormClose
  ExplicitWidth = 663
  ExplicitHeight = 622
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 647
    ExplicitWidth = 647
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 647
    ExplicitWidth = 647
    inherited btnConferma: TUniSpeedButton
      Action = actStampaClienti
    end
  end
  object PCStampe: TUniPageControl [2]
    Left = 0
    Top = 69
    Width = 647
    Height = 514
    Hint = ''
    ActivePage = pgClienti
    Align = alClient
    ClientEvents.ExtEvents.Strings = (
      
        'tabPanel.afterrender=function tabPanel.afterrender(sender, eOpts' +
        ')'#13#10'{'#13#10'    var _height = 40;'#13#10'    sender.tabBar.setHeight(_height' +
        ');'#13#10'    sender.tabBar.items.each(function(el) {'#13#10'        el.setH' +
        'eight(_height)'#13#10'    })'#13#10'}')
    TabOrder = 2
    OnChange = PCStampeChange
    ExplicitTop = 74
    object pgClienti: TUniTabSheet
      Hint = ''
      Caption = 'Stampe Clienti'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object cbbOrdClienti: TUniComboBox
        Left = 17
        Top = 40
        Width = 209
        Hint = ''
        Style = csDropDownList
        Text = 'Nome'
        Items.Strings = (
          'Nome'
          'Citt'#224
          'Referente')
        ItemIndex = 0
        TabOrder = 0
        ForceSelection = True
        FieldLabel = 'Ordinamento'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object cbbCollabCliente: TUniDBLookupComboBox
        Left = 25
        Top = 92
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSProdut
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Collaboratore'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbCompagniaCliente: TUniDBLookupComboBox
        Left = 25
        Top = 144
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSCompagnie
        KeyField = 'COD_COMPAGNIA'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Compagnia'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbProfessioneCliente: TUniDBLookupComboBox
        Left = 25
        Top = 196
        Width = 209
        Hint = ''
        ListField = 'DESCRIZ'
        ListSource = dsProfessioni
        KeyField = 'COD_PROFESSIONE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 3
        Color = clWindow
        FieldLabel = 'Professione / attivit'#224
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbGruppoCliente: TUniDBLookupComboBox
        Left = 25
        Top = 248
        Width = 209
        Hint = ''
        Visible = False
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 4
        Color = clWindow
        FieldLabel = 'Gruppo'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbStatoClienti: TUniComboBox
        Left = 336
        Top = 40
        Width = 209
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Potenziali'
          'Attivi'
          'Ex clienti')
        TabOrder = 5
        ForceSelection = True
        ClearButton = True
        FieldLabel = 'Stato clienti'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object cbbTipoClienti: TUniComboBox
        Left = 336
        Top = 92
        Width = 209
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Persone fisiche'
          'Persone giuridiche')
        ParentFont = False
        TabOrder = 6
        ForceSelection = True
        ClearButton = True
        FieldLabel = 'Tipo clienti'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object grpbxIndirizzoContraente: TUniGroupBox
        Left = 336
        Top = 160
        Width = 281
        Height = 113
        Hint = ''
        Caption = 'Filtro sulla citt'#224' / CAP del contraente  '
        TabOrder = 7
        object unedtCittaContraente: TUniEdit
          Left = 24
          Top = 24
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 1
          FieldLabel = 'Citt'#224
          FieldLabelWidth = 80
        end
        object unedtCapContraente: TUniEdit
          Left = 24
          Top = 50
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 2
          FieldLabel = 'Cap'
          FieldLabelWidth = 80
        end
        object unedtProvContraente: TUniEdit
          Left = 24
          Top = 76
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 3
          FieldLabel = 'Provincia'
          FieldLabelWidth = 80
        end
      end
    end
    object pgContratti: TUniTabSheet
      Hint = ''
      Caption = 'Stampe Contratti'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object cbbOrdContratti: TUniComboBox
        Left = 25
        Top = 64
        Width = 209
        Hint = ''
        Style = csDropDownList
        Text = 'Contraente'
        Items.Strings = (
          'Contraente'
          'Polizza'
          'Scadenza'
          '')
        ItemIndex = 0
        TabOrder = 0
        ForceSelection = True
        FieldLabel = 'Ordinamento'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object cbbCollabContratti: TUniDBLookupComboBox
        Left = 25
        Top = 116
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSProdut
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Collaboratore'
        FieldLabelAlign = laTop
        ForceSelection = True
        OnChange = cbbCollabContrattiChange
      end
      object cbbCompContratti: TUniDBLookupComboBox
        Left = 25
        Top = 276
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSCompagnie
        KeyField = 'COD_COMPAGNIA'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Compagnia'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbStatoPolizza: TUniComboBox
        Left = 336
        Top = 64
        Width = 209
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Sospese'
          'Non sospese')
        TabOrder = 5
        ForceSelection = True
        ClearButton = True
        FieldLabel = 'Stato Polizza'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object cbbNuoviContratti: TUniComboBox
        Left = 336
        Top = 116
        Width = 209
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Nuovi'
          'In sostituzione')
        TabOrder = 6
        ForceSelection = True
        ClearButton = True
        FieldLabel = 'Tipo contratti'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object ungrpbxIndContratti: TUniGroupBox
        Left = 320
        Top = 176
        Width = 297
        Height = 113
        Hint = ''
        Caption = 'Filtro sulla citt'#224' / CAP del contraente  '
        TabOrder = 7
        object unedtCittaContratti: TUniEdit
          Left = 24
          Top = 24
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 1
          FieldLabel = 'Citt'#224
          FieldLabelWidth = 80
        end
        object unedtCapContratti: TUniEdit
          Left = 24
          Top = 50
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 2
          FieldLabel = 'Cap'
          FieldLabelWidth = 80
        end
        object unedtProvContratti: TUniEdit
          Left = 24
          Top = 76
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 3
          FieldLabel = 'Provincia'
          FieldLabelWidth = 80
        end
      end
      object cbbRamoPolizza: TUniDBLookupComboBox
        Left = 25
        Top = 329
        Width = 209
        Hint = ''
        ListField = 'DESCRIZIONE'
        ListSource = dsRamoPolizza
        KeyField = 'COD_RAMO'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 3
        Color = clWindow
        FieldLabel = 'Ramo'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object ungrpbxDate: TUniGroupBox
        Left = 320
        Top = 316
        Width = 297
        Height = 133
        Hint = ''
        Caption = ' Filtro su date effetto/scadenza '
        TabOrder = 8
        object DtPkDtEffettoDa: TUniDateTimePicker
          Left = 15
          Top = 24
          Width = 154
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 1
          ClearButton = True
          FieldLabel = 'Effetto'
          FieldLabelWidth = 50
        end
        object DtPkDtEffettoA: TUniDateTimePicker
          Left = 183
          Top = 24
          Width = 99
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 2
          ClearButton = True
          FieldLabelWidth = 50
        end
        object DtPkDtScadenzaDa: TUniDateTimePicker
          Left = 15
          Top = 50
          Width = 154
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 3
          ClearButton = True
          FieldLabel = 'Scadenza'
          FieldLabelWidth = 50
        end
        object DtPkDtScadenzaA: TUniDateTimePicker
          Left = 183
          Top = 50
          Width = 99
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 4
          ClearButton = True
          FieldLabelWidth = 50
        end
        object DtPkDtAnnullA: TUniDateTimePicker
          Left = 183
          Top = 76
          Width = 99
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 5
          ClearButton = True
          FieldLabelWidth = 50
        end
        object DtPkDtAnnullDa: TUniDateTimePicker
          Left = 15
          Top = 76
          Width = 154
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 6
          ClearButton = True
          FieldLabel = 'Annullam.'
          FieldLabelWidth = 50
        end
        object DtincassoDal: TUniDateTimePicker
          Left = 15
          Top = 102
          Width = 154
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 7
          ClearButton = True
          FieldLabel = 'Incassata'
          FieldLabelWidth = 50
        end
        object DtincassoAl: TUniDateTimePicker
          Left = 183
          Top = 102
          Width = 99
          Hint = ''
          DateTime = 43973.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 8
          ClearButton = True
          FieldLabelWidth = 50
        end
      end
      object cbbTipoPolizza: TUniDBLookupComboBox
        Left = 25
        Top = 378
        Width = 209
        Hint = ''
        ListField = 'DESCRIZIONE'
        ListSource = DMdatiAge.DStipoPolizzeAll
        KeyField = 'COD_TIPO_POL'
        ListFieldIndex = 0
        ClearButton = True
        ParentFont = False
        TabOrder = 4
        Color = clWindow
        FieldLabel = 'Tipo polizza'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object rgGrpPolizzeMorte: TUniRadioGroup
        Left = 26
        Top = 6
        Width = 209
        Height = 43
        Hint = ''
        Items.Strings = (
          'Vive'
          'Morte')
        ItemIndex = 0
        Caption = 'Polizze in Vita / Morte'
        TabOrder = 9
        Columns = 2
      end
      object uncbxOggettiAssicurati: TUniCheckBox
        Left = 336
        Top = 31
        Width = 193
        Height = 17
        Hint = ''
        Caption = 'Elenca oggetti in garanzia'
        TabOrder = 10
      end
      object cbbCollabSubContratti: TUniDBLookupComboBox
        Left = 25
        Top = 168
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSsubProdut
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 11
        Color = clWindow
        FieldLabel = 'Sub-Collab.'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbIntermediataDa: TUniDBLookupComboBox
        Left = 26
        Top = 222
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSProdut
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 12
        Color = clWindow
        FieldLabel = 'Intermediata da'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
    end
    object pgTitoli: TUniTabSheet
      Hint = ''
      Caption = 'Stampe Titoli'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object cbbOrdTitoli: TUniComboBox
        Left = 25
        Top = 23
        Width = 209
        Height = 25
        Hint = ''
        Style = csDropDownList
        Text = 'Decorrenza'
        Items.Strings = (
          'Decorrenza'
          'Denominazione'
          'Polizza')
        ItemIndex = 0
        TabOrder = 0
        FieldLabel = 'Ordinamento'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object cbbCollabTitoli: TUniDBLookupComboBox
        Left = 25
        Top = 76
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSProdut
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Collaboratore'
        FieldLabelAlign = laTop
        ForceSelection = True
        OnChange = cbbCollabContrattiChange
      end
      object cbbCompagniaTitoli: TUniDBLookupComboBox
        Left = 26
        Top = 184
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSCompagnie
        KeyField = 'COD_COMPAGNIA'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Compagnia'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbRamoPolTitoli: TUniDBLookupComboBox
        Left = 26
        Top = 237
        Width = 209
        Hint = ''
        ListField = 'DESCRIZIONE'
        ListSource = dsRamoPolizza
        KeyField = 'COD_RAMO'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 3
        Color = clWindow
        FieldLabel = 'Ramo'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbTipoPolTitoli: TUniDBLookupComboBox
        Left = 26
        Top = 286
        Width = 209
        Hint = ''
        ListField = 'DESCRIZIONE'
        ListSource = DMdatiAge.DStipoPolizzeAll
        KeyField = 'COD_TIPO_POL'
        ListFieldIndex = 0
        ClearButton = True
        ParentFont = False
        TabOrder = 4
        Color = clWindow
        FieldLabel = 'Tipo polizza'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object cbbCollabSubTitoli: TUniDBLookupComboBox
        Left = 25
        Top = 128
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSsubProdut
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 6
        Color = clWindow
        FieldLabel = 'Sub-Collab.'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object ungrpbxTitoli: TUniGroupBox
        Left = 328
        Top = 184
        Width = 297
        Height = 113
        Hint = ''
        Caption = 'Filtro sulla citt'#224' / CAP del contraente  '
        TabOrder = 7
        object unedtCittaTitoli: TUniEdit
          Left = 24
          Top = 24
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 1
          FieldLabel = 'Citt'#224
          FieldLabelWidth = 80
        end
        object unedtCapTitoli: TUniEdit
          Left = 24
          Top = 50
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 2
          FieldLabel = 'Cap'
          FieldLabelWidth = 80
        end
        object unedtProvTitoli: TUniEdit
          Left = 24
          Top = 76
          Width = 233
          Hint = ''
          CharCase = ecUpperCase
          Text = ''
          TabOrder = 3
          FieldLabel = 'Provincia'
          FieldLabelWidth = 80
        end
      end
      object cbbStatoPagTitoli: TUniComboBox
        Left = 352
        Top = 23
        Width = 209
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Incassati'
          'Non incassati')
        TabOrder = 5
        EmptyText = 'Tutti'
        ForceSelection = True
        ClearButton = True
        FieldLabel = 'Stato Pagamento'
        FieldLabelAlign = laTop
        IconItems = <>
        OnChange = cbbStatoPagTitoliChange
      end
      object UniGBDateTitoli: TUniGroupBox
        Left = 320
        Top = 91
        Width = 297
        Height = 63
        Hint = ''
        Caption = ' Filtro su data effetto'
        TabOrder = 8
        object dtPkDtEffettoDaTit: TUniDateTimePicker
          Left = 15
          Top = 24
          Width = 154
          Hint = ''
          DateTime = 44109.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 1
          ClearButton = True
          FieldLabel = 'Effetto'
          FieldLabelWidth = 50
        end
        object dtPkDtEffettoATit: TUniDateTimePicker
          Left = 183
          Top = 24
          Width = 99
          Hint = ''
          DateTime = 44109.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 2
          ClearButton = True
          FieldLabelWidth = 50
        end
      end
    end
    object pgIncassi: TUniTabSheet
      Hint = ''
      Caption = 'Statistiche Incassi'
      object cbbCollabIncassi: TUniDBLookupComboBox
        Left = 25
        Top = 68
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSProdut
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Collaboratore'
        FieldLabelAlign = laTop
        ForceSelection = True
        OnChange = cbbCollabContrattiChange
      end
      object cbbCompagniaIncassi: TUniDBLookupComboBox
        Left = 344
        Top = 68
        Width = 209
        Hint = ''
        ListField = 'NOME'
        ListSource = DMdatiAge.DSCompagnie
        KeyField = 'COD_COMPAGNIA'
        ListFieldIndex = 0
        ClearButton = True
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Compagnia'
        FieldLabelAlign = laTop
        ForceSelection = True
      end
      object ungrpbxFiltroData: TUniGroupBox
        Left = 10
        Top = 121
        Width = 297
        Height = 63
        Hint = ''
        Caption = ' Filtro su periodo'
        TabOrder = 3
        object dtPkDataDaInc: TUniDateTimePicker
          Left = 15
          Top = 24
          Width = 154
          Hint = ''
          DateTime = 44109.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 1
          ClearButton = True
          FieldLabel = 'Effetto'
          FieldLabelWidth = 50
        end
        object dtPkDataAInc: TUniDateTimePicker
          Left = 183
          Top = 24
          Width = 99
          Hint = ''
          DateTime = 44109.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 2
          ClearButton = True
          FieldLabelWidth = 50
        end
      end
      object cbbTipoStampaInc: TUniComboBox
        Left = 344
        Top = 145
        Width = 209
        Height = 25
        Hint = ''
        Style = csDropDownList
        Text = 'Totale incassi dal .. al ...'
        Items.Strings = (
          'Totale incassi dal .. al ...'
          'Dettaglio incassi dal .. al ...'
          'Totale polizze per mese'
          'Dettaglio polizze dal .. al ..'
          'Dettaglio incassi divisi per promoter')
        ItemIndex = 0
        TabOrder = 4
        FieldLabel = 'Ordinamento'
        FieldLabelAlign = laTop
        IconItems = <>
        OnChange = cbbTipoStampaIncChange
      end
      object cbxExportToXLS: TUniCheckBox
        Left = 25
        Top = 17
        Width = 137
        Height = 17
        Hint = ''
        Caption = 'Esporta in Excel'
        TabOrder = 0
      end
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 504
    Top = 41
    inherited actConferma: TAction
      Caption = 'Anteprima'
      OnExecute = nil
    end
    inherited actAnnulla: TAction
      Category = ''
      Caption = 'Chiudi'
    end
    object actStampaClienti: TAction
      Category = 'Stampe'
      Caption = 'Anteprima'
      OnExecute = actStampaClientiExecute
    end
    object actStampaContratti: TAction
      Category = 'Stampe'
      Caption = 'Anteprima'
      OnExecute = actStampaContrattiExecute
    end
    object actStampaTitoli: TAction
      Category = 'Stampe'
      Caption = 'Anteprima'
      OnExecute = actStampaTitoliExecute
    end
    object actStampaIncassi: TAction
      Category = 'Stampe'
      Caption = 'Anteprima'
      OnExecute = actStampaIncassiExecute
    end
  end
  inherited dsoEdit: TDataSource
    Left = 352
  end
  inherited untvmglstIcons: TUniNativeImageList
    Left = 404
    Top = 38
  end
  object dsProfessioni: TDataSource
    Left = 300
    Top = 365
  end
  object dsRamoPolizza: TDataSource
    Left = 260
    Top = 309
  end
  object UniScreenMask1: TUniScreenMask
    AttachedControl = btnConferma
    Enabled = True
    DisplayMessage = 'Attendere, generazione report in corso ...'
    Left = 284
    Top = 397
  end
end
