inherited FStampaPolizzaOM: TFStampaPolizzaOM
  Caption = 'Polizza Orsa Maggiore'
  ScreenMask.Target = Owner
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlEditButtons: TUniSimplePanel
    inherited btnConferma: TUniSpeedButton
      ScreenMask.Target = Owner
    end
    inherited btnAnteprimaStampa: TUniSpeedButton
      ScreenMask.Target = Owner
    end
  end
  inherited pnlDatiSpecificiPolizza: TUniPanel
    ExplicitHeight = 364
    inherited pcDatiEreditabili: TUniPageControl
      ActivePage = pgMassimaliGaranzie
      object pgAssicurato: TUniTabSheet
        Hint = ''
        Caption = 'Dati assicurato / Dati Familiari'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 910
        ExplicitHeight = 280
        object pcOggettoAssicurato: TUniPageControl
          Left = 0
          Top = 0
          Width = 902
          Height = 324
          Hint = ''
          ActivePage = pgOggettoAssicurato
          Align = alClient
          TabOrder = 0
          object pgOggettoAssicurato: TUniTabSheet
            Hint = ''
            Caption = 'Assicurato'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 902
            ExplicitHeight = 313
            object undbrdgrpTipoAssicurato: TUniDBRadioGroup
              Left = 12
              Top = 3
              Width = 189
              Height = 39
              Hint = ''
              DataField = 'TipoAssicurato'
              DataSource = dsoEdit
              Caption = 'Tipo Assicurato'
              TabOrder = 0
              Items.Strings = (
                'Patente'
                'Veicolo')
              Columns = 2
              Values.Strings = (
                'P'
                'V')
              OnChangeValue = undbrdgrpTipoAssicuratoChangeValue
            end
            object pnlpncontaier: TUniContainerPanel
              Left = 12
              Top = 43
              Width = 871
              Height = 239
              Hint = ''
              ParentColor = False
              TabOrder = 1
              object pnlVeicolo: TUniPanel
                Left = 0
                Top = 0
                Width = 871
                Height = 239
                Hint = ''
                Align = alClient
                TabOrder = 2
                Title = 'Veicolo'
                Caption = ''
                ExplicitHeight = 236
                object dbcblkupTipoVeicolo: TUniDBLookupComboBox
                  Left = 21
                  Top = 24
                  Width = 270
                  Hint = ''
                  ListField = 'descrizione'
                  ListSource = dsTipoVeicolo
                  KeyField = 'sigla'
                  ListFieldIndex = 0
                  DataField = 'TipoVeicolo'
                  DataSource = dsAssicurato
                  ParentFont = False
                  TabOrder = 1
                  Color = clWindow
                  FieldLabel = 'Tipo veicolo'
                  FieldLabelWidth = 80
                end
                object dbedtMarca: TUniDBEdit
                  Left = 325
                  Top = 24
                  Width = 180
                  Height = 22
                  Hint = ''
                  DataField = 'Marca'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 2
                  FieldLabel = 'Marca'
                  FieldLabelWidth = 80
                end
                object dbedtModello: TUniDBEdit
                  Left = 580
                  Top = 24
                  Width = 242
                  Height = 22
                  Hint = ''
                  DataField = 'Modello'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 3
                  FieldLabel = 'Modello'
                  FieldLabelWidth = 80
                end
                object dbedtTarga: TUniDBEdit
                  Left = 21
                  Top = 66
                  Width = 270
                  Height = 22
                  Hint = ''
                  DataField = 'Targa'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 4
                  FieldLabel = 'Targa'
                  FieldLabelWidth = 80
                end
                object dbedtTelaio: TUniDBEdit
                  Left = 325
                  Top = 66
                  Width = 180
                  Height = 22
                  Hint = ''
                  DataField = 'Telaio'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 5
                  FieldLabel = 'Telaio'
                  FieldLabelWidth = 80
                end
                object dbedtQlHp: TUniDBEdit
                  Left = 580
                  Top = 66
                  Width = 242
                  Height = 22
                  Hint = ''
                  DataField = 'HpQl'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 6
                  FieldLabel = 'Ql/Hp'
                  FieldLabelWidth = 80
                end
                object dtpckScadenzaRevisione: TUniDBDateTimePicker
                  Left = 21
                  Top = 112
                  Width = 270
                  Hint = ''
                  DataField = 'DataScadRevisione'
                  DataSource = dsAssicurato
                  DateTime = 43798.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 7
                  FieldLabel = 'Scadenza revisione'
                  FieldLabelWidth = 80
                end
              end
              object pnlAssicurato: TUniPanel
                Left = 0
                Top = 0
                Width = 871
                Height = 239
                Hint = ''
                Align = alClient
                TabOrder = 1
                Title = 'Patente'
                Caption = ''
                object dbedtDenominazione: TUniDBEdit
                  Left = 24
                  Top = 24
                  Width = 449
                  Height = 22
                  Hint = ''
                  DataField = 'Denominazione'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 1
                  FieldLabel = 'Nome'
                  FieldLabelWidth = 60
                end
                object dbedtCodiceFiscale: TUniDBEdit
                  Left = 497
                  Top = 25
                  Width = 328
                  Height = 22
                  Hint = ''
                  DataField = 'CodFiscIvas'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 2
                  FieldLabel = 'Cod. fiscale'
                  FieldLabelWidth = 60
                end
                object dbedtPatente: TUniDBEdit
                  Left = 24
                  Top = 73
                  Width = 270
                  Height = 22
                  Hint = ''
                  DataField = 'Patente'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 3
                  FieldLabel = 'Patente'
                  FieldLabelWidth = 60
                end
                object dbedtRilasciataDa: TUniDBEdit
                  Left = 497
                  Top = 73
                  Width = 180
                  Height = 22
                  Hint = ''
                  DataField = 'RilasciataDa'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 4
                  FieldLabel = 'Prov. Comune emittente'
                  FieldLabelWidth = 150
                end
                object dtpckDataRilascioPat: TUniDBDateTimePicker
                  Left = 24
                  Top = 129
                  Width = 270
                  Hint = ''
                  DataField = 'DataRilascio'
                  DataSource = dsAssicurato
                  DateTime = 43797.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 5
                  FieldLabel = 'Rilasciata il'
                  FieldLabelWidth = 60
                end
                object dtpckDataScadenzaPat: TUniDBDateTimePicker
                  Left = 497
                  Top = 129
                  Width = 294
                  Hint = ''
                  DataField = 'DataScadenza'
                  DataSource = dsAssicurato
                  DateTime = 43797.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 6
                  FieldLabel = 'Scadenza'
                  FieldLabelWidth = 60
                end
                object cbbCategoriaPat: TUniDBComboBox
                  Left = 328
                  Top = 73
                  Width = 145
                  Hint = ''
                  DataField = 'CategoriaPatente'
                  DataSource = dsAssicurato
                  Items.Strings = (
                    'A'
                    'B'
                    'C'
                    'D')
                  TabOrder = 7
                  FieldLabel = 'Categoria'
                  FieldLabelWidth = 60
                  IconItems = <>
                end
              end
            end
          end
          object pgFamiliari: TUniTabSheet
            Hint = ''
            Caption = 'Familiari'
            object dbedtNome1: TUniDBEdit
              Left = 36
              Top = 52
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome1'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 0
              FieldLabel = 'Nome 1'
              FieldLabelWidth = 50
            end
            object dbedtPatente1: TUniDBEdit
              Left = 364
              Top = 52
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente1'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 1
              FieldLabel = 'Patente 1'
              FieldLabelWidth = 60
            end
            object dbedtCategoria1: TUniDBEdit
              Left = 636
              Top = 52
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente1'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 2
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 1'
              FieldLabelWidth = 70
            end
            object dbedtCategoria2: TUniDBEdit
              Left = 636
              Top = 84
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente2'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 3
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 2'
              FieldLabelWidth = 70
            end
            object dbedtNome2: TUniDBEdit
              Left = 36
              Top = 84
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome2'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 4
              FieldLabel = 'Nome 2'
              FieldLabelWidth = 50
            end
            object dbedtPatente2: TUniDBEdit
              Left = 364
              Top = 84
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente2'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 5
              FieldLabel = 'Patente 2'
              FieldLabelWidth = 60
            end
            object dbedtNome3: TUniDBEdit
              Left = 36
              Top = 121
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome3'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 6
              FieldLabel = 'Nome 3'
              FieldLabelWidth = 50
            end
            object dbedtPatente3: TUniDBEdit
              Left = 364
              Top = 121
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente3'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 7
              FieldLabel = 'Patente 3'
              FieldLabelWidth = 60
            end
            object dbedtCategoria3: TUniDBEdit
              Left = 636
              Top = 121
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente3'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 8
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 3'
              FieldLabelWidth = 70
            end
            object dbedtNome4: TUniDBEdit
              Left = 36
              Top = 156
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome4'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 9
              FieldLabel = 'Nome 4'
              FieldLabelWidth = 50
            end
            object dbedtPatente4: TUniDBEdit
              Left = 364
              Top = 156
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente4'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 10
              FieldLabel = 'Patente 4'
              FieldLabelWidth = 60
            end
            object dbedtCategoria4: TUniDBEdit
              Left = 636
              Top = 156
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente4'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 11
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 4'
              FieldLabelWidth = 70
            end
            object UniLabel1: TUniLabel
              Left = 169
              Top = 14
              Width = 571
              Height = 16
              Hint = ''
              Caption = 
                'Inserire SOLO i dati dei componenti del nucleo famigliare AD ESC' +
                'LUSIONE del CONTRAENTE'
              ParentFont = False
              Font.Color = clRed
              Font.Height = -13
              Font.Style = [fsBold]
              TabOrder = 12
            end
          end
        end
      end
      object pgMassimaliGaranzie: TUniTabSheet
        Hint = ''
        Caption = 'Massimali e Garanzie'
        ParentFont = False
        object cbbMassimali: TUniDBComboBox
          Left = 16
          Top = 24
          Width = 249
          Hint = ''
          DataField = 'SiglaMassimale'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 0
          FieldLabel = 'Massimale per  vertenza'
          FieldLabelWidth = 80
          IconItems = <>
        end
        object ckbxRischioAccessorio: TUniDBCheckBox
          Left = 289
          Top = 26
          Width = 281
          Height = 17
          Hint = ''
          DataField = 'RimborsoSpesePatPunti'
          DataSource = dsoEdit
          Caption = 'RISCHIO ACC. - RIMBORSO SPESE PAT. A PUNTI'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 1
          ParentColor = False
          Color = clBtnFace
          OnClick = ckbxRischioAccessorioClick
        end
        object cbbRimbSpesePatPunti: TUniDBComboBox
          Left = 577
          Top = 24
          Width = 224
          Hint = ''
          Enabled = False
          DataField = 'SiglaRimborsoSpesePatPunti'
          DataSource = dsoEdit
          Items.Strings = (
            '1R=150 + 250'
            '2R=200 + 400'
            '3R=300 + 500'
            '4R=500 + 700'
            '5R=700 + 1000')
          ParentFont = False
          TabOrder = 2
          FieldLabel = 'Rimborso Spese Pat. a punti'
          FieldLabelFont.Style = [fsBold]
          IconItems = <>
        end
        object dbgrpForma: TUniDBRadioGroup
          Left = 807
          Top = 19
          Width = 80
          Height = 42
          Hint = 'A = qualsiasi moto e autovettura, autocarri fino a 35 ql.'
          DataField = 'Forma'
          DataSource = dsoEdit
          Caption = 'Forma'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 3
          Items.Strings = (
            'A'
            'B')
          Columns = 2
          Values.Strings = (
            'A'
            'B')
          OnChangeValue = dbgrpFormaChangeValue
        end
        object ckbxPolizzaIndicizzata: TUniDBCheckBox
          Left = 16
          Top = 71
          Width = 206
          Height = 17
          Hint = ''
          DataField = 'Indicizzata'
          DataSource = dsoEdit
          Caption = 'INDICIZZATA'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 4
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxAppendiceControversie: TUniDBCheckBox
          Left = 289
          Top = 71
          Width = 281
          Height = 17
          Hint = ''
          Visible = False
          DataField = 'AppControversie'
          DataSource = dsoEdit
          Caption = 'APP. CONTROVERSIE CONTRATTUALI'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 5
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxIncMaxDP: TUniDBCheckBox
          Left = 577
          Top = 71
          Width = 202
          Height = 17
          Hint = ''
          Visible = False
          DataSource = dsoEdit
          Caption = 'Incremento max DP +5.000'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 6
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxbProblematicaRCA: TUniDBCheckBox
          Left = 16
          Top = 113
          Width = 249
          Height = 17
          Hint = ''
          DataField = 'ProblematicheRCA'
          DataSource = dsoEdit
          Caption = 'C - EST. PER LA PROBLEMATICA RC AUTO'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 7
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxPerizieParte: TUniDBCheckBox
          Left = 289
          Top = 113
          Width = 249
          Height = 17
          Hint = ''
          DataField = 'PerizieParte'
          DataSource = dsoEdit
          Caption = 'D - EST. PERIZIE DI PARTE ED ARBITRATI'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 8
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxMancatoInterventoRCA: TUniDBCheckBox
          Left = 577
          Top = 113
          Width = 320
          Height = 17
          Hint = ''
          Visible = False
          DataField = 'MancatoIntRCA'
          DataSource = dsoEdit
          Caption = 'APP. MANCATO INTERV. IN SEDE PENALE RCAUTO'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 9
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxProtezioneFamiglia: TUniDBCheckBox
          Left = 16
          Top = 155
          Width = 175
          Height = 17
          Hint = ''
          DataField = 'ProtezioneFamiglia'
          DataSource = dsoEdit
          Caption = 'E - PROTEZIONE FAMIGLIA'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 10
          ParentColor = False
          Color = clBtnFace
          OnClick = ckbxProtezioneFamigliaClick
        end
        object ckbxEstEPatenti: TUniDBCheckBox
          Left = 289
          Top = 155
          Width = 46
          Height = 18
          Hint = ''
          DataField = 'EstPatente'
          DataSource = dsoEdit
          Caption = 'F - '
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 11
          ParentColor = False
          Color = clBtnFace
          OnClick = ckbxEstEPatentiClick
        end
        object unmEstEPatente: TUniMemo
          Left = 325
          Top = 148
          Width = 555
          Height = 30
          Hint = ''
          Lines.Strings = (
            
              'Taxi - Ambulanze - Targhe prova - Carri Attrezzi - Autonoleggi c' +
              'on o senza autista - Rimorchi - Autoscuola'
            
              'Mezzi speciali - Patenti minist. - Patente B96 - Estensione E al' +
              'le patenti cat. B,C1,C,D1 o D'
            '')
          ParentFont = False
          ReadOnly = True
          TabOrder = 12
        end
        object dbedtNumeroAssicurati: TUniDBNumberEdit
          Left = 214
          Top = 152
          Width = 51
          Height = 22
          Hint = ''
          Enabled = False
          DataField = 'NumeroAssicurati'
          DataSource = dsoEdit
          TabOrder = 13
          MaxValue = 4.000000000000000000
          ShowTrigger = True
          DecimalSeparator = ','
          OnChange = dbedtNumeroAssicuratiChange
        end
      end
    end
  end
  inherited pnlDatiFissi: TUniPanel
    inherited dbedtSostPol: TUniDBEdit
      TabOrder = 16
    end
    inherited dtpckScadSostPol: TUniDBDateTimePicker
      TabOrder = 20
    end
    inherited dtpckDataEffetto: TUniDBDateTimePicker
      TabOrder = 21
    end
    inherited dtpckDataScadenza: TUniDBDateTimePicker
      TabOrder = 10
    end
    inherited cbbFrazionamento: TUniDBComboBox
      TabOrder = 11
    end
    inherited dbedtDurataAnni: TUniDBEdit
      TabOrder = 12
    end
    inherited dbedtDurataMesi: TUniDBEdit
      TabOrder = 13
    end
    inherited dbedtRateoGG: TUniDBEdit
      TabOrder = 14
    end
    inherited ckbxRatino: TUniDBCheckBox
      TabOrder = 15
    end
    inherited dtpckScad1Rata: TUniDBDateTimePicker
      TabOrder = 17
    end
    inherited dtpckDataEmissione: TUniDBDateTimePicker
      TabOrder = 18
    end
    inherited ckbxTacitoRinnovo: TUniDBCheckBox
      TabOrder = 22
    end
    inherited btnPolizzaSostituita: TUniMenuButton
      TabOrder = 19
    end
  end
  inherited pnlPremiPolizza: TUniPanel
    inherited dbedtAccessori2: TUniDBNumberEdit
      ParentFont = False
    end
  end
  object dsTipoVeicolo: TDataSource
    DataSet = DMdatiAge.Qtipo_veicolo
    Left = 385
    Top = 280
  end
  object UniScreenMask: TUniScreenMask
    AttachedControl = btnAnteprimaStampa
    Enabled = True
    DisplayMessage = 'Attendere, generazione anteprima in corso ...'
    TargetControl = Owner
    Left = 693
    Top = 320
  end
end
