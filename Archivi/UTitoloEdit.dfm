inherited FTitoloEdit: TFTitoloEdit
  ClientHeight = 316
  ClientWidth = 593
  Caption = 'Editazione dati Titolo'
  OnClose = UniFormClose
  ExplicitWidth = 609
  ExplicitHeight = 355
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 593
    ExplicitWidth = 593
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 593
    ExplicitWidth = 593
  end
  object pnlEdit: TUniContainerPanel [2]
    Left = 0
    Top = 69
    Width = 593
    Height = 247
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    ExplicitTop = 74
    object cbbTipoTitolo: TUniDBLookupComboBox
      Left = 24
      Top = 24
      Width = 249
      Hint = ''
      ListField = 'DESCRIZIONE'
      ListSource = DMdatiAgeTitoli.DSTipoTitolo
      KeyField = 'SIGLA'
      ListFieldIndex = 0
      DataField = 'TIPO_TITOLO'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 1
      Color = clWindow
      FieldLabel = 'Tipo titolo'
      OnChange = cbbTipoTitoloChange
    end
    object dbedtNPolizza: TUniDBNumberEdit
      Left = 24
      Top = 52
      Width = 249
      Height = 22
      Hint = ''
      DataField = 'N_POLIZZA'
      DataSource = dsoEdit
      TabOrder = 2
      ReadOnly = True
      FieldLabel = 'N. polizza'
      DecimalSeparator = ','
    end
    object dbedtPercTasse: TUniDBNumberEdit
      Left = 24
      Top = 136
      Width = 249
      Height = 22
      Hint = ''
      DataField = 'PTASSE'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 7
      FieldLabel = 'Percentuale tasse'
      DecimalSeparator = ','
    end
    object dbedtImponibile: TUniDBNumberEdit
      Left = 336
      Top = 136
      Width = 233
      Height = 22
      Hint = ''
      DataField = 'NETTO'
      DataSource = dsoEdit
      TabOrder = 8
      FieldLabel = 'Imponibile'
      DecimalSeparator = ','
    end
    object dbedtPremioLordo: TUniDBNumberEdit
      Left = 24
      Top = 164
      Width = 249
      Height = 22
      Hint = ''
      DataField = 'LORDO'
      DataSource = dsoEdit
      TabOrder = 9
      FieldLabel = 'Premio lordo'
      DecimalSeparator = ','
      OnChange = dbedtPremioLordoChange
    end
    object dbedtProvvigioni: TUniDBNumberEdit
      Left = 336
      Top = 164
      Width = 233
      Height = 22
      Hint = ''
      DataField = 'PROVVIGIONI'
      DataSource = dsoEdit
      TabOrder = 10
      FieldLabel = 'Provvigioni'
      DecimalSeparator = ','
    end
    object dbedtImposte: TUniDBNumberEdit
      Left = 24
      Top = 192
      Width = 249
      Height = 22
      Hint = ''
      DataField = 'TASSE'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 11
      FieldLabel = 'Imposte'
      DecimalSeparator = ','
    end
    object dbedtProvvigioniSub: TUniDBNumberEdit
      Left = 336
      Top = 192
      Width = 233
      Height = 22
      Hint = ''
      DataField = 'provvigioni_sub'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 12
      ReadOnly = True
      FieldLabel = 'Prov. sub.'
      DecimalSeparator = ','
    end
    object btnSelezionaPolizza: TUniBitBtn
      Left = 277
      Top = 51
      Width = 25
      Height = 25
      Action = actSelPolizza
      ParentFont = False
      TabOrder = 5
      IconAlign = iaCenter
    end
    object dbedtContraente: TUniDBEdit
      Left = 24
      Top = 80
      Width = 545
      Height = 22
      Hint = ''
      DataField = 'DENOMINAZ'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 3
      ReadOnly = True
    end
    object dbedtFrazionamento: TUniDBEdit
      Left = 336
      Top = 108
      Width = 233
      Height = 22
      Hint = ''
      DataField = 'DESFRAZIONAMENTO'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 6
      ReadOnly = True
    end
    object dbedtScadenza: TUniDBDateTimePicker
      Left = 24
      Top = 108
      Width = 249
      Hint = ''
      DataField = 'rata'
      DataSource = dsoEdit
      DateTime = 43926.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 4
      FieldLabel = 'Scadenza'
      OnChange = dbedtScadenzaChange
    end
    object DBLcompagnie: TUniDBLookupComboBox
      Left = 320
      Top = 24
      Width = 249
      Hint = ''
      ListField = 'NOME'
      ListSource = DMdatiAgeTitoli.DScompagnie
      KeyField = 'COD_COMPAGNIA'
      ListFieldIndex = 0
      DataField = 'RIF_COD_COMPAGNIA'
      DataSource = dsoEdit
      ParentFont = False
      TabOrder = 13
      Color = clWindow
      FieldLabel = 'Compagnia:'
      OnChange = cbbTipoTitoloChange
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 528
    object actSelPolizza: TAction
      Category = 'Polizza'
      OnExecute = actSelPolizzaExecute
    end
  end
  inherited dsoEdit: TDataSource
    Left = 432
  end
end
