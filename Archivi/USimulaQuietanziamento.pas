unit USimulaQuietanziamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniDateTimePicker,
  uniGUIBaseClasses, uniPanel, UMasterEdit, UDMdatiAgeTitoli, uniBasicGrid, uniDBGrid, uniImageList, Data.DB,
  System.Actions, Vcl.ActnList, uniButton, uniBitBtn, uniSpeedButton, uniLabel, uniScreenMask;

type
  TFSimulaQuietanziamento = class(TFMasterEdit)
    pnlTop: TUniSimplePanel;
    dtPckDataQuietanziamento: TUniDateTimePicker;
    cbbMandante: TUniDBLookupComboBox;
    grdTempQuietanze: TUniDBGrid;
    actSimulaQuietanziamento: TAction;
    actCalcolaQuietanziamento: TAction;
    actEsportaInExcel: TAction;
    actStampaQuietanziamento: TAction;
    btnStampaQuietanziamento: TUniSpeedButton;
    btnEsportaToExcel: TUniSpeedButton;
    actEsci: TAction;
    ScreenMask: TUniScreenMask;
    procedure UniFormCreate(Sender: TObject);
    procedure btnAnnullaClick(Sender: TObject);
    procedure actEsciExecute(Sender: TObject);
    procedure actSimulaQuietanziamentoExecute(Sender: TObject);
    procedure actCalcolaQuietanziamentoExecute(Sender: TObject);
    procedure actEsportaInExcelExecute(Sender: TObject);
    procedure dsoEditDataChange(Sender: TObject; Field: TField);
    procedure dtPckDataQuietanziamentoChange(Sender: TObject);
    procedure actStampaQuietanziamentoExecute(Sender: TObject);
    procedure grdTempQuietanzeColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
  private
    FTipoQuietanziamento: TEnTipoQuietanziamento;
    FTipoCompagniaQuietanziamento: TEnTipoCopagniaQuietanzamento;
    procedure setTipoQuietanziamento(const Value: TEnTipoQuietanziamento);
    procedure setTipoCompagniaQuietanziamento(const Value: TEnTipoCopagniaQuietanzamento);
    { Private declarations }
  public
    { Public declarations }
    property TipoQuietanziamento: TEnTipoQuietanziamento write setTipoQuietanziamento;
    property TipoCompagniaQuietanziamento: TEnTipoCopagniaQuietanzamento write setTipoCompagniaQuietanziamento;
  end;

implementation

uses
  uniGUIApplication, UdmdatiAge, MainModule, FireDAC.Comp.Client;

{$R *.dfm}
{ TFSimulaQuietanziamento }

procedure TFSimulaQuietanziamento.actCalcolaQuietanziamentoExecute(Sender: TObject);
begin
  actCalcolaQuietanziamento.Enabled := False;
  ScreenMask.Enabled                := True;
  try
    UniMainModule.DMdatiAgeTitoli.CalcolaQuietanziamento(FTipoCompagniaQuietanziamento, FTipoQuietanziamento,
      dtPckDataQuietanziamento.DateTime, cbbMandante.KeyValue);
  finally
    ScreenMask.Enabled := False;
  end;

end;

procedure TFSimulaQuietanziamento.actEsciExecute(Sender: TObject);
begin
  Close;
end;

procedure TFSimulaQuietanziamento.actEsportaInExcelExecute(Sender: TObject);
begin
  actEsportaInExcel.Enabled := False;
  try
    UniMainModule.DMdatiAgeTitoli.ExportToExcelAndSend('Quietanziamenti');
  finally
    actEsportaInExcel.Enabled := False;

  end;
end;

procedure TFSimulaQuietanziamento.actSimulaQuietanziamentoExecute(Sender: TObject);
begin
  actStampaQuietanziamento.Enabled := False;
  ScreenMask.Enabled               := True;
  try
    UniMainModule.DMdatiAgeTitoli.CalcolaQuietanziamento(FTipoCompagniaQuietanziamento, FTipoQuietanziamento,
      dtPckDataQuietanziamento.DateTime);
  finally
    actStampaQuietanziamento.Enabled := True;
    btnConferma.Enabled              := True;
    ScreenMask.Enabled               := False;
  end;
end;

procedure TFSimulaQuietanziamento.actStampaQuietanziamentoExecute(Sender: TObject);
begin
  UniMainModule.DMDatiReport.ShowReportQuietanziamento;
end;

procedure TFSimulaQuietanziamento.btnAnnullaClick(Sender: TObject);
begin
  Close;
end;

procedure TFSimulaQuietanziamento.dsoEditDataChange(Sender: TObject; Field: TField);
begin
  actStampaQuietanziamento.Enabled := not dsoEdit.DataSet.IsEmpty;
  actEsportaInExcel.Enabled        := not dsoEdit.DataSet.IsEmpty;
end;

procedure TFSimulaQuietanziamento.dtPckDataQuietanziamentoChange(Sender: TObject);
begin
  actSimulaQuietanziamento.Enabled := dtPckDataQuietanziamento.DateTime > 0;
end;

procedure TFSimulaQuietanziamento.grdTempQuietanzeColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
var
  EditDataSet: TFDMemTable;
begin
  EditDataSet               := dsoEdit.DataSet as TFDMemTable;
  EditDataSet.IndexesActive := True;
  if Column.FieldName = 'n_polizza' then
    EditDataSet.IndexName := 'IxPolizza'
  else
    if Column.FieldName = 'denominaz' then
      EditDataSet.IndexName := 'IxContraente'
    else
      EditDataSet.IndexName := 'IxScadenza';

end;

procedure TFSimulaQuietanziamento.setTipoCompagniaQuietanziamento(const Value: TEnTipoCopagniaQuietanzamento);
begin
  FTipoCompagniaQuietanziamento := Value;
  case FTipoCompagniaQuietanziamento of
    tcSLP:
      begin
        cbbMandante.KeyValue := 1;
        cbbMandante.ReadOnly := True;
      end;
    tcNotSLP:
      begin
        cbbMandante.KeyValue    := cbbMandante.ListSource.DataSet.FieldByName('COD_COMPAGNIA').AsInteger;
        cbbMandante.ClearButton := False;
      end;

  else
    ;

  end;
end;

procedure TFSimulaQuietanziamento.setTipoQuietanziamento(const Value: TEnTipoQuietanziamento);
begin
  FTipoQuietanziamento := Value;
  case FTipoQuietanziamento of
    tqSimulazione:
      begin
        btnConferma.Action   := actSimulaQuietanziamento;
        cbbMandante.ReadOnly := false;
      end
  else
    begin
      btnConferma.Action               := actCalcolaQuietanziamento;
      actStampaQuietanziamento.Enabled := False;
      actEsportaInExcel.Enabled        := False;
    end;
  end;
end;

procedure TFSimulaQuietanziamento.UniFormCreate(Sender: TObject);
begin
  inherited;
  cbbMandante.ListSource.DataSet.Open;
  btnAnnulla.Action               := actStampaQuietanziamento;
  btnStampaQuietanziamento.Action := actEsportaInExcel;
  btnEsportaToExcel.Action        := actEsci;
end;

end.
