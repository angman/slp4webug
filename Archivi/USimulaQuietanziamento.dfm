inherited FSimulaQuietanziamento: TFSimulaQuietanziamento
  ClientHeight = 436
  ClientWidth = 899
  Caption = 'FSimulaQuietanziamento'
  BorderIcons = []
  ExplicitWidth = 915
  ExplicitHeight = 475
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 899
    TabOrder = 2
    ExplicitWidth = 899
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 899
    ExplicitWidth = 899
    inherited btnConferma: TUniSpeedButton
      Width = 145
      Action = actSimulaQuietanziamento
      ExplicitWidth = 145
    end
    object btnStampaQuietanziamento: TUniSpeedButton [1]
      Left = 165
      Top = 0
      Width = 145
      Height = 43
      Constraints.MaxWidth = 150
      Action = actStampaQuietanziamento
      ParentFont = False
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 3
    end
    object btnEsportaToExcel: TUniSpeedButton [2]
      Left = 309
      Top = 0
      Width = 145
      Height = 43
      Constraints.MaxWidth = 150
      Action = actEsportaInExcel
      ParentFont = False
      Font.Color = clRed
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 4
    end
    inherited btnAnnulla: TUniSpeedButton
      Left = 453
      Top = 0
      Width = 145
      Action = actEsci
      Font.Color = clWindowText
      Font.Style = []
      ExplicitLeft = 453
      ExplicitTop = 0
      ExplicitWidth = 145
    end
  end
  object pnlTop: TUniSimplePanel [2]
    Left = 0
    Top = 69
    Width = 899
    Height = 49
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    ExplicitLeft = 8
    ExplicitTop = 152
    ExplicitWidth = 612
    object dtPckDataQuietanziamento: TUniDateTimePicker
      Left = 24
      Top = 16
      Width = 241
      Hint = ''
      DateTime = 44170.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 1
      FieldLabel = 'Mese e anno quietanziamento'
      FieldLabelWidth = 150
      OnChange = dtPckDataQuietanziamentoChange
    end
    object cbbMandante: TUniDBLookupComboBox
      Left = 271
      Top = 16
      Width = 257
      Hint = ''
      ListField = 'NOME'
      ListSource = DMdatiAge.DSCompagnie
      KeyField = 'COD_COMPAGNIA'
      ListFieldIndex = 0
      ClearButton = True
      TabOrder = 2
      Color = clWindow
      FieldLabel = 'Mandante'
      FieldLabelWidth = 80
    end
  end
  object grdTempQuietanze: TUniDBGrid [3]
    Left = 0
    Top = 118
    Width = 899
    Height = 318
    Hint = ''
    DataSource = dsoEdit
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgAutoRefreshRow, dgRowNumbers]
    ReadOnly = True
    LoadMask.Message = 'Loading data...'
    Align = alClient
    TabOrder = 3
    OnColumnSort = grdTempQuietanzeColumnSort
    Columns = <
      item
        FieldName = 'n_polizza'
        Title.Caption = 'N'#176' polizza'
        Width = 94
        Sortable = True
      end
      item
        FieldName = 'denominaz'
        Title.Caption = 'Contraente'
        Width = 250
        Sortable = True
      end
      item
        FieldName = 'decorrenza'
        Title.Caption = 'Decorrenza'
        Width = 64
      end
      item
        FieldName = 'scadenza'
        Title.Caption = 'Scadenza'
        Width = 64
        Sortable = True
      end
      item
        FieldName = 'lordo'
        Title.Caption = 'Lordo'
        Width = 64
      end
      item
        FieldName = 'provvigioni'
        Title.Caption = 'Provvig'
        Width = 64
      end
      item
        FieldName = 'provvigioni_sub'
        Title.Caption = 'Prov.Sub'
        Width = 64
      end
      item
        FieldName = 'frazionam'
        Title.Caption = 'Fraz.'
        Width = 29
      end
      item
        FieldName = 'scadenza_annuale'
        Title.Caption = 'Scad.Ann'
        Width = 50
      end
      item
        FieldName = 'prod_sigla'
        Title.Caption = 'Sigla'
        Width = 26
      end
      item
        FieldName = 'compagnia'
        Title.Caption = 'Compagnia'
        Width = 154
      end
      item
        FieldName = 'ramo'
        Title.Caption = 'Ramo'
        Width = 154
      end
      item
        FieldName = 'oggetto'
        Title.Caption = 'In garanzia'
        Width = 244
      end
      item
        FieldName = 'prod_nome'
        Title.Caption = 'Nome produttore'
        Width = 124
      end
      item
        FieldName = 'telefono'
        Title.Caption = 'Telefono'
        Width = 184
      end>
  end
  inherited actlstEditOperation: TActionList
    inherited actAnnulla: TAction
      Caption = 'Esci'
    end
    object actSimulaQuietanziamento: TAction
      Category = 'EditOperation'
      Caption = 'Simula Quietanziamento'
      OnExecute = actSimulaQuietanziamentoExecute
    end
    object actCalcolaQuietanziamento: TAction
      Category = 'EditOperation'
      Caption = 'Calcola Quietanziamento'
      OnExecute = actCalcolaQuietanziamentoExecute
    end
    object actEsportaInExcel: TAction
      Category = 'EditOperation'
      Caption = 'Esporta In Excel'
      Enabled = False
      OnExecute = actEsportaInExcelExecute
    end
    object actStampaQuietanziamento: TAction
      Category = 'EditOperation'
      Caption = 'Stampa elenco quietanze'
      Enabled = False
      OnExecute = actStampaQuietanziamentoExecute
    end
    object actEsci: TAction
      Category = 'EditOperation'
      Caption = 'Esci'
      OnExecute = actEsciExecute
    end
  end
  inherited dsoEdit: TDataSource
    DataSet = DMdatiAgeTitoli.fdmtblScadenzeTemp
    OnDataChange = dsoEditDataChange
  end
  inherited untvmglstIcons: TUniNativeImageList
    Left = 740
    Top = 142
  end
  object ScreenMask: TUniScreenMask
    AttachedControl = btnConferma
    Enabled = False
    DisplayMessage = 'Attendere,  calcolo quietanziamento in corso ..'
    Left = 468
    Top = 357
  end
end
