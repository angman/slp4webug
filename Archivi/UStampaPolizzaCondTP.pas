unit UStampaPolizzaCondTP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, Vcl.Menus, uniMainMenu, uniGUIBaseClasses, uniImageList, Data.DB,
  System.Actions, Vcl.ActnList, uniButton, uniRadioGroup, uniDBRadioGroup, uniMenuButton, uniCheckBox, uniDBCheckBox,
  uniDBComboBox, uniMultiItem, uniComboBox, uniDBLookupComboBox, uniDateTimePicker, uniDBDateTimePicker, uniDBEdit,
  uniBitBtn, uniEdit, uniPageControl, uniPanel, uniSpeedButton, uniLabel, uniGroupBox;

type
  TFStampaPolizzaCondTP = class(TFStampaBasePolizza)
    pgAmministratore: TUniTabSheet;
    dbedtAmministratore: TUniDBEdit;
    dbedtIndirizzoStudio: TUniDBEdit;
    dbedtCapSudio: TUniDBEdit;
    dbedtCittaStudio: TUniDBEdit;
    dbedtProvincia1: TUniDBEdit;
    pgMassimali: TUniTabSheet;
    cbbMassimali: TUniDBComboBox;
    dbedtNumeroUnitaImmobiliari: TUniDBNumberEdit;
    dbedtNumeroBox: TUniDBNumberEdit;
    grpbxMassimaleBase: TUniGroupBox;
    grpbxEstensioni: TUniGroupBox;
    dbedtNPolizza: TUniDBEdit;
    dbedtCompagniaEmittente: TUniDBEdit;
    dbedtIndirizzoGar: TUniDBEdit;
    dtpckDecorrenza: TUniDBDateTimePicker;
    dtpckScadenza: TUniDBDateTimePicker;
    dbedtPremioAnnuoLordo: TUniDBNumberEdit;
    dbedtPremioNettoSLP: TUniDBNumberEdit;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    dsReferente: TDataSource;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); override;

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;

  end;

function FStampaPolizzaCondTP: TFStampaPolizzaCondTP;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiTempPolizzaCondTP, UDMdatiAgeClienti;

function FStampaPolizzaCondTP: TFStampaPolizzaCondTP;
begin
  Result := TFStampaPolizzaCondTP(UniMainModule.GetFormInstance(TFStampaPolizzaCondTP));
end;

procedure TFStampaPolizzaCondTP.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited;
  dsAssicurato.DataSet.DisableControls;
  try
    dsAssicurato.DataSet.First;
    dsAssicurato.DataSet.Next;
  finally
    dsAssicurato.DataSet.EnableControls;
  end;
end;

procedure TFStampaPolizzaCondTP.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited;
  UniMainModule.DMDatiTempPolizza.SetDatiAssicuratoDaContraente(true);
end;

procedure TFStampaPolizzaCondTP.UniFormCreate(Sender: TObject);
begin
  inherited;
  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE');

  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  TDMDatiTempPolizzaCondTP(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.asstring :=
    Copy(cbbMassimali.Text, 1, 3);
  pcDatiEreditabili.ActivePage := pgContraente;
end;

initialization

RegisterClass(TFStampaPolizzaCondTP);

end.
