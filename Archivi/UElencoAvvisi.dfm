object FElencoAvvisi: TFElencoAvvisi
  Left = 0
  Top = 0
  ClientHeight = 332
  ClientWidth = 555
  Caption = 'Avvisi dalla Direzione'
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  Font.Height = -13
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object pnlButtons: TUniSimplePanel
    Left = 0
    Top = 0
    Width = 555
    Height = 35
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    object btnEsci: TUniSpeedButton
      Left = 133
      Top = 3
      Width = 106
      Height = 29
      Hint = ''
      Caption = 'Esci'
      ParentFont = False
      Font.Height = -13
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 1
      OnClick = btnEsciClick
    end
    object btnVisualizzaMessaggio: TUniSpeedButton
      Left = 13
      Top = 3
      Width = 124
      Height = 29
      Action = actVisAvviso
      ParentFont = False
      Font.Height = -13
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 2
    end
  end
  object grdElencoAvvisi: TUniDBGrid
    Left = 0
    Top = 35
    Width = 555
    Height = 297
    Hint = ''
    DataSource = dsAvvisiAge
    ReadOnly = True
    LoadMask.Message = 'Loading data...'
    Align = alClient
    Font.Height = -15
    ParentFont = False
    TabOrder = 1
    OnDblClick = grdElencoAvvisiDblClick
    Columns = <
      item
        FieldName = 'DESCRIZIONE'
        Title.Caption = 'Descrizione'
        Width = 285
      end
      item
        FieldName = 'dt_operativita'
        Title.Caption = 'dt operativit'#224
        Width = 70
      end>
  end
  object dsAvvisiAge: TDataSource
    Left = 416
    Top = 3
  end
  object actlstVisAvviso: TActionList
    Left = 496
    Top = 24
    object actVisAvviso: TAction
      Caption = 'Visualizza avviso'
      OnExecute = actVisAvvisoExecute
    end
  end
end
