unit USceltaTipoPolizza;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, uniPanel, uniMemo, uniDBMemo, uniSplitter,
  uniBasicGrid, uniDBGrid, UStampaBasePolizza, uniImageList;

type
  TFStampaBasePolizzaClass = class of TFStampaBasePolizza;

  TFSceltaTipoPolizza = class(TFMasterEdit)
    pnlContent: TUniPanel;
    grdTipiPolizza: TUniDBGrid;
    unspltrGrid: TUniSplitter;
    undbmNote: TUniDBMemo;
    procedure actConfermaExecute(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure grdTipiPolizzaSelectionChange(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure actAnnullaExecute(Sender: TObject);
  private
    { Private declarations }
    FSiglaPolizza: string;
    FFormPolizzaClassName: string;

    procedure DoShowPolizza(Sender: TComponent; Res: Integer);
  public
    { Public declarations }
  end;

function FSceltaTipoPolizza: TFSceltaTipoPolizza;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, libreria;

function FSceltaTipoPolizza: TFSceltaTipoPolizza;
begin
  Result := TFSceltaTipoPolizza(UniMainModule.GetFormInstance(TFSceltaTipoPolizza));
end;

procedure TFSceltaTipoPolizza.actAnnullaExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFSceltaTipoPolizza.actConfermaExecute(Sender: TObject);
var
  StampaBasePolizzaClass: TFStampaBasePolizzaClass;
begin
  if FSiglaPolizza = '' then
    ShowMessage('Non � stato selezionato nessun dato')
  else
    try
      // LOG('prima di stacca numero polizza linea 62');
      UniMainModule.DMdatiAge.staccaNumeroPolizza(FSiglaPolizza, false);
      // LOG('dopo stacca numero polizza linea 64');
      DoShowPolizza(nil, 0);
    except
      on E: Exception do
      begin
        ShowMessage(E.Message, DoShowPolizza);
      end;
    end;

end;

procedure TFSceltaTipoPolizza.DoShowPolizza(Sender: TComponent; Res: Integer);
var
  StampaBasePolizzaClass: TFStampaBasePolizzaClass;

begin
  // LOG('sceltapolizza linea 78');
  StampaBasePolizzaClass := TFStampaBasePolizzaClass(FindClass(FFormPolizzaClassName));
  if Assigned(StampaBasePolizzaClass) then
    with StampaBasePolizzaClass.Create(uniApplication) do
    begin
      ShowModal;
    end;
end;

procedure TFSceltaTipoPolizza.grdTipiPolizzaSelectionChange(Sender: TObject);
begin
  FSiglaPolizza              := UniMainModule.DMdatiAge.QTipoPolizze.FieldByName('SIGLA').AsString;
  UniMainModule.TipoPolizza  := UniMainModule.DMdatiAge.QTipoPolizze.FieldByName('COD_TIPO_POL').AsInteger;
  UniMainModule.SiglaPolizza := FSiglaPolizza;

  FFormPolizzaClassName := UniMainModule.DMdatiAge.QTipoPolizze.FieldByName('FORM_CLASS_NAME').AsString;
  btnConferma.Enabled   := FFormPolizzaClassName <> '';
  // if UniMainModule.DMdatiAge.QTipoPolizze.FieldByName('DMTempPolizzaClassName').AsString <> '' then
  UniMainModule.DMTempPolizzaClassName := UniMainModule.DMdatiAge.QTipoPolizze.FieldByName
    ('DMTempPolizzaClassName').AsString;
  // if UniMainModule.DMdatiAge.QTipoPolizze.FieldByName('DMBasePolizzaClassName').AsString <> '' then
  UniMainModule.DMBasePolizzaClassName := UniMainModule.DMdatiAge.QTipoPolizze.FieldByName
    ('DMBasePolizzaClassName').AsString;
  // LOG(FFormPolizzaClassName + ' - ' + UniMainModule.DMTempPolizzaClassName + ' - ' + UniMainModule.DMBasePolizzaClassName);
end;

procedure TFSceltaTipoPolizza.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  dsoEdit.DataSet.Close;
end;

procedure TFSceltaTipoPolizza.UniFormCreate(Sender: TObject);
begin
  inherited;
  dsoEdit.DataSet := UniMainModule.DMdatiAge.QTipoPolizze;
  dsoEdit.DataSet.Open;
end;

end.
