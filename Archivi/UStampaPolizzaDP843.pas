unit UStampaPolizzaDP843;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, libSLP,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, uniGUIBaseClasses, uniImageList, Data.DB,
  System.Actions, Vcl.ActnList, uniRadioGroup, uniDBRadioGroup, uniCheckBox, uniDBCheckBox,
  uniMultiItem, uniComboBox, uniDBComboBox, uniEdit, uniDateTimePicker, uniDBDateTimePicker,
  uniDBEdit, uniBitBtn, uniPageControl, uniPanel, uniButton, uniSpeedButton, uniLabel, uniMemo,
  uniListBox, uniGroupBox, uniScreenMask, uniDBLookupComboBox, Vcl.Menus,
  uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaDP843 = class(TFStampaBasePolizza)
    pgAssicurato: TUniTabSheet;
    pcOggettoAssicurato: TUniPageControl;
    pgOggettoAssicurato: TUniTabSheet;
    pnlpncontaier: TUniContainerPanel;
    pnlAssicurato: TUniPanel;
    dbedtDenominazione: TUniDBEdit;
    dbedtCodiceFiscale: TUniDBEdit;
    dbedtPatente: TUniDBEdit;
    dbedtRilasciataDa: TUniDBEdit;
    dtpckDataRilascioPat: TUniDBDateTimePicker;
    dtpckDataScadenzaPat: TUniDBDateTimePicker;
    cbbCategoriaPat: TUniDBComboBox;
    pgFamiliari: TUniTabSheet;
    dbedtNome1: TUniDBEdit;
    dbedtPatente1: TUniDBEdit;
    dbedtCategoria1: TUniDBEdit;
    dbedtCategoria2: TUniDBEdit;
    dbedtNome2: TUniDBEdit;
    dbedtPatente2: TUniDBEdit;
    dbedtNome3: TUniDBEdit;
    dbedtPatente3: TUniDBEdit;
    dbedtCategoria3: TUniDBEdit;
    dbedtNome4: TUniDBEdit;
    dbedtPatente4: TUniDBEdit;
    dbedtCategoria4: TUniDBEdit;
    pgMassimaliGaranzie: TUniTabSheet;
    cbbMassimali: TUniDBComboBox;
    cbbRimbSpesePatPunti: TUniDBComboBox;
    dbgrpForma: TUniDBRadioGroup;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    ckbxDifPenVitaPrivata: TUniDBCheckBox;
    UniDBNumberEdit1: TUniDBNumberEdit;
    UniDBNumberEdit2: TUniDBNumberEdit;
    UniDBNumberEdit3: TUniDBNumberEdit;
    UniDBNumberEdit4: TUniDBNumberEdit;
    UniDBNumberEdit5: TUniDBNumberEdit;
    UniDBNumberEdit6: TUniDBNumberEdit;
    UniDBNumberEdit7: TUniDBNumberEdit;
    UniDBNumberEdit8: TUniDBNumberEdit;
    UniDBNumberEdit9: TUniDBNumberEdit;
    UniDBNumberEdit10: TUniDBNumberEdit;
    UniDBNumberEdit11: TUniDBNumberEdit;
    UniDBNumberEdit12: TUniDBNumberEdit;
    UniDBNumberEdit13: TUniDBNumberEdit;
    UniScreenMask1: TUniScreenMask;
    UniDBNumberEdit14: TUniDBNumberEdit;
    UniLabel1: TUniLabel;
    UniDBCheckBox1: TUniDBCheckBox;
    procedure UniFormCreate(Sender: TObject);
    procedure ckbxProtezioneFamigliaClick(Sender: TObject);
    procedure dbgrpFormaChangeValue(Sender: TObject);
    procedure ckbxDifPenVitaPrivataClick(Sender: TObject);
  private
    { Private declarations }
    procedure RiallineaMaxGaranzie;
  protected
    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); override;

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;

  end;

function FStampaPolizzaDP843: TFStampaPolizzaDP843;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiTempPolizza, UDMDatiTempPolizzaDP843;

function FStampaPolizzaDP843: TFStampaPolizzaDP843;
begin
  Result := TFStampaPolizzaDP843(UniMainModule.GetFormInstance(TFStampaPolizzaDP843));
end;

procedure TFStampaPolizzaDP843.ckbxDifPenVitaPrivataClick(Sender: TObject);
begin
  inherited;
  if (dbedtPIva.Text > '') or (isdigit(Copy(dbedtCodFiscale.Text, 1, 1))) then
  begin
    ckbxDifPenVitaPrivata.Checked := False;
    ckbxDifPenVitaPrivata.DataSource.DataSet.FieldByName('DifPenVPrivata').AsBoolean := False;
    ShowMessage('Non � possibile utilizzare questa estensione in presenza di partita IVA.');
  end;

end;

procedure TFStampaPolizzaDP843.ckbxProtezioneFamigliaClick(Sender: TObject);
begin
    pcOggettoAssicurato.ActivePage := pgAssicurato;
    pgFamiliari.TabVisible         := False;
    dbgrpForma.Enabled             := True;
    cbbCategoriaPat.Enabled        := True;

end;

procedure TFStampaPolizzaDP843.dbgrpFormaChangeValue(Sender: TObject);
var
  selectedForma: string;
begin

  selectedForma := dbgrpForma.Values[dbgrpForma.ItemIndex];
  if (selectedForma = 'A') then
  begin
    cbbMassimali.DataField := '';
    try
      UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
        selectedForma);
    finally
      cbbMassimali.DataField := 'SiglaMassimale';
    end;
  end
  else
  begin
    dbgrpForma.ItemIndex := 0;
    ShowMessage('Non � possibile selezionare la forma B in presenza dell''estensione "Protezione famiglia".');
  end;
end;

procedure TFStampaPolizzaDP843.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited;
  RiallineaMaxGaranzie;

end;

procedure TFStampaPolizzaDP843.RiallineaMaxGaranzie;
begin

end;

procedure TFStampaPolizzaDP843.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited SetCliente(ASelectedItem, ASelectedDescription);
{$MESSAGE WARN 'Sarebbe meglio gestirlo direttamente in CopiaDatiContraente'}
  // if UniMainModule.ContraenteIsSocieta then
  // begin
  // TDMDatiTempPolizzaDP843(UniMainModule.DMDatiTempPolizza).fdmtblAssicuratoDenominazione.AsString := '';
  // TDMDatiTempPolizzaDP843(UniMainModule.DMDatiTempPolizza).fdmtblAssicuratoCodFiscIvas.AsString := '';
  // end;
end;

procedure TFStampaPolizzaDP843.UniFormCreate(Sender: TObject);
begin
  inherited;
  // ckbxAppendiceControversie.Enabled := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICECC');
  // ckbxTacitoRinnovo.Enabled         := UniMainModule.DMdatiAge.isExtensionEnabled('SENZA_T_R');

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;

  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaForma.AsString);
  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  TDMDatiTempPolizzaDP843(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.AsString :=
    Copy(cbbMassimali.Text, 1, 3);

  pgFamiliari.TabVisible       := False;
  pcDatiEreditabili.ActivePage := pgContraente;
  UniDBCheckBox1.Checked := True;
  dbgrpForma.ItemIndex := 0;
end;

initialization

RegisterClass(TFStampaPolizzaDP843);

end.
