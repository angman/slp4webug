unit UStampaPolizzaDPTR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, Data.DB, System.Actions, Vcl.ActnList,
  uniPageControl, uniEdit, uniDBEdit, uniButton, uniBitBtn, uniSpeedButton, uniLabel,
  uniGUIBaseClasses, uniPanel, uniImageList, uniRadioGroup, uniDBRadioGroup, uniCheckBox,
  uniDBCheckBox, uniMultiItem, uniComboBox, uniDBComboBox, uniDateTimePicker, uniDBDateTimePicker,
  uniDBLookupComboBox, uniMemo, uniSpinEdit, UdmdatiAge, unimDBEdit, System.Math,
  uniScreenMask, Vcl.Menus, uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaDPTR = class(TFStampaBasePolizza)
    pgAssicurato: TUniTabSheet;
    pcOggettoAssicurato: TUniPageControl;
    pgOggettoAssicurato: TUniTabSheet;
    pgFamiliari: TUniTabSheet;
    pgMassimaliGaranzie: TUniTabSheet;
    undbrdgrpTipoAssicurato: TUniDBRadioGroup;
    pnlpncontaier: TUniContainerPanel;
    pnlAssicurato: TUniPanel;
    pnlVeicolo: TUniPanel;
    dbedtDenominazione: TUniDBEdit;
    dbedtCodiceFiscale: TUniDBEdit;
    dbedtPatente: TUniDBEdit;
    dbedtRilasciataDa: TUniDBEdit;
    dtpckDataRilascioPat: TUniDBDateTimePicker;
    dtpckDataScadenzaPat: TUniDBDateTimePicker;
    cbbCategoriaPat: TUniDBComboBox;
    dbedtNome1: TUniDBEdit;
    dbedtPatente1: TUniDBEdit;
    dbedtCategoria1: TUniDBEdit;
    dbedtNome2: TUniDBEdit;
    dbedtPatente2: TUniDBEdit;
    dbedtCategoria2: TUniDBEdit;
    dbedtNome3: TUniDBEdit;
    dbedtPatente3: TUniDBEdit;
    dbedtCategoria3: TUniDBEdit;
    dbedtNome4: TUniDBEdit;
    dbedtPatente4: TUniDBEdit;
    dbedtCategoria4: TUniDBEdit;
    dbcblkupTipoVeicolo: TUniDBLookupComboBox;
    dbedtMarca: TUniDBEdit;
    dbedtModello: TUniDBEdit;
    dbedtTarga: TUniDBEdit;
    dbedtTelaio: TUniDBEdit;
    dbedtQlHp: TUniDBEdit;
    dtpckScadenzaRevisione: TUniDBDateTimePicker;
    cbbMassimali: TUniDBComboBox;
    ckbxRischioAccessorio: TUniDBCheckBox;
    cbbRimbSpesePatPunti: TUniDBComboBox;
    dbgrpForma: TUniDBRadioGroup;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    ckbxAppendiceControversie: TUniDBCheckBox;
    ckbxIncMaxDP: TUniDBCheckBox;
    ckbxbProblematicaRCA: TUniDBCheckBox;
    ckbxPerizieParte: TUniDBCheckBox;
    ckbxMancatoInterventoRCA: TUniDBCheckBox;
    ckbxProtezioneFamiglia: TUniDBCheckBox;
    ckbxEstEPatenti: TUniDBCheckBox;
    unmEstEPatente: TUniMemo;
    dsTipoVeicolo: TDataSource;
    dbedtNumeroAssicurati: TUniDBNumberEdit;
    UniScreenMask: TUniScreenMask;
    procedure undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure dbgrpFormaChangeValue(Sender: TObject);
    procedure ckbxProtezioneFamigliaClick(Sender: TObject);
    procedure ckbxRischioAccessorioClick(Sender: TObject);
    procedure dbedtNumeroAssicuratiChange(Sender: TObject);
  private
    { Private declarations }
    procedure EnableDisableEstFamiglia(Enable: Boolean);
    procedure EnableDisableEstRischioAccessorio(Enable: Boolean);
    procedure RiallineaMaxGaranzie;
  protected
    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); override;

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;
    function doControllaAvanzato: Boolean; override;
  end;

function FStampaPolizzaDPTR: TFStampaPolizzaDPTR;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiPolizzaOM, UDMDatiTempPolizzaOM, UDMDatiTempPolizza;

function FStampaPolizzaDPTR: TFStampaPolizzaDPTR;
begin
  Result := TFStampaPolizzaDPTR(UniMainModule.GetFormInstance(TFStampaPolizzaDPTR));
end;

procedure TFStampaPolizzaDPTR.ckbxProtezioneFamigliaClick(Sender: TObject);
begin
  dbedtNumeroAssicurati.Enabled := ckbxProtezioneFamiglia.Checked;
  pgFamiliari.TabVisible        := ckbxProtezioneFamiglia.Checked;
  if not ckbxProtezioneFamiglia.Checked then
  begin
    pcOggettoAssicurato.ActivePage := pgAssicurato;
    dbedtNumeroAssicurati.Value    := 0;
    pgFamiliari.TabVisible         := false
  end
  else
  begin
    if Sender <> nil then
      dbedtNumeroAssicurati.Value := 1;
    pgFamiliari.TabVisible        := True;
  end;
  dbedtNumeroAssicuratiChange(nil);
end;

procedure TFStampaPolizzaDPTR.ckbxRischioAccessorioClick(Sender: TObject);
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
end;

procedure TFStampaPolizzaDPTR.dbedtNumeroAssicuratiChange(Sender: TObject);
var
  i: Integer;

  procedure EnableDisableFamiliari(Enable: Boolean);
  begin
    TUniDBEdit(FindComponent('DBEdtNome' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtPatente' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtCategoria' + i.ToString)).Enabled := Enable;

  end;

begin
  for i := 1 to 4 do
    EnableDisableFamiliari(i <= dbedtNumeroAssicurati.Value);
end;

procedure TFStampaPolizzaDPTR.dbgrpFormaChangeValue(Sender: TObject);
var
  selectedForma: string;
  ItemIndex: Integer;
begin
  selectedForma := dbgrpForma.Values[dbgrpForma.ItemIndex];
  // if cbbMassimali.ItemIndex = -1 then
  // cbbMassimali.ItemIndex := 0;

  ItemIndex := cbbMassimali.ItemIndex;
  // if ItemIndex = -1 then
  // ItemIndex := 0;

  cbbMassimali.DataField := '';
  try
    UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
      selectedForma);
    cbbMassimali.ItemIndex := ifThen(ItemIndex >= 0, ItemIndex, 0);

  finally
    cbbMassimali.DataField := 'SiglaMassimale';
    if cbbMassimali.ItemIndex >= 0 then
    begin
      cbbMassimali.Text := cbbMassimali.Items[cbbMassimali.ItemIndex];
      if not(UniMainModule.DMDatiTempPolizza.fdmtblPolizza.state in [dsinsert, dsedit]) then
        UniMainModule.DMDatiTempPolizza.fdmtblPolizza.edit;
      TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.asstring :=
        cbbMassimali.Items[cbbMassimali.ItemIndex];
    end;
  end;
  EnableDisableEstFamiglia(dbgrpForma.ItemIndex = 0);
  UniMainModule.DMDatiTempPolizza.calcolaPremio;
end;


function TFStampaPolizzaDPTR.doControllaAvanzato: Boolean;
begin
   // showmessage('eccomi qua !');
  result :=true;
  {
  if ( dsAssicurato.DataSet.FieldByName('targa').AsString = '') then
  begin
     ShowMessage('Bisogna inserire la targa del mezzo.');
     Result:=False;
  end;
  }
end;


procedure TFStampaPolizzaDPTR.EnableDisableEstFamiglia(Enable: Boolean);
begin
  ckbxProtezioneFamiglia.Enabled := Enable;
  dbedtNumeroAssicurati.Enabled  := false;
  ckbxProtezioneFamiglia.Checked := false;

end;

procedure TFStampaPolizzaDPTR.EnableDisableEstRischioAccessorio(Enable: Boolean);
begin
  ckbxRischioAccessorio.Enabled := Enable;
  cbbRimbSpesePatPunti.Enabled  := Enable and ckbxRischioAccessorio.Checked;
end;

procedure TFStampaPolizzaDPTR.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited;
  RiallineaMaxGaranzie;
end;

procedure TFStampaPolizzaDPTR.RiallineaMaxGaranzie;
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
  ckbxProtezioneFamigliaClick(nil);
  undbrdgrpTipoAssicuratoChangeValue(nil);
end;

procedure TFStampaPolizzaDPTR.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited SetCliente(ASelectedItem, ASelectedDescription);
  // undbrdgrpTipoAssicurato.readonly  := UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta;
  undbrdgrpTipoAssicurato.ItemIndex := 1;  // sempre veicolo !!! ifThen(UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta, 1, 0);
end;

procedure TFStampaPolizzaDPTR.undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
begin
{
  pnlAssicurato.visible := False; // undbrdgrpTipoAssicurato.ItemIndex = 0;

  pnlVeicolo.visible     := True;  // undbrdgrpTipoAssicurato.ItemIndex = 1;
  pgFamiliari.TabVisible := False; // undbrdgrpTipoAssicurato.ItemIndex = 0;
  dbgrpForma.Enabled     := True; // undbrdgrpTipoAssicurato.ItemIndex = 1;
  dbgrpForma.ItemIndex   := 1; // undbrdgrpTipoAssicurato.ItemIndex;
  EnableDisableEstFamiglia(false); //(undbrdgrpTipoAssicurato.ItemIndex = 0);
  EnableDisableEstRischioAccessorio(false); //((undbrdgrpTipoAssicurato.ItemIndex = 0)); // and ckbxRischioAccessorio.Checked);
 }
end;

procedure TFStampaPolizzaDPTR.UniFormCreate(Sender: TObject);
begin
  inherited;
  pcDatiEreditabili.ActivePage   := pgContraente;
  pcOggettoAssicurato.ActivePage := pgOggettoAssicurato;

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;

  ckbxMancatoInterventoRCA.Enabled  := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICEDP');
  ckbxAppendiceControversie.Enabled := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICECC');
  // ckbxTacitoRinnovo.Enabled         := UniMainModule.DMdatiAge.TacitoRinnovoEnabled;
  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaForma.asstring);
  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.asstring :=
    Copy(cbbMassimali.Text, 1, 3);
  // TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzaSiglaMassimaleChange(nil);
  dsTipoVeicolo.DataSet.Open;

  pnlAssicurato.visible  := false;
  pnlVeicolo.visible     := True;
  pgFamiliari.TabVisible := false;
  // veicolo fisso per questa polizza !!!
  undbrdgrpTipoAssicurato.Field.AsString:='V';
  undbrdgrpTipoAssicurato.OnChangeValue := undbrdgrpTipoAssicuratoChangeValue;
end;

initialization

RegisterClass(TFStampaPolizzaDPTR);

end.
