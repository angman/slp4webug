unit UStampaPolizzaPM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, uniGUIBaseClasses, uniImageList, Data.DB,
  System.Actions, Vcl.ActnList, uniRadioGroup, uniDBRadioGroup, uniCheckBox, uniDBCheckBox,
  uniMultiItem, uniComboBox, uniDBComboBox, uniEdit, uniDateTimePicker, uniDBDateTimePicker,
  uniDBEdit, uniBitBtn, uniPageControl, uniPanel, uniButton, uniSpeedButton, uniLabel, uniGroupBox, uniDBLookupComboBox,
  UDatiPolizzaPM, uniScreenMask, Vcl.Menus, uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaPM = class(TFStampaBasePolizza)
    dbedtSedeAttivita: TUniDBEdit;
    pgMassimaliGaranzie: TUniTabSheet;
    ungrpbxDatiPolizza: TUniGroupBox;
    cbbGruppoSpecializzazione: TUniDBLookupComboBox;
    dbrdgrpAttvit�: TUniDBRadioGroup;
    ungrpbxGaranzie: TUniGroupBox;
    cbbMassimali: TUniDBComboBox;
    ckbxEst14_15_16: TUniDBCheckBox;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    dbedtNumeroAssicurati: TUniDBNumberEdit;
    dsProfessioni: TDataSource;
    UniScreenMask: TUniScreenMask;
    procedure UniFormCreate(Sender: TObject);
    procedure cbbGruppoSpecializzazioneChange(Sender: TObject);
    procedure dbedtNumeroAssicuratiChange(Sender: TObject);
    procedure actConfermaExecute(Sender: TObject);
    procedure dbrdgrpAttvit�ChangeValue(Sender: TObject);

  private
    { Private declarations }
    codice_professione: integer;
  public
    { Public declarations }
  end;

function FStampaPolizzaPM: TFStampaPolizzaPM;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMdatiAgePolizze;

function FStampaPolizzaPM: TFStampaPolizzaPM;
begin
  Result := TFStampaPolizzaPM(UniMainModule.GetFormInstance(TFStampaPolizzaPM));
end;

procedure TFStampaPolizzaPM.actConfermaExecute(Sender: TObject);
begin
  if dbedtSedeAttivita.Text>'' then
     inherited
  else ShowMessage('Bisogna indicare la sede dell''attivit�.');

end;


procedure TFStampaPolizzaPM.cbbGruppoSpecializzazioneChange(Sender: TObject);
begin
  // UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
  //  VarToStr(cbbGruppoSpecializzazione.KeyValue));
  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
    cbbGruppoSpecializzazione.ListSource.DataSet.FieldByName('gruppo').AsString);
  cbbMassimali.DataSource.DataSet.FieldByName('siglamassimale').AsString:='';
end;

procedure TFStampaPolizzaPM.dbedtNumeroAssicuratiChange(Sender: TObject);
begin
  // devi applicare la tariffa corrispondente !!


end;

procedure TFStampaPolizzaPM.dbrdgrpAttvit�ChangeValue(Sender: TObject);
begin
  // inherited;
  if dbrdgrpAttvit�.ItemIndex<2 then
  begin
     dbedtNumeroAssicurati.Value:=0;
     dbedtNumeroAssicurati.Visible:=false;
  end else
     dbedtNumeroAssicurati.Visible:=True;
end;

procedure TFStampaPolizzaPM.UniFormCreate(Sender: TObject);
begin
  inherited;
  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;

  pcDatiEreditabili.ActivePage := pgContraente;
  cbbGruppoSpecializzazione.ListSource.DataSet.Open;

end;

initialization

RegisterClass(TFStampaPolizzaPM);

end.
