﻿inherited FStampaPolizzaPM: TFStampaPolizzaPM
  Caption = 'Polizza paramedico'
  ScreenMask.Target = Owner
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlEditButtons: TUniSimplePanel
    inherited btnConferma: TUniSpeedButton
      ScreenMask.Target = Owner
    end
    inherited btnAnteprimaStampa: TUniSpeedButton
      ScreenMask.Target = Owner
    end
  end
  inherited pnlDatiSpecificiPolizza: TUniPanel
    inherited pcDatiEreditabili: TUniPageControl
      ActivePage = pgMassimaliGaranzie
      inherited pgContraente: TUniTabSheet
        inherited cbbLkProfessione: TUniDBLookupComboBox
          Visible = False
        end
        inherited dbedtDescrAttività: TUniDBEdit
          Visible = False
        end
        object dbedtSedeAttivita: TUniDBEdit
          Left = 16
          Top = 201
          Width = 700
          Height = 22
          Hint = ''
          DataField = 'dati5'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          TabOrder = 12
          FieldLabel = 'Sede attivit'#224
          FieldLabelWidth = 65
        end
      end
      object pgMassimaliGaranzie: TUniTabSheet
        Hint = ''
        Caption = 'Massimali e Garanzie'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 910
        ExplicitHeight = 352
        object ungrpbxDatiPolizza: TUniGroupBox
          Left = 3
          Top = 10
          Width = 886
          Height = 95
          Hint = ''
          Caption = 'Dati specifici della polizza'
          TabOrder = 0
          object cbbGruppoSpecializzazione: TUniDBLookupComboBox
            Left = 13
            Top = 20
            Width = 459
            Hint = ''
            ListField = 'titolo'
            ListSource = dsProfessioni
            KeyField = 'codice'
            ListFieldIndex = 0
            DataField = 'AppNumero'
            DataSource = dsoEdit
            TabOrder = 1
            Color = clWindow
            FieldLabel = 'Specializzazione'
            OnChange = cbbGruppoSpecializzazioneChange
          end
          object dbrdgrpAttvità: TUniDBRadioGroup
            Left = 13
            Top = 50
            Width = 381
            Height = 38
            Hint = ''
            DataField = 'Dati4'
            DataSource = dsoEdit
            Caption = 'Attivit'#224
            TabOrder = 2
            Items.Strings = (
              'Dip. S.S.N.'
              'Dip. struttura privata'
              'Libero professionista')
            Columns = 3
            Values.Strings = (
              'D'
              'P'
              'L')
            OnChangeValue = dbrdgrpAttvitàChangeValue
          end
        end
        object ungrpbxGaranzie: TUniGroupBox
          Left = 3
          Top = 111
          Width = 886
          Height = 130
          Hint = ''
          Caption = 'Garanzie'
          TabOrder = 1
          object cbbMassimali: TUniDBComboBox
            Left = 13
            Top = 21
            Width = 310
            Hint = ''
            DataField = 'SiglaMassimale'
            DataSource = dsoEdit
            ParentFont = False
            TabOrder = 1
            FieldLabel = 'Massimale per  vertenza'
            IconItems = <>
          end
          object ckbxEst14_15_16: TUniDBCheckBox
            Left = 366
            Top = 23
            Width = 121
            Height = 17
            Hint = ''
            DataField = 'Est13'
            DataSource = dsoEdit
            Caption = 'Estensione 14/15/16'
            TabOrder = 2
            ParentColor = False
            Color = clBtnFace
          end
          object ckbxPolizzaIndicizzata: TUniDBCheckBox
            Left = 13
            Top = 104
            Width = 100
            Height = 17
            Hint = ''
            DataField = 'Indicizzata'
            DataSource = dsoEdit
            Caption = 'INDICIZZATA'
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 3
            ParentColor = False
            Color = clBtnFace
          end
          object dbedtNumeroAssicurati: TUniDBNumberEdit
            Left = 13
            Top = 61
            Width = 253
            Height = 22
            Hint = ''
            DataField = 'NDipAmmin'
            DataSource = dsoEdit
            TabOrder = 4
            MaxValue = 4.000000000000000000
            ShowTrigger = True
            FieldLabel = 'Numero dipendenti amministrativi'
            FieldLabelWidth = 150
            DecimalSeparator = ','
            OnChange = dbedtNumeroAssicuratiChange
          end
        end
      end
    end
  end
  inherited dsoEdit: TDataSource
    Left = 736
  end
  object dsProfessioni: TDataSource
    DataSet = DMDatiPolizzaPM.QProfessioniParamedico
    Left = 373
    Top = 348
  end
  object UniScreenMask: TUniScreenMask
    AttachedControl = btnAnteprimaStampa
    Enabled = True
    DisplayMessage = 'Attendere, generazione anteprima in corso ...'
    TargetControl = Owner
    Left = 693
    Top = 320
  end
end
