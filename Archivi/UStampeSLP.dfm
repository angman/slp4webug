inherited FStampeSLP: TFStampeSLP
  ClientWidth = 543
  Caption = 'Elenco documenti agenzia'
  ExplicitWidth = 559
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 543
    ExplicitWidth = 543
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 543
    ExplicitWidth = 543
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 543
      Height = 110
      ExplicitWidth = 543
      ExplicitHeight = 110
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 543
        Height = 45
        Align = alTop
        ExplicitWidth = 543
        ExplicitHeight = 45
        inherited btnInsert: TUniSpeedButton
          Action = actVisualizzaDocumento
        end
        inherited btnModify: TUniSpeedButton
          Top = -2
          Action = actEsportaDocumento
          ExplicitTop = -2
        end
        inherited btnSeleziona: TUniSpeedButton
          Left = 247
          Top = -2
          Action = actEsportaDocumento
          ExplicitLeft = 247
          ExplicitTop = -2
        end
        inherited btnChiudi: TUniSpeedButton
          Left = 433
          ExplicitLeft = 433
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 543
      end
      object UniPanelFilter: TUniSimplePanel
        Left = 0
        Top = 65
        Width = 543
        Height = 45
        Hint = ''
        ParentColor = False
        Align = alClient
        TabOrder = 3
        ExplicitLeft = 16
        ExplicitTop = 72
        ExplicitWidth = 256
        ExplicitHeight = 128
        object UniCBtipoStampe: TUniComboBox
          Left = 13
          Top = 12
          Width = 460
          Hint = ''
          Text = ''
          TabOrder = 1
          ClearButton = True
          FieldLabel = 'Tipo stampa'
          FieldLabelWidth = 80
          IconItems = <>
          OnChange = UniCBtipoStampeChange
        end
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Top = 110
      Width = 543
      Height = 309
      ExplicitWidth = 543
      inherited grdElenco: TUniDBGrid
        Width = 543
        Height = 309
        OnFieldImageURL = grdElencoFieldImageURL
        Columns = <
          item
            FieldName = 'DATA_LETTURA'
            Title.Caption = 'Visto'
            Width = 40
            ReadOnly = True
            ImageOptions.Visible = True
            ImageOptions.Width = 32
            ImageOptions.Height = 32
          end
          item
            FieldName = 'DATA_DOC'
            Title.Caption = 'Data'
            Width = 64
          end
          item
            FieldName = 'DESCRIZIONE'
            Title.Caption = 'Descrizione'
            Width = 400
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 48
        Top = 78
        ExplicitLeft = 48
        ExplicitTop = 78
      end
    end
  end
  inherited actlstOperazioni: TActionList
    Left = 320
    Top = 152
    object actVisualizzaDocumento: TAction
      Category = 'ItemsDocumento'
      Caption = 'Visualizza'
      OnExecute = actVisualizzaDocumentoExecute
    end
    object actEsportaDocumento: TAction
      Category = 'ItemsDocumento'
      Caption = 'Esporta PDF'
      OnExecute = actEsportaDocumentoExecute
    end
  end
  inherited dsGrid: TDataSource
    Left = 408
    Top = 144
  end
  object DStipoStampe: TDataSource
    DataSet = DMDatiReport.QtipoStampe
    Left = 416
    Top = 224
  end
end
