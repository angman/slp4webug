inherited FStampaPolizzaFM: TFStampaPolizzaFM
  Caption = 'Polizza famiglia plus'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlEditButtons: TUniSimplePanel
    inherited btnAnteprimaStampa: TUniSpeedButton
      ScreenMask.Target = Owner
    end
  end
  inherited pnlDatiSpecificiPolizza: TUniPanel
    inherited pcDatiEreditabili: TUniPageControl
      ExplicitLeft = 3
      ExplicitTop = 6
      object pgDatiAssicurato: TUniTabSheet
        Hint = ''
        Caption = 'Dati Assicurato'
        ParentFont = False
        object dbedtDimSaltINDIRIZZO: TUniDBEdit
          Left = 12
          Top = 76
          Width = 414
          Height = 22
          Hint = ''
          Visible = False
          DataField = 'INDIRIZZO'
          DataSource = dsAssicurato
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 0
          FieldLabel = 'Indirizzo'
          FieldLabelWidth = 65
        end
        object dbedtDimSaltCAP: TUniDBEdit
          Left = 432
          Top = 76
          Width = 121
          Height = 22
          Hint = ''
          Visible = False
          DataField = 'CAP'
          DataSource = dsAssicurato
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 1
          FieldLabel = 'Cap'
          FieldLabelWidth = 60
        end
        object dbedtDimSaltCITTA: TUniDBEdit
          Left = 559
          Top = 76
          Width = 263
          Height = 22
          Hint = ''
          Visible = False
          DataField = 'CITTA'
          DataSource = dsAssicurato
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 2
          FieldLabel = 'Citt'#224
          FieldLabelWidth = 35
        end
        object lblDimoraSaltuaria: TUniLabel
          Left = 12
          Top = 40
          Width = 319
          Height = 13
          Hint = ''
          Visible = False
          Caption = 'Dimora saltuaria in ITALIA (per garanzia VITA PRIVATA):'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 3
        end
        object dbedtDimSaltPROV: TUniDBEdit
          Left = 828
          Top = 76
          Width = 74
          Height = 22
          Hint = ''
          Visible = False
          DataField = 'Provincia'
          DataSource = dsAssicurato
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 4
          FieldLabel = 'Prov'
          FieldLabelWidth = 40
        end
      end
      object pgMassimaliGaranzie: TUniTabSheet
        Hint = ''
        Caption = 'Massimali e Garanzie'
        object cbbMassimali: TUniDBComboBox
          Left = 16
          Top = 24
          Width = 249
          Hint = ''
          DataField = 'SiglaMassimale'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 0
          FieldLabel = 'Massimale per  vertenza'
          FieldLabelWidth = 80
          IconItems = <>
        end
        object ckbxControvFiscali: TUniDBCheckBox
          Left = 330
          Top = 27
          Width = 174
          Height = 17
          Hint = ''
          DataField = 'Estensione12'
          DataSource = dsoEdit
          ValueChecked = 'true'
          ValueUnchecked = 'false'
          Caption = 'Estensione 12 - controv. fiscali'
          TabOrder = 1
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxConsulTecnic: TUniDBCheckBox
          Left = 545
          Top = 27
          Width = 174
          Height = 17
          Hint = ''
          DataField = 'Estensione13'
          DataSource = dsoEdit
          ValueChecked = 'true'
          ValueUnchecked = 'false'
          Caption = 'Estensione 13 - consul. tecnic.'
          TabOrder = 2
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxIndicizzata: TUniDBCheckBox
          Left = 758
          Top = 27
          Width = 92
          Height = 17
          Hint = ''
          DataField = 'Indicizzata'
          DataSource = dsoEdit
          ValueChecked = 'true'
          ValueUnchecked = 'false'
          Caption = 'INDICIZZATA'
          TabOrder = 3
          ParentColor = False
          Color = clBtnFace
        end
      end
    end
  end
  object UniScreenMask: TUniScreenMask
    AttachedControl = btnAnteprimaStampa
    Enabled = True
    DisplayMessage = 'Attendere, generazione anteprima in corso ...'
    TargetControl = Owner
    Left = 693
    Top = 320
  end
end
