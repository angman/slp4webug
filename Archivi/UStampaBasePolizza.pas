unit UStampaBasePolizza;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, uniPanel, uniEdit, uniDBEdit,
  uniPageControl, UDMDatiTempPolizza, uniImageList, uniDateTimePicker, uniDBDateTimePicker,
  uniMultiItem, uniComboBox, uniDBComboBox, uniCheckBox, uniDBCheckBox, uniRadioGroup,
  uniDBRadioGroup, UTipoSalvataggioDocumento, uniSpinEdit, uniScreenMask, uniDBLookupComboBox, Vcl.Menus, uniMainMenu,
  uniMenuButton, UDatiPolSostituitaRec;

type
  TFStampaBasePolizza = class(TFMasterEdit)
    dsGaranzia: TDataSource;
    dsAssicurato: TDataSource;
    btnEsci: TUniSpeedButton;
    actUscita: TAction;
    dbedtPolizza: TUniDBEdit;
    pnlDatiSpecificiPolizza: TUniPanel;
    pcDatiEreditabili: TUniPageControl;
    pnlDatiFissi: TUniPanel;
    dbedtAgenzia: TUniDBEdit;
    dbedtCodAge: TUniDBEdit;
    dbedtSubAge: TUniDBEdit;
    dbedtSubPromoter: TUniDBEdit;
    dbedtSostPol: TUniDBEdit;
    dtpckScadSostPol: TUniDBDateTimePicker;
    dtpckDataEffetto: TUniDBDateTimePicker;
    dtpckDataScadenza: TUniDBDateTimePicker;
    cbbFrazionamento: TUniDBComboBox;
    dbedtDurataAnni: TUniDBEdit;
    dbedtDurataMesi: TUniDBEdit;
    dbedtRateoGG: TUniDBEdit;
    ckbxRatino: TUniDBCheckBox;
    dtpckScad1Rata: TUniDBDateTimePicker;
    dtpckDataEmissione: TUniDBDateTimePicker;
    pnlPremiPolizza: TUniPanel;
    cbbFrazionamento1: TUniDBComboBox;
    dtpckRataFirmaFinoAl: TUniDBDateTimePicker;
    dbedtPremioNetto: TUniDBNumberEdit;
    dbedtPremioAccessori: TUniDBNumberEdit;
    dbedtIntFrazionamento: TUniDBNumberEdit;
    dbedtImponibile1: TUniDBNumberEdit;
    dbedtImposte1: TUniDBNumberEdit;
    dbedtTotale1: TUniDBNumberEdit;
    dbedtNetto2: TUniDBNumberEdit;
    dbedtAccessori2: TUniDBNumberEdit;
    dbedtRimbPolSost: TUniDBNumberEdit;
    dbedtImponibile2: TUniDBNumberEdit;
    dbedtImposte2: TUniDBNumberEdit;
    dbedtTotale2: TUniDBNumberEdit;
    undbrdgrpProvvig: TUniDBRadioGroup;
    actSelezionaCollaboratore: TAction;
    pgContraente: TUniTabSheet;
    dbedtContraente: TUniDBEdit;
    btnSelCliente: TUniBitBtn;
    dbedtCodFiscale: TUniDBEdit;
    dbedtPIva: TUniDBNumberEdit;
    dbedtLocNascita: TUniDBEdit;
    dtpckDataNascita: TUniDBDateTimePicker;
    dbedtIndirizzo: TUniDBEdit;
    dbedtCap: TUniDBEdit;
    dbedtCitta: TUniDBEdit;
    actSelezionaContraente: TAction;
    btnRiprendiPolizza: TUniSpeedButton;
    actRiprendiPolizza: TAction;
    pnlHeadPolizza: TUniPanel;
    lblHeadPolizza: TUniLabel;
    lblDescrPolizza: TUniLabel;
    dbedtPercAccessori: TUniDBNumberEdit;
    actSelezionaPolizzaDaSostituire: TAction;
    ckbxTacitoRinnovo: TUniDBCheckBox;
    btnPiuNPercAccessori: TUniBitBtn;
    btnMenoNPercAccessori: TUniBitBtn;
    btnAnteprimaStampa: TUniSpeedButton;
    actAnteprimaStampa: TAction;
    dbedtPROVINCIA: TUniDBEdit;
    btnAzzeraValori: TUniSpeedButton;
    actAzzeraValori: TAction;
    cbbLkProfessione: TUniDBLookupComboBox;
    dbedtDescrAttivit�: TUniDBEdit;
    UniDBProvvigioni: TUniDBNumberEdit;
    BaccMax: TUniButton;
    BaccMin: TUniButton;
    actSelezionaSubCollaboratore: TAction;
    dbedtIntermediataDaSigla: TUniDBEdit;
    actIntermediataDa: TAction;
    unpmnPolSost: TUniPopupMenu;
    btnSelezionaProduttore: TUniMenuButton;
    unpmnSelProduttore: TUniPopupMenu;
    mnuSelezionaCollaboratore: TUniMenuItem;
    actTogliCollaboratore: TAction;
    mnuTogliCollaboratore: TUniMenuItem;
    btnSelezionaSubproduttore: TUniMenuButton;
    unpmnSelSubCollaboratore: TUniPopupMenu;
    UniMenuItem1: TUniMenuItem;
    UniMenuItem2: TUniMenuItem;
    actTogliSubcollaboratore: TAction;
    btnIntermediataDa: TUniMenuButton;
    unpmnIntermediataDa: TUniPopupMenu;
    mnuIntermediataDa: TUniMenuItem;
    mnuTogliIntermediataDa: TUniMenuItem;
    actTogliIntermediataDa: TAction;
    btnPolizzaSostituita: TUniMenuButton;
    mnuSelezionaPolizzaDaSostituire: TUniMenuItem;
    actTogliPolizzaSostituita: TAction;
    mnuTogliPolizzaSostituita: TUniMenuItem;
    actRicalcolaPremio: TAction;
    mnuRicalcolaPremio: TUniMenuItem;
    UniBBincAnno: TUniBitBtn;
    UniBBdecAnno: TUniBitBtn;
    cbbTelefono: TUniDBLookupComboBox;
    cbbMail: TUniDBLookupComboBox;    
    procedure actUscitaExecute(Sender: TObject);
    procedure actEmettiPolizzaExecute(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure actSelezionaCollaboratoreExecute(Sender: TObject);
    procedure actSelezionaContraenteExecute(Sender: TObject);
    procedure actRiprendiPolizzaExecute(Sender: TObject);
    procedure actSelezionaPolizzaDaSostituireExecute(Sender: TObject);
    procedure btnPiuNPercAccessoriClick(Sender: TObject);
    procedure actAnteprimaStampaExecute(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure actConfermaExecute(Sender: TObject);
    procedure actConfermaUpdate(Sender: TObject);
    procedure actAzzeraValoriExecute(Sender: TObject);
    procedure BaccMaxClick(Sender: TObject);
    procedure actSelezionaSubCollaboratoreExecute(Sender: TObject);
    procedure actIntermediataDaExecute(Sender: TObject);
    procedure dbedtSubPromoterDblClick(Sender: TObject);
    procedure dbedtSubAgeDblClick(Sender: TObject);
    procedure dbedtSostPolChangeValue(Sender: TObject);
    procedure actTogliCollaboratoreExecute(Sender: TObject);
    procedure actTogliIntermediataDaExecute(Sender: TObject);
    procedure actTogliPolizzaSostituitaExecute(Sender: TObject);
    procedure actRicalcolaPremioExecute(Sender: TObject);
    procedure dtpckDataScadenzaExit(Sender: TObject);
    procedure UniBBincAnnoClick(Sender: TObject);
    procedure UniMenuItem2Click(Sender: TObject);
    procedure undbrdgrpProvvigClick(Sender: TObject);
  private
    { Private declarations }
    lDescrUtente: string;
    lTipoSalvataggioDoc: TTipoSalvataggio;

    FTipoDocumentoAttuale: TTipoSalvataggio;
    ActConfermaEnabled: Boolean;
    DatiPolizzaSostituita: TDatiPolizzaSostituita;

    procedure ProtectDatiContraente;
    procedure SetTipoDocumentoAttuale(const Value: TTipoSalvataggio);
    procedure doSaveDoc(Sender: TComponent; AResult: Integer);
    procedure doRicalcoloPolizza(Sender: TComponent; AResult: Integer);

    procedure SaveDescrUtente(Sender: TComponent; AResult: Integer; AText: string);
  protected
    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); virtual;

    property TipoDocumentoAttuale: TTipoSalvataggio read FTipoDocumentoAttuale write SetTipoDocumentoAttuale;

  public
    { Public declarations }

    procedure SetCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
    procedure SetSubCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
    procedure SetIntermediataDa(ASelectedItem: Integer; const ASelectedDescription: string);

    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); virtual;
    procedure svuota(datiPS: TDatiPolizzaSostituita);
    procedure mostra_print_house(npol: string);
    function doControllaAvanzato: boolean; virtual;


  end;

function FStampaBasePolizza: TFStampaBasePolizza;

implementation

{$R *.dfm}

uses
  uniGUIApplication, MainModule, UCollaboratori, UClienti, UDMAgeCollaboratori, URiprendiBozza, UAskDocSaveType,
  UContratti, UVediTempTables, UdmdatiAge, System.Math, uniGUIDialogs, UDatiPolizzaSostituita, uPrintHouseSLP,
  UPolizzaExceptions, date360;

function FStampaBasePolizza: TFStampaBasePolizza;
begin
  Result := TFStampaBasePolizza(UniMainModule.GetFormInstance(TFStampaBasePolizza));
end;

procedure TFStampaBasePolizza.actAnteprimaStampaExecute(Sender: TObject);
begin
  Cursor := crHourGlass;
  try
    UniMainModule.DMDatiTempPolizza.AnteprimaPolizza;
  finally
    Cursor := crDefault;
  end;
end;

procedure TFStampaBasePolizza.actAzzeraValoriExecute(Sender: TObject);
begin
  UniMainModule.DMDatiTempPolizza.AggiungiPolizza;
end;

procedure TFStampaBasePolizza.actConfermaExecute(Sender: TObject);
begin
  // if UniMainModule.VerificaAutorizzazione('PPP', 'STA') then
  if UniMainModule.VerificaAutorizzazione('PPO', 'MEM') then
    TFAskDocSaveType.ShowAskDocSaveType(uniApplication, TipoDocumentoAttuale, doSaveDoc);

end;

procedure TFStampaBasePolizza.actConfermaUpdate(Sender: TObject);
begin
  inherited;
  actConferma.Enabled := ActConfermaEnabled;
end;

procedure TFStampaBasePolizza.actEmettiPolizzaExecute(Sender: TObject);
begin
  // UniMainModule.DMDatiTempPolizza.MemorizzaDocumento(TipoDocumentoAttuale);
end;

procedure TFStampaBasePolizza.actIntermediataDaExecute(Sender: TObject);
var parametro: Integer;
begin
  // tolta il 10/12/2020 in quanto non bisogna bloccare la seleziona a nessuno !
  // la visibilit� � gi� schermata a dovere

  // with TFCollaboratori.Create(uniApplication) do
  if UniMainModule.DMdatiAge.is_utente_promoter then parametro:= -UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger
  else parametro:=0;

  with TFCollaboratori.Create(uniApplication, parametro) do
  begin
    OnSelectedItem := SetIntermediataDa;
    ShowModal;
  end;
end;

procedure TFStampaBasePolizza.actSelezionaSubCollaboratoreExecute(Sender: TObject);
begin
  // operativo solo se si � scelto il FATTA DA
  if dbedtSubAge.Text > '' then
  begin
    // tolta il 10/12/2020 in quanto non bisogna bloccare la seleziona a nessuno !
    // la visibilit� � gi� schermata a dovere
    // UniMainModule.aggiornaPosizione('COL', 'MOD', 'Agenti-Dipendenti-Collaboratori');
    with TFCollaboratori.Create(uniApplication, UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger) do
    begin
      OnSelectedItem := SetSubCollaboratore;
      ShowModal;
    end;
  end;
end;

procedure TFStampaBasePolizza.actTogliCollaboratoreExecute(Sender: TObject);
begin
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubAgenzia.AsString := '';
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger := 0;
  dbedtSubAge.Text := '';
  // se c'e'ra un sub promoter ripulisci !! non pu� esistere un sub promoter senza il promoter
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoterSigla.AsString := '';
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger := 0;
  dbedtSubPromoter.Text := '';
end;

procedure TFStampaBasePolizza.actTogliIntermediataDaExecute(Sender: TObject);
begin
  dbedtIntermediataDaSigla.Text := '';
end;

procedure TFStampaBasePolizza.actTogliPolizzaSostituitaExecute(Sender: TObject);
begin
  dbedtSostPol.Text := '';
  svuota(DatiPolizzaSostituita);
  UniMainModule.DMDatiTempPolizza.impostaDatiPolizzaSostituita(0, 0);
  UniMainModule.DMDatiTempPolizza.calcolaPremio;
end;

procedure TFStampaBasePolizza.actRicalcolaPremioExecute(Sender: TObject);
begin
  UniMainModule.DMdatiAgePolizze.getDatiPolizzaSostituita(dbedtSostPol.Text, DatiPolizzaSostituita);
  DatiPolizzaSostituita.nuovaDataEffetto := dtpckDataEffetto.DateTime;
  TFDatiPolizzaSostituita.askDatiPolizzaSost(uniApplication, DatiPolizzaSostituita, doRicalcoloPolizza);
end;


procedure TFStampaBasePolizza.actRiprendiPolizzaExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('PRE', 'GET', 'Riprendi bozza/preventivo');
  with TFRiprendiBozza.Create(uniApplication) do
  begin
    OnSelectedItem := ReloadDocumento;
    ShowModal;
  end;

end;

procedure TFStampaBasePolizza.actSelezionaCollaboratoreExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('COL', 'MOD', 'Agenti-Dipendenti-Collaboratori');
  with TFCollaboratori.Create(uniApplication, 0, 3) do
  begin
    OnSelectedItem := SetCollaboratore;
    ShowModal;
  end;

end;

procedure TFStampaBasePolizza.actSelezionaContraenteExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('CLI', 'MOD', 'Clienti');
  // MB 26102020
  if not (UniMainModule.DMDatiTempPolizza.fdmtblPolizza.State in [dsEdit, dsInsert]) then
     UniMainModule.DMDatiTempPolizza.fdmtblPolizza.Edit;
  if not (UniMainModule.DMDatiTempPolizza.fdmtblAssicurato.State in [dsEdit, dsInsert]) then
     UniMainModule.DMDatiTempPolizza.fdmtblAssicurato.Edit;

  with TFClienti.Create(uniApplication) do
  begin
    ImpostaDmMaster(UniMainModule.DMDatiAGeClienti);
    OnSelectedItem := SetCliente;
    ShowModal;
  end;

end;

procedure TFStampaBasePolizza.actSelezionaPolizzaDaSostituireExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('POL', 'VIS', 'Contratti');

  with TFContratti.Create(uniApplication) do
  begin
    OnSelectedItem := UniMainModule.DMDatiTempPolizza.SetPolizzaSostituita;
    OnlySelection  := True;
    ShowModal;
  end;

end;

procedure TFStampaBasePolizza.actUscitaExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFStampaBasePolizza.btnPiuNPercAccessoriClick(Sender: TObject);
begin
  if TUniBitBtn(Sender) = btnPiuNPercAccessori then
  begin
    if UniMainModule.DMDatiTempPolizza.fdmtblPolizzaPercAccessori.AsCurrency < dbedtPercAccessori.MaxValue then
       UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPercAccessori.AsInteger :=
         UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPercAccessori.AsInteger + 1;
  end else
    if (UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPercAccessori.AsInteger - 1) >= 0 then
      UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPercAccessori.AsInteger :=
        UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPercAccessori.AsInteger - 1;

end;

procedure TFStampaBasePolizza.dbedtSostPolChangeValue(Sender: TObject);
begin
  actRicalcolaPremio.Enabled        := dbedtSostPol.Text <> '';
  actTogliPolizzaSostituita.Enabled := dbedtSostPol.Text <> '';
end;

procedure TFStampaBasePolizza.dbedtSubAgeDblClick(Sender: TObject);
begin
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubAgenzia.AsString := '';
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger := 0;
  dbedtSubAge.Text := '';
end;

procedure TFStampaBasePolizza.dbedtSubPromoterDblClick(Sender: TObject);
begin
  // azzera i dati scelti in precedenza
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoterSigla.AsString := '';
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger := 0;
  dbedtSubPromoter.Text := '';
end;

function TFStampaBasePolizza.doControllaAvanzato: boolean;
begin
   //
   Result := True;
end;

procedure TFStampaBasePolizza.doRicalcoloPolizza(Sender: TComponent; AResult: Integer);
begin
  if AResult = mrOk then
  begin
    UniMainModule.DMDatiTempPolizza.impostaDatiPolizzaSostituita(TFDatiPolizzaSostituita(Sender)
      .nedtNettoRimborsoContraente.Value, TFDatiPolizzaSostituita(Sender).dtPckPagA.DateTime);
    UniMainModule.DMDatiTempPolizza.calcolaPremio;
  end;
end;

procedure TFStampaBasePolizza.doSaveDoc(Sender: TComponent; AResult: Integer);
// var
// lTipoSalvataggioDoc: TTipoSalvataggio;
   procedure invia_com(tipo: string);
   begin
        UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' MEM' + tipo+'-' + UniMainModule.SiglaPolizza + ' Np'+
         UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPolizza.AsString
         + ' ' + FormatDateTime('dd-mm-yyy', UniMainModule.DMDatiTempPolizza.fdmtblPolizzaDecorrenza.AsDateTime),
         'CK', '', '', '', 'MPO', UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPolizza.AsString,
         UniMainModule.DMDatiTempPolizza.fdmtblPolizzaDecorrenza.AsDateTime);

   end;
begin
  if AResult = mrOk then
  begin
    if True then // doControllaAvanzato then
    begin
    lTipoSalvataggioDoc := TFAskDocSaveType(Sender).TipoDocumentoToSave;
    Cursor              := crHourGlass;
    UniMainModule.DMDatiBasePolizza.DescrUtente := '';
    try
       try
         case lTipoSalvataggioDoc of
           tsBozza, tsNuovaBozza:
           begin
             invia_com('BOZ');
             prompt('@@Aggiungi descrizione e conferma per salvare la bozza:', '', mtInformation, mbOKCancel, SaveDescrUtente, False);
           end;

           tsPreventivo, tsNuovoPreventivo:
           begin
             invia_com('PRE');

             prompt('@@Aggiungi descrizione e conferma per salvare il preventivo:', '', mtInformation, mbOKCancel, SaveDescrUtente, False);
           end;
         else
           begin
             // la memorizzazione dell'operazione viene fatta da altra parte per aver disponibile il numero di polizza
             UniMainModule.DMDatiTempPolizza.MemorizzaDocumento(lTipoSalvataggioDoc,
               TFAskDocSaveType(Sender).MostraReportPolizza, mostra_print_house);
             TipoDocumentoAttuale := lTipoSalvataggioDoc;

           end;
         end;
         if lTipoSalvataggioDoc = tsPolizza then
         begin
           Close; // mb 06082020
         end;

      except
       on E: EPolizzaError do raise;
      end;
    finally
      Cursor := crDefault;
      {
      if lTipoSalvataggioDoc = tsPolizza then
      begin
        Close; // mb 06082020
      end;
      }
    end;
    end;
  end;

end;




procedure TFStampaBasePolizza.dtpckDataScadenzaExit(Sender: TObject);
begin
  inherited;
  // mb 07122020  altrimenti non aggiorna la data nel caso di forzatura da codie per inserimento scadenza non valida
  if UniMainModule.DMDatiTempPolizza.fdmtblPolizzaScadenza.AsDateTime<>dtpckDataScadenza.DateTime then
     dtpckDataScadenza.DateTime := UniMainModule.DMDatiTempPolizza.fdmtblPolizzaScadenza.AsDateTime;
end;

procedure TFStampaBasePolizza.SaveDescrUtente(Sender: TComponent; AResult: Integer; AText: string);
begin
  lDescrUtente := '';
  UniMainModule.DMDatiBasePolizza.DescrUtente := '';
  if AResult= mrOk then
  begin

     if AResult = mrOk then
       UniMainModule.DMDatiBasePolizza.DescrUtente := AText
     else
       UniMainModule.DMDatiBasePolizza.DescrUtente := '';

     if lTipoSalvataggioDoc in [tsNuovoPreventivo,tsPreventivo] then
     begin
        ScreenMask.showMask('Attendere.Generazione anteprima preventivo in corso');
        UniSession.Synchronize;
     end;

     UniMainModule.DMDatiTempPolizza.MemorizzaDocumento(lTipoSalvataggioDoc, TFAskDocSaveType(Sender).MostraReportPolizza,mostra_print_house);

     if lTipoSalvataggioDoc in [tsNuovoPreventivo,tsPreventivo] then
        //Self.ScreenMask.HideMask;

     TipoDocumentoAttuale := lTipoSalvataggioDoc;

  end;
end;

procedure TFStampaBasePolizza.ProtectDatiContraente;
begin
  cbbLkProfessione.ReadOnly   := dsoEdit.DataSet.FieldByName('PROFESSIONE').AsInteger > 0;
  dbedtDescrAttivit�.ReadOnly := dsoEdit.DataSet.FieldByName('DESCRIZIONEATTIVITA').AsString <> '';
  cbbLkProfessione.Color      := IfThen(cbbLkProfessione.ReadOnly, clYellow, clWindow);
  dbedtDescrAttivit�.Color    := IfThen(dbedtDescrAttivit�.ReadOnly, clYellow, clWindow);

end;

procedure TFStampaBasePolizza.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  FTipoDocumentoAttuale := UniMainModule.DMDatiTempPolizza.RiprendiDocumento(ASelectedItem);
  // ricopia il contraente per aggiornare la property ConstaenteIsSocieta in UniMainModule
  if UniMainModule.DMDatiTempPolizza.readTipoAssicurato = 'P' then
    UniMainModule.DMDatiTempPolizza.CopiaDatiContraente
      (UniMainModule.DMDatiTempPolizza.fdmtblPolizzaRifCodCli.AsInteger);
  ProtectDatiContraente;
{$IFDEF TEMPTABLES}
  // fdmtblGaranzia.SaveToFile(TPath.combine(SLPDatiAge.Dati_gen.dir_temp, 'TempGaranzie.txt'), sfXML);
  TFmVediTempTables.ShowGaranzie(uniApplication, UniMainModule.DMDatiTempPolizza.fdmtblPolizza,
    UniMainModule.DMDatiTempPolizza.fdmtblAssicurato, UniMainModule.DMDatiTempPolizza.fdmtblGaranzia);
{$ENDIF}
end;

procedure TFStampaBasePolizza.BaccMaxClick(Sender: TObject);
begin
  if TUniButton(Sender) = BaccMax then
  begin
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaPercAccessori.AsCurrency := dbedtPercAccessori.MaxValue;
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPercAccessori.AsCurrency := dbedtPercAccessori.MaxValue / 0.25;
  end
  else
  begin
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaPercAccessori.AsCurrency := 0;
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPercAccessori.AsCurrency := 0;
  end;

end;

procedure TFStampaBasePolizza.UniBBincAnnoClick(Sender: TObject);
var i, diff_mesi: Integer;

   function muovi(dir: Integer ): integer;
   begin
      Result:=dir;
      diff_mesi := diffMesi30(dtpckDataEffetto.DateTime , IncMonth(dtpckDataScadenza.DateTime, dir));
      if dtpckDataEffetto.DateTime < IncMonth(dtpckDataScadenza.DateTime, dir) then
      begin
         if UniMainModule.DMdatiAge.find_durata(trunc(diff_mesi/12),UniMainModule.DMDatiTempPolizza.fdmtblPolizzaProvvig.AsString ) then
            result:=dir
         else
            result:= dir * 2;
      end else result:=0;
   end;
begin

  if TUniBitBtn(Sender) = UniBBincAnno then
  begin
     UniMainModule.DMDatiTempPolizza.fdmtblPolizzaScadenza.AsDateTime :=
     IncMonth(UniMainModule.DMDatiTempPolizza.fdmtblPolizzaScadenza.AsDateTime, muovi(12));
  end else begin
     UniMainModule.DMDatiTempPolizza.fdmtblPolizzaScadenza.AsDateTime :=
     IncMonth(UniMainModule.DMDatiTempPolizza.fdmtblPolizzaScadenza.AsDateTime, muovi(-12));
  end;
end;

procedure TFStampaBasePolizza.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  inherited;
end;

procedure TFStampaBasePolizza.UniFormCreate(Sender: TObject);
begin
  inherited;

  lblDescrPolizza.Caption     := UniMainModule.DMdatiAgePolizze.getLabelDatiPolizza(UniMainModule.TipoPolizza);
  dbedtPercAccessori.MaxValue := UniMainModule.DMdatiAgePolizze.getMaxAccessoriPolizza(UniMainModule.TipoPolizza);

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;

  UniMainModule.DMDatiTempPolizza.AggiungiPolizza;
  // ActConfermaEnabled        := UniMainModule.DMdatiAge.VerAutGenAgenzia('PPO_STA') and
  // UniMainModule.VerificaAutorizzazione('PPO', 'MEM');
  ActConfermaEnabled := UniMainModule.VerificaAutorizzazione('PPO', 'MEM');

  btnConferma.Action        := actConferma;
  ckbxTacitoRinnovo.Visible := UniMainModule.DMdatiAge.TacitoRinnovoEnabled;
  cbbLkProfessione.ListSource.DataSet.Open;

  // ripulisci il record relativo alla polizza sostituita (altrimenti entrando e uscendo dopo aver lavorato su una
  // sostituzione rimangono i dati ..)
  svuota(DatiPolizzaSostituita);

  // se si tratta di un tunte promoter allora devi bloccare la possibilit� di cambiare la sub-agenzia
  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    // nascondi le provvigioni !!!! da sistemare scollegando la visualizzazione delle provvigioni dal
    // calcolo in modo da poter mostrare importi per i collaboratori, ma memorizzare sempre quelle di agenzia
    // vedi uDMdatiTempPolizza.pas l. 1252 TDMDatiTempPolizza.calcolaProv
    UniDBProvvigioni.Visible := False;

    // 1 - blocca la selezione del collaboratore
    // 2 - istanzia il collaboratore all'utente-prooter correntemente loggato
    // 3 - valorizza i campi necessari e le scritte
    // 4 - itera il ragionamento per il sub-produttore

    btnSelezionaProduttore.Enabled := False;
    SetCollaboratore(UniMainModule.UtentePromoter, '');
    if UniMainModule.UtenteSubPromoter > 0 then
    begin
      btnSelezionaSubproduttore.Enabled := False;
      SetSubCollaboratore(UniMainModule.UtenteSubPromoter, '');

    end;
  end;
  // ProtectDatiContraente;
end;

procedure TFStampaBasePolizza.UniMenuItem2Click(Sender: TObject);
begin
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoterSigla.AsString := '';
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger := 0;
  dbedtSubPromoter.Text := '';
end;

procedure TFStampaBasePolizza.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  // utilizzato per verificare se le memtable fanno casino con le sezioni !!!!
  UniMainModule.cod_cliente_preparazione := ASelectedItem;

  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubAgenzia.AsString := '';
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger := 0;
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoterSigla.AsString := '';
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger := 0;

  UniMainModule.DMDatiTempPolizza.CopiaDatiContraente(ASelectedItem);
  ProtectDatiContraente;
  if UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger > 0 then
  begin
    UniMainModule.DMdatiAgeCollaboratori.Qpromoter.Open; // EseguiQuery(0);
    SetCollaboratore(UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger, '');
  end;
  if UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger > 0 then
    SetSubCollaboratore(UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger, '');

  // utilizzato per verificare se le memtable fanno casino con le sezioni !!!!
  if UniMainModule.cod_cliente_preparazione <> UniMainModule.DMDatiTempPolizza.fdmtblPolizzaRifCodCli.AsInteger then
  begin
    ShowMessage('Problema con i codici di riferimento dei clienti.');
    UniMainModule.comunicaDIR('###', 'errore di cod_cliente in memTable ' +
      IntToStr(UniMainModule.cod_cliente_preparazione) + ' ' + UniMainModule.DMDatiTempPolizza.fdmtblPolizzaAgenzia.
      AsString, '', '', '', '', '##P', '', date);
    exit;
  end;
  if UniMainModule.DMdatiAge.SLPdati_age.agenzia <> UniMainModule.DMDatiTempPolizza.fdmtblPolizzaAgenzia.AsString then
  begin
    ShowMessage('Problema con i codici di riferimento dei clienti.');
    UniMainModule.comunicaDIR('###', 'errore di cod_cliente in memTable ' +
      IntToStr(UniMainModule.cod_cliente_preparazione) + ' ' + UniMainModule.DMDatiTempPolizza.fdmtblPolizzaAgenzia.
      AsString, '', '', '', '', '##P', '', date);
    exit;
  end;

end;

procedure TFStampaBasePolizza.SetCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
var
  resetSub: Boolean;
begin
  if not(dsoEdit.DataSet.State in [dsEdit, dsInsert]) then
    dsoEdit.DataSet.Edit;
  // dsoEdit.DataSet.FieldByName('PROMOTER').AsInteger := ASelectedItem;

  UniMainModule.DMdatiAgeCollaboratori.PosizionaQuery(ASelectedItem);

  resetSub := UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger <>
    UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('cod_produttore').AsInteger;
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubAgenzia.AsString :=
    UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('SIGLA').AsString;
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaFatta_da.AsInteger :=
    UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('cod_produttore').AsInteger;
  dbedtSubAge.Text := UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('SIGLA').AsString;
  // ASelectedDescription;
  if resetSub then
  begin
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoterSigla.AsString := '';
    dbedtSubPromoter.Text := '';
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.Clear;
  end;

end;

procedure TFStampaBasePolizza.SetIntermediataDa(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  if not(dsoEdit.DataSet.State in [dsEdit, dsInsert]) then
    dsoEdit.DataSet.Edit;
  UniMainModule.DMdatiAgeCollaboratori.PosizionaQuery(ASelectedItem);

  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaintermediataDaSigla.AsString :=
    UniMainModule.DMdatiAgeCollaboratori.Qpromoter.FieldByName('SIGLA').AsString;
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaIntermediataDa.AsInteger :=
    UniMainModule.DMdatiAgeCollaboratori.Qpromoter.FieldByName('cod_produttore').AsInteger;
  dbedtIntermediataDaSigla.Text := UniMainModule.DMdatiAgeCollaboratori.Qpromoter.FieldByName('SIGLA').AsString;
  // ASelectedDescription;
end;

procedure TFStampaBasePolizza.SetSubCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  if not(dsoEdit.DataSet.State in [dsEdit, dsInsert]) then
    dsoEdit.DataSet.Edit;
  UniMainModule.DMdatiAgeCollaboratori.PosizionaQuery(ASelectedItem);

  // UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoterSigla.AsString :=
  // UniMainModule.DMdatiAgeCollaboratori.Qpromoter.FieldByName('SIGLA').AsString;
  // UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger :=
  // UniMainModule.DMdatiAgeCollaboratori.Qpromoter.FieldByName('cod_produttore').AsInteger;
  // dbedtSubPromoter.Text := UniMainModule.DMdatiAgeCollaboratori.Qpromoter.FieldByName('SIGLA').AsString; //ASelectedDescription;

  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoterSigla.AsString :=
    UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('SIGLA').AsString;
  UniMainModule.DMDatiTempPolizza.fdmtblPolizzaSubPromoter.AsInteger :=
    UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('cod_produttore').AsInteger;
  dbedtSubPromoter.Text := UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('SIGLA').AsString;
  // ASelectedDescription;

end;

procedure TFStampaBasePolizza.SetTipoDocumentoAttuale(const Value: TTipoSalvataggio);
begin
  FTipoDocumentoAttuale := Value;
end;


procedure TFStampaBasePolizza.svuota(datiPS: TDatiPolizzaSostituita);
begin
    datiPS.numeroPolizzaSostituita:='';
    datiPS.nuovaDataEffetto:= 0.0  ;
    datiPS.percentualeTasse:= 0;
    datiPS.ScadenzaUltimoPagamentoDa := 0.0;
    datiPS.ScadenzaUltimoPagamentoA:= 0.0;
    datiPS.lastPremio:=0;
    datiPS.scadenza:= 0.0;
    datiPS.decorrenza:= 0.0;
    datiPS.frazionamento:='';
    datiPS.premioDaRimborsare:=0;
end;


procedure TFStampaBasePolizza.undbrdgrpProvvigClick(Sender: TObject);
begin
   if undbrdgrpProvvig.ItemIndex=1 then
      if not UniMainModule.DMdatiAge.find_durata(UniMainModule.DMDatiTempPolizza.fdmtblPolizzaDurataAnni.AsInteger,
                                          UniMainModule.DMDatiTempPolizza.fdmtblPolizzaProvvig.AsString ) then
      begin
         undbrdgrpProvvig.ItemIndex:=0;
         UniMainModule.DMDatiTempPolizza.fdmtblPolizzaProvvig.AsString:='R';
      end;
end;


procedure TFStampaBasePolizza.mostra_print_house(npol: string);
begin
  // visualizza la polizza corrente se esiste nel printhouse
  if UniMainModule.VerificaAutorizzazione('STP', 'VIS') then // and (UniCBstatoPolizze.ItemIndex<>5) then
  begin
     if UniMainModule.IsUtentePromoter then
        TFPrintHouseSLP.ShowPrintHousePolizza(uniApplication, npol, True, false)
     else
        TFPrintHouseSLP.ShowPrintHousePolizza(uniApplication, npol, True, true);
  end;
end;

end.
