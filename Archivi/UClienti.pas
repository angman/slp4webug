unit UClienti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, Vcl.Menus,
  uniMainMenu, uniBasicGrid, uniDBGrid, uniCheckBox, uniEdit, uniMultiItem, uniComboBox, uniPanel,
  uniLabel, uniButton, uniBitBtn, uniSpeedButton, uniGUIBaseClasses, UdmdatiAge, uniGUIDialogs, UDMMaster,
  uniSplitter;

type
  TFClienti = class(TFMasterElenco)
    unedtNome: TUniEdit;
    unedtSiglaCollab: TUniEdit;
    unedtCitta: TUniEdit;
    btnCancellaCliente: TUniSpeedButton;
    actCancellaCliente: TAction;
    actVisualizza: TAction;
    pnlContratti: TUniPanel;
    UniDBGrid1: TUniDBGrid;
    UniSplitter1: TUniSplitter;
    dsGridpolizze: TDataSource;
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoSelectionChange(Sender: TObject);
    procedure actCancellaClienteExecute(Sender: TObject);
    procedure actVisualizzaExecute(Sender: TObject);
    procedure grdElencoBodyDblClick(Sender: TObject);
    procedure actModifyExecute(Sender: TObject);
  private
    { Private declarations }
    procedure CreateAndShowEditForm;
  public
    { Public declarations }
    procedure ImpostaDmMaster(ADMMaster: TDMMaster);
  end;

function FClienti: TFClienti;

implementation

{$R *.dfm}

uses
  uniGUIApplication, MainModule, ServerModule, UQueryElencoIntf, libreria, dbisamtb, UCodiciErroriPolizza,
  UQueryEditIntf, Main, UDMdatiAgeClienti, UClienteEdit;

function FClienti: TFClienti;
begin
  Result := TFClienti(UniMainModule.GetFormInstance(TFClienti));
end;

procedure TFClienti.actCancellaClienteExecute(Sender: TObject);
begin
  if not UniMainModule.DMDatiAgeClienti.canDeleteCli(SelectedCode) then
    ShowMessage(MSG_CLI_NOT_ERASABLE)
  else
    MessageDlg('Conferma la cancellazione del cliente?', mtConfirmation, mbyesno,
      procedure(Sender: TComponent; Res: Integer)
      begin
        if Res = mrYes then
          UniMainModule.DMDatiAgeClienti.CancellaCliente(SelectedCode);
      end);

end;

procedure TFClienti.actModifyExecute(Sender: TObject);
begin
  if Assigned(EditPageClass) then
  begin
    UniMainModule.operazione := 'MOD';
    (DMMaster as IQueryEdit).PosizionaQuery(SelectedCode);
    CreateAndShowEditForm;
  end;
end;

procedure TFClienti.actVisualizzaExecute(Sender: TObject);
begin
  if Assigned(EditPageClass) then
  begin
    UniMainModule.operazione := 'VIS';
    (DMMaster as IQueryEdit).PosizionaQuery(SelectedCode);
    with (EditPageClass.Create(UniApplication) as TFClienteEdit) do
    begin
      QueryEditCliente := (Self.DMMaster as TDMdatiAgeClienti).QeditCliente;
      DSoEdit.DataSet.Edit;
      DatasetElenco := grdElenco.DataSource.DataSet;
      ShowModal;
    end;
  end;
end;

procedure TFClienti.CreateAndShowEditForm;
begin
  with (EditPageClass.Create(UniApplication) as TFClienteEdit) do
  begin
    try
      QueryEditCliente := (Self.DMMaster as TDMdatiAgeClienti).QeditCliente;
      DSoEdit.DataSet.Edit;
      DatasetElenco := grdElenco.DataSource.DataSet;
      dsTelefoniMail.DataSet := (Self.DMMaster as TDMdatiAgeClienti).QTelefoniMail;
      dsDocuments.DataSet := (Self.DMMaster as TDMdatiAgeClienti).QDocuments;
      dsContattiCliente.DataSet := (Self.DMMaster as TDMdatiAgeClienti).QContattoCliente;
      ShowModal;
    except
      on e: Exception do
        if EDBISAMEngineError(e).ErrorCode = 10258 then
        begin
          ShowMessage(MSG_RECORD_ALREADY_LOCKED);
          Close;
        end;

    end;
  end;
end;

procedure TFClienti.grdElencoBodyDblClick(Sender: TObject);
begin
  if Assigned(OnSelectedItem) then
    actSelezionaDaElenco.Execute
  else
    actVisualizza.Execute;
end;

procedure TFClienti.grdElencoSelectionChange(Sender: TObject);
begin
  SelectedCode        := dsGrid.DataSet.fieldbyname('COD_CLIENTE').asinteger;
  SelectedDescription := dsGrid.DataSet.fieldbyname('DENOMINAZ').AsString;
  // btnChiudi.Enabled := UniMainModule.DMdatiAgeClienti.canDeleteCli(SelectedCode);
end;

procedure TFClienti.ImpostaDmMaster(ADMMaster: TDMMaster);
begin
  DMMaster       := (ADMMaster as IQueryElenco);
  dsGrid.DataSet := (ADMMaster as TDMdatiAgeClienti).Qclienti;
  DMMaster.EseguiQuery('DENOMINAZ');
end;

procedure TFClienti.UniFormCreate(Sender: TObject);
begin
  UniMainModule.archivio := 'CLI';
  inherited UniFormCreate(Sender);

  self.DMMaster  := (UniMainModule.DMDatiAgeClienti as IQueryElenco);
  dsGrid.DataSet := UniMainModule.DMDatiAgeClienti.Qclienti;

  // log('UClienti Thread_id: ' + TThread.CurrentThread.ThreadID.ToString + ' sessione_agenzia: ' + UniMainModule.DMdatiAge.SessionAgenzia.SessionName + ' age:' + UniMainModule.DMdatiAge.SLPdati_age.agenzia);
  // log('UClienti Thread_id: ' + TThread.CurrentThread.ThreadID.ToString + ' QClienti Sessionename: '+ (dsGrid.dataset as TDBISAMQuery).sessionname + ' age:' +UniMainModule.DMdatiAge.SLPdati_age.agenzia);

  self.DMMaster.EseguiQuery('DENOMINAZ');
  dsGrid.DataSet.Filtered := False;
  dsGrid.DataSet.Filter   := '';

  EditPageClassName := 'TFClienteEdit';
  // WindowState       := wsMaximized;
  btnChiudi.Action             := actCancellaCliente;
  btnCancellaCliente.Action    := actChiudi;
  actSelezionaDaElenco.Visible := False;
  actChiudi.Visible            := True;
  btnVisualizza.Visible        := True;

  UniMainModule.traccia('CLIENTI',0,'','Ingresso CLI '+DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente);

  // if UniMainModule.DMdatiAge.is_utente_promoter then
  // begin
  // lblCollaboratori.Visible := false;
  // CBcollaboratori.Visible  := false;
  // end
  // else
  // UniMainModule.DMdatiAge.carica_promoter(CBcollaboratori.Items);
end;

end.
