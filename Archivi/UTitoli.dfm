inherited FTitoli: TFTitoli
  ClientHeight = 486
  ClientWidth = 984
  Caption = 'Gestione Titoli'
  ExplicitWidth = 1000
  ExplicitHeight = 525
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 984
    ExplicitWidth = 984
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 984
    Height = 461
    ExplicitWidth = 1045
    ExplicitHeight = 461
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 984
      Layout = 'hbox'
      ExplicitWidth = 984
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 984
        Constraints.MaxWidth = 1000
        Constraints.MinWidth = 500
        ExplicitWidth = 984
        inherited btnInsert: TUniSpeedButton
          Left = 3
          Top = 2
          TabOrder = 0
          ExplicitLeft = 3
          ExplicitTop = 2
        end
        inherited btnModify: TUniSpeedButton
          Left = 115
          Top = -1
          TabOrder = 1
          ExplicitLeft = 115
          ExplicitTop = -1
        end
        object btnIncassa: TUniSpeedButton [2]
          Left = 227
          Top = 2
          Width = 106
          Height = 43
          Action = actIncassa
          ParentFont = False
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 2
        end
        inherited btnVisualizza: TUniSpeedButton
          TabOrder = 9
        end
        object btnChiusContab: TUniSpeedButton [4]
          Left = 339
          Top = 2
          Width = 106
          Height = 43
          Action = actChiusuraContabile
          ParentFont = False
          ParentColor = False
          Color = clWindow
          ScreenMask.Enabled = True
          ScreenMask.Message = 'Attendere, chiusura contabile in corso ...'
          LayoutConfig.Flex = 1
          TabOrder = 3
        end
        object btnCancella: TUniSpeedButton [5]
          Left = 459
          Top = 2
          Width = 106
          Height = 43
          Action = actCancella
          ParentFont = False
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 4
        end
        inherited btnSeleziona: TUniSpeedButton
          Left = 579
          Top = 2
          TabOrder = 5
          ExplicitLeft = 579
          ExplicitTop = 2
        end
        object UniSpeedButton1: TUniSpeedButton [7]
          Left = 707
          Top = 1
          Width = 106
          Height = 43
          Action = actStampaQuietanza
          ParentFont = False
          Font.Color = clBlack
          ParentColor = False
          Color = clWindow
          ScreenMask.Enabled = True
          ScreenMask.Message = 'Attendere ...'
          ScreenMask.Target = Owner
          LayoutConfig.Flex = 1
          TabOrder = 6
        end
        inherited btnChiudi: TUniSpeedButton
          Left = 819
          Top = 2
          TabOrder = 8
          ExplicitLeft = 819
          ExplicitTop = 2
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 984
        ExplicitWidth = 984
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 984
      Height = 401
      ExplicitWidth = 984
      ExplicitHeight = 401
      inherited grdElenco: TUniDBGrid
        Top = 34
        Width = 984
        Height = 367
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgFilterClearButton, dgAutoRefreshRow, dgRowNumbers]
        OnMouseDown = grdElencoMouseDown
        OnSelectionChange = grdElencoSelectionChange
        OnDrawColumnCell = grdElencoDrawColumnCell
        Columns = <
          item
            FieldName = 'CompagniaLkup'
            Filtering.Enabled = True
            Title.Caption = 'Comp.'
            Width = 53
          end
          item
            FieldName = 'stato'
            Title.Caption = 'St. pag.'
            Width = 50
          end
          item
            FieldName = 'data_pag'
            Title.Caption = 'Data pag.'
            Width = 64
          end
          item
            FieldName = 'siglaSubAge'
            Title.Caption = 'S.Age'
            Width = 40
          end
          item
            FieldName = 'siglaSubColl'
            Title.Caption = 'S.Coll'
            Width = 40
          end
          item
            FieldName = 'tipoTitoloLkup'
            Title.Caption = 'Tipo titolo'
            Width = 150
          end
          item
            FieldName = 'n_polizza'
            Filtering.Enabled = True
            Filtering.Editor = unmbrdtNPolizza
            Filtering.ChangeDelay = 500
            Title.Caption = 'N. polizza'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'denominaz'
            Filtering.Enabled = True
            Filtering.Editor = unedtContraente
            Filtering.ChangeDelay = 500
            Title.Caption = 'Contraente'
            Width = 260
            Sortable = True
          end
          item
            FieldName = 'decorrenza'
            Title.Caption = 'Decorrenza'
            Width = 70
            Sortable = True
          end
          item
            FieldName = 'lordo'
            Title.Caption = 'Lordo'
            Width = 64
            Alignment = taRightJustify
          end
          item
            FieldName = 'provvigioni'
            Title.Caption = 'Provvigioni'
            Width = 64
            Alignment = taRightJustify
          end
          item
            FieldName = 'frazionamentoLkup'
            Title.Caption = 'Frazionam'
            Width = 64
          end
          item
            FieldName = 'tipo_polizza'
            Title.Caption = 'Tipo polizza'
            Width = 150
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 241
        Top = 62
        ExplicitLeft = 241
        ExplicitTop = 62
        object unmbrdtNPolizza: TUniNumberEdit
          Left = 40
          Top = 48
          Width = 145
          Hint = ''
          TabOrder = 1
          DecimalSeparator = ','
        end
        object unedtContraente: TUniEdit
          Left = 40
          Top = 76
          Width = 145
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTCONTRAENTE'
          TabOrder = 2
        end
        object cbbCompagnia: TUniDBLookupComboBox
          Left = 40
          Top = 20
          Width = 145
          Hint = ''
          ListField = 'DesTipoEmiss'
          ListSource = DMdatiAge.dsTipoEmiss
          KeyField = 'CodCompagnia'
          ListFieldIndex = 0
          TabOrder = 3
          Color = clWindow
        end
      end
      object pnlFilters: TUniSimplePanel
        Left = 0
        Top = 0
        Width = 984
        Height = 34
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 3
        DesignSize = (
          984
          34)
        object cbbStatoPagamento: TUniComboBox
          Left = 21
          Top = 8
          Width = 200
          Hint = ''
          Style = csDropDownList
          Text = ''
          Items.Strings = (
            'Pagati'
            'Pagati oggi'
            'Insoluti'
            'Insoluti da 30 giorni'
            'Insoluti da 45 giorni')
          TabOrder = 1
          EmptyText = 'Tutti'
          ClearButton = True
          FieldLabel = 'Stato pag.'
          FieldLabelWidth = 65
          IconItems = <>
          OnSelect = cbbStatoPagamentoSelect
        end
        object cbbCompContratti: TUniDBLookupComboBox
          Left = 408
          Top = 8
          Width = 305
          Hint = ''
          ListField = 'NOME'
          ListSource = DMdatiAge.DSCompagnie
          KeyField = 'COD_COMPAGNIA'
          ListFieldIndex = 0
          ClearButton = True
          Anchors = [akLeft, akTop, akBottom]
          TabOrder = 2
          Color = clWindow
          FieldLabel = 'Compagnia'
          FieldLabelWidth = 65
          ForceSelection = True
          OnChange = cbbCompContrattiChange
        end
        object UniCBcoloraInsoluti: TUniCheckBox
          Left = 728
          Top = 10
          Width = 97
          Height = 17
          Hint = ''
          Checked = True
          Caption = 'Col. insoluti'
          TabOrder = 3
          OnChange = UniCBcoloraInsolutiChange
        end
        object UniCBtipo_tit: TUniComboBox
          Left = 241
          Top = 8
          Width = 152
          Hint = ''
          Style = csDropDownList
          Text = ''
          Items.Strings = (
            '20 Quiet.'
            '10 Polizza'
            '11 Pol.Sost.'
            '12 App.'
            '15 App.Incr.'
            '50 Rilievo')
          TabOrder = 4
          ClearButton = True
          FieldLabel = 'Tipo tit.'
          FieldLabelWidth = 50
          IconItems = <>
          OnSelect = UniCBtipo_titSelect
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    object actIncassa: TAction
      Category = 'OperazioniTitoli'
      Caption = 'Incassa'
      OnExecute = actIncassaExecute
    end
    object actChiusuraContabile: TAction
      Category = 'OperazioniTitoli'
      Caption = 'Chius. Contabile'
      OnExecute = actChiusuraContabileExecute
    end
    object actFCProvvisorio: TAction
      Category = 'OperazioniTitoli'
      Caption = 'FC Provvisorio'
      OnExecute = actFCProvvisorioExecute
    end
    object actCancella: TAction
      Category = 'OperazioniTitoli'
      Caption = 'Cancella'
      OnExecute = actCancellaExecute
    end
    object actStampaQuietanza: TAction
      Category = 'OperazioniTitoli'
      Caption = 'Stampa quiet.'
      OnExecute = actStampaQuietanzaExecute
    end
  end
  inherited dsGrid: TDataSource
    Left = 664
  end
  object UniPopupMenu1: TUniPopupMenu
    Left = 544
    Top = 261
    object Annotazionisullaquietanza1: TUniMenuItem
      Caption = 'Annotazioni sulla quietanza'
      OnClick = Annotazionisullaquietanza1Click
    end
  end
  object UniScreenMask1: TUniScreenMask
    AttachedControl = btnChiusContab
    Enabled = True
    DisplayMessage = 'Attendere ....'
    TargetControl = Owner
    Left = 648
    Top = 333
  end
end
