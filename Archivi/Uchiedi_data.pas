unit Uchiedi_data;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  uniButton, uniBitBtn, uniDateTimePicker, uniLabel, uniGUIApplication, UDMAgeCollaboratori,
  Data.DB, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, UDMdatiAgeTitoli, UDMDatiAgePolizzePerf,
  System.StrUtils, uniEdit, uniGUIDialogs;

type
{$MESSAGE WARN 'Sarebbe meglio usare un nome pi� esplicativo'}
  TFchiedi_data = class(TUniForm)
    Babbandona: TUniBitBtn;
    Bconferma: TUniBitBtn;
    EuniData: TUniDateTimePicker;
    UniLabel1: TUniLabel;
    Ltitolo: TUniLabel;
    procedure BconfermaClick(Sender: TObject);
    procedure BabbandonaClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    class procedure Showchiedi_data(UniApplication: TUniGUIApplication; titolo: string;
      CallBackProc: TUniDialogCallBackProc);
  end;

function Fchiedi_data: TFchiedi_data;

implementation

{$R *.dfm}

uses
  MainModule, libSLP;

function Fchiedi_data: TFchiedi_data;
begin
  Result := TFchiedi_data(UniMainModule.GetFormInstance(TFchiedi_data));
end;

class procedure TFchiedi_data.Showchiedi_data(UniApplication: TUniGUIApplication; titolo: string;
  CallBackProc: TUniDialogCallBackProc);
begin
  with TFchiedi_data.Create(UniApplication) do
  begin
    if titolo > '' then
      Ltitolo.Caption := titolo
    else
      Ltitolo.Caption := 'Inserire una data';

    ShowModal(CallBackProc);
  end;
end;

procedure TFchiedi_data.UniFormCreate(Sender: TObject);
begin
  EuniData.DateTime := date;
end;

procedure TFchiedi_data.BabbandonaClick(Sender: TObject);
begin
  ModalResult := mrNo;
  Close;
end;

procedure TFchiedi_data.BconfermaClick(Sender: TObject);
begin
  // apera da eseguire in caso di conferma
  UniMainModule.dataChiediData := EuniData.DateTime;
  ModalResult                  := mrOk;
  Close;
end;

end.
