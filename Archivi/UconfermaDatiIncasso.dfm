object FconfermaDatiIncasso: TFconfermaDatiIncasso
  Left = 0
  Top = 0
  ClientHeight = 287
  ClientWidth = 453
  Caption = 'Conferma dati incasso'
  BorderStyle = bsNoneSizeable
  OldCreateOrder = False
  BorderIcons = [biSystemMenu]
  MonitoredKeys.Keys = <>
  OnCreate = UniFormCreate
  DesignSize = (
    453
    287)
  PixelsPerInch = 96
  TextHeight = 13
  object Babbandona: TUniBitBtn
    Left = 318
    Top = 244
    Width = 127
    Height = 35
    Hint = ''
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF404040606060808080808080808080606060404040FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF20202020206020209F0000BF00
      00BF0000BF20209F404080808080606060202020FFFFFFFFFFFFFFFFFFFFFFFF
      00002020209F0000FF0000FF0000FF0000FF0000FF0000FF0000FF20209F6060
      80808080202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
      00FF0000FF0000FF0000FF0000FF0000DF606080606060FFFFFFFFFFFF00007F
      0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
      FF20209F8080804040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
      00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF40408060606000007F0000FF
      0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
      FF0000FF20209F8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
      FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF8080800000BF0000FF
      0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000
      FF0000FF0000BF8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
      FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF60606000007F0000FF
      0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
      FF0000FF20209F4040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
      00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF202060FFFFFFFFFFFF00007F
      0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
      FF20209F202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
      00FF0000FF0000FF0000FF0000FF0000DF000020FFFFFFFFFFFFFFFFFFFFFFFF
      00002000007F0000FF0000FF0000FF0000FF0000FF0000FF0000FF00007F0000
      20FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00004000007F0000BF00
      00BF0000BF00007F000040FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    Caption = ' Abbandona'
    ModalResult = 7
    Anchors = [akRight, akBottom]
    ParentFont = False
    Font.Height = -13
    Font.Style = [fsBold]
    TabOrder = 0
    OnClick = BabbandonaClick
  end
  object Bconferma: TUniBitBtn
    Left = 8
    Top = 244
    Width = 127
    Height = 35
    Hint = ''
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000202020404040
      4040404040404040404040404040404040404040404040404040404040404040
      404040404040402020207F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0909090808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0309030008000608060808080B0
      B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      30CF30008000008000008000608060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800000800000800060
      8060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      00BF00008000008000008000008000008000608060909090C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800020DF20009F0000
      8000208020808080A0A0A0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      90CF9000BF0000800080808030EF30009F00008000208020808080B0B0B0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C090CF9000DF00A0A0A0C0C0C030
      EF30009F00008000608060808080B0B0B0C0C0C0C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F000080006080609090
      90C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C030EF30009F00208020808080A0A0A0C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F002080
      20808080C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C030EF30009F00608060C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C060DF
      6090B090C0C0C04040403F3F3F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
      7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F202020}
    Caption = ' Conferma'
    ModalResult = 1
    Anchors = [akLeft, akBottom]
    ParentFont = False
    Font.Height = -13
    Font.Style = [fsBold]
    TabOrder = 1
    OnClick = BconfermaClick
  end
  object pcIncasso: TUniPageControl
    Left = 8
    Top = 8
    Width = 437
    Height = 230
    Hint = ''
    ActivePage = tsDatiIncasso
    TabOrder = 2
    object tsDatiIncasso: TUniTabSheet
      Hint = ''
      Caption = 'DATI INCASSO'
      Font.Height = -13
      ParentFont = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object UniLabel1: TUniLabel
        Left = 14
        Top = 24
        Width = 109
        Height = 19
        Hint = ''
        Caption = 'Data incasso:'
        ParentFont = False
        Font.Height = -16
        Font.Style = [fsBold]
        TabOrder = 0
      end
      object EuniDataPag: TUniDateTimePicker
        Left = 137
        Top = 18
        Width = 128
        Height = 33
        Hint = ''
        DateTime = 43786.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        ReadOnly = True
        TabOrder = 1
        ParentFont = False
        Font.Height = -16
        Font.Style = [fsBold]
      end
      object cbbPromoter: TUniDBLookupComboBox
        Left = 14
        Top = 72
        Width = 323
        Height = 24
        Hint = ''
        ListField = 'NOME'
        ListSource = dsCollaboratore
        KeyField = 'COD_PRODUTTORE'
        ListFieldIndex = 0
        ParentFont = False
        Font.Height = -13
        TabOrder = 2
        Color = clWindow
        FieldLabel = 'Incassato da'
      end
      object undtmpckrDataValuta: TUniDateTimePicker
        Left = 14
        Top = 128
        Width = 251
        Hint = ''
        DateTime = 43804.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 3
        FieldLabel = 'Data del CCP / Valuta bonifico:'
      end
    end
    object tsEstremiPagamento: TUniTabSheet
      Hint = ''
      Caption = 'ESTREMI PAGAMENTO'
      Font.Height = -13
      ParentFont = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object cbbTipoPagamento: TUniDBLookupComboBox
        Left = 3
        Top = 3
        Width = 422
        Height = 24
        Hint = ''
        ListField = 'codice_pag;descrizione'
        ListSource = dsTipoPagamento
        KeyField = 'codice_pag'
        ListFieldIndex = 0
        TabOrder = 0
        Color = clWindow
        FieldLabel = 'Tipo pagamento'
      end
      object unmbrdtImportoPagato: TUniNumberEdit
        Left = 3
        Top = 33
        Width = 214
        Hint = ''
        Alignment = taRightJustify
        TabOrder = 1
        FieldLabel = 'Importo pagato'
        DecimalSeparator = ','
      end
      object undtmpckrDataValutaPag: TUniDateTimePicker
        Left = 3
        Top = 61
        Width = 214
        Hint = ''
        DateTime = 44153.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 2
        FieldLabel = 'Data valuta'
      end
      object undtmpckrDataMovimento: TUniDateTimePicker
        Left = 248
        Top = 61
        Width = 177
        Hint = ''
        DateTime = 44153.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 3
        FieldLabel = 'Data Movim.'
        FieldLabelWidth = 80
      end
      object unedtDescrizioneIncasso: TUniEdit
        Left = 3
        Top = 89
        Width = 422
        Hint = ''
        CharCase = ecUpperCase
        Text = ''
        TabOrder = 4
        FieldLabel = 'Descrizione'
      end
      object unedtPagEseguitoDa: TUniEdit
        Left = 3
        Top = 117
        Width = 376
        Hint = ''
        CharCase = ecUpperCase
        Text = ''
        TabOrder = 5
        FieldLabel = 'Pag. eseguito da'
      end
      object unedtEstremiPagamento: TUniEdit
        Left = 3
        Top = 145
        Width = 422
        Hint = ''
        CharCase = ecUpperCase
        Text = ''
        TabOrder = 7
        FieldLabel = 'Estremi pag.'
      end
      object unedtBancaPagamento: TUniEdit
        Left = 3
        Top = 173
        Width = 422
        Hint = ''
        CharCase = ecUpperCase
        Text = ''
        TabOrder = 8
        FieldLabel = 'Banca'
      end
      object btnContraente: TUniButton
        Left = 380
        Top = 116
        Width = 45
        Height = 25
        Hint = ''
        Caption = 'Contr.'
        TabOrder = 6
        OnClick = btnContraenteClick
      end
    end
  end
  object dsCollaboratore: TDataSource
    DataSet = DMAgeCollaboratori.Qpromoter
    Left = 316
  end
  object dsTipoPagamento: TDataSource
    DataSet = DMdatiAgeTitoli.Qtipo_pagamenti
    Left = 404
  end
end
