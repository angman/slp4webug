inherited FperfezionaPolizze: TFperfezionaPolizze
  ClientHeight = 497
  ClientWidth = 1131
  Caption = 'Gestione polizze da perfezionare'
  OnClose = UniFormClose
  ExplicitWidth = 1147
  ExplicitHeight = 536
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 1131
    ExplicitWidth = 1131
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 1131
    Height = 472
    ExplicitWidth = 1284
    ExplicitHeight = 472
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 1131
      Height = 113
      ExplicitWidth = 1131
      ExplicitHeight = 113
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 1131
        Height = 93
        Constraints.MaxWidth = 900
        Layout = 'auto'
        ExplicitWidth = 1131
        ExplicitHeight = 93
        inherited btnInsert: TUniSpeedButton
          Action = actPerfezionaPolizza
          ScreenMask.Enabled = True
        end
        inherited btnModify: TUniSpeedButton
          Action = actCancellaPolizza
        end
        inherited btnSeleziona: TUniSpeedButton
          Action = actVisualizza
        end
        object UniCBstatoPolizze: TUniComboBox
          Left = 629
          Top = 6
          Width = 268
          Height = 29
          Hint = ''
          Text = 'UniCBstatoPolizze'
          Items.Strings = (
            'Da Perfezionare'
            'Perfezionate'
            'Rese'
            'Cancellate'
            'Emmese da Direz.'
            'Preventivi da Direz.')
          ParentFont = False
          Font.Height = -12
          TabOrder = 6
          FieldLabel = 'Stato Polizza'
          IconItems = <>
          OnChange = UniCBstatoPolizzeChange
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 1131
        ExplicitWidth = 1131
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Top = 113
      Width = 1131
      ExplicitTop = 113
      ExplicitWidth = 1131
      inherited grdElenco: TUniDBGrid
        Width = 1131
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgFilterClearButton, dgAutoRefreshRow, dgRowNumbers]
        Columns = <
          item
            FieldName = 'EmissLkup'
            Filtering.Enabled = True
            Filtering.Editor = cbbTipoEmiss
            Title.Caption = 'Emessa da'
            Width = 53
          end
          item
            FieldName = 'sub_age'
            Filtering.Enabled = True
            Filtering.Editor = unedtSubage
            Filtering.ChangeDelay = 500
            Title.Caption = 'S.Age'
            Width = 40
          end
          item
            FieldName = 'SiglaPromoterLkup'
            Filtering.Enabled = True
            Filtering.Editor = cbbPromoter
            Title.Caption = 'Promoter'
            Width = 200
            Visible = False
          end
          item
            FieldName = 'siglaSubPromoter'
            Title.Caption = 'S.coll'
            Width = 40
          end
          item
            FieldName = 'n_polizza'
            Filtering.Enabled = True
            Filtering.Editor = unmbrdtNPolizza
            Filtering.ChangeDelay = 500
            Title.Caption = 'N. polizza'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'contraente'
            Filtering.Enabled = True
            Filtering.Editor = unedtContraente
            Filtering.ChangeDelay = 500
            Title.Caption = 'Contraente'
            Width = 200
            Sortable = True
          end
          item
            FieldName = 'decorrenza'
            Title.Caption = 'Decorrenza'
            Width = 66
            Sortable = True
          end
          item
            FieldName = 'scadenza'
            Title.Caption = 'Scadenza'
            Width = 64
          end
          item
            FieldName = 'data_perfez'
            Title.Caption = 'Dt. perfez'
            Width = 64
          end
          item
            FieldName = 'totale1'
            Title.Caption = 'Incassato'
            Width = 64
          end
          item
            FieldName = 'totale2'
            Title.Caption = 'Rata succ.'
            Width = 64
          end
          item
            FieldName = 'data_emissione'
            Title.Caption = 'Dt. emiss'
            Width = 64
          end
          item
            FieldName = 'FrazionamentoLkup'
            Title.Caption = 'Fraz.'
            Width = 50
          end
          item
            FieldName = 'tipo_polizza'
            Title.Caption = 'T. polizza'
            Width = 200
          end
          item
            FieldName = 'provvigioni'
            Title.Caption = 'Provvig.'
            Width = 50
          end
          item
            FieldName = 'fatta_da'
            Title.Caption = 'fatta_da'
            Width = 64
            Visible = False
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 208
        Top = 70
        ExplicitLeft = 208
        ExplicitTop = 70
        object cbbTipoEmiss: TUniDBLookupComboBox
          Left = 40
          Top = 3
          Width = 145
          Hint = ''
          ListField = 'DesTipoEmiss'
          ListSource = DMdatiAge.dsTipoEmiss
          KeyField = 'CodEmmiss'
          ListFieldIndex = 0
          TabOrder = 1
          Color = clWindow
        end
        object cbbPromoter: TUniDBLookupComboBox
          Left = 40
          Top = 52
          Width = 145
          Hint = ''
          ListField = 'NOME'
          ListSource = DMdatiAge.DSSelPromoter
          KeyField = 'cod_produttore'
          ListFieldIndex = 0
          TabOrder = 2
          Color = clWindow
        end
        object cbbStatoPolizza: TUniDBLookupComboBox
          Left = 40
          Top = 148
          Width = 145
          Hint = ''
          Enabled = False
          ListField = 'DesStatoPolizza'
          KeyField = 'CodStatoPolizza'
          ListFieldIndex = 0
          TabOrder = 3
          Color = clWindow
        end
        object unmbrdtNPolizza: TUniNumberEdit
          Left = 40
          Top = 80
          Width = 145
          Hint = ''
          TabOrder = 4
          DecimalSeparator = ','
        end
        object unedtContraente: TUniEdit
          Left = 40
          Top = 108
          Width = 145
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTCONTRAENTE'
          TabOrder = 5
        end
        object cbbSubAge: TUniDBLookupComboBox
          Left = 40
          Top = 27
          Width = 145
          Hint = ''
          ListField = 'SIGLA'
          ListSource = DMdatiAge.DSSelPromoter
          KeyField = 'cod_produttore'
          ListFieldIndex = 0
          TabOrder = 6
          Color = clWindow
        end
        object unedtSubage: TUniEdit
          Left = 40
          Top = 184
          Width = 145
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTSUBAGE'
          TabOrder = 7
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    object actPerfezionaPolizza: TAction
      Category = 'OpPolizze'
      Caption = 'Perfeziona'
      OnExecute = actPerfezionaPolizzaExecute
    end
    object actCancellaPolizza: TAction
      Category = 'OpPolizze'
      Caption = 'Cancella'
      OnExecute = actCancellaPolizzaExecute
    end
    object actVisualizza: TAction
      Category = 'OpPolizze'
      Caption = 'Visualizza polizza'
      OnExecute = actVisualizzaExecute
    end
  end
  object UniScreenMask1: TUniScreenMask
    AttachedControl = btnInsert
    Enabled = True
    DisplayMessage = 'Attendere, perfezionamento in corso ...'
    TargetControl = Owner
    Left = 536
    Top = 333
  end
end
