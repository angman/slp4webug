unit URiprendiBozza;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniPanel,
  uniBasicGrid, uniDBGrid, uniButton, uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, UDMBozzePreventivi,
  uniEdit, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox;

type
  TFRiprendiBozza = class(TFMasterElenco)
    unedtContraente: TUniEdit;
    pnlFilters: TUniSimplePanel;
    cbbOperatore: TUniDBLookupComboBox;
    dsCollaboratori: TDataSource;
    actCancellaBozze: TAction;
    dsStatus: TDataSource;
    cbbDesStatus: TUniDBLookupComboBox;
    procedure grdElencoSelectionChange(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure cbbOperatoreChange(Sender: TObject);
    procedure grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn; const Value: Variant);
    procedure grdElencoClearFilters(Sender: TObject);
    procedure actCancellaBozzeExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FRiprendiBozza: TFRiprendiBozza;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UQueryElencoIntf;

function FRiprendiBozza: TFRiprendiBozza;
begin
  Result := TFRiprendiBozza(UniMainModule.GetFormInstance(TFRiprendiBozza));
end;

procedure TFRiprendiBozza.actCancellaBozzeExecute(Sender: TObject);
var
  i: Integer;
  codPolizzeArr: array of TBookmark;
begin
  if grdElenco.SelectedRows.Count > 0 then
  begin
    SetLength(codPolizzeArr, grdElenco.SelectedRows.Count);
    for i              := 0 to grdElenco.SelectedRows.Count - 1 do
      codPolizzeArr[i] := grdElenco.SelectedRows[i];
    UniMainModule.DMBozzePreventivi.CancellaBozzePreventivi(codPolizzeArr);
  end;
end;

procedure TFRiprendiBozza.cbbOperatoreChange(Sender: TObject);
begin
  if cbbOperatore.KeyValue > 0 then
    UniMainModule.DMBozzePreventivi.EseguiQuery('DT_PREVENTIVO', nil, '', 2, 0, cbbOperatore.KeyValue)
  else
    UniMainModule.DMBozzePreventivi.EseguiQuery('DT_PREVENTIVO', nil, '', 2, 0);

end;

procedure TFRiprendiBozza.grdElencoClearFilters(Sender: TObject);
begin
  inherited;
  actCancellaBozze.Enabled := False;
  // btnVisualizza.Enabled := False;
  grdElenco.Options := grdElenco.Options - [dgmultiselect];
  grdElenco.JSInterface.JSCall('getSelectionModel().deselectAll', []);
  grdElenco.JSInterface.JSCall('getSelectionModel().setSelectionMode', ['SINGLE']);
  cbbOperatoreChange(nil);
end;

procedure TFRiprendiBozza.grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn;
  const Value: Variant);
var
  ParamList: TStringList;
begin
  // if Value = '' then
  // begin
  // grdElencoClearFilters(nil);
  // Exit;
  // end;
  { TODO 1 -oAngelo -cCorrezione bugs : Inserire chiamata ad eseguiquery }
  ParamList := TStringList.Create;
  if Column.FieldName = 'descrStatus' then
    ParamList.Append('STATUS=' + UniMainModule.DMBozzePreventivi.getTipoDocFromDescr(Value));

  if Column.FieldName = 'CONTRAENTE' then
  begin
     inherited grdElencoColumnFilter(Sender, Column, value);
     exit;
  end;

  UniMainModule.DMBozzePreventivi.EseguiQuery('DT_PREVENTIVO', ParamList, '', 2);
  try
    actCancellaBozze.Enabled := (Value = 'PREVENTIVO') or (Value = 'BOZZA') or (Value = 'PREVENTIVO DIR');
    btnVisualizza.Enabled    := actCancellaBozze.Enabled;
    if actCancellaBozze.Enabled then
    begin
      // grdElenco.Options := grdElenco.Options + [dgmultiselect];
      grdElenco.JSInterface.JSCall('getSelectionModel().deselectAll', []);
      grdElenco.JSInterface.JSCall('getSelectionModel().setSelectionMode', ['MULTI']);
    end
    else
    begin
      // grdElenco.Options := grdElenco.Options - [dgmultiselect];
      grdElenco.JSInterface.JSCall('getSelectionModel().deselectAll', []);
      grdElenco.JSInterface.JSCall('getSelectionModel().setSelectionMode', ['SINGLE']);
    end;
  finally
    ParamList.Free;
  end;

end;

procedure TFRiprendiBozza.grdElencoSelectionChange(Sender: TObject);
begin
  SelectedCode        := dsGrid.dataset.fieldbyname('COD_POLIZZA').asinteger;
  SelectedDescription := dsGrid.dataset.fieldbyname('CONTRAENTE').AsString;

end;

procedure TFRiprendiBozza.UniFormCreate(Sender: TObject);
begin
  inherited;

  Self.DMMaster := (UniMainModule.DMBozzePreventivi as IQueryElenco);
  // DMMaster       := UniMainModule.DMBozzePreventivi;
  dsGrid.dataset := UniMainModule.DMBozzePreventivi.QBozzePreventivi;

  // UniMainModule.DMBozzePreventivi.EseguiQuery('DT_PREVENTIVO', nil, '', 2);
  Self.DMMaster.EseguiQuery('DT_PREVENTIVO', nil, '', 2);
  dsGrid.DataSet.Filtered := False;
  dsGrid.DataSet.Filter   := '';

  btnSeleziona.Enabled  := not dsGrid.dataset.IsEmpty;
  pnlFilters.Visible    := not UniMainModule.IsUtentePromoter;
  btnVisualizza.Action  := actCancellaBozze;
  btnVisualizza.Visible := True;
  btnVisualizza.Enabled := False;
  grdElenco.ClearFilters;
  if pnlFilters.Visible then
  begin
    dsCollaboratori.dataset := UniMainModule.DMdatiAgeCollaboratori.Qpromoter;
    UniMainModule.DMdatiAgeCollaboratori.Qpromoter.Open;
  end;
end;

end.
