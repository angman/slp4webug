inherited FStoricoIncassi: TFStoricoIncassi
  ClientWidth = 891
  Caption = 'Storico Incassi'
  OnClose = UniFormClose
  ExplicitWidth = 907
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 891
    ExplicitWidth = 891
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 891
    ExplicitWidth = 963
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 891
      ExplicitWidth = 891
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 891
        ExplicitWidth = 891
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 891
        ExplicitWidth = 891
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 891
      ExplicitWidth = 891
      inherited grdElenco: TUniDBGrid
        Width = 888
        Align = alLeft
        Columns = <
          item
            FieldName = 'codice_agenzia'
            Filtering.Enabled = True
            Filtering.Editor = unedtSiglaCompagnia
            Title.Caption = 'Comp.'
            Width = 45
            Sortable = True
          end
          item
            FieldName = 'n_polizza'
            Filtering.Enabled = True
            Filtering.Editor = unmbrdtFoglioCassa
            Title.Caption = 'N. polizza'
            Width = 80
            Sortable = True
          end
          item
            FieldName = 'decorrenza'
            Filtering.Enabled = True
            Title.Caption = 'decorrenza'
            Width = 90
            ReadOnly = True
          end
          item
            FieldName = 'scadenza'
            Title.Caption = 'scadenza'
            Width = 80
            Sortable = True
          end
          item
            FieldName = 'fraz'
            Title.Caption = 'Fraz.'
            Width = 30
          end
          item
            FieldName = 'data_pag'
            Title.Caption = 'Data Pag.'
            Width = 80
          end
          item
            FieldName = 'data_pag_ccp'
            Title.Caption = 'Dt.valuta'
            Width = 64
          end
          item
            FieldName = 'lordo'
            Title.Caption = 'Premio'
            Width = 70
          end
          item
            FieldName = 'n_foglio_cassa'
            Title.Caption = 'N. F. Cassa'
            Width = 80
          end
          item
            FieldName = 'denominaz'
            Title.Caption = 'Nome'
            Width = 180
          end
          item
            FieldName = 'siglasubage'
            Title.Caption = 'Sub.Age.'
            Width = 64
          end
          item
            FieldName = 'siglasubcoll'
            Title.Caption = 'Sub.Collab.'
            Width = 120
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 160
        Top = 62
        ExplicitLeft = 160
        ExplicitTop = 62
        object unedtSiglaCompagnia: TUniEdit
          Left = 48
          Top = 16
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTSIGLACOMPAGNIA'
          TabOrder = 1
        end
        object unmbrdtFoglioCassa: TUniNumberEdit
          Left = 48
          Top = 56
          Width = 121
          Hint = ''
          TabOrder = 2
          DecimalSeparator = ','
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    inherited actInsert: TAction
      Visible = False
    end
    inherited actSelezionaDaElenco: TAction
      Visible = False
    end
  end
end
