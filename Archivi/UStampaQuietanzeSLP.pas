unit UStampaQuietanzeSLP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniDateTimePicker, uniGUIBaseClasses, uniPanel, uniMultiItem, uniComboBox, uniButton,
  uniBitBtn, uniSpeedButton, uniRadioGroup, uniScreenMask, uniGUIApplication, uniGUIDialogs,
  uniLabel, uniEdit;

type
  TFStampaQuietanzeSLP = class(TUniForm)
    pnlData: TUniSimplePanel;
    dtPckDataQuietanze: TUniDateTimePicker;
    cbbOrdinamento: TUniComboBox;
    pnlButtons: TUniSimplePanel;
    btnEsci: TUniSpeedButton;
    btnStampaQuietanze: TUniSpeedButton;
    rgGrpTipoCopia: TUniRadioGroup;
    unscrnmsk: TUniScreenMask;
    UniEdit1: TUniEdit;
    UniLabel1: TUniLabel;
    btnSelezionaProduttore: TUniBitBtn;
    procedure btnEsciClick(Sender: TObject);
    procedure btnStampaQuietanzeClick(Sender: TObject);
    procedure btnSelezionaProduttoreClick(Sender: TObject);
    procedure UniEdit1DblClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    codCollaboratore: integer;
  public
    { Public declarations }
    procedure SetCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
  end;

implementation

uses
  MainModule, System.DateUtils, UDMDatiReport, UPolizzaExceptions, UCollaboratori;

{$R *.dfm}

procedure TFStampaQuietanzeSLP.btnEsciClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;


procedure TFStampaQuietanzeSLP.btnSelezionaProduttoreClick(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('COL', 'MOD', 'Agenti-Dipendenti-Collaboratori');
  with TFCollaboratori.Create(uniApplication, 0, 3) do
  begin
    OnSelectedItem := SetCollaboratore;
    ShowModal;
  end;
end;

procedure TFStampaQuietanzeSLP.SetCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
begin

  UniMainModule.DMdatiAgeCollaboratori.PosizionaQuery(ASelectedItem);

  UniEdit1.Text :=
    UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('SIGLA').AsString;
  codCollaboratore :=
    UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.FieldByName('cod_produttore').AsInteger;

end;


procedure TFStampaQuietanzeSLP.UniEdit1DblClick(Sender: TObject);
begin
   codCollaboratore:=0;
   UniEdit1.Text:='';
end;

procedure TFStampaQuietanzeSLP.UniFormCreate(Sender: TObject);
begin
   dtPckDataQuietanze.DateTime := Date;
end;

procedure TFStampaQuietanzeSLP.btnStampaQuietanzeClick(Sender: TObject);
var
  AnnoQuitetanze, MeseQuietanze: Integer;
  StampaQuietanze: TFStampaQuietanzeSLP;
begin

    try
      AnnoQuitetanze  := YearOf(dtPckDataQuietanze.DateTime);
      MeseQuietanze   := MonthOf(dtPckDataQuietanze.DateTime);
      try
        UniMainModule.DMDatiReport.stampaQuietanziamentiSLP(AnnoQuitetanze, MeseQuietanze,codCollaboratore,
          TipoOrdQuietanziamenti(cbbOrdinamento.ItemIndex),
          TipoCopieQuietanziamenti(rgGrpTipoCopia.ItemIndex));
        ModalResult := mrOk;

      except
        on E: ESLP4WebException do
          ShowMessage(E.Message);
      end;
    finally

    end;
end;


end.
