object FAvviso: TFAvviso
  Left = 0
  Top = 0
  ClientHeight = 396
  ClientWidth = 587
  Caption = 'Comunicazione dalla Direzione'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  Font.Height = -13
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object undbhtmlmAvvisoAge: TUniDBHTMLMemo
    Left = 0
    Top = 47
    Width = 587
    Height = 349
    Hint = ''
    ShowToolbar = False
    DataField = 'TESTO'
    DataSource = dsAvvisoAge
    ScrollBars = ssVertical
    ParentFont = False
    Font.Height = -16
    Align = alClient
    ReadOnly = True
    TabOrder = 0
    Color = clWindow
  end
  object pnlButtons: TUniSimplePanel
    Left = 0
    Top = 0
    Width = 587
    Height = 47
    Hint = ''
    ParentColor = False
    Color = clYellow
    Align = alTop
    TabOrder = 1
    object btnEsci: TUniSpeedButton
      Left = 12
      Top = 9
      Width = 93
      Height = 29
      Hint = ''
      Enabled = False
      Caption = 'Esci'
      ParentFont = False
      Font.Height = -13
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 1
      OnClick = btnEsciClick
    end
    object btnNextMessage: TUniSpeedButton
      Left = 104
      Top = 9
      Width = 119
      Height = 29
      Hint = ''
      Caption = 'Avviso Successivo'
      ParentFont = False
      Font.Height = -13
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 2
      OnClick = btnNextMessageClick
    end
    object btnPriorMessage: TUniSpeedButton
      Left = 221
      Top = 9
      Width = 124
      Height = 29
      Hint = ''
      Caption = 'Avviso preced.'
      ParentFont = False
      Font.Height = -13
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 3
      OnClick = btnPriorMessageClick
    end
  end
  object dsAvvisoAge: TDataSource
    Left = 416
    Top = 3
  end
end
