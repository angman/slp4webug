object FStampaQuietanzeSLP: TFStampaQuietanzeSLP
  Left = 0
  Top = 0
  ClientHeight = 330
  ClientWidth = 458
  Caption = 'Stampa quietanziamenti'
  OldCreateOrder = False
  Visible = True
  MonitoredKeys.Keys = <>
  ActiveControl = dtPckDataQuietanze
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlData: TUniSimplePanel
    Left = 0
    Top = 0
    Width = 458
    Height = 292
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 0
    object dtPckDataQuietanze: TUniDateTimePicker
      Left = 16
      Top = 24
      Width = 361
      Hint = ''
      DateFormat = 'MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 1
      FieldLabel = 'Mese ed anno delle quietanze'
    end
    object cbbOrdinamento: TUniComboBox
      Left = 16
      Top = 64
      Width = 361
      Hint = ''
      Style = csDropDownList
      Text = 'N'#176' Polizza'
      Items.Strings = (
        'N'#176' Polizza'
        'Contraente'
        'Collaboratore + n'#176' pol'
        'Collaboratore + contraent.'
        'Scadenza + Contraente'
        'Scadenza + N'#176' polizza')
      ItemIndex = 0
      TabOrder = 2
      FieldLabel = 'Ordinamento'
      IconItems = <>
      Mode = umNameValue
    end
    object rgGrpTipoCopia: TUniRadioGroup
      Left = 16
      Top = 104
      Width = 361
      Height = 89
      Hint = ''
      Items.Strings = (
        'Copia Contraente e Copia Agenzia su STESSA pagina'
        'Copia Contraente e Copia Agenzia su DUE pagina ')
      ItemIndex = 0
      Caption = 'Tipo Copia'
      TabOrder = 3
    end
    object UniEdit1: TUniEdit
      Left = 140
      Top = 209
      Width = 71
      Height = 30
      Hint = ''
      Text = ''
      TabOrder = 4
      ReadOnly = True
      OnDblClick = UniEdit1DblClick
    end
    object UniLabel1: TUniLabel
      Left = 24
      Top = 216
      Width = 110
      Height = 13
      Hint = ''
      Caption = 'Filtro sul collaboratore:'
      TabOrder = 5
    end
    object btnSelezionaProduttore: TUniBitBtn
      Left = 217
      Top = 209
      Width = 32
      Height = 30
      Hint = ''
      Caption = ''
      ParentFont = False
      TabOrder = 6
      IconAlign = iaCenter
      OnClick = btnSelezionaProduttoreClick
    end
  end
  object pnlButtons: TUniSimplePanel
    Left = 0
    Top = 292
    Width = 458
    Height = 38
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 1
    object btnEsci: TUniSpeedButton
      Left = 128
      Top = 5
      Width = 106
      Height = 29
      Hint = ''
      Caption = 'Esci'
      ParentFont = False
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 1
      OnClick = btnEsciClick
    end
    object btnStampaQuietanze: TUniSpeedButton
      Left = 16
      Top = 5
      Width = 106
      Height = 29
      Hint = ''
      Caption = 'St. quietanze'
      ParentFont = False
      ParentColor = False
      Color = clWindow
      ScreenMask.Enabled = True
      ScreenMask.Message = 'Attendere prego, creazione anteprima in corso'
      LayoutConfig.Flex = 1
      TabOrder = 0
      OnClick = btnStampaQuietanzeClick
    end
  end
  object unscrnmsk: TUniScreenMask
    Enabled = True
    DisplayMessage = 'Attendere, creazione anteprima in corso ...'
    TargetControl = Owner
    Left = 319
    Top = 199
  end
end
