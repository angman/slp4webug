unit UDatiPolizzaSostituita;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniMultiItem, uniComboBox, uniEdit, uniDateTimePicker, uniGUIBaseClasses, uniButton,
  uniBitBtn, uniGroupBox, uniGUIDialogs, uniGUIApplication, UDatiPolSostituitaRec;

type

  TFDatiPolizzaSostituita = class(TUniForm)
    btnRicalcoloPolSostBconferma: TUniBitBtn;
    btnRicalcoloPolSostBabbandona: TUniBitBtn;
    dtPckDecorrenzaPolizzaNew: TUniDateTimePicker;
    dtPckScadenzaPolizzaSost: TUniDateTimePicker;
    dtPckDecorrenzaPolizzaSost: TUniDateTimePicker;
    edtNPolSostituita: TUniEdit;
    cbbFrazionamento: TUniComboBox;
    nedtLastPremioPagato: TUniNumberEdit;
    nedtPercentualeTasse: TUniNumberEdit;
    dtPckPagDa: TUniDateTimePicker;
    dtPckPagA: TUniDateTimePicker;
    nedtGiorniRimborso: TUniNumberEdit;
    grpbxRimborsoContraente: TUniGroupBox;
    nedtNettoRimborsoContraente: TUniNumberEdit;
    nedtStornoProvvigionale: TUniNumberEdit;
    UniBitBtnCalcola: TUniBitBtn;
    procedure dtPckPagAChangeValue(Sender: TObject);
    procedure UniBitBtnCalcolaClick(Sender: TObject);
  private
    { Private declarations }
    lDatiPolizzaSostituita: TDatiPolizzaSostituita;

    procedure calcola;
  public
    { Public declarations }
    class procedure askDatiPolizzaSost(UniApplication: TUniGUIApplication;
      ADatiPolizzaSostituita: TDatiPolizzaSostituita; CallBackProc: TUniDialogCallBackProc);
  end;

implementation

uses
  libSLP, date360;

{$R *.dfm}
{ TFDatiPolizzaSosttuita }

class procedure TFDatiPolizzaSostituita.askDatiPolizzaSost(UniApplication: TUniGUIApplication;
  ADatiPolizzaSostituita: TDatiPolizzaSostituita; CallBackProc: TUniDialogCallBackProc);
begin
  with TFDatiPolizzaSostituita.Create(UniApplication) do
  begin
    lDatiPolizzaSostituita              := ADatiPolizzaSostituita;
    edtNPolSostituita.Text              := ADatiPolizzaSostituita.numeroPolizzaSostituita;
    dtPckDecorrenzaPolizzaNew.DateTime  := ADatiPolizzaSostituita.nuovaDataEffetto;
    cbbFrazionamento.ItemIndex          := dai_frazionamento(ADatiPolizzaSostituita.frazionamento) - 1;
    dtPckDecorrenzaPolizzaSost.DateTime := ADatiPolizzaSostituita.decorrenza;
    dtPckScadenzaPolizzaSost.DateTime   := ADatiPolizzaSostituita.scadenza;
    dtPckPagDa.DateTime                 := ADatiPolizzaSostituita.ScadenzaUltimoPagamentoDa;
    dtPckPagA.DateTime                  := ADatiPolizzaSostituita.ScadenzaUltimoPagamentoA;
    nedtLastPremioPagato.Value          := ADatiPolizzaSostituita.lastPremio;
    nedtPercentualeTasse.Value          := ADatiPolizzaSostituita.percentualeTasse;
    ShowModal(CallBackProc);
  end;

end;


procedure TFDatiPolizzaSostituita.calcola;
var
  diff, fraz: integer;
  rimb: currency;
begin
  // esegui il calcolo della cifra da rimborsare se esiste
  // 1) calcola la differenza tra scadenza dell'ultima rata pagata e decorrenza polizza nuova
  fraz := dai_frazionamento(cbbFrazionamento.Text);
  if dtPckPagA.DateTime > dtPckDecorrenzaPolizzaNew.DateTime then
  begin
    diff                                      := dist360(dtPckPagA.DateTime, dtPckDecorrenzaPolizzaNew.DateTime);
    nedtGiorniRimborso.value                  := diff;
    rimb                                      := nedtLastPremioPagato.value / (1 + (nedtPercentualeTasse.value / 100));
    rimb                                      := (rimb / (dist360(dtPckPagA.DateTime, dtPckPagDa.DateTime))) * diff;
    nedtNettoRimborsoContraente.value         := rimb;
    lDatiPolizzaSostituita.premioDaRimborsare := rimb;
    lDatiPolizzaSostituita.ScadenzaUltimoPagamentoA := dtPckPagA.DateTime;
  end
  else
  begin
    // rimborso a 0 !!!!
    // devi calcolare l'integrazione da pagare per coprire il periodo di scopertura ?

  end;
end;


procedure TFDatiPolizzaSostituita.dtPckPagAChangeValue(Sender: TObject);
begin
  calcola;
end;

procedure TFDatiPolizzaSostituita.UniBitBtnCalcolaClick(Sender: TObject);
begin
   btnRicalcoloPolSostBconferma.Enabled:=True;
   calcola;
end;

end.
