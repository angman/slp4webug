unit UStampaPolizzaFM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, uniGUIBaseClasses, uniImageList, Data.DB,
  System.Actions, Vcl.ActnList, uniRadioGroup, uniDBRadioGroup, uniCheckBox, uniDBCheckBox,
  uniMultiItem, uniComboBox, uniDBComboBox, uniEdit, uniDateTimePicker, uniDBDateTimePicker,
  uniDBEdit, uniBitBtn, uniPageControl, uniPanel, uniButton, uniSpeedButton, uniLabel,
  uniScreenMask, uniDBLookupComboBox, Vcl.Menus, uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaFM = class(TFStampaBasePolizza)
    pgMassimaliGaranzie: TUniTabSheet;
    cbbMassimali: TUniDBComboBox;
    ckbxControvFiscali: TUniDBCheckBox;
    ckbxConsulTecnic: TUniDBCheckBox;
    ckbxIndicizzata: TUniDBCheckBox;
    pgDatiAssicurato: TUniTabSheet;
    dbedtDimSaltINDIRIZZO: TUniDBEdit;
    dbedtDimSaltCAP: TUniDBEdit;
    dbedtDimSaltCITTA: TUniDBEdit;
    lblDimoraSaltuaria: TUniLabel;
    dbedtDimSaltPROV: TUniDBEdit;
    UniScreenMask: TUniScreenMask;
    procedure UniFormCreate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FStampaPolizzaFM: TFStampaPolizzaFM;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UMDatiTempPolizzaFM;

function FStampaPolizzaFM: TFStampaPolizzaFM;
begin
  Result := TFStampaPolizzaFM(UniMainModule.GetFormInstance(TFStampaPolizzaFM));
end;


procedure TFStampaPolizzaFM.UniFormCreate(Sender: TObject);
begin
  inherited;

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;

  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE');
  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  TDMDatiTempPolizzaFM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.asstring :=
    Copy(cbbMassimali.Text, 1, 3);

  pcDatiEreditabili.ActivePage := pgContraente;
  pcDatiEreditabili.Pages[1].Visible:=False;
end;

initialization

RegisterClass(TFStampaPolizzaFM);

end.
