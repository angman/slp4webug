inherited FSceltaTipoPolizza: TFSceltaTipoPolizza
  ClientHeight = 412
  ClientWidth = 778
  Caption = 'Scelta tipo polizza da preparare'
  OnClose = UniFormClose
  ExplicitWidth = 794
  ExplicitHeight = 451
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 778
    ExplicitWidth = 778
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 778
    LayoutConfig.Flex = 1
    LayoutConfig.Height = '8%'
    ExplicitWidth = 778
  end
  object pnlContent: TUniPanel [2]
    Left = 0
    Top = 69
    Width = 778
    Height = 343
    Hint = ''
    Align = alClient
    TabOrder = 2
    Caption = ''
    object grdTipiPolizza: TUniDBGrid
      Left = 1
      Top = 1
      Width = 320
      Height = 341
      Hint = ''
      DataSource = dsoEdit
      ReadOnly = True
      WebOptions.Paged = False
      LoadMask.Message = 'Loading data...'
      Align = alLeft
      ParentFont = False
      TabOrder = 1
      OnSelectionChange = grdTipiPolizzaSelectionChange
      OnDblClick = actConfermaExecute
      Columns = <
        item
          FieldName = 'descrizione'
          Title.Caption = 'descrizione'
          Width = 303
        end>
    end
    object unspltrGrid: TUniSplitter
      Left = 321
      Top = 1
      Width = 6
      Height = 341
      Hint = ''
      Align = alLeft
      ParentColor = False
      Color = clBtnFace
    end
    object undbmNote: TUniDBMemo
      Left = 327
      Top = 1
      Width = 450
      Height = 341
      Hint = ''
      DataField = 'NOTE'
      DataSource = dsoEdit
      Align = alClient
      TabOrder = 3
      FieldLabel = 'Descrizione polizza'
      FieldLabelAlign = laTop
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 600
    inherited actConferma: TAction
      Caption = 'Prep. Polizza'
    end
    inherited actAnnulla: TAction
      Caption = 'Esci'
    end
  end
  inherited dsoEdit: TDataSource
    Left = 528
  end
end
