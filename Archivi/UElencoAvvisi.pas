unit UElencoAvvisi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, uniBitBtn, uniSpeedButton, uniGUIBaseClasses, uniPanel, uniBasicGrid, uniDBGrid,
  Data.DB, System.Actions, Vcl.ActnList;

type
  TFElencoAvvisi = class(TUniForm)
    pnlButtons: TUniSimplePanel;
    btnEsci: TUniSpeedButton;
    btnVisualizzaMessaggio: TUniSpeedButton;
    dsAvvisiAge: TDataSource;
    grdElencoAvvisi: TUniDBGrid;
    actlstVisAvviso: TActionList;
    actVisAvviso: TAction;
    procedure btnEsciClick(Sender: TObject);
    procedure grdElencoAvvisiDblClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure actVisAvvisoExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function ImpostaParent(AParent: TUniControl): TFElencoAvvisi;

  end;

implementation

uses
  uniGUIApplication, UAvvisoAgenzia, dbisamtb, MainModule;

{$R *.dfm}

procedure TFElencoAvvisi.actVisAvvisoExecute(Sender: TObject);
var
  AQyMessage: TDBISAMQuery;
begin
  UniMainModule.DMDatiAgeAvvisi.FindAvviso(dsAvvisiAge.DataSet.FieldByName('COD_MSG').AsInteger, AQyMessage);
  TFAvviso.ShowAvviso(UniApplication, AQyMessage);
end;

procedure TFElencoAvvisi.btnEsciClick(Sender: TObject);
begin
  Close;
end;

procedure TFElencoAvvisi.grdElencoAvvisiDblClick(Sender: TObject);
var
  AQyMessage: TDBISAMQuery;
begin
  UniMainModule.DMDatiAgeAvvisi.FindAvviso(dsAvvisiAge.DataSet.FieldByName('COD_MSG').AsInteger, AQyMessage);
  TFAvviso.ShowAvviso(UniApplication, AQyMessage);
end;

function TFElencoAvvisi.ImpostaParent(AParent: TUniControl): TFElencoAvvisi;
begin
  Parent := AParent;
  Result := Self;
end;

procedure TFElencoAvvisi.UniFormCreate(Sender: TObject);
begin
  actVisAvviso.Enabled := UniMainModule.DMDatiAgeAvvisi.hasMessages;
  dsAvvisiAge.DataSet  := UniMainModule.DMDatiAgeAvvisi.QAvvisiAgenzia;
end;

end.
