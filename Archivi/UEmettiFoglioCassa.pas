unit UEmettiFoglioCassa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, uniGUIApplication,
  uniGUIClasses, uniGUIForm, uniButton, uniBitBtn, uniDateTimePicker, uniGUIBaseClasses, uniLabel;

type
  TFEmettiFoglioCassa = class(TUniForm)
    lblIntervalloDate: TUniLabel;
    undtmpckrDataDa: TUniDateTimePicker;
    undtmpckrDataA: TUniDateTimePicker;
    btnBconferma: TUniBitBtn;
    btnBabbandona: TUniBitBtn;
    procedure btnBconfermaClick(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
    chiusura: Boolean;
    function checkTemporaryFC: Boolean;
    function check_days_interval: Boolean;
  public
    { Public declarations }
    // constructor Create(AOwner : TComponent); overload; override;
    constructor Create(AOwner: TComponent; xchiusura: Boolean); reintroduce; overload;

    class procedure ShowChiediDatiFC(UniApplication: TUniGUIApplication; xchiusura: Boolean = False);

  end;

implementation

uses
  MainModule, System.DateUtils, UCodiciErroriPolizza, UDMDatiReport, uDMdatiAgeFogliCassa;

{$R *.dfm}
{ TUniForm1 }

function TFEmettiFoglioCassa.check_days_interval: Boolean;
begin
  // verifica che non ci sia un altro foglio cassa nello stesso periodo o in un periodo
  // che si accavalli a questo
  Result := True;
  if UniMainModule.DMDatiReport.checkDataChiusuraContabile(undtmpckrDataDa.DateTime) then
  begin
    if UniMainModule.DMDatiReport.contaTitoliIncassati('50') < 2 then
    begin

    end
    else
    begin
      Result := False;
      ShowMessage('Nello stesso foglio cassa non si pu� scaricare pi� di un rilievo.');
      Exit(False);
    end;

    // controlla che non ci siano titoli pagati in data precedente alla data di inizio del periodo di chiusura
    if UniMainModule.DMdatiAgeTitoli.ciSonoIncassiPrecedenti(undtmpckrDataDa.DateTime) then
    begin
      ShowMessage
        ('Ci sono incassi registrati in data precedente alla data di inizio periodo di chiusura. Operazione impossibile.');
      Exit(False);
    end;

  end
  else
  begin
    Result := False;
    ShowMessage('Le date selezionate non sono valide.');
    Exit(False);
  end;
end;

procedure TFEmettiFoglioCassa.btnBconfermaClick(Sender: TObject);
begin
  if chiusura then
  begin
    // ShowMessage('Richiesta chiusura contabile');
    if checkTemporaryFC then
    begin
      // controlli sulla corenza delle date e sulla successione dei vari fogli cassa
      if check_days_interval then
      begin
        UniMainModule.DMdatiAgeFogliCassa.chiusura_contabile(undtmpckrDataDa.DateTime, undtmpckrDataA.DateTime);
        ShowMessage
          ('CHIUSURA CONTABILE ESEGUITA CON SUCCESSO. Il foglio cassa pu� essere stampato dall''area dei fogli cassa.');
        ModalResult := mrOk;
      end;
    end;
  end
  else
  begin
    if checkTemporaryFC then
    begin
      UniMainModule.DMDatiReport.StampaFoglioCassaProvvisorio(undtmpckrDataDa.DateTime, undtmpckrDataA.DateTime);
      ModalResult := mrOk;
    end;

  end;

end;

function TFEmettiFoglioCassa.checkTemporaryFC: Boolean;
begin
  Result := True;
  if chiusura and (MonthOf(undtmpckrDataDa.DateTime) <> MonthOf(undtmpckrDataA.DateTime)) then
  begin
    // tutti devono fare il definitivo all'interno del mese!!
    ShowMessage(MSG_DT_FC_MONTHS_DIFF);
    Exit(False);
  end;

  // controlla che la data di chiusura non sia pi� avanti della data corrente di pi� di un giorno
  if (undtmpckrDataA.DateTime > Date + 2) then
  begin
    ShowMessage(MSG_DT_END_FC_TOO_FORWARD);
    Exit(False);
  end;

  // controlla che la data di chiusura sia >= della data di inizio periodo
  if (undtmpckrDataDa.DateTime > undtmpckrDataA.DateTime) then
  begin
    ShowMessage(MSG_DT_END_FC_LESS_DT_START);
    Exit(False);
  end;

end;

constructor TFEmettiFoglioCassa.Create(AOwner: TComponent; xchiusura: Boolean);
begin
  Create(AOwner);
  chiusura                 := xchiusura;
  undtmpckrDataDa.DateTime := Date;
  undtmpckrDataA.DateTime  := Date;

end;

class procedure TFEmettiFoglioCassa.ShowChiediDatiFC(UniApplication: TUniGUIApplication; xchiusura: Boolean = False);
begin
  TFEmettiFoglioCassa.Create(UniApplication, xchiusura).ShowModal;
end;

procedure TFEmettiFoglioCassa.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
