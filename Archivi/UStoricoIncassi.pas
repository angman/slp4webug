unit UStoricoIncassi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses,
  uniDateTimePicker, uniEdit, UDMdatiStoricoIncassi, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox, uniMemo, uniDBText;

type
  TFStoricoIncassi = class(TFMasterElenco)
    unedtSiglaCompagnia: TUniEdit;
    unmbrdtFoglioCassa: TUniNumberEdit;
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn; const Value: Variant);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure grdElencoBodyDblClick(Sender: TObject);
  private
    { Private declarations }
    PDFFolderPath: string;
    PDFUrl: string;
  public
    { Public declarations }
  end;

function FStoricoIncassi: TFStoricoIncassi;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UQueryElencoIntf, ServerModule, System.IOUtils,
  System.StrUtils;

function FStoricoIncassi: TFStoricoIncassi;
begin
  Result := TFStoricoIncassi(UniMainModule.GetFormInstance(TFStoricoIncassi));
end;

procedure TFStoricoIncassi.grdElencoBodyDblClick(Sender: TObject);
var mess: string ;
begin
  // inherited;
  mess:= 'Dati tracciamento incasso' + Chr(13)+
  Chr(13) +
  'Dt. operazione: ' + dsGrid.DataSet.FieldByName('data_operazione').AsString + Chr(13) +
  'Importo: ' + dsGrid.DataSet.FieldByName('importo').AsString + Chr(13) +
  'Mezzo pag.: ' + dsGrid.DataSet.FieldByName('descriz_pag').AsString + Chr(13) +
  'Descriz.: ' + dsGrid.DataSet.FieldByName('descrizione').AsString + Chr(13) +
  'Banca: ' + dsGrid.DataSet.FieldByName('banca').AsString + Chr(13) +
  'eseguito da: ' + dsGrid.DataSet.FieldByName('emesso_da').AsString + Chr(13) +
  'Chiusura sospeso: ' + dsGrid.DataSet.FieldByName('descrizione_chiusura_sospeso').AsString + Chr(13) +
  'Estremi: ' + dsGrid.DataSet.FieldByName('estremi1').AsString + Chr(13) +
  'Estremi: ' + dsGrid.DataSet.FieldByName('estremi2').AsString + Chr(13) +
  'Estremi: ' + dsGrid.DataSet.FieldByName('estremi3').AsString;

  showmessage(mess);
end;

procedure TFStoricoIncassi.grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn; const Value: Variant);
begin

  dsGrid.DataSet.DisableControls;
  try
    dsGrid.DataSet.Filtered      := False;
    dsGrid.DataSet.FilterOptions := [foCaseInsensitive];

    inherited;
  finally
    dsGrid.DataSet.EnableControls
  end;

end;

procedure TFStoricoIncassi.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  dsGrid.DataSet.Close;
end;

procedure TFStoricoIncassi.UniFormCreate(Sender: TObject);
begin
  UniMainModule.archivio := 'SIN';

  inherited;
  PDFUrl           := UniServerModule.FilesFolderURL + 'PDF/';
  PDFFolderPath    := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  btnInsert.Action := nil;
  self.DMMaster    := (UniMainModule.DMDatiStoricoIncassi as IQueryElenco);
  dsGrid.DataSet   := UniMainModule.DMDatiStoricoIncassi.QStoricoIncassi;
  self.DMMaster.EseguiQuery('N_POLIZZA', nil, '');


end;

end.
