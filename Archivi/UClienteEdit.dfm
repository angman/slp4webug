inherited FClienteEdit: TFClienteEdit
  ClientHeight = 499
  ClientWidth = 907
  Caption = 'Editazione dati Cliente'
  OnClose = UniFormClose
  Visible = True
  OnBeforeShow = UniFormBeforeShow
  ExplicitWidth = 923
  ExplicitHeight = 538
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 907
    ExplicitWidth = 907
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 907
    ExplicitWidth = 907
  end
  object pnlEdit: TUniContainerPanel [2]
    Left = 0
    Top = 69
    Width = 907
    Height = 430
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    ExplicitLeft = 336
    ExplicitTop = 224
    ExplicitWidth = 256
    ExplicitHeight = 128
    object pcCliente: TUniPageControl
      Left = 0
      Top = 0
      Width = 907
      Height = 430
      Hint = ''
      ActivePage = pgDatiAnag
      Align = alClient
      ClientEvents.ExtEvents.Strings = (
        
          'tabPanel.afterrender=function tabPanel.afterrender(sender, eOpts' +
          ')'#13#10'{'#13#10'    var _height = 40;'#13#10'    sender.tabBar.setHeight(_height' +
          ');'#13#10'    sender.tabBar.items.each(function(el) {'#13#10'        el.setH' +
          'eight(_height)'#13#10'    })'#13#10'}')
      TabOrder = 0
      object pgDatiAnag: TUniTabSheet
        Hint = ''
        Caption = 'Dati anagrafici'
        ClientEvents.ExtEvents.Strings = (
          
            'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
            'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
        ParentFont = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 912
        ExplicitHeight = 453
        object pnlDenominazione: TUniSimplePanel
          Left = 0
          Top = 0
          Width = 899
          Height = 36
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 0
          object dbedtDenominazione: TUniDBEdit
            Left = 24
            Top = 8
            Width = 578
            Height = 22
            Hint = ''
            DataField = 'DENOM'
            TabOrder = 1
            ReadOnly = True
            FieldLabel = 'Denominazione'
          end
        end
        object pnlDatiAnag: TUniSimplePanel
          Left = 0
          Top = 36
          Width = 899
          Height = 366
          Hint = ''
          ParentColor = False
          Align = alClient
          TabOrder = 1
          object btnDelCollaboratore: TUniBitBtn
            Left = 402
            Top = 186
            Width = 25
            Height = 25
            Action = actCancellaProduttore
            TabOrder = 17
            IconAlign = iaCenter
            Images = untvmglstIcons
            ImageIndex = 1
          end
          object btnSelezionaProduttore: TUniBitBtn
            Left = 377
            Top = 186
            Width = 25
            Height = 25
            Action = actSelezionaProduttore
            ParentFont = False
            TabOrder = 16
            IconAlign = iaCenter
            Images = untvmglstIcons
            ImageIndex = 0
          end
          object cbbLivello: TUniDBLookupComboBox
            Left = 680
            Top = 13
            Width = 201
            Hint = ''
            ListFieldIndex = 0
            ClearButton = True
            DataField = 'livello'
            DataSource = dsoEdit
            TabOrder = 3
            Color = clWindow
            FieldLabel = 'Livello'
            FieldLabelWidth = 45
          end
          object cbbProfessione: TUniDBLookupComboBox
            Left = 24
            Top = 129
            Width = 350
            Hint = ''
            ListField = 'DESCRIZ'
            ListSource = DMdatiAge.dsClientiProfessioni
            KeyField = 'COD_PROFESSIONE'
            ListFieldIndex = 0
            ClearButton = True
            DataField = 'PROFESSION'
            DataSource = dsoEdit
            TabOrder = 11
            Color = clWindow
            FieldLabel = 'Professione'
            FieldLabelWidth = 120
          end
          object cbbSesso: TUniDBComboBox
            Left = 458
            Top = 13
            Width = 210
            Hint = ''
            DataField = 'sesso'
            DataSource = dsoEdit
            Items.Strings = (
              'Maschio=M'
              'Femmina=F'
              'Persona Giuridica=P')
            TabOrder = 2
            FieldLabel = 'Sesso'
            FieldLabelWidth = 40
            ClearButton = True
            IconItems = <>
          end
          object cbbTitolo: TUniDBLookupComboBox
            Left = 24
            Top = 13
            Width = 350
            Hint = ''
            ListField = 'TITOLO'
            ListSource = DMdatiAgeClienti.DSClienteTitoli
            KeyField = 'COD_TITOLO'
            ListFieldIndex = 0
            ClearButton = True
            DataField = 'TITOLO'
            DataSource = dsoEdit
            TabOrder = 1
            Color = clWindow
            FieldLabel = 'Titolo'
            FieldLabelWidth = 120
          end
          object ckbxPrivacy: TUniDBCheckBox
            Left = 458
            Top = 102
            Width = 372
            Height = 17
            Hint = ''
            DataField = 'PRIVACY'
            DataSource = dsoEdit
            ValueChecked = 'S'
            Caption = 
              'Privacy (Se il cliente ha firmato il modulo per il trattamento d' +
              'ati generico)'
            TabOrder = 23
            ParentColor = False
            Color = clBtnFace
          end
          object ckbxStatoCivile: TUniDBCheckBox
            Left = 680
            Top = 74
            Width = 73
            Height = 17
            Hint = ''
            DataField = 'STATO_CIVI'
            DataSource = dsoEdit
            ValueChecked = 'C'
            ValueUnchecked = 'N'
            Caption = 'Coniugato'
            TabOrder = 8
            ParentColor = False
            Color = clBtnFace
          end
          object dbedtCocnome: TUniDBEdit
            Left = 24
            Top = 42
            Width = 350
            Height = 22
            Hint = ''
            DataField = 'COGNOME'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 4
            FieldLabel = 'Cognome/Rag.Soc.'
            FieldLabelWidth = 120
          end
          object dbedtCodFisc: TUniDBEdit
            Left = 24
            Top = 71
            Width = 350
            Height = 22
            Hint = ''
            DataField = 'COD_FISC'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 6
            FieldLabel = 'Codice fiscale'
            FieldLabelWidth = 120
          end
          object dbedtDescrizAttivita: TUniDBEdit
            Left = 458
            Top = 129
            Width = 423
            Height = 22
            Hint = ''
            DataField = 'DESCRIZIONE_ATTIVITA'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 12
            FieldLabel = 'Descr. attivit'#224
            FieldLabelWidth = 40
          end
          object dbedtLocNascita: TUniDBEdit
            Left = 24
            Top = 158
            Width = 350
            Height = 22
            Hint = ''
            DataField = 'NATO_A'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 13
            FieldLabel = 'Nato a'
            FieldLabelWidth = 120
          end
          object dbedtNazioneNascita: TUniDBEdit
            Left = 680
            Top = 158
            Width = 201
            Height = 22
            Hint = ''
            DataField = 'NATO_NAZIONE'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 15
            FieldLabel = 'Nazione'
            FieldLabelWidth = 50
          end
          object dbedtNome: TUniDBEdit
            Left = 458
            Top = 42
            Width = 350
            Height = 22
            Hint = ''
            DataField = 'NOME'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 5
            FieldLabel = 'Nome'
            FieldLabelWidth = 40
          end
          object edtReferente: TUniEdit
            Left = 24
            Top = 100
            Width = 350
            Hint = ''
            Text = ''
            TabOrder = 9
            TabStop = False
            Color = clYellow
            ReadOnly = True
            FieldLabel = 'Referente'
            FieldLabelWidth = 120
            OnChangeValue = edtReferenteChangeValue
          end
          object undbdtmpckrDataNascita: TUniDBDateTimePicker
            Left = 458
            Top = 158
            Width = 210
            Hint = ''
            DataField = 'NATO_IL'
            DataSource = dsoEdit
            DateTime = 43745.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 14
            FieldLabel = 'Nato il'
            FieldLabelWidth = 40
          end
          object unedtPromoter: TUniEdit
            Left = 24
            Top = 187
            Width = 350
            Hint = ''
            Text = 'unedtPromoter'
            ParentFont = False
            TabOrder = 18
            ReadOnly = True
            FieldLabel = 'Seguito da'
            FieldLabelWidth = 120
            OnChangeValue = unedtPromoterChangeValue
          end
          object unedtSubPromoter: TUniEdit
            Left = 458
            Top = 187
            Width = 350
            Hint = ''
            Enabled = False
            Text = ''
            ParentFont = False
            TabOrder = 19
            ReadOnly = True
            FieldLabel = 'Sub collaboratore'
            FieldLabelWidth = 120
            OnChangeValue = unedtSubPromoterChangeValue
          end
          object edtTelefono: TUniEdit
            Left = 24
            Top = 218
            Width = 350
            Hint = ''
            Text = ''
            TabOrder = 21
            TabStop = False
            Color = clYellow
            ReadOnly = True
            FieldLabel = 'Telefono'
            FieldLabelWidth = 120
          end
          object edtMail: TUniEdit
            Left = 458
            Top = 218
            Width = 350
            Hint = ''
            Text = ''
            TabOrder = 22
            TabStop = False
            Color = clYellow
            ReadOnly = True
            FieldLabel = 'Mail'
            FieldLabelWidth = 40
          end
          object btnSelezionaSubproduttore: TUniMenuButton
            Left = 814
            Top = 186
            Width = 25
            Height = 25
            Hint = ''
            Enabled = False
            DropdownMenu = unpmnSelSubCollaboratore
            Caption = ''
            TabOrder = 20
            Images = untvmglstIcons
          end
          object btnSelezionaSubProduttore: TUniBitBtn
            Left = 812
            Top = 186
            Width = 25
            Height = 25
            Enabled = False
            Action = actSelezionaSubProduttore
            ParentFont = False
            TabOrder = 21
            IconAlign = iaCenter
            Images = untvmglstIcons
          end
          object dbedtPIva: TUniDBEdit
            Left = 458
            Top = 71
            Width = 209
            Height = 22
            Hint = ''
            DataField = 'P_IVA'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 7
            ClientEvents.ExtEvents.Strings = (
              
                'keydown=function keydown(sender, e, eOpts)'#13#10'{'#13#10'    var event = e' +
                ';'#13#10'    if (event.keyCode == 46 || event.keyCode == 8 || event.ke' +
                'yCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||     ' +
                '   '#13#10'        (event.keyCode == 65 && event.ctrlKey === true) || ' +
                '       '#13#10'        (event.keyCode >= 35 && event.keyCode <= 39)) {' +
                '        '#13#10'        return;'#13#10'    } else {        '#13#10'        if (eve' +
                'nt.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (ev' +
                'ent.keyCode < 96 || event.keyCode > 105)) {'#13#10'            event.p' +
                'reventDefault();'#13#10'        }'#13#10'    }'#13#10#13#10'}')
            FieldLabel = 'P.Iva'
            FieldLabelWidth = 40
          end
        end
      end
      object pgIndirizzo: TUniTabSheet
        Hint = ''
        Caption = 'Indirizzo'
        ClientEvents.ExtEvents.Strings = (
          
            'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
            'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
        object pnlIndirizzo1: TUniPanel
          Left = 17
          Top = 33
          Width = 864
          Height = 67
          Hint = ''
          ParentFont = False
          TabOrder = 0
          BorderStyle = ubsSolid
          Caption = ''
          object dbedtCap1: TUniDBEdit
            Left = 335
            Top = 25
            Width = 121
            Height = 22
            Hint = ''
            DataField = 'CAP'
            DataSource = dsoEdit
            ParentFont = False
            TabOrder = 3
            FieldLabel = 'Cap'
            FieldLabelWidth = 30
          end
          object dbedtCitta1: TUniDBEdit
            Left = 462
            Top = 25
            Width = 258
            Height = 22
            Hint = ''
            DataField = 'CITTA'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 4
            FieldLabel = 'Citt'#224
            FieldLabelWidth = 30
          end
          object dbedtIndirizzo1: TUniDBEdit
            Left = 15
            Top = 25
            Width = 314
            Height = 22
            Hint = ''
            DataField = 'INDIRIZZO'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 2
            FieldLabel = 'Indirizzo'
            FieldLabelWidth = 50
          end
          object dbedtProvincia1: TUniDBEdit
            Left = 726
            Top = 25
            Width = 122
            Height = 22
            Hint = ''
            DataField = 'PROVINCIA'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            TabOrder = 5
            FieldLabel = 'Provincia'
            FieldLabelWidth = 60
          end
          object lblIndirizzo1: TUniLabel
            Left = 24
            Top = 4
            Width = 170
            Height = 13
            Hint = ''
            Caption = '(1) Residenza/Sede principale'
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 1
          end
        end
        object pnlIndirizzo2: TUniPanel
          Left = 17
          Top = 108
          Width = 864
          Height = 67
          Hint = ''
          ParentFont = False
          TabOrder = 1
          BorderStyle = ubsSolid
          Caption = ''
          object dbedtCAP2: TUniDBEdit
            Left = 335
            Top = 25
            Width = 121
            Height = 22
            Hint = ''
            DataField = 'CAP_2'
            DataSource = dsoEdit
            ParentFont = False
            TabOrder = 3
            FieldLabel = 'Cap'
            FieldLabelWidth = 30
          end
          object dbedtCITTA2: TUniDBEdit
            Left = 462
            Top = 25
            Width = 258
            Height = 22
            Hint = ''
            DataField = 'CITTA_2'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 4
            FieldLabel = 'Citt'#224
            FieldLabelWidth = 30
          end
          object dbedtINDIRIZZO2: TUniDBEdit
            Left = 15
            Top = 25
            Width = 314
            Height = 22
            Hint = ''
            DataField = 'INDIRIZZO_2'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 2
            FieldLabel = 'Indirizzo'
            FieldLabelWidth = 50
          end
          object dbedtPROVINCIA2: TUniDBEdit
            Left = 726
            Top = 25
            Width = 122
            Height = 22
            Hint = ''
            DataField = 'PROVINCIA_2'
            DataSource = dsoEdit
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 5
            FieldLabel = 'Provincia'
            FieldLabelWidth = 60
          end
          object lblIndirizzo2: TUniLabel
            Left = 24
            Top = 4
            Width = 177
            Height = 13
            Hint = ''
            Caption = '(2) Residenza/Sede secondaria'
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 1
          end
        end
        object pnlVarAppendice: TUniPanel
          Left = 17
          Top = 193
          Width = 864
          Height = 144
          Hint = ''
          TabOrder = 2
          Caption = ''
          object btnAppCambioIndirizzo: TUniSpeedButton
            Left = 15
            Top = 16
            Width = 154
            Height = 49
            Hint = ''
            ParentRTL = False
            Visible = False
            Caption = 'Appendice Variazione indirizzo'
            ParentColor = False
            Color = clYellow
            TabOrder = 1
          end
        end
      end
      object pgNote: TUniTabSheet
        Hint = ''
        Caption = 'Note'
        ClientEvents.ExtEvents.Strings = (
          
            'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
            'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
        object undbmNotaCliente: TUniDBMemo
          Left = 0
          Top = 0
          Width = 899
          Height = 402
          Hint = ''
          DataField = 'NOTE'
          DataSource = dsoEdit
          Align = alClient
          TabOrder = 0
        end
      end
      object pgTelefoniInternet: TUniTabSheet
        Hint = ''
        Caption = ' Telefoni / mail'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 256
        ExplicitHeight = 128
        object pnlTelefoniMail: TUniSimplePanel
          Left = 0
          Top = 0
          Width = 899
          Height = 200
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 0
          object pnlTeleMailDescr: TUniPanel
            Left = 0
            Top = 0
            Width = 899
            Height = 20
            Hint = ''
            Align = alTop
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 1
            BorderStyle = ubsNone
            Caption = 'Telefoni / mail'
            object dbnvgtrTelefoniMail: TUniDBNavigator
              Left = 0
              Top = -1
              Width = 241
              Height = 25
              Hint = ''
              DataSource = dsTelefoniMail
              TabOrder = 0
            end
          end
          object grdTelefoniMail: TUniDBGrid
            Left = 0
            Top = 20
            Width = 899
            Height = 180
            Hint = ''
            DataSource = dsTelefoniMail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgAutoRefreshRow, dgRowNumbers]
            LoadMask.Message = 'Loading data...'
            Align = alClient
            ParentFont = False
            TabOrder = 3
            Columns = <
              item
                FieldName = 'DescrTitolo'
                Title.Caption = 'Tipo '
                Width = 106
                Editor = cbbTipoTelefonoMail
              end
              item
                FieldName = 'Descrizione'
                Title.Caption = 'Descrizione'
                Width = 200
                Editor = edtTelDescr
              end
              item
                FieldName = 'Numero'
                Title.Caption = 'Telefono / mail'
                Width = 300
                Editor = edtTelefonoMail
              end
              item
                FieldName = 'valore_default'
                Title.Caption = 'preferenziale'
                Width = 80
                Alignment = taLeftJustify
                CheckBoxField.BooleanFieldOnly = False
                CheckBoxField.FieldValues = 'S;'#13#10'N'
                CheckBoxField.DisplayValues = 'Si;'#13#10'No'
              end>
          end
          object pnlHdrTelefoniMail: TUniHiddenPanel
            Left = 705
            Top = -1
            Width = 160
            Height = 256
            Hint = ''
            Visible = True
            object edtTelDescr: TUniEdit
              Left = 24
              Top = 43
              Width = 121
              Hint = ''
              Text = 'EDTTELDESCR'
              TabOrder = 2
            end
            object edtTelefonoMail: TUniEdit
              Left = 24
              Top = 71
              Width = 121
              Hint = ''
              Text = 'edtTelefonoMail'
              TabOrder = 3
            end
            object cbbTipoTelefonoMail: TUniDBLookupComboBox
              Left = 24
              Top = 16
              Width = 145
              Hint = ''
              ListField = 'TelMailDescr'
              ListSource = dsTipoTelMail
              KeyField = 'TipoTelMail'
              ListFieldIndex = 0
              DataField = 'TITOLO'
              TabOrder = 1
              Color = clWindow
            end
          end
        end
        object spltrPanels: TUniSplitter
          Left = 0
          Top = 200
          Width = 899
          Height = 6
          Cursor = crVSplit
          Hint = ''
          Align = alTop
          ParentColor = False
          Color = clBtnFace
        end
        object pnlDocumenti: TUniSimplePanel
          Left = 0
          Top = 206
          Width = 899
          Height = 196
          Hint = ''
          ParentColor = False
          Align = alClient
          TabOrder = 2
          object pnlDocuments: TUniPanel
            Left = 0
            Top = 0
            Width = 899
            Height = 20
            Hint = ''
            Align = alTop
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 0
            BorderStyle = ubsNone
            Caption = 'Documenti'
            object dbnvgtrDocuments: TUniDBNavigator
              Left = 0
              Top = -5
              Width = 241
              Height = 25
              Hint = ''
              DataSource = dsDocuments
              VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit]
              TabOrder = 0
              BeforeAction = dbnvgtrDocumentsBeforeAction
            end
          end
          object grdDocuments: TUniDBGrid
            Left = 0
            Top = 20
            Width = 899
            Height = 176
            Hint = ''
            DataSource = dsDocuments
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgAutoRefreshRow, dgRowNumbers]
            ReadOnly = True
            LoadMask.Message = 'Loading data...'
            Align = alClient
            ParentFont = False
            TabOrder = 2
            OnBodyDblClick = grdDocumentsBodyDblClick
            Columns = <
              item
                FieldName = 'descrizione_doc'
                Title.Caption = 'Descrizione'
                Width = 232
              end
              item
                FieldName = 'RILASCIATO_IL'
                Title.Caption = 'Data rilascio'
                Width = 87
              end
              item
                FieldName = 'RILASCIATO_DA'
                Title.Caption = 'Rilasciato da'
                Width = 125
              end
              item
                FieldName = 'PRIMA_SCADENZA'
                Title.Caption = 'Prima scadenza'
                Width = 104
              end
              item
                FieldName = 'SCADENZA'
                Title.Caption = 'Scadenza'
                Width = 64
              end>
          end
        end
      end
      object pgContattiCliente: TUniTabSheet
        Hint = ''
        Caption = 'Contatti col cliente'
        object grdContattiCliente: TUniDBGrid
          Left = 0
          Top = 32
          Width = 899
          Height = 370
          Hint = ''
          DataSource = dsContattiCliente
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgAutoRefreshRow, dgRowNumbers]
          ReadOnly = True
          LoadMask.Message = 'Loading data...'
          Align = alClient
          ParentFont = False
          TabOrder = 1
          OnBodyDblClick = grdContattiClienteBodyDblClick
          Columns = <
            item
              FieldName = 'DATA'
              Title.Caption = 'Data'
              Width = 64
            end
            item
              FieldName = 'DescrTipoCont'
              Title.Caption = 'Tipo contatto'
              Width = 150
            end
            item
              FieldName = 'MOTIVO'
              Title.Caption = 'Motivo contatto'
              Width = 232
            end
            item
              FieldName = 'referente'
              Title.Caption = 'Parlato con'
              Width = 150
            end
            item
              FieldName = 'rif_operatore'
              Title.Caption = 'Operatore'
              Width = 150
            end>
        end
        object pnlNav: TUniSimplePanel
          Left = 0
          Top = 0
          Width = 899
          Height = 32
          Hint = ''
          ParentColor = False
          Align = alTop
          TabOrder = 0
          object dbnvgtrContattiCliente: TUniDBNavigator
            Left = 8
            Top = 3
            Width = 241
            Height = 25
            Hint = ''
            DataSource = dsContattiCliente
            VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit]
            TabOrder = 1
            BeforeAction = dbnvgtrContattiClienteBeforeAction
          end
        end
      end
    end
  end
  inherited actlstEditOperation: TActionList
    object actSelezionaProduttore: TAction
      Category = 'SelProduttore'
      Caption = 'Seleziona Produttore'
      OnExecute = actSelezionaProduttoreExecute
    end
    object actCancellaProduttore: TAction
      Category = 'SelProduttore'
      Caption = 'Cancella produttore'
      OnExecute = actCancellaProduttoreExecute
    end
    object actCancellaSubProduttore: TAction
      Category = 'SelProduttore'
      Caption = 'Cancella subproduttore'
      OnExecute = actCancellaSubProduttoreExecute
    end
    object actSelezionaSubProduttore: TAction
      Category = 'SelProduttore'
      Caption = 'Seleziona subproduttore'
      OnExecute = actSelezionaSubProduttoreExecute
    end
    object actSelezionaReferente: TAction
      Category = 'SelReferente'
      Caption = 'Seleziona Referente'
      OnExecute = actSelezionaReferenteExecute
    end
    object actCancellaReferente: TAction
      Category = 'SelReferente'
      Caption = 'Cancella Referente'
      OnExecute = actCancellaReferenteExecute
    end
  end
  inherited dsoEdit: TDataSource
    DataSet = DMdatiAgeClienti.QeditCliente
  end
  inherited untvmglstIcons: TUniNativeImageList
    Left = 644
    Top = 38
  end
  object dsTelefoniMail: TDataSource
    DataSet = DMdatiAgeClienti.QTelefoniMail
    OnDataChange = dsTelefoniMailDataChange
    Left = 412
    Top = 149
  end
  object dsDocuments: TDataSource
    DataSet = DMdatiAgeClienti.QDocuments
    Left = 604
    Top = 437
  end
  object dsTipoTelMail: TDataSource
    DataSet = DMdatiAgeClienti.fdmtblTipo
    Left = 108
    Top = 173
  end
  object dsContattiCliente: TDataSource
    DataSet = DMdatiAgeClienti.QContattoCliente
    Left = 748
    Top = 429
  end
  object unpmnSelProduttore: TUniPopupMenu
    Images = untvmglstIcons
    Left = 320
    Top = 405
    object mnuSelezionaCollaboratore: TUniMenuItem
      Action = actSelezionaProduttore
      ImageIndex = 0
    end
    object mnuTogliCollaboratore: TUniMenuItem
      Action = actCancellaProduttore
      ImageIndex = 1
    end
  end
  object unpmnSelSubCollaboratore: TUniPopupMenu
    Images = untvmglstIcons
    Left = 832
    Top = 397
    object mnuSelezionaSubProduttore: TUniMenuItem
      Action = actSelezionaSubProduttore
      ImageIndex = 0
    end
    object mnuCancellaSubProduttore: TUniMenuItem
      Action = actCancellaSubProduttore
      ImageIndex = 1
    end
  end
  object unpmnSelReferente: TUniPopupMenu
    Images = untvmglstIcons
    Left = 392
    Top = 261
    object mnuSelezionaReferente: TUniMenuItem
      Action = actSelezionaReferente
    end
    object mnuCancellaReferente: TUniMenuItem
      Action = actCancellaReferente
    end
  end
end
