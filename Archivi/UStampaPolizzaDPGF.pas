unit UStampaPolizzaDPGF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, Data.DB, System.Actions, Vcl.ActnList,
  uniPageControl, uniEdit, uniDBEdit, uniButton, uniBitBtn, uniSpeedButton, uniLabel,
  uniGUIBaseClasses, uniPanel, uniImageList, uniRadioGroup, uniDBRadioGroup, uniCheckBox,
  uniDBCheckBox, uniMultiItem, uniComboBox, uniDBComboBox, uniDateTimePicker, uniDBDateTimePicker,
  uniDBLookupComboBox, uniMemo, uniSpinEdit, UdmdatiAge, unimDBEdit, System.Math,
  uniScreenMask, Vcl.Menus, uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaDPGF = class(TFStampaBasePolizza)
    pgAssicurato: TUniTabSheet;
    pcOggettoAssicurato: TUniPageControl;
    pgOggettoAssicurato: TUniTabSheet;
    pgFamiliari: TUniTabSheet;
    pgMassimaliGaranzie: TUniTabSheet;
    undbrdgrpTipoAssicurato: TUniDBRadioGroup;
    pnlpncontaier: TUniContainerPanel;
    pnlAssicurato: TUniPanel;
    pnlVeicolo: TUniPanel;
    dbedtDenominazione: TUniDBEdit;
    dbedtCodiceFiscale: TUniDBEdit;
    dbedtPatente: TUniDBEdit;
    dbedtRilasciataDa: TUniDBEdit;
    dtpckDataRilascioPat: TUniDBDateTimePicker;
    dtpckDataScadenzaPat: TUniDBDateTimePicker;
    cbbCategoriaPat: TUniDBComboBox;
    dbedtNome1: TUniDBEdit;
    dbedtPatente1: TUniDBEdit;
    dbedtCategoria1: TUniDBEdit;
    dbedtNome2: TUniDBEdit;
    dbedtPatente2: TUniDBEdit;
    dbedtCategoria2: TUniDBEdit;
    dbedtNome3: TUniDBEdit;
    dbedtPatente3: TUniDBEdit;
    dbedtCategoria3: TUniDBEdit;
    dbedtNome4: TUniDBEdit;
    dbedtPatente4: TUniDBEdit;
    dbedtCategoria4: TUniDBEdit;
    dbcblkupTipoVeicolo: TUniDBLookupComboBox;
    dbedtMarca: TUniDBEdit;
    dbedtModello: TUniDBEdit;
    dbedtTarga: TUniDBEdit;
    dbedtTelaio: TUniDBEdit;
    dbedtQlHp: TUniDBEdit;
    dtpckScadenzaRevisione: TUniDBDateTimePicker;
    cbbMassimali: TUniDBComboBox;
    ckbxRischioAccessorio: TUniDBCheckBox;
    cbbRimbSpesePatPunti: TUniDBComboBox;
    dbgrpForma: TUniDBRadioGroup;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    ckbxAppendiceControversie: TUniDBCheckBox;
    ckbxIncMaxDP: TUniDBCheckBox;
    ckbxbProblematicaRCA: TUniDBCheckBox;
    ckbxPerizieParte: TUniDBCheckBox;
    ckbxMancatoInterventoRCA: TUniDBCheckBox;
    ckbxProtezioneFamiglia: TUniDBCheckBox;
    ckbxEstEPatenti: TUniDBCheckBox;
    unmEstEPatente: TUniMemo;
    dsTipoVeicolo: TDataSource;
    dbedtNumeroAssicurati: TUniDBNumberEdit;
    UniScreenMask: TUniScreenMask;
    procedure undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure dbgrpFormaChangeValue(Sender: TObject);
    procedure ckbxProtezioneFamigliaClick(Sender: TObject);
    procedure ckbxRischioAccessorioClick(Sender: TObject);
    procedure dbedtNumeroAssicuratiChange(Sender: TObject);
    procedure actSelezionaContraenteExecute(Sender: TObject);
  private
    { Private declarations }
    procedure EnableDisableEstFamiglia(Enable: Boolean);
    procedure EnableDisableEstRischioAccessorio(Enable: Boolean);
    procedure RiallineaMaxGaranzie;
  protected
    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); override;

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;

  end;

function FStampaPolizzaDPGF: TFStampaPolizzaDPGF;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiPolizzaOM, UDMDatiTempPolizzaOM, UDMDatiTempPolizza, libSLP, UDMDatiTempPolizzaDPGF;

function FStampaPolizzaDPGF: TFStampaPolizzaDPGF;
begin
  Result := TFStampaPolizzaDPGF(UniMainModule.GetFormInstance(TFStampaPolizzaDPGF));
end;

procedure TFStampaPolizzaDPGF.actSelezionaContraenteExecute(Sender: TObject);
begin
  inherited;
  // devi forzare sempre la patente e non mostrare mai il selettore relativo a patente/veicolo
end;

procedure TFStampaPolizzaDPGF.ckbxProtezioneFamigliaClick(Sender: TObject);
begin
  if (dbedtPIva.Text > '') or (isdigit(Copy(dbedtCodFiscale.Text, 1, 1))) and ckbxProtezioneFamiglia.Checked then
  begin
    ckbxProtezioneFamiglia.Checked := False;
    ckbxProtezioneFamiglia.DataSource.DataSet.FieldByName('ProtezioneFamiglia').AsBoolean := False;
    ShowMessage('Non � possibile utilizzare questa estensione in presenza di partita IVA.');
  end else begin

     if (undbrdgrpTipoAssicurato.ItemIndex=1) and ckbxProtezioneFamiglia.Checked then
     begin
        ShowMessage('La protezione famiglia � attivabile solo per garanzie sulla patente.');
        ckbxProtezioneFamiglia.Checked:=false;
     end else begin

        dbedtNumeroAssicurati.Enabled := ckbxProtezioneFamiglia.Checked;
        pgFamiliari.TabVisible        := ckbxProtezioneFamiglia.Checked;
        if Sender <> nil then
          if not ckbxProtezioneFamiglia.Checked then
          begin
            pcOggettoAssicurato.ActivePage := pgAssicurato;
            dbedtNumeroAssicurati.Value    := 0;
            pgFamiliari.TabVisible         := false
          end
          else
          begin
            if Sender <> nil then
              dbedtNumeroAssicurati.Value := 1;
            pgFamiliari.TabVisible        := True;
          end;
        dbedtNumeroAssicuratiChange(nil);
     end;
  end;
end;

procedure TFStampaPolizzaDPGF.ckbxRischioAccessorioClick(Sender: TObject);
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
  if (not ckbxRischioAccessorio.Checked) then
    cbbRimbSpesePatPunti.Text := '';
end;

procedure TFStampaPolizzaDPGF.dbedtNumeroAssicuratiChange(Sender: TObject);
var
  i: Integer;

  procedure EnableDisableFamiliari(Enable: Boolean);
  begin
    TUniDBEdit(FindComponent('DBEdtNome' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtPatente' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtCategoria' + i.ToString)).Enabled := Enable;
    if Enable then
    begin
       TUniDBEdit(FindComponent('DBEdtCategoria' + i.ToString)).Text :='B';
       dsoEdit.DataSet.FieldByName('CatPatente'+ i.ToString).AsString := 'B';
    end else begin
       TUniDBEdit(FindComponent('DBEdtCategoria' + i.ToString)).Text := '';
       dsoEdit.DataSet.FieldByName('CatPatente'+ i.ToString).AsString := '';
    end;
  end;

begin
  for i := 1 to 4 do
    EnableDisableFamiliari(i <= dbedtNumeroAssicurati.Value);
end;

procedure TFStampaPolizzaDPGF.dbgrpFormaChangeValue(Sender: TObject);
var
  selectedForma: string;
begin
  selectedForma          := dbgrpForma.Values[dbgrpForma.ItemIndex];
  cbbMassimali.DataField := '';
  try
    UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
      selectedForma);

  finally
    cbbMassimali.DataField := 'SiglaMassimale';
  end;

  EnableDisableEstFamiglia(dbgrpForma.ItemIndex = 0);
end;

procedure TFStampaPolizzaDPGF.EnableDisableEstFamiglia(Enable: Boolean);
begin
  if UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta then
    ckbxProtezioneFamiglia.Enabled := false
  else
    ckbxProtezioneFamiglia.Enabled := Enable;
  dbedtNumeroAssicurati.Enabled    := false;
  ckbxProtezioneFamiglia.Checked   := false;
end;

procedure TFStampaPolizzaDPGF.EnableDisableEstRischioAccessorio(Enable: Boolean);
begin
  ckbxRischioAccessorio.Enabled := Enable;
  cbbRimbSpesePatPunti.Enabled  := Enable and ckbxRischioAccessorio.Checked;
end;

procedure TFStampaPolizzaDPGF.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited;
  RiallineaMaxGaranzie;
end;

procedure TFStampaPolizzaDPGF.RiallineaMaxGaranzie;
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
  ckbxProtezioneFamigliaClick(nil);
  undbrdgrpTipoAssicuratoChangeValue(nil);
end;

procedure TFStampaPolizzaDPGF.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited SetCliente(ASelectedItem, ASelectedDescription);

  if UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta then
  begin
    undbrdgrpTipoAssicurato.ItemIndex := 0; // 1;  SEMPRE PATENTE !!!
    dbedtNumeroAssicurati.Value       := 0;
    dbedtNumeroAssicurati.DataSource.DataSet.FieldByName('protezionefamiglia').AsBoolean := false;
    // fdmtblPolizzaProtezioneFamiglia.AsBoolean := false;
  end
  else
    undbrdgrpTipoAssicurato.ItemIndex := 0;

  undbrdgrpTipoAssicurato.ItemIndex := ifThen(UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta, 1, 0);
end;

procedure TFStampaPolizzaDPGF.undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
begin
  pnlAssicurato.visible := True; // undbrdgrpTipoAssicurato.ItemIndex = 0;

  pnlVeicolo.visible     := false; // undbrdgrpTipoAssicurato.ItemIndex = 1;
  pgFamiliari.TabVisible := undbrdgrpTipoAssicurato.ItemIndex = 0;
  // dbgrpForma.Enabled     := undbrdgrpTipoAssicurato.ItemIndex = 1;
  // dbgrpForma.ItemIndex   := undbrdgrpTipoAssicurato.ItemIndex;
  EnableDisableEstFamiglia(undbrdgrpTipoAssicurato.ItemIndex = 0);
  EnableDisableEstRischioAccessorio((undbrdgrpTipoAssicurato.ItemIndex = 0)); // and ckbxRischioAccessorio.Checked);

end;

procedure TFStampaPolizzaDPGF.UniFormCreate(Sender: TObject);
begin
  inherited;
  pcDatiEreditabili.ActivePage   := pgContraente;
  pcOggettoAssicurato.ActivePage := pgOggettoAssicurato;

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;
  // IWFraAssicuratoPolizzaOM.OnAfterTipoAssicuratoChanged := UniMainModule.DMDatiTempPolizza.SetDatiAssicuratoDaContraente;

  ckbxMancatoInterventoRCA.Enabled  := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICEDP');
  ckbxAppendiceControversie.Enabled := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICECC');
  // ckbxTacitoRinnovo.Enabled         := UniMainModule.DMdatiAge.TacitoRinnovoEnabled;
  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaForma.asstring);
  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  TDMDatiTempPolizzaDPGF(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.asstring :=
    Copy(cbbMassimali.Text, 1, 3);
  // TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzaSiglaMassimaleChange(nil);
  dsTipoVeicolo.DataSet.Open;

  pnlAssicurato.visible                 := True;
  pnlVeicolo.visible                    := false;
  undbrdgrpTipoAssicurato.OnChangeValue := undbrdgrpTipoAssicuratoChangeValue;
  undbrdgrpTipoAssicurato.DataSource.DataSet.FieldByName('tipoassicurato').asstring := 'P';
  ckbxProtezioneFamiglia.Enabled := True;
end;

initialization

RegisterClass(TFStampaPolizzaDPGF);

end.
