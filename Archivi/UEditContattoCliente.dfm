inherited FEditContattoCliente: TFEditContattoCliente
  ClientHeight = 327
  ClientWidth = 565
  Caption = 'Contatto cliente'
  BorderStyle = bsDialog
  BorderIcons = []
  ActiveControl = cbbTipoDocumento
  ExplicitWidth = 571
  ExplicitHeight = 356
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 565
    ExplicitWidth = 555
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 565
    ExplicitWidth = 555
  end
  object pnlEdit: TUniSimplePanel [2]
    Left = 0
    Top = 69
    Width = 565
    Height = 258
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    ExplicitLeft = -53
    ExplicitWidth = 554
    ExplicitHeight = 357
    object cbbTipoDocumento: TUniDBLookupComboBox
      Left = 21
      Top = 24
      Width = 247
      Hint = ''
      ListField = 'descriz'
      ListSource = dsTipoContatto
      KeyField = 'cod_tipo_cont'
      ListFieldIndex = 0
      DataField = 'TIPO'
      DataSource = dsoEdit
      TabOrder = 1
      Color = clWindow
      FieldLabel = 'Tipo contatto'
    end
    object dbedtContattoCon: TUniDBEdit
      Left = 21
      Top = 98
      Width = 516
      Height = 22
      Hint = ''
      DataField = 'referente'
      DataSource = dsoEdit
      TabOrder = 4
      FieldLabel = 'Contatto con'
    end
    object dbedtMotivo: TUniDBEdit
      Left = 21
      Top = 61
      Width = 516
      Height = 22
      Hint = ''
      DataField = 'MOTIVO'
      DataSource = dsoEdit
      TabOrder = 3
      FieldLabel = 'Motivo'
    end
    object dtpckDataRilascio: TUniDBDateTimePicker
      Left = 290
      Top = 24
      Width = 247
      Hint = ''
      DataField = 'DATA'
      DataSource = dsoEdit
      DateTime = 44147.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 2
      FieldLabel = 'Data'
    end
    object undbmNoteContatto: TUniDBMemo
      Left = 21
      Top = 137
      Width = 516
      Height = 97
      Hint = ''
      DataField = 'testo'
      DataSource = dsoEdit
      TabOrder = 5
      FieldLabel = 'Note'
      FieldLabelAlign = laTop
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 328
  end
  inherited dsoEdit: TDataSource
    DataSet = DMdatiAgeClienti.QContattoCliente
    Left = 280
    Top = 27
  end
  inherited untvmglstIcons: TUniNativeImageList
    Left = 372
    Top = 38
  end
  object dsTipoContatto: TDataSource
    DataSet = DMdatiAgeClienti.QTipoContatto
    OnStateChange = dsoEditStateChange
    Left = 432
    Top = 35
  end
end
