unit UcausaliAnnullamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions,
  Vcl.ActnList, uniPanel, uniBasicGrid, uniDBGrid, uniButton, uniBitBtn,
  uniSpeedButton, uniLabel, uniGUIBaseClasses;

type
  TFcausaliAnnullamento = class(TFMasterElenco)
    procedure actChiudiExecute(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoSelectionChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FcausaliAnnullamento: TFcausaliAnnullamento;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UdmdatiAge;

function FcausaliAnnullamento: TFcausaliAnnullamento;
begin
  Result := TFcausaliAnnullamento(UniMainModule.GetFormInstance(TFcausaliAnnullamento));
end;

procedure TFcausaliAnnullamento.actChiudiExecute(Sender: TObject);
begin
  inherited;
  // abbandona senza selezionare

end;

procedure TFcausaliAnnullamento.grdElencoSelectionChange(Sender: TObject);
begin
  SelectedCode        := dsGrid.dataset.fieldbyname('COD_CAUSALE').asinteger;
  SelectedDescription := dsGrid.dataset.fieldbyname('descrizione').AsString;
end;

procedure TFcausaliAnnullamento.UniFormCreate(Sender: TObject);
begin
  inherited;
  UniMainModule.archivio := 'CAA';

  inherited;
  self.DMMaster  := (UniMainModule.DMdatiAge);
  dsGrid.dataset := UniMainModule.DMdatiAge.Qcausali;

  UniMainModule.DMdatiAge.Qcausali.Open;
  // WindowState       := wsMaximized;
  btnSeleziona.Visible         := True;
  btnChiudi.Action             := actChiudi;
  actSelezionaDaElenco.Visible := True;
  actChiudi.Visible            := True;

end;

end.
