object FDatiPolizzaSostituita: TFDatiPolizzaSostituita
  Left = 0
  Top = 0
  ClientHeight = 363
  ClientWidth = 569
  Caption = 'Dati polizza sosttuita'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 13
  object btnRicalcoloPolSostBconferma: TUniBitBtn
    Left = 202
    Top = 319
    Width = 111
    Height = 35
    Hint = ''
    Enabled = False
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000202020404040
      4040404040404040404040404040404040404040404040404040404040404040
      404040404040402020207F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0909090808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0309030008000608060808080B0
      B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      30CF30008000008000008000608060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800000800000800060
      8060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      00BF00008000008000008000008000008000608060909090C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800020DF20009F0000
      8000208020808080A0A0A0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
      90CF9000BF0000800080808030EF30009F00008000208020808080B0B0B0C0C0
      C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C090CF9000DF00A0A0A0C0C0C030
      EF30009F00008000608060808080B0B0B0C0C0C0C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F000080006080609090
      90C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C030EF30009F00208020808080A0A0A0C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F002080
      20808080C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
      C0C0C0C0C0C0C0C0C0C0C030EF30009F00608060C0C0C04040407F7F7FC0C0C0
      C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C060DF
      6090B090C0C0C04040403F3F3F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
      7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F202020}
    Caption = ' Conferma'
    ModalResult = 1
    ParentFont = False
    Font.Height = -13
    Font.Style = [fsBold]
    TabOrder = 11
  end
  object btnRicalcoloPolSostBabbandona: TUniBitBtn
    Left = 319
    Top = 319
    Width = 111
    Height = 35
    Hint = ''
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF404040606060808080808080808080606060404040FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF20202020206020209F0000BF00
      00BF0000BF20209F404080808080606060202020FFFFFFFFFFFFFFFFFFFFFFFF
      00002020209F0000FF0000FF0000FF0000FF0000FF0000FF0000FF20209F6060
      80808080202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
      00FF0000FF0000FF0000FF0000FF0000DF606080606060FFFFFFFFFFFF00007F
      0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
      FF20209F8080804040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
      00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF40408060606000007F0000FF
      0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
      FF0000FF20209F8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
      FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF8080800000BF0000FF
      0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000
      FF0000FF0000BF8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
      FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF60606000007F0000FF
      0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
      FF0000FF20209F4040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
      00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF202060FFFFFFFFFFFF00007F
      0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
      FF20209F202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
      00FF0000FF0000FF0000FF0000FF0000DF000020FFFFFFFFFFFFFFFFFFFFFFFF
      00002000007F0000FF0000FF0000FF0000FF0000FF0000FF0000FF00007F0000
      20FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00004000007F0000BF00
      00BF0000BF00007F000040FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    Caption = ' Abbandona'
    ModalResult = 7
    ParentFont = False
    Font.Height = -13
    Font.Style = [fsBold]
    TabOrder = 12
  end
  object dtPckDecorrenzaPolizzaNew: TUniDateTimePicker
    Left = 8
    Top = 46
    Width = 241
    Hint = ''
    DateTime = 44130.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 1
    FieldLabel = 'Decorrenza pol. nuova'
    FieldLabelWidth = 130
  end
  object dtPckScadenzaPolizzaSost: TUniDateTimePicker
    Left = 279
    Top = 85
    Width = 241
    Hint = ''
    DateTime = 44130.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 4
    FieldLabel = 'Scadenza pol. sost.'
    FieldLabelWidth = 130
  end
  object dtPckDecorrenzaPolizzaSost: TUniDateTimePicker
    Left = 8
    Top = 85
    Width = 241
    Hint = ''
    DateTime = 44130.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 3
    FieldLabel = 'Decorrenza pol. sost.'
    FieldLabelWidth = 130
  end
  object edtNPolSostituita: TUniEdit
    Left = 279
    Top = 46
    Width = 241
    Hint = ''
    Text = 'edtNPolSostituita'
    TabOrder = 2
    TabStop = False
    ReadOnly = True
    FieldLabel = 'Numero pol. sostituita'
    FieldLabelWidth = 140
  end
  object cbbFrazionamento: TUniComboBox
    Left = 8
    Top = 8
    Width = 217
    Hint = ''
    Text = 'cbbFrazionamento'
    Items.Strings = (
      'Annuale'
      'Semestrale'
      'Quadrimestrale'
      'Trimestrale')
    TabOrder = 0
    FieldLabel = 'Frazionam.'
    IconItems = <>
  end
  object nedtLastPremioPagato: TUniNumberEdit
    Left = 8
    Top = 127
    Width = 241
    Hint = ''
    Alignment = taRightJustify
    ParentFont = False
    TabOrder = 5
    FieldLabel = 'Ultimo premio pagato'
    FieldLabelWidth = 130
    DecimalSeparator = ','
  end
  object nedtPercentualeTasse: TUniNumberEdit
    Left = 279
    Top = 127
    Width = 241
    Hint = ''
    Alignment = taRightJustify
    TabOrder = 6
    FieldLabel = 'Percentuale tasse'
    FieldLabelWidth = 130
    DecimalSeparator = ','
  end
  object dtPckPagDa: TUniDateTimePicker
    Left = 8
    Top = 168
    Width = 241
    Hint = ''
    DateTime = 44130.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 7
    FieldLabel = 'Ultimo pagamento dal'
    FieldLabelWidth = 130
  end
  object dtPckPagA: TUniDateTimePicker
    Left = 279
    Top = 168
    Width = 241
    Hint = ''
    DateTime = 44130.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 8
    FieldLabel = 'Ultimo pagamento al'
    FieldLabelWidth = 130
    OnChangeValue = dtPckPagAChangeValue
  end
  object grpbxRimborsoContraente: TUniGroupBox
    Left = 8
    Top = 206
    Width = 529
    Height = 65
    Hint = ''
    Caption = 'Rimborso Contraente'
    TabOrder = 9
    object nedtGiorniRimborso: TUniNumberEdit
      Left = 3
      Top = 19
      Width = 238
      Hint = ''
      Alignment = taRightJustify
      ParentFont = False
      TabOrder = 1
      FieldLabel = 'Giorni da rimborsare'
      FieldLabelWidth = 140
      DecimalSeparator = ','
    end
    object nedtNettoRimborsoContraente: TUniNumberEdit
      Left = 271
      Top = 19
      Width = 241
      Hint = ''
      Alignment = taRightJustify
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsBold]
      TabOrder = 2
      FieldLabel = 'Netto da rimborsare'
      FieldLabelWidth = 140
      DecimalSeparator = ','
    end
  end
  object nedtStornoProvvigionale: TUniNumberEdit
    Left = 8
    Top = 279
    Width = 217
    Hint = ''
    Visible = False
    Alignment = taRightJustify
    TabOrder = 10
    FieldLabel = 'Storno provvigionale'
    FieldLabelWidth = 140
    DecimalSeparator = ','
  end
  object UniBitBtnCalcola: TUniBitBtn
    Left = 24
    Top = 320
    Width = 137
    Height = 34
    Hint = ''
    Caption = 'Calcola rimborso'
    ParentFont = False
    Font.Height = -13
    Font.Style = [fsBold]
    TabOrder = 13
    OnClick = UniBitBtnCalcolaClick
  end
end
