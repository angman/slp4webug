unit UStampaPolizzaTP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, libSLP,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, uniGUIBaseClasses, uniImageList, Data.DB,
  System.Actions, Vcl.ActnList, uniRadioGroup, uniDBRadioGroup, uniCheckBox, uniDBCheckBox,
  uniMultiItem, uniComboBox, uniDBComboBox, uniEdit, uniDateTimePicker, uniDBDateTimePicker,
  uniDBEdit, uniBitBtn, uniPageControl, uniPanel, uniButton, uniSpeedButton, uniLabel, uniMemo,
  uniListBox, uniGroupBox, uniScreenMask, uniDBLookupComboBox, Vcl.Menus,
  uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaTP = class(TFStampaBasePolizza)
    pgAssicurato: TUniTabSheet;
    pcOggettoAssicurato: TUniPageControl;
    pgOggettoAssicurato: TUniTabSheet;
    pnlpncontaier: TUniContainerPanel;
    pnlAssicurato: TUniPanel;
    dbedtDenominazione: TUniDBEdit;
    dbedtCodiceFiscale: TUniDBEdit;
    dbedtPatente: TUniDBEdit;
    dbedtRilasciataDa: TUniDBEdit;
    dtpckDataRilascioPat: TUniDBDateTimePicker;
    dtpckDataScadenzaPat: TUniDBDateTimePicker;
    cbbCategoriaPat: TUniDBComboBox;
    pgFamiliari: TUniTabSheet;
    dbedtNome1: TUniDBEdit;
    dbedtPatente1: TUniDBEdit;
    dbedtCategoria1: TUniDBEdit;
    dbedtCategoria2: TUniDBEdit;
    dbedtNome2: TUniDBEdit;
    dbedtPatente2: TUniDBEdit;
    dbedtNome3: TUniDBEdit;
    dbedtPatente3: TUniDBEdit;
    dbedtCategoria3: TUniDBEdit;
    dbedtNome4: TUniDBEdit;
    dbedtPatente4: TUniDBEdit;
    dbedtCategoria4: TUniDBEdit;
    pgMassimaliGaranzie: TUniTabSheet;
    cbbMassimali: TUniDBComboBox;
    cbbRimbSpesePatPunti: TUniDBComboBox;
    dbgrpForma: TUniDBRadioGroup;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    ckbxAppendiceControversie: TUniDBCheckBox;
    ckbxDanniSubDaFamiliariConv: TUniDBCheckBox;
    ckbxbProblematicaRCA: TUniDBCheckBox;
    ckbxPerizieParte: TUniDBCheckBox;
    ckbxLegaleDomiciliataio: TUniDBCheckBox;
    ckbxProtezioneFamiglia: TUniDBCheckBox;
    dbedtNumeroAssicurati: TUniDBNumberEdit;
    ckbxRecuperoDanni: TUniDBCheckBox;
    ckbxDifPenVitaPrivata: TUniDBCheckBox;
    rgGrpPatenteHa: TUniRadioGroup;
    UniDBNumberEdit1: TUniDBNumberEdit;
    UniDBNumberEdit2: TUniDBNumberEdit;
    UniDBNumberEdit3: TUniDBNumberEdit;
    UniDBNumberEdit4: TUniDBNumberEdit;
    UniDBNumberEdit5: TUniDBNumberEdit;
    UniDBNumberEdit6: TUniDBNumberEdit;
    UniDBNumberEdit7: TUniDBNumberEdit;
    UniDBNumberEdit8: TUniDBNumberEdit;
    UniDBNumberEdit9: TUniDBNumberEdit;
    UniDBNumberEdit10: TUniDBNumberEdit;
    UniDBNumberEdit11: TUniDBNumberEdit;
    UniDBNumberEdit12: TUniDBNumberEdit;
    UniDBNumberEdit13: TUniDBNumberEdit;
    UniScreenMask1: TUniScreenMask;
    UniDBNumberEdit14: TUniDBNumberEdit;
    UniLabel1: TUniLabel;
    procedure UniFormCreate(Sender: TObject);
    procedure ckbxProtezioneFamigliaClick(Sender: TObject);
    procedure dbedtNumeroAssicuratiChange(Sender: TObject);
    procedure dbgrpFormaChangeValue(Sender: TObject);
    procedure rgGrpPatenteHaClick(Sender: TObject);
    procedure ckbxRecuperoDanniClick(Sender: TObject);
    procedure ckbxDifPenVitaPrivataClick(Sender: TObject);
    procedure ckbxDanniSubDaFamiliariConvClick(Sender: TObject);
    procedure actRiprendiPolizzaExecute(Sender: TObject);
  private
    { Private declarations }
    procedure RiallineaMaxGaranzie;
    procedure AzzeraEstensioni;
  protected
    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); override;

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;

  end;

function FStampaPolizzaTP: TFStampaPolizzaTP;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiTempPolizza, UDMDatiTempPolizzaTP;

function FStampaPolizzaTP: TFStampaPolizzaTP;
begin
  Result := TFStampaPolizzaTP(UniMainModule.GetFormInstance(TFStampaPolizzaTP));
end;

procedure TFStampaPolizzaTP.actRiprendiPolizzaExecute(Sender: TObject);
begin
  inherited;
  rgGrpPatenteHa.ItemIndex := TDMDatiTempPolizzaTP(UniMainModule.DMDatiTempPolizza).LeggiCaratteristicaPatente;
end;

procedure TFStampaPolizzaTP.AzzeraEstensioni;
begin
   // azzera le estensioni eventualmente richiamate
   if ckbxRecuperoDanni.Checked then
   begin
      ckbxRecuperoDanni.Checked:=false;
      dsoEdit.DataSet.FieldByName('recuperoDanni').AsBoolean:=False;
   end;
   if ckbxPerizieParte.Checked then
   begin
      ckbxPerizieParte.Checked:=false;
      dsoEdit.DataSet.FieldByName('perizieparte').AsBoolean:=False;
   end;
   if ckbxbProblematicaRCA.Checked then
   begin
      ckbxbProblematicaRCA.Checked:=false;
      dsoEdit.DataSet.FieldByName('ProblematicheRCA').AsBoolean:=False;
   end;
   if ckbxProtezioneFamiglia.Checked then
   begin
      ckbxProtezioneFamiglia.Checked:=false;
      dsoEdit.DataSet.FieldByName('ProtezioneFamiglia').AsBoolean:=False;
      dsoEdit.DataSet.FieldByName('NumeroAssicurati').AsInteger:=0;
   end;
   if ckbxAppendiceControversie.Checked then
   begin
      ckbxAppendiceControversie.Checked:=false;
      dsoEdit.DataSet.FieldByName('AppControversie').AsBoolean:=False;
   end;
   if ckbxLegaleDomiciliataio.Checked then
   begin
      ckbxLegaleDomiciliataio.Checked:=false;
      dsoEdit.DataSet.FieldByName('LegaleDomiciliatario').AsBoolean:=False;
   end;
   if ckbxDanniSubDaFamiliariConv.Checked then
   begin
      ckbxDanniSubDaFamiliariConv.Checked:=false;
      dsoEdit.DataSet.FieldByName('RecuperoDanniFam').AsBoolean:=False;
   end;
   if ckbxDifPenVitaPrivata.Checked then
   begin
      ckbxDifPenVitaPrivata.Checked:=false;
      dsoEdit.DataSet.FieldByName('DifPenVPrivata').AsBoolean:=False;
   end;

   rgGrpPatenteHa.ItemIndex:=0;
   if cbbRimbSpesePatPunti.ItemIndex>=0 then
   begin
      cbbRimbSpesePatPunti.ItemIndex:=-1;
      dsoEdit.DataSet.FieldByName('SiglaRimborsoSpesePatPunti').AsString:='';
   end;

end;

procedure TFStampaPolizzaTP.ckbxDanniSubDaFamiliariConvClick(Sender: TObject);
begin
  inherited;
  if not ckbxRecuperoDanni.Checked then
  begin
    ckbxDanniSubDaFamiliariConv.Checked := False;
    ckbxDanniSubDaFamiliariConv.DataSource.DataSet.FieldByName('RecuperoDanniFam').AsBoolean := False;
    ShowMessage('Per attivare questa estensione bisogna prima attivare l''estensione "Recupero Danni"');
  end;

  if (dbedtPIva.Text > '') or (isdigit(Copy(dbedtCodFiscale.Text, 1, 1))) then
  begin
    ckbxDanniSubDaFamiliariConv.Checked := False;
    ckbxDanniSubDaFamiliariConv.DataSource.DataSet.FieldByName('RecuperoDanniFam').AsBoolean := False;
    ShowMessage('Non � possibile utilizzare questa estensione in presenza di partita IVA.');
  end;
end;

procedure TFStampaPolizzaTP.ckbxDifPenVitaPrivataClick(Sender: TObject);
begin
  inherited;
  if (dbedtPIva.Text > '') or (isdigit(Copy(dbedtCodFiscale.Text, 1, 1))) then
  begin
    ckbxDifPenVitaPrivata.Checked := False;
    ckbxDifPenVitaPrivata.DataSource.DataSet.FieldByName('DifPenVPrivata').AsBoolean := False;
    ShowMessage('Non � possibile utilizzare questa estensione in presenza di partita IVA.');
  end;

end;

procedure TFStampaPolizzaTP.ckbxProtezioneFamigliaClick(Sender: TObject);
begin

  if (dbedtPIva.Text > '') or (isdigit(Copy(dbedtCodFiscale.Text, 1, 1))) then
  begin
    ckbxProtezioneFamiglia.Checked := False;
    ckbxProtezioneFamiglia.DataSource.DataSet.FieldByName('ProtezioneFamiglia').AsBoolean := False;
    ShowMessage('Non � possibile utilizzare questa estensione in presenza di partita IVA.');
  end;

  dbedtNumeroAssicurati.Enabled := ckbxProtezioneFamiglia.Checked;
  // pgFamiliari.TabVisible        := ckbxProtezioneFamiglia.Checked;
  if not ckbxProtezioneFamiglia.Checked then
  begin
    pcOggettoAssicurato.ActivePage := pgAssicurato;
    dbedtNumeroAssicurati.Value    := 0;
    // pgFamiliari.TabVisible         := False;
    dbgrpForma.Enabled             := True;
    cbbCategoriaPat.Enabled        := True;
    rgGrpPatenteHa.Enabled         := True;
  end
  else
  begin
    if dbgrpForma.Values[dbgrpForma.ItemIndex] = 'A' then
    begin
      begin
        if Sender <> nil then
          dbedtNumeroAssicurati.Value := 1;
      end;
      // pgFamiliari.TabVisible  := True;
      dbgrpForma.Enabled      := True;
      cbbCategoriaPat.Enabled := False;
      rgGrpPatenteHa.Enabled  := False;
      if rgGrpPatenteHa.ItemIndex > 0 then
        ShowMessage('Con l''estensione "Protezione Famiglia" non � possibile avere una patente con qualifica.');
      rgGrpPatenteHa.ItemIndex := 0;
    end;
  end;
  dbedtNumeroAssicuratiChange(nil);

end;

procedure TFStampaPolizzaTP.ckbxRecuperoDanniClick(Sender: TObject);
begin
  // ckbxAppendiceControversie.Enabled := ckbxRecuperoDanni.Checked;
  // ckbxbProblematicaRCA.Enabled      := ckbxRecuperoDanni.Checked;
  // if not ckbxRecuperoDanni.Checked then
  // begin
  // ckbxAppendiceControversie.Checked := False;
  // ckbxbProblematicaRCA.Checked      := False;
  // end;
end;

procedure TFStampaPolizzaTP.dbedtNumeroAssicuratiChange(Sender: TObject);
var
  i: Integer;

  procedure EnableDisableFamiliari(Enable: Boolean);
  begin
    TUniDBEdit(FindComponent('DBEdtNome' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtPatente' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtCategoria' + i.ToString)).Enabled := Enable;

  end;

begin
  for i := 1 to 4 do
    EnableDisableFamiliari(i <= dbedtNumeroAssicurati.Value);

end;

procedure TFStampaPolizzaTP.dbgrpFormaChangeValue(Sender: TObject);
var
  selectedForma: string;
begin

  selectedForma := dbgrpForma.Values[dbgrpForma.ItemIndex];
  if (selectedForma = 'A') or (not ckbxProtezioneFamiglia.Checked) then
  begin
    cbbMassimali.DataField := '';
    try
      UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
        selectedForma);
    finally
      cbbMassimali.DataField := 'SiglaMassimale';
    end;
  end
  else
  begin
    dbgrpForma.ItemIndex := 0;
    ShowMessage('Non � possibile selezionare la forma B in presenza dell''estensione "Protezione famiglia".');
  end;
end;

procedure TFStampaPolizzaTP.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  AzzeraEstensioni;
  inherited;
  RiallineaMaxGaranzie;

end;

procedure TFStampaPolizzaTP.rgGrpPatenteHaClick(Sender: TObject);
begin
  TDMDatiTempPolizzaTP(UniMainModule.DMDatiTempPolizza).ImpostaCaratteristicaPatente
    (TEnumCaratteristicaPatente(rgGrpPatenteHa.ItemIndex));
end;

procedure TFStampaPolizzaTP.RiallineaMaxGaranzie;
begin
  ckbxRecuperoDanniClick(nil);
  dbedtNumeroAssicurati.Enabled := ckbxProtezioneFamiglia.Checked;
  ckbxProtezioneFamigliaClick(nil);

  rgGrpPatenteHa.ItemIndex := Integer(TDMDatiTempPolizzaTP(UniMainModule.DMDatiTempPolizza).getCaratteristicaPatente);

end;

procedure TFStampaPolizzaTP.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited SetCliente(ASelectedItem, ASelectedDescription);
end;

procedure TFStampaPolizzaTP.UniFormCreate(Sender: TObject);
begin
  inherited;
  // ckbxAppendiceControversie.Enabled := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICECC');
  // ckbxTacitoRinnovo.Enabled         := UniMainModule.DMdatiAge.isExtensionEnabled('SENZA_T_R');

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;

  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaForma.AsString);
  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  TDMDatiTempPolizzaTP(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.AsString :=
    Copy(cbbMassimali.Text, 1, 3);

  // mb 18112020
  pgFamiliari.TabVisible       := True;  //False;
  pcDatiEreditabili.ActivePage := pgContraente;
end;

initialization

RegisterClass(TFStampaPolizzaTP);

end.
