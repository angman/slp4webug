unit UPrinthouseSLP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses,
  uniURLFrame, uniSplitter, uniGUIApplication, uniScreenMask;

type
  TFPrintHouseSLP = class(TFMasterElenco)
    actVisualizzaDocumento: TAction;
    actEsportaDocumento: TAction;
    UniScreenMask1: TUniScreenMask;
    procedure UniFormCreate(Sender: TObject);
    // procedure grdElencoFieldImageURL(const Column: TUniDBGridColumn; const AField: TField; var OutImageURL: string);
    procedure actVisualizzaDocumentoExecute(Sender: TObject);
    procedure actEsportaDocumentoExecute(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure grdElencoBodyDblClick(Sender: TObject);
  private
    // FComingFromLogin: Boolean;
    LCanPrint: Boolean;
    PDFFolderPath: string;
    PDFUrl: string;
    FCodDocumento: Integer;
    { Private declarations }
    function getDocumentFileName(ACodDocument: Integer): string;
    // procedure SetComingFromLogin(const Value: Boolean);

  public
    { Public declarations }
    // property ComingFromLogin: Boolean write SetComingFromLogin;
    class procedure ShowPrintHousePolizza(UniApplication: TUniGUIApplication; NPolizza: string;
      CanPrint, CanExport: Boolean; CodDoc: Integer = 0);
    property CodDocumento: Integer read FCodDocumento write FCodDocumento;
  end;

function FPrintHouseSLP: TFPrintHouseSLP;

implementation

{$R *.dfm}

uses
  MainModule, UDMDatiReport, ServerModule, System.IOUtils, System.StrUtils, UStampePDFViewer, System.Math;

function FPrintHouseSLP: TFPrintHouseSLP;
begin
  Result := TFPrintHouseSLP(UniMainModule.GetFormInstance(TFPrintHouseSLP));
end;

procedure TFPrintHouseSLP.actEsportaDocumentoExecute(Sender: TObject);
var
  afilename: string;
begin
  afilename := getDocumentFileName(dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
  if (afilename <> '') then // and FileExists(afilename) then
  begin
    UniSession.SendFile(PDFFolderPath + '\' + afilename);
    UniMainModule.DMDatiReport.UpdDataStampaDoc(dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
    TFile.delete(PDFFolderPath + '\' + afilename);
  end;
end;

procedure TFPrintHouseSLP.actVisualizzaDocumentoExecute(Sender: TObject);
var
  afilename: string;
begin
  CodDocumento := IfThen(CodDocumento <> 0, CodDocumento, dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
  try
    grdElenco.ShowMask('Attendere, creazione anteprima in corso ...');
    afilename := getDocumentFileName(CodDocumento);
    grdElenco.HideMask;
    if (afilename <> '') then // and FileExists(afilename) then
    begin
      TFPDFViewer.ShowReport(UniApplication, afilename, PDFUrl, PDFFolderPath, False, True);
      // UniMainModule.DMDatiReport.UpdDataVisualizzazioneDoc(dsGrid.DataSet.FieldByName('CODICE_DOC').AsInteger);
      dsGrid.DataSet.DisableControls;
      dsGrid.DataSet.Close;
      dsGrid.DataSet.Open;
      dsGrid.DataSet.EnableControls;
      // TFile.delete(PDFFolderPath + '\' + afilename);
    end;
  finally
    CodDocumento := 0;
  end;

end;

function TFPrintHouseSLP.getDocumentFileName(ACodDocument: Integer): string;
begin

  Result := UniMainModule.DMDatiReport.SalvaTempDocument(PDFFolderPath, ACodDocument, LCanPrint, tdPrintHouse);

end;

procedure TFPrintHouseSLP.grdElencoBodyDblClick(Sender: TObject);
begin
  inherited;

end;

// procedure TFPrintHouseSLP.SetComingFromLogin(const Value: Boolean);
// begin
// FComingFromLogin := Value;
// UniMainModule.DMDatiReport.getDocuments(grdElenco.DataSource.DataSet, FComingFromLogin);
// end;

// procedure TFPrintHouseSLP.grdElencoFieldImageURL(const Column: TUniDBGridColumn; const AField: TField;
// var OutImageURL: string);
//
// var
// imagesPath: string;
//
// begin
// imagesPath := TPath.Combine(UniServerModule.FilesFolderURL, 'Images/');
//
// if SameText(AField.FieldName, 'DATA_LETTURA') then
// begin
// if AField.IsNull then
// OutImageURL := imagesPath + 'flag_mark_red.png'
// else
// OutImageURL := imagesPath + 'flag_mark_green.png';
// end
// else
// inherited;
// end;

class procedure TFPrintHouseSLP.ShowPrintHousePolizza(UniApplication: TUniGUIApplication; NPolizza: string;
  CanPrint, CanExport: Boolean; CodDoc: Integer);
begin
  with TFPrintHouseSLP.Create(UniApplication) do
  begin
    btnModify.visible := CanExport;
    LCanPrint         := CanPrint;
    CodDocumento      := CodDoc;
    if UniMainModule.DMDatiReport.OpenPrinthousePolizza(NPolizza) then
      ShowModal
    else
    begin
      ShowMessage('Non sono presenti immagini.');
      Close;
    end;
  end;
end;

procedure TFPrintHouseSLP.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFPrintHouseSLP.UniFormCreate(Sender: TObject);
begin
  inherited;
  PDFUrl            := UniServerModule.FilesFolderURL + 'PDF/';
  PDFFolderPath     := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  dsGrid.DataSet    := UniMainModule.DMDatiReport.QdocHouse;
  btnModify.Action  := actEsportaDocumento;
  btnModify.Enabled := UniMainModule.VerificaAutorizzazione('STP', 'EXP');
  btnInsert.Action  := actVisualizzaDocumento;
  btnInsert.Enabled := UniMainModule.VerificaAutorizzazione('STP', 'VIS');
  UniMainModule.aggiornaPosizione('STP', 'VIS', 'Elenco documenti printhouse');
  dsGrid.DataSet := UniMainModule.DMDatiReport.QDocPrintHousePolizza;
end;

end.
