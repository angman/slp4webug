unit UCollaboratoreEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, uniPanel, uniPageControl, uniEdit,
  uniDBEdit, uniMultiItem, uniComboBox, uniDBComboBox, uniDateTimePicker, uniDBDateTimePicker,
  uniImageList, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  uniMemo, uniDBMemo, uniFileUpload, unimFileUpload, uniBasicGrid, uniDBGrid, uniDBNavigator, uniDBLookupComboBox;

type
  TFCollaboratoreEdit = class(TFMasterEdit)
    pnlEdit: TUniSimplePanel;
    pcCollaboratore: TUniPageControl;
    pgAnagrafica: TUniTabSheet;
    pgNote: TUniTabSheet;
    pgNormeContabili: TUniTabSheet;
    dbedtNome: TUniDBEdit;
    dbedtSigla: TUniDBEdit;
    cbbTipo: TUniDBComboBox;
    dbedtDescrizione: TUniDBEdit;
    lblNotaSedeOperativa: TUniLabel;
    dbedtIndirizzo: TUniDBEdit;
    dbedtCap1: TUniDBEdit;
    dbedtCitta1: TUniDBEdit;
    dbedtProvincia1: TUniDBEdit;
    dbedtCodFisc: TUniDBEdit;
    dbedtPIva: TUniDBNumberEdit;
    dbedtTelefono: TUniDBEdit;
    dbedtMail: TUniDBEdit;
    dbedtPec: TUniDBEdit;
    dbedtNRui: TUniDBEdit;
    dtpckDataIscrizRui: TUniDBDateTimePicker;
    dbedtRefrerente: TUniDBEdit;
    actSelezioneProduttore: TAction;
    actClearProduttore: TAction;
    fdmtblTipoCollaboratore: TFDMemTable;
    undbmNota: TUniDBMemo;
    dbedtPagamenti: TUniDBEdit;
    dbedtRendiconti: TUniDBEdit;
    dbedtPercProvvAg: TUniDBNumberEdit;
    dbedtPercRitAcc: TUniDBNumberEdit;
    undbmNoteContabili: TUniDBMemo;
    pgAllegati: TUniTabSheet;
    pnlButtons: TUniSimplePanel;
    actVisualizzaAllegato: TAction;
    btnVisualizzaAllegato: TUniSpeedButton;
    grdAllegati: TUniDBGrid;
    dsAllegati: TDataSource;
    UniDBNavigator1: TUniDBNavigator;
    UniHiddenPanel1: TUniHiddenPanel;
    UniEditDescrAllegato: TUniEdit;
    undtmpckrDtInizioValidita: TUniDateTimePicker;
    cbbTipoAlegato: TUniDBLookupComboBox;
    dsTipoAllegato: TDataSource;
    btnCaricaAllegato: TUniFileUploadButton;
    cbbPromoter: TUniDBLookupComboBox;
    dsLinkProdutt: TDataSource;
    procedure UniFormCreate(Sender: TObject);
    procedure actSelezioneProduttoreExecute(Sender: TObject);
    procedure actClearProduttoreExecute(Sender: TObject);
    procedure btnCaricaAllegatoCompleted(Sender: TObject; AStream: TFileStream);
    procedure dsAllegatiDataChange(Sender: TObject; Field: TField);
    procedure actVisualizzaAllegatoExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FCollaboratoreEdit: TFCollaboratoreEdit;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMAgeCollaboratori;

function FCollaboratoreEdit: TFCollaboratoreEdit;
begin
  Result := TFCollaboratoreEdit(UniMainModule.GetFormInstance(TFCollaboratoreEdit));
end;

procedure TFCollaboratoreEdit.actClearProduttoreExecute(Sender: TObject);
begin
  // dsoedit.DataSet.fieldbyname('COD_PRODUTTORE').clear;
  // unedtPromoter.Text := ''; // ---
end;

procedure TFCollaboratoreEdit.actSelezioneProduttoreExecute(Sender: TObject);
begin
  // ------
end;

procedure TFCollaboratoreEdit.actVisualizzaAllegatoExecute(Sender: TObject);
begin
  UniMainModule.DMdatiAgeCollaboratori.VisualizzaAllegato;
end;

procedure TFCollaboratoreEdit.btnCaricaAllegatoCompleted(Sender: TObject; AStream: TFileStream);
begin
  UniMainModule.DMdatiAgeCollaboratori.CaricaAllegato(AStream);
end;

procedure TFCollaboratoreEdit.dsAllegatiDataChange(Sender: TObject; Field: TField);
begin
  btnCaricaAllegato.Enabled     := not dsAllegati.DataSet.IsEmpty;
  actVisualizzaAllegato.Enabled := not(dsAllegati.DataSet.IsEmpty or dsAllegati.DataSet.fieldbyname('TESTO').IsNull);

end;

procedure TFCollaboratoreEdit.UniFormCreate(Sender: TObject);
begin
  dsoedit.DataSet := UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore;
  inherited;
  // if dsoedit.DataSet.State <> dsInsert then
  UniMainModule.DMdatiAgeCollaboratori.ApriLstProdutt
    (UniMainModule.DMdatiAgeCollaboratori.QeditCollaboratore.fieldbyname('COD_PRODUTTORE').AsInteger);
  // else
  // begin
  // cbbPromoter.ReadOnly := True;
  // cbbPromoter.Color := clYellow;
  // end;
  dsAllegati.DataSet.Open;
  dsTipoAllegato.DataSet.Open;
  pcCollaboratore.ActivePage := pgAnagrafica;
end;

initialization

RegisterClass(TFCollaboratoreEdit);

end.
