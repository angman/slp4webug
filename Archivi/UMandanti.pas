unit UMandanti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniPanel,
  uniBasicGrid, uniDBGrid, uniButton, uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, UDMDatiMandante;

type
  TFMandanti = class(TFMasterElenco)
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FMandanti: TFMandanti;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function FMandanti: TFMandanti;
begin
  Result := TFMandanti(UniMainModule.GetFormInstance(TFMandanti));
end;

procedure TFMandanti.UniFormCreate(Sender: TObject);
begin
  inherited;
  dsGrid.DataSet := UniMainModule.DMDatiMandante.QCompagnie;
  UniMainModule.DMDatiMandante.QCompagnie.Open;
end;

end.
