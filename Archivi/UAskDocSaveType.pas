unit UAskDocSaveType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, uniGUIApplication, uniGUIDialogs,
  uniGUIClasses, uniGUIForm, uniButton, uniBitBtn, uniGUIBaseClasses, uniRadioGroup, UTipoSalvataggioDocumento,
  uniRadioButton, uniCheckBox, uniScreenMask, System.DateUtils ;

type
  TFAskDocSaveType = class(TUniForm)
    unrdgrpSaveDocType: TUniRadioGroup;
    btnBconferma: TUniBitBtn;
    btnBabbandona: TUniBitBtn;
    uncbxShowReport: TUniCheckBox;
    ScreeMask: TUniScreenMask;
    procedure btnBconfermaClick(Sender: TObject);
    procedure uncbxShowReportChange(Sender: TObject);
  private
    { Private declarations }
    FTipoDocumento: TTipoSalvataggio;
    FMostraReportPolizza: Boolean;

    constructor Create(Owner: TComponent; TipoSalvataggioDocumento: TTipoSalvataggio); Overload;
    procedure doVerIntermedia(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    class procedure ShowAskDocSaveType(UniApplication: TUniGUIApplication; TipoSalvataggioDocumento: TTipoSalvataggio;
      CallBackProc: TUniDialogCallBackProc);

    property TipoDocumentoToSave: TTipoSalvataggio read FTipoDocumento;
    property MostraReportPolizza: Boolean read FMostraReportPolizza;
  end;

implementation

uses MainModule, UCodiciErroriPolizza;
{$R *.dfm}
{ TFAskDocSaveType }

procedure TFAskDocSaveType.btnBconfermaClick(Sender: TObject);
begin
  if unrdgrpSaveDocType.Items.Count = 4 then
    case unrdgrpSaveDocType.ItemIndex of
      1:
        FTipoDocumento := tsNuovaBozza;
      2:
        FTipoDocumento := tsNuovoPreventivo;
      3:
        FTipoDocumento := tsPolizza;
    else
      ;
    end
  else
    case unrdgrpSaveDocType.ItemIndex of
      0:
        FTipoDocumento := tsNuovaBozza;
      1:
        FTipoDocumento := tsNuovoPreventivo;
      2:
        FTipoDocumento := tsPolizza;
    else
      ;
    end;

  // se il tipodocumento = tspolizza verifica se � stato istanziato chi intermedia la polizza
  // se non � stato istanziato chiedi conferma
  if (FTipoDocumento = tsPolizza) and (yearof(UniMainModule.DMDatiTempPolizza.fdmtblPolizzaDataEmissione.AsDateTime)<2000) then
  begin
     ShowMessage(MSG_DATA_EMISSIONE_TOO_LOW);
     ModalResult := mrNo;
  end else
  if (FTipoDocumento = tsPolizza) and (UniMainModule.DMDatiTempPolizza.fdmtblPolizzaIntermediataDa.asInteger=0) then
  begin
     // chiedi conferma del fatto che non si vuole indicare chi intermedia la polizza
     // mb 30102020 corretto frase in quanto dato non obbligatorio su modulo della coerenza
     MessageDlg('@@Vuoi confermare la polizza SENZA indicare la persona che INTERMEDIA il contratto ?', mtConfirmation, mbYesNo, doVerIntermedia);
  end else
     ModalResult := mrOk;
end;


procedure TFAskDocSaveType.doVerIntermedia(Sender: TComponent; AResult: Integer);
begin
   if AResult = mrYes then
   begin
     //btnBconferma.ShowMask('Attendere generazione documenti in corso');
     ModalResult := mrOk;
   end
   else ModalResult := mrNo;
end;


constructor TFAskDocSaveType.Create(Owner: TComponent; TipoSalvataggioDocumento: TTipoSalvataggio);
begin
  inherited Create(Owner);
  FTipoDocumento       := TipoSalvataggioDocumento;
  FMostraReportPolizza := True;
  unrdgrpSaveDocType.Items.Clear;
  case TipoSalvataggioDocumento of
    // tsNone, tsPolizza:
    // Self.unrdgrpSaveDocType.Controls[0].enabled := False;
    tsBozza:
      Self.unrdgrpSaveDocType.Items.Append('Bozza corrente');
    tsPreventivo:
      Self.unrdgrpSaveDocType.Items.Append('Preventivo corrente');
    tsPolizza:
      begin
        if not(UniMainModule.UtenteSoloFacSimileSLP = 'S') then
          Self.unrdgrpSaveDocType.Items.Append('Polizza effettiva');
      end
  else
    ;
  end;
  Self.unrdgrpSaveDocType.Items.Append('Nuova bozza');
  Self.unrdgrpSaveDocType.Items.Append('Nuovo preventivo');
  // questo solo se l'utenza � autorizzata a fare una polizza effettiva
  // devi estrarre questa informazione da codicinew e dal caricamento fatto dell'utente !!!!
  if not(UniMainModule.UtenteSoloFacSimileSLP = 'S') then
    Self.unrdgrpSaveDocType.Items.Append('Polizza effettiva');
  Self.unrdgrpSaveDocType.ItemIndex := 0;
end;



class procedure TFAskDocSaveType.ShowAskDocSaveType(UniApplication: TUniGUIApplication;
  TipoSalvataggioDocumento: TTipoSalvataggio; CallBackProc: TUniDialogCallBackProc);
begin
  TFAskDocSaveType.Create(UniApplication, TipoSalvataggioDocumento).ShowModal(CallBackProc);
end;

procedure TFAskDocSaveType.uncbxShowReportChange(Sender: TObject);
begin
  FMostraReportPolizza := uncbxShowReport.Checked;
end;

end.
