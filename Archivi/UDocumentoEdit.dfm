inherited FEditDocumento: TFEditDocumento
  ClientHeight = 436
  ClientWidth = 564
  Caption = 'Dati Documento'
  BorderStyle = bsDialog
  BorderIcons = []
  ExplicitWidth = 570
  ExplicitHeight = 465
  PixelsPerInch = 96
  TextHeight = 13
  object pnlEdit: TUniSimplePanel [0]
    Left = 0
    Top = 69
    Width = 564
    Height = 367
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    ExplicitLeft = 320
    ExplicitTop = 240
    ExplicitWidth = 256
    ExplicitHeight = 128
    object cbbTipoDocumento: TUniDBLookupComboBox
      Left = 21
      Top = 24
      Width = 247
      Hint = ''
      ListField = 'descrizione'
      ListSource = dsTipoDocumento
      KeyField = 'cod_tipo_doc'
      ListFieldIndex = 0
      DataField = 'TIPO_DOC'
      DataSource = dsoEdit
      TabOrder = 1
      Color = clWindow
      FieldLabel = 'Tipo documento'
    end
    object dbedtEstremiDocumento: TUniDBEdit
      Left = 21
      Top = 209
      Width = 516
      Height = 22
      Hint = ''
      DataField = 'ESTREMI_DOC'
      DataSource = dsoEdit
      TabOrder = 9
      FieldLabel = 'Estremi documento'
    end
    object dbedtLuogoRilascio: TUniDBEdit
      Left = 21
      Top = 98
      Width = 516
      Height = 22
      Hint = ''
      DataField = 'RILASCIATO_A'
      DataSource = dsoEdit
      TabOrder = 4
      FieldLabel = 'Luogo di rilascio'
    end
    object dbedtPeriodoRinnovo: TUniDBNumberEdit
      Left = 21
      Top = 172
      Width = 247
      Height = 22
      Hint = ''
      DataField = 'PERIODO_RINNOVO'
      DataSource = dsoEdit
      Alignment = taRightJustify
      TabOrder = 7
      MaxValue = 120.000000000000000000
      ShowTrigger = True
      FieldLabel = 'Periodo rinnovo'
      DecimalPrecision = 0
      DecimalSeparator = ','
    end
    object dbedtRilasciatoDa: TUniDBEdit
      Left = 21
      Top = 61
      Width = 516
      Height = 22
      Hint = ''
      DataField = 'RILASCIATO_DA'
      DataSource = dsoEdit
      TabOrder = 3
      FieldLabel = 'Rilasciato da'
    end
    object dtpckDataRilascio: TUniDBDateTimePicker
      Left = 290
      Top = 24
      Width = 247
      Hint = ''
      DataField = 'RILASCIATO_IL'
      DataSource = dsoEdit
      DateTime = 44147.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 2
      FieldLabel = 'Rilasciato il'
    end
    object dtpckPrimaScadenza: TUniDBDateTimePicker
      Left = 21
      Top = 135
      Width = 247
      Hint = ''
      DataField = 'PRIMA_SCADENZA'
      DataSource = dsoEdit
      DateTime = 44147.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 6
      FieldLabel = 'Prima scadenza'
    end
    object dtpckScadenza: TUniDBDateTimePicker
      Left = 290
      Top = 132
      Width = 247
      Hint = ''
      DataField = 'SCADENZA'
      DataSource = dsoEdit
      DateTime = 44147.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 5
      FieldLabel = 'Scadenza'
    end
    object undbmNote: TUniDBMemo
      Left = 21
      Top = 249
      Width = 516
      Height = 97
      Hint = ''
      DataField = 'NOTE'
      DataSource = dsoEdit
      TabOrder = 8
      FieldLabel = 'Note'
      FieldLabelAlign = laTop
    end
  end
  inherited pnlHead: TUniSimplePanel
    Width = 564
    ExplicitWidth = 554
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 564
    ExplicitWidth = 554
  end
  inherited actlstEditOperation: TActionList
    Left = 320
  end
  inherited dsoEdit: TDataSource
    Left = 264
    Top = 27
  end
  inherited untvmglstIcons: TUniNativeImageList
    Left = 372
    Top = 30
  end
  object dsTipoDocumento: TDataSource
    DataSet = DMdatiAgeClienti.QTipoDocumento
    OnStateChange = dsoEditStateChange
    Left = 456
    Top = 35
  end
end
