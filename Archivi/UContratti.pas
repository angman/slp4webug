unit UContratti;

interface

uses
  Windows, Messages, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses, UDMdatiAgePolizze,
  UdmdatiAge,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniEdit,
  uniRadioGroup;

type
  TFContratti = class(TFMasterElenco)
    cbbReferente: TUniDBLookupComboBox;
    UniSpeedButton1: TUniSpeedButton;
    actVisualizzaDocumenti: TAction;
    unedtContratente: TUniEdit;
    unmbrdtNPolizza: TUniNumberEdit;
    UniPanel1: TUniPanel;
    uRGstatoPolizze: TUniRadioGroup;
    unedtSiglaCollab: TUniEdit;
    actVisualizza: TAction;
    cbbCompContratti: TUniDBLookupComboBox;
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoSelectionChange(Sender: TObject);
    procedure actVisualizzaDocumentiExecute(Sender: TObject);
    procedure uRGstatoPolizzeClick(Sender: TObject);
    procedure actVisualizzaExecute(Sender: TObject);
    procedure grdElencoBodyDblClick(Sender: TObject);
    procedure grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
    procedure cbbCompContrattiChange(Sender: TObject);
    procedure grdElencoDrawColumnCell(Sender: TObject; ACol, ARow: Integer;
      Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
    procedure actInsertExecute(Sender: TObject);
  private
    FOnlySelection: Boolean;
    nomeColonna: string;
    xcodCompagnia: Integer;
    procedure setOnlySelection(const Value: Boolean);

    { Private declarations }
  public
    { Public declarations }
    property OnlySelection: Boolean read FOnlySelection write setOnlySelection;
  end;

function FContratti: TFContratti;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, System.SysUtils, UQueryElencoIntf,
  libreria, dbisamtb, uPrintHouseSLP, UQueryEditIntf;

function FContratti: TFContratti;
begin
  Result := TFContratti(UniMainModule.GetFormInstance(TFContratti));
end;

procedure TFContratti.actInsertExecute(Sender: TObject);
begin
  inherited;
  grdElenco.Refresh;
end;

procedure TFContratti.actVisualizzaDocumentiExecute(Sender: TObject);
begin
  inherited;
  // visualizza la polizza se presente in printhouse
  TFPrintHouseSLP.ShowPrintHousePolizza(uniApplication, dsGrid.dataset.fieldbyname('N_POLIZZA').asString, True, false);
end;

procedure TFContratti.actVisualizzaExecute(Sender: TObject);
begin
  if Assigned(EditPageClass) then
  begin
    UniMainModule.operazione := 'VIS';
    (DMMaster as IQueryEdit).PosizionaQuery(SelectedCode);
    with EditPageClass.Create(uniApplication) do
    begin
      DSoEdit.dataset.Edit;
      DatasetElenco := grdElenco.DataSource.dataset;
      ShowModal;
    end;
  end;

end;

procedure TFContratti.cbbCompContrattiChange(Sender: TObject);
begin
  if not(cbbCompContratti.Text > '') then
    xcodCompagnia := 0
  else
    xcodCompagnia := UniMainModule.DMdatiAge.QCompagnie.fieldbyname('cod_compagnia').AsInteger;

  UniMainModule.DMdatiAgePolizze.EseguiQuery(nomeColonna, uRGstatoPolizze.ItemIndex, '', 0, 0, 0, xcodCompagnia);
end;

procedure TFContratti.grdElencoBodyDblClick(Sender: TObject);
begin
  actVisualizza.Execute;
end;

procedure TFContratti.grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
begin
  // UniMainModule.DMdatiAgePolizze.EseguiQuery(Column.FieldName, uRGstatoPolizze.ItemIndex, '', 0, Integer(Direction));
  nomeColonna:=Column.FieldName;
  UniMainModule.DMdatiAgePolizze.EseguiQuery(Column.FieldName, uRGstatoPolizze.ItemIndex, '', 0, Integer(Direction),
                                             0,xcodCompagnia);

end;

procedure TFContratti.grdElencoDrawColumnCell(Sender: TObject; ACol,
  ARow: Integer; Column: TUniDBGridColumn; Attribs: TUniCellAttribs);
begin
  if grdElenco.DataSource.DataSet.FieldByName('CAUSA_MORTE').AsString>'' then //for rows
  begin
    //Attribs.Font.Style:=[fsbold];
    //Attribs.Font.Color:=clWhite; //font color
    Attribs.Color:=clWebLightgrey; //background color
  end;

end;

procedure TFContratti.grdElencoSelectionChange(Sender: TObject);
begin
  // SelectedCode        := StrToIntDef(dsGrid.dataset.fieldbyname('N_POLIZZA').asString,0);
  // mb 02052020
  SelectedCode := dsGrid.dataset.fieldbyname('cod_polizza').AsInteger;
end;

procedure TFContratti.setOnlySelection(const Value: Boolean);
begin
  FOnlySelection          := Value;
  btnInsert.Visible       := not FOnlySelection;
  btnModify.Visible       := not FOnlySelection;
  UniSpeedButton1.Visible := not FOnlySelection;
end;

procedure TFContratti.UniFormCreate(Sender: TObject);
begin
  UniMainModule.archivio := 'POL';
  inherited;
  self.DMMaster  := (UniMainModule.DMdatiAgePolizze as IQueryElenco);
  dsGrid.dataset := UniMainModule.DMdatiAgePolizze.QPolizze;
  // cbbReferente.ListSource.DataSet.Open;
  log('UContratti Thread_id: ' + TThread.CurrentThread.ThreadID.ToString + ' sessione_agenzia: ' +
    UniMainModule.DMdatiAge.SessionAgenzia.SessionName + ' age:' + UniMainModule.DMdatiAge.SLPdati_age.agenzia);
  log('UContratti Thread_id: ' + TThread.CurrentThread.ThreadID.ToString + ' QPolizze Sessionename: ' +
    (dsGrid.dataset as TDBISAMQuery).SessionName + ' age:' + UniMainModule.DMdatiAge.SLPdati_age.agenzia);
  btnVisualizza.Visible := True;
  UniMainModule.DMdatiAge.DSCompagnie.dataset.Open;
  UniMainModule.DMdatiAge.DSCompagnie.dataset.First;
  nomeColonna   := 'RIF_REFERENTE';
  xcodCompagnia := 0;
  self.DMMaster.EseguiQuery('RIF_REFERENTE');
  dsGrid.DataSet.Filtered := False;
  dsGrid.DataSet.Filter   := '';

  EditPageClassName := 'TFContrattoEdit';
  grdElenco.ClearFilters;

end;

procedure TFContratti.uRGstatoPolizzeClick(Sender: TObject);
begin
  // UniMainModule.DMdatiAgePolizze.EseguiQuery('RIF_REFERENTE', uRGstatoPolizze.ItemIndex);
  UniMainModule.DMdatiAgePolizze.EseguiQuery(nomeColonna, uRGstatoPolizze.ItemIndex, '', 0, 0, 0, xcodCompagnia)
end;

end.
