object FRistampaAppendice: TFRistampaAppendice
  Left = 0
  Top = 0
  ClientHeight = 303
  ClientWidth = 476
  Caption = 'Ristampa Appendice'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = [biSystemMenu]
  MonitoredKeys.Keys = <>
  DesignSize = (
    476
    303)
  PixelsPerInch = 96
  TextHeight = 13
  object pnlButtonsEditAss: TUniSimplePanel
    Left = 0
    Top = 262
    Width = 476
    Height = 41
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 0
    object btnBconferma: TUniBitBtn
      Left = 3
      Top = 3
      Width = 126
      Height = 35
      Hint = ''
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000202020404040
        4040404040404040404040404040404040404040404040404040404040404040
        404040404040402020207F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
        C0C0C0C0C0C0909090808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0309030008000608060808080B0
        B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
        30CF30008000008000008000608060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800000800000800060
        8060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
        00BF00008000008000008000008000008000608060909090C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800020DF20009F0000
        8000208020808080A0A0A0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
        90CF9000BF0000800080808030EF30009F00008000208020808080B0B0B0C0C0
        C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C090CF9000DF00A0A0A0C0C0C030
        EF30009F00008000608060808080B0B0B0C0C0C0C0C0C04040407F7F7FC0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F000080006080609090
        90C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C030EF30009F00208020808080A0A0A0C0C0C04040407F7F7FC0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F002080
        20808080C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C030EF30009F00608060C0C0C04040407F7F7FC0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C060DF
        6090B090C0C0C04040403F3F3F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
        7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F202020}
      Caption = 'Anteprima'
      ModalResult = 1
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsBold]
      TabOrder = 1
    end
    object btnBabbandona: TUniBitBtn
      Left = 135
      Top = 3
      Width = 111
      Height = 35
      Hint = ''
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF404040606060808080808080808080606060404040FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF20202020206020209F0000BF00
        00BF0000BF20209F404080808080606060202020FFFFFFFFFFFFFFFFFFFFFFFF
        00002020209F0000FF0000FF0000FF0000FF0000FF0000FF0000FF20209F6060
        80808080202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000DF606080606060FFFFFFFFFFFF00007F
        0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
        FF20209F8080804040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
        00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF40408060606000007F0000FF
        0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
        FF0000FF20209F8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
        FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF8080800000BF0000FF
        0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000
        FF0000FF0000BF8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
        FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF60606000007F0000FF
        0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
        FF0000FF20209F4040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
        00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF202060FFFFFFFFFFFF00007F
        0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
        FF20209F202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000DF000020FFFFFFFFFFFFFFFFFFFFFFFF
        00002000007F0000FF0000FF0000FF0000FF0000FF0000FF0000FF00007F0000
        20FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00004000007F0000BF00
        00BF0000BF00007F000040FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Caption = 'Esci'
      ModalResult = 2
      ParentFont = False
      Font.Height = -13
      Font.Style = [fsBold]
      TabOrder = 2
    end
  end
  object ungrpbxEstremiAppendice: TUniGroupBox
    Left = 15
    Top = 5
    Width = 444
    Height = 251
    Hint = ''
    Caption = ' Estremi dell'#39'appendice '
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    ParentFont = False
    Font.Height = -13
    object dtpckDataEmissione: TUniDateTimePicker
      Left = 237
      Top = 32
      Width = 185
      Height = 24
      Hint = ''
      DateTime = 43958.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 2
      ParentFont = False
      Font.Height = -13
      FieldLabel = 'Data Stampa'
      FieldLabelWidth = 80
    end
    object edtNumeroAppendice: TUniNumberEdit
      Left = 32
      Top = 32
      Width = 185
      Height = 24
      Hint = ''
      ParentFont = False
      Font.Height = -13
      TabOrder = 0
      FieldLabel = 'Numero'
      FieldLabelWidth = 80
      DecimalSeparator = ','
    end
    object dtpckDataDa: TUniDateTimePicker
      Left = 32
      Top = 80
      Width = 185
      Height = 24
      Hint = ''
      DateTime = 43958.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 4
      ParentFont = False
      Font.Height = -13
      FieldLabel = 'Periodo dal'
      FieldLabelWidth = 80
    end
    object dtpckDataA: TUniDateTimePicker
      Left = 237
      Top = 80
      Width = 185
      Height = 24
      Hint = ''
      DateTime = 43958.000000000000000000
      DateFormat = 'dd/MM/yyyy'
      TimeFormat = 'HH:mm:ss'
      TabOrder = 5
      ParentFont = False
      Font.Height = -13
      FieldLabel = 'fino al'
      FieldLabelWidth = 80
    end
    object edtImporto: TUniFormattedNumberEdit
      Left = 32
      Top = 208
      Width = 185
      Height = 25
      Hint = ''
      ParentFont = False
      Font.Height = -15
      TabOrder = 7
      FieldLabel = 'Importo'
      FieldLabelWidth = 80
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object edtNextRata: TUniFormattedNumberEdit
      Left = 244
      Top = 208
      Width = 178
      Height = 25
      Hint = ''
      Visible = False
      ParentFont = False
      Font.Height = -15
      TabOrder = 8
      FieldLabel = 'Prossima rata'
      FieldLabelWidth = 80
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
    object ckbxTipoTitolo: TUniCheckBox
      Left = 237
      Top = 125
      Width = 196
      Height = 17
      Hint = ''
      Visible = False
      Caption = 'Il premio viene modificato'
      ParentFont = False
      Font.Height = -15
      TabOrder = 3
    end
    object uncbxMemorizza: TUniCheckBox
      Left = 32
      Top = 125
      Width = 185
      Height = 17
      Hint = ''
      Caption = 'Memorizza l'#39'appendice'
      ParentFont = False
      Font.Height = -15
      TabOrder = 6
    end
    object uncbxIncassa: TUniCheckBox
      Left = 32
      Top = 165
      Width = 199
      Height = 17
      Hint = ''
      Caption = 'Metti a cassa l'#39'appendice'
      ParentFont = False
      Font.Height = -15
      TabOrder = 9
    end
  end
end
