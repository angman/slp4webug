inherited FUtenteEdit: TFUtenteEdit
  ClientHeight = 471
  ClientWidth = 574
  Caption = 'Editazione utente'
  ExplicitWidth = 590
  ExplicitHeight = 510
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 574
    ExplicitWidth = 574
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 574
    ExplicitWidth = 574
  end
  object pnlEdit: TUniSimplePanel [2]
    Left = 0
    Top = 69
    Width = 574
    Height = 402
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    object unidbedtNOME: TUniDBEdit
      Left = 21
      Top = 10
      Width = 244
      Height = 22
      Hint = ''
      DataField = 'NOME'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      ParentFont = False
      TabOrder = 1
      FieldLabel = 'Nome'
      FieldLabelWidth = 80
    end
    object unidbedtSIGLA: TUniDBEdit
      Left = 21
      Top = 38
      Width = 244
      Height = 22
      Hint = ''
      DataField = 'SIGLA'
      DataSource = dsoEdit
      CharCase = ecUpperCase
      TabOrder = 2
      FieldLabel = 'Sigla'
      FieldLabelWidth = 80
    end
    object uniedtActPassword: TUniEdit
      Left = 21
      Top = 217
      Width = 244
      Hint = ''
      PasswordChar = #1
      Text = ''
      ParentFont = False
      TabOrder = 3
      FieldLabel = 'Password attuale'
      FieldLabelFont.Color = clRed
      OnChange = uniedtActPasswordChange
    end
    object uniedtNewPassword: TUniEdit
      Left = 21
      Top = 249
      Width = 244
      Hint = ''
      PasswordChar = #1
      Text = ''
      TabOrder = 5
      FieldLabel = 'Nuova password'
    end
    object uniedtNewPasswordConfirm: TUniEdit
      Left = 21
      Top = 281
      Width = 244
      Hint = ''
      PasswordChar = #1
      Text = ''
      ParentFont = False
      TabOrder = 6
      FieldLabel = 'Conferma Nuova Password'
    end
    object lblNotaPassword: TUniLabel
      Left = 288
      Top = 222
      Width = 229
      Height = 16
      Hint = ''
      Caption = 'da inserire sempre per aggiornare i dati'
      ParentFont = False
      Font.Color = clRed
      Font.Height = -13
      TabOrder = 4
    end
    object lblNotaSave: TUniLabel
      Left = 21
      Top = 321
      Width = 439
      Height = 16
      Hint = ''
      Caption = 
        'Per poter salvare le modifiche inserire la password nel edit pas' +
        'sword attuale'
      ParentFont = False
      Font.Color = clRed
      Font.Height = -13
      TabOrder = 7
    end
    object uniedtReferenteCollegato: TUniEdit
      Left = 21
      Top = 361
      Width = 458
      Hint = ''
      Text = 'uniedtReferenteCollegato'
      ParentFont = False
      TabOrder = 8
      ReadOnly = True
      FieldLabel = 'Collaboratore'
    end
    object btnSelezionaReferente: TUniBitBtn
      Left = 482
      Top = 360
      Width = 25
      Height = 25
      Action = actSelezionaReferente
      ParentFont = False
      TabOrder = 9
      IconAlign = iaCenter
      Images = untvmglstIcons
      ImageIndex = 0
    end
    object btnDelReferente: TUniBitBtn
      Left = 508
      Top = 360
      Width = 25
      Height = 25
      Action = actDelReferente
      ParentFont = False
      TabOrder = 10
      IconAlign = iaCenter
      Images = untvmglstIcons
      ImageIndex = 1
    end
    object UniPCautorizzazioni: TUniPageControl
      Left = 21
      Top = 71
      Width = 540
      Height = 137
      Hint = ''
      ActivePage = UniTabSheet1
      TabOrder = 11
      object UniTabSheet1: TUniTabSheet
        Hint = ''
        Caption = 'Autorizzazioni emissione polizze'
        object ckbxStampaPolizze: TUniDBCheckBox
          Left = 12
          Top = 11
          Width = 317
          Height = 17
          Hint = ''
          DataField = 'SOLO_EMISSIONE_SLP'
          DataSource = dsoEdit
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Caption = 'Autorizzato SOLO alla stampa delle polizze'
          ParentFont = False
          Font.Height = -13
          TabOrder = 0
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxStampaFascicoli: TUniDBCheckBox
          Left = 12
          Top = 42
          Width = 317
          Height = 17
          Hint = ''
          DataField = 'SOLO_FAC_SIMILE_SLP'
          DataSource = dsoEdit
          ValueChecked = 'S'#13#10#13#10
          ValueUnchecked = 'N'
          Caption = 'Autorizzato SOLO alla stampa dei fac-simili'
          ParentFont = False
          Font.Height = -13
          TabOrder = 1
          ParentColor = False
          Color = clBtnFace
        end
        object UniDBCheckBox1: TUniDBCheckBox
          Left = 12
          Top = 76
          Width = 341
          Height = 17
          Hint = ''
          DataField = 'NO_APPENDICI'
          DataSource = dsoEdit
          ValueChecked = 'S'#13#10#13#10
          ValueUnchecked = 'N'
          Caption = 'NON Autorizzato all'#39'emissione delle appendici'
          ParentFont = False
          Font.Height = -13
          TabOrder = 2
          ParentColor = False
          Color = clBtnFace
        end
      end
      object UniTabSheet2: TUniTabSheet
        Hint = ''
        Caption = 'Autorizzazioni  titoli'
        object ckbxSTAMPA_QUIETANZE: TUniDBCheckBox
          Left = 12
          Top = 12
          Width = 317
          Height = 17
          Hint = ''
          DataField = 'STAMPA_QUIETANZE'
          DataSource = dsoEdit
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Caption = 'Autorizzato alla stampa delle quitetanze'
          ParentFont = False
          Font.Height = -13
          TabOrder = 0
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxNO_INCASSO: TUniDBCheckBox
          Left = 12
          Top = 41
          Width = 317
          Height = 17
          Hint = ''
          DataField = 'NO_INCASSO'
          DataSource = dsoEdit
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Caption = 'NON autorizzato all'#39'incasso dei titoli'
          ParentFont = False
          Font.Height = -13
          TabOrder = 1
          ParentColor = False
          Color = clBtnFace
        end
      end
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 360
    object actSelezionaReferente: TAction
      Category = 'ReferenteCollegato'
      OnExecute = actSelezionaReferenteExecute
    end
    object actDelReferente: TAction
      Category = 'ReferenteCollegato'
      OnExecute = actDelReferenteExecute
    end
  end
  inherited dsoEdit: TDataSource
    Left = 280
  end
end
