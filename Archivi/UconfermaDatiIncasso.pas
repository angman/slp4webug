unit UconfermaDatiIncasso;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniPageControl, uniGUIBaseClasses,
  uniButton, uniBitBtn, uniDateTimePicker, uniLabel, uniGUIApplication, UDMAgeCollaboratori,
  Data.DB, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, UDMdatiAgeTitoli, UDMDatiAgePolizzePerf,
  System.StrUtils, uniEdit, uniGUIDialogs;

type

  TFconfermaDatiIncasso = class(TUniForm)
    Babbandona: TUniBitBtn;
    Bconferma: TUniBitBtn;
    pcIncasso: TUniPageControl;
    tsDatiIncasso: TUniTabSheet;
    tsEstremiPagamento: TUniTabSheet;
    UniLabel1: TUniLabel;
    EuniDataPag: TUniDateTimePicker;
    cbbPromoter: TUniDBLookupComboBox;
    dsCollaboratore: TDataSource;
    undtmpckrDataValuta: TUniDateTimePicker;
    cbbTipoPagamento: TUniDBLookupComboBox;
    dsTipoPagamento: TDataSource;
    unmbrdtImportoPagato: TUniNumberEdit;
    undtmpckrDataValutaPag: TUniDateTimePicker;
    undtmpckrDataMovimento: TUniDateTimePicker;
    unedtDescrizioneIncasso: TUniEdit;
    unedtPagEseguitoDa: TUniEdit;
    unedtEstremiPagamento: TUniEdit;
    unedtBancaPagamento: TUniEdit;
    btnContraente: TUniButton;
    procedure BconfermaClick(Sender: TObject);
    procedure BabbandonaClick(Sender: TObject);
    procedure btnContraenteClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    cod_titolo: Integer;
    tipo: string;
    tipoRiga: string;

  public
    { Public declarations }
    class procedure ShowConfermaDatiIncasso(UniApplication: TUniGUIApplication; Xcod_titolo: Integer; Xtipo: string;
      CallBackProc: TUniDialogCallBackProc);
  end;

function FconfermaDatiIncasso: TFconfermaDatiIncasso;

implementation

{$R *.dfm}

uses
  MainModule, libSLP;

function FconfermaDatiIncasso: TFconfermaDatiIncasso;
begin
  Result := TFconfermaDatiIncasso(UniMainModule.GetFormInstance(TFconfermaDatiIncasso));
end;

class procedure TFconfermaDatiIncasso.ShowConfermaDatiIncasso(UniApplication: TUniGUIApplication; Xcod_titolo: Integer;
  Xtipo: string; CallBackProc: TUniDialogCallBackProc);
begin
  with TFconfermaDatiIncasso.Create(UniApplication) do
  begin
    cod_titolo := Xcod_titolo;
    tipo       := Xtipo;
    if tipo = 'Polizza' then
    begin
      Caption := Xtipo + ' ' + UniMainModule.DMdatiAgePolizzePerf.Qpolizza.FieldByName('n_polizza').AsString + ' del ' +
        UniMainModule.DMdatiAgePolizzePerf.Qpolizza.FieldByName('decorrenza').AsString;
      tipoRiga := 'PPP';

    end
    else
    begin
      Caption := Xtipo + ' ' + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('n_polizza').AsString + ' del ' +
        UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('decorrenza').AsString;
      tipoRiga := IfThen(Xtipo = 'Appendice', 'INA', 'INQ');
    end;

    // if UniMainModule.DMDatiReport.OpenPrinthousePolizza(NPolizza) then ShowModal
    // else Close;
    ShowModal(CallBackProc);
  end;
end;

procedure TFconfermaDatiIncasso.UniFormCreate(Sender: TObject);
begin
  undtmpckrDataValuta.DateTime    := 0;
  undtmpckrDataValutaPag.DateTime := 0;
  undtmpckrDataMovimento.DateTime := 0;
  EuniDataPag.DateTime            := date;
  undtmpckrDataMovimento.DateTime := 0;  // EuniDataPag.DateTime;

  cbbPromoter.ListSource.DataSet  := UniMainModule.DMdatiAgeCollaboratori.Qpromoter;
  cbbPromoter.ListSource.DataSet.Open;
  cbbTipoPagamento.ListSource.DataSet := UniMainModule.DMdatiAgeTitoli.Qtipo_pagamenti;
  cbbTipoPagamento.ListSource.DataSet.Open;
  pcIncasso.ActivePage := tsDatiIncasso;

end;

procedure TFconfermaDatiIncasso.BabbandonaClick(Sender: TObject);
begin
  Close;
end;

procedure TFconfermaDatiIncasso.BconfermaClick(Sender: TObject);
var
  descriz: string;
  provvig, old_provvig, impPag: Currency;
  prog: Integer;
  tinPdf: string;
  CodScadenza: Integer;
  errore: integer;
  procedi: Boolean;
begin
  errore:=0;
  tinPdf:='';
  UniMainModule.DMdatiAge.get_codiceX('numero3', prog);
  try
    if prog > -1 then
    begin
      // dimezza le provvigioni se � passato pi� dei giorni stabiliti
      descriz := '';

      if tipo = 'Polizza' then
      begin
        // caso di richiesta perfezionamento di una polizza
        // esegui tutte le operazioni relative al perfezionamento di una polizza
        cod_titolo := UniMainModule.DMdatiAgePolizzePerf.incassa_polizza(cod_titolo,
          cbbPromoter.ListSource.DataSet.FieldByName('COD_PRODUTTORE').asInteger,prog,Date,undtmpckrDataValuta.DateTime);

        // devi assegnare a cod_titolo il valore corretto relativo al titolo inserito per l'incasso della polizza
        if cod_titolo = -1 then
        begin
          ShowMessage('Problema nell''inserimento del titolo relativo all''incasso della polizza perfezionata. ERRORE-IP1');

          Close;
        end;
        // posizionati sul giusto record di UniMainModule.DMdatiAgeTitoli.Qtitoli
        UniMainModule.DMdatiAgeTitoli.EseguiQuery('N_POLIZZA', nil, '');
        // UniMainModule.DMdatiAgeTitoli.Qtitoli.Open;
        UniMainModule.DMdatiAgeTitoli.Qtitoli.Locate('cod_scadenza', cod_titolo, []);
      end;
      // sbagliato: se ad eseguire l'incasso � un collaboratore nel dataset Qtitoli il campo provvigioni non � corretto
      // provvig := UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('PROVVIGIONI').asCurrency;
      UniMainModule.DMdatiAgeTitoli.QFindTitolo.Close;
      UniMainModule.DMdatiAgeTitoli.QFindTitolo.ParamByName('cod_scadenza').AsInteger :=
                      UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('COD_SCADENZA').asInteger;
      UniMainModule.DMdatiAgeTitoli.QFindTitolo.Open;
      provvig := UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('PROVVIGIONI').asCurrency;
      old_provvig := provvig;

      if tipo = 'Quietanza' then
      begin
        provvig := UniMainModule.DMdatiAgeTitoli.dimezza_provvigioni(provvig,
          UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime,
          UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO').AsString, descriz);

        if (provvig < old_provvig) then
          ShowMessage('Procedo alla riduzione del 50% delle provvigioni (incasso in ritardo).');
      end;

      impPag := 0;

      CodScadenza := UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('COD_SCADENZA').asInteger;
      procedi:=True;
      if tipo<>'Polizza' then
         procedi := UniMainModule.DMdatiAgeTitoli.incassaTitolo(CodScadenza, provvig, descriz, date, undtmpckrDataValuta.DateTime,
                    'P', cbbPromoter.ListSource.DataSet.FieldByName('COD_PRODUTTORE').asInteger, prog,
                    UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO').AsString);

      if procedi then
      begin
        // procedi alla registrazione degli eventuali estremi dell'incasso
        if unmbrdtImportoPagato.Value > 0 then
          impPag := unmbrdtImportoPagato.Value;

         if (unedtDescrizioneIncasso.Text > '') or (unedtPagEseguitoDa.Text > '') or (unedtEstremiPagamento.Text > '') or
           (unedtBancaPagamento.Text > '') or (cbbTipoPagamento.Text > '') then
         begin
           UniMainModule.DMdatiAgeTitoli.estremi_incasso(CodScadenza, impPag, undtmpckrDataMovimento.DateTime,
             undtmpckrDataValutaPag.DateTime, UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime,
             cbbTipoPagamento.ListSource.DataSet.FieldByName('CODICE_PAG').AsString, unedtEstremiPagamento.Text, '', '',
             unedtBancaPagamento.Text, unedtDescrizioneIncasso.Text, '', '', unedtPagEseguitoDa.Text,
             UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString, '');
         end;

         // verifica se il pagamento � stato eseguito !!! altrimenti avvisa
         // cerca e verifica che esista il titolo e che risulti incassato, altrimenti avvisa
         UniMainModule.DMdatiAgeTitoli.QFindTitolo.Close;
         UniMainModule.DMdatiAgeTitoli.QFindTitolo.ParamByName('cod_scadenza').AsInteger:=codScadenza;
         UniMainModule.DMdatiAgeTitoli.QFindTitolo.Open;

         if (UniMainModule.DMdatiAgeTitoli.QFindTitolo.RecordCount=1) and
            (UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('stato').AsString='P') then
         begin

            tinPdf := '';
            // MB 20112020 non serve pi�
            //if UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('INpdf').AsString = 'S' then
            //  tinPdf := 'PDF ';

            descriz := datetimetostr(now) + ' INC ' + tinPdf + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName
              ('TIPO_TITOLO').AsString + ' Np' + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString + ' '
              + dtoc(UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime);

            UniMainModule.comunicaDIR('CKA', descriz, '', '', '', '', tipoRiga,
              UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString,
              UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime);
         end else errore:=1;
      end else begin
         // errore trappato nella procedura che aggiorna il record di scadenze.dat con i dati di pagamento
         ShowMessage('Errore nella fase di incasso. Avvertire il proprio referente commerciale.  ERRORE-IQ3');
         errore:=3;
      end;
    end else begin
       errore:=2;
    end;
    if errore>0 then
    begin
       if errore=2 then
       begin
          // fallito l'acquisizione di un numero progressivo di incasso
          descriz := datetimetostr(now) + ' ##y ' + tinPdf + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO')
            .AsString + ' Np' + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString + ' ' +
            dtoc(UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime) + ' ERR2';
       end else if errore=3 then begin
          descriz := datetimetostr(now) + ' ##j ' + tinPdf + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO')
            .AsString + ' Np' + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString + ' ' +
            dtoc(UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime)+ 'ERR-IQ3';
       end else begin
          // errore=1 allora non ha registrato la data di pagamento !!!
          descriz := datetimetostr(now) + ' ##i ' + tinPdf + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO')
            .AsString + ' Np' + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString + ' ' +
            dtoc(UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime)+ ' ERR1';
       end;

       UniMainModule.comunicaDIR('###', descriz, '', '', '', '', tipoRiga,
       UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString,
       UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime);

       if UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO').AsString='20' then
          ShowMessage('Il pagamento del titolo non � andato a buon fine. Riprovare.  (ERRORE-ERR1)')
       else
          if UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO').AsString<'20' then
             ShowMessage('Il titolo relativo alla polizza � stato registrato ma NON SONO STATI REGISTRATI GLI ESTREMI DI PAGAMENTO. Andare nei Titoli e procedere al pagamento manualmente. Avvertire il proprio referente commerciale.  (ERRORE-IT2)');
    end;

  except
    ShowMessage('Errore nella fase di incasso. Avvertire il proprio referente commerciale.  ERRORE-IG3');

    descriz := datetimetostr(now) + ' ##I ' + tinPdf + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('TIPO_TITOLO')
      .AsString + ' Np' + UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString + ' ' +
      dtoc(UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime)+ 'ERR-IG3';

    UniMainModule.comunicaDIR('###', descriz, '', '', '', '', tipoRiga,
      UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString,
      UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime);

  end;

  // dataPag := EuniDataPag.DateTime;
  Close;
end;

procedure TFconfermaDatiIncasso.btnContraenteClick(Sender: TObject);
begin
  unedtPagEseguitoDa.Text := UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DENOMINAZ').AsString;
end;

end.
