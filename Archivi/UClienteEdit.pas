unit UClienteEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, uniPanel, uniMemo, uniDBMemo,
  uniPageControl, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniEdit, uniDBEdit,
  uniDateTimePicker, uniDBDateTimePicker, uniCheckBox, uniDBCheckBox, uniFieldSet, uniImageList,
  uniColorButton, unimButton, uniSplitter, uniBasicGrid, uniDBGrid, uniDBNavigator, Vcl.DBCtrls, Vcl.Menus, uniMainMenu,
  uniMenuButton, UDMdatiAgeClienti;

type
  TFClienteEdit = class(TFMasterEdit)
    pnlEdit: TUniContainerPanel;
    pcCliente: TUniPageControl;
    pgDatiAnag: TUniTabSheet;
    pgIndirizzo: TUniTabSheet;
    pgNote: TUniTabSheet;
    undbmNotaCliente: TUniDBMemo;
    cbbTitolo: TUniDBLookupComboBox;
    cbbSesso: TUniDBComboBox;
    dbedtNome: TUniDBEdit;
    dbedtCodFisc: TUniDBEdit;
    dbedtCocnome: TUniDBEdit;
    edtReferente: TUniEdit;
    ckbxPrivacy: TUniDBCheckBox;
    cbbProfessione: TUniDBLookupComboBox;
    dbedtDescrizAttivita: TUniDBEdit;
    dbedtLocNascita: TUniDBEdit;
    undbdtmpckrDataNascita: TUniDBDateTimePicker;
    dbedtNazioneNascita: TUniDBEdit;
    cbbLivello: TUniDBLookupComboBox;
    ckbxStatoCivile: TUniDBCheckBox;
    unedtPromoter: TUniEdit;
    dbedtIndirizzo1: TUniDBEdit;
    dbedtCap1: TUniDBEdit;
    dbedtCitta1: TUniDBEdit;
    dbedtProvincia1: TUniDBEdit;
    pnlIndirizzo1: TUniPanel;
    lblIndirizzo1: TUniLabel;
    pnlIndirizzo2: TUniPanel;
    dbedtCAP2: TUniDBEdit;
    dbedtCITTA2: TUniDBEdit;
    dbedtINDIRIZZO2: TUniDBEdit;
    dbedtPROVINCIA2: TUniDBEdit;
    lblIndirizzo2: TUniLabel;
    actSelezionaProduttore: TAction;
    actCancellaProduttore: TAction;
    pnlDenominazione: TUniSimplePanel;
    dbedtDenominazione: TUniDBEdit;
    pnlDatiAnag: TUniSimplePanel;
    pnlVarAppendice: TUniPanel;
    btnAppCambioIndirizzo: TUniSpeedButton;
    unedtSubPromoter: TUniEdit;
    actCancellaSubProduttore: TAction;
    actSelezionaSubProduttore: TAction;
    dbedtPIva: TUniDBEdit;
    pnlTelefoniMail: TUniSimplePanel;
    pnlTeleMailDescr: TUniPanel;
    grdTelefoniMail: TUniDBGrid;
    spltrPanels: TUniSplitter;
    pnlDocumenti: TUniSimplePanel;
    pnlDocuments: TUniPanel;
    grdDocuments: TUniDBGrid;
    dbnvgtrTelefoniMail: TUniDBNavigator;
    dbnvgtrDocuments: TUniDBNavigator;
    pnlHdrTelefoniMail: TUniHiddenPanel;
    dsTelefoniMail: TDataSource;
    dsDocuments: TDataSource;
    edtTelDescr: TUniEdit;
    edtTelefonoMail: TUniEdit;
    cbbTipoTelefonoMail: TUniDBLookupComboBox;
    dsTipoTelMail: TDataSource;
    pgContattiCliente: TUniTabSheet;
    grdContattiCliente: TUniDBGrid;
    dbnvgtrContattiCliente: TUniDBNavigator;
    pnlNav: TUniSimplePanel;
    dsContattiCliente: TDataSource;
    edtTelefono: TUniEdit;
    edtMail: TUniEdit;
    btnSelezionaProduttore: TUniBitBtn;
    unpmnSelProduttore: TUniPopupMenu;
    mnuSelezionaCollaboratore: TUniMenuItem;
    mnuTogliCollaboratore: TUniMenuItem;
    unpmnSelSubCollaboratore: TUniPopupMenu;
    mnuSelezionaSubProduttore: TUniMenuItem;
    mnuCancellaSubProduttore: TUniMenuItem;
    btnSelezionaSubproduttore: TUniMenuButton;
    btnSelezionaReferente: TUniMenuButton;
    unpmnSelReferente: TUniPopupMenu;
    mnuSelezionaReferente: TUniMenuItem;
    mnuCancellaReferente: TUniMenuItem;
    actSelezionaReferente: TAction;
    actCancellaReferente: TAction;
    procedure UniFormCreate(Sender: TObject);
    procedure actSelezionaProduttoreExecute(Sender: TObject);
    procedure actCancellaProduttoreExecute(Sender: TObject);
    procedure actConfermaExecute(Sender: TObject);
    procedure actCancellaSubProduttoreExecute(Sender: TObject);
    procedure actSelezionaSubProduttoreExecute(Sender: TObject);
    procedure dbnvgtrDocumentsBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure grdDocumentsBodyDblClick(Sender: TObject);
    procedure grdContattiClienteBodyDblClick(Sender: TObject);
    procedure dbnvgtrContattiClienteBeforeAction(Sender: TObject; Button: TNavigateBtn);
    procedure dsTelefoniMailDataChange(Sender: TObject; Field: TField);
    procedure actCancellaReferenteExecute(Sender: TObject);
    procedure actSelezionaReferenteExecute(Sender: TObject);
    procedure edtReferenteChangeValue(Sender: TObject);
    procedure unedtPromoterChangeValue(Sender: TObject);
    procedure unedtSubPromoterChangeValue(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure UniFormBeforeShow(Sender: TObject);
  private
    { Private declarations }
  class var
    IstanceNum: integer;

    DMdatiAgeClienti: TDMDatiAGeClienti;

    procedure ShowCallBack(Sender: TComponent; AResult: integer);
    procedure ShowEditContattoCliente;
    procedure ShowEditDocumento;
    procedure SetPgTelefoniMailReadOnly;
    procedure setNavigators;
    procedure SetNavigatorButtons(ANavigator: TUniDBNavigator);
    procedure AggiornaTelMailPref;
    procedure setQueryEditCliente(const Value: TDataSet);

  public
    { Public declarations }
    class constructor Create;
    class procedure resetInstanceNumber;

    procedure SetCollaboratore(ASelectedItem: integer; const ASelectedDescription: string);
    procedure SetSubCollaboratore(ASelectedItem: integer; const ASelectedDescription: string);
    procedure setReferente(ASelectedItem: integer; const ASelectedDescription: string);

    property QueryEditCliente: TDataSet write setQueryEditCliente;
  end;

function FClienteEdit: TFClienteEdit;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UdmdatiAge, UCollaboratori, ServerModule, UShowAppendiceRec,
  UDocumentoEdit, UEditContattoCliente, UClienti;

function FClienteEdit: TFClienteEdit;
begin
  Result := TFClienteEdit(UniMainModule.GetFormInstance(TFClienteEdit));
end;

procedure TFClienteEdit.actCancellaProduttoreExecute(Sender: TObject);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('PROMOTER').Clear;
  unedtPromoter.Text := '';

  DSoEdit.DataSet.FieldByName('SUB_PROMOTER').Clear;
  unedtSubPromoter.Text := '';
  // actCancellaProduttore.Enabled := unedtSubPromoter.Text <> '';

end;

procedure TFClienteEdit.actCancellaReferenteExecute(Sender: TObject);
begin
  DSoEdit.DataSet.FieldByName('RIF_REFERENTE').Clear;
  edtReferente.Text := '';
  // actCancellaReferente.Enabled := False;

end;

procedure TFClienteEdit.actCancellaSubProduttoreExecute(Sender: TObject);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('SUB_PROMOTER').Clear;
  unedtSubPromoter.Text := '';
  // actCancellaSubProduttore.Enabled := False;

end;

procedure TFClienteEdit.actConfermaExecute(Sender: TObject);
begin
  UniMainModule.traccia(UniMainModule.operazione + '-CLIENTI', DSoEdit.DataSet.FieldByName('cod_cliente').AsInteger, '',
    'END ' + UniMainModule.operazione + ' CLI ' + DateTimeToStr(Now) + ' ope:' +
    UniMainModule.DMdatiAge.SLPdati_age.utente + ' - ' + DSoEdit.DataSet.FieldByName('denominaz').AsString);

  if UniMainModule.DMdatiAgeClienti.NeedGenerateAppendice and (UniMainModule.operazione <> 'INS') then
    MessageDlg('Genero appendice cambio indirizzo?', mtConfirmation, mbYesNo, ShowCallBack)
  else
    inherited;

end;

procedure TFClienteEdit.actSelezionaProduttoreExecute(Sender: TObject);
begin
  // se puoi modificare il cliente allora devi poter vedere i collaboratori !!!
  // UniMainModule.aggiornaPosizione('COL', 'MOD', 'Agenti-Dipendenti-Collaboratori');

  with TFCollaboratori.Create(UniApplication, 0, 3) do
  begin
    OnSelectedItem := SetCollaboratore;
    WindowState    := wsNormal;
    ShowModal;
  end;

end;

procedure TFClienteEdit.actSelezionaReferenteExecute(Sender: TObject);
var
  FormCli: TFClienti;
begin
  // with TFClienti.Create(UniApplication, TDMdatiAgeClienti.Create(Self)) do
  // FormCli := TFClienti.Create(UniApplication, TDMdatiAgeClienti.Create(Self));
  FormCli := TFClienti.Create(UniApplication);
  with FormCli do
  begin
    ImpostaDmMaster(TDMDatiAGeClienti.Create(Self));
    FormCli.OnSelectedItem := Self.setReferente;
    FormCli.WindowState    := wsNormal;
    FormCli.ShowModal;
  end;

end;

procedure TFClienteEdit.actSelezionaSubProduttoreExecute(Sender: TObject);
begin
  // se puoi modificare il cliente allora devi poter vedere i sub collaboratori !!!
  // UniMainModule.aggiornaPosizione('COL', 'MOD', 'Agenti-Dipendenti-Collaboratori');

  with TFCollaboratori.Create(UniApplication, DSoEdit.DataSet.FieldByName('PROMOTER').AsInteger) do
  begin

    OnSelectedItem := SetSubCollaboratore;
    WindowState    := wsNormal;
    ShowModal;
  end;
end;

procedure TFClienteEdit.dbnvgtrContattiClienteBeforeAction(Sender: TObject; Button: TNavigateBtn);
begin
  if (Button = nbInsert) or ((Button = nbedit) and (not DSoEdit.DataSet.IsEmpty)) then
    ShowEditContattoCliente;
end;

procedure TFClienteEdit.dbnvgtrDocumentsBeforeAction(Sender: TObject; Button: TNavigateBtn);
begin
  if (Button = nbInsert) or ((Button = nbedit) and (not DSoEdit.DataSet.IsEmpty)) then
    ShowEditDocumento;
end;

procedure TFClienteEdit.dsTelefoniMailDataChange(Sender: TObject; Field: TField);
begin
  AggiornaTelMailPref;
end;

procedure TFClienteEdit.edtReferenteChangeValue(Sender: TObject);
begin
  actCancellaReferente.Enabled := edtReferente.Text <> '';
end;

procedure TFClienteEdit.grdContattiClienteBodyDblClick(Sender: TObject);
begin
  if not dsContattiCliente.DataSet.IsEmpty then
    ShowEditContattoCliente;
end;

procedure TFClienteEdit.grdDocumentsBodyDblClick(Sender: TObject);
begin
  if not dsDocuments.DataSet.IsEmpty then
    ShowEditDocumento;
end;

procedure TFClienteEdit.SetCollaboratore(ASelectedItem: integer; const ASelectedDescription: string);
var
  resetSub: Boolean;
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  resetSub := DSoEdit.DataSet.FieldByName('PROMOTER').AsInteger <> ASelectedItem;

  DSoEdit.DataSet.FieldByName('PROMOTER').AsInteger := ASelectedItem;
  unedtPromoter.Text                                := ASelectedDescription;
  if ASelectedDescription > '' then
  begin
    if resetSub then
    begin
      DSoEdit.DataSet.FieldByName('SUB_PROMOTER').Clear;
      unedtSubPromoter.Text := '';
    end;
    unedtSubPromoter.Enabled          := True;
    btnSelezionaSubproduttore.Enabled := True;
    actSelezionaSubProduttore.Enabled := True;
    actCancellaSubProduttore.Enabled  := unedtSubPromoter.Text <> '';
  end;
end;

procedure TFClienteEdit.SetNavigatorButtons(ANavigator: TUniDBNavigator);
begin
  if ANavigator.DataSource.DataSet.IsEmpty then
    ANavigator.VisibleButtons := ANavigator.VisibleButtons - [nbdelete, nbedit]
  else
    ANavigator.VisibleButtons := ANavigator.VisibleButtons + [nbdelete, nbedit];
end;

procedure TFClienteEdit.setNavigators;
begin
  // SetNavigatorButtons(dbnvgtrDocuments);
  // SetNavigatorButtons(dbnvgtrContattiCliente);
end;

procedure TFClienteEdit.SetPgTelefoniMailReadOnly;
begin
  grdTelefoniMail.ReadOnly              := True;
  grdTelefoniMail.Options               := grdTelefoniMail.Options + [dgRowSelect];
  grdDocuments.Options                  := grdTelefoniMail.Options;
  grdDocuments.ReadOnly                 := True;
  dbnvgtrTelefoniMail.VisibleButtons    := [nbfirst, nbprior, nbNext, nbLast];
  dbnvgtrDocuments.VisibleButtons       := dbnvgtrTelefoniMail.VisibleButtons;
  dbnvgtrContattiCliente.VisibleButtons := dbnvgtrTelefoniMail.VisibleButtons;
end;

procedure TFClienteEdit.setQueryEditCliente(const Value: TDataSet);
begin
  // FQueryEditCliente := Value;
  DSoEdit.DataSet := Value;
end;

procedure TFClienteEdit.setReferente(ASelectedItem: integer; const ASelectedDescription: string);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('RIF_REFERENTE').AsInteger := ASelectedItem;
  edtReferente.Text                                      := ASelectedDescription;

end;

procedure TFClienteEdit.SetSubCollaboratore(ASelectedItem: integer; const ASelectedDescription: string);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('SUB_PROMOTER').AsInteger := ASelectedItem;
  unedtSubPromoter.Text                                 := ASelectedDescription;
end;

procedure TFClienteEdit.AggiornaTelMailPref;
begin
  edtTelefono.Text := UniMainModule.DMdatiAgeClienti.getTelefonoMailpref(DSoEdit.DataSet.FieldByName('COD_CLIENTE')
    .AsInteger, ttmTelefono);
  edtMail.Text := UniMainModule.DMdatiAgeClienti.getTelefonoMailpref(DSoEdit.DataSet.FieldByName('COD_CLIENTE')
    .AsInteger, ttmEmail);
end;

class constructor TFClienteEdit.Create;
begin
  IstanceNum := 0;
end;

class procedure TFClienteEdit.resetInstanceNumber;
begin
  IstanceNum := 0;
end;

procedure TFClienteEdit.ShowCallBack(Sender: TComponent; AResult: integer);
var
  ReportFileName: string;
  AppRec: TShowAppendiceRec;
begin
  if AResult = mrYes then
  begin
    inherited actConfermaExecute(actConferma);
    ReportFileName := UniMainModule.DMdatiAgeClienti.PrepareAppendiceCliente(AppRec);
    UniMainModule.DMdatiAgePolizze.memorizzaAppendice(UniMainModule.DMDatiReport.QFindCliente, AppRec,
      ReportFileName, True);

    UniMainModule.DMDatiReport.ShowReportAppendice(ReportFileName, True);
  end;

end;

procedure TFClienteEdit.ShowEditContattoCliente;
begin
  with TFEditContattoCliente.Create(UniApplication) do
  begin
    DSoEdit.DataSet := dsContattiCliente.DataSet;
    show(
      procedure(Sender: TComponent; Result: integer)
      begin
        if Result = mrOk then
          AggiornaTelMailPref;

      end)
  end;
end;

procedure TFClienteEdit.ShowEditDocumento;
begin
  with TFEditDocumento.Create(UniApplication) do
  begin
    DSoEdit.DataSet := dsDocuments.DataSet;
    show(
      procedure(Sender: TComponent; Result: integer)
      begin
        if Result = mrOk then
          AggiornaTelMailPref;
      end)
  end;
end;

procedure TFClienteEdit.unedtPromoterChangeValue(Sender: TObject);
begin
  actCancellaProduttore.Enabled := unedtPromoter.Text <> '';
end;

procedure TFClienteEdit.unedtSubPromoterChangeValue(Sender: TObject);
begin
  actCancellaSubProduttore.Enabled := unedtSubPromoter.Text <> '';
end;

procedure TFClienteEdit.UniFormBeforeShow(Sender: TObject);
begin
  DMdatiAgeClienti      := (DatasetElenco.Owner as TDMdatiAgeClienti);
  edtReferente.Text     := DMdatiAgeClienti.posizionaReferente(DSoEdit.DataSet.FieldByName('RIF_REFERENTE').AsInteger);
  DMdatiAgeClienti.ApriTelefoniDocumenti(DSoEdit.DataSet.FieldByName('COD_CLIENTE').AsInteger);
end;

procedure TFClienteEdit.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Dec(IstanceNum);
end;

procedure TFClienteEdit.UniFormCreate(Sender: TObject);
var codC: Integer;
begin
  inherited;
  if UniMainModule.operazione = 'INS' then
  begin
     // MB 23122020
     UniMainModule.DMdatiAgeClienti.QeditCliente.Cancel;

     UniMainModule.DMdatiAgeClienti.set_post_ins_cliente(False);
     codC:= UniMainModule.DMdatiAgeClienti.ins_cli_new;
     UniMainModule.DMdatiAgeClienti.set_post_ins_cliente(true);

     UniMainModule.DMdatiAgeClienti.PosizionaQuery(codC);
  end;

  pcCliente.ActivePage := pgDatiAnag;
  cbbTitolo.listSource.DataSet.open;
  cbbProfessione.listSource.DataSet.open;
  unedtSubPromoter.Text := '';
  unedtPromoter.Text    := UniMainModule.DMdatiAge.getNomePromoter(DSoEdit.DataSet.FieldByName('PROMOTER').AsInteger);
  if unedtPromoter.Text > '' then
  begin
    unedtSubPromoter.Text := UniMainModule.DMdatiAge.getNomePromoter(DSoEdit.DataSet.FieldByName('SUB_PROMOTER')
      .AsInteger);
    unedtSubPromoter.Enabled          := True;
    actSelezionaProduttore.Enabled    := True;
    actSelezionaSubProduttore.Enabled := True;
    actCancellaReferente.Enabled      := edtReferente.Text <> '';
    actCancellaProduttore.Enabled     := unedtPromoter.Text <> '';
    actCancellaSubProduttore.Enabled  := unedtSubPromoter.Text <> '';
  end;
  inc(IstanceNum);
  actSelezionaReferente.Enabled    := IstanceNum = 1;
  actCancellaReferente.Enabled     := edtReferente.Text <> '';
  actCancellaProduttore.Enabled    := unedtPromoter.Text <> '';
  actCancellaSubProduttore.Enabled := unedtSubPromoter.Text <> '';
  // se si tratta di un tunte promoter allora devi bloccare la possibilitÓ di cambiare la sub-agenzia
  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    // 1 - blocca la selezione del collaboratore
    // 2 - istanzia il collaboratore all'utente-prooter correntemente loggato
    // 3 - valorizza i campi necessari e le scritte
    // 4 - itera il ragionamento per il sub-produttore

    // SetCollaboratore(UniMainModule.UtentePromoter,'');
    btnSelezionaProduttore.Enabled := False;
    if UniMainModule.UtenteSubPromoter > 0 then
    begin
      // SetSubCollaboratore(UniMainModule.UtenteSubPromoter,'');
      btnSelezionaSubProduttore.Enabled := false;
      //btnDelSubCollaboratore.Enabled    := false;
    end;
  end;

  pcCliente.ActivePage     := pgDatiAnag;
  pnlDenominazione.Visible := UniMainModule.DMdatiAgeClienti.mustShowDenominazione;
  if UniMainModule.operazione = 'VIS' then
  begin
    UniMainModule.traccia('VIS-CLIENTI',dsoEdit.DataSet.FieldByName('cod_cliente').AsInteger,'','START VIS CLI '+
                           DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente + ' - ' +
                           dsoEdit.DataSet.FieldByName('denominaz').AsString);
    actConferma.Enabled := false;
    SetAllControlsReadOnly(pcCliente);
    btnSelezionaReferente.Enabled     := False;
    btnSelezionaProduttore.Enabled    := False;
    btnSelezionaSubproduttore.Enabled := False;
    SetPgTelefoniMailReadOnly;
    AggiornaTelMailPref;
  end;

  if UniMainModule.operazione = 'MOD' then
  begin
    setNavigators;
    AggiornaTelMailPref;
    UniMainModule.traccia('MOD-CLIENTI', DSoEdit.DataSet.FieldByName('cod_cliente').AsInteger, '',
      'START MOD CLI ' + DateTimeToStr(Now) + ' ope:' + UniMainModule.DMdatiAge.SLPdati_age.utente + ' - ' +
      DSoEdit.DataSet.FieldByName('denominaz').AsString);
  end;

  if UniMainModule.operazione = 'INS' then
  begin
    setNavigators;
    UniMainModule.traccia('INS-CLIENTI', 0, '', 'START INS CLI ' + DateTimeToStr(Now) + ' ope:' +
      UniMainModule.DMdatiAge.SLPdati_age.utente);
    if UniMainModule.DMdatiAge.is_utente_promoter then
    begin
      // carica l promoter collegato all'utente corrente !!!
      unedtPromoter.Text := UniMainModule.DMdatiAge.getNomePromoter(UniMainModule.UtentePromoter);
      DSoEdit.DataSet.FieldByName('PROMOTER').AsInteger := UniMainModule.UtentePromoter;

      if UniMainModule.UtenteSubPromoter > 0 then
      begin
        unedtSubPromoter.Text := UniMainModule.DMdatiAge.getNomePromoter(UniMainModule.UtenteSubPromoter);
        DSoEdit.DataSet.FieldByName('SUB_PROMOTER').AsInteger := UniMainModule.UtenteSubPromoter;
      end
      else
      begin
        btnSelezionaSubproduttore.Enabled := True;
        actCancellaSubProduttore.Enabled  := unedtSubPromoter.Text <> '';
      end;
    end;
  end;

end;

initialization

RegisterClass(TFClienteEdit);

end.
