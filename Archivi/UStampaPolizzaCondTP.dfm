inherited FStampaPolizzaCondTP: TFStampaPolizzaCondTP
  Caption = 'Tutela plus condominio'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlEditButtons: TUniSimplePanel
    inherited btnAnteprimaStampa: TUniSpeedButton
      ScreenMask.Target = Owner
    end
  end
  inherited pnlDatiSpecificiPolizza: TUniPanel
    inherited pcDatiEreditabili: TUniPageControl
      ActivePage = pgMassimali
      inherited pgContraente: TUniTabSheet
        inherited cbbTelefono: TUniDBLookupComboBox
          FieldLabel = 'Telefono'
        end
      end
      object pgAmministratore: TUniTabSheet
        Hint = ''
        Caption = 'Amministratore'
        object dbedtAmministratore: TUniDBEdit
          Left = 16
          Top = 40
          Width = 414
          Height = 22
          Hint = ''
          DataField = 'DENOMINAZ'
          DataSource = dsReferente
          TabOrder = 0
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Amministratore'
          FieldLabelWidth = 85
        end
        object dbedtIndirizzoStudio: TUniDBEdit
          Left = 16
          Top = 71
          Width = 414
          Height = 22
          Hint = ''
          DataField = 'INDIRIZZO'
          DataSource = dsReferente
          TabOrder = 1
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Indirizzo studio'
          FieldLabelWidth = 85
        end
        object dbedtCapSudio: TUniDBEdit
          Left = 436
          Top = 71
          Width = 121
          Height = 22
          Hint = ''
          DataField = 'CAP'
          DataSource = dsReferente
          TabOrder = 2
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Cap'
          FieldLabelWidth = 60
        end
        object dbedtCittaStudio: TUniDBEdit
          Left = 563
          Top = 71
          Width = 263
          Height = 22
          Hint = ''
          DataField = 'CITTA'
          DataSource = dsReferente
          TabOrder = 3
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Citt'#224
          FieldLabelWidth = 35
        end
        object dbedtProvincia1: TUniDBEdit
          Left = 828
          Top = 71
          Width = 66
          Height = 22
          Hint = ''
          DataField = 'Provincia'
          DataSource = dsReferente
          CharCase = ecUpperCase
          TabOrder = 4
          Color = clYellow
          ReadOnly = True
          FieldLabel = 'Prov.'
          FieldLabelWidth = 30
        end
      end
      object pgMassimali: TUniTabSheet
        Hint = ''
        Caption = 'Massimali e garanzie'
        object grpbxMassimaleBase: TUniGroupBox
          Left = 7
          Top = 9
          Width = 833
          Height = 49
          Hint = ''
          Caption = 'Massimale Base'
          TabOrder = 3
        end
        object cbbMassimali: TUniDBComboBox
          Left = 25
          Top = 24
          Width = 249
          Hint = ''
          DataField = 'SiglaMassimale'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 0
          FieldLabel = 'Massimale'
          FieldLabelWidth = 80
          IconItems = <>
        end
        object dbedtNumeroUnitaImmobiliari: TUniDBNumberEdit
          Left = 290
          Top = 23
          Width = 248
          Height = 22
          Hint = ''
          DataField = 'UnitaImmobiliari'
          DataSource = dsoEdit
          TabOrder = 1
          MaxValue = 999.000000000000000000
          MinValue = 5.000000000000000000
          ShowTrigger = True
          FieldLabel = 'Numero unit'#224' immobiliari'
          DecimalSeparator = ','
        end
        object dbedtNumeroBox: TUniDBNumberEdit
          Left = 565
          Top = 23
          Width = 248
          Height = 22
          Hint = ''
          DataField = 'NumeroBox'
          DataSource = dsoEdit
          TabOrder = 2
          MaxValue = 999.000000000000000000
          ShowTrigger = True
          FieldLabel = 'Numero dei box'
          DecimalSeparator = ','
        end
        object grpbxEstensioni: TUniGroupBox
          Left = 7
          Top = 72
          Width = 833
          Height = 129
          Hint = ''
          Caption = 'Estensioni'
          TabOrder = 4
          object dbedtNPolizza: TUniDBEdit
            Left = 18
            Top = 28
            Width = 249
            Height = 22
            Hint = ''
            DataField = 'CodFiscIvas'
            DataSource = dsAssicurato
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 1
            FieldLabel = 'Polizza N.'
            FieldLabelWidth = 70
          end
          object dbedtCompagniaEmittente: TUniDBEdit
            Left = 283
            Top = 28
            Width = 248
            Height = 22
            Hint = ''
            DataField = 'Citta'
            DataSource = dsAssicurato
            CharCase = ecUpperCase
            TabOrder = 2
            FieldLabel = 'Emessa da Comp'
          end
          object dbedtIndirizzoGar: TUniDBEdit
            Left = 558
            Top = 28
            Width = 248
            Height = 22
            Hint = ''
            DataField = 'Indirizzo'
            DataSource = dsAssicurato
            CharCase = ecUpperCase
            TabOrder = 3
            FieldLabel = 'Agenzia di'
            FieldLabelWidth = 70
          end
          object dtpckDecorrenza: TUniDBDateTimePicker
            Left = 18
            Top = 56
            Width = 160
            Height = 26
            Hint = ''
            DataField = 'DataRilascio'
            DataSource = dsAssicurato
            DateTime = 43874.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 4
            FieldLabel = 'Decorrenza'
            FieldLabelWidth = 70
          end
          object dtpckScadenza: TUniDBDateTimePicker
            Left = 283
            Top = 56
            Width = 150
            Height = 26
            Hint = ''
            DataField = 'DataScadenza'
            DataSource = dsAssicurato
            DateTime = 43874.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 5
            FieldLabel = 'Scadenza'
            FieldLabelWidth = 60
          end
          object dbedtPremioAnnuoLordo: TUniDBNumberEdit
            Left = 558
            Top = 60
            Width = 248
            Height = 22
            Hint = ''
            DataField = 'PremioAnnuoLordo'
            DataSource = dsAssicurato
            ParentFont = False
            TabOrder = 6
            SelectOnFocus = True
            FieldLabel = 'Premio annuo lordo'
            DecimalSeparator = ','
          end
          object dbedtPremioNettoSLP: TUniDBNumberEdit
            Left = 558
            Top = 92
            Width = 248
            Height = 22
            Hint = ''
            DataField = 'PremioSLP'
            DataSource = dsAssicurato
            ParentFont = False
            TabOrder = 7
            Color = clYellow
            ReadOnly = True
            FieldLabel = 'Premio netto SLP'
            DecimalSeparator = ','
          end
        end
        object ckbxPolizzaIndicizzata: TUniDBCheckBox
          Left = 25
          Top = 219
          Width = 206
          Height = 17
          Hint = ''
          DataField = 'Indicizzata'
          DataSource = dsoEdit
          Caption = 'INDICIZZATA'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 5
          ParentColor = False
          Color = clBtnFace
        end
      end
    end
  end
  inherited pnlDatiFissi: TUniPanel
    inherited btnPolizzaSostituita: TUniMenuButton
      Width = 25
      ExplicitWidth = 25
    end
  end
  inherited dsoEdit: TDataSource
    DataSet = DMDatiTempPolizzaCondTP.fdmtblPolizza
  end
  inherited dsGaranzia: TDataSource
    DataSet = DMDatiTempPolizzaCondTP.fdmtblGaranzia
  end
  inherited dsAssicurato: TDataSource
    DataSet = DMDatiTempPolizzaCondTP.fdmtblAssicurato
  end
  object dsReferente: TDataSource
    DataSet = DMdatiAgeClienti.QFindReferente
    Left = 581
    Top = 348
  end
end
