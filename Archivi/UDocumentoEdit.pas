unit UDocumentoEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, unimComboBox,
  uniGUIBaseClasses, uniImageList, Data.DB, System.Actions, Vcl.ActnList, uniButton, uniBitBtn, uniSpeedButton,
  uniLabel, uniPanel, uniMemo, uniDBMemo, uniDBEdit, uniEdit, uniDateTimePicker, uniDBDateTimePicker;

type
  TFEditDocumento = class(TFMasterEdit)
    dtpckDataRilascio: TUniDBDateTimePicker;
    dbedtRilasciatoDa: TUniDBEdit;
    dbedtLuogoRilascio: TUniDBEdit;
    dtpckPrimaScadenza: TUniDBDateTimePicker;
    dtpckScadenza: TUniDBDateTimePicker;
    dbedtPeriodoRinnovo: TUniDBNumberEdit;
    dbedtEstremiDocumento: TUniDBEdit;
    undbmNote: TUniDBMemo;
    dsTipoDocumento: TDataSource;
    cbbTipoDocumento: TUniDBLookupComboBox;
    pnlEdit: TUniSimplePanel;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FEditDocumento: TFEditDocumento;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMdatiAgeClienti;

function FEditDocumento: TFEditDocumento;
begin
  Result := TFEditDocumento(UniMainModule.GetFormInstance(TFEditDocumento));
end;

procedure TFEditDocumento.UniFormCreate(Sender: TObject);
begin
  inherited;
  dsTipoDocumento.DataSet.Open;
  if UniMainModule.operazione = 'VIS' then
    SetAllControlsReadOnly(pnlEdit);
end;

end.
