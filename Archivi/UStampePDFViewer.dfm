object FPDFViewer: TFPDFViewer
  Left = 0
  Top = 0
  ClientHeight = 608
  ClientWidth = 930
  Caption = 'Visualizzatore documenti'
  OldCreateOrder = False
  OnClose = UniFormClose
  MonitoredKeys.Keys = <>
  PixelsPerInch = 96
  TextHeight = 13
  object unpdfrmDoc: TUniPDFFrame
    Left = 0
    Top = 0
    Width = 930
    Height = 608
    Hint = ''
    ScreenMask.Enabled = True
    ScreenMask.Message = 'Attendere, caricamento anteprima in corso ...'
    ScreenMask.Target = unpdfrmDoc
    ClientEvents.ExtEvents.Strings = (
      
        'frameload=function frameload(sender, frame, eOpts)'#13#10'{'#13#10'    sende' +
        'r.iframe.contentWindow.oncontextmenu = function() {'#13#10'        ret' +
        'urn false'#13#10'    }'#13#10'}'#13#10)
    Align = alClient
    TabOrder = 0
    OnFrameLoaded = unpdfrmDocFrameLoaded
  end
end
