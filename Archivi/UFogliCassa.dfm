inherited FFogliCassa: TFFogliCassa
  ClientWidth = 891
  Caption = 'Gestione Fogli Cassa'
  OnClose = UniFormClose
  ExplicitWidth = 907
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 891
    ExplicitWidth = 963
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 891
    ExplicitWidth = 963
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 891
      ExplicitWidth = 963
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 891
        ExplicitWidth = 963
        inherited btnModify: TUniSpeedButton
          Visible = False
        end
        inherited btnVisualizza: TUniSpeedButton
          TabOrder = 6
        end
        object UniSpeedButton1: TUniSpeedButton
          Left = 458
          Top = 0
          Width = 106
          Height = 43
          Action = actAnteprima
          ParentFont = False
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 5
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 891
        ExplicitWidth = 963
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 891
      ExplicitWidth = 963
      inherited grdElenco: TUniDBGrid
        Width = 891
        Columns = <
          item
            FieldName = 'sigla_compagnia'
            Filtering.Enabled = True
            Filtering.Editor = unedtSiglaCompagnia
            Title.Caption = 'Compagnia'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'numero_fc'
            Filtering.Enabled = True
            Filtering.Editor = unmbrdtFoglioCassa
            Title.Caption = 'N. Foglio cassa'
            Width = 90
            Sortable = True
          end
          item
            FieldName = 'ANNO'
            Filtering.Enabled = True
            Filtering.Editor = cbbAnno
            Title.Caption = 'Anno'
            Width = 90
            ReadOnly = True
          end
          item
            FieldName = 'periodo_dal'
            Title.Caption = 'Periodo_dal'
            Width = 80
            Sortable = True
          end
          item
            FieldName = 'periodo_al'
            Title.Caption = 'Periodo_al'
            Width = 80
          end
          item
            FieldName = 'data_chiusura'
            Title.Caption = 'Data chiusura'
            Width = 80
          end
          item
            FieldName = 'versato'
            Title.Caption = 'Versato'
            Width = 100
          end
          item
            FieldName = 'trattenuto'
            Title.Caption = 'Trattenuto'
            Width = 100
          end
          item
            FieldName = 'data_invio'
            Title.Caption = 'Data invio'
            Width = 64
          end
          item
            FieldName = 'tipo_invio'
            Title.Caption = 'Tipo invio'
            Width = 64
          end
          item
            FieldName = 'codice_foglio'
            Title.Caption = 'Codice foglio'
            Width = 120
            Visible = False
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 160
        Top = 62
        ExplicitLeft = 160
        ExplicitTop = 62
        object unedtSiglaCompagnia: TUniEdit
          Left = 48
          Top = 16
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTSIGLACOMPAGNIA'
          TabOrder = 1
        end
        object unmbrdtFoglioCassa: TUniNumberEdit
          Left = 48
          Top = 56
          Width = 121
          Hint = ''
          TabOrder = 2
          DecimalSeparator = ','
        end
        object cbbAnno: TUniDBLookupComboBox
          Left = 48
          Top = 96
          Width = 121
          Hint = ''
          ListField = 'ANNO'
          ListSource = dsAnno
          KeyField = 'ANNO'
          ListFieldIndex = 0
          DataField = 'ANNO'
          DataSource = dsGrid
          TabOrder = 3
          Color = clWindow
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    object actStampaFoglioCassa: TAction
      Category = 'FCOperation'
      Caption = 'Scarica FC'
      OnExecute = actStampaFoglioCassaExecute
    end
    object actAnteprima: TAction
      Category = 'FCOperation'
      Caption = 'Anteprima'
      OnExecute = actAnteprimaExecute
    end
  end
  inherited dsGrid: TDataSource
    DataSet = DMDatiAgeFogliCassa.QfogliCassa
  end
  object dsAnno: TDataSource
    DataSet = DMDatiAgeFogliCassa.QAnni
    Left = 736
    Top = 253
  end
end
