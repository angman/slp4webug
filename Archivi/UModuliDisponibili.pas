unit UModuliDisponibili;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses,
  uniURLFrame, uniSplitter, uniGUIApplication, uniScreenMask, uniRadioGroup;

type
  TFModuliDisponibili = class(TFMasterElenco)
    actVisualizzaDocumento: TAction;
    actEsportaDocumento: TAction;
    UniScreenMask1: TUniScreenMask;
    UniPanel1: TUniPanel;
    uniRGtipoModuli: TUniRadioGroup;
    procedure UniFormCreate(Sender: TObject);
    procedure actVisualizzaDocumentoExecute(Sender: TObject);
    procedure actEsportaDocumentoExecute(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure uniRGtipoModuliClick(Sender: TObject);
  private
    // FComingFromLogin: Boolean;
    LCanPrint: Boolean;
    PDFFolderPath: string;
    PDFUrl: string;
    FCodDocumento: Integer;
    { Private declarations }
    function getDocumentFileName(ACodDocument: Integer): string;
    // procedure SetComingFromLogin(const Value: Boolean);

  public
    { Public declarations }
    // property ComingFromLogin: Boolean write SetComingFromLogin;
    class procedure ShowModuliDisponibili(UniApplication: TUniGUIApplication; CanPrint, CanExport: Boolean;
      CodDoc: Integer = 0);
    property CodDocumento: Integer read FCodDocumento write FCodDocumento;
  end;

function FModuliDisponibili: TFModuliDisponibili;

implementation

{$R *.dfm}

uses
  MainModule, UDMDatiReport, ServerModule, System.IOUtils, System.StrUtils, UStampePDFViewer, System.Math;

function FModuliDisponibili: TFModuliDisponibili;
begin
  Result := TFModuliDisponibili(UniMainModule.GetFormInstance(TFModuliDisponibili));
end;

procedure TFModuliDisponibili.actEsportaDocumentoExecute(Sender: TObject);
var
  afilename: string;
begin
  afilename := getDocumentFileName(dsGrid.DataSet.FieldByName('COD_stampa').AsInteger);
  if (afilename <> '') then // and FileExists(afilename) then
  begin
    UniSession.SendFile(PDFFolderPath + '\' + afilename);
    UniMainModule.DMDatiReport.UpdDataStampaDoc(dsGrid.DataSet.FieldByName('COD_stampa').AsInteger);
    TFile.delete(PDFFolderPath + '\' + afilename);
  end;
end;

procedure TFModuliDisponibili.actVisualizzaDocumentoExecute(Sender: TObject);
var
  afilename: string;
begin
  if uniRGtipoModuli.ItemIndex = 2 then
    ShowMessage('Visualizzazione da attivare.')
  else
  begin
    CodDocumento := IfThen(CodDocumento <> 0, CodDocumento, dsGrid.DataSet.FieldByName('COD_stampa').AsInteger);
    try
      afilename := getDocumentFileName(CodDocumento);
      if (afilename <> '') then // and FileExists(afilename) then
      begin
        TFPDFViewer.ShowReport(UniApplication, afilename, PDFUrl, PDFFolderPath, False, True);
        dsGrid.DataSet.DisableControls;
        dsGrid.DataSet.Close;
        dsGrid.DataSet.Open;
        dsGrid.DataSet.EnableControls;
      end;
    finally
      CodDocumento := 0;
    end;
  end;

end;

function TFModuliDisponibili.getDocumentFileName(ACodDocument: Integer): string;
begin

  Result := UniMainModule.DMDatiReport.SalvaTempDocument(PDFFolderPath, ACodDocument, LCanPrint, tdModuli);

end;

class procedure TFModuliDisponibili.ShowModuliDisponibili(UniApplication: TUniGUIApplication;
  CanPrint, CanExport: Boolean; CodDoc: Integer);
begin
  with TFModuliDisponibili.Create(UniApplication) do
  begin
    btnModify.visible := CanExport;
    LCanPrint         := CanPrint;
    CodDocumento      := CodDoc;
    if UniMainModule.DMDatiReport.OpenModuliDisponibili(uniRGtipoModuli.ItemIndex) then
      ShowModal
    else
      Close;
  end;
end;

procedure TFModuliDisponibili.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFModuliDisponibili.UniFormCreate(Sender: TObject);
begin
  inherited;
  PDFUrl            := UniServerModule.FilesFolderURL + 'PDF/';
  PDFFolderPath     := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  btnModify.Action  := actEsportaDocumento;
  btnModify.Enabled := UniMainModule.VerificaAutorizzazione('MOD', 'EXP');
  btnInsert.Action  := actVisualizzaDocumento;
  btnInsert.Enabled := UniMainModule.VerificaAutorizzazione('MOD', 'VIS');
  UniMainModule.aggiornaPosizione('MOD', 'VIS', 'Elenco polizze/moduli disponibili');
  UniMainModule.DMDatiReport.OpenModuliDisponibili(0);
  dsGrid.DataSet := UniMainModule.DMDatiReport.QModuliDisponibili;
end;

procedure TFModuliDisponibili.uniRGtipoModuliClick(Sender: TObject);
begin
  UniMainModule.DMDatiReport.OpenModuliDisponibili(uniRGtipoModuli.ItemIndex);
end;

end.
