unit UTitoloEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, uniPanel, uniDBEdit, uniEdit,
  uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, UDMdatiAgeTitoli, uniImageList,
  uniDateTimePicker, uniDBDateTimePicker, System.Math;

type
  TFTitoloEdit = class(TFMasterEdit)
    pnlEdit: TUniContainerPanel;
    cbbTipoTitolo: TUniDBLookupComboBox;
    dbedtNPolizza: TUniDBNumberEdit;
    dbedtPercTasse: TUniDBNumberEdit;
    dbedtImponibile: TUniDBNumberEdit;
    dbedtPremioLordo: TUniDBNumberEdit;
    dbedtProvvigioni: TUniDBNumberEdit;
    dbedtImposte: TUniDBNumberEdit;
    dbedtProvvigioniSub: TUniDBNumberEdit;
    btnSelezionaPolizza: TUniBitBtn;
    actSelPolizza: TAction;
    dbedtContraente: TUniDBEdit;
    dbedtFrazionamento: TUniDBEdit;
    dbedtScadenza: TUniDBDateTimePicker;
    DBLcompagnie: TUniDBLookupComboBox;
    procedure actConfermaExecute(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure actSelPolizzaExecute(Sender: TObject);
    procedure cbbTipoTitoloChange(Sender: TObject);
    procedure dbedtPremioLordoChange(Sender: TObject);
    procedure dbedtScadenzaChange(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure SetPolizza(ASelectedItem: Integer; const ASelectedDescription: string);
    procedure imposta_campi(per, tipo_titolo: string);

  public
    { Public declarations }
  end;

function FTitoloEdit: TFTitoloEdit;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UContratti, date360;

function FTitoloEdit: TFTitoloEdit;
begin
  Result := TFTitoloEdit(UniMainModule.GetFormInstance(TFTitoloEdit));
end;

procedure TFTitoloEdit.actSelPolizzaExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('POL', 'VIS', 'Contratti');

  with TFContratti.Create(UniApplication) do
  begin
    OnSelectedItem := SetPolizza;
    ShowModal;
  end;

end;

procedure TFTitoloEdit.cbbTipoTitoloChange(Sender: TObject);
begin
  inherited;
  // se scelgo rilievo non devo permettere la scelta della polizza !!!
  imposta_campi(UniMainModule.operazione, UniMainModule.DMdatiAgeTitoli.QTipoTitolo.FieldByName('sigla').AsString);
end;

procedure TFTitoloEdit.dbedtPremioLordoChange(Sender: TObject);
begin
  inherited;
  // esegui il ricalcolo del premio netto e delle tasse in base al premio lordo appena inserito
  if True then // UniMainModule.operazione='MOD' then
  begin

{$MESSAGE HINT 'Fare un metodo nel dmdatiagetitoli'}
    UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('NETTO').AsCurrency :=
      roundTo((UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('lordo').AsCurrency /
      (1 + (UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('ptasse').AsCurrency / 100))), -2);
    UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('TASSE').AsCurrency :=
      UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('LORDO').AsCurrency -
      UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('NETTO').AsCurrency;
  end;
end;

procedure TFTitoloEdit.dbedtScadenzaChange(Sender: TObject);
begin
  UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('decorrenza').asDateTime :=
    UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('rata').asDateTime;
  if UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('FRAZIONAM').AsString>'' then
     UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('SCADENZA').asDateTime :=
        dai_scadenza_rata(UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('rata').asDateTime,
        UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('FRAZIONAM').AsString)
  else
     UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('SCADENZA').asDateTime :=
        UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('rata').asDateTime;
end;

procedure TFTitoloEdit.imposta_campi(per, tipo_titolo: string);
  procedure impostaM(campo: TUniCustomEdit; editabile: Boolean);
  begin
    if editabile then
    begin
      campo.ReadOnly := False;
      campo.Color    := clWindow;
    end
    else
    begin
      campo.ReadOnly := True;
      campo.Color    := clYellow;
    end;
  end;
  procedure impostaD(campo: TUniDBDateTimePicker; editabile: Boolean);
  begin
    if editabile then
    begin
      campo.ReadOnly := False;
      campo.Color    := clWindow;
    end
    else
    begin
      campo.ReadOnly := True;
      campo.Color    := clYellow;
    end;
  end;

begin
  // VALORI FISSI PER TUTTI IN TUTTI I CASI
  impostaM(dbedtFrazionamento, False);
  impostaM(dbedtPercTasse, False);
  impostaM(dbedtImponibile, False);
  impostaM(dbedtImposte, False);
  impostaM(dbedtContraente, False);

   dbedtContraente.Text        := '';
   dbedtPremioLordo.FieldLabel := 'Premio lordo';
   dbedtProvvigioni.FieldLabel := 'Provvigioni';

  if tipo_titolo = '50' then
  begin
    impostaM(dbedtContraente, True);
    btnSelezionaPolizza.Visible := False;
    dbedtContraente.Text        := 'Scarico rilievo n. ';
    dbedtPremioLordo.FieldLabel := 'Importo a debito';
    dbedtProvvigioni.FieldLabel := 'Importo a cred.';
  end
  else
    if (per = 'INS') then
      btnSelezionaPolizza.Visible := True;

  if Pos(tipo_titolo, '10.11.20') > 0 then
    impostaD(dbedtScadenza, (per = 'INS'));
  if Pos(tipo_titolo, '12.15.50') > 0 then
    impostaD(dbedtScadenza, True);

  if Pos(tipo_titolo, '10.11.15.20') > 0 then
    impostaM(dbedtPremioLordo, (per = 'INS'));

  if Pos(tipo_titolo, '12.50') > 0 then
    impostaM(dbedtPremioLordo, True);

  if Pos(tipo_titolo, '10.11.50') > 0 then
    impostaM(dbedtProvvigioni, True);

  if Pos(tipo_titolo, '12.15.20') > 0 then
    impostaM(dbedtProvvigioni, True);

  if (per = 'INS') then
  begin
    if tipo_titolo = '20' then
      dsoEdit.DataSet.FieldByName('valuta').AsString := 'r'
    else
      dsoEdit.DataSet.FieldByName('valuta').AsString := 'E'
  end
  else
    if tipo_titolo = '20' then
    begin
      if dsoEdit.DataSet.FieldByName('valuta').AsString = 'r' then
        dbedtProvvigioni.ReadOnly := False
      else
        dbedtProvvigioni.ReadOnly := True;
    end;

end;


procedure TFTitoloEdit.SetPolizza(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  UniMainModule.DMdatiAgeTitoli.new_titolo(ASelectedItem);

  // begin
  // txtNPolizza.Text      := UniMainModule.DMdatiAge.QPolizze.FieldByName('N_POLIZZA').AsString;
  // cod_compagnia         := UniMainModule.DMdatiAge.QPolizze.FieldByName('RIF_COMPAGNIA').AsInteger;
  // txtContraente.Text    := UniMainModule.DMdatiAge.QPolizze.FieldByName('CONTRAENTE').AsString;
  // txtFrazionamento.Text := UniMainModule.DMdatiAge.QPolizze.FieldByName('FRAZIONAM').AsString;
  // dtScadenza.Date       := UniMainModule.DMdatiAge.QPolizze.FieldByName('SCADENZA').AsDateTime;
  // txtPercTasse.Text     := UniMainModule.DMdatiAge.QPolizze.FieldByName('PTASSE').AsString;
  // txtImponibile.Text    := UniMainModule.DMdatiAge.QPolizze.FieldByName('NETTO').AsString;
  // txtPremioLordo.Text   := UniMainModule.DMdatiAge.QPolizze.FieldByName('LORDO').AsString;
  // txtProvvigioni.Text   := UniMainModule.DMdatiAge.QPolizze.FieldByName('PROVVIGIONI').AsString;
  // txtImposte.Text       := UniMainModule.DMdatiAge.QPolizze.FieldByName('TASSE').AsString;
  // txtFrazionamento.Text := UniMainModule.DMdatiAge.getDesFrazionamentoPolizza(txtFrazionamento.Text);
  //
  // UniMainModule.numPolizza           := UniMainModule.DMdatiAge.QPolizze.FieldByName('N_POLIZZA').AsString;
  // UniMainModule.codCompagnia         := UniMainModule.DMdatiAge.QPolizze.FieldByName('RIF_COMPAGNIA').AsInteger;
  // UniMainModule.contraentePolizza    := UniMainModule.DMdatiAge.QPolizze.FieldByName('CONTRAENTE').AsString;
  // UniMainModule.frazionamentoPolizza := UniMainModule.DMdatiAge.QPolizze.FieldByName('FRAZIONAM').AsString;
  // UniMainModule.scadenzaPolizza      := UniMainModule.DMdatiAge.QPolizze.FieldByName('SCADENZA').AsString;
  // UniMainModule.percTassePolizza     := UniMainModule.DMdatiAge.QPolizze.FieldByName('PTASSE').AsCurrency;
  // UniMainModule.imponibilePolizza    := UniMainModule.DMdatiAge.QPolizze.FieldByName('NETTO').AsCurrency;
  // UniMainModule.lordoPolizza         := UniMainModule.DMdatiAge.QPolizze.FieldByName('LORDO').AsCurrency;
  // UniMainModule.provvPolizza         := UniMainModule.DMdatiAge.QPolizze.FieldByName('PROVVIGIONI').AsCurrency;
  // UniMainModule.tassePolizza         := UniMainModule.DMdatiAge.QPolizze.FieldByName('TASSE').AsCurrency;
  // UniMainModule.codPolizza           := UniMainModule.DMdatiAge.QPolizze.FieldByName('cod_polizza').AsInteger;
  // UniMainModule.codCliente           := UniMainModule.DMdatiAge.QPolizze.FieldByName('cod_cli').AsInteger;
  // UniMainModule.provvPolizzaSubAge   := 0;
  // end;

end;

procedure TFTitoloEdit.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  actAnnullaExecute(Sender) ;
end;

procedure TFTitoloEdit.UniFormCreate(Sender: TObject);
begin
  dsoEdit.DataSet := UniMainModule.DMdatiAgeTitoli.QTitoloEdit;
  UniMainModule.DMdatiAgeTitoli.Qcompagnie.Open;
  UniMainModule.DMdatiAgeTitoli.Qcompagnie.First;
  inherited;
  cbbTipoTitolo.ListSource.DataSet.Open;
  btnSelezionaPolizza.Visible := UniMainModule.operazione = 'INS';
  cbbTipoTitolo.ReadOnly      := UniMainModule.operazione = 'MOD';
  dbedtPremioLordo.ReadOnly   := UniMainModule.operazione = 'MOD';
  dbedtImposte.ReadOnly       := UniMainModule.operazione = 'MOD';
  dbedtImponibile.ReadOnly    := UniMainModule.operazione = 'MOD';

  if (UniMainModule.operazione='INS') then
  begin
     UniMainModule.traccia('INS-TITOLO',0,'','START INS TIT '+DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente);
     imposta_campi(UniMainModule.operazione,'');
     UniMainModule.DMdatiAgeTitoli.QTitoloEdit.FieldByName('rif_cod_compagnia').AsInteger:=1;
  end else
     imposta_campi(UniMainModule.operazione, UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('tipo_titolo').AsString);

end;


procedure TFTitoloEdit.actConfermaExecute(Sender: TObject);
var
  valuta, sigla_titolo: string;
  lordo, imponibile, tasse: Currency;
  procedi: Boolean;

  function cercaTIT_1011: Boolean;
  begin
     // cerca se esiste il titolo in scadenze o s_scadenze
     UniMainModule.DMdatiAgeTitoli.QcercaTitoli1011.Close;
     UniMainModule.DMdatiAgeTitoli.QcercaTitoli1011.ParamByName('n_polizza').AsString := dsoEdit.DataSet.FieldByName('n_polizza').AsString;
     UniMainModule.DMdatiAgeTitoli.QcercaTitoli1011.ParamByName('tipo_titolo').AsString := dsoEdit.DataSet.FieldByName('tipo_titolo').AsString;
     UniMainModule.DMdatiAgeTitoli.QcercaTitoli1011.Open;
     Result := UniMainModule.DMdatiAgeTitoli.QcercaTitoli1011.RecordCount>0 ;
  end;

  function cercaTIT_20: Boolean;
  begin
     // cerca se esiste il titolo in scadenze o s_scadenze
     UniMainModule.DMdatiAgeTitoli.Qscadenze.Close;
     UniMainModule.DMdatiAgeTitoli.Qscadenze.ParamByName('n_polizza').AsString := dsoEdit.DataSet.FieldByName('n_polizza').AsString;
     UniMainModule.DMdatiAgeTitoli.Qscadenze.ParamByName('tipo_titolo').AsString := dsoEdit.DataSet.FieldByName('tipo_titolo').AsString;
     UniMainModule.DMdatiAgeTitoli.Qscadenze.ParamByName('rif_cod_compagnia').AsInteger := 1;
     UniMainModule.DMdatiAgeTitoli.Qscadenze.ParamByName('rata').AsDateTime := dsoEdit.DataSet.FieldByName('decorrenza').AsDateTime;
     UniMainModule.DMdatiAgeTitoli.Qscadenze.Open;
     Result := UniMainModule.DMdatiAgeTitoli.Qscadenze.RecordCount>0 ;

     UniMainModule.DMdatiAgeTitoli.QscadenzeStorico.Close;
     UniMainModule.DMdatiAgeTitoli.QscadenzeStorico.ParamByName('n_polizza').AsString := dsoEdit.DataSet.FieldByName('n_polizza').AsString;
     UniMainModule.DMdatiAgeTitoli.QscadenzeStorico.ParamByName('tipo_titolo').AsString := dsoEdit.DataSet.FieldByName('tipo_titolo').AsString;
     UniMainModule.DMdatiAgeTitoli.QscadenzeStorico.ParamByName('rif_cod_compagnia').AsInteger := 1;
     UniMainModule.DMdatiAgeTitoli.QscadenzeStorico.ParamByName('rata').AsDateTime := dsoEdit.DataSet.FieldByName('decorrenza').AsDateTime;
     UniMainModule.DMdatiAgeTitoli.QscadenzeStorico.Open;
     Result := Result and (UniMainModule.DMdatiAgeTitoli.Qscadenze.RecordCount>0) ;
  end;

begin
  procedi := True;
  if (cbbTipoTitolo.Text > '') and (dbedtContraente.Text > '') then
  begin
     if pos(dsoEdit.DataSet.FieldByName('tipo_titolo').AsString,'10.11')>0 then
     begin
        // controlla che non esista gi� un titolo dello stesso tipo nello storico o in scadenze.dat
        if cercaTIT_1011 and (UniMainModule.operazione='INS') then
        begin
           procedi:=false;
           ShowMessage('Esiste gi� un titolo codice '+dsoEdit.DataSet.FieldByName('tipo_titolo').AsString+
                       ' relativo alla polizza '+dsoEdit.DataSet.FieldByName('n_polizza').AsString);
        end;
     end else if (dsoEdit.DataSet.FieldByName('tipo_titolo').AsString='20') then
     begin
        // controlla che non ci sia un titolo identico in scadenze.dat o s_scadenze.dat
        if cercaTIT_20 and (UniMainModule.operazione='INS') then
        begin
           procedi:=false;
           ShowMessage('Esiste gi� una quietanza relativa alla polizza '+dsoEdit.DataSet.FieldByName('n_polizza').AsString +
                       ' con la stessa data di effetto.');
        end;
     end;
  end;

  if procedi then
  begin
     UniMainModule.traccia(UniMainModule.operazione+'-TITOLI',dsoEdit.DataSet.FieldByName('cod_scadenza').AsInteger,'',
                           'END ' + UniMainModule.operazione + ' TIT '+
                           DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente + ' - pol:' +
                           dsoEdit.DataSet.FieldByName('n_polizza').AsString+' - '+
                           dsoEdit.DataSet.FieldByName('decorrenza').AsString+' - '+dsoEdit.DataSet.FieldByName('tipo_titolo').AsString);
     inherited
  end;

end;

initialization

RegisterClass(TFTitoloEdit);

end.
