inherited FClienti: TFClienti
  ClientHeight = 493
  ClientWidth = 968
  Caption = 'Gestione Clienti'
  ExplicitWidth = 984
  ExplicitHeight = 532
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 968
    ExplicitWidth = 968
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 968
    Height = 468
    ExplicitWidth = 968
    ExplicitHeight = 468
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 968
      ExplicitWidth = 968
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 968
        ExplicitWidth = 968
        inherited btnVisualizza: TUniSpeedButton
          Action = actVisualizza
          TabOrder = 6
        end
        object btnCancellaCliente: TUniSpeedButton [4]
          Left = 458
          Top = 0
          Width = 106
          Height = 43
          Hint = ''
          Caption = 'Chiudi'
          ParentFont = False
          Font.Color = clRed
          Font.Height = -13
          Font.Style = [fsBold, fsUnderline]
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 5
        end
        inherited btnChiudi: TUniSpeedButton
          Caption = 'Cancella'
          Font.Color = clBlack
          Font.Style = []
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 968
        ExplicitWidth = 968
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 968
      Height = 408
      ExplicitWidth = 968
      ExplicitHeight = 408
      inherited grdElenco: TUniDBGrid
        Width = 968
        Height = 286
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgFilterClearButton, dgAutoRefreshRow, dgRowNumbers]
        OnSelectionChange = grdElencoSelectionChange
        Columns = <
          item
            FieldName = 'DENOMINAZ'
            Filtering.Enabled = True
            Filtering.Editor = unedtNome
            Filtering.ChangeDelay = 500
            Title.Caption = 'Nome'
            Width = 250
            Sortable = True
          end
          item
            FieldName = 'SIGLA'
            Filtering.Enabled = True
            Filtering.Editor = unedtSiglaCollab
            Filtering.ChangeDelay = 500
            Title.Caption = 'Sub.Age'
            Width = 60
            Sortable = True
          end
          item
            FieldName = 'siglaSubColl'
            Title.Caption = 'S.Coll'
            Width = 40
          end
          item
            FieldName = 'INDIRIZZO'
            Title.Caption = 'Indirizzo'
            Width = 220
            Sortable = True
          end
          item
            FieldName = 'CITTA'
            Filtering.Enabled = True
            Filtering.Editor = unedtCitta
            Filtering.ChangeDelay = 500
            Title.Caption = 'Citt'#224
            Width = 150
            Sortable = True
          end>
      end
      object UniSplitter1: TUniSplitter [1]
        Left = 0
        Top = 286
        Width = 968
        Height = 9
        Cursor = crVSplit
        Hint = ''
        Align = alBottom
        ParentColor = False
        Color = clBtnFace
        ExplicitLeft = 3
        ExplicitTop = 272
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 168
        Top = 54
        ExplicitLeft = 168
        ExplicitTop = 54
        object unedtNome: TUniEdit
          Left = 24
          Top = 32
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTNOME'
          TabOrder = 1
        end
        object unedtSiglaCollab: TUniEdit
          Left = 24
          Top = 60
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTSIGLACOLLAB'
          TabOrder = 2
        end
        object unedtCitta: TUniEdit
          Left = 24
          Top = 88
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTCITTA'
          TabOrder = 3
        end
      end
      object pnlContratti: TUniPanel
        Left = 0
        Top = 295
        Width = 968
        Height = 113
        Hint = ''
        Visible = False
        Align = alBottom
        TabOrder = 3
        TitleVisible = True
        Title = 'Contratti'
        Caption = ''
        Collapsible = True
        CollapseDirection = cdTop
        Collapsed = True
        ExplicitTop = 254
        object UniDBGrid1: TUniDBGrid
          Left = 1
          Top = 1
          Width = 966
          Height = 111
          Hint = ''
          Visible = False
          WebOptions.Paged = False
          LoadMask.Message = 'Loading data...'
          Align = alClient
          TabOrder = 1
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    inherited actInsert: TAction
      Caption = 'Nuovo cliente'
    end
    object actCancellaCliente: TAction
      Category = 'ItemOperation'
      Caption = 'Cancella'
      OnExecute = actCancellaClienteExecute
    end
    object actVisualizza: TAction
      Category = 'ItemOperation'
      Caption = 'Visualizza'
      OnExecute = actVisualizzaExecute
    end
  end
  object dsGridpolizze: TDataSource
    Left = 584
    Top = 416
  end
end
