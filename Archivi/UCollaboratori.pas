unit UCollaboratori;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, Vcl.Menus,
  uniMainMenu, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniButton, uniBitBtn, uniSpeedButton,
  uniGUIBaseClasses, UQueryElencoIntf, uniEdit;

type
  TFCollaboratori = class(TFMasterElenco)
    unedtNome: TUniEdit;
    unedtCitta: TUniEdit;
    unedtSigla: TUniEdit;
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoSelectionChange(Sender: TObject);
  private
    { Private declarations }
    ParamsList: TStringList;
  public
    { Public declarations }
    produtt_padre: Integer;
    filtroLivello: Integer;

    constructor Create(AOwner: TComponent; p_padre: Integer; sololivello1: Integer = 0); reintroduce; overload;
  end;

function FCollaboratori: TFCollaboratori;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMAgeCollaboratori;

function FCollaboratori: TFCollaboratori;
begin
  Result := TFCollaboratori(UniMainModule.GetFormInstance(TFCollaboratori));
end;

constructor TFCollaboratori.Create(AOwner: TComponent; p_padre: Integer; sololivello1: Integer = 0);
begin
  inherited  Create(AOwner);
  produtt_padre := p_padre;
  filtroLivello := sololivello1;
end;

procedure TFCollaboratori.grdElencoSelectionChange(Sender: TObject);
begin
  SelectedCode        := dsGrid.dataset.fieldbyname('COD_PRODUTTORE').asinteger;
  SelectedDescription := dsGrid.dataset.fieldbyname('NOME').AsString;
end;

procedure TFCollaboratori.UniFormCreate(Sender: TObject);
begin
  UniMainModule.archivio := 'COL';
  inherited;
  self.DMMaster  := (UniMainModule.DMdatiAgeCollaboratori as IQueryElenco);
  dsGrid.dataset := UniMainModule.DMdatiAgeCollaboratori.Qpromoter;
  if produtt_padre > 0 then
  begin
    ParamsList := TStringList.Create;
    ParamsList.Append('link_produtt=' + inttostr(produtt_padre));
    self.DMMaster.EseguiQuery('NOME', ParamsList, '');
  end
  else if produtt_padre < 0 then
  begin
     // caso speciale per polizza intermediata da ...
    produtt_padre:= -produtt_padre;
    ParamsList := TStringList.Create;
    ParamsList.Append('#((link_produtt=' + inttostr(produtt_padre)+ ') or (cod_produttore=' +inttostr(produtt_padre)  +'))');
    self.DMMaster.EseguiQuery('NOME', ParamsList, '');
  end else
    self.DMMaster.EseguiQuery('NOME', filtroLivello);

  dsGrid.DataSet.Filtered := False;
  dsGrid.DataSet.Filter   := '';
  EditPageClassName := 'TFCollaboratoreEdit';

end;

end.
