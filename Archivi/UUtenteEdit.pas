unit UUtenteEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, uniPanel, uniImageList, uniEdit,
  uniCheckBox, uniDBCheckBox, uniDBEdit, Vcl.Dialogs, uniPageControl;

type
  TFUtenteEdit = class(TFMasterEdit)
    pnlEdit: TUniSimplePanel;
    unidbedtNOME: TUniDBEdit;
    unidbedtSIGLA: TUniDBEdit;
    uniedtActPassword: TUniEdit;
    uniedtNewPassword: TUniEdit;
    uniedtNewPasswordConfirm: TUniEdit;
    lblNotaPassword: TUniLabel;
    lblNotaSave: TUniLabel;
    uniedtReferenteCollegato: TUniEdit;
    btnSelezionaReferente: TUniBitBtn;
    btnDelReferente: TUniBitBtn;
    actSelezionaReferente: TAction;
    actDelReferente: TAction;
    UniPCautorizzazioni: TUniPageControl;
    UniTabSheet1: TUniTabSheet;
    UniTabSheet2: TUniTabSheet;
    ckbxStampaPolizze: TUniDBCheckBox;
    ckbxStampaFascicoli: TUniDBCheckBox;
    ckbxSTAMPA_QUIETANZE: TUniDBCheckBox;
    ckbxNO_INCASSO: TUniDBCheckBox;
    UniDBCheckBox1: TUniDBCheckBox;
    procedure actDelReferenteExecute(Sender: TObject);
    procedure actSelezionaReferenteExecute(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure actConfermaExecute(Sender: TObject);
    procedure uniedtActPasswordChange(Sender: TObject);
  private
  { Private declarations }
    const
    pwdNotInserted = 'Password non inserita';

    function AskPasswordAdmin: boolean;
    function checkPasswordAdmin(const APasswordAdmin: string): boolean;
    procedure SetCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);

  public
    { Public declarations }
    procedure ProtectSystemData(IsUserAdmin: boolean);

  end;

function FUtenteEdit: TFUtenteEdit;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UCollaboratori;

function FUtenteEdit: TFUtenteEdit;
begin
  Result := TFUtenteEdit(UniMainModule.GetFormInstance(TFUtenteEdit));
end;

procedure TFUtenteEdit.actConfermaExecute(Sender: TObject);
  procedure CheckAndChangePassword;
  begin
    if (not UniMainModule.DMdatiAgeUtenti.CheckPassword(uniedtActPassword.Text)) and (UniMainModule.operazione <> 'INS')
    then
      ShowMessage('Password attuale non corretta')
    else
      if uniedtNewPassword.Text <> uniedtNewPasswordConfirm.Text then
        ShowMessage('Le nuove password non corrispondono')
      else
      begin
        UniMainModule.DMdatiAgeUtenti.ChangePassword(Trim(uniedtNewPassword.Text));
        inherited;
      end;
  end;

  procedure ValidateSystemsData;
  begin
    if AskPasswordAdmin then
      inherited;
  end;

begin
  // if not UniMainModule.IsUserAdmin and not UniMainModule.DMdatiAgeUtenti.CheckPassword(uniedtActPassword.Text) then
  // ShowMessage('Password attuale non corretta')
  // else
  // if uniedtNewPassword.Text <> uniedtNewPasswordConfirm.Text then
  // ShowMessage('Le nuove password non corrispondono')
  // else
  // if UniMainModule.IsUserAdmin and AskPasswordAdmin then
  // inherited
  // else
  // begin
  // UniMainModule.DMdatiAgeUtenti.ChangePassword(Trim(uniedtNewPassword.Text));
  // inherited;
  // end;

  {
    if not UniMainModule.IsUserAdmin then
    CheckAndChangePassword
    else
    ValidateSystemsData;
  }
  if not UniMainModule.IsUserAdmin then
  begin
    // cambio password da parte di un utente normale
    // devi verificare che l'utente abbia inserito correttamente la password precedente se non � un inserimento
    if (UniMainModule.operazione <> 'INS') and (not UniMainModule.DMdatiAgeUtenti.CheckPassword(uniedtActPassword.Text))
    then
      ShowMessage('Password attuale non corretta')
    else
    begin

      if uniedtNewPassword.Text <> uniedtNewPasswordConfirm.Text then
        ShowMessage('Le nuove password non corrispondono')
      else
      begin
        UniMainModule.DMdatiAgeUtenti.ChangePassword(Trim(uniedtNewPassword.Text));
        DSoEdit.DataSet.Post;
        Close;
      end;
    end;
  end
  else
  begin
    // cambio password da parte di un superuser
    // la vecchia password non serve
    if uniedtNewPassword.Text <> uniedtNewPasswordConfirm.Text then
      ShowMessage('Le nuove password non corrispondono')
    else
    begin
      UniMainModule.DMdatiAgeUtenti.ChangePassword(Trim(uniedtNewPassword.Text));
      DSoEdit.DataSet.Post;
      Close;
    end;
  end;
end;

procedure TFUtenteEdit.actDelReferenteExecute(Sender: TObject);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('PROMOTER').Clear;
  uniedtReferenteCollegato.Text := '';

end;

procedure TFUtenteEdit.actSelezionaReferenteExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('COL', 'MOD', 'Agenti-Dipendenti-Collaboratori');
  uniedtReferenteCollegato.Text := UniMainModule.DMdatiAge.getNomePromoter(DSoEdit.DataSet.FieldByName('PROMOTER')
    .AsInteger);

  with TFCollaboratori.Create(UniApplication) do
  begin
    OnSelectedItem := SetCollaboratore;
    ShowModal;
  end;

end;

procedure TFUtenteEdit.ProtectSystemData(IsUserAdmin: boolean);
begin
  unidbedtNOME.Enabled             := IsUserAdmin;
  unidbedtSIGLA.Enabled            := IsUserAdmin;
  ckbxStampaPolizze.Enabled        := IsUserAdmin;
  ckbxStampaFascicoli.Enabled      := IsUserAdmin;
  uniedtReferenteCollegato.Enabled := IsUserAdmin;
  btnSelezionaReferente.Enabled    := IsUserAdmin;
  btnDelReferente.Enabled          := IsUserAdmin;
  UniPCautorizzazioni.Visible       := IsUserAdmin;

end;

procedure TFUtenteEdit.SetCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('PROMOTER').AsInteger := ASelectedItem;
  uniedtReferenteCollegato.Text                     := ASelectedDescription;
end;

procedure TFUtenteEdit.uniedtActPasswordChange(Sender: TObject);
begin
  DSoEdit.DataSet.Edit;
end;


procedure TFUtenteEdit.UniFormCreate(Sender: TObject);
begin
  DSoEdit.DataSet := UniMainModule.DMdatiAgeUtenti.QUtenti;
  inherited;
  uniedtReferenteCollegato.Text := UniMainModule.DMdatiAge.getNomePromoter(DSoEdit.DataSet.FieldByName('PROMOTER')
    .AsInteger);
  ProtectSystemData(UniMainModule.IsUserAdmin);

  if (UniMainModule.operazione = 'INS') then
  begin
    uniedtActPassword.Visible := False;
    lblNotaPassword.Visible   := False;
  end;

  // se l'utente loggato corrente � di livello 11 (SLP) e l'utente che si sta modificando � <> da SLP
  // allora non mostrare le parti che richiedono di inserire la password attuale
  if UniMainModule.IsUserAdmin and (DSoEdit.DataSet.FieldByName('livello').AsInteger<>11) then
  begin
     // utente amministratore
     // la password di controllo deve essere chiesta solo se stiamo parlando di un pari livello
     uniedtActPassword.Visible := false;
     lblNotaPassword.Visible   := false;
     lblNotaSave.Visible       := False;
  end;

end;

function TFUtenteEdit.AskPasswordAdmin: boolean;
var
  adminPassword: string;
  BoxCaption: string;

begin
  // while ((adminPassword <> pwdNotInserted) and not checkPasswordAdmin(adminPassword)) do
  // begin
  //
  // adminPassword := inputBox('Inserire password amministrazione', #31'Password', pwdNotInserted);
  // if (adminPassword <> pwdNotInserted) and not checkPasswordAdmin(adminPassword) then
  // ShowMessage('Password non corretta');
  // end;
  BoxCaption := 'Inserire password amministrazione';
  repeat
    if adminPassword <> '' then
      BoxCaption := 'Password errata - reinserire password amministrazione';

    adminPassword := inputBox(BoxCaption, #31'Password', pwdNotInserted);

  until ((adminPassword = pwdNotInserted) or checkPasswordAdmin(adminPassword));
  if adminPassword = pwdNotInserted then
    Result := False
  else
    Result := True;
end;

function TFUtenteEdit.checkPasswordAdmin(const APasswordAdmin: string): boolean;
begin
  Result := APasswordAdmin = UniMainModule.utentePassword;
end;

initialization

RegisterClass(TFUtenteEdit);

end.
