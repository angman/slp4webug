inherited FMandanti: TFMandanti
  ClientWidth = 956
  Caption = 'Mandanti'
  ExplicitWidth = 972
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 956
    ExplicitWidth = 956
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 956
    ExplicitWidth = 956
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 956
      ExplicitWidth = 956
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 956
      ExplicitWidth = 956
      inherited grdElenco: TUniDBGrid
        Width = 956
        Columns = <
          item
            FieldName = 'NOME'
            Title.Caption = 'Nome'
            Width = 286
          end
          item
            FieldName = 'Descriz'
            Title.Caption = 'Descriz'
            Width = 295
          end
          item
            FieldName = 'CITTA'
            Title.Caption = 'CITTA'
            Width = 150
          end
          item
            FieldName = 'INDIRIZZO'
            Title.Caption = 'Indirizzo'
            Width = 200
          end>
      end
    end
  end
  inherited dsGrid: TDataSource
    DataSet = DMMandanti.QCompagnie
  end
end
