unit UContrattoEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, System.IOUtils,
  uniGUIClasses, uniGUIForm, UMasterEdit, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses, uniPanel, UdmdatiAge, uniMultiItem,
  uniComboBox, uniDBComboBox, uniDBLookupComboBox, uniGroupBox, uniPageControl, uniEdit, uniDBEdit,
  uniRadioGroup, uniDateTimePicker, uniDBDateTimePicker, uniImageList, uniMemo, uniDBMemo, UDMdatiAgePolizze,
  uniBasicGrid, uniDBGrid, uniSplitter, uniCheckBox, uniDBCheckBox, System.Rtti, System.Math;

type
  TFContrattoEdit = class(TFMasterEdit)
    pcContrattoEdit: TUniPageControl;
    pgEdit: TUniTabSheet;
    pgNota: TUniTabSheet;
    ungrpbxMandanteRamoTipoPol: TUniGroupBox;
    cbbCompagnia: TUniDBLookupComboBox;
    cbbTipoPolizza: TUniDBLookupComboBox;
    cbbRamo: TUniDBLookupComboBox;
    cbbAppoggiata: TUniDBLookupComboBox;
    dbedtNPolizza: TUniDBEdit;
    dbedtCodPolizza: TUniDBEdit;
    cbbProvvigioni: TUniDBComboBox;
    ungrpbxDati: TUniGroupBox;
    unedtReferente: TUniEdit;
    dtpckDECORRENZA: TUniDBDateTimePicker;
    dtpckSCADENZA: TUniDBDateTimePicker;
    dtpckEmissione: TUniDBDateTimePicker;
    dtpckSOSPESO_DAL: TUniDBDateTimePicker;
    dtpckSOSPESO_AL: TUniDBDateTimePicker;
    dbedtContraente: TUniDBEdit;
    btnSelezionaContraente: TUniBitBtn;
    cbbFrazionamento: TUniDBComboBox;
    dbedtIntermediataDa: TUniDBEdit;
    dbedtIntermDaDesc: TUniDBEdit;
    dbedtN_POLIZZA_SOSTITUITA: TUniDBEdit;
    dbedtDescrizione: TUniDBEdit;
    cbbConvenzione: TUniDBLookupComboBox;
    dbedtSubPromoter: TUniDBEdit;
    ungrpbxDatIndicizazzione: TUniGroupBox;
    ungrpbxPremio: TUniGroupBox;
    ungrpbxDatiAnnullContratto: TUniGroupBox;
    dtpckDataStartIndice: TUniDBDateTimePicker;
    dtpckDataEndIndice: TUniDBDateTimePicker;
    dbedtIncIndice: TUniDBNumberEdit;
    dbedtLORDO: TUniDBNumberEdit;
    dbedtnetto: TUniDBNumberEdit;
    dbedtptasse: TUniDBNumberEdit;
    dbedttasse: TUniDBNumberEdit;
    dbedtaccessori: TUniDBNumberEdit;
    dtpckDataMorte: TUniDBDateTimePicker;
    cbbCausaMorte: TUniDBLookupComboBox;
    btnSelezionaReferente: TUniBitBtn;
    btnDelReferente: TUniBitBtn;
    undbmNOTE: TUniDBMemo;
    actSelezionaReferente: TAction;
    actDelReferente: TAction;
    actSelezionaContraente: TAction;
    btnSelezionaCollaboratore: TUniBitBtn;
    btnDelCollaboratore: TUniBitBtn;
    actSelezionaCollaboratore: TAction;
    pgGaranzie: TUniTabSheet;
    pgAssicurati: TUniTabSheet;
    pgAppendici: TUniTabSheet;
    pgTitoliScadenze: TUniTabSheet;
    grdGaranzie: TUniDBGrid;
    dsGaranzie: TDataSource;
    dsAssicurati: TDataSource;
    grdTitScadenze: TUniDBGrid;
    pnlFilter: TUniSimplePanel;
    rgGrpTipoMovimento: TUniRadioGroup;
    UniSimplePanel1: TUniSimplePanel;
    rgGrpStatoTitoli: TUniRadioGroup;
    btnInserisciAss: TUniSpeedButton;
    actInsOggGaranzia: TAction;
    actModOggGaranzia: TAction;
    btnDelOggGaranzia: TUniSpeedButton;
    actDelOggGaranzia: TAction;
    btnGeneraAppendice: TUniSpeedButton;
    actGeneraAppendice: TAction;
    pnlBtnlAppendici: TUniSimplePanel;
    btnDelAppendice: TUniSpeedButton;
    btnRistampaAppendice: TUniSpeedButton;
    actDelAppendice: TAction;
    actRistampaAppendice: TAction;
    pnlPnlAppendici: TUniPanel;
    unspltrApp: TUniSplitter;
    pnlDettAppendici: TUniPanel;
    grdAppendici: TUniDBGrid;
    grdDettAppendici: TUniDBGrid;
    dsAppendice: TDataSource;
    dsDettAppendice: TDataSource;
    pnlGridAssicurati: TUniSimplePanel;
    grdAssicurati: TUniDBGrid;
    unspltrAssicurati: TUniSplitter;
    pcOggettoAssicurato: TUniPageControl;
    pgPatente: TUniTabSheet;
    pgVeicolo: TUniTabSheet;
    pgPersona: TUniTabSheet;
    pgContratto: TUniTabSheet;
    pgAltro: TUniTabSheet;
    pnlButtonsEditAss: TUniSimplePanel;
    btnBconferma: TUniBitBtn;
    btnBabbandona: TUniBitBtn;
    dsEditOggAssicurato: TDataSource;
    cbbTipoVeicolo: TUniDBLookupComboBox;
    dbedtMarca: TUniDBEdit;
    dbedtModello: TUniDBEdit;
    dbedtTarga: TUniDBEdit;
    dbedtTelaio: TUniDBEdit;
    dbedtQlHp: TUniDBEdit;
    dtpckScadenzaRevisione: TUniDBDateTimePicker;
    dtpckDataRilascio: TUniDBDateTimePicker;
    pnlTipoOggAssicurato: TUniPanel;
    rgGrpTipoOggAssicurato: TUniRadioGroup;
    cbbTipoAlimentazione: TUniDBComboBox;
    ckbxAziendale: TUniDBCheckBox;
    grpbxDatiPra: TUniGroupBox;
    dbedtPraNome: TUniDBEdit;
    dbedtPraCodFisc: TUniDBEdit;
    dbedtPraIndirizzo: TUniDBEdit;
    dbedtPraCitta: TUniDBEdit;
    dbedtPraCap: TUniDBEdit;
    dbedtPraProvincia: TUniDBEdit;
    cbbCategoriaPat: TUniDBComboBox;
    dbedtCodiceFiscale: TUniDBEdit;
    dbedtDenominazione: TUniDBEdit;
    dbedtPatente: TUniDBEdit;
    dtpckDataRilascioPat: TUniDBDateTimePicker;
    dtpckDataScadenzaPat: TUniDBDateTimePicker;
    dbedtPersNome: TUniDBEdit;
    dbedtPersCodFisc: TUniDBEdit;
    dbedtPersIndirizzo: TUniDBEdit;
    dbedtPersCitta: TUniDBEdit;
    dbedtPersCAP: TUniDBEdit;
    dbedtPersProvincia: TUniDBEdit;
    dbedtConNPolizza: TUniDBEdit;
    dbedtConCompagnia: TUniDBEdit;
    dbedtConImporto: TUniDBFormattedNumberEdit;
    dtpckConDecorrenza: TUniDBDateTimePicker;
    dtpckConScadenza: TUniDBDateTimePicker;
    dbedtConAgenzia: TUniDBEdit;
    dbedtConGaranzia: TUniDBEdit;
    dbedtConGarPrescelte: TUniDBEdit;
    dbedtAltDescrizione: TUniDBEdit;
    ungrpbxUbicazione: TUniGroupBox;
    dbedtAltINDIRIZZO: TUniDBEdit;
    dbedtAltCITTA: TUniDBEdit;
    dbedtAltCAP: TUniDBEdit;
    dbedtAltPROV: TUniDBEdit;
    dtpckDataIngresso: TUniDBDateTimePicker;
    dsScadenze: TDataSource;
    lbl1: TUniLabel;
    dsTipoVeicolo: TDataSource;
    uncbxModificaDatiAnnullamento: TUniCheckBox;
    procedure UniFormCreate(Sender: TObject);
    procedure actSelezionaContraenteExecute(Sender: TObject);
    procedure actSelezionaReferenteExecute(Sender: TObject);
    procedure actDelReferenteExecute(Sender: TObject);
    procedure actSelezionaCollaboratoreExecute(Sender: TObject);
    procedure cbbTipoPolizzaChange(Sender: TObject);
    procedure rgGrpTipoMovimentoClick(Sender: TObject);
    procedure rgGrpStatoTitoliClick(Sender: TObject);
    procedure btnBconfermaClick(Sender: TObject);
    procedure btnBabbandonaClick(Sender: TObject);
    procedure rgGrpTipoOggAssicuratoClick(Sender: TObject);
    procedure actConfermaExecute(Sender: TObject);
    procedure actInsOggGaranziaExecute(Sender: TObject);
    procedure actDelOggGaranziaExecute(Sender: TObject);
    procedure grdAssicuratiSelectionChange(Sender: TObject);
    procedure actGeneraAppendiceExecute(Sender: TObject);
    procedure uncbxModificaDatiAnnullamentoClick(Sender: TObject);
    procedure actRistampaAppendiceExecute(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure grdTitScadenzeDblClick(Sender: TObject);
    procedure actAnnullaExecute(Sender: TObject);
  private
    { Private declarations }
    CanClose: boolean;
    tipo_prec: string;
    procedure AbbandonaOggAssicurato;
    procedure EnableOggettoInGaranzia(Enable: boolean; EnableTogli: boolean);
    procedure EnableAppendici(Enable: boolean);
    function getPageIxFromTipoAssicurato: Integer;
    procedure OggAssicuratoDisableEdit(Disable: boolean);
    procedure SetControlReadOnly(ATabSheet: TUniTabSheet; ReadOnlyState: boolean = False);
    procedure SetContraente(ASelectedItem: Integer; const ASelectedDescription: string);
    procedure SetReferente(ASelectedItem: Integer; const ASelectedDescription: string);
    procedure SetCollaboratore(ASelectedItem: Integer; const ASelectedDescription: string);
    procedure sincronizza;
    procedure togliOggetto(Sender: TComponent; AResult: Integer);
    function valida_dati_oggetti: string;
    procedure imposta_tasti_ogg_assic(attiva: boolean);

  public
    { Public declarations }
  end;

function FContrattoEdit: TFContrattoEdit;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UClienti, UCollaboratori, uniGUIDialogs, URistampaAppendice, Uchiedi_data,
  ServerModule, UDMDatiReport;

const
  ArOggettoAssicurato: array [0 .. 4] of string = ('P', 'V', 'N', 'C', 'A');

function FContrattoEdit: TFContrattoEdit;
begin
  Result := TFContrattoEdit(UniMainModule.GetFormInstance(TFContrattoEdit));
end;

procedure TFContrattoEdit.actAnnullaExecute(Sender: TObject);
begin
  inherited;
  if UniMainModule.operazione = 'INS' then
  begin
     // MB 23122020
     DSoEdit.DataSet.Delete;
  end;

end;

procedure TFContrattoEdit.actConfermaExecute(Sender: TObject);
begin
  // mb 18112020
  // tolto in quanto comunicamancaza dei dati a sroposito ..
  // btnBconfermaClick(nil);
  inherited;
end;

procedure TFContrattoEdit.actDelOggGaranziaExecute(Sender: TObject);
begin
  if (UniMainModule.UtenteNoAppendici = 'S') then
    ShowMessage(UniServerModule.const_OPERAZIONE_NON_AUTORIZZATA)
  else
  { TODO : Definire messaggio in UCodiciErrorePolizza }
    TFchiedi_data.Showchiedi_data(uniApplication, 'Inserire la data di uscita', togliOggetto);
end;

procedure TFContrattoEdit.togliOggetto(Sender: TComponent; AResult: Integer);
begin
  if AResult = mrOk then
  begin
    tipo_prec:= UniMainModule.DMdatiAgePolizze.QAssicuratiPolizzaEdit.FieldByName('tipo').AsString;
    UniMainModule.DMdatiAgePolizze.TogliOggAssicuratoPolizza
      (UniMainModule.DMdatiAgePolizze.QPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger,
      UniMainModule.DMdatiAgePolizze.QAssicuratiPolizzaEdit.FieldByName('COD_ASSICURATI').AsInteger,
      UniMainModule.dataChiediData);
    dsAssicurati.DataSet.Close;
    dsAssicurati.DataSet.Open;
    grdAssicurati.Refresh;


  if tipo_prec='P' then pcOggettoAssicurato.ActivePage := pgPatente
  else if tipo_prec='V' then pcOggettoAssicurato.ActivePage := pgVeicolo
  else if tipo_prec='C' then pcOggettoAssicurato.ActivePage := pgContratto
  else pcOggettoAssicurato.ActivePage := pgPatente;
  EnableAppendici(not dsAppendice.DataSet.IsEmpty);
  EnableOggettoInGaranzia(not dsEditOggAssicurato.DataSet.IsEmpty, not dsAssicurati.DataSet.IsEmpty);
  rgGrpTipoMovimentoClick(Sender);


    // ShowMessage('Confermato ....' + DateToStr(UniMainModule.dataChiediData));
  end;

end;

procedure TFContrattoEdit.uncbxModificaDatiAnnullamentoClick(Sender: TObject);
begin
  dtpckDataMorte.ReadOnly := not uncbxModificaDatiAnnullamento.Checked;
  cbbCausaMorte.ReadOnly  := dtpckDataMorte.ReadOnly;
  dtpckDataMorte.Color    := IfThen(dtpckDataMorte.ReadOnly, clYellow, clWindow);
  cbbCausaMorte.Color     := IfThen(cbbCausaMorte.ReadOnly, clYellow, clWindow);
end;

procedure TFContrattoEdit.actDelReferenteExecute(Sender: TObject);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('RIF_REFERENTE').Clear;
  unedtReferente.Text := '';

end;

procedure TFContrattoEdit.actGeneraAppendiceExecute(Sender: TObject);
begin
  if (UniMainModule.UtenteNoAppendici = 'S') then
    ShowMessage(UniServerModule.const_OPERAZIONE_NON_AUTORIZZATA)
  else
    TFRistampaAppendice.ShowRistampaAppendiceForm(uniApplication, UniMainModule.DMdatiAgePolizze.QPolizzaEdit);
end;

procedure TFContrattoEdit.actInsOggGaranziaExecute(Sender: TObject);
begin
  if (UniMainModule.UtenteNoAppendici = 'S') then
    ShowMessage(UniServerModule.const_OPERAZIONE_NON_AUTORIZZATA)
  else
  begin
    imposta_tasti_ogg_assic(false);
    rgGrpTipoOggAssicurato.ItemIndex    := 0;

    if tipo_prec='P' then
    begin
       pcOggettoAssicurato.ActivePage := pgPatente;
       rgGrpTipoOggAssicurato.ItemIndex:=0;
    end else if tipo_prec='V' then
    begin
       pcOggettoAssicurato.ActivePage := pgVeicolo;
       rgGrpTipoOggAssicurato.ItemIndex:=1;
    end else if tipo_prec='C' then
    begin
       pcOggettoAssicurato.ActivePage := pgContratto;
       rgGrpTipoOggAssicurato.ItemIndex:=3;
    end else begin
       pcOggettoAssicurato.ActivePage := pgPatente;
       rgGrpTipoOggAssicurato.ItemIndex:=0;
    end;

    // pcOggettoAssicurato.ActivePageIndex := 0;
    SetControlReadOnly(pcOggettoAssicurato.ActivePage, False);

    UniMainModule.DMdatiAgePolizze.AggiungiOggAssicuratoPolizza
      (UniMainModule.DMdatiAgePolizze.QPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger);
    OggAssicuratoDisableEdit(dsEditOggAssicurato.DataSet.IsEmpty);
    EnableOggettoInGaranzia(not dsEditOggAssicurato.DataSet.IsEmpty, not dsAssicurati.DataSet.IsEmpty);

  end;
end;

procedure TFContrattoEdit.actRistampaAppendiceExecute(Sender: TObject);
var
  ReportFileName: string;
  PDFUrl: string;
  PDFFolderPath: string;
begin
  if dsAppendice.DataSet.RecordCount > 0 then
  begin
    PDFUrl        := UniServerModule.FilesFolderURL + 'PDF/';
    PDFFolderPath := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');

    ReportFileName := UniMainModule.DMDatiReport.SalvaTempDocument(PDFFolderPath,
      dsAppendice.DataSet.FieldByName('cod_appendice').AsInteger, true, tdAppendice);
    UniMainModule.DMDatiReport.ShowReportAppendice(ReportFileName, true);
  end
  else
   { TODO : Definire messaggio in UCodiciErrorePolizza }
   ShowMessage('Non sono presenti appendici da spampare.');
end;

procedure TFContrattoEdit.actSelezionaCollaboratoreExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('COL', 'MOD', 'Collaboratori');

  with TFCollaboratori.Create(uniApplication) do
  begin
    OnSelectedItem := SetCollaboratore;
    ShowModal;
  end;

end;

procedure TFContrattoEdit.actSelezionaContraenteExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('CLI', 'MOD', 'Clienti');

  with TFClienti.Create(uniApplication) do
  begin
    OnSelectedItem := SetContraente;
    ShowModal;
  end;

end;

procedure TFContrattoEdit.actSelezionaReferenteExecute(Sender: TObject);
begin
  UniMainModule.aggiornaPosizione('CLI', 'MOD', 'Clienti');

  with TFClienti.Create(uniApplication) do
  begin
    OnSelectedItem := SetReferente;
    ShowModal;
  end;

end;

procedure TFContrattoEdit.btnBabbandonaClick(Sender: TObject);
begin
  AbbandonaOggAssicurato;
end;

procedure TFContrattoEdit.btnBconfermaClick(Sender: TObject);
var
  ind: Integer;
  tt: string;
begin
  tt := valida_dati_oggetti;
  { TODO : Definire messaggio in UCodiciErrorePolizza }
  if tt>'' then ShowMessage('Controllare: ' + tt);
  if tt>'' then
  else begin
    if dsEditOggAssicurato.State in [dsInsert, dsEdit] then
      dsEditOggAssicurato.DataSet.Post;
    ind := dsEditOggAssicurato.DataSet.FieldByName('cod_assicurati').AsInteger;
    EnableAppendici(not dsAppendice.DataSet.IsEmpty);
    EnableOggettoInGaranzia(not dsEditOggAssicurato.DataSet.IsEmpty, not dsAssicurati.DataSet.IsEmpty);

    dsAssicurati.DataSet.Close;
    dsAssicurati.DataSet.Open;
    grdAssicurati.Refresh;
    dsAssicurati.DataSet.Locate('cod_assicurati', ind, []);

    rgGrpTipoOggAssicurato.ItemIndex := getPageIxFromTipoAssicurato;
    rgGrpTipoOggAssicuratoClick(rgGrpTipoOggAssicurato);

    SetControlReadOnly(pcOggettoAssicurato.Pages[getPageIxFromTipoAssicurato], true);
    imposta_tasti_ogg_assic(true);
  end;
end;

procedure TFContrattoEdit.cbbTipoPolizzaChange(Sender: TObject);
begin
  inherited;
  // imposta il valore delle tasse !
  DSoEdit.DataSet.FieldByName('PTASSE').AsCurrency := UniMainModule.DMdatiAge.DStipoPolizzeAll.DataSet.FieldByName
    ('PERC_TASSE').AsCurrency;
  // dbedtptasse.Value:=
  dbedtptasse.Refresh;
end;

procedure TFContrattoEdit.EnableAppendici(Enable: boolean);
begin
  actRistampaAppendice.Enabled := true;
  actDelAppendice.Enabled      := Enable;

end;

procedure TFContrattoEdit.EnableOggettoInGaranzia(Enable: boolean; EnableTogli: boolean);
begin
  actDelOggGaranzia.Enabled    := EnableTogli;
  actGeneraAppendice.Enabled   := EnableTogli;
  pcOggettoAssicurato.Enabled  := Enable;
  pnlTipoOggAssicurato.Enabled := Enable;

end;

procedure TFContrattoEdit.grdAssicuratiSelectionChange(Sender: TObject);
begin
  OggAssicuratoDisableEdit(true);
  // aggiorna la posizione del tab corrente !!!
  sincronizza;
end;

procedure TFContrattoEdit.grdTitScadenzeDblClick(Sender: TObject);
var mess: string;
begin
  // mostra i dati di pagamento, se presenti
  mess:= 'Dati tracciamento incasso' + Chr(13)+
  Chr(13) +
  'Dt. operazione: ' + grdTitScadenze.DataSource.DataSet.FieldByName('data_operazione').AsString + Chr(13) +
  'Importo: ' + grdTitScadenze.DataSource.DataSet.FieldByName('importo').AsString + Chr(13) +
  'Mezzo pag.: ' + grdTitScadenze.DataSource.DataSet.FieldByName('descriz_pag').AsString + Chr(13) +
  'Descriz.: ' + grdTitScadenze.DataSource.DataSet.FieldByName('descrizione').AsString + Chr(13) +
  'Banca: ' + grdTitScadenze.DataSource.DataSet.FieldByName('banca').AsString + Chr(13) +
  'eseguito da: ' + grdTitScadenze.DataSource.DataSet.FieldByName('emesso_da').AsString + Chr(13) +
  'Chiusura sospeso: ' + grdTitScadenze.DataSource.DataSet.FieldByName('descrizione_chiusura_sospeso').AsString + Chr(13) +
  'Estremi: ' + grdTitScadenze.DataSource.DataSet.FieldByName('estremi1').AsString + Chr(13) +
  'Estremi: ' + grdTitScadenze.DataSource.DataSet.FieldByName('estremi2').AsString + Chr(13) +
  'Estremi: ' + grdTitScadenze.DataSource.DataSet.FieldByName('estremi3').AsString;

  showmessage(mess);
end;

procedure TFContrattoEdit.imposta_tasti_ogg_assic(attiva: boolean);
begin
   btnInserisciAss.Enabled:=attiva;
   btnDelOggGaranzia.Enabled:=attiva;
   btnGeneraAppendice.Enabled:=attiva;
end;

procedure TFContrattoEdit.OggAssicuratoDisableEdit(Disable: boolean);

begin
  rgGrpTipoOggAssicurato.ReadOnly := Disable;
  dtpckDataIngresso.ReadOnly      := Disable;
  dtpckDataIngresso.Color         := IfThen(Disable, clYellow, clWindow);

  pnlButtonsEditAss.Visible := not Disable;
  SetControlReadOnly(pcOggettoAssicurato.Pages[getPageIxFromTipoAssicurato], Disable);
  // pnlTipoOggAssicurato.Enabled := not Disable;
  // pcOggettoAssicurato.ActivePage.Enabled  := not Disable;
  pnlButtonsEditAss.Visible := dsEditOggAssicurato.DataSet.State = dsInsert;
  if not Disable then
    dtpckDataIngresso.SetFocus;
end;

function TFContrattoEdit.getPageIxFromTipoAssicurato: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i  := Low(ArOggettoAssicurato) to High(ArOggettoAssicurato) do
    if ArOggettoAssicurato[i] = UniMainModule.DMdatiAgePolizze.QAssicuratiPolizza.FieldByName('TIPO').AsString then
    begin
      Result := i;
      Break;
    end;
end;

procedure TFContrattoEdit.AbbandonaOggAssicurato;
// var
// BResult: Boolean;
begin
  CanClose := true;
  if dsEditOggAssicurato.State in [dsInsert, dsEdit] then
  { TODO : Definire messaggio in UCodiciErrorePolizza }
    MessageDlg('Confermi di non salvare l''ogetto assicurato? ', mtConfirmation, mbYesNo,
      procedure(Sender: TComponent; Res: Integer)
      begin
        case Res of
          mrYes:
            begin
              // UniMainModule.DMdatiAgePolizze.ClearAssicuratiPolizzaEdit;
              dsEditOggAssicurato.DataSet.Cancel;
              rgGrpTipoOggAssicurato.ItemIndex := getPageIxFromTipoAssicurato;
              rgGrpTipoOggAssicuratoClick(rgGrpTipoOggAssicurato);
              imposta_tasti_ogg_assic(true);
            end
        else
          CanClose := False;
        end;
        EnableAppendici(not dsAppendice.DataSet.IsEmpty);
        EnableOggettoInGaranzia(not dsEditOggAssicurato.DataSet.IsEmpty, not dsAssicurati.DataSet.IsEmpty);

      end);
end;

procedure TFContrattoEdit.rgGrpStatoTitoliClick(Sender: TObject);
begin
  UniMainModule.DMdatiAgePolizze.ApriTitoliPolizza(UniMainModule.DMdatiAgePolizze.CurrentCodPolizza,
    rgGrpStatoTitoli.ItemIndex);
end;

procedure TFContrattoEdit.rgGrpTipoMovimentoClick(Sender: TObject);

  procedure enableActionsOggInGaranzia(Enable: boolean);
  begin
    actInsOggGaranzia.Enabled  := Enable;
    actModOggGaranzia.Enabled  := Enable;
    actDelOggGaranzia.Enabled  := Enable;
    actGeneraAppendice.Enabled := Enable;
  end;

begin
  case rgGrpTipoMovimento.ItemIndex of
    0:
      dsAssicurati.DataSet.Filtered := False;
    1:
      begin
        dsAssicurati.DataSet.Filter   := 'movim = ''E''';
        dsAssicurati.DataSet.Filtered := true;
      end;
    2:
      begin
        dsAssicurati.DataSet.Filter   := 'movim = ''U''';
        dsAssicurati.DataSet.Filtered := true;
      end;
  end;
  enableActionsOggInGaranzia(rgGrpTipoMovimento.ItemIndex = 1);
  dsAssicurati.DataSet.Refresh;
end;

procedure TFContrattoEdit.rgGrpTipoOggAssicuratoClick(Sender: TObject);
var
  i: Integer;
begin
  if dsEditOggAssicurato.DataSet.State = dsInsert then
  begin
    dsEditOggAssicurato.DataSet.FieldByName('TIPO').AsString := ArOggettoAssicurato[rgGrpTipoOggAssicurato.ItemIndex];
    UniMainModule.DMdatiAgePolizze.ClearAssicuratiPolizzaEdit;
  end;

  for i := 0 to pcOggettoAssicurato.PageCount - 1 do
  begin
    if i = rgGrpTipoOggAssicurato.ItemIndex then
    begin
      pcOggettoAssicurato.ActivePageIndex := i;
      OggAssicuratoDisableEdit(dsEditOggAssicurato.DataSet.State <> dsInsert);
      // lbl1.Caption := pcOggettoAssicurato.Pages[i].Name;
    end;
    pcOggettoAssicurato.Pages[i].TabVisible := i = rgGrpTipoOggAssicurato.ItemIndex;
    pcOggettoAssicurato.Pages[i].Update;
  end;
end;

procedure TFContrattoEdit.SetCollaboratore(ASelectedItem: Integer;

const ASelectedDescription: string);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('FATTA_DA').AsInteger := ASelectedItem;
  unedtReferente.Text                               := ASelectedDescription;

end;

procedure TFContrattoEdit.SetContraente(ASelectedItem: Integer;

const ASelectedDescription: string);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('CONTRAENTE').AsString := ASelectedDescription;
  DSoEdit.DataSet.FieldByName('cod_cli').AsInteger := ASelectedItem;

end;

procedure TFContrattoEdit.SetControlReadOnly(ATabSheet: TUniTabSheet; ReadOnlyState: boolean);
var
  i: Integer;

begin
  for i := 0 to ATabSheet.ControlCount - 1 do
  begin
    TUniFormControl(ATabSheet.Controls[i]).ReadOnly := ReadOnlyState;
    TUniFormControl(ATabSheet.Controls[i]).Color    := IfThen(ReadOnlyState, clYellow, clWindow);
  end;

end;

procedure TFContrattoEdit.SetReferente(ASelectedItem: Integer;

const ASelectedDescription: string);
begin
  if not(DSoEdit.DataSet.State in [dsEdit, dsInsert]) then
    DSoEdit.DataSet.Edit;
  DSoEdit.DataSet.FieldByName('RIF_REFERENTE').AsInteger := ASelectedItem;
  unedtReferente.Text                                    := ASelectedDescription;
end;

procedure TFContrattoEdit.sincronizza;
var
  i: Integer;
begin
  // chiamata quando si scorre l'elenco degli oggetti assicurati per visualizzare il tab giusto !!!
  for i := Low(ArOggettoAssicurato) to High(ArOggettoAssicurato) do
    if ArOggettoAssicurato[i] = UniMainModule.DMdatiAgePolizze.QAssicuratiPolizza.FieldByName('TIPO').AsString then
    begin
      pcOggettoAssicurato.ActivePageIndex := i;
      rgGrpTipoOggAssicurato.ItemIndex := i;
      Break;
    end;
end;

procedure TFContrattoEdit.UniFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   actAnnullaExecute(Sender);
end;


procedure TFContrattoEdit.UniFormCreate(Sender: TObject);
var codp: integer;
begin
  inherited;
  if UniMainModule.operazione = 'INS' then
  begin
     // MB 23122020
     UniMainModule.DMdatiAgePolizze.QPolizzaEdit.Cancel;

     UniMainModule.DMdatiAgePolizze.set_post_ins_polizza(False);
     codp:= UniMainModule.DMdatiAgePolizze.ins_polizza_new;
     UniMainModule.DMdatiAgePolizze.set_post_ins_polizza(true);

     //UniMainModule.DMdatiAgePolizze.QPolizzaEdit.close;
     //UniMainModule.DMdatiAgePolizze.QPolizzaEdit.ParamByName('cod_polizza').AsInteger:=codp;
     //UniMainModule.DMdatiAgePolizze.QPolizzaEdit.Open;
     UniMainModule.DMdatiAgePolizze.PosizionaQuery(codp);

  end;

  cbbCompagnia.listSource.DataSet.Open;
  cbbRamo.listSource.DataSet.Open;
  cbbTipoPolizza.listSource.DataSet.Open;
  cbbAppoggiata.listSource.DataSet.Open;
  cbbConvenzione.listSource.DataSet.Open;
  cbbCausaMorte.listSource.DataSet.Open;
  cbbTipoVeicolo.listSource.DataSet.Open;
  unedtReferente.Text := UniMainModule.DMdatiAge.getNomePromoter(DSoEdit.DataSet.FieldByName('RIF_REFERENTE')
    .AsInteger);
  UniMainModule.DMdatiAgePolizze.ApriGaranziePolizza(UniMainModule.DMdatiAgePolizze.CurrentCodPolizza);
  UniMainModule.DMdatiAgePolizze.ApriOggAssicuratiPolizza(UniMainModule.DMdatiAgePolizze.CurrentCodPolizza);
  UniMainModule.DMdatiAgePolizze.ApriTitoliPolizza(UniMainModule.DMdatiAgePolizze.CurrentCodPolizza,
    rgGrpStatoTitoli.ItemIndex);
  UniMainModule.DMdatiAgePolizze.ApriAppendiciPolizza(UniMainModule.DMdatiAgePolizze.CurrentCodPolizza);
  UniMainModule.DMdatiAgePolizze.ApriOggAssicuratoPolizza(UniMainModule.DMdatiAgePolizze.CurrentCodPolizza);
  pcContrattoEdit.ActivePage     := pgEdit;
  pcOggettoAssicurato.ActivePage := pgPatente;
  EnableAppendici(not dsAppendice.DataSet.IsEmpty);
  EnableOggettoInGaranzia(not dsEditOggAssicurato.DataSet.IsEmpty, not dsAssicurati.DataSet.IsEmpty);
  rgGrpTipoMovimentoClick(Sender);
  Self.Caption := 'Editazione dati polizza n.:' + DSoEdit.DataSet.FieldByName('n_polizza').AsString + ' ' +
    DSoEdit.DataSet.FieldByName('contraente').AsString;

  if UniMainModule.operazione = 'VIS' then
  begin
    actConferma.Enabled := False;
    SetAllControlsReadOnly(pcContrattoEdit);

    btnRistampaAppendice.Enabled := true;
    pgTitoliScadenze.Enabled     := true;
    UniSimplePanel1.Enabled      := true;
    rgGrpStatoTitoli.Enabled     := true;
    rgGrpStatoTitoli.ReadOnly    := False;

    rgGrpTipoMovimento.ReadOnly := False; // questo deve essere attivo anche in visualizzazione !!
  end;
  tipo_prec:='P';
end;

function TFContrattoEdit.valida_dati_oggetti: string;
begin
  Result := '';
  // esegue le validazioni sui dati degli oggetti caricati

  if (dtpckDataIngresso.DateTime)<EncodeDate(1980,01,01) then
      Result := 'inserire la data di ingresso.'
  else
  if rgGrpTipoOggAssicurato.ItemIndex = 0 then
  begin
    // patente
    // nome e categoria patente obbligatori
    if not((dbedtDenominazione.Text > '') and (cbbCategoriaPat.Text > '')) then
  { TODO : Definire messaggio in UCodiciErrorePolizza }
      Result := 'inserire almeno nome e categoria patente.';

  end
  else
    if rgGrpTipoOggAssicurato.ItemIndex = 1 then
    begin
      // veicolo
      // tipo veicolo, targa obbligatori
      if not((cbbTipoVeicolo.Text > '') and (dbedtTarga.Text > '')) then
  { TODO : Definire messaggio in UCodiciErrorePolizza }
        Result := 'inserire almeno tipo veicolo e targa.';

    end
    else
      if rgGrpTipoOggAssicurato.ItemIndex = 2 then
      begin
        // persona
        // nome e cf
        if not((dbedtPersNome.Text > '') and (dbedtPersCodFisc.Text > '')) then
  { TODO : Definire messaggio in UCodiciErrorePolizza }
          Result := 'inserire almeno nome e codice fiscale.';

      end
      else
        if rgGrpTipoOggAssicurato.ItemIndex = 3 then
        begin
          // contratto
          // numero polizza, compagnia, garanzie
          if not((dbedtConNPolizza.Text > '') and (dbedtConCompagnia.Text > '') and (dbedtConImporto.Value > 0) and
            (dbedtConGaranzia.Text > '')) then
  { TODO : Definire messaggio in UCodiciErrorePolizza }
            Result := 'inserire almeno numero polizza, compagnia, importo e garanzie.';

        end
        else
          if rgGrpTipoOggAssicurato.ItemIndex = 4 then
          begin
            // altro
            if not(dbedtAltDescrizione.Text > '') then
  { TODO : Definire messaggio in UCodiciErrorePolizza }
              Result := 'inserire almeno la descrizione.';
          end;

end;

initialization

RegisterClass(TFContrattoEdit);

end.
