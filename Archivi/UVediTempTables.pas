unit UVediTempTables;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, uniGUIApplication,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  uniGUIClasses, uniGUIForm, Data.DB, uniDBNavigator, uniGUIBaseClasses, uniBasicGrid, uniDBGrid, uniPanel,
  uniPageControl;

type
  TFmVediTempTables = class(TUniForm)
    dsPolizza: TDataSource;
    pcTempTables: TUniPageControl;
    pgPolizza: TUniTabSheet;
    grdPloizza: TUniDBGrid;
    undbnvgtrPolizza: TUniDBNavigator;
    dsAssicurati: TDataSource;
    dsGaranzie: TDataSource;
    pgAssicurati: TUniTabSheet;
    pgGaranzie: TUniTabSheet;
    grdAssicurati: TUniDBGrid;
    undbnvgtrAssicurati: TUniDBNavigator;
    grdGaranzie: TUniDBGrid;
    undbnvgtrGaranzie: TUniDBNavigator;
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure ShowGaranzie(UniApplication: TUniGUIApplication;
      TblPolizza, TblAssicurato, TblGaranzie: TFDMemTable);
  end;

implementation

{$R *.dfm}
{ TUniForm1 }

class procedure TFmVediTempTables.ShowGaranzie(UniApplication: TUniGUIApplication;
  TblPolizza, TblAssicurato, TblGaranzie: TFDMemTable);
begin
  with TFmVediTempTables.Create(UniApplication) do
  begin
    dsPolizza.DataSet    := TblPolizza;
    dsAssicurati.DataSet := TblAssicurato;
    dsGaranzie.DataSet   := TblGaranzie;
    TblPolizza.Open;
    TblGaranzie.Open;
    TblGaranzie.Open;
    ShowModal;
  end;
end;

procedure TFmVediTempTables.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFmVediTempTables.UniFormCreate(Sender: TObject);
begin
  pcTempTables.ActivePage := pgPolizza;
end;

end.
