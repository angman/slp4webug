inherited FUtenti: TFUtenti
  Caption = 'Gestione Utenti'
  ExplicitWidth = 320
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlGlobal: TUniSimplePanel
    inherited pnlGrid: TUniContainerPanel
      inherited grdElenco: TUniDBGrid
        Columns = <
          item
            FieldName = 'NOME'
            Filtering.Enabled = True
            Filtering.Editor = unedtNome
            Title.Caption = 'Nome'
            Width = 200
            Sortable = True
          end
          item
            FieldName = 'SIGLA'
            Filtering.Enabled = True
            Filtering.Editor = unedtSigla
            Title.Caption = 'Sigla'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'LIVELLO'
            Filtering.Enabled = True
            Filtering.Editor = unedtLivello
            Title.Caption = 'Livello'
            Width = 64
            Sortable = True
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 232
        Top = 62
        ExplicitLeft = 232
        ExplicitTop = 62
        object unedtNome: TUniEdit
          Left = 24
          Top = 32
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTNOME'
          TabOrder = 1
        end
        object unedtSigla: TUniEdit
          Left = 24
          Top = 60
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTREFERENTE'
          TabOrder = 2
        end
        object unedtLivello: TUniEdit
          Left = 24
          Top = 88
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTCITTA'
          TabOrder = 3
        end
      end
    end
  end
end
