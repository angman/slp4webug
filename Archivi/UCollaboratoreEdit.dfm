inherited FCollaboratoreEdit: TFCollaboratoreEdit
  ClientHeight = 520
  ClientWidth = 537
  Caption = 'Editazione dati Intermediari / Collaboratori / Dipendenti'
  ExplicitWidth = 553
  ExplicitHeight = 559
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 537
    ExplicitWidth = 537
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 537
    ExplicitWidth = 537
  end
  object pnlEdit: TUniSimplePanel [2]
    Left = 0
    Top = 69
    Width = 537
    Height = 451
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
    object pcCollaboratore: TUniPageControl
      Left = 0
      Top = 0
      Width = 537
      Height = 451
      Hint = ''
      ActivePage = pgAnagrafica
      Align = alClient
      TabOrder = 1
      object pgAnagrafica: TUniTabSheet
        Hint = ''
        Caption = 'Anagrafica'
        ParentFont = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 256
        ExplicitHeight = 128
        object dbedtNome: TUniDBEdit
          Left = 41
          Top = 16
          Width = 424
          Height = 22
          Hint = ''
          DataField = 'NOME'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 0
          FieldLabel = 'Rag. Soc./Nome'
        end
        object dbedtSigla: TUniDBEdit
          Left = 41
          Top = 44
          Width = 145
          Height = 22
          Hint = ''
          DataField = 'SIGLA'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 1
          FieldLabel = 'Sigla'
        end
        object cbbTipo: TUniDBComboBox
          Left = 192
          Top = 44
          Width = 273
          Hint = ''
          DataField = 'TIPO'
          DataSource = dsoEdit
          Items.Strings = (
            'P= PRODUTTORE ISCRITTO RUI'
            'S= SUB-AGENTE'
            'A= AGENTE / BROKER'
            'O= PRODUTTORE OCCASIONALE ISCRITTO RUI'
            #39'I=IMPIEGATA/O / COLLAB. INTERNO NON ISCRITTO RUI'
            'Y= IMPIEGATA/O ISCRITTO RUI'
            'X= SEGNALATORE'
            'Z= COLLABORAZIONE A-AB'
            '')
          ParentFont = False
          TabOrder = 2
          FieldLabel = 'Tipo Collaboratore'
          FieldLabelWidth = 120
          ClearButton = True
          MatchFieldWidth = False
          IconItems = <>
        end
        object dbedtDescrizione: TUniDBEdit
          Left = 41
          Top = 72
          Width = 424
          Height = 22
          Hint = ''
          DataField = 'DESCRIZ'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 3
          FieldLabel = 'Descrizione'
        end
        object lblNotaSedeOperativa: TUniLabel
          Left = 17
          Top = 108
          Width = 495
          Height = 14
          Hint = ''
          Caption = 
            'Dati della sede operativa dell'#39'intermediario / collaboratore (se' +
            ' diversa da quella dell'#39'agenzia)'
          ParentFont = False
          Font.Color = clBlue
          Font.Height = -12
          TabOrder = 4
        end
        object dbedtIndirizzo: TUniDBEdit
          Left = 41
          Top = 137
          Width = 424
          Height = 22
          Hint = ''
          DataField = 'INDIRIZZO'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 5
          FieldLabel = 'Indirizzo'
        end
        object dbedtCap1: TUniDBEdit
          Left = 305
          Top = 165
          Width = 121
          Height = 22
          Hint = ''
          DataField = 'CAP'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 6
          FieldLabel = 'Cap'
          FieldLabelWidth = 30
        end
        object dbedtCitta1: TUniDBEdit
          Left = 41
          Top = 165
          Width = 258
          Height = 22
          Hint = ''
          DataField = 'CITTA'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 7
          FieldLabel = 'Citt'#224
        end
        object dbedtProvincia1: TUniDBEdit
          Left = 432
          Top = 165
          Width = 94
          Height = 22
          Hint = ''
          DataField = 'PROV'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 8
          FieldLabel = 'Provincia'
          FieldLabelWidth = 50
        end
        object dbedtCodFisc: TUniDBEdit
          Left = 41
          Top = 193
          Width = 258
          Height = 22
          Hint = ''
          DataField = 'COD_FISC'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 9
          FieldLabel = 'Codice fiscale'
        end
        object dbedtPIva: TUniDBNumberEdit
          Left = 41
          Top = 221
          Width = 258
          Height = 22
          Hint = ''
          DataField = 'P_IVA'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 10
          FieldLabel = 'P.Iva'
          DecimalPrecision = 0
          DecimalSeparator = ','
        end
        object dbedtTelefono: TUniDBEdit
          Left = 41
          Top = 249
          Width = 258
          Height = 22
          Hint = ''
          DataField = 'TELEFONO'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 11
          FieldLabel = 'Telefono'
        end
        object dbedtMail: TUniDBEdit
          Left = 41
          Top = 277
          Width = 258
          Height = 22
          Hint = ''
          DataField = 'EMAIL'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 12
          FieldLabel = 'Mail'
        end
        object dbedtPec: TUniDBEdit
          Left = 41
          Top = 305
          Width = 258
          Height = 22
          Hint = ''
          DataField = 'PEC'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 13
          FieldLabel = 'Pec'
        end
        object dbedtNRui: TUniDBEdit
          Left = 41
          Top = 333
          Width = 176
          Height = 22
          Hint = ''
          DataField = 'N_RUI'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 14
          FieldLabel = 'N. Rui'
        end
        object dtpckDataIscrizRui: TUniDBDateTimePicker
          Left = 272
          Top = 333
          Width = 193
          Hint = ''
          DataField = 'DATA_ISCRIZ_RUI'
          DataSource = dsoEdit
          DateTime = 43749.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 15
          ParentFont = False
          FieldLabel = 'Data Iscr. Rui'
        end
        object dbedtRefrerente: TUniDBEdit
          Left = 41
          Top = 361
          Width = 424
          Height = 22
          Hint = ''
          DataField = 'REFERENTE'
          DataSource = dsoEdit
          CharCase = ecUpperCase
          ParentFont = False
          TabOrder = 16
          FieldLabel = 'Referente'
        end
        object cbbPromoter: TUniDBLookupComboBox
          Left = 41
          Top = 389
          Width = 424
          Hint = ''
          ListField = 'SIGLA;NOME;TIPO'
          ListSource = dsLinkProdutt
          KeyField = 'COD_PRODUTTORE'
          ListFieldIndex = 0
          ClearButton = True
          DataField = 'LINK_PRODUTT'
          DataSource = dsoEdit
          TabOrder = 17
          Color = clWindow
          FieldLabel = 'Lavora per:'
        end
      end
      object pgNote: TUniTabSheet
        Hint = ''
        Caption = 'Note'
        object undbmNota: TUniDBMemo
          Left = 0
          Top = 0
          Width = 529
          Height = 423
          Hint = ''
          DataField = 'NOTE'
          DataSource = dsoEdit
          Align = alClient
          TabOrder = 0
        end
      end
      object pgNormeContabili: TUniTabSheet
        Hint = ''
        Caption = 'Norme Contabili'
        DesignSize = (
          529
          423)
        object dbedtPagamenti: TUniDBEdit
          Left = 17
          Top = 32
          Width = 495
          Height = 22
          Hint = ''
          DataField = 'PAGAMENTI'
          DataSource = dsoEdit
          TabOrder = 0
          FieldLabel = 'Versamenti'
        end
        object dbedtRendiconti: TUniDBEdit
          Left = 17
          Top = 72
          Width = 495
          Height = 22
          Hint = ''
          DataField = 'INVIO_RENDICONTI'
          DataSource = dsoEdit
          TabOrder = 1
          FieldLabel = 'Rendiconti'
        end
        object dbedtPercProvvAg: TUniDBNumberEdit
          Left = 17
          Top = 109
          Width = 495
          Height = 22
          Hint = ''
          DataField = 'PERC_PROVV_AGE'
          DataSource = dsoEdit
          Alignment = taRightJustify
          TabOrder = 2
          FieldLabel = 
            'Percentuale delle provvigioni di agenzia spettanti al collaborat' +
            'ore:'
          FieldLabelWidth = 400
          DecimalPrecision = 0
          DecimalSeparator = ','
        end
        object dbedtPercRitAcc: TUniDBNumberEdit
          Left = 17
          Top = 144
          Width = 495
          Height = 22
          Hint = ''
          Alignment = taRightJustify
          TabOrder = 3
          FieldLabel = 
            'Percentuale da applicare per il calcolo della ritenuta di accont' +
            'o:'
          FieldLabelWidth = 400
          DecimalSeparator = ','
        end
        object undbmNoteContabili: TUniDBMemo
          Left = 17
          Top = 182
          Width = 495
          Height = 227
          Hint = ''
          DataField = 'NOTE_CONTABILI'
          DataSource = dsoEdit
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 4
          FieldLabel = 'Note contabili'
          FieldLabelAlign = laTop
        end
      end
      object pgAllegati: TUniTabSheet
        Hint = ''
        Caption = 'Allegati'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 256
        ExplicitHeight = 128
        object pnlButtons: TUniSimplePanel
          Left = 0
          Top = 384
          Width = 529
          Height = 39
          Hint = ''
          ParentColor = False
          Align = alBottom
          TabOrder = 0
          object btnVisualizzaAllegato: TUniSpeedButton
            Left = 295
            Top = 5
            Width = 106
            Height = 30
            Action = actVisualizzaAllegato
            ParentFont = False
            ParentColor = False
            Color = clWindow
            LayoutConfig.Flex = 1
            TabOrder = 1
          end
          object UniDBNavigator1: TUniDBNavigator
            Left = 3
            Top = 6
            Width = 241
            Height = 25
            Hint = ''
            DataSource = dsAllegati
            TabOrder = 2
          end
        end
        object grdAllegati: TUniDBGrid
          Left = 0
          Top = 0
          Width = 529
          Height = 384
          Hint = ''
          DataSource = dsAllegati
          LoadMask.Message = 'Loading data...'
          Align = alClient
          TabOrder = 1
          Columns = <
            item
              FieldName = 'descrizione'
              Title.Caption = 'descrizione'
              Width = 280
              Editor = UniEditDescrAllegato
            end
            item
              FieldName = 'tipo'
              Filtering.Enabled = True
              Filtering.Editor = cbbTipoAlegato
              Title.Caption = 'tipo'
              Width = 64
              Editor = cbbTipoAlegato
            end
            item
              FieldName = 'dt_inzio_validita'
              Title.Caption = 'inzio validit'#224
              Width = 90
              Editor = undtmpckrDtInizioValidita
            end
            item
              FieldName = 'dt_ins'
              Title.Caption = 'Dt inserimento'
              Width = 80
            end>
        end
        object UniHiddenPanel1: TUniHiddenPanel
          Left = 108
          Top = 88
          Width = 205
          Height = 256
          Hint = ''
          Visible = True
          object UniEditDescrAllegato: TUniEdit
            Left = 32
            Top = 21
            Width = 145
            Hint = ''
            CharCase = ecUpperCase
            Text = 'UNIEDITDESCRALLEGATO'
            TabOrder = 1
          end
          object undtmpckrDtInizioValidita: TUniDateTimePicker
            Left = 32
            Top = 88
            Width = 145
            Hint = ''
            DateTime = 43941.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 2
          end
          object cbbTipoAlegato: TUniDBLookupComboBox
            Left = 32
            Top = 56
            Width = 145
            Hint = ''
            ListField = 'descrizione'
            ListSource = dsTipoAllegato
            KeyField = 'cod_tipo'
            ListFieldIndex = 0
            DataField = 'tipo'
            DataSource = dsAllegati
            TabOrder = 3
            Color = clWindow
          end
        end
        object btnCaricaAllegato: TUniFileUploadButton
          Left = 407
          Top = 389
          Width = 106
          Height = 30
          Hint = ''
          Caption = 'Carica Allegato'
          Filter = '*.pdf'
          MaxAllowedSize = 4000000
          Messages.Uploading = 'Sto caricando ...'
          Messages.PleaseWait = 'Attendere prego ...'
          Messages.UploadError = 'Errore di caricamento'
          Messages.UploadTimeout = 'Timeout occurred...'
          Messages.MaxSizeError = 'File di dimensioni maggiori del consentito'
          Messages.MaxFilesError = 'You can upload maximum %d files.'
          EraseCacheAfterCompleted = True
          ShowUploadingMsg = False
          OnCompleted = btnCaricaAllegatoCompleted
        end
      end
    end
  end
  inherited actlstEditOperation: TActionList
    Left = 424
    Top = 41
    object actSelezioneProduttore: TAction
      Category = 'EditOperation'
      OnExecute = actSelezioneProduttoreExecute
    end
    object actClearProduttore: TAction
      Category = 'EditOperation'
      OnExecute = actClearProduttoreExecute
    end
    object actVisualizzaAllegato: TAction
      Category = 'OpAllegato'
      Caption = 'Visualizza Allegato'
      OnExecute = actVisualizzaAllegatoExecute
    end
  end
  inherited dsoEdit: TDataSource
    Left = 328
  end
  object fdmtblTipoCollaboratore: TFDMemTable [5]
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 476
    Top = 133
  end
  object dsAllegati: TDataSource
    DataSet = DMAgeCollaboratori.QAllegatiCollaboratore
    OnDataChange = dsAllegatiDataChange
    Left = 340
    Top = 437
  end
  object dsTipoAllegato: TDataSource
    DataSet = DMAgeCollaboratori.QTipoAllegato
    Left = 412
    Top = 437
  end
  object dsLinkProdutt: TDataSource
    DataSet = DMAgeCollaboratori.QLinkProduttori
    Left = 484
    Top = 477
  end
end
