unit UStampePDFViewer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, uniGUIApplication,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniURLFrame;

type
  TFPDFViewer = class(TUniForm)
    unpdfrmDoc: TUniPDFFrame;
    procedure unpdfrmDocFrameLoaded(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FFileNameFullPath: string;
    FDowloadEnabled: Boolean;
    FPrintEnabled: Boolean;
  public
    { Public declarations }
    class procedure ShowReport(UniApplication: TUniGUIApplication; const AFilename, PDFUrl, PDFFolder: string;
      DownLoadEnabled: Boolean = false; PrintEnabled: Boolean = false);
  end;

implementation

uses
  System.IOUtils, System.StrUtils;

{$R *.dfm}

class procedure TFPDFViewer.ShowReport(UniApplication: TUniGUIApplication; const AFilename, PDFUrl, PDFFolder: string;
  DownLoadEnabled: Boolean; PrintEnabled: Boolean);
begin
  with TFPDFViewer.Create(UniApplication) do
  begin
    Caption           := AFilename;
    FFileNameFullPath := TPath.Combine(PDFFolder, AFilename);
    unpdfrmDoc.PDFUrl := PDFUrl + AFilename;
    FDowloadEnabled   := DownLoadEnabled;
    FPrintEnabled     := PrintEnabled;
    ShowModal;
  end;
end;

procedure TFPDFViewer.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  TFile.delete(FFileNameFullPath);
end;

procedure TFPDFViewer.unpdfrmDocFrameLoaded(Sender: TObject);
begin
  // Per disabilitare i bottoni del viewer pdf
  if unpdfrmDoc.PDFUrl > '' then
  begin
    if not FDowloadEnabled then
    begin
      UniSession.AddJS(unpdfrmDoc.JSName + '.iframe.contentWindow.document.' +
        'getElementById ("download").style.display = "none"');
      UniSession.AddJS(unpdfrmDoc.JSName + '.iframe.contentWindow.document.' +
        'getElementById ("secondaryDownload").style.display = "none"');
    end;

    if not FPrintEnabled then
    begin
      UniSession.AddJS(unpdfrmDoc.JSName + '.iframe.contentWindow.document.' +
        'getElementById ("print").style.display = "none"');
      UniSession.AddJS(unpdfrmDoc.JSName + '.iframe.contentWindow.document.' +
        'getElementById ("secondaryPrint").style.display = "none"');
    end;

    UniSession.AddJS(unpdfrmDoc.JSName + '.iframe.contentWindow.document.' +
      'getElementById ("openFile").style.display = "none"');
    UniSession.AddJS(unpdfrmDoc.JSName + '.iframe.contentWindow.document.' +
      'getElementById ("secondaryOpenFile").style.display = "none"');

  end;

end;

end.
