unit UFogliCassa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses,
  uniDateTimePicker, uniEdit, UdmdatiAgeFogliCassa, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox;

type
  TFFogliCassa = class(TFMasterElenco)
    actStampaFoglioCassa: TAction;
    unedtSiglaCompagnia: TUniEdit;
    unmbrdtFoglioCassa: TUniNumberEdit;
    dsAnno: TDataSource;
    cbbAnno: TUniDBLookupComboBox;
    UniSpeedButton1: TUniSpeedButton;
    actAnteprima: TAction;
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn; const Value: Variant);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure actStampaFoglioCassaExecute(Sender: TObject);
    procedure actAnteprimaExecute(Sender: TObject);
  private
    { Private declarations }
    PDFFolderPath: string;
    PDFUrl: string;
  public
    { Public declarations }
  end;

function FFogliCassa: TFFogliCassa;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UQueryElencoIntf, UDMDatiReport, ServerModule, UStampePDFViewer, System.IOUtils,
  System.StrUtils;

function FFogliCassa: TFFogliCassa;
begin
  Result := TFFogliCassa(UniMainModule.GetFormInstance(TFFogliCassa));
end;

procedure TFFogliCassa.actAnteprimaExecute(Sender: TObject);
var
  PDFFilename: string;
begin
  // anteprima del foglio cassa
  PDFFilename := (UniMainModule.DMDatiReport.StampaFoglioCassa(grdElenco.DataSource.DataSet.FieldByName('CODICE_FOGLIO')
    .AsInteger));
  if (PDFFilename <> '') and FileExists(PDFFilename) then
  begin
    PDFFilename := ExtractFileName(PDFFilename);
    TFPDFViewer.ShowReport(UniApplication, PDFFilename, PDFUrl, PDFFolderPath, true, true);
  end;
end;

procedure TFFogliCassa.actStampaFoglioCassaExecute(Sender: TObject);
var
  PDFFilename: string;
begin
  PDFFilename := UniMainModule.DMDatiReport.StampaFoglioCassa(grdElenco.DataSource.DataSet.FieldByName('CODICE_FOGLIO')
    .AsInteger);
  if (PDFFilename <> '') and FileExists(PDFFilename) then
    UniSession.SendFile(PDFFilename);
end;

procedure TFFogliCassa.grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn; const Value: Variant);
begin

  dsGrid.DataSet.DisableControls;
  try
    dsGrid.DataSet.Filtered      := False;
    dsGrid.DataSet.FilterOptions := [foCaseInsensitive];
    if Column.FieldName = 'ANNO' then
      self.DMMaster.EseguiQuery('NUMERO_FC', 0, Value)
    else
      inherited;
  finally
    dsGrid.DataSet.EnableControls
  end;

end;

procedure TFFogliCassa.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  dsGrid.DataSet.Close;
  dsAnno.DataSet.Close;
end;

procedure TFFogliCassa.UniFormCreate(Sender: TObject);
begin
  UniMainModule.archivio := 'FCA';

  inherited;
  PDFUrl           := UniServerModule.FilesFolderURL + 'PDF/';
  PDFFolderPath    := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  btnInsert.Action := actStampaFoglioCassa;
  self.DMMaster    := (UniMainModule.DMDatiAgeFogliCassa as IQueryElenco);
  dsGrid.DataSet   := UniMainModule.DMDatiAgeFogliCassa.QfogliCassa;
  self.DMMaster.EseguiQuery('NUMERO_FC');
  dsAnno.DataSet.Open;
end;

end.
