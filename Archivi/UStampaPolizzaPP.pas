unit UStampaPolizzaPP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, uniGUIBaseClasses, uniImageList, Data.DB,
  System.Actions, Vcl.ActnList, uniRadioGroup, uniDBRadioGroup, uniCheckBox, uniDBCheckBox,
  uniMultiItem, uniComboBox, uniDBComboBox, uniEdit, uniDateTimePicker, uniDBDateTimePicker,
  uniDBEdit, uniBitBtn, uniPageControl, uniPanel, uniButton, uniSpeedButton, uniLabel,
  uniGUIFrame, uniListBox, uniDBListBox, uniSplitter, uniDBNavigator, uniBasicGrid, uniDBGrid, uniStringGrid,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, uniScreenMask, uniDBLookupComboBox,
  Vcl.Menus, uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaPP = class(TFStampaBasePolizza)
    pgGaranzie: TUniTabSheet;
    pnlGrid: TUniPanel;
    grdPolizzeInGaranzia: TUniDBGrid;
    unspltr: TUniSplitter;
    pnlEdit: TUniPanel;
    dbedtNPolizza: TUniDBEdit;
    dbedtIndirizzoGar: TUniDBEdit;
    dtpckDecorrenza: TUniDBDateTimePicker;
    dbedtCompagniaEmittente: TUniDBEdit;
    dbedtPremioAnnuoLordo: TUniDBNumberEdit;
    dtpckScadenza: TUniDBDateTimePicker;
    ckbx_C: TUniDBCheckBox;
    ckbx_B: TUniDBCheckBox;
    ckbx_A: TUniDBCheckBox;
    cbbMassimale: TUniDBComboBox;
    dbedtPremioNettoSLP: TUniDBNumberEdit;
    grdSelectedGaranzie: TUniDBGrid;
    dsSelGar: TDataSource;
    btnInsert: TUniSpeedButton;
    actInsGaranzia: TAction;
    btnDelGaranzia: TUniSpeedButton;
    actDelGaranzia: TAction;
    btnConfermaGaranzia: TUniSpeedButton;
    actConfermaGaranzia: TAction;
    btnAnnullaGaranzia: TUniSpeedButton;
    actAnnullaModGaranzia: TAction;
    btnModPolizza: TUniSpeedButton;
    actModGaranzia: TAction;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    procedure UniFormCreate(Sender: TObject);
    procedure actInsGaranziaExecute(Sender: TObject);
    procedure actDelGaranziaExecute(Sender: TObject);
    procedure actConfermaGaranziaExecute(Sender: TObject);
    procedure actAnnullaModGaranziaExecute(Sender: TObject);
    procedure actModGaranziaExecute(Sender: TObject);
    procedure pcDatiEreditabiliChange(Sender: TObject);
    procedure dsAssicuratoDataChange(Sender: TObject; Field: TField);
    procedure ckbx_CClick(Sender: TObject);
    procedure cbbMassimaleChange(Sender: TObject);
  private
    { Private declarations }
    procedure RiallineaMaxGaranzie;
    procedure SetAllControlsReadOnly(AContainer: TUniContainer; ReadOnly: boolean);

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;
  end;

function FStampaPolizzaPP: TFStampaPolizzaPP;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiTempPolizzaPP, System.Math, Vcl.DBCtrls;

function FStampaPolizzaPP: TFStampaPolizzaPP;
begin
  Result := TFStampaPolizzaPP(UniMainModule.GetFormInstance(TFStampaPolizzaPP));
end;

procedure TFStampaPolizzaPP.actAnnullaModGaranziaExecute(Sender: TObject);
begin
    case dsAssicurato.DataSet.State of
      dsInsert, dsEdit:
        dsAssicurato.DataSet.Cancel;
    end;

end;

procedure TFStampaPolizzaPP.actConfermaGaranziaExecute(Sender: TObject);
begin
  dsAssicurato.DataSet.Post;
end;


procedure TFStampaPolizzaPP.actDelGaranziaExecute(Sender: TObject);
begin
  dsAssicurato.DataSet.Delete;
  actDelGaranzia.Enabled := not dsAssicurato.DataSet.IsEmpty;
end;


procedure TFStampaPolizzaPP.actInsGaranziaExecute(Sender: TObject);
begin
  if dsAssicurato.DataSet.RecordCount<4 then
  begin
    dsAssicurato.DataSet.Append;
    dbedtNPolizza.SetFocus;
  end else ShowMessage('Si possono inserire al massimo quattro polizze in garanzia.');
end;


procedure TFStampaPolizzaPP.actModGaranziaExecute(Sender: TObject);
begin
  dsAssicurato.DataSet.Edit;
end;


procedure TFStampaPolizzaPP.cbbMassimaleChange(Sender: TObject);
begin
   if cbbMassimale.Text>'' then
   begin
     dbedtPremioAnnuoLordo.ReadOnly:=false;
     dbedtPremioAnnuoLordo.Color := clWindow;
   end;

end;

procedure TFStampaPolizzaPP.ckbx_CClick(Sender: TObject);
begin
   if dbedtNPolizza.Text>'' then
   begin
      if ((Sender as TUniDBCheckBox).Name='ckbx_C') and ckbx_C.Checked then
      begin
         if dsoEdit.DataSet.FieldByName('professione').AsInteger=2 then  // medico !!!
         begin
            ckbx_C.Checked:=false;
            ShowMessage('ATTENZIONE. Vi ricordiamo che La difesa penale non e'' attivabile con Contraenti che esercitano la professione medica.');
         end;
      end;

      if ckbx_C.Checked or ckbx_B.Checked or ckbx_A.Checked then
      begin
        cbbMassimale.ReadOnly:=false;
        cbbMassimale.Color:=clWindow;
      end;

   end else begin
      ShowMessage('Bisogna prima inserire il numero di polizza.');
      ckbx_C.Checked:=false;
      ckbx_B.Checked:=false;
      ckbx_A.Checked:=false;
   end;

end;

procedure TFStampaPolizzaPP.dsAssicuratoDataChange(Sender: TObject; Field: TField);
begin
   actConfermaGaranzia.Enabled   := (dsAssicurato.DataSet.State in [dsInsert, dsEdit]);
  actAnnullaModGaranzia.Enabled := actConfermaGaranzia.Enabled;
  actInsGaranzia.Enabled        := not actConfermaGaranzia.Enabled;
  // pnlEdit.Enabled               := actConfermaGaranzia.Enabled;
  actDelGaranzia.Enabled := not dsAssicurato.DataSet.IsEmpty and (dsAssicurato.DataSet.State <> dsInsert);
  actModGaranzia.Enabled := actDelGaranzia.Enabled;
  SetAllControlsReadOnly(pnlEdit, not actConfermaGaranzia.Enabled);

  if (dsAssicurato.DataSet.State = dsInsert) and (cbbMassimale.ItemIndex=-1) then
  begin
     dbedtPremioAnnuoLordo.ReadOnly:=True;
     dbedtPremioAnnuoLordo.Color := clYellow;
     cbbMassimale.ReadOnly:=True;
     cbbMassimale.Color:=clYellow;
  end;
end;


procedure TFStampaPolizzaPP.pcDatiEreditabiliChange(Sender: TObject);
begin
  if (pcDatiEreditabili.ActivePage = pgGaranzie) and (dsAssicurato.DataSet.State in [dsInsert, dsEdit]) then
    dbedtNPolizza.SetFocus;

end;

procedure TFStampaPolizzaPP.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited;
  RiallineaMaxGaranzie;

end;

procedure TFStampaPolizzaPP.RiallineaMaxGaranzie;
begin
end;

procedure TFStampaPolizzaPP.SetAllControlsReadOnly(AContainer: TUniContainer; ReadOnly: boolean);
var
  i: Integer;

begin
  for i := 0 to AContainer.ControlCount - 1 do
  begin
    // if (AContainer.Controls[i].InheritsFrom(TUniContainer)) then
    // SetAllControlsReadOnly(AContainer.Controls[i] as TUniContainer)
    // else
    // if AContainer.Controls[i].InheritsFrom(TUniBitBtn) then
    // (AContainer.Controls[i] as TUniBitBtn).Enabled := False
    // else
    // if AContainer.Controls[i].InheritsFrom(TUniSpeedButton) then
    // (AContainer.Controls[i] as TUniSpeedButton).Enabled := False
    // else
    if AContainer.Controls[i].InheritsFrom(TUniFormControl) and (AContainer.Controls[i] <> dbedtPremioNettoSLP) then
    begin
      TUniFormControl(AContainer.Controls[i]).ReadOnly := ReadOnly;
      TUniFormControl(AContainer.Controls[i]).Color    := IfThen(ReadOnly, clYellow, clWindow);
    end;
  end;

end;

procedure TFStampaPolizzaPP.UniFormCreate(Sender: TObject);
begin
  inherited;
  pcDatiEreditabili.ActivePage := pgContraente;
  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimale.Items, 'MAXBASE');
  cbbMassimale.ItemIndex := 0;

end;

initialization

RegisterClass(TFStampaPolizzaPP);

end.
