unit UMandanteEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, uniGUIBaseClasses, uniImageList, Data.DB, System.Actions,
  Vcl.ActnList, uniButton, uniBitBtn, uniSpeedButton, uniLabel, uniPanel;

type
  TFMasterEdit1 = class(TFMasterEdit)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FMasterEdit1: TFMasterEdit1;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function FMasterEdit1: TFMasterEdit1;
begin
  Result := TFMasterEdit1(UniMainModule.GetFormInstance(TFMasterEdit1));
end;

end.
