inherited FContratti: TFContratti
  ClientWidth = 972
  Caption = 'Gestione Contratti'
  ExplicitWidth = 988
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 972
    ExplicitWidth = 972
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 972
    ExplicitWidth = 972
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 972
      ExplicitWidth = 972
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 972
        Constraints.MaxWidth = 585
        Constraints.MinWidth = 290
        ExplicitWidth = 972
        inherited btnModify: TUniSpeedButton [0]
          CreateOrder = 1
          Font.Height = -13
          TabOrder = 1
        end
        inherited btnSeleziona: TUniSpeedButton [1]
          Left = 349
          CreateOrder = 2
          Font.Height = -13
          TabOrder = 3
          ExplicitLeft = 349
        end
        object UniSpeedButton1: TUniSpeedButton [2]
          Left = 462
          Top = 0
          Width = 106
          Height = 43
          CreateOrder = 3
          Action = actVisualizzaDocumenti
          ParentFont = False
          Font.Height = -13
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 4
        end
        inherited btnChiudi: TUniSpeedButton [3]
          AlignWithMargins = True
          Left = 580
          CreateOrder = 4
          Font.Height = -13
          Color = clRed
          ScreenMask.Color = clRed
          TabOrder = 6
          ExplicitLeft = 580
        end
        inherited btnInsert: TUniSpeedButton [4]
          Font.Height = -13
          TabOrder = 0
        end
        inherited btnVisualizza: TUniSpeedButton [5]
          Action = actVisualizza
          TabOrder = 2
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 972
        ExplicitWidth = 972
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 972
      ExplicitWidth = 972
      inherited grdElenco: TUniDBGrid
        Top = 51
        Width = 972
        Height = 308
        OnSelectionChange = grdElencoSelectionChange
        OnDrawColumnCell = grdElencoDrawColumnCell
        Columns = <
          item
            FieldName = 'Sigla'
            Filtering.Enabled = True
            Filtering.Editor = unedtSiglaCollab
            Filtering.ChangeDelay = 500
            Title.Caption = 'Sub.Age'
            Width = 60
          end
          item
            FieldName = 'siglaSubColl'
            Title.Caption = 'S.Coll'
            Width = 40
          end
          item
            FieldName = 'N_POLIZZA'
            Filtering.Enabled = True
            Filtering.Editor = unmbrdtNPolizza
            Filtering.ChangeDelay = 500
            Title.Caption = 'N. Polizza'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'CONTRAENTE'
            Filtering.Enabled = True
            Filtering.Editor = unedtContratente
            Filtering.ChangeDelay = 500
            Title.Caption = 'Contraente'
            Width = 250
            Sortable = True
          end
          item
            FieldName = 'DECORRENZA'
            Title.Caption = 'Decorrenza'
            Width = 64
          end
          item
            FieldName = 'SCADENZA'
            Title.Caption = 'Scadenza'
            Width = 64
          end
          item
            FieldName = 'FRAZIONAM'
            Title.Caption = 'Fraz.'
            Width = 64
          end
          item
            FieldName = 'CODICE_AGENZIA'
            Title.Caption = 'Comp.'
            Width = 64
          end
          item
            FieldName = 'DESTIPOPOL'
            Title.Caption = 'Tipo Polizza'
            Width = 165
          end
          item
            FieldName = 'DESRAMO'
            Title.Caption = 'Ramo'
            Width = 150
          end
          item
            FieldName = 'STATO'
            Title.Caption = 'Stato'
            Width = 80
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 160
        Top = 70
        ExplicitLeft = 160
        ExplicitTop = 70
        object cbbReferente: TUniDBLookupComboBox
          Left = 40
          Top = 16
          Width = 145
          Hint = ''
          ListField = 'SIGLA'
          ListSource = DMdatiAge.DSSelPromoter
          KeyField = 'COD_PRODUTTORE'
          ListFieldIndex = 0
          DataField = 'rif_referente'
          DataSource = dsGrid
          TabOrder = 1
          Color = clWindow
        end
        object unedtContratente: TUniEdit
          Left = 40
          Top = 56
          Width = 145
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTCONTRAENTE'
          TabOrder = 2
        end
        object unmbrdtNPolizza: TUniNumberEdit
          Left = 40
          Top = 80
          Width = 145
          Hint = ''
          TabOrder = 3
          DecimalSeparator = ','
        end
        object unedtSiglaCollab: TUniEdit
          Left = 40
          Top = 108
          Width = 145
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTSIGLACOLLAB'
          TabOrder = 4
        end
      end
      object UniPanel1: TUniPanel
        Left = 0
        Top = 0
        Width = 972
        Height = 51
        Hint = ''
        Align = alTop
        TabOrder = 3
        Caption = ''
        DesignSize = (
          972
          51)
        object uRGstatoPolizze: TUniRadioGroup
          Left = 1
          Top = 1
          Width = 216
          Height = 49
          Hint = ''
          Items.Strings = (
            'In vigore'
            'Annullate')
          ItemIndex = 0
          Align = alLeft
          Caption = ' Stato Polizze '
          TabOrder = 1
          Columns = 2
          OnClick = uRGstatoPolizzeClick
        end
        object cbbCompContratti: TUniDBLookupComboBox
          Left = 385
          Top = 9
          Width = 328
          Hint = ''
          ListField = 'NOME'
          ListSource = DMdatiAge.DSCompagnie
          KeyField = 'COD_COMPAGNIA'
          ListFieldIndex = 0
          ClearButton = True
          Anchors = [akLeft, akTop, akBottom]
          TabOrder = 2
          Color = clWindow
          FieldLabel = 'Compagnia'
          ForceSelection = True
          OnChange = cbbCompContrattiChange
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    object actVisualizzaDocumenti: TAction
      Category = 'ItemOperation'
      Caption = 'Vedi Documenti'
      OnExecute = actVisualizzaDocumentiExecute
    end
    object actVisualizza: TAction
      Category = 'ItemOperation'
      Caption = 'Visualizza'
      OnExecute = actVisualizzaExecute
    end
  end
end
