unit UEditContattoCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, uniGUIBaseClasses, uniImageList, Data.DB, System.Actions, Vcl.ActnList,
  uniButton, uniBitBtn, uniSpeedButton, uniLabel, uniPanel, uniMemo, uniDBMemo, uniDateTimePicker, uniDBDateTimePicker,
  uniDBEdit, uniEdit, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox;

type
  TFEditContattoCliente = class(TFMasterEdit)
    pnlEdit: TUniSimplePanel;
    cbbTipoDocumento: TUniDBLookupComboBox;
    dbedtContattoCon: TUniDBEdit;
    dbedtMotivo: TUniDBEdit;
    dtpckDataRilascio: TUniDBDateTimePicker;
    undbmNoteContatto: TUniDBMemo;
    dsTipoContatto: TDataSource;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FEditContattoCliente: TFEditContattoCliente;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMdatiAgeClienti;

function FEditContattoCliente: TFEditContattoCliente;
begin
  Result := TFEditContattoCliente(UniMainModule.GetFormInstance(TFEditContattoCliente));
end;

procedure TFEditContattoCliente.UniFormCreate(Sender: TObject);
begin
  inherited;
  if UniMainModule.operazione = 'VIS' then
    SetAllControlsReadOnly(pnlEdit);

end;

end.
