unit UstampeStatistiche;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterEdit, uniGUIBaseClasses, uniImageList,
  Data.DB, System.Actions, Vcl.ActnList, uniButton, uniBitBtn, uniSpeedButton,
  uniLabel, uniPanel, uniPageControl, uniDBComboBox, uniDBLookupComboBox, uniMultiItem, uniComboBox, uniEdit,
  uniGroupBox, UdmdatiAge, UDMdatiAgeClienti, uniDateTimePicker, uniRadioGroup, uniCheckBox,
  uniScreenMask;

type
  TFstampeStatistiche = class(TFMasterEdit)
    PCStampe: TUniPageControl;
    pgClienti: TUniTabSheet;
    pgContratti: TUniTabSheet;
    pgTitoli: TUniTabSheet;
    cbbOrdClienti: TUniComboBox;
    cbbCollabCliente: TUniDBLookupComboBox;
    cbbCompagniaCliente: TUniDBLookupComboBox;
    cbbProfessioneCliente: TUniDBLookupComboBox;
    cbbGruppoCliente: TUniDBLookupComboBox;
    cbbStatoClienti: TUniComboBox;
    cbbTipoClienti: TUniComboBox;
    grpbxIndirizzoContraente: TUniGroupBox;
    unedtCittaContraente: TUniEdit;
    unedtCapContraente: TUniEdit;
    unedtProvContraente: TUniEdit;
    cbbOrdContratti: TUniComboBox;
    cbbCollabContratti: TUniDBLookupComboBox;
    cbbCompContratti: TUniDBLookupComboBox;
    cbbStatoPolizza: TUniComboBox;
    cbbNuoviContratti: TUniComboBox;
    ungrpbxIndContratti: TUniGroupBox;
    unedtCittaContratti: TUniEdit;
    unedtCapContratti: TUniEdit;
    unedtProvContratti: TUniEdit;
    cbbRamoPolizza: TUniDBLookupComboBox;
    ungrpbxDate: TUniGroupBox;
    DtPkDtEffettoDa: TUniDateTimePicker;
    DtPkDtEffettoA: TUniDateTimePicker;
    DtPkDtScadenzaDa: TUniDateTimePicker;
    DtPkDtScadenzaA: TUniDateTimePicker;
    DtPkDtAnnullDa: TUniDateTimePicker;
    DtPkDtAnnullA: TUniDateTimePicker;
    actStampaClienti: TAction;
    actStampaContratti: TAction;
    cbbTipoPolizza: TUniDBLookupComboBox;
    rgGrpPolizzeMorte: TUniRadioGroup;
    dsProfessioni: TDataSource;
    dsRamoPolizza: TDataSource;
    uncbxOggettiAssicurati: TUniCheckBox;
    cbbCollabSubContratti: TUniDBLookupComboBox;
    cbbIntermediataDa: TUniDBLookupComboBox;
    UniScreenMask1: TUniScreenMask;
    DtincassoDal: TUniDateTimePicker;
    DtincassoAl: TUniDateTimePicker;
    cbbOrdTitoli: TUniComboBox;
    cbbCollabTitoli: TUniDBLookupComboBox;
    cbbCompagniaTitoli: TUniDBLookupComboBox;
    cbbRamoPolTitoli: TUniDBLookupComboBox;
    cbbTipoPolTitoli: TUniDBLookupComboBox;
    cbbCollabSubTitoli: TUniDBLookupComboBox;
    ungrpbxTitoli: TUniGroupBox;
    unedtCittaTitoli: TUniEdit;
    unedtCapTitoli: TUniEdit;
    unedtProvTitoli: TUniEdit;
    actStampaTitoli: TAction;
    cbbStatoPagTitoli: TUniComboBox;
    UniGBDateTitoli: TUniGroupBox;
    dtPkDtEffettoDaTit: TUniDateTimePicker;
    dtPkDtEffettoATit: TUniDateTimePicker;
    pgIncassi: TUniTabSheet;
    cbbCollabIncassi: TUniDBLookupComboBox;
    cbbCompagniaIncassi: TUniDBLookupComboBox;
    ungrpbxFiltroData: TUniGroupBox;
    dtPkDataDaInc: TUniDateTimePicker;
    dtPkDataAInc: TUniDateTimePicker;
    cbbTipoStampaInc: TUniComboBox;
    actStampaIncassi: TAction;
    cbxExportToXLS: TUniCheckBox;
    procedure UniFormCreate(Sender: TObject);
    procedure actAnnullaExecute(Sender: TObject);
    procedure PCStampeChange(Sender: TObject);
    procedure UniFormClose(Sender: TObject; var Action: TCloseAction);
    procedure actStampaClientiExecute(Sender: TObject);
    procedure actStampaContrattiExecute(Sender: TObject);
    procedure cbbCollabContrattiChange(Sender: TObject);
    procedure actStampaTitoliExecute(Sender: TObject);
    procedure cbbStatoPagTitoliChange(Sender: TObject);
    procedure actStampaIncassiExecute(Sender: TObject);
    procedure cbbTipoStampaIncChange(Sender: TObject);
  private
    { Private declarations }
    ParamsList: TStringList;

    procedure ApriDatasets;
    procedure ComponiParametriCliente;
    procedure ComponiParametriContratti;
    procedure ComponiParametriTitoli;
    procedure ComponiParametriIncassi;
    procedure ValidaDate(aDataDa, aDataA: TUniDateTimePicker; const Etichetta: string);

  public
    { Public declarations }
  end;

function FstampeStatistiche: TFstampeStatistiche;

implementation

{$R *.dfm}
{

  LE DIMENSIONI DEI TAB NE COMPONENTE UniPAgeControl sono gestite dal codice javascript messo in
  PageControl -> ClientEvents -> ExtEvents -> [Ext.tab.Panel [tabPanel]] -> afterrender
  BISOGNA USARE ANCHE IL SELETTORE CHE SI TROVA IN ALTO DOPO AVER CLICCATO SU ClientEvents.ExtEvents
  function tabPanel.afterrender(sender, eOpts)
  graffaAperta
  var _height = 35;
  sender.tabBar.setHeight(_height);
  sender.tabBar.items.each(function(el) {
  el.setHeight(_height)
  graffaChiusa )
  graffaChiusa

  UniTabsheet extevents

  function afterrender(sender, eOpts)
  graffaAperta
  sender.tab.btnInnerEl.setStyle("fontSize", "12");
  graffaChiusa
}

uses
  MainModule, uniGUIApplication, System.DateUtils, UPolizzaExceptions, UCodiciErroriPolizza, libSLP, System.StrUtils,
  UDMDatiReport;

function FstampeStatistiche: TFstampeStatistiche;
begin
  Result := TFstampeStatistiche(UniMainModule.GetFormInstance(TFstampeStatistiche));
end;

procedure TFstampeStatistiche.actAnnullaExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFstampeStatistiche.actStampaClientiExecute(Sender: TObject);
begin
  ComponiParametriCliente;
  UniMainModule.DMDatiReport.PreparaQueryStatisticheClienti(ParamsList);
  UniMainModule.DMDatiReport.AnteprimaStatisticheClienti;
end;

procedure TFstampeStatistiche.actStampaContrattiExecute(Sender: TObject);
begin
  ComponiParametriContratti;
  UniMainModule.DMDatiReport.PreparaQueryStatisticheContratti(ParamsList, rgGrpPolizzeMorte.itemindex = 1);
  UniMainModule.DMDatiReport.AnteprimaStatisticheContratti(uncbxOggettiAssicurati.Checked);
end;

procedure TFstampeStatistiche.actStampaIncassiExecute(Sender: TObject);
begin
  ComponiParametriIncassi;
  UniMainModule.DMDatiReport.PreparaQueryStatisticheIncassi(ParamsList, TipoStampaIncassi(cbbTipoStampaInc.itemindex),
    cbxExportToXLS.Checked);
end;

procedure TFstampeStatistiche.actStampaTitoliExecute(Sender: TObject);
begin
  ComponiParametriTitoli;
  UniMainModule.DMDatiReport.PreparaQueryStatisticheTitoli(ParamsList);
  UniMainModule.DMDatiReport.AnteprimaStatisticheTitoli;

end;

procedure TFstampeStatistiche.ApriDatasets;
begin
  cbbCollabCliente.ListSource.DataSet.Open;
  cbbCompagniaCliente.ListSource.DataSet.Open;
  cbbProfessioneCliente.ListSource.DataSet.Open;
  cbbRamoPolizza.ListSource.DataSet.Open;
  cbbTipoPolizza.ListSource.DataSet.Open;

  cbbCollabContratti.ListSource.DataSet.Open;
  // cbbCollabSubContratti.ListSource.DataSet.Open;
  cbbIntermediataDa.ListSource.DataSet.Open;
  cbbCompContratti.ListSource.DataSet.Open;

end;

procedure TFstampeStatistiche.cbbCollabContrattiChange(Sender: TObject);
begin
  inherited;
  // se � stato selezionato questo livello, dai la possibiit� di selezionare anche un eventuale sotto livello
  if cbbCollabContratti.Text > '' then
  begin
    UniMainModule.DMdatiAge.QsubProdut.ParamByName('cod_produttore').AsInteger := cbbCollabContratti.KeyValue;
    UniMainModule.DMdatiAge.QsubProdut.Open;
  end
  else
  begin
    UniMainModule.DMdatiAge.QsubProdut.Close;
  end;
end;

procedure TFstampeStatistiche.cbbStatoPagTitoliChange(Sender: TObject);
begin
  UniGBDateTitoli.Caption := IfThen(cbbStatoPagTitoli.itemindex = 0, 'Filtro su data incasso',
    'Filtro su data effetto');
end;

procedure TFstampeStatistiche.cbbTipoStampaIncChange(Sender: TObject);
begin
  if cbbTipoStampaInc.ItemIndex = 4 then
  begin
     cbbCollabIncassi.ItemIndex := -1;
     cbbCollabIncassi.ReadOnly := True;
  end
  else
     cbbCollabIncassi.ReadOnly := False;
end;

procedure TFstampeStatistiche.ComponiParametriCliente;
begin
  ParamsList.Clear;
  ParamsList.Append('Ordinamento=' + cbbOrdClienti.Text);

  if cbbCollabCliente.Text <> '' then
  begin
    ParamsList.Append('Promoter=' + cbbCollabCliente.KeyValueStr);
    ParamsList.Append('PromoterNome=' + cbbCollabCliente.Text);
  end;

  if cbbCompagniaCliente.Text <> '' then
  begin
    ParamsList.Append('Compagnia=' + cbbCompagniaCliente.KeyValueStr);
    ParamsList.Append('ConpagniaNome=' + cbbCompagniaCliente.Text);
  end;

  if cbbProfessioneCliente.Text <> '' then
  begin
    ParamsList.Append('Professione=' + cbbProfessioneCliente.KeyValueStr);
    ParamsList.Append('ProfessioneNome=' + cbbProfessioneCliente.Text);
  end;

  if cbbStatoClienti.Text <> '' then
  begin
    ParamsList.Append('StatoCli=' + cbbStatoClienti.Text);
    ParamsList.Append('StatoCliNome=' + cbbStatoClienti.Text);
  end;

  if cbbTipoClienti.Text <> '' then
  begin
    ParamsList.Append('NaturaGiuridica=' + cbbTipoClienti.Text);
    ParamsList.Append('NaturaGiuridicaNome=' + cbbTipoClienti.Text);
  end;

  if unedtCapContraente.Text <> '' then
  begin
    ParamsList.Append('Cap=' + Trim(unedtCapContraente.Text));
    ParamsList.Append('CapNome=' + Trim(unedtCapContraente.Text));
  end;

  if unedtCittaContraente.Text <> '' then
  begin
    ParamsList.Append('Citta=' + Trim(unedtCittaContraente.Text));
    ParamsList.Append('CittaNome=' + unedtCittaContraente.Text);
  end;

  if unedtProvContraente.Text <> '' then
  begin
    ParamsList.Append('Provincia=' + Trim(unedtProvContraente.Text));
    ParamsList.Append('ProvinciaNome=' + unedtProvContraente.Text);
  end;

end;

procedure TFstampeStatistiche.ComponiParametriContratti;

begin
  ParamsList.Clear;

  ValidaDate(DtPkDtEffettoDa, DtPkDtEffettoA, 'DataEffetto');
  ValidaDate(DtPkDtScadenzaDa, DtPkDtScadenzaA, 'DataScadenza');
  ValidaDate(DtPkDtAnnullDa, DtPkDtAnnullA, 'DataAnnullamento');
  ValidaDate(DtincassoDal, DtincassoAl, 'DataIncasso');

  ParamsList.Append('Ordinamento=' + cbbOrdContratti.Text);

  if (cbbCollabContratti.Text <> '') then
  begin
    ParamsList.Append('Promoter=' + cbbCollabContratti.KeyValueStr);
    ParamsList.Append('PromoterNome=' + cbbCollabContratti.Text);
  end;

  if cbbCollabSubContratti.Text <> '' then
  begin
    ParamsList.Append('subPromoter=' + cbbCollabSubContratti.KeyValueStr);
    ParamsList.Append('subPromoterNome=' + cbbCollabSubContratti.Text);
  end;

  if cbbIntermediataDa.Text <> '' then
  begin
    ParamsList.Append('intermediataDa=' + cbbIntermediataDa.KeyValueStr);
    ParamsList.Append('inermediataDaNome=' + cbbIntermediataDa.Text);
  end;

  if cbbCompContratti.Text <> '' then
  begin
    ParamsList.Append('Compagnia=' + cbbCompContratti.KeyValueStr);
    ParamsList.Append('CompagniaNome=' + cbbCompContratti.Text);
  end;

  if cbbRamoPolizza.Text <> '' then
  begin
    ParamsList.Append('RamoPolizza=' + cbbRamoPolizza.KeyValueStr);
    ParamsList.Append('RamoPolizzaNome=' + cbbRamoPolizza.Text);
  end;

  if cbbTipoPolizza.Text <> '' then
  begin
    ParamsList.Append('TipoPolizza=' + cbbTipoPolizza.KeyValueStr);
    ParamsList.Append('TipoPolizzaNome=' + cbbTipoPolizza.Text);
  end;

  if cbbStatoPolizza.Text <> '' then
  begin
    ParamsList.Append('StatoPolizze=' + cbbStatoPolizza.Text);
    ParamsList.Append('TipoPolizzaNome=' + cbbTipoPolizza.Text);
  end;

  if cbbNuoviContratti.Text <> '' then
  begin
    ParamsList.Append('NuoviContratti=' + cbbNuoviContratti.Text);
    ParamsList.Append('NuoviContrattiNome=' + cbbNuoviContratti.Text);
  end;

  if unedtCapContratti.Text <> '' then
  begin
    ParamsList.Append('Cap=' + Trim(unedtCapContratti.Text));
    ParamsList.Append('CapNome=' + unedtCapContratti.Text);
  end;

  if unedtCittaContratti.Text <> '' then
  begin
    ParamsList.Append('Citta=' + Trim(unedtCittaContratti.Text));
    ParamsList.Append('CittaNome=' + unedtCittaContratti.Text);
  end;

  if unedtProvContratti.Text <> '' then
  begin
    ParamsList.Append('Provincia=' + Trim(unedtProvContratti.Text));
    ParamsList.Append('ProvinciaNome=' + unedtProvContratti.Text);
  end;

end;

procedure TFstampeStatistiche.ComponiParametriIncassi;
begin
  ParamsList.Clear;

  ValidaDate(dtPkDataDaInc, dtPkDataAInc, 'Periodo');

  if (cbbCollabIncassi.Text <> '') then
  begin
    ParamsList.Append('Promoter=' + cbbCollabIncassi.KeyValueStr);
    ParamsList.Append('PromoterNome=' + cbbCollabIncassi.Text);
  end;

  if cbbCompagniaIncassi.Text <> '' then
  begin
    ParamsList.Append('Compagnia=' + cbbCompagniaIncassi.KeyValueStr);
    ParamsList.Append('CompagniaNome=' + cbbCompagniaIncassi.Text);
  end;

end;

procedure TFstampeStatistiche.ComponiParametriTitoli;
begin
  ParamsList.Clear;

  ValidaDate(dtPkDtEffettoDaTit, dtPkDtEffettoATit, IfThen(cbbStatoPagTitoli.Text = 'Incassati', 'DataIncasso',
    'DataEffetto'));

  ParamsList.Append('Ordinamento=' + cbbOrdTitoli.Text);

  if (cbbCollabTitoli.Text <> '') then
  begin
    ParamsList.Append('Promoter=' + cbbCollabTitoli.KeyValueStr);
    ParamsList.Append('PromoterNome=' + cbbCollabTitoli.Text);
  end;

  if cbbCollabSubTitoli.Text <> '' then
  begin
    ParamsList.Append('subPromoter=' + cbbCollabSubTitoli.KeyValueStr);
    ParamsList.Append('subPromoterNome=' + cbbCollabSubTitoli.Text);
  end;

  if cbbCompagniaTitoli.Text <> '' then
  begin
    ParamsList.Append('Compagnia=' + cbbCompagniaTitoli.KeyValueStr);
    ParamsList.Append('CompagniaNome=' + cbbCompagniaTitoli.Text);
  end;

  if cbbRamoPolTitoli.Text <> '' then
  begin
    ParamsList.Append('RamoPolizza=' + cbbRamoPolTitoli.KeyValueStr);
    ParamsList.Append('RamoPolizzaNome=' + cbbRamoPolTitoli.Text);
  end;

  if cbbTipoPolTitoli.Text <> '' then
  begin
    ParamsList.Append('TipoPolizza=' + cbbTipoPolTitoli.KeyValueStr);
    ParamsList.Append('TipoPolizzaNome=' + cbbTipoPolTitoli.Text);
  end;

  if cbbStatoPagTitoli.itemindex > -1 then
    ParamsList.Append('StatoPagamento=' + cbbStatoPagTitoli.Text);

  // if cbbStatoPolizza.Text <> '' then
  // begin
  // ParamsList.Append('StatoPolizze=' + cbbStatoPolizza.Text);
  // ParamsList.Append('TipoPolizzaNome=' + cbbTipoPolizza.Text);
  // end;

  // if cbbNuoviContratti.Text <> '' then
  // begin
  // ParamsList.Append('NuoviContratti=' + cbbNuoviContratti.Text);
  // ParamsList.Append('NuoviContrattiNome=' + cbbNuoviContratti.Text);
  // end;

  if unedtCapTitoli.Text <> '' then
  begin
    ParamsList.Append('Cap=' + Trim(unedtCapTitoli.Text));
    ParamsList.Append('CapNome=' + unedtCapTitoli.Text);
  end;

  if unedtCittaTitoli.Text <> '' then
  begin
    ParamsList.Append('Citta=' + Trim(unedtCittaTitoli.Text));
    ParamsList.Append('CittaNome=' + unedtCittaTitoli.Text);
  end;

  if unedtProvTitoli.Text <> '' then
  begin
    ParamsList.Append('Provincia=' + Trim(unedtProvTitoli.Text));
    ParamsList.Append('ProvinciaNome=' + unedtProvTitoli.Text);
  end;

end;

procedure TFstampeStatistiche.ValidaDate(aDataDa, aDataA: TUniDateTimePicker; const Etichetta: string);
begin
  if ((yearOf(aDataDa.DateTime) > 1980) and (yearOf(aDataA.DateTime) > 1980)) then
    if (aDataDa.DateTime > aDataA.DateTime) then
    begin
      aDataDa.SetFocus;
      raise ESLP4WebException.create(MSG_DATA_A_LT_DATA_DA);
    end
    else
      ParamsList.Append(Etichetta + '=' + dtosql(aDataDa.DateTime) + ';' + dtosql(aDataA.DateTime));
end;

procedure TFstampeStatistiche.PCStampeChange(Sender: TObject);
begin
  // if pcStampe.ActivePage = UniTSclienti then
  // btnConferma.Action := actStampaClienti
  // else
  // if pcStampe.ActivePage = UniTScontratti then
  // btnConferma.Action := actStampaContratti;

  case PCStampe.TabIndex of
    0:
      btnConferma.Action := actStampaClienti;

    1:
      btnConferma.Action := actStampaContratti;

    2:
      btnConferma.Action := actStampaTitoli;

  else
    btnConferma.Action := actStampaIncassi;
  end;
end;

procedure TFstampeStatistiche.UniFormClose(Sender: TObject; var Action: TCloseAction);
begin
  ParamsList.Free;
  inherited;
end;

procedure TFstampeStatistiche.UniFormCreate(Sender: TObject);
begin
  inherited;

  UniMainModule.archivio                   := 'STS';
  PCStampe.ActivePage                      := pgClienti;
  cbbProfessioneCliente.ListSource.DataSet := UniMainModule.DMdatiAgeClienti.QProfessioniAll;
  cbbRamoPolizza.ListSource.DataSet        := UniMainModule.DMDatiReport.QStatisticheRami;
  cbbTipoPolizza.ListSource.DataSet        := UniMainModule.DMdatiAge.QtipoPolizzeAll;
  cbbCollabContratti.ListSource.DataSet    := UniMainModule.DMdatiAge.QProdut;
  cbbCollabSubContratti.ListSource.DataSet := UniMainModule.DMdatiAge.QsubProdut;
  cbbIntermediataDa.ListSource.DataSet     := UniMainModule.DMdatiAge.QintermediataDa;
  cbbCompContratti.ListSource.DataSet      := UniMainModule.DMdatiAge.QCompagnie;
  cbbRamoPolizza.ListSource.DataSet        := UniMainModule.DMdatiAge.QRami;
  cbbTipoPolizza.ListSource.DataSet        := UniMainModule.DMdatiAge.QTipoPolizze;
  ApriDatasets;
  ParamsList                  := TStringList.create;
  btnConferma.Enabled         := true;
  DtPkDtEffettoDa.DateTime    := 0;
  DtPkDtEffettoA.DateTime     := 0;
  DtPkDtScadenzaDa.DateTime   := 0;
  DtPkDtScadenzaA.DateTime    := 0;
  DtPkDtAnnullDa.DateTime     := 0;
  DtPkDtAnnullA.DateTime      := 0;
  DtincassoDal.DateTime       := 0;
  DtincassoAl.DateTime        := 0;
  dtPkDtEffettoDaTit.DateTime := 0;
  dtPkDtEffettoATit.DateTime  := 0;
  dtPkDataDaInc.DateTime      := 0;
  dtPkDataAInc.DateTime       := 0;

  cbbCollabCliente.ReadOnly   := UniMainModule.IsUtentePromoter;
  cbbCollabContratti.ReadOnly := cbbCollabCliente.ReadOnly;
  cbbCollabTitoli.ReadOnly    := UniMainModule.IsUtentePromoter;
  cbbCollabIncassi.ReadOnly   := UniMainModule.IsUtentePromoter;

  // if UniMainModule.IsUtentePromoter then
  // cbbCollabCliente.ListSource.DataSet.Locate('COD_PRODUTTORE', UniMainModule.UtenteCodice, []);
end;

end.
