inherited FPrintHouseSLP: TFPrintHouseSLP
  ClientHeight = 484
  ClientWidth = 648
  Caption = 'Elenco documenti printhouse'
  OnClose = UniFormClose
  ExplicitWidth = 664
  ExplicitHeight = 523
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 648
    ExplicitWidth = 580
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 648
    Height = 459
    ExplicitWidth = 580
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 648
      ExplicitWidth = 580
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 590
        ExplicitWidth = 580
        inherited btnInsert: TUniSpeedButton
          Action = actVisualizzaDocumento
          ScreenMask.Enabled = True
        end
        inherited btnModify: TUniSpeedButton
          Top = -2
          Action = actEsportaDocumento
          ExplicitTop = -2
        end
        inherited btnSeleziona: TUniSpeedButton
          Left = 247
          Top = -2
          Action = actEsportaDocumento
          ExplicitLeft = 247
          ExplicitTop = -2
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 648
        ExplicitWidth = 580
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 648
      Height = 399
      ExplicitWidth = 580
      inherited grdElenco: TUniDBGrid
        Width = 648
        Height = 399
        WebOptions.Paged = False
        OnDblClick = actVisualizzaDocumentoExecute
        Columns = <
          item
            FieldName = 'DATA_LETTURA'
            Title.Caption = 'Visto'
            Width = 40
            Visible = False
            ReadOnly = True
            ImageOptions.Visible = True
            ImageOptions.Width = 32
            ImageOptions.Height = 32
          end
          item
            FieldName = 'N_POLIZZA'
            Title.Caption = 'N. polizza'
            Width = 64
          end
          item
            FieldName = 'EFFETTO'
            Title.Caption = 'Data'
            Width = 64
          end
          item
            FieldName = 'TIPO_DOC'
            Title.Caption = 'Tipo doc.'
            Width = 64
          end
          item
            FieldName = 'DESCRIZIONE'
            Title.Caption = 'Descrizione'
            Width = 400
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 48
        Top = 78
        ExplicitLeft = 48
        ExplicitTop = 78
      end
    end
  end
  inherited actlstOperazioni: TActionList
    Left = 320
    Top = 152
    object actVisualizzaDocumento: TAction
      Category = 'ItemsDocumento'
      Caption = 'Visualizza'
      OnExecute = actVisualizzaDocumentoExecute
    end
    object actEsportaDocumento: TAction
      Category = 'ItemsDocumento'
      Caption = 'Esporta PDF'
      OnExecute = actEsportaDocumentoExecute
    end
  end
  inherited dsGrid: TDataSource
    Left = 408
    Top = 144
  end
  object UniScreenMask1: TUniScreenMask
    AttachedControl = grdElenco
    Enabled = True
    DisplayMessage = 'Attendere, creazione anteprima in corso ...'
    TargetControl = Owner
    Left = 384
    Top = 261
  end
end
