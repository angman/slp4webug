object FmVediTempTables: TFmVediTempTables
  Left = 0
  Top = 0
  ClientHeight = 386
  ClientWidth = 893
  Caption = 'FmVediTempTables'
  OldCreateOrder = False
  OnClose = UniFormClose
  MonitoredKeys.Keys = <>
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pcTempTables: TUniPageControl
    Left = 0
    Top = 0
    Width = 893
    Height = 386
    Hint = ''
    ActivePage = pgPolizza
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 88
    ExplicitTop = 88
    ExplicitWidth = 289
    ExplicitHeight = 193
    object pgPolizza: TUniTabSheet
      Hint = ''
      Caption = 'Polizza'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 893
      ExplicitHeight = 386
      object grdPloizza: TUniDBGrid
        Left = 3
        Top = 8
        Width = 897
        Height = 319
        Hint = ''
        DataSource = dsPolizza
        LoadMask.Message = 'Loading data...'
        TabOrder = 0
      end
      object undbnvgtrPolizza: TUniDBNavigator
        Left = 8
        Top = 333
        Width = 241
        Height = 25
        Hint = ''
        DataSource = dsPolizza
        TabOrder = 1
      end
    end
    object pgAssicurati: TUniTabSheet
      Hint = ''
      Caption = 'Assicurati'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 893
      ExplicitHeight = 386
      object grdAssicurati: TUniDBGrid
        Left = -4
        Top = 8
        Width = 897
        Height = 319
        Hint = ''
        DataSource = dsAssicurati
        LoadMask.Message = 'Loading data...'
        TabOrder = 0
      end
      object undbnvgtrAssicurati: TUniDBNavigator
        Left = 16
        Top = 333
        Width = 241
        Height = 25
        Hint = ''
        DataSource = dsAssicurati
        TabOrder = 1
      end
    end
    object pgGaranzie: TUniTabSheet
      Hint = ''
      Caption = 'Garanzie'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 893
      ExplicitHeight = 386
      object grdGaranzie: TUniDBGrid
        Left = -12
        Top = 8
        Width = 897
        Height = 319
        Hint = ''
        DataSource = dsGaranzie
        LoadMask.Message = 'Loading data...'
        TabOrder = 0
      end
      object undbnvgtrGaranzie: TUniDBNavigator
        Left = 24
        Top = 333
        Width = 241
        Height = 25
        Hint = ''
        DataSource = dsGaranzie
        TabOrder = 1
      end
    end
  end
  object dsPolizza: TDataSource
    Left = 448
    Top = 344
  end
  object dsAssicurati: TDataSource
    Left = 528
    Top = 336
  end
  object dsGaranzie: TDataSource
    Left = 608
    Top = 344
  end
end
