inherited FCollaboratori: TFCollaboratori
  ClientHeight = 516
  ClientWidth = 972
  Caption = 'Gestione Intermediari / Collaboratori / Dipendenti'
  ExplicitWidth = 988
  ExplicitHeight = 555
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 972
    ExplicitWidth = 972
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 972
    Height = 491
    ExplicitWidth = 972
    ExplicitHeight = 491
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 972
      ExplicitWidth = 972
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 972
        ExplicitWidth = 972
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 972
        ExplicitWidth = 972
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 972
      Height = 431
      ExplicitWidth = 972
      ExplicitHeight = 431
      inherited grdElenco: TUniDBGrid
        Width = 972
        Height = 431
        WebOptions.Paged = False
        OnSelectionChange = grdElencoSelectionChange
        Columns = <
          item
            FieldName = 'NOME'
            Filtering.Enabled = True
            Filtering.Editor = unedtNome
            Title.Caption = 'Nome'
            Width = 300
            Sortable = True
          end
          item
            FieldName = 'SIGLA'
            Filtering.Enabled = True
            Filtering.Editor = unedtSigla
            Title.Caption = 'Sigla'
            Width = 64
            Sortable = True
          end
          item
            FieldName = 'CITTA'
            Filtering.Enabled = True
            Filtering.Editor = unedtCitta
            Title.Caption = 'Citt'#224
            Width = 300
            Sortable = True
          end
          item
            FieldName = 'TIPODESC'
            Title.Caption = 'Tipo'
            Width = 260
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 144
        Top = 46
        ExplicitLeft = 144
        ExplicitTop = 46
        object unedtNome: TUniEdit
          Left = 24
          Top = 32
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTNOME'
          TabOrder = 1
        end
        object unedtCitta: TUniEdit
          Left = 24
          Top = 88
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTCITTA'
          TabOrder = 2
        end
        object unedtSigla: TUniEdit
          Left = 24
          Top = 60
          Width = 121
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTREFERENTE'
          TabOrder = 3
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    inherited actInsert: TAction
      Caption = 'Nuovo collab.'
    end
  end
end
