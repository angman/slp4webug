﻿inherited FStampaPolizzaTP: TFStampaPolizzaTP
  Caption = 'Polizza Tutela Personale'
  ScreenMask.Target = Owner
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlEditButtons: TUniSimplePanel
    inherited btnConferma: TUniSpeedButton
      ScreenMask.Target = Owner
    end
    inherited btnAnteprimaStampa: TUniSpeedButton
      ScreenMask.Target = Owner
    end
  end
  inherited pnlDatiSpecificiPolizza: TUniPanel
    Top = 377
    Height = 284
    TabOrder = 5
    ExplicitTop = 393
    ExplicitHeight = 268
    inherited pcDatiEreditabili: TUniPageControl
      Height = 282
      ActivePage = pgMassimaliGaranzie
      ExplicitHeight = 282
      inherited pgContraente: TUniTabSheet
        ExplicitLeft = 4
        ExplicitTop = 24
        ExplicitWidth = 902
        ExplicitHeight = 238
        inherited dbedtContraente: TUniDBEdit
          Top = 16
          TabOrder = 1
          ExplicitTop = 16
        end
        inherited btnSelCliente: TUniBitBtn
          Top = 14
          TabOrder = 0
          ExplicitTop = 14
        end
        inherited dbedtCodFiscale: TUniDBEdit
          Top = 48
          ExplicitTop = 48
        end
        inherited dbedtPIva: TUniDBNumberEdit
          Top = 48
          ExplicitTop = 48
        end
        inherited dbedtLocNascita: TUniDBEdit
          Top = 80
          ExplicitTop = 80
        end
        inherited dtpckDataNascita: TUniDBDateTimePicker
          Top = 80
          ExplicitTop = 80
        end
        inherited dbedtIndirizzo: TUniDBEdit
          Top = 112
          ExplicitTop = 112
        end
        inherited dbedtCap: TUniDBEdit
          Top = 112
          ExplicitTop = 112
        end
        inherited dbedtCitta: TUniDBEdit
          Top = 112
          ExplicitTop = 112
        end
        inherited dbedtPROVINCIA: TUniDBEdit
          Top = 112
          ExplicitTop = 112
        end
        inherited cbbLkProfessione: TUniDBLookupComboBox
          Top = 144
          ExplicitTop = 144
        end
        inherited dbedtDescrAttività: TUniDBEdit
          Top = 144
          ExplicitTop = 144
        end
      end
      object pgAssicurato: TUniTabSheet
        Hint = ''
        Caption = 'Dati Assicurato / Dati Familiari'
        ParentFont = False
        object pcOggettoAssicurato: TUniPageControl
          Left = 0
          Top = 0
          Width = 902
          Height = 254
          Hint = ''
          ActivePage = pgOggettoAssicurato
          Align = alClient
          TabOrder = 0
          object pgOggettoAssicurato: TUniTabSheet
            Hint = ''
            Caption = 'Assicurato'
            object pnlpncontaier: TUniContainerPanel
              Left = 12
              Top = 43
              Width = 871
              Height = 239
              Hint = ''
              ParentColor = False
              TabOrder = 0
              object pnlAssicurato: TUniPanel
                Left = 0
                Top = 0
                Width = 871
                Height = 239
                Hint = ''
                Align = alClient
                TabOrder = 0
                BorderStyle = ubsNone
                Title = 'Patente'
                Caption = ''
                object dbedtDenominazione: TUniDBEdit
                  Left = 24
                  Top = 8
                  Width = 449
                  Height = 22
                  Hint = ''
                  DataField = 'Denominazione'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 1
                  FieldLabel = 'Nome'
                  FieldLabelWidth = 60
                end
                object dbedtCodiceFiscale: TUniDBEdit
                  Left = 497
                  Top = 9
                  Width = 328
                  Height = 22
                  Hint = ''
                  DataField = 'CodFiscIvas'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 2
                  FieldLabel = 'Cod. fiscale'
                  FieldLabelWidth = 60
                end
                object dbedtPatente: TUniDBEdit
                  Left = 24
                  Top = 49
                  Width = 270
                  Height = 22
                  Hint = ''
                  DataField = 'Patente'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 3
                  FieldLabel = 'Patente'
                  FieldLabelWidth = 60
                end
                object dbedtRilasciataDa: TUniDBEdit
                  Left = 497
                  Top = 49
                  Width = 180
                  Height = 22
                  Hint = ''
                  DataField = 'RilasciataDa'
                  DataSource = dsAssicurato
                  CharCase = ecUpperCase
                  TabOrder = 5
                  FieldLabel = 'Prov. Comune emittente'
                  FieldLabelWidth = 150
                end
                object dtpckDataRilascioPat: TUniDBDateTimePicker
                  Left = 24
                  Top = 97
                  Width = 270
                  Hint = ''
                  DataField = 'DataRilascio'
                  DataSource = dsAssicurato
                  DateTime = 43797.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 6
                  FieldLabel = 'Rilasciata il'
                  FieldLabelWidth = 60
                end
                object dtpckDataScadenzaPat: TUniDBDateTimePicker
                  Left = 497
                  Top = 97
                  Width = 294
                  Hint = ''
                  DataField = 'DataScadenza'
                  DataSource = dsAssicurato
                  DateTime = 43797.000000000000000000
                  DateFormat = 'dd/MM/yyyy'
                  TimeFormat = 'HH:mm:ss'
                  TabOrder = 7
                  FieldLabel = 'Scadenza'
                  FieldLabelWidth = 60
                end
                object cbbCategoriaPat: TUniDBComboBox
                  Left = 328
                  Top = 49
                  Width = 145
                  Hint = ''
                  DataField = 'CategoriaPatente'
                  DataSource = dsAssicurato
                  Items.Strings = (
                    'A'
                    'B'
                    'C'
                    'D')
                  TabOrder = 4
                  FieldLabel = 'Categoria'
                  FieldLabelWidth = 60
                  IconItems = <>
                end
              end
            end
          end
          object pgFamiliari: TUniTabSheet
            Hint = ''
            Caption = 'Familiari'
            object dbedtNome1: TUniDBEdit
              Left = 36
              Top = 39
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome1'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 0
              FieldLabel = 'Nome 1'
              FieldLabelWidth = 50
            end
            object dbedtPatente1: TUniDBEdit
              Left = 364
              Top = 39
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente1'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 1
              FieldLabel = 'Patente 1'
              FieldLabelWidth = 60
            end
            object dbedtCategoria1: TUniDBEdit
              Left = 636
              Top = 39
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente1'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 2
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 1'
              FieldLabelWidth = 70
            end
            object dbedtCategoria2: TUniDBEdit
              Left = 636
              Top = 71
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente2'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 5
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 2'
              FieldLabelWidth = 70
            end
            object dbedtNome2: TUniDBEdit
              Left = 36
              Top = 71
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome2'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 3
              FieldLabel = 'Nome 2'
              FieldLabelWidth = 50
            end
            object dbedtPatente2: TUniDBEdit
              Left = 364
              Top = 71
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente2'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 4
              FieldLabel = 'Patente 2'
              FieldLabelWidth = 60
            end
            object dbedtNome3: TUniDBEdit
              Left = 36
              Top = 108
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome3'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 6
              FieldLabel = 'Nome 3'
              FieldLabelWidth = 50
            end
            object dbedtPatente3: TUniDBEdit
              Left = 364
              Top = 108
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente3'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 7
              FieldLabel = 'Patente 3'
              FieldLabelWidth = 60
            end
            object dbedtCategoria3: TUniDBEdit
              Left = 636
              Top = 108
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente3'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 8
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 3'
              FieldLabelWidth = 70
            end
            object dbedtNome4: TUniDBEdit
              Left = 36
              Top = 143
              Width = 286
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Nome4'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 9
              FieldLabel = 'Nome 4'
              FieldLabelWidth = 50
            end
            object dbedtPatente4: TUniDBEdit
              Left = 364
              Top = 143
              Width = 225
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'Patente4'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 10
              FieldLabel = 'Patente 4'
              FieldLabelWidth = 60
            end
            object dbedtCategoria4: TUniDBEdit
              Left = 636
              Top = 143
              Width = 189
              Height = 22
              Hint = ''
              Enabled = False
              DataField = 'CatPatente4'
              DataSource = dsoEdit
              CharCase = ecUpperCase
              TabOrder = 11
              TabStop = False
              Color = clYellow
              ReadOnly = True
              FieldLabel = 'Categoria 4'
              FieldLabelWidth = 70
            end
            object UniLabel1: TUniLabel
              Left = 169
              Top = 9
              Width = 571
              Height = 16
              Hint = ''
              Caption = 
                'Inserire SOLO i dati dei componenti del nucleo famigliare AD ESC' +
                'LUSIONE del CONTRAENTE'
              ParentFont = False
              Font.Color = clRed
              Font.Height = -13
              Font.Style = [fsBold]
              TabOrder = 12
            end
          end
        end
      end
      object pgMassimaliGaranzie: TUniTabSheet
        Hint = ''
        Caption = 'Massimali e Garanzie'
        ParentFont = False
        object cbbMassimali: TUniDBComboBox
          Left = 16
          Top = 10
          Width = 249
          Hint = ''
          DataField = 'SiglaMassimale'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 0
          FieldLabel = 'Massimale per  vertenza'
          FieldLabelWidth = 80
          IconItems = <>
        end
        object cbbRimbSpesePatPunti: TUniDBComboBox
          Left = 289
          Top = 10
          Width = 224
          Hint = ''
          DataField = 'SiglaRimborsoSpesePatPunti'
          DataSource = dsoEdit
          Items.Strings = (
            '1R=150 + 250'
            '2R=200 + 400'
            '3R=300 + 500'
            '4R=500 + 700')
          ParentFont = False
          TabOrder = 1
          FieldLabel = 'Rimborso Spese Pat. a punti'
          FieldLabelFont.Style = [fsBold]
          ClearButton = True
          IconItems = <>
        end
        object dbgrpForma: TUniDBRadioGroup
          Left = 577
          Top = 5
          Width = 80
          Height = 42
          Hint = 'A = qualsiasi moto e autovettura, autocarri fino a 35 ql.'
          DataField = 'Forma'
          DataSource = dsoEdit
          Caption = 'Forma'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 2
          Items.Strings = (
            'A'
            'B')
          Columns = 2
          Values.Strings = (
            'A'
            'B')
          OnChangeValue = dbgrpFormaChangeValue
        end
        object ckbxPolizzaIndicizzata: TUniDBCheckBox
          Left = 576
          Top = 171
          Width = 206
          Height = 17
          Hint = ''
          DataField = 'Indicizzata'
          DataSource = dsoEdit
          Caption = 'INDICIZZATA'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 12
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxAppendiceControversie: TUniDBCheckBox
          Left = 16
          Top = 94
          Width = 281
          Height = 17
          Hint = ''
          DataField = 'AppControversie'
          DataSource = dsoEdit
          Caption = ' CONTROVERSIE CONTRATTUALI'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 8
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxDanniSubDaFamiliariConv: TUniDBCheckBox
          Left = 576
          Top = 94
          Width = 322
          Height = 17
          Hint = ''
          DataField = 'RecuperoDanniFam'
          DataSource = dsoEdit
          Caption = 'RECUPERO DANNI SUB. DA FAMILIARI CONVIVENTI'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 10
          ParentColor = False
          Color = clBtnFace
          OnClick = ckbxDanniSubDaFamiliariConvClick
        end
        object ckbxbProblematicaRCA: TUniDBCheckBox
          Left = 289
          Top = 71
          Width = 249
          Height = 17
          Hint = ''
          DataField = 'ProblematicheRCA'
          DataSource = dsoEdit
          Caption = ' PROBLEMATICA RC AUTO'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 5
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxPerizieParte: TUniDBCheckBox
          Left = 16
          Top = 67
          Width = 249
          Height = 17
          Hint = ''
          DataField = 'PerizieParte'
          DataSource = dsoEdit
          Caption = ' PERIZIE DI PARTE ED ARBITRATI'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 4
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxLegaleDomiciliataio: TUniDBCheckBox
          Left = 289
          Top = 94
          Width = 280
          Height = 17
          Hint = ''
          DataField = 'LegaleDomiciliatario'
          DataSource = dsoEdit
          Caption = 'LEGALE DOMICILIATARIO'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 9
          ParentColor = False
          Color = clBtnFace
        end
        object ckbxProtezioneFamiglia: TUniDBCheckBox
          Left = 575
          Top = 71
          Width = 231
          Height = 17
          Hint = ''
          DataField = 'ProtezioneFamiglia'
          DataSource = dsoEdit
          Caption = ' Protezione Patenti nucleo familiare'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 6
          ParentColor = False
          Color = clBtnFace
          OnClick = ckbxProtezioneFamigliaClick
        end
        object dbedtNumeroAssicurati: TUniDBNumberEdit
          Left = 800
          Top = 66
          Width = 51
          Height = 22
          Hint = ''
          Enabled = False
          DataField = 'NumeroAssicurati'
          DataSource = dsoEdit
          TabOrder = 7
          MaxValue = 4.000000000000000000
          ShowTrigger = True
          DecimalSeparator = ','
          OnChange = dbedtNumeroAssicuratiChange
        end
        object ckbxRecuperoDanni: TUniDBCheckBox
          Left = 16
          Top = 42
          Width = 206
          Height = 17
          Hint = ''
          DataField = 'RecuperoDanni'
          DataSource = dsoEdit
          Caption = 'RECUPERO DANNI'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 3
          ParentColor = False
          Color = clBtnFace
          OnClick = ckbxRecuperoDanniClick
        end
        object ckbxDifPenVitaPrivata: TUniDBCheckBox
          Left = 16
          Top = 172
          Width = 294
          Height = 17
          Hint = ''
          DataField = 'DifPenVPrivata'
          DataSource = dsoEdit
          Caption = 'DIFESA PENALE PER FATTI DELLA VITA PRIVATA'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 11
          ParentColor = False
          Color = clBtnFace
          OnClick = ckbxDifPenVitaPrivataClick
        end
        object rgGrpPatenteHa: TUniRadioGroup
          Left = 16
          Top = 123
          Width = 450
          Height = 41
          Hint = ''
          Items.Strings = (
            'NESSUNA'
            'CAP'
            'CAP+CQC'
            'CAP+CQC+ADR')
          ItemIndex = 0
          Caption = 'La patente assicurato ha'
          TabOrder = 13
          ParentFont = False
          Font.Style = [fsBold]
          Columns = 4
          OnClick = rgGrpPatenteHaClick
        end
      end
    end
  end
  inherited pnlDatiFissi: TUniPanel
    TabOrder = 3
    inherited dbedtSubPromoter: TUniDBEdit
      TabOrder = 5
    end
    inherited dbedtSostPol: TUniDBEdit
      TabOrder = 6
    end
    inherited dbedtDurataAnni: TUniDBEdit
      TabOrder = 20
    end
    inherited dbedtDurataMesi: TUniDBEdit
      TabOrder = 21
    end
    inherited dbedtRateoGG: TUniDBEdit
      TabOrder = 22
    end
    inherited ckbxRatino: TUniDBCheckBox
      TabOrder = 19
    end
    inherited dtpckScad1Rata: TUniDBDateTimePicker
      TabOrder = 16
    end
    inherited dtpckDataEmissione: TUniDBDateTimePicker
      TabOrder = 17
    end
    inherited ckbxTacitoRinnovo: TUniDBCheckBox
      TabOrder = 18
    end
    inherited dbedtIntermediataDaSigla: TUniDBEdit
      TabOrder = 10
    end
    inherited btnSelezionaProduttore: TUniMenuButton
      TabOrder = 8
    end
    inherited btnIntermediataDa: TUniMenuButton
      TabOrder = 11
    end
    inherited btnPolizzaSostituita: TUniMenuButton
      TabOrder = 9
    end
  end
  inherited pnlPremiPolizza: TUniPanel
    Height = 175
    TabOrder = 4
    ExplicitHeight = 175
    inherited dtpckRataFirmaFinoAl: TUniDBDateTimePicker
      Top = 85
      TabOrder = 9
      ExplicitTop = 85
    end
    inherited dbedtPremioNetto: TUniDBNumberEdit
      Width = 80
      ExplicitWidth = 80
    end
    inherited dbedtIntFrazionamento: TUniDBNumberEdit
      Left = 283
      ExplicitLeft = 283
    end
    inherited dbedtImponibile1: TUniDBNumberEdit
      Left = 390
      Width = 80
      ExplicitLeft = 390
      ExplicitWidth = 80
    end
    inherited dbedtImposte1: TUniDBNumberEdit
      Left = 478
      ExplicitLeft = 478
    end
    inherited dbedtTotale1: TUniDBNumberEdit
      Left = 565
      Width = 90
      ExplicitLeft = 565
      ExplicitWidth = 90
    end
    inherited dbedtNetto2: TUniDBNumberEdit
      Top = 85
      Width = 80
      TabOrder = 10
      ExplicitTop = 85
      ExplicitWidth = 80
    end
    inherited dbedtAccessori2: TUniDBNumberEdit
      Left = 207
      Top = 85
      TabOrder = 11
      ExplicitLeft = 207
      ExplicitTop = 85
    end
    inherited dbedtRimbPolSost: TUniDBNumberEdit
      Left = 283
      Top = 85
      TabOrder = 12
      ExplicitLeft = 283
      ExplicitTop = 85
    end
    inherited dbedtImponibile2: TUniDBNumberEdit
      Left = 390
      Top = 85
      Width = 80
      TabOrder = 13
      ExplicitLeft = 390
      ExplicitTop = 85
      ExplicitWidth = 80
    end
    inherited dbedtImposte2: TUniDBNumberEdit
      Left = 478
      Top = 85
      TabOrder = 14
      ExplicitLeft = 478
      ExplicitTop = 85
    end
    inherited dbedtTotale2: TUniDBNumberEdit
      Left = 565
      Top = 85
      Width = 90
      TabOrder = 15
      ExplicitLeft = 565
      ExplicitTop = 85
      ExplicitWidth = 90
    end
    inherited undbrdgrpProvvig: TUniDBRadioGroup
      Left = 797
      Top = 3
      Width = 106
      ExplicitLeft = 797
      ExplicitTop = 3
      ExplicitWidth = 106
    end
    inherited dbedtPercAccessori: TUniDBNumberEdit
      Left = 824
      Top = 129
      Width = 74
      TabOrder = 16
      FieldLabel = 'Accessori %'
      ExplicitLeft = 824
      ExplicitTop = 129
      ExplicitWidth = 74
    end
    inherited dbedtPremioAccessori: TUniDBNumberEdit
      Left = 207
      ExplicitLeft = 207
    end
    inherited btnPiuNPercAccessori: TUniBitBtn
      Top = 79
      TabOrder = 17
      ExplicitTop = 79
    end
    inherited btnMenoNPercAccessori: TUniBitBtn
      Top = 101
      TabOrder = 20
      ExplicitTop = 101
    end
    inherited UniDBProvvigioni: TUniDBNumberEdit
      Left = 797
      Top = 51
      Width = 100
      Height = 22
      TabOrder = 21
      FieldLabelWidth = 40
      FieldLabelAlign = laLeft
      ExplicitLeft = 797
      ExplicitTop = 51
      ExplicitWidth = 100
      ExplicitHeight = 22
    end
    inherited BaccMax: TUniButton
      Top = 78
      TabOrder = 18
      ExplicitTop = 78
    end
    inherited BaccMin: TUniButton
      Top = 102
      TabOrder = 19
      ExplicitTop = 102
    end
  end
  inherited pnlHeadPolizza: TUniPanel
    TabOrder = 2
  end
  object UniDBNumberEdit1: TUniDBNumberEdit [6]
    Left = 120
    Top = 257
    Width = 80
    Height = 22
    Hint = ''
    DataField = 'Netto1A'
    DataSource = dsoEdit
    Alignment = taRightJustify
    TabOrder = 6
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit2: TUniDBNumberEdit [7]
    Left = 208
    Top = 257
    Width = 70
    Height = 22
    Hint = ''
    DataField = 'Accessori1A'
    DataSource = dsoEdit
    Alignment = taRightJustify
    ParentFont = False
    TabOrder = 7
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit3: TUniDBNumberEdit [8]
    Left = 284
    Top = 257
    Width = 100
    Height = 22
    Hint = ''
    DataField = 'InteressiFrazionamentoA'
    DataSource = dsoEdit
    Alignment = taRightJustify
    TabOrder = 8
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit4: TUniDBNumberEdit [9]
    Left = 391
    Top = 257
    Width = 80
    Height = 22
    Hint = ''
    DataField = 'Imponibile1A'
    DataSource = dsoEdit
    TabOrder = 9
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit5: TUniDBNumberEdit [10]
    Left = 479
    Top = 257
    Width = 80
    Height = 22
    Hint = ''
    DataField = 'Imposte1A'
    DataSource = dsoEdit
    TabOrder = 10
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit6: TUniDBNumberEdit [11]
    Left = 566
    Top = 257
    Width = 90
    Height = 22
    Hint = ''
    DataField = 'Totale1A'
    DataSource = dsoEdit
    TabOrder = 11
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit7: TUniDBNumberEdit [12]
    Left = 120
    Top = 345
    Width = 80
    Height = 22
    Hint = ''
    DataField = 'Netto2A'
    DataSource = dsoEdit
    TabOrder = 12
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit8: TUniDBNumberEdit [13]
    Left = 208
    Top = 345
    Width = 70
    Height = 22
    Hint = ''
    DataField = 'Accessori2A'
    DataSource = dsoEdit
    TabOrder = 13
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit9: TUniDBNumberEdit [14]
    Left = 284
    Top = 345
    Width = 100
    Height = 22
    Hint = ''
    DataField = 'RimborsoSostA'
    DataSource = dsoEdit
    TabOrder = 14
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit10: TUniDBNumberEdit [15]
    Left = 391
    Top = 345
    Width = 80
    Height = 22
    Hint = ''
    DataField = 'Imponibile2A'
    DataSource = dsoEdit
    TabOrder = 15
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit11: TUniDBNumberEdit [16]
    Left = 479
    Top = 345
    Width = 80
    Height = 22
    Hint = ''
    DataField = 'Imposte2A'
    DataSource = dsoEdit
    TabOrder = 16
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit12: TUniDBNumberEdit [17]
    Left = 567
    Top = 345
    Width = 90
    Height = 22
    Hint = ''
    DataField = 'Totale2A'
    DataSource = dsoEdit
    TabOrder = 17
    Color = clYellow
    ReadOnly = True
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit13: TUniDBNumberEdit [18]
    Left = 667
    Top = 317
    Width = 90
    Height = 41
    Hint = ''
    DataField = 'TotaleAB'
    DataSource = dsoEdit
    ParentFont = False
    Font.Height = -13
    Font.Style = [fsBold]
    TabOrder = 18
    Color = clYellow
    ReadOnly = True
    FieldLabel = 'Totale'
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniDBNumberEdit14: TUniDBNumberEdit [19]
    Left = 667
    Top = 229
    Width = 90
    Height = 41
    Hint = ''
    DataField = 'TotaleAB0'
    DataSource = dsoEdit
    ParentFont = False
    Font.Height = -13
    Font.Style = [fsBold]
    TabOrder = 19
    Color = clYellow
    ReadOnly = True
    FieldLabel = 'Totale'
    FieldLabelAlign = laTop
    DecimalSeparator = ','
  end
  object UniScreenMask1: TUniScreenMask
    AttachedControl = btnAnteprimaStampa
    Enabled = True
    DisplayMessage = 'Attendere, generazione anteprima in corso ...'
    TargetControl = Owner
    Left = 757
    Top = 418
  end
end
