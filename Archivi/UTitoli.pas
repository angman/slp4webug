unit UTitoli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniPanel,
  uniBasicGrid, uniDBGrid, uniButton, uniBitBtn, uniSpeedButton, uniLabel, uniGUIBaseClasses,
  uniEdit, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox,
  Vcl.Menus, uniMainMenu, uniScreenMask, uniCheckBox;

type
  TFTitoli = class(TFMasterElenco)
    actIncassa: TAction;
    actChiusuraContabile: TAction;
    actFCProvvisorio: TAction;
    actCancella: TAction;
    btnIncassa: TUniSpeedButton;
    btnChiusContab: TUniSpeedButton;
    btnCancella: TUniSpeedButton;
    unmbrdtNPolizza: TUniNumberEdit;
    unedtContraente: TUniEdit;
    cbbCompagnia: TUniDBLookupComboBox;
    pnlFilters: TUniSimplePanel;
    cbbStatoPagamento: TUniComboBox;
    UniSpeedButton1: TUniSpeedButton;
    actStampaQuietanza: TAction;
    UniPopupMenu1: TUniPopupMenu;
    Annotazionisullaquietanza1: TUniMenuItem;
    UniScreenMask1: TUniScreenMask;
    cbbCompContratti: TUniDBLookupComboBox;
    UniCBcoloraInsoluti: TUniCheckBox;
    UniCBtipo_tit: TUniComboBox;
    procedure UniFormCreate(Sender: TObject);
    procedure cbbStatoPagamentoSelect(Sender: TObject);
    procedure actIncassaExecute(Sender: TObject);
    procedure grdElencoSelectionChange(Sender: TObject);
    procedure actFCProvvisorioExecute(Sender: TObject);
    procedure actChiusuraContabileExecute(Sender: TObject);
    procedure actStampaQuietanzaExecute(Sender: TObject);
    procedure actCancellaExecute(Sender: TObject);
    procedure Annotazionisullaquietanza1Click(Sender: TObject);
    procedure grdElencoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure actModifyExecute(Sender: TObject);
    procedure grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
    procedure cbbCompContrattiChange(Sender: TObject);
    procedure grdElencoDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
    procedure UniCBcoloraInsolutiChange(Sender: TObject);
    procedure UniCBtipo_titSelect(Sender: TObject);
  private
    { Private declarations }
    QuestionDispaga: string;
    MaxDepth: Integer;
    AskDepth: Integer;

    SelectedPolizza: string;
    nomeColonna: string;
    xcodCompagnia: Integer;

  const
    QUEST_DISPAGA = 'l''intenzione di togliere i dati di pagamento dal titolo corrente ?';

    procedure DoQuery(OrdDirection: Boolean);
    procedure ElaboraAppendice;
    procedure ElaboraQuietanza;
    procedure gridRefresh(Sender: TComponent; AResult: Integer);
    procedure AskConfirmation;
    procedure AskConfCallBack(Sender: TComponent; Res: Integer);
    procedure cancellaQuietanzaCallBack(Sender: TComponent; Res: Integer);
    procedure IncassaAppendiceCallBack(Sender: TComponent; Res: Integer);

  public
    { Public declarations }
    procedure SetCausaleAnnullamento(ASelectedItem: Integer; const ASelectedDescription: string);
  end;

function FTitoli: TFTitoli;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UQueryElencoIntf, ServerModule, UCodiciErroriPolizza, System.DateUtils,
  UconfermaDatiIncasso, UEmettiFoglioCassa, uniGUIDialogs, System.StrUtils, System.Math, uTitoloEdit,
  UcausaliAnnullamento;

function FTitoli: TFTitoli;
begin
  Result := TFTitoli(UniMainModule.GetFormInstance(TFTitoli));
end;

procedure TFTitoli.SetCausaleAnnullamento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  // esegui le operazioni con i dati ritornati
  if UniMainModule.DMdatiAgeTitoli.cancellaTitolo(UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('cod_scadenza')
    .AsInteger, ASelectedItem) then
  begin
    grdElenco.DataSource.DataSet.Close;
    grdElenco.DataSource.DataSet.Open;
    { TODO : Definire messaggio in UCodiciErrorePolizza }
    ShowMessage('Cancellazione eseguita. ');
  end;
end;

procedure TFTitoli.cancellaQuietanzaCallBack(Sender: TComponent; Res: Integer);
begin
  case Res of
    mrYes:
      begin
        // ShowMessage('procedo con la cancellazione ');
        UniMainModule.aggiornaPosizione('CLI', 'MOD', 'Clienti');

        with TFcausaliAnnullamento.Create(uniApplication) do
        begin
          OnSelectedItem := SetCausaleAnnullamento; // metodo per restituzione valori  codice, descrizione
          ShowModal;
        end;
      end;
    mrNo:
      ;
  end;

end;

procedure TFTitoli.actCancellaExecute(Sender: TObject);
begin
  inherited;
  // cancellazione della quietanza corrente
  // blocca se titolo incassato
  // chiedi conferma
  // copia i dati del titolo in scadenze1
  // aggiorna i riferimenti delle comunicazioni sul titolo

  // if utente_ok('CANC-TITO') then begin

  if (UniMainModule.IsUtentePromoter) or (UniMainModule.UtenteNoIncasso = 'S') then
  begin
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
    Exit;
  end;

  if (UniMainModule.DMdatiAgeTitoli.TitoloIsQuietanza or UniMainModule.DMdatiAgeTitoli.TitoloIsAppendice or
    UniMainModule.DMdatiAgeTitoli.TitoloIsAppendice12) then
  begin
    if not UniMainModule.DMdatiAgeTitoli.TitoloIsPagato then
    begin
      MessageDlg('Confermi la cancellazione del titolo corrente ?', mtConfirmation, mbYesNo, cancellaQuietanzaCallBack);

    end
    else
      { TODO : Definire messaggio in UCodiciErrorePolizza }
      ShowMessage('Non � possibile cancellare un titolo incassato.');
  end
  else
    { TODO : Definire messaggio in UCodiciErrorePolizza }
    ShowMessage('La cancellazione � possibile solo per quietanze e appendici.');

end;

procedure TFTitoli.actChiusuraContabileExecute(Sender: TObject);
begin
  // chiusura definitiva del periodo
  if (UniMainModule.IsUtentePromoter) then
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA)
  else
    TFEmettiFoglioCassa.ShowChiediDatiFC(uniApplication, true);
end;

procedure TFTitoli.actFCProvvisorioExecute(Sender: TObject);
begin
  if (not UniMainModule.IsUtentePromoter) then
    TFEmettiFoglioCassa.ShowChiediDatiFC(uniApplication, false)
  else
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
end;

procedure TFTitoli.IncassaAppendiceCallBack(Sender: TComponent; Res: Integer);
begin
  ElaboraAppendice;
end;

procedure TFTitoli.actIncassaExecute(Sender: TObject);
var
  i: Integer;
begin
  if (not UniMainModule.VerificaAutorizzazione('SCA', 'PAG')) or
  // (UniMainModule.IsUtentePromoter) or
    (UniMainModule.UtenteNoIncasso = 'S') then
  begin
    ShowMessage(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
    Exit;
  end;

  if UniMainModule.DMdatiAgeTitoli.TitoloIsSLP then // caso per titoli SLP
  begin

    if UniMainModule.DMdatiAgeTitoli.TitoloIsPagato then
    begin
      // probabile richiesta di dispagamento

      AskDepth := 0;
      AskConfirmation;
    end
    else
    begin
      // procedi per l'incasso del titolo corrente
      // considera che pu� essere una quietanza = 20 un'appendice = 12 o 15 una polizza = 11 o 10
      if UniMainModule.DMdatiAgeTitoli.TitoloIsQuietanza or UniMainModule.DMdatiAgeTitoli.TitoloIsPolizza then
      begin
        ElaboraQuietanza;
      end
      else
      begin
        if UniMainModule.DMdatiAgeTitoli.TitoloIsAppendice then
        begin
          // controlla se si tratta di una appendice automatica da direzione
          // e non esiste la quietanza relativa incassata avverti l'operatore che sta
          // sbagliando

          if UniMainModule.DMdatiAgeTitoli.TitoloIsAppendiceDIR then
          begin
            i := UniMainModule.DMdatiAgeTitoli.titolo_pagato(1,
              UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('N_POLIZZA').AsString,
              UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('DECORRENZA').AsDateTime, '20');
            if (i = 1) then
              ElaboraAppendice;
            if (i = 0) then
              { TODO : Definire messaggio in UCodiciErrorePolizza }
              ShowMessage
                ('ATTENZIONE ! Stai incassando una appendice di incremento premio collegata a una quietanza NON ancora incassata ! Bisogna incassare prima la quietanza.');
            // ,IncassaAppendiceCallBack);
            if (i = 2) then
              ShowMessage
                ('ATTENZIONE ! Stai incassando una appendice di incremento premio collegata a una quietanza incassata in un foglio cassa precedente.',
                IncassaAppendiceCallBack);
            if (i = -1) then
            begin
              ShowMessage
                ('ATTENZIONE ! Stai incassando una appendice di incremento premio per la quale non esiste la relativa quietanza ! Operazione impossibile.');
              Abort;
            end;
          end
          else
            ElaboraAppendice;
        end
        else
          if UniMainModule.DMdatiAgeTitoli.TitoloIsAppendice12 then
            ElaboraQuietanza; // caso del titolo relativo ad una polizza non incassato
      end;
    end;

  end;

end;

procedure TFTitoli.actModifyExecute(Sender: TObject);
begin
  if UniMainModule.IsUtentePromoter then
    { TODO : Definire messaggio in UCodiciErrorePolizza }
    ShowMessage('Operazione non consentita per l''utente corrente.')
  else
  begin
    if UniMainModule.DMdatiAgeTitoli.TitoloIsPagato then
      ShowMessage('Non � possibile modificare un titolo gi� incassato.')
    else begin
      if true then
        UniMainModule.traccia('MOD-TITOLO',dsGrid.DataSet.FieldByName('cod_scadenza').AsInteger,'','START MOD TIT '+
                              DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente + ' - pol:' +
                              dsGrid.DataSet.FieldByName('n_polizza').AsString+' - '+
                              dsGrid.DataSet.FieldByName('decorrenza').AsString+' - '+dsGrid.DataSet.FieldByName('tipo_titolo').AsString);
      {
      if (UniMainModule.operazione='VIS') then
        UniMainModule.traccia('VIS-TITOLO',dsoEdit.DataSet.FieldByName('cod_scadenza').AsInteger,'','START VIS TIT '+
                              DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente + ' - pol:' +
                              dsoEdit.DataSet.FieldByName('n_polizza').AsString+' - '+
                              dsoEdit.DataSet.FieldByName('decorrenza').AsString+' - '+dsoEdit.DataSet.FieldByName('tipo_titolo').AsString);
      }
      inherited;
    end;
  end;

end;


procedure TFTitoli.actStampaQuietanzaExecute(Sender: TObject);
var
  PDFFilename: string;
begin
  // stampa la quietanza corrente
  // controlla che il titolo corrente sia una quietanza !!!!!
  if (UniMainModule.VerificaAutorizzazione('SCA', 'STA')) or not(UniMainModule.UtenteStampaQuietanze = 'N') then
  begin
    if (UniMainModule.DMdatiAgeTitoli.TitoloIsQuietanza) then
    begin
      PDFFilename := (UniMainModule.DMDatiReport.StampaQuietanza(grdElenco.DataSource.DataSet.FieldByName
        ('COD_SCADENZA').AsInteger));
    end
    else
      { TODO : Definire messaggio in UCodiciErrorePolizza }
      ShowMessage('Operazione possibile solo su quietanza.');
  end
  else
    ShowMessage('Operazione non consentita per l''utente corrente.');

end;

procedure TFTitoli.cbbCompContrattiChange(Sender: TObject);
begin
  if not(cbbCompContratti.Text > '') then
    xcodCompagnia := 0
  else
    xcodCompagnia := UniMainModule.DMdatiAge.QCompagnie.FieldByName('COD_COMPAGNIA').AsInteger;
  DoQuery(false);
  // DMMaster.EseguiQuery(nomeColonna, 0, '', cbbStatoPagamento.ItemIndex + 1, 0, 0, xcodCompagnia);
end;

procedure TFTitoli.cbbStatoPagamentoSelect(Sender: TObject);
begin
  // DMMaster.EseguiQuery(nomeColonna, 0, '', cbbStatoPagamento.ItemIndex + 1, 0, 0, xcodCompagnia);
  DoQuery(false);
end;

procedure TFTitoli.DoQuery(OrdDirection: Boolean);
var
  ParamList: TStringList;
begin
  // DMMaster.EseguiQuery(nomeColonna, 0, '', cbbStatoPagamento.ItemIndex + 1, 0, 0, xcodCompagnia, UniCBtipo_tit.Text);
  ParamList := TStringList.Create;
  try
    if UniCBtipo_tit.Text <> '' then
      ParamList.Append('Sca.Tipo_Titolo=' + Copy(UniCBtipo_tit.Text,1,2));

    if cbbStatoPagamento.ItemIndex > -1 then
      ParamList.Append('StatoPagamento=' + IntToStr(cbbStatoPagamento.ItemIndex + 1));

    DMMaster.EseguiQuery(nomeColonna, ParamList, '', Integer(OrdDirection), 0, xcodCompagnia);
  finally
    ParamList.Free;
  end;
end;

// procedure TFTitoli.dispagaTitolo;
//
// begin
// deve mettere scadenze.stato=' ' e dat_pag=null
// poi deve fare le segnalazione dell'operazione utilizzando COMUNICA_DIR()

// se il titolo � scadenze.tipo_titolo>11 allora:
// fai la serie di tre domande di conferma
// devi solo mettere scadenze.stato=' ' e dat_pag=null
// fare le segnalazione dell'operazione utilizzando COMUNICA_DIR()

// se il titolo � scadenze.tipo_titolo<=11 allora:
// fai la serie di tre domande di conferma
// fai ulteriore domanda specifica per le polizze per verificare che l'utente sia conscio dell'operazione
// inizia transazione
// cancella da scadenze il record corrente
// cancella da polizze.dat il record relativo alla polizza
// in slp_tpolizze.status=' '
// fine transazione
// comunica_dir se transazione finita bene

{
  if application.MessageBox('Confermi l''intenzione di togliere i dati di pagamento dal titolo corrente ?',
  'Attenzione', MB_OKCANCEL ) = IDOK then
  begin
  if application.MessageBox('RICONFERMI l''intenzione di togliere i dati di pagamento dal titolo corrente ?',
  'Attenzione', MB_OKCANCEL ) = IDOK then
  begin
  if application.MessageBox('RICONFERMI l''intenzione di togliere i dati di pagamento dal titolo corrente ? La data di copertura precedentemente registrata per questo titolo NON sar� pi� ripristinabile !',
  'Attenzione', MB_OKCANCEL ) = IDOK then
  begin
  if Tscadenze.locate('cod_scadenza',Qscadenze.fieldbyname('cod_scadenza').asinteger,[]) then
  begin
  passa:=true;
  if (Tscadenze.FieldByName('tipo_titolo').AsString<='11') and (Tscadenze.FieldByName('rif_cod_compagnia').AsInteger=1) then begin
  passa:= application.MessageBox('Il titolo � relativo all''incasso di una polizza !!! RIConfermi l''intenzione di togliere i dati di pagamento dal titolo corrente ?',
  'Attenzione', MB_OKCANCEL ) = IDOK ;
  end;
  if passa then
  begin
  if reclock(Tscadenze) then
  begin
  Tscadenze.fieldbyname('data_pag').asString:='';
  Tscadenze.fieldbyname('stato').asString:=' ';

  if (copy(Tscadenze.fieldbyname('descriz').asString,1,2)='PO') then
  begin
  Tscadenze.fieldbyname('provvigioni').asCurrency:=roundTo(strtoint(copy(Tscadenze.fieldbyname('descriz').asString,3,7))/100,-1);
  Tscadenze.fieldbyname('descriz').asString:='';
  end;

  Tscadenze.post;
  // comunica l'operazione !!!
  // function comunica(operazione, varie,tipo,nomefile,userfilec,mes: string):boolean;
  t:='';

  if (Tscadenze.FieldByName('RIF_COD_COMPAGNIA').asInteger=1) and (Tscadenze.FieldByName('tipo_titolo').AsString>='20') then
  menubase.comunica('CKA', datetimetostr(now)+' DISQ Np'+Tscadenze.fieldbyname('N_POLIZZA').asString+' '+
  dataNormale(Tscadenze.fieldbyname('decorrenza').asDateTime) ,'CK','','','',
  'DIQ',Tscadenze.fieldbyname('N_POLIZZA').asString,Tscadenze.fieldbyname('decorrenza').asDateTime);

  if (Tscadenze.FieldByName('RIF_COD_COMPAGNIA').asInteger=1) and
  ((Tscadenze.FieldByName('tipo_titolo').AsString='15') or (Tscadenze.FieldByName('tipo_titolo').AsString='12')) then
  menubase.comunica('CKA', datetimetostr(now)+' DISQ Np'+Tscadenze.fieldbyname('N_POLIZZA').asString+' '+
  dataNormale(Tscadenze.fieldbyname('decorrenza').asDateTime) ,'CK','','','',
  'DIA',Tscadenze.fieldbyname('N_POLIZZA').asString,Tscadenze.fieldbyname('decorrenza').asDateTime);

  // aggiorna la data pi� recente vista dal programma ...
  aggiorna_data(date);
  t:=padr(Tscadenze.fieldbyname('N_POLIZZA').asString,' ',15) + dtos(Tscadenze.fieldbyname('RATA').asDateTime);
  traccia('DISPAGSCA',Tscadenze.fieldbyname('COD_SCADENZA').asInteger,'SCADENZE',t);

  Qscadenze.DisableControls;
  Qscadenze.close;
  Qscadenze.open;
  Qscadenze.locate('cod_scadenza',Tscadenze.fieldbyname('cod_scadenza').asinteger,[]);
  Qscadenze.EnableControls;
  if (Tscadenze.FieldByName('tipo_titolo').AsString<='11') and (Tscadenze.FieldByName('rif_cod_compagnia').AsInteger=1) then
  begin
  // cancella il titolo e rimetti la polizza tra le polizze da perfezionare !!!
  npol:=Tscadenze.fieldbyname('n_polizza').asString;
  if DMdatiAge.Tpolizze.locate('n_polizza;rif_compagnia',vararrayof([Tscadenze.fieldbyname('n_polizza').asString,1]),[]) then
  begin
  // cambia lo stat alla polizza perfezionata
  // cancella la poliza da Tpolizze
  // aggiorna lo stato del cliente se serve
  if DMdatiAge.Tslp_tpolizze.locate('n_polizza;rif_compagnia',vararrayof([Tscadenze.fieldbyname('n_polizza').asString,1]),[]) then
  begin
  // cancella solo se la polizza arriva da questo archivio !!!
  DMdatiAge.Tpolizze.Delete;
  end;
  if not DMdatiAge.Tslp_tpolizze.Active then DMdatiAge.Tslp_tpolizze.open;
  if DMdatiAge.Tslp_tpolizze.locate('n_polizza;rif_compagnia',vararrayof([Tscadenze.fieldbyname('n_polizza').asString,1]),[]) then
  begin
  DMdatiAge.Tslp_tpolizze.Edit;
  DMdatiAge.Tslp_tpolizze.fieldbyname('status').asString:='' ;
  DMdatiAge.Tslp_tpolizze.Post;
  end;
  // comunica l'operazione !!!
  // function comunica(operazione, varie,tipo,nomefile,userfilec,mes: string):boolean;
  menubase.comunica('CKA', datetimetostr(now)+' DISP Np'+Tscadenze.fieldbyname('N_POLIZZA').asString+' '+
  dataNormale(Tscadenze.fieldbyname('decorrenza').asDateTime) ,'CK','','','',
  'DIP',Tscadenze.fieldbyname('N_POLIZZA').asString,Tscadenze.fieldbyname('decorrenza').asDateTime);   //

  end;
  Tscadenze.delete;
  Qscadenze.DisableControls;
  Qscadenze.close;
  Qscadenze.open;
  Qscadenze.EnableControls;
  Qscadenze.locate('cod_scadenza',csca,[]);
  end;
  end else record_bloccato;
  end;
  end;
  end;
  end;
  end;

}
// end;

procedure TFTitoli.AskConfirmation;
const
  PrefArray: array [1 .. 4] of string = ('Confermi ', 'Riconfermi ', 'Riconfermi ', 'Riconfermi ');
  MsgContinuazione = 'La data di copertura precedentemente registrata per questo titolo NON sar� pi� ripristinabile';
var
  Messaggio: string;
begin
  MaxDepth := ifThen(UniMainModule.DMdatiAgeTitoli.TitoloIsScadenza, 3, 4);
  inc(AskDepth);
  Messaggio := format(QuestionDispaga, [PrefArray[AskDepth], ifThen((MaxDepth = 3) and (AskDepth <> 3), '',
    MsgContinuazione)]);
  MessageDlg(Messaggio, mtConfirmation, mbYesNo, AskConfCallBack);
end;

procedure TFTitoli.Annotazionisullaquietanza1Click(Sender: TObject);
begin
  inherited;
  ShowMessage('annotazioni');
end;

procedure TFTitoli.AskConfCallBack(Sender: TComponent; Res: Integer);
begin
  case Res of
    mrYes:
      begin
        if AskDepth < MaxDepth then
          AskConfirmation
        else
        begin
          if UniMainModule.DMdatiAgeTitoli.TitoloIsScadenza then
            UniMainModule.DMdatiAgeTitoli.dispagaScadenza(SelectedCode)
          else
            UniMainModule.DMdatiAgeTitoli.dispagaPolizza(SelectedPolizza, SelectedCode);
          grdElenco.Update;
          grdElenco.refresh;
        end;
      end
  else
    ;
  end;

end;

procedure TFTitoli.ElaboraAppendice;
begin
  TFconfermaDatiIncasso.ShowConfermaDatiIncasso(uniApplication,
    UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('COD_SCADENZA').AsInteger, 'Appendice', gridRefresh);
end;

procedure TFTitoli.ElaboraQuietanza;
begin
  TFconfermaDatiIncasso.ShowConfermaDatiIncasso(uniApplication,
    UniMainModule.DMdatiAgeTitoli.Qtitoli.FieldByName('COD_SCADENZA').AsInteger, 'Quietanza', gridRefresh);
  grdElenco.Update;
end;

procedure TFTitoli.gridRefresh(Sender: TComponent; AResult: Integer);
begin
  grdElenco.refresh;
end;

procedure TFTitoli.grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
begin
  nomeColonna := Column.FieldName;
  DoQuery(Direction);
  // DMMaster.EseguiQuery(nomeColonna, 0, '', cbbStatoPagamento.ItemIndex + 1, Integer(Direction), 0, xcodCompagnia);
end;

procedure TFTitoli.grdElencoDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
  Attribs: TUniCellAttribs);
begin
  if (ACol>=0) and (ACol<=2) and (grdElenco.DataSource.DataSet.FieldByName('decorrenza').AsDateTime < Date) and UniCBcoloraInsoluti.Checked then
  begin

    if (DaysBetween(Date, grdElenco.DataSource.DataSet.FieldByName('decorrenza').AsDateTime) > 40) and // for rows
      (grdElenco.DataSource.DataSet.FieldByName('tipo_titolo').AsString = '20') then
    begin
      // Attribs.Font.Style:=[fsbold];
      // Attribs.Font.Color:=clWhite; //font color
      Attribs.Color := clWebLightCoral; // background color
    end;
  end;
end;

procedure TFTitoli.grdElencoMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if Button = mbRight then
  begin
    // LastX:=X;
    // LastY:=Y;
    UniPopupMenu1.Popup(X, Y, grdElenco);
  end;
end;

procedure TFTitoli.grdElencoSelectionChange(Sender: TObject);
begin
  SelectedCode    := grdElenco.DataSource.DataSet.FieldByName('COD_SCADENZA').AsInteger;
  SelectedPolizza := grdElenco.DataSource.DataSet.FieldByName('N_POLIZZA').AsString;
end;

procedure TFTitoli.UniCBcoloraInsolutiChange(Sender: TObject);
begin
  grdElenco.DataSource.DataSet.refresh;

end;

procedure TFTitoli.UniCBtipo_titSelect(Sender: TObject);
begin
  DoQuery(false);
end;

procedure TFTitoli.UniFormCreate(Sender: TObject);
begin
  // UniMainModule.archivio := 'TIT';
  inherited;
  self.DMMaster  := (UniMainModule.DMdatiAgeTitoli as IQueryElenco);
  dsGrid.DataSet := UniMainModule.DMdatiAgeTitoli.Qtitoli;
  UniMainModule.DMdatiAge.DSCompagnie.DataSet.Open;
  UniMainModule.DMdatiAge.DSCompagnie.DataSet.First;
  nomeColonna   := 'N_POLIZZA';
  xcodCompagnia := 0;

  self.DMMaster.EseguiQuery('N_POLIZZA', nil, '');
  dsGrid.DataSet.Filtered := False;
  dsGrid.DataSet.Filter   := '';

  EditPageClassName         := 'TFTitoloEdit';
  btnSeleziona.action       := actIncassa;
  btnChiudi.action          := actChiusuraContabile;
  btnChiusContab.Font.Color := clBlack;
  btnIncassa.action         := actFCProvvisorio;
  btnChiusContab.action     := actCancella;
  btnCancella.action        := actChiudi;
  btnCancella.Font.Color    := clRed;
  btnCancella.Font.Style    := [fsUnderline];
  QuestionDispaga           := '%s' + QUEST_DISPAGA + sLineBreak + ' %s';
  grdElenco.ClearFilters;
  UniMainModule.traccia('TITOLI',0,'','Ingresso TIT '+DateTimeToStr(Now) + ' ope:'+ UniMainModule.DMdatiAge.SLPdati_age.utente);
end;

end.
