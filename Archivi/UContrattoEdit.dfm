inherited FContrattoEdit: TFContrattoEdit
  ClientHeight = 615
  ClientWidth = 1021
  Caption = 'Editazione dati Contratto'
  OnClose = UniFormClose
  ExplicitWidth = 1037
  ExplicitHeight = 654
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 1021
    ExplicitWidth = 1021
  end
  inherited pnlEditButtons: TUniSimplePanel
    Width = 1021
    ExplicitWidth = 1021
    inherited btnConferma: TUniSpeedButton
      Width = 150
      Font.Height = -12
      Font.Style = [fsBold]
      ExplicitWidth = 150
    end
    inherited btnAnnulla: TUniSpeedButton
      Left = 181
      Font.Height = -12
      ExplicitLeft = 181
    end
  end
  object pcContrattoEdit: TUniPageControl [2]
    Left = 0
    Top = 69
    Width = 1021
    Height = 546
    Hint = ''
    ActivePage = pgEdit
    Align = alClient
    ClientEvents.ExtEvents.Strings = (
      
        'tabPanel.afterrender=function tabPanel.afterrender(sender, eOpts' +
        ')'#13#10'{'#13#10'    var _height = 40;'#13#10'    sender.tabBar.setHeight(_height' +
        ');'#13#10'    sender.tabBar.items.each(function(el) {'#13#10'        el.setH' +
        'eight(_height)'#13#10'    })'#13#10'}')
    TabOrder = 2
    ExplicitLeft = 304
    ExplicitTop = 200
    ExplicitWidth = 289
    ExplicitHeight = 193
    object pgEdit: TUniTabSheet
      Hint = ''
      Caption = 'Dati generali'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      ParentFont = False
      DesignSize = (
        1013
        518)
      object ungrpbxDati: TUniGroupBox
        Left = 17
        Top = 100
        Width = 984
        Height = 191
        Hint = ''
        Caption = 'Dati'
        TabOrder = 1
        ParentFont = False
        Font.Height = -13
        object unedtReferente: TUniEdit
          Left = 13
          Top = 71
          Width = 409
          Hint = ''
          Text = 'unedtReferente'
          ParentFont = False
          TabOrder = 10
          ReadOnly = True
          FieldLabel = 'Referente/ Amministratore'
        end
        object dtpckDECORRENZA: TUniDBDateTimePicker
          Left = 13
          Top = 99
          Width = 197
          Hint = ''
          DataField = 'DECORRENZA'
          DataSource = dsoEdit
          DateTime = 43754.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 14
          ParentFont = False
          FieldLabel = 'Decorrenza'
        end
        object dtpckSCADENZA: TUniDBDateTimePicker
          Left = 228
          Top = 99
          Width = 197
          Hint = ''
          DataField = 'SCADENZA'
          DataSource = dsoEdit
          DateTime = 43754.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 15
          ParentFont = False
          FieldLabel = 'Scadenza'
          FieldLabelWidth = 70
        end
        object dtpckEmissione: TUniDBDateTimePicker
          Left = 460
          Top = 99
          Width = 197
          Hint = ''
          DataField = 'DATA_EMISSIONE'
          DataSource = dsoEdit
          DateTime = 43754.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 16
          ParentFont = False
          FieldLabel = 'Emessa il'
        end
        object dtpckSOSPESO_DAL: TUniDBDateTimePicker
          Left = 13
          Top = 152
          Width = 197
          Hint = ''
          DataField = 'SOSPESO_DAL'
          DataSource = dsoEdit
          DateTime = 43754.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 20
          ParentFont = False
          FieldLabel = 'Sospeso dal'
        end
        object dtpckSOSPESO_AL: TUniDBDateTimePicker
          Left = 225
          Top = 152
          Width = 197
          Hint = ''
          DataField = 'SOSPESO_AL'
          DataSource = dsoEdit
          DateTime = 43754.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 21
          ParentFont = False
          FieldLabel = 'Sospeso al'
          FieldLabelWidth = 70
        end
        object dbedtContraente: TUniDBEdit
          Left = 13
          Top = 43
          Width = 409
          Height = 22
          Hint = ''
          DataField = 'CONTRAENTE'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 4
          ReadOnly = True
          FieldLabel = 'Contraente'
        end
        object btnSelezionaContraente: TUniBitBtn
          Left = 431
          Top = 41
          Width = 25
          Height = 25
          Action = actSelezionaContraente
          ParentFont = False
          TabOrder = 5
          IconAlign = iaCenter
          Images = untvmglstIcons
          ImageIndex = 0
        end
        object cbbFrazionamento: TUniDBComboBox
          Left = 707
          Top = 99
          Width = 265
          Hint = ''
          DataField = 'FRAZIONAM'
          DataSource = dsoEdit
          Items.Strings = (
            'Annuale=A'
            'Semestrale=S'
            'Trimestrale=T'
            'Quadrimetrale=Q'
            'Bimestrale=B'
            'Mensile=M'
            'P-temporaneo=P'
            'Unico=U')
          ItemIndex = 0
          ParentFont = False
          TabOrder = 17
          FieldLabel = 'Frazionamento'
          ClearButton = True
          IconItems = <>
        end
        object dbedtIntermediataDa: TUniDBEdit
          Left = 516
          Top = 43
          Width = 143
          Height = 22
          Hint = ''
          DataField = 'INTERMEDIATA_DA'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 6
          ReadOnly = True
          FieldLabel = 'Intermed. da'
          FieldLabelWidth = 80
        end
        object dbedtIntermDaDesc: TUniDBEdit
          Left = 707
          Top = 43
          Width = 265
          Height = 22
          Hint = ''
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 9
        end
        object dbedtN_POLIZZA_SOSTITUITA: TUniDBEdit
          Left = 707
          Top = 152
          Width = 265
          Height = 22
          Hint = ''
          DataField = 'N_POLIZZA_SOSTITUITA'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 22
          FieldLabel = 'Ha sostituito polizza'
        end
        object cbbProvvigioni: TUniDBComboBox
          Left = 707
          Top = 15
          Width = 265
          Hint = ''
          DataField = 'TIPO_PROVVIGIONI'
          DataSource = dsoEdit
          Items.Strings = (
            ''
            'RICORRENTI=R'
            'PRECONTATE=P'
            'NULLA=N')
          ParentFont = False
          TabOrder = 3
          FieldLabel = 'Tipo provvig.'
          ClearButton = True
          IconItems = <>
        end
        object dbedtCodPolizza: TUniDBEdit
          Left = 247
          Top = 15
          Width = 175
          Height = 22
          Hint = ''
          DataField = 'codice'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 2
          FieldLabel = 'Codice'
          FieldLabelWidth = 70
        end
        object dbedtNPolizza: TUniDBEdit
          Left = 13
          Top = 15
          Width = 179
          Height = 22
          Hint = ''
          DataField = 'N_POLIZZA'
          DataSource = dsoEdit
          ParentFont = False
          TabOrder = 1
          FieldLabel = 'N. Contratto'
        end
        object dbedtDescrizione: TUniDBEdit
          Left = 13
          Top = 127
          Width = 409
          Height = 22
          Hint = ''
          DataField = 'DESCRIZIONE'
          DataSource = dsoEdit
          TabOrder = 18
          FieldLabel = 'Descrizione'
        end
        object cbbConvenzione: TUniDBLookupComboBox
          Left = 707
          Top = 125
          Width = 265
          Height = 24
          Hint = ''
          ListField = 'descrizione'
          ListSource = DMdatiAge.DSConvenzioni
          KeyField = 'cod_convenzione'
          ListFieldIndex = 0
          ClearButton = True
          DataField = 'CONVENZIONE'
          DataSource = dsoEdit
          TabOrder = 19
          Color = clWindow
          FieldLabel = 'Convenzione'
        end
        object dbedtSubPromoter: TUniDBEdit
          Left = 707
          Top = 71
          Width = 265
          Height = 22
          Hint = ''
          Visible = False
          DataField = 'SUB_PROMOTER'
          DataSource = dsoEdit
          ParentFont = False
          Font.Height = -13
          TabOrder = 13
        end
        object btnDelReferente: TUniBitBtn
          Left = 459
          Top = 72
          Width = 25
          Height = 25
          Hint = ''
          Caption = ''
          ParentFont = False
          TabOrder = 12
          IconAlign = iaCenter
          Images = untvmglstIcons
          ImageIndex = 1
        end
        object btnSelezionaReferente: TUniBitBtn
          Left = 431
          Top = 72
          Width = 25
          Height = 25
          Action = actSelezionaReferente
          ParentFont = False
          TabOrder = 11
          IconAlign = iaCenter
          Images = untvmglstIcons
          ImageIndex = 0
        end
        object btnSelezionaCollaboratore: TUniBitBtn
          Left = 660
          Top = 42
          Width = 25
          Height = 25
          Hint = ''
          Caption = ''
          ParentFont = False
          Font.Height = -13
          TabOrder = 7
          IconAlign = iaCenter
          Images = untvmglstIcons
          ImageIndex = 0
        end
        object btnDelCollaboratore: TUniBitBtn
          Left = 681
          Top = 42
          Width = 25
          Height = 25
          Hint = ''
          Caption = ''
          ParentFont = False
          Font.Height = -13
          TabOrder = 8
          IconAlign = iaCenter
          Images = untvmglstIcons
          ImageIndex = 1
        end
      end
      object ungrpbxMandanteRamoTipoPol: TUniGroupBox
        Left = 17
        Top = 16
        Width = 984
        Height = 83
        Hint = ''
        Caption = 'Mandante Ramo Tipo Polizza'
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        ParentFont = False
        Font.Color = clBlack
        Font.Height = -13
        object cbbCompagnia: TUniDBLookupComboBox
          Left = 13
          Top = 21
          Width = 409
          Height = 24
          Hint = ''
          ListField = 'NOME'
          ListSource = DMdatiAge.DSCompagnie
          KeyField = 'COD_COMPAGNIA'
          ListFieldIndex = 0
          ClearButton = True
          DataField = 'RIF_COMPAGNIA'
          DataSource = dsoEdit
          ParentFont = False
          Font.Color = clBlack
          Font.Height = -13
          TabOrder = 1
          Color = clWindow
          FieldLabel = 'Compagnia'
        end
        object cbbTipoPolizza: TUniDBLookupComboBox
          Left = 13
          Top = 49
          Width = 409
          Height = 24
          Hint = ''
          ListField = 'DESCRIZIONE'
          ListSource = DMdatiAge.DStipoPolizzeAll
          KeyField = 'COD_TIPO_POL'
          ListFieldIndex = 0
          ClearButton = True
          DataField = 'RIF_TIPO_POL'
          DataSource = dsoEdit
          ParentFont = False
          Font.Color = clBlack
          Font.Height = -13
          TabOrder = 3
          Color = clWindow
          FieldLabel = 'Tipo contratto'
          OnChange = cbbTipoPolizzaChange
        end
        object cbbRamo: TUniDBLookupComboBox
          Left = 563
          Top = 21
          Width = 409
          Height = 24
          Hint = ''
          ListField = 'DESCRIZIONE'
          ListSource = DMdatiAge.DSRami
          KeyField = 'COD_RAMO'
          ListFieldIndex = 0
          ClearButton = True
          DataField = 'RIF_COD_RAMO'
          DataSource = dsoEdit
          ParentFont = False
          Font.Color = clBlack
          Font.Height = -13
          TabOrder = 2
          Color = clWindow
          FieldLabel = 'Ramo'
        end
        object cbbAppoggiata: TUniDBLookupComboBox
          Left = 563
          Top = 51
          Width = 409
          Height = 24
          Hint = ''
          ListField = 'nome'
          ListSource = DMdatiAge.DSAppoggio
          KeyField = 'cod_appoggio'
          ListFieldIndex = 0
          ClearButton = True
          DataField = 'appoggiata_a'
          DataSource = dsoEdit
          ParentFont = False
          Font.Color = clBlack
          Font.Height = -13
          TabOrder = 4
          Color = clWindow
          FieldLabel = 'Appoggiata a'
        end
      end
      object ungrpbxDatIndicizazzione: TUniGroupBox
        Left = 17
        Top = 294
        Width = 984
        Height = 60
        Hint = ''
        Caption = 'Dati Indicizazione'
        TabOrder = 2
        ParentFont = False
        Font.Height = -13
        object dtpckDataStartIndice: TUniDBDateTimePicker
          Left = 13
          Top = 24
          Width = 197
          Hint = ''
          DataField = 'data_ini_indice'
          DataSource = dsoEdit
          DateTime = 43755.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 1
          ParentFont = False
          Font.Height = -13
          FieldLabel = 'Inizio indicizazione'
        end
        object dtpckDataEndIndice: TUniDBDateTimePicker
          Left = 707
          Top = 24
          Width = 197
          Hint = ''
          DataField = 'data_end_indice'
          DataSource = dsoEdit
          DateTime = 43755.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 3
          ParentFont = False
          Font.Height = -13
          FieldLabel = 'Data blocco indicizazione'
        end
        object dbedtIncIndice: TUniDBNumberEdit
          Left = 408
          Top = 24
          Width = 249
          Height = 22
          Hint = ''
          DataField = 'inc_indice'
          DataSource = dsoEdit
          ParentFont = False
          Font.Height = -13
          TabOrder = 2
          FieldLabel = 'Incremento raggiunto'
          DecimalSeparator = ','
        end
      end
      object ungrpbxPremio: TUniGroupBox
        Left = 17
        Top = 358
        Width = 984
        Height = 80
        Hint = ''
        Caption = 'Premio'
        TabOrder = 3
        ParentFont = False
        Font.Height = -13
        object dbedtLORDO: TUniDBNumberEdit
          Left = 13
          Top = 16
          Width = 197
          Height = 22
          Hint = ''
          DataField = 'LORDO'
          DataSource = dsoEdit
          Alignment = taRightJustify
          ParentFont = False
          Font.Height = -13
          TabOrder = 1
          FieldLabel = 'Lordo'
          DecimalSeparator = ','
        end
        object dbedtnetto: TUniDBNumberEdit
          Left = 13
          Top = 44
          Width = 197
          Height = 22
          Hint = ''
          DataField = 'netto'
          DataSource = dsoEdit
          ParentFont = False
          Font.Height = -13
          TabOrder = 3
          FieldLabel = 'Netto'
          DecimalSeparator = ','
        end
        object dbedtptasse: TUniDBNumberEdit
          Left = 225
          Top = 16
          Width = 197
          Height = 22
          Hint = ''
          DataField = 'ptasse'
          DataSource = dsoEdit
          Alignment = taRightJustify
          ParentFont = False
          Font.Height = -13
          TabOrder = 2
          FieldLabel = '% tasse'
          FieldLabelWidth = 70
          DecimalSeparator = ','
        end
        object dbedttasse: TUniDBNumberEdit
          Left = 225
          Top = 44
          Width = 197
          Height = 22
          Hint = ''
          DataField = 'tasse'
          DataSource = dsoEdit
          Alignment = taRightJustify
          ParentFont = False
          Font.Height = -13
          TabOrder = 4
          FieldLabel = 'Tasse'
          FieldLabelWidth = 70
          DecimalSeparator = ','
        end
        object dbedtaccessori: TUniDBNumberEdit
          Left = 460
          Top = 44
          Width = 197
          Height = 22
          Hint = ''
          DataField = 'accessori'
          DataSource = dsoEdit
          Alignment = taRightJustify
          ParentFont = False
          Font.Height = -13
          TabOrder = 5
          FieldLabel = 'Accessori'
          FieldLabelWidth = 70
          DecimalSeparator = ','
        end
      end
      object ungrpbxDatiAnnullContratto: TUniGroupBox
        Left = 17
        Top = 444
        Width = 984
        Height = 60
        Hint = ''
        Caption = 'Dati annullamento contratto'
        TabOrder = 4
        ParentFont = False
        Font.Height = -13
        object dtpckDataMorte: TUniDBDateTimePicker
          Left = 460
          Top = 24
          Width = 197
          Hint = ''
          DataField = 'DATA_MORTE'
          DataSource = dsoEdit
          DateTime = 43755.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          ReadOnly = True
          TabOrder = 1
          ParentFont = False
          Font.Height = -13
          Color = clYellow
          FieldLabel = 'Annullato al'
          FieldLabelWidth = 70
        end
        object cbbCausaMorte: TUniDBLookupComboBox
          Left = 707
          Top = 24
          Width = 265
          Height = 24
          Hint = ''
          ListField = 'DESCRIZIONE'
          ListSource = DMdatiAge.DSCausali
          KeyField = 'COD_CAUSALE'
          ListFieldIndex = 0
          DataField = 'CAUSA_MORTE'
          DataSource = dsoEdit
          ParentFont = False
          Font.Height = -13
          TabOrder = 2
          ReadOnly = True
          Color = clYellow
          FieldLabel = 'Causale'
        end
        object uncbxModificaDatiAnnullamento: TUniCheckBox
          Left = 225
          Top = 27
          Width = 193
          Height = 17
          Hint = ''
          Caption = 'Modifica Dati Annullamento'
          TabOrder = 3
          OnClick = uncbxModificaDatiAnnullamentoClick
        end
      end
    end
    object pgGaranzie: TUniTabSheet
      Hint = ''
      Caption = 'Garanzie'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object grdGaranzie: TUniDBGrid
        Left = 0
        Top = 0
        Width = 1013
        Height = 518
        Hint = ''
        DataSource = dsGaranzie
        LoadMask.Message = 'Loading data...'
        Align = alClient
        TabOrder = 0
        Columns = <
          item
            FieldName = 'Decrizione'
            Title.Caption = 'Decrizione'
            Width = 195
          end
          item
            FieldName = 'Lordo'
            Title.Caption = 'Lordo'
            Width = 64
          end
          item
            FieldName = 'Tasse'
            Title.Caption = 'Tasse'
            Width = 64
          end
          item
            FieldName = 'SSN'
            Title.Caption = 'SSN'
            Width = 64
          end
          item
            FieldName = 'ValoreAss'
            Title.Caption = 'Valore ass.'
            Width = 64
          end
          item
            FieldName = 'PremioImponibile'
            Title.Caption = 'Pr. Imponibile'
            Width = 68
          end
          item
            FieldName = 'ValoreIF'
            Title.Caption = 'Valore I/F'
            Width = 64
          end
          item
            FieldName = 'Sigla'
            Title.Caption = 'Sigla'
            Width = 64
          end
          item
            FieldName = 'DataIngresso'
            Title.Caption = 'Dt Ingresso'
            Width = 64
          end
          item
            FieldName = 'Franchigia'
            Title.Caption = 'Franchigia'
            Width = 64
          end
          item
            FieldName = 'Massimale'
            Title.Caption = 'Massimale'
            Width = 64
          end
          item
            FieldName = 'PTasse'
            Title.Caption = 'Perc Tasse'
            Width = 64
          end>
      end
    end
    object pgAssicurati: TUniTabSheet
      Hint = ''
      Caption = 'Assicurati'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object pnlFilter: TUniSimplePanel
        Left = 0
        Top = 0
        Width = 1013
        Height = 41
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 0
        object rgGrpTipoMovimento: TUniRadioGroup
          Left = 13
          Top = -1
          Width = 267
          Height = 39
          Hint = ''
          Items.Strings = (
            'Tutti'
            'In garanzia'
            'Usciti')
          ItemIndex = 1
          Caption = 'Tipo movimento'
          TabOrder = 1
          Columns = 3
          OnClick = rgGrpTipoMovimentoClick
        end
        object btnDelOggGaranzia: TUniSpeedButton
          Left = 405
          Top = 9
          Width = 124
          Height = 23
          Action = actDelOggGaranzia
          ParentFont = False
          Font.Height = -13
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 2
        end
        object btnGeneraAppendice: TUniSpeedButton
          Left = 533
          Top = 9
          Width = 124
          Height = 23
          Action = actGeneraAppendice
          ParentFont = False
          Font.Height = -13
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 3
        end
        object btnInserisciAss: TUniSpeedButton
          Left = 286
          Top = 9
          Width = 115
          Height = 23
          Action = actInsOggGaranzia
          ParentFont = False
          Font.Height = -13
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 4
        end
      end
      object pnlGridAssicurati: TUniSimplePanel
        Left = 0
        Top = 41
        Width = 1013
        Height = 146
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 1
        object grdAssicurati: TUniDBGrid
          Left = 0
          Top = 0
          Width = 1013
          Height = 146
          Hint = ''
          DataSource = dsAssicurati
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgAutoRefreshRow]
          LoadMask.Message = 'Loading data...'
          Align = alClient
          TabOrder = 1
          OnSelectionChange = grdAssicuratiSelectionChange
          Columns = <
            item
              FieldName = 'OggettoAssicurato'
              Title.Caption = 'Oggetto assicurato'
              Width = 600
            end
            item
              FieldName = 'movim'
              Title.Caption = 'movim'
              Width = 64
            end>
        end
      end
      object unspltrAssicurati: TUniSplitter
        Left = 0
        Top = 187
        Width = 1013
        Height = 6
        Cursor = crVSplit
        Hint = ''
        Align = alTop
        ParentColor = False
        Color = clBtnFace
      end
      object pcOggettoAssicurato: TUniPageControl
        Left = 0
        Top = 231
        Width = 1013
        Height = 233
        Hint = ''
        ActivePage = pgVeicolo
        TabBarVisible = False
        Align = alClient
        TabOrder = 3
        ExplicitTop = 229
        object pgPatente: TUniTabSheet
          Hint = ''
          Caption = 'Patente'
          object cbbCategoriaPat: TUniDBComboBox
            Left = 303
            Top = 56
            Width = 170
            Hint = ''
            DataField = 'categoria_pat'
            DataSource = dsEditOggAssicurato
            Items.Strings = (
              'A'
              'B'
              'C'
              'D')
            TabOrder = 3
            FieldLabel = 'Categoria'
            FieldLabelWidth = 60
            IconItems = <>
          end
          object dbedtCodiceFiscale: TUniDBEdit
            Left = 483
            Top = 25
            Width = 328
            Height = 22
            Hint = ''
            DataField = 'cod_fisc_iva'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 1
            FieldLabel = 'Cod. fiscale'
            FieldLabelWidth = 60
          end
          object dbedtDenominazione: TUniDBEdit
            Left = 24
            Top = 25
            Width = 449
            Height = 22
            Hint = ''
            DataField = 'Denominazione'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 0
            FieldLabel = 'Nome'
            FieldLabelWidth = 60
          end
          object dbedtPatente: TUniDBEdit
            Left = 24
            Top = 56
            Width = 270
            Height = 22
            Hint = ''
            DataField = 'patente'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 2
            FieldLabel = 'Patente'
            FieldLabelWidth = 60
          end
          object dtpckDataRilascioPat: TUniDBDateTimePicker
            Left = 24
            Top = 87
            Width = 270
            Hint = ''
            DataField = 'data_rilascio'
            DataSource = dsEditOggAssicurato
            DateTime = 43922.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 4
            FieldLabel = 'Rilasciata il'
            FieldLabelWidth = 60
          end
          object dtpckDataScadenzaPat: TUniDBDateTimePicker
            Left = 483
            Top = 87
            Width = 294
            Hint = ''
            DataField = 'data_scadenza'
            DataSource = dsEditOggAssicurato
            DateTime = 43922.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 5
            FieldLabel = 'Scadenza'
            FieldLabelWidth = 60
          end
        end
        object pgVeicolo: TUniTabSheet
          Hint = ''
          TabVisible = False
          Caption = 'Veicolo'
          object cbbTipoVeicolo: TUniDBLookupComboBox
            Left = 9
            Top = 25
            Width = 270
            Hint = ''
            ListField = 'descrizione'
            ListSource = dsTipoVeicolo
            KeyField = 'sigla'
            ListFieldIndex = 0
            DataField = 'tipo_veicolo'
            DataSource = dsEditOggAssicurato
            ParentFont = False
            TabOrder = 0
            Color = clWindow
            FieldLabel = 'Tipo veicolo'
            FieldLabelWidth = 80
          end
          object dbedtMarca: TUniDBEdit
            Left = 288
            Top = 25
            Width = 180
            Height = 22
            Hint = ''
            DataField = 'marca'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 1
            FieldLabel = 'Marca'
            FieldLabelWidth = 80
          end
          object dbedtModello: TUniDBEdit
            Left = 476
            Top = 25
            Width = 242
            Height = 22
            Hint = ''
            DataField = 'modello'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 2
            FieldLabel = 'Modello'
            FieldLabelWidth = 80
          end
          object dbedtTarga: TUniDBEdit
            Left = 9
            Top = 53
            Width = 270
            Height = 22
            Hint = ''
            DataField = 'Targa'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 4
            FieldLabel = 'Targa'
            FieldLabelWidth = 80
          end
          object dbedtTelaio: TUniDBEdit
            Left = 288
            Top = 53
            Width = 180
            Height = 22
            Hint = ''
            DataField = 'Telaio'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 5
            FieldLabel = 'Telaio'
            FieldLabelWidth = 80
          end
          object dbedtQlHp: TUniDBEdit
            Left = 476
            Top = 53
            Width = 242
            Height = 22
            Hint = ''
            DataField = 'hp_ql'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 6
            FieldLabel = 'Ql/Hp'
            FieldLabelWidth = 80
          end
          object dtpckScadenzaRevisione: TUniDBDateTimePicker
            Left = 727
            Top = 53
            Width = 270
            Hint = ''
            Visible = False
            DataField = 'data_scadenza'
            DataSource = dsEditOggAssicurato
            DateTime = 43922.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 7
            FieldLabel = 'Scadenza'
            FieldLabelWidth = 80
          end
          object dtpckDataRilascio: TUniDBDateTimePicker
            Left = 727
            Top = 25
            Width = 270
            Hint = ''
            Visible = False
            DataField = 'data_rilascio'
            DataSource = dsEditOggAssicurato
            DateTime = 43922.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 3
            FieldLabel = 'Data rilascio'
            FieldLabelWidth = 80
          end
          object cbbTipoAlimentazione: TUniDBComboBox
            Left = 9
            Top = 82
            Width = 270
            Hint = ''
            DataField = 'tipo_alimentazione'
            DataSource = dsEditOggAssicurato
            Items.Strings = (
              'Benzina'
              'Gasolio'
              'Misto'
              'Elettrico')
            TabOrder = 8
            FieldLabel = 'Tipo alimentazione'
            FieldLabelWidth = 80
            ClearButton = True
            IconItems = <>
          end
          object ckbxAziendale: TUniDBCheckBox
            Left = 288
            Top = 85
            Width = 249
            Height = 17
            Hint = ''
            Caption = 'Veicolo Aziendale guidato anche da dipendenti'
            TabOrder = 9
            ParentColor = False
            Color = clWindow
          end
          object grpbxDatiPra: TUniGroupBox
            Left = 3
            Top = 114
            Width = 999
            Height = 83
            Hint = ''
            Caption = 'Intestatario PRA'
            TabOrder = 10
            object dbedtPraNome: TUniDBEdit
              Left = 6
              Top = 19
              Width = 459
              Height = 22
              Hint = ''
              DataField = 'PRA_denominazione'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              TabOrder = 1
              FieldLabel = 'Nome'
              FieldLabelWidth = 50
            end
            object dbedtPraCodFisc: TUniDBEdit
              Left = 473
              Top = 19
              Width = 350
              Height = 22
              Hint = ''
              DataField = 'PRA_cod_fisc_iva'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              TabOrder = 2
              FieldLabel = 'Codice fiscale'
              FieldLabelWidth = 80
            end
            object dbedtPraIndirizzo: TUniDBEdit
              Left = 6
              Top = 47
              Width = 459
              Height = 22
              Hint = ''
              DataField = 'PRA_indirizzo'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 3
              FieldLabel = 'Indirizzo'
              FieldLabelWidth = 50
            end
            object dbedtPraCitta: TUniDBEdit
              Left = 473
              Top = 47
              Width = 242
              Height = 22
              Hint = ''
              DataField = 'PRA_citta'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 4
              FieldLabel = 'Citt'#224
              FieldLabelWidth = 80
            end
            object dbedtPraCap: TUniDBEdit
              Left = 723
              Top = 47
              Width = 121
              Height = 22
              Hint = ''
              DataField = 'PRA_cap'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 5
              FieldLabel = 'Cap'
              FieldLabelWidth = 30
            end
            object dbedtPraProvincia: TUniDBEdit
              Left = 850
              Top = 47
              Width = 94
              Height = 22
              Hint = ''
              DataField = 'PRA_prov'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 6
              FieldLabel = 'Provincia'
              FieldLabelWidth = 50
            end
          end
        end
        object pgPersona: TUniTabSheet
          Hint = ''
          TabVisible = False
          Caption = 'Persona'
          object dbedtPersNome: TUniDBEdit
            Left = 14
            Top = 27
            Width = 459
            Height = 22
            Hint = ''
            DataField = 'denominazione'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 0
            FieldLabel = 'Nome'
            FieldLabelWidth = 50
          end
          object dbedtPersCodFisc: TUniDBEdit
            Left = 479
            Top = 27
            Width = 350
            Height = 22
            Hint = ''
            DataField = 'cod_fisc_iva'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 1
            FieldLabel = 'Codice fiscale'
            FieldLabelWidth = 80
          end
          object dbedtPersIndirizzo: TUniDBEdit
            Left = 14
            Top = 54
            Width = 459
            Height = 22
            Hint = ''
            DataField = 'indirizzo'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 2
            FieldLabel = 'Indirizzo'
            FieldLabelWidth = 50
          end
          object dbedtPersCitta: TUniDBEdit
            Left = 479
            Top = 55
            Width = 242
            Height = 22
            Hint = ''
            DataField = 'CITTA'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 3
            FieldLabel = 'Citt'#224
            FieldLabelWidth = 80
          end
          object dbedtPersCAP: TUniDBEdit
            Left = 730
            Top = 55
            Width = 121
            Height = 22
            Hint = ''
            DataField = 'CAP'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 4
            FieldLabel = 'Cap'
            FieldLabelWidth = 30
          end
          object dbedtPersProvincia: TUniDBEdit
            Left = 857
            Top = 55
            Width = 94
            Height = 22
            Hint = ''
            DataField = 'PROV'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 5
            FieldLabel = 'Provincia'
            FieldLabelWidth = 50
          end
        end
        object pgContratto: TUniTabSheet
          Hint = ''
          Caption = 'Contratto'
          object dbedtConNPolizza: TUniDBEdit
            Left = 14
            Top = 25
            Width = 241
            Height = 22
            Hint = ''
            DataField = 'cod_fisc_iva'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 0
            FieldLabel = 'N'#176' Contratto'
          end
          object dbedtConCompagnia: TUniDBEdit
            Left = 264
            Top = 25
            Width = 350
            Height = 22
            Hint = ''
            DataField = 'citta'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 1
            FieldLabel = 'Compagnia'
          end
          object dbedtConImporto: TUniDBFormattedNumberEdit
            Left = 639
            Top = 53
            Width = 218
            Height = 22
            Hint = ''
            DataField = 'cifra'
            DataSource = dsEditOggAssicurato
            Alignment = taRightJustify
            TabOrder = 5
            FieldLabel = 'Importo'
            DecimalSeparator = ','
            ThousandSeparator = '.'
          end
          object dtpckConDecorrenza: TUniDBDateTimePicker
            Left = 14
            Top = 55
            Width = 241
            Hint = ''
            DataField = 'data_rilascio'
            DataSource = dsEditOggAssicurato
            DateTime = 43922.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 3
            FieldLabel = 'Decorrenza'
          end
          object dtpckConScadenza: TUniDBDateTimePicker
            Left = 264
            Top = 55
            Width = 241
            Hint = ''
            DataField = 'data_scadenza'
            DataSource = dsEditOggAssicurato
            DateTime = 43922.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 4
            FieldLabel = 'Scadenza'
          end
          object dbedtConAgenzia: TUniDBEdit
            Left = 639
            Top = 23
            Width = 350
            Height = 24
            Hint = ''
            DataField = 'indirizzo'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 2
            FieldLabel = 'Em. da Agenzia'
          end
          object dbedtConGaranzia: TUniDBEdit
            Left = 14
            Top = 84
            Width = 491
            Height = 22
            Hint = ''
            DataField = 'denominazione'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 6
            FieldLabel = 'Garanzie polizza base'
          end
          object dbedtConGarPrescelte: TUniDBEdit
            Left = 639
            Top = 86
            Width = 308
            Height = 22
            Hint = ''
            DataField = 'modello'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 7
            FieldLabel = 'Garanzie prescelte (A/B/C)'
          end
        end
        object pgAltro: TUniTabSheet
          Hint = ''
          TabVisible = False
          Caption = 'Altro'
          object dbedtAltDescrizione: TUniDBEdit
            Left = 14
            Top = 25
            Width = 459
            Height = 22
            Hint = ''
            DataField = 'denominazione'
            DataSource = dsEditOggAssicurato
            CharCase = ecUpperCase
            TabOrder = 0
            FieldLabel = 'Descrizione'
            FieldLabelWidth = 80
          end
          object ungrpbxUbicazione: TUniGroupBox
            Left = 6
            Top = 60
            Width = 979
            Height = 63
            Hint = ''
            Caption = 'Ubicazione oggetto in garanzia'
            TabOrder = 1
            object dbedtAltINDIRIZZO: TUniDBEdit
              Left = 8
              Top = 23
              Width = 459
              Height = 22
              Hint = ''
              DataField = 'INDIRIZZO'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 1
              FieldLabel = 'Indirizzo'
              FieldLabelWidth = 80
            end
            object dbedtAltCITTA: TUniDBEdit
              Left = 473
              Top = 23
              Width = 242
              Height = 22
              Hint = ''
              DataField = 'CITTA'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 2
              FieldLabel = 'Citt'#224
              FieldLabelWidth = 60
            end
            object dbedtAltCAP: TUniDBEdit
              Left = 724
              Top = 23
              Width = 121
              Height = 22
              Hint = ''
              DataField = 'CAP'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 3
              FieldLabel = 'Cap'
              FieldLabelWidth = 30
            end
            object dbedtAltPROV: TUniDBEdit
              Left = 851
              Top = 23
              Width = 94
              Height = 22
              Hint = ''
              DataField = 'PROV'
              DataSource = dsEditOggAssicurato
              CharCase = ecUpperCase
              ParentFont = False
              TabOrder = 4
              FieldLabel = 'Provincia'
              FieldLabelWidth = 50
            end
          end
        end
      end
      object pnlButtonsEditAss: TUniSimplePanel
        Left = 0
        Top = 464
        Width = 1013
        Height = 54
        Hint = ''
        Visible = False
        ParentColor = False
        Align = alBottom
        TabOrder = 4
        object btnBconferma: TUniBitBtn
          Left = 3
          Top = 9
          Width = 233
          Height = 35
          Hint = ''
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000202020404040
            4040404040404040404040404040404040404040404040404040404040404040
            404040404040402020207F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
            C0C0C0C0C0C0909090808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0309030008000608060808080B0
            B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
            30CF30008000008000008000608060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800000800000800060
            8060808080B0B0B0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
            00BF00008000008000008000008000008000608060909090C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C04040407F7F7FC0C0C000BF0000800000800020DF20009F0000
            8000208020808080A0A0A0C0C0C0C0C0C0C0C0C0C0C0C04040407F7F7FC0C0C0
            90CF9000BF0000800080808030EF30009F00008000208020808080B0B0B0C0C0
            C0C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C090CF9000DF00A0A0A0C0C0C030
            EF30009F00008000608060808080B0B0B0C0C0C0C0C0C04040407F7F7FC0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F000080006080609090
            90C0C0C0C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C030EF30009F00208020808080A0A0A0C0C0C04040407F7F7FC0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C030EF30009F002080
            20808080C0C0C04040407F7F7FC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
            C0C0C0C0C0C0C0C0C0C0C030EF30009F00608060C0C0C04040407F7F7FC0C0C0
            C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C060DF
            6090B090C0C0C04040403F3F3F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F
            7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F202020}
          Caption = ' Conferma dati assicurato'
          ParentFont = False
          Font.Height = -13
          Font.Style = [fsBold]
          TabOrder = 1
          OnClick = btnBconfermaClick
        end
        object btnBabbandona: TUniBitBtn
          Left = 247
          Top = 9
          Width = 120
          Height = 35
          Hint = ''
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF404040606060808080808080808080606060404040FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF20202020206020209F0000BF00
            00BF0000BF20209F404080808080606060202020FFFFFFFFFFFFFFFFFFFFFFFF
            00002020209F0000FF0000FF0000FF0000FF0000FF0000FF0000FF20209F6060
            80808080202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
            00FF0000FF0000FF0000FF0000FF0000DF606080606060FFFFFFFFFFFF00007F
            0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
            FF20209F8080804040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
            00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF40408060606000007F0000FF
            0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
            FF0000FF20209F8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
            FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF8080800000BF0000FF
            0000FF0000FF0000FF0000FFFFFFFFFFFFFFFFFFFF0000FF0000FF0000FF0000
            FF0000FF0000BF8080800000BF0000FF0000FF0000FF0000FFBFBFFFFFFFFFFF
            FFFFFFFFFFBFBFFF0000FF0000FF0000FF0000FF0000BF60606000007F0000FF
            0000FF0000FFBFBFFFFFFFFFBFBFFF0000FFBFBFFFFFFFFFBFBFFF0000FF0000
            FF0000FF20209F4040400000400000FF0000FF3F3FFFFFFFFFBFBFFF0000FF00
            00FF0000FFBFBFFFFFFFFF3F3FFF0000FF0000FF202060FFFFFFFFFFFF00007F
            0000FF0000FF3F3FFF0000FF0000FF0000FF0000FF0000FF3F3FFF0000FF0000
            FF20209F202020FFFFFFFFFFFF0000200000DF0000FF0000FF0000FF0000FF00
            00FF0000FF0000FF0000FF0000FF0000DF000020FFFFFFFFFFFFFFFFFFFFFFFF
            00002000007F0000FF0000FF0000FF0000FF0000FF0000FF0000FF00007F0000
            20FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00004000007F0000BF00
            00BF0000BF00007F000040FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          Caption = ' Abbandona'
          ParentFont = False
          Font.Height = -13
          Font.Style = [fsBold]
          TabOrder = 2
          OnClick = btnBabbandonaClick
        end
      end
      object pnlTipoOggAssicurato: TUniPanel
        Left = 0
        Top = 193
        Width = 1013
        Height = 38
        Hint = ''
        Align = alTop
        TabOrder = 5
        Caption = ''
        object rgGrpTipoOggAssicurato: TUniRadioGroup
          Left = 325
          Top = -2
          Width = 400
          Height = 37
          Hint = ''
          Items.Strings = (
            'Patente'
            'Veicolo'
            'Persona'
            'Contratto'
            'Altro')
          ItemIndex = 0
          Caption = 'Tipo Oggetto Assicurato'
          TabOrder = 1
          ParentColor = False
          Color = clWindow
          Columns = 5
          OnClick = rgGrpTipoOggAssicuratoClick
        end
        object dtpckDataIngresso: TUniDBDateTimePicker
          Left = 28
          Top = 6
          Width = 287
          Hint = ''
          DataField = 'entrata'
          DataSource = dsEditOggAssicurato
          DateTime = 43922.000000000000000000
          DateFormat = 'dd/MM/yyyy'
          TimeFormat = 'HH:mm:ss'
          TabOrder = 2
          FieldLabel = 'Data Ingresso'
          FieldLabelWidth = 85
        end
        object lbl1: TUniLabel
          Left = 792
          Top = 8
          Width = 16
          Height = 13
          Hint = ''
          Visible = False
          Caption = 'lbl1'
          TabOrder = 3
        end
      end
    end
    object pgNota: TUniTabSheet
      Hint = ''
      Caption = 'Note'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object undbmNOTE: TUniDBMemo
        Left = 0
        Top = 0
        Width = 1013
        Height = 518
        Hint = ''
        DataField = 'NOTE'
        DataSource = dsoEdit
        ParentFont = False
        Align = alClient
        TabOrder = 0
      end
    end
    object pgTitoliScadenze: TUniTabSheet
      Hint = ''
      Caption = 'Titoli / Scadenze'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object grdTitScadenze: TUniDBGrid
        Left = 0
        Top = 41
        Width = 1013
        Height = 477
        Hint = ''
        DataSource = dsScadenze
        LoadMask.Message = 'Loading data...'
        Align = alClient
        Anchors = []
        TabOrder = 0
        OnDblClick = grdTitScadenzeDblClick
        Columns = <
          item
            FieldName = 'Polizza'
            Title.Caption = 'Polizza'
            Width = 64
          end
          item
            FieldName = 'Decorrenza'
            Title.Caption = 'Decorrenza'
            Width = 70
          end
          item
            FieldName = 'Lordo'
            Title.Caption = 'Lordo'
            Width = 64
          end
          item
            FieldName = 'DataPagamento'
            Title.Caption = 'Data pag.'
            Width = 64
          end
          item
            FieldName = 'Fraz'
            Title.Caption = 'Fraz'
            Width = 64
          end
          item
            FieldName = 'TipoTitolo'
            Title.Caption = 'Tipo Tit.'
            Width = 64
          end
          item
            FieldName = 'NumeroFC'
            Title.Caption = 'Numero FC'
            Width = 70
          end
          item
            FieldName = 'DataAnnullamento'
            Title.Caption = 'Data ann.'
            Width = 64
          end
          item
            FieldName = 'CausaleAnnullamento'
            Title.Caption = 'Causale ann.'
            Width = 70
          end>
      end
      object UniSimplePanel1: TUniSimplePanel
        Left = 0
        Top = 0
        Width = 1013
        Height = 41
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 1
        object rgGrpStatoTitoli: TUniRadioGroup
          Left = 17
          Top = -1
          Width = 600
          Height = 39
          Hint = ''
          Items.Strings = (
            'In giacenza'
            'In giacenza + storico'
            'In giacenza + storico + annullati')
          ItemIndex = 0
          Caption = 'Stato titoli'
          TabOrder = 1
          Columns = 3
          OnClick = rgGrpStatoTitoliClick
        end
      end
    end
    object pgAppendici: TUniTabSheet
      Hint = ''
      Caption = 'Appendici di variazione'
      ClientEvents.ExtEvents.Strings = (
        
          'afterrender=function afterrender(sender, eOpts)'#13#10'{'#13#10'   sender.ta' +
          'b.btnInnerEl.setStyle("fontSize", "12");'#13#10'}')
      object pnlBtnlAppendici: TUniSimplePanel
        Left = 0
        Top = 0
        Width = 1013
        Height = 41
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 0
        object btnDelAppendice: TUniSpeedButton
          Left = 17
          Top = 2
          Width = 128
          Height = 38
          Visible = False
          Action = actDelAppendice
          ParentFont = False
          Font.Height = -13
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 1
        end
        object btnRistampaAppendice: TUniSpeedButton
          Left = 148
          Top = 2
          Width = 132
          Height = 38
          Action = actRistampaAppendice
          ParentFont = False
          Font.Height = -13
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 2
        end
      end
      object pnlPnlAppendici: TUniPanel
        Left = 0
        Top = 41
        Width = 1013
        Height = 208
        Hint = ''
        Align = alTop
        TabOrder = 1
        Caption = ''
        object grdAppendici: TUniDBGrid
          Left = 1
          Top = 1
          Width = 1011
          Height = 206
          Hint = ''
          DataSource = dsAppendice
          LoadMask.Message = 'Loading data...'
          Align = alClient
          TabOrder = 1
          Columns = <
            item
              FieldName = 'numero'
              Title.Caption = 'Numero'
              Width = 64
            end
            item
              FieldName = 'data_emissione'
              Title.Caption = 'Data emissione'
              Width = 64
            end
            item
              FieldName = 'n_polizza'
              Title.Caption = 'N polizza'
              Width = 64
            end
            item
              FieldName = 'DAL'
              Title.Caption = 'Dal'
              Width = 64
            end
            item
              FieldName = 'AL'
              Title.Caption = 'Al'
              Width = 64
            end
            item
              FieldName = 'descrizione'
              Title.Caption = 'Descrizione'
              Width = 70
            end
            item
              FieldName = 'compagnia'
              Title.Caption = 'Compagnia'
              Width = 70
            end
            item
              FieldName = 'IMPORTO'
              Title.Caption = 'Importo'
              Width = 64
            end
            item
              FieldName = 'NEXT_RATA'
              Title.Caption = 'Next rata'
              Width = 64
            end>
        end
      end
      object unspltrApp: TUniSplitter
        Left = 0
        Top = 249
        Width = 1013
        Height = 6
        Cursor = crVSplit
        Hint = ''
        Align = alTop
        ParentColor = False
        Color = clBtnFace
      end
      object pnlDettAppendici: TUniPanel
        Left = 0
        Top = 255
        Width = 1013
        Height = 263
        Hint = ''
        Align = alClient
        TabOrder = 3
        Caption = ''
        object grdDettAppendici: TUniDBGrid
          Left = 1
          Top = 1
          Width = 1011
          Height = 261
          Hint = ''
          DataSource = dsDettAppendice
          LoadMask.Message = 'Loading data...'
          Align = alClient
          TabOrder = 1
          Columns = <
            item
              FieldName = 'garanziaTit'
              Title.Caption = 'In garanzia Tit.'
              Width = 710
            end>
        end
      end
    end
  end
  inherited actlstEditOperation: TActionList
    inherited actConferma: TAction
      Caption = 'Conferma dati polizza'
    end
    object actSelezionaReferente: TAction
      Category = 'OpReferente'
      OnExecute = actSelezionaReferenteExecute
    end
    object actDelReferente: TAction
      Category = 'OpReferente'
      Caption = 'actDelReferente'
      OnExecute = actDelReferenteExecute
    end
    object actSelezionaContraente: TAction
      Category = 'OpContraente'
      OnExecute = actSelezionaContraenteExecute
    end
    object actSelezionaCollaboratore: TAction
      Category = 'OpCollaboratore'
      OnExecute = actSelezionaCollaboratoreExecute
    end
    object actInsOggGaranzia: TAction
      Category = 'OggettoGaranzia'
      Caption = 'Ins Ogg Garanzia'
      OnExecute = actInsOggGaranziaExecute
    end
    object actModOggGaranzia: TAction
      Category = 'OggettoGaranzia'
      Caption = 'Mod Ogg Garanzia'
    end
    object actDelOggGaranzia: TAction
      Category = 'OggettoGaranzia'
      Caption = 'Togli Ogg Garanzia'
      OnExecute = actDelOggGaranziaExecute
    end
    object actGeneraAppendice: TAction
      Category = 'Appendici'
      Caption = 'Genera Appendice'
      OnExecute = actGeneraAppendiceExecute
    end
    object actDelAppendice: TAction
      Category = 'Appendici'
      Caption = 'Cancella Appendice'
    end
    object actRistampaAppendice: TAction
      Category = 'Appendici'
      Caption = 'Ristampa Appendice'
      OnExecute = actRistampaAppendiceExecute
    end
  end
  inherited dsoEdit: TDataSource
    DataSet = DMdatiAgePolizze.QPolizzaEdit
  end
  inherited untvmglstIcons: TUniNativeImageList
    Left = 852
    Top = 102
  end
  object dsGaranzie: TDataSource
    DataSet = DMdatiAgePolizze.QgaranziePolizza
    Left = 652
    Top = 93
  end
  object dsAssicurati: TDataSource
    DataSet = DMdatiAgePolizze.QAssicuratiPolizza
    Left = 760
    Top = 88
  end
  object dsAppendice: TDataSource
    DataSet = DMdatiAgePolizze.Qappendice
    Left = 268
    Top = 174
  end
  object dsDettAppendice: TDataSource
    DataSet = DMdatiAgePolizze.QdettAppendice
    Left = 428
    Top = 196
  end
  object dsEditOggAssicurato: TDataSource
    DataSet = DMdatiAgePolizze.QAssicuratiPolizzaEdit
    Left = 408
    Top = 300
  end
  object dsScadenze: TDataSource
    DataSet = DMdatiAgePolizze.QtitoliPolizza
    Left = 604
    Top = 190
  end
  object dsTipoVeicolo: TDataSource
    DataSet = DMdatiAge.Qtipo_veicolo
    Left = 256
    Top = 313
  end
end
