unit UAvvisoAgenzia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, uniMemo, uniDBMemo, uniButton, uniBitBtn, uniSpeedButton, uniGUIBaseClasses,
  uniPanel, uniGUIApplication, dbisamtb, uniGUIDialogs;

type
  TFAvviso = class(TUniForm)
    undbhtmlmAvvisoAge: TUniDBHTMLMemo;
    pnlButtons: TUniSimplePanel;
    btnEsci: TUniSpeedButton;
    dsAvvisoAge: TDataSource;
    btnNextMessage: TUniSpeedButton;
    btnPriorMessage: TUniSpeedButton;
    procedure btnEsciClick(Sender: TObject);
    procedure btnNextMessageClick(Sender: TObject);
    procedure btnPriorMessageClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    nAvvisiLetti: Integer;
  public
    { Public declarations }
    class procedure ShowAvviso(UniApplication: TUniGUIApplication; AQyAvvisi: TDBISAMQuery;
      CallBackProc: TUniDialogCallBackProc); overload;

    class procedure ShowAvviso(UniApplication: TUniGUIApplication; AQyAvvisi: TDBISAMQuery); overload;

  end;

implementation

uses
  MainModule;

{$R *.dfm}

procedure TFAvviso.btnEsciClick(Sender: TObject);
begin
  Close;
end;

procedure TFAvviso.btnNextMessageClick(Sender: TObject);
begin
  if not dsAvvisoAge.DataSet.Eof then
  begin
    dsAvvisoAge.DataSet.Next;
    UniMainModule.DMDatiAgeAvvisi.UpdDateMessaggio(dsAvvisoAge.DataSet.FieldByName('COD_MSG').AsInteger);
    if dsAvvisoAge.DataSet.RecNo = dsAvvisoAge.DataSet.recordcount then
      btnEsci.Enabled := True;
  end;
end;

procedure TFAvviso.btnPriorMessageClick(Sender: TObject);
begin
  if not dsAvvisoAge.DataSet.Bof then
    dsAvvisoAge.DataSet.Prior;

end;

class procedure TFAvviso.ShowAvviso(UniApplication: TUniGUIApplication; AQyAvvisi: TDBISAMQuery);
begin
  with TFAvviso.Create(UniApplication) do
  begin
    dsAvvisoAge.DataSet := AQyAvvisi;
    UniMainModule.DMDatiAgeAvvisi.UpdDateMessaggio(dsAvvisoAge.DataSet.FieldByName('COD_MSG').AsInteger);

    btnEsci.Enabled         := True;
    btnNextMessage.Visible  := not btnEsci.Enabled;
    btnPriorMessage.Visible := not btnEsci.Enabled;

    ShowModal;
  end;

end;

class procedure TFAvviso.ShowAvviso(UniApplication: TUniGUIApplication; AQyAvvisi: TDBISAMQuery;
  CallBackProc: TUniDialogCallBackProc);
begin
  with TFAvviso.Create(UniApplication) do
  begin
    dsAvvisoAge.DataSet := AQyAvvisi;
    UniMainModule.DMDatiAgeAvvisi.UpdDateMessaggio(dsAvvisoAge.DataSet.FieldByName('COD_MSG').AsInteger);
    btnEsci.Enabled         := nAvvisiLetti = dsAvvisoAge.DataSet.recordcount;
    btnNextMessage.Enabled  := not btnEsci.Enabled;
    btnPriorMessage.Enabled := not btnEsci.Enabled;

    ShowModal(CallBackProc);
  end;
end;

procedure TFAvviso.UniFormCreate(Sender: TObject);
begin
  nAvvisiLetti := 1;
end;

end.
