unit UUtenti;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterElenco, Data.DB, System.Actions, Vcl.ActnList, uniButton,
  uniBitBtn, uniSpeedButton, uniPanel, uniBasicGrid, uniDBGrid, uniLabel, uniGUIBaseClasses, uniEdit;

type
  TFUtenti = class(TFMasterElenco)
    unedtNome: TUniEdit;
    unedtSigla: TUniEdit;
    unedtLivello: TUniEdit;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function FUtenti: TFUtenti;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UQueryElencoIntf;

function FUtenti: TFUtenti;
begin
  Result := TFUtenti(UniMainModule.GetFormInstance(TFUtenti));
end;

procedure TFUtenti.UniFormCreate(Sender: TObject);
begin
  UniMainModule.archivio := 'UTE';
  inherited;
  self.DMMaster  := (UniMainModule.DMdatiAgeUtenti as IQueryElenco);
  dsGrid.DataSet := UniMainModule.DMdatiAgeUtenti.QUtenti;
  self.DMMaster.EseguiQuery('NOME');
  EditPageClassName := 'TFUtenteEdit';

end;

end.
