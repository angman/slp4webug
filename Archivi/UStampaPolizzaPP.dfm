inherited FStampaPolizzaPP: TFStampaPolizzaPP
  Caption = 'Perizie di parte'
  ScreenMask.Target = Owner
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlEditButtons: TUniSimplePanel
    inherited btnConferma: TUniSpeedButton
      ScreenMask.Target = Owner
    end
    inherited btnAnteprimaStampa: TUniSpeedButton
      ScreenMask.Target = Owner
    end
  end
  inherited pnlDatiSpecificiPolizza: TUniPanel
    inherited pcDatiEreditabili: TUniPageControl
      ActivePage = pgGaranzie
      OnChange = pcDatiEreditabiliChange
      object pgGaranzie: TUniTabSheet
        Hint = ''
        Caption = 'Polizze RE in garanzia e massimali'
        ParentFont = False
        object pnlGrid: TUniPanel
          Left = 0
          Top = 0
          Width = 276
          Height = 324
          Hint = ''
          Align = alLeft
          TabOrder = 0
          BorderStyle = ubsNone
          Caption = 'pnlGrid'
          object grdPolizzeInGaranzia: TUniDBGrid
            Left = 0
            Top = 43
            Width = 276
            Height = 248
            Hint = ''
            DataSource = dsAssicurato
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgAutoRefreshRow, dgRowNumbers]
            WebOptions.Paged = False
            LoadMask.Message = 'Loading data...'
            TabOrder = 1
            Columns = <
              item
                FieldName = 'CodFiscIvas'
                Title.Caption = 'N. Polizza'
                Width = 95
                ReadOnly = True
              end
              item
                FieldName = 'Citta'
                Title.Caption = 'Comp. Emittente'
                Width = 146
                ReadOnly = True
              end>
          end
          object btnInsert: TUniSpeedButton
            Left = 8
            Top = 7
            Width = 81
            Height = 28
            Action = actInsGaranzia
            ParentFont = False
            Font.Style = [fsBold]
            ParentColor = False
            Color = clWindow
            LayoutConfig.Flex = 1
            TabOrder = 2
          end
          object btnDelGaranzia: TUniSpeedButton
            Left = 183
            Top = 7
            Width = 83
            Height = 28
            Action = actDelGaranzia
            ParentFont = False
            Font.Style = [fsBold]
            ParentColor = False
            Color = clWindow
            LayoutConfig.Flex = 1
            TabOrder = 3
          end
          object btnModPolizza: TUniSpeedButton
            Left = 94
            Top = 7
            Width = 83
            Height = 28
            Action = actModGaranzia
            ParentFont = False
            Font.Style = [fsBold]
            ParentColor = False
            Color = clWindow
            LayoutConfig.Flex = 1
            TabOrder = 4
          end
          object ckbxPolizzaIndicizzata: TUniDBCheckBox
            Left = 48
            Top = 299
            Width = 206
            Height = 17
            Hint = ''
            DataField = 'Indicizzata'
            DataSource = dsoEdit
            Caption = 'INDICIZZATA'
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 5
            ParentColor = False
            Color = clBtnFace
          end
        end
        object unspltr: TUniSplitter
          Left = 276
          Top = 0
          Width = 6
          Height = 324
          Hint = ''
          Align = alLeft
          ParentColor = False
          Color = clBtnFace
        end
        object pnlEdit: TUniPanel
          Left = 282
          Top = 0
          Width = 620
          Height = 324
          Hint = ''
          Align = alClient
          ParentFont = False
          TabOrder = 2
          BorderStyle = ubsNone
          Caption = ''
          object dbedtNPolizza: TUniDBEdit
            Left = 6
            Top = 43
            Width = 200
            Height = 22
            Hint = ''
            DataField = 'CodFiscIvas'
            DataSource = dsAssicurato
            CharCase = ecUpperCase
            ParentFont = False
            TabOrder = 1
            FieldLabel = 'Polizza N.'
            FieldLabelWidth = 70
          end
          object dbedtIndirizzoGar: TUniDBEdit
            Left = 6
            Top = 71
            Width = 200
            Height = 22
            Hint = ''
            DataField = 'Indirizzo'
            DataSource = dsAssicurato
            CharCase = ecUpperCase
            TabOrder = 3
            FieldLabel = 'Agenzia di'
            FieldLabelWidth = 70
          end
          object dtpckDecorrenza: TUniDBDateTimePicker
            Left = 238
            Top = 71
            Width = 160
            Height = 26
            Hint = ''
            DataField = 'DataRilascio'
            DataSource = dsAssicurato
            DateTime = 43874.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 4
            FieldLabel = 'Decorrenza'
            FieldLabelWidth = 70
          end
          object dbedtCompagniaEmittente: TUniDBEdit
            Left = 298
            Top = 43
            Width = 241
            Height = 22
            Hint = ''
            DataField = 'Citta'
            DataSource = dsAssicurato
            CharCase = ecUpperCase
            TabOrder = 2
            FieldLabel = 'Emessa da Comp'
          end
          object dbedtPremioAnnuoLordo: TUniDBNumberEdit
            Left = 366
            Top = 189
            Width = 160
            Height = 22
            Hint = ''
            DataField = 'PremioAnnuoLordo'
            DataSource = dsAssicurato
            ParentFont = False
            TabOrder = 11
            SelectOnFocus = True
            FieldLabel = 'Premio annuo lordo pol.base'
            FieldLabelWidth = 180
            FieldLabelAlign = laTop
            DecimalSeparator = ','
          end
          object dtpckScadenza: TUniDBDateTimePicker
            Left = 410
            Top = 71
            Width = 150
            Height = 26
            Hint = ''
            DataField = 'DataScadenza'
            DataSource = dsAssicurato
            DateTime = 43874.000000000000000000
            DateFormat = 'dd/MM/yyyy'
            TimeFormat = 'HH:mm:ss'
            TabOrder = 5
            FieldLabel = 'Scadenza'
            FieldLabelWidth = 60
          end
          object ckbx_C: TUniDBCheckBox
            Left = 247
            Top = 108
            Width = 122
            Height = 17
            Hint = ''
            DataField = 'GaranziaC'
            DataSource = dsAssicurato
            ValueChecked = 'true'
            ValueUnchecked = 'false'
            Caption = 'C - Difesa Penale'
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 8
            ParentColor = False
            Color = clBtnFace
            OnClick = ckbx_CClick
          end
          object ckbx_B: TUniDBCheckBox
            Left = 97
            Top = 108
            Width = 144
            Height = 17
            Hint = ''
            DataField = 'GaranziaB'
            DataSource = dsAssicurato
            ValueChecked = 'true'
            ValueUnchecked = 'false'
            Caption = 'B - Chiamata in causa'
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 7
            ParentColor = False
            Color = clBtnFace
            OnClick = ckbx_CClick
          end
          object ckbx_A: TUniDBCheckBox
            Left = 11
            Top = 108
            Width = 83
            Height = 17
            Hint = ''
            DataField = 'GaranziaA'
            DataSource = dsAssicurato
            ValueChecked = 'true'
            ValueUnchecked = 'false'
            Caption = 'A - Perizie'
            ParentFont = False
            Font.Style = [fsBold]
            TabOrder = 6
            ParentColor = False
            Color = clBtnFace
            OnClick = ckbx_CClick
          end
          object cbbMassimale: TUniDBComboBox
            Left = 366
            Top = 135
            Width = 160
            Hint = ''
            DataField = 'SiglaMassimaleGaranzia'
            DataSource = dsAssicurato
            ParentFont = False
            TabOrder = 10
            FieldLabel = 'Massimale SLP scelto'
            FieldLabelAlign = laTop
            IconItems = <>
            OnChange = cbbMassimaleChange
          end
          object dbedtPremioNettoSLP: TUniDBNumberEdit
            Left = 366
            Top = 241
            Width = 160
            Height = 22
            Hint = ''
            DataField = 'PremioNettoSLP'
            DataSource = dsAssicurato
            ParentFont = False
            TabOrder = 12
            Color = clYellow
            ReadOnly = True
            FieldLabel = 'Premio netto SLP'
            FieldLabelAlign = laTop
            DecimalSeparator = ','
          end
          object grdSelectedGaranzie: TUniDBGrid
            Left = 6
            Top = 131
            Width = 323
            Height = 160
            Hint = ''
            DataSource = dsSelGar
            WebOptions.Paged = False
            LoadMask.Message = 'Loading data...'
            TabOrder = 9
            Columns = <
              item
                FieldName = 'Selected'
                Title.Caption = ' '
                Width = 34
              end
              item
                FieldName = 'GarDescr'
                Title.Caption = 'Garanzia'
                Width = 214
                ReadOnly = True
              end>
          end
          object btnConfermaGaranzia: TUniSpeedButton
            Left = 6
            Top = 7
            Width = 81
            Height = 28
            Action = actConfermaGaranzia
            ParentFont = False
            Font.Style = [fsBold]
            ParentColor = False
            Color = clWindow
            LayoutConfig.Flex = 1
            TabOrder = 13
          end
          object btnAnnullaGaranzia: TUniSpeedButton
            Left = 93
            Top = 7
            Width = 81
            Height = 28
            Action = actAnnullaModGaranzia
            ParentFont = False
            Font.Style = [fsBold]
            ParentColor = False
            Color = clWindow
            LayoutConfig.Flex = 1
            TabOrder = 14
          end
        end
      end
    end
  end
  inherited actlstEditOperation: TActionList
    object actInsGaranzia: TAction
      Category = 'EditGaranzia'
      Caption = 'Ins Polizza'
      OnExecute = actInsGaranziaExecute
    end
    object actDelGaranzia: TAction
      Category = 'EditGaranzia'
      Caption = 'Canc Polizza'
      Enabled = False
      OnExecute = actDelGaranziaExecute
    end
    object actConfermaGaranzia: TAction
      Category = 'EditGaranzia'
      Caption = 'Conferma'
      Enabled = False
      OnExecute = actConfermaGaranziaExecute
    end
    object actAnnullaModGaranzia: TAction
      Category = 'EditGaranzia'
      Caption = 'Annulla'
      Enabled = False
      OnExecute = actAnnullaModGaranziaExecute
    end
    object actModGaranzia: TAction
      Category = 'EditGaranzia'
      Caption = 'Mod Polizza'
      Enabled = False
      OnExecute = actModGaranziaExecute
    end
  end
  inherited dsoEdit: TDataSource
    Left = 712
  end
  inherited dsAssicurato: TDataSource
    OnDataChange = dsAssicuratoDataChange
  end
  object dsSelGar: TDataSource
    DataSet = DMDatiTempPolizzaPP.fdmtblSelGar
    Left = 847
    Top = 548
  end
end
