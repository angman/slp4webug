inherited FModuliDisponibili: TFModuliDisponibili
  ClientHeight = 602
  ClientWidth = 721
  Caption = 'Elenco moduli disponibili'
  OnClose = UniFormClose
  ExplicitWidth = 737
  ExplicitHeight = 641
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 721
    ExplicitWidth = 721
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 721
    Height = 577
    ExplicitWidth = 721
    ExplicitHeight = 577
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 721
      ExplicitWidth = 721
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 721
        ExplicitWidth = 721
        inherited btnInsert: TUniSpeedButton
          Action = actVisualizzaDocumento
        end
        inherited btnModify: TUniSpeedButton
          Top = -2
          Action = actEsportaDocumento
          ExplicitTop = -2
        end
        inherited btnSeleziona: TUniSpeedButton
          Left = 247
          Top = -2
          Action = actEsportaDocumento
          ExplicitLeft = 247
          ExplicitTop = -2
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 721
        ExplicitWidth = 721
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 721
      Height = 517
      ExplicitWidth = 721
      ExplicitHeight = 517
      inherited grdElenco: TUniDBGrid
        Top = 72
        Width = 721
        Height = 445
        WebOptions.Paged = False
        OnDblClick = actVisualizzaDocumentoExecute
        Columns = <
          item
            FieldName = 'descrizione'
            Title.Caption = 'Descrizione'
            Width = 400
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 48
        Top = 78
        ExplicitLeft = 48
        ExplicitTop = 78
      end
      object UniPanel1: TUniPanel
        Left = 0
        Top = 0
        Width = 721
        Height = 72
        Hint = ''
        Align = alTop
        TabOrder = 3
        Caption = ''
        object uniRGtipoModuli: TUniRadioGroup
          Left = 1
          Top = 1
          Width = 719
          Height = 70
          Hint = ''
          Items.Strings = (
            'Polizze'
            'Modulistica'
            'Circolari')
          ItemIndex = 0
          Align = alClient
          Caption = ' Seleziona i moduli da visionare '
          TabOrder = 1
          ParentFont = False
          Font.Height = -13
          Columns = 3
          OnClick = uniRGtipoModuliClick
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    Left = 320
    Top = 152
    object actVisualizzaDocumento: TAction
      Category = 'ItemsDocumento'
      Caption = 'Visualizza'
      OnExecute = actVisualizzaDocumentoExecute
    end
    object actEsportaDocumento: TAction
      Category = 'ItemsDocumento'
      Caption = 'Esporta PDF'
      OnExecute = actEsportaDocumentoExecute
    end
  end
  inherited dsGrid: TDataSource
    Left = 408
    Top = 144
  end
  object UniScreenMask1: TUniScreenMask
    AttachedControl = btnInsert
    Enabled = True
    DisplayMessage = 'Attendere, creazione anteprima in corso ...'
    TargetControl = Owner
    Left = 384
    Top = 261
  end
end
