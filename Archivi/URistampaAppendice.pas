unit URistampaAppendice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, System.StrUtils,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses, uniCheckBox,
  uniEdit, uniGUIClasses, uniDateTimePicker, uniGroupBox, uniButton, uniBitBtn,
  uniGUIBaseClasses, uniPanel,
  uniGUIForm, uniDBCheckBox,
  uniDBEdit, Data.DB, uniGUIApplication, dbisamtb, UShowAppendiceRec;

type
  TFRistampaAppendice = class(TUniForm)
    pnlButtonsEditAss: TUniSimplePanel;
    btnBconferma: TUniBitBtn;
    btnBabbandona: TUniBitBtn;
    ungrpbxEstremiAppendice: TUniGroupBox;
    dtpckDataEmissione: TUniDateTimePicker;
    edtNumeroAppendice: TUniNumberEdit;
    dtpckDataDa: TUniDateTimePicker;
    dtpckDataA: TUniDateTimePicker;
    edtImporto: TUniFormattedNumberEdit;
    edtNextRata: TUniFormattedNumberEdit;
    ckbxTipoTitolo: TUniCheckBox;
    uncbxMemorizza: TUniCheckBox;
    uncbxIncassa: TUniCheckBox;
  private
    { Private declarations }
    QyPolizzaEdit: TDBISAMQuery;

    procedure ShowCallBack(Sender: TComponent; AResult: Integer);
    function PrepareAnteprima(var AppRec: TShowAppendiceRec): string;
  public
    { Public declarations }
    class procedure ShowRistampaAppendiceForm(UniApplication: TUniGUIApplication; QPolizzaEdit: TDBISAMQuery);
  end;

implementation

uses
  libSLP, MainModule, UDMDatiReport;

{$R *.dfm}
{ TFRistampaAppendice }

function TFRistampaAppendice.PrepareAnteprima(var AppRec: TShowAppendiceRec): string;
begin
  AppRec.CodCompagnia      := QyPolizzaEdit.FieldByName('RIF_COMPAGNIA').AsInteger;
  AppRec.CodPolizza        := QyPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger;
  AppRec.NPolizza          := QyPolizzaEdit.FieldByName('N_POLIZZA').AsString;
  AppRec.CodCliente        := QyPolizzaEdit.FieldByName('COD_CLI').AsInteger;
  AppRec.NAppendice        := Round(edtNumeroAppendice.Value);
  AppRec.DataAppendice     := dtpckDataEmissione.DateTime;
  AppRec.PTasse            := QyPolizzaEdit.FieldByName('PTASSE').AsCurrency;
  AppRec.DtDalAppendice    := dtpckDataDa.DateTime;
  AppRec.DtAlAppendice     := dtpckDataA.DateTime;
  AppRec.ImportoModificato := ckbxTipoTitolo.Checked;
  AppRec.bCanPrint         := True;
  AppRec.Importo           := edtImporto.Value;
  AppRec.NextRata          := edtNextRata.Value;
  AppRec.tipo_titolo       := IfThen(ckbxTipoTitolo.Checked, '15', '12');

  result := UniMainModule.DMDatiReport.PrepareReportAppendice(UniMainModule.DMdatiAgePolizze.QdettAppendice, AppRec);

end;

procedure TFRistampaAppendice.ShowCallBack(Sender: TComponent; AResult: Integer);
var
  ReportFileName: string;
  AppRec: TShowAppendiceRec;

begin
  if AResult = mrOk then
  begin
    ReportFileName := PrepareAnteprima(AppRec);
    if uncbxMemorizza.Checked then
      UniMainModule.DMdatiAgePolizze.memorizzaAppendice(UniMainModule.DMDatiReport.QFindCliente, AppRec,
        ChangeFileExt(ReportFileName, '.FR3'), uncbxIncassa.Checked);

    UniMainModule.DMDatiReport.ShowReportAppendice(ReportFileName, True);
  end;

end;

class procedure TFRistampaAppendice.ShowRistampaAppendiceForm(UniApplication: TUniGUIApplication;
  QPolizzaEdit: TDBISAMQuery);
begin
  with TFRistampaAppendice.Create(UniApplication) do
  begin
    QyPolizzaEdit            := QPolizzaEdit;
    edtNumeroAppendice.Value := QPolizzaEdit.FieldByName('APPENDICE').AsInteger + 1;
    // tem.ptasse      := QPolizzaEdit.fieldbyname('PTASSE').ascurrency;
    edtNextRata.Value := QPolizzaEdit.FieldByName('LORDO').AsCurrency /
      dai_frazionamento(QPolizzaEdit.FieldByName('FRAZIONAM').AsString);
    // tem.polizza     := QPolizzaEdit.fieldbyname('N_POLIZZA').asString;
    // tem.cod_cliente := QPolizzaEdit.fieldbyname('COD_CLI').asInteger;
    dtpckDataEmissione.DateTime := Date;
    dtpckDataDa.DateTime        := Date;
    dtpckDataA.DateTime         := Date;
    ShowModal(ShowCallBack);
  end;
end;

end.
