inherited FRiprendiBozza: TFRiprendiBozza
  ClientWidth = 1002
  Caption = 'Riprendi dati bozza / preventivo'
  ExplicitWidth = 1018
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 1002
    ExplicitWidth = 1035
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 1002
    ExplicitWidth = 1035
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 1002
      ExplicitWidth = 1035
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 1002
        ExplicitWidth = 1035
        inherited btnInsert: TUniSpeedButton
          Visible = False
        end
        inherited btnModify: TUniSpeedButton
          Visible = False
        end
        inherited btnVisualizza: TUniSpeedButton
          Enabled = False
          Action = actCancellaBozze
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 1002
        ExplicitWidth = 1035
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 1002
      ExplicitWidth = 1035
      inherited grdElenco: TUniDBGrid
        Top = 34
        Width = 1002
        Height = 325
        TrackOver = False
        OnSelectionChange = grdElencoSelectionChange
        Columns = <
          item
            FieldName = 'DT_PREVENTIVO'
            Title.Caption = 'Data Bozza / prev.'
            Width = 120
            Sortable = True
          end
          item
            FieldName = 'CONTRAENTE'
            Filtering.Enabled = True
            Filtering.Editor = unedtContraente
            Filtering.ChangeDelay = 500
            Title.Caption = 'CONTRAENTE'
            Width = 200
            Sortable = True
          end
          item
            FieldName = 'descrutente'
            Title.Caption = 'Des Utente'
            Width = 180
          end
          item
            FieldName = 'DECORRENZA'
            Title.Caption = 'Decorrenza'
            Width = 80
          end
          item
            FieldName = 'lordo'
            Title.Caption = 'Lordo'
            Width = 64
          end
          item
            FieldName = 'DESTIPOPOL'
            Title.Caption = 'Tipo polizza'
            Width = 180
          end
          item
            FieldName = 'descrStatus'
            Filtering.Enabled = True
            Filtering.Editor = cbbDesStatus
            Title.Caption = 'Status'
            Width = 124
            ReadOnly = True
          end
          item
            FieldName = 'COD_POLIZZA'
            Title.Caption = 'Cod_polizza'
            Width = 64
            Visible = False
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 160
        Top = 70
        ExplicitLeft = 160
        ExplicitTop = 70
        object unedtContraente: TUniEdit
          Left = 40
          Top = 20
          Width = 145
          Hint = ''
          CharCase = ecUpperCase
          Text = 'UNEDTCONTRAENTE'
          TabOrder = 1
        end
        object cbbDesStatus: TUniDBLookupComboBox
          Left = 40
          Top = 64
          Width = 145
          Hint = ''
          ListField = 'Descrizione'
          ListSource = dsStatus
          KeyField = 'Descrizione'
          ListFieldIndex = 0
          DataField = 'descrStatus'
          DataSource = dsGrid
          TabOrder = 2
          Color = clWindow
        end
      end
      object pnlFilters: TUniSimplePanel
        Left = 0
        Top = 0
        Width = 1002
        Height = 34
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 3
        ExplicitTop = 8
        ExplicitWidth = 935
        object cbbOperatore: TUniDBLookupComboBox
          Left = 10
          Top = 7
          Width = 330
          Hint = ''
          ListField = 'SIGLA;NOME'
          ListSource = dsCollaboratori
          KeyField = 'COD_PRODUTTORE'
          ListFieldIndex = 0
          ClearButton = True
          TabOrder = 1
          Color = clWindow
          FieldLabel = 'Operatore'
          OnChange = cbbOperatoreChange
        end
      end
    end
  end
  inherited actlstOperazioni: TActionList
    object actCancellaBozze: TAction
      Category = 'ItemOperation'
      Caption = 'Cancella'
      OnExecute = actCancellaBozzeExecute
    end
  end
  inherited dsGrid: TDataSource
    DataSet = DMBozzePreventivi.QBozzePreventivi
  end
  object dsCollaboratori: TDataSource
    Left = 376
    Top = 85
  end
  object dsStatus: TDataSource
    DataSet = DMBozzePreventivi.fdmtblStatusPolizza
    Left = 480
    Top = 245
  end
end
