unit UStampaPolizzaOM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UStampaBasePolizza, Data.DB, System.Actions, Vcl.ActnList,
  uniPageControl, uniEdit, uniDBEdit, uniButton, uniBitBtn, uniSpeedButton, uniLabel,
  uniGUIBaseClasses, uniPanel, uniImageList, uniRadioGroup, uniDBRadioGroup, uniCheckBox,
  uniDBCheckBox, uniMultiItem, uniComboBox, uniDBComboBox, uniDateTimePicker, uniDBDateTimePicker,
  uniDBLookupComboBox, uniMemo, uniSpinEdit, UdmdatiAge, unimDBEdit, System.Math,
  uniScreenMask, UDMdatiAgePolizze, Vcl.Menus, uniMainMenu, uniMenuButton;

type
  TFStampaPolizzaOM = class(TFStampaBasePolizza)
    pgAssicurato: TUniTabSheet;
    pcOggettoAssicurato: TUniPageControl;
    pgOggettoAssicurato: TUniTabSheet;
    pgFamiliari: TUniTabSheet;
    pgMassimaliGaranzie: TUniTabSheet;
    undbrdgrpTipoAssicurato: TUniDBRadioGroup;
    pnlpncontaier: TUniContainerPanel;
    pnlAssicurato: TUniPanel;
    pnlVeicolo: TUniPanel;
    dbedtDenominazione: TUniDBEdit;
    dbedtCodiceFiscale: TUniDBEdit;
    dbedtPatente: TUniDBEdit;
    dbedtRilasciataDa: TUniDBEdit;
    dtpckDataRilascioPat: TUniDBDateTimePicker;
    dtpckDataScadenzaPat: TUniDBDateTimePicker;
    cbbCategoriaPat: TUniDBComboBox;
    dbedtNome1: TUniDBEdit;
    dbedtPatente1: TUniDBEdit;
    dbedtCategoria1: TUniDBEdit;
    dbedtNome2: TUniDBEdit;
    dbedtPatente2: TUniDBEdit;
    dbedtCategoria2: TUniDBEdit;
    dbedtNome3: TUniDBEdit;
    dbedtPatente3: TUniDBEdit;
    dbedtCategoria3: TUniDBEdit;
    dbedtNome4: TUniDBEdit;
    dbedtPatente4: TUniDBEdit;
    dbedtCategoria4: TUniDBEdit;
    dbcblkupTipoVeicolo: TUniDBLookupComboBox;
    dbedtMarca: TUniDBEdit;
    dbedtModello: TUniDBEdit;
    dbedtTarga: TUniDBEdit;
    dbedtTelaio: TUniDBEdit;
    dbedtQlHp: TUniDBEdit;
    dtpckScadenzaRevisione: TUniDBDateTimePicker;
    cbbMassimali: TUniDBComboBox;
    ckbxRischioAccessorio: TUniDBCheckBox;
    cbbRimbSpesePatPunti: TUniDBComboBox;
    dbgrpForma: TUniDBRadioGroup;
    ckbxPolizzaIndicizzata: TUniDBCheckBox;
    ckbxAppendiceControversie: TUniDBCheckBox;
    ckbxIncMaxDP: TUniDBCheckBox;
    ckbxbProblematicaRCA: TUniDBCheckBox;
    ckbxPerizieParte: TUniDBCheckBox;
    ckbxMancatoInterventoRCA: TUniDBCheckBox;
    ckbxProtezioneFamiglia: TUniDBCheckBox;
    ckbxEstEPatenti: TUniDBCheckBox;
    unmEstEPatente: TUniMemo;
    dsTipoVeicolo: TDataSource;
    dbedtNumeroAssicurati: TUniDBNumberEdit;
    uniScreenMask: TUniScreenMask;
    UniLabel1: TUniLabel;
    procedure undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure dbgrpFormaChangeValue(Sender: TObject);
    procedure ckbxProtezioneFamigliaClick(Sender: TObject);
    procedure ckbxRischioAccessorioClick(Sender: TObject);
    procedure dbedtNumeroAssicuratiChange(Sender: TObject);
    procedure ckbxEstEPatentiClick(Sender: TObject);
  private
    { Private declarations }

    procedure EnableDisableEstFamiglia(Enable: Boolean);
    procedure EnableDisableEstRischioAccessorio(Enable: Boolean);
    procedure RiallineaMaxGaranzie;
    procedure AzzeraEstensioni;

  protected
    procedure SetCliente(ASelectedItem: Integer; const ASelectedDescription: string); override;

  public
    { Public declarations }
    procedure ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string); override;

  end;

function FStampaPolizzaOM: TFStampaPolizzaOM;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UDMDatiPolizzaOM, UDMDatiTempPolizzaOM, UDMDatiTempPolizza;

function FStampaPolizzaOM: TFStampaPolizzaOM;
begin
  Result := TFStampaPolizzaOM(UniMainModule.GetFormInstance(TFStampaPolizzaOM));
end;

procedure TFStampaPolizzaOM.AzzeraEstensioni;
begin
   // azzera le estensioni eventualmente richiamate
   if ckbxPerizieParte.Checked then
   begin
      ckbxPerizieParte.Checked:=false;
      dsoEdit.DataSet.FieldByName('perizieparte').AsBoolean:=False;
   end;
   if ckbxbProblematicaRCA.Checked then
   begin
      ckbxbProblematicaRCA.Checked:=false;
      dsoEdit.DataSet.FieldByName('ProblematicheRCA').AsBoolean:=False;
   end;
   if ckbxProtezioneFamiglia.Checked then
   begin
      ckbxProtezioneFamiglia.Checked:=false;
      dsoEdit.DataSet.FieldByName('ProtezioneFamiglia').AsBoolean:=False;
      dsoEdit.DataSet.FieldByName('NumeroAssicurati').AsInteger:=0;
   end;
   if ckbxAppendiceControversie.Checked then
   begin
      ckbxAppendiceControversie.Checked:=false;
      dsoEdit.DataSet.FieldByName('AppControversie').AsBoolean:=False;
   end;
   if ckbxMancatoInterventoRCA.Checked then
   begin
      ckbxMancatoInterventoRCA.Checked:=false;
      dsoEdit.DataSet.FieldByName('MancatoIntRCA').AsBoolean:=False;
   end;
   if ckbxEstEPatenti.Checked then
   begin
      ckbxEstEPatenti.Checked:=false;
      dsoEdit.DataSet.FieldByName('EstPatente').AsBoolean:=False;
   end;

   if ckbxRischioAccessorio.Checked then
   begin
      ckbxRischioAccessorio.Checked:=false;
      dsoEdit.DataSet.FieldByName('RimborsoSpesePatPunti').AsBoolean:=False;
      cbbRimbSpesePatPunti.ItemIndex:=-1;
      dsoEdit.DataSet.FieldByName('SiglaRimborsoSpesePatPunti').AsString:='';
   end;

end;

procedure TFStampaPolizzaOM.ckbxEstEPatentiClick(Sender: TObject);
begin
  inherited;
  if ckbxEstEPatenti.Checked then
  begin
    ckbxProtezioneFamiglia.Checked := false;
    ckbxProtezioneFamiglia.Enabled := false;
    ckbxProtezioneFamigliaClick(Sender);
  end
  else
  begin
    if not UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta then
      ckbxProtezioneFamiglia.Enabled := True;
  end;
end;

procedure TFStampaPolizzaOM.ckbxProtezioneFamigliaClick(Sender: TObject);
begin
  dbedtNumeroAssicurati.Enabled := ckbxProtezioneFamiglia.Checked;
  pgFamiliari.TabVisible        := ckbxProtezioneFamiglia.Checked;
  if not ckbxProtezioneFamiglia.Checked then
  begin
    pcOggettoAssicurato.ActivePage := pgAssicurato;
    dbedtNumeroAssicurati.Value    := 0;
    pgFamiliari.TabVisible         := false;
    dbgrpForma.Enabled             := True;
    cbbCategoriaPat.Enabled        := True;
    ckbxEstEPatenti.Enabled        := True;
  end
  else
  begin
    if Sender <> nil then
      dbedtNumeroAssicurati.Value := 1;
    pgFamiliari.TabVisible        := True;
    dbgrpForma.Enabled            := false;
    cbbCategoriaPat.Enabled       := false;
    ckbxEstEPatenti.Checked       := false;
    ckbxEstEPatenti.Enabled       := false;
  end;
  dbedtNumeroAssicuratiChange(nil);

end;

procedure TFStampaPolizzaOM.ckbxRischioAccessorioClick(Sender: TObject);
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
  if (not ckbxRischioAccessorio.Checked) then
    cbbRimbSpesePatPunti.Text := '';

end;

procedure TFStampaPolizzaOM.dbedtNumeroAssicuratiChange(Sender: TObject);
var
  i: Integer;

  procedure EnableDisableFamiliari(Enable: Boolean);
  begin
    TUniDBEdit(FindComponent('DBEdtNome' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtPatente' + i.ToString)).Enabled := Enable;
    TUniDBEdit(FindComponent('DBEdtCategoria' + i.ToString)).Enabled := Enable;

  end;

begin
  for i := 1 to 4 do
    EnableDisableFamiliari(i <= dbedtNumeroAssicurati.Value);
end;

procedure TFStampaPolizzaOM.dbgrpFormaChangeValue(Sender: TObject);
var
  selectedForma: string;
begin
  selectedForma          := dbgrpForma.Values[dbgrpForma.ItemIndex];
  cbbMassimali.DataField := '';
  try
    UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
      selectedForma);

  finally
    cbbMassimali.DataField := 'SiglaMassimale';
  end;
  EnableDisableEstFamiglia( dbgrpForma.ItemIndex = 0 );

end;

procedure TFStampaPolizzaOM.EnableDisableEstFamiglia(Enable: Boolean);
begin
  ckbxProtezioneFamiglia.Enabled := Enable and (undbrdgrpTipoAssicurato.ItemIndex=0) ;
  dbedtNumeroAssicurati.Enabled  := dsoEdit.DataSet.FieldByName('PROTEZIONEFAMIGLIA').AsBoolean;
  ckbxProtezioneFamiglia.Checked := dsoEdit.DataSet.FieldByName('PROTEZIONEFAMIGLIA').AsBoolean;
end;

procedure TFStampaPolizzaOM.EnableDisableEstRischioAccessorio(Enable: Boolean);
begin
  ckbxRischioAccessorio.Enabled := Enable;
  cbbRimbSpesePatPunti.Enabled  := Enable and ckbxRischioAccessorio.Checked;
end;

procedure TFStampaPolizzaOM.ReloadDocumento(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  AzzeraEstensioni;
  inherited;
  RiallineaMaxGaranzie;
end;

procedure TFStampaPolizzaOM.RiallineaMaxGaranzie;
begin
  cbbRimbSpesePatPunti.Enabled := ckbxRischioAccessorio.Checked;
  ckbxProtezioneFamigliaClick(nil);
  undbrdgrpTipoAssicuratoChangeValue(nil);
end;

procedure TFStampaPolizzaOM.SetCliente(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  inherited SetCliente(ASelectedItem, ASelectedDescription);
  if UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta then
  begin
    undbrdgrpTipoAssicurato.ItemIndex := 1;
    dbedtNumeroAssicurati.Value       := 0;
    dbedtNumeroAssicurati.DataSource.DataSet.FieldByName('protezionefamiglia').AsBoolean := false;
    // fdmtblPolizzaProtezioneFamiglia.AsBoolean := false;
  end
  else
    undbrdgrpTipoAssicurato.ItemIndex := 0;
  // undbrdgrpTipoAssicurato.ItemIndex := ifThen(UniMainModule.DMDatiTempPolizza.ContraenteIsSocieta, 1, 0);

end;

procedure TFStampaPolizzaOM.undbrdgrpTipoAssicuratoChangeValue(Sender: TObject);
begin
  pnlAssicurato.visible := undbrdgrpTipoAssicurato.ItemIndex = 0;

  pnlVeicolo.visible     := undbrdgrpTipoAssicurato.ItemIndex = 1;
  pgFamiliari.TabVisible := undbrdgrpTipoAssicurato.ItemIndex = 0;

  EnableDisableEstFamiglia(undbrdgrpTipoAssicurato.ItemIndex = 0);
  EnableDisableEstRischioAccessorio((undbrdgrpTipoAssicurato.ItemIndex = 0)); // and ckbxRischioAccessorio.Checked);
end;

procedure TFStampaPolizzaOM.UniFormCreate(Sender: TObject);
begin
  inherited;
  pcDatiEreditabili.ActivePage   := pgContraente;
  pcOggettoAssicurato.ActivePage := pgOggettoAssicurato;

  dsoEdit.DataSet      := UniMainModule.DMDatiTempPolizza.fdmtblPolizza;
  dsGaranzia.DataSet   := UniMainModule.DMDatiTempPolizza.fdmtblGaranzia;
  dsAssicurato.DataSet := UniMainModule.DMDatiTempPolizza.fdmtblAssicurato;

  ckbxMancatoInterventoRCA.Enabled  := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICEDP');
  ckbxAppendiceControversie.Enabled := UniMainModule.DMdatiAge.isExtensionEnabled('APPENDICECC');
  // ckbxTacitoRinnovo.Enabled         := UniMainModule.DMdatiAge.TacitoRinnovoEnabled;
  UniMainModule.DMdatiAgePolizze.caricaTariffa(UniMainModule.TipoPolizza, cbbMassimali.Items, 'MAXBASE',
    UniMainModule.DMDatiTempPolizza.fdmtblPolizzaForma.asstring);
  cbbMassimali.ItemIndex := 0;
  // Forza il calcolo sul primo massimale
  TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzasiglaMassimale.asstring :=
    Copy(cbbMassimali.Text, 1, 3);
  // TDMDatiTempPolizzaOM(UniMainModule.DMDatiTempPolizza).fdmtblPolizzaSiglaMassimaleChange(nil);
  dsTipoVeicolo.DataSet.Open;

  pnlAssicurato.visible := True;
  pnlVeicolo.visible    := false;

  undbrdgrpTipoAssicurato.OnChangeValue := undbrdgrpTipoAssicuratoChangeValue;
end;

initialization

RegisterClass(TFStampaPolizzaOM);

end.
