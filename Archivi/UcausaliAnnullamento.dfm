inherited FcausaliAnnullamento: TFcausaliAnnullamento
  ClientWidth = 306
  Caption = 'Causali annullamento'
  ExplicitWidth = 322
  ExplicitHeight = 240
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 306
    Visible = False
    ExplicitWidth = 300
  end
  inherited pnlGlobal: TUniSimplePanel
    Width = 306
    ExplicitWidth = 300
    inherited pnlOperationButtons: TUniSimplePanel
      Width = 306
      ExplicitWidth = 300
      inherited pnlButtonContainer: TUniSimplePanel
        Width = 306
        Constraints.MaxWidth = 0
        Constraints.MinWidth = 0
        ExplicitWidth = 300
        inherited btnInsert: TUniSpeedButton
          Width = 31
          Visible = False
          ExplicitWidth = 31
        end
        inherited btnModify: TUniSpeedButton
          Left = 47
          Width = 31
          Visible = False
          ExplicitLeft = 47
          ExplicitWidth = 31
        end
        inherited btnSeleziona: TUniSpeedButton [2]
          Left = 82
          ExplicitLeft = 82
        end
        inherited btnChiudi: TUniSpeedButton [3]
          Left = 194
          ExplicitLeft = 194
        end
        inherited btnVisualizza: TUniSpeedButton [4]
        end
      end
      inherited UniSimplePanel1: TUniSimplePanel
        Width = 306
        Visible = False
        ExplicitWidth = 300
      end
    end
    inherited pnlGrid: TUniContainerPanel
      Width = 306
      ExplicitWidth = 300
      inherited grdElenco: TUniDBGrid
        Width = 306
        OnSelectionChange = grdElencoSelectionChange
        OnBodyDblClick = nil
        OnDblClick = actSelezionaDaElencoExecute
        OnClearFilters = nil
        OnColumnFilter = nil
        Columns = <
          item
            FieldName = 'descrizione'
            Title.Caption = 'Causale annullamento'
            Width = 260
          end>
      end
      inherited pnlhdrSearch: TUniHiddenPanel
        Left = 64
        Top = 54
        ExplicitLeft = 64
        ExplicitTop = 54
      end
    end
  end
  inherited actlstOperazioni: TActionList
    Left = 56
    Top = 160
  end
  inherited dsGrid: TDataSource
    Left = 64
    Top = 248
  end
end
