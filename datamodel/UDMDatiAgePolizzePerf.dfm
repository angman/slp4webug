inherited DMDatiAgePolizzePerf: TDMDatiAgePolizzePerf
  OldCreateOrder = True
  Height = 343
  Width = 481
  object Qpol_da_perfez: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        '  select cod_polizza, contraente, decorrenza, scadenza, fraziona' +
        'm, tipologia,'
      
        '  n_polizza,lordo, sub_age, data_inserimento, data_emissione, da' +
        'ta_perfez,'
      
        '  totale2, totale1, status, data_canc, rif_tipo_pol, fatta_da, c' +
        'on_appendice,'
      
        '  app_report, n_polizza_sostituita, inpdf , tpol.descrizione as ' +
        'tipo_polizza, '
      '  tpol.sigla, provvigioni , pol.n_preventivo'
      '  from slp_tpolizze pol'
      
        '  left join tipo_pol tpol on tpol.cod_tipo_pol = pol.rif_tipo_po' +
        'l'
      'where n_polizza > '#39#39
      'and (status= '#39#39' or status = null )'
      'order by n_polizza')
    Params = <>
    Left = 208
    Top = 136
    object Qpol_da_perfezcod_polizza: TAutoIncField
      FieldName = 'cod_polizza'
    end
    object Qpol_da_perfezContraente: TStringField
      FieldName = 'contraente'
      Size = 50
    end
    object Qpol_da_perfezDecorrenza: TDateField
      FieldName = 'decorrenza'
    end
    object Qpol_da_perfezScadenza: TDateField
      FieldName = 'scadenza'
    end
    object Qpol_da_perfezFrazionam: TStringField
      FieldName = 'frazionam'
      FixedChar = True
      Size = 1
    end
    object Qpol_da_perfezN_polizza: TStringField
      FieldName = 'n_polizza'
      Size = 6
    end
    object Qpol_da_perfezLordo: TCurrencyField
      FieldName = 'lordo'
    end
    object Qpol_da_perfezSub_age: TStringField
      FieldName = 'sub_age'
      Size = 3
    end
    object Qpol_da_perfezData_inserimento: TDateField
      FieldName = 'data_inserimento'
    end
    object Qpol_da_perfezData_emissione: TDateField
      FieldName = 'data_emissione'
    end
    object Qpol_da_perfezData_perfez: TDateTimeField
      FieldName = 'data_perfez'
    end
    object Qpol_da_perfezTotale2: TCurrencyField
      FieldName = 'totale2'
    end
    object Qpol_da_perfezTotale1: TCurrencyField
      FieldName = 'totale1'
    end
    object Qpol_da_perfezStatus: TStringField
      FieldName = 'status'
      Size = 1
    end
    object Qpol_da_perfezData_canc: TDateTimeField
      FieldName = 'data_canc'
    end
    object Qpol_da_perfezRif_tipo_pol: TIntegerField
      FieldName = 'rif_tipo_pol'
    end
    object Qpol_da_perfezfatta_da: TIntegerField
      FieldName = 'fatta_da'
    end
    object Qpol_da_perfezCon_appendice: TStringField
      FieldName = 'con_appendice'
      Size = 1
    end
    object Qpol_da_perfezApp_report: TBlobField
      FieldName = 'app_report'
    end
    object Qpol_da_perfezN_polizza_sostituita: TStringField
      FieldName = 'n_polizza_sostituita'
      Size = 6
    end
    object Qpol_da_perfezInpdf: TStringField
      FieldName = 'inpdf'
      Size = 1
    end
    object Qpol_da_perfezTipo_polizza: TStringField
      FieldName = 'tipo_polizza'
      Size = 30
    end
    object Qpol_da_perfezSigla: TStringField
      FieldName = 'sigla'
      Size = 4
    end
    object Qpol_da_perfezProvvigioni: TCurrencyField
      FieldName = 'provvigioni'
    end
    object Qpol_da_perfezn_preventivo: TStringField
      FieldName = 'n_preventivo'
    end
    object Qpol_da_perfezPromoterLkup: TStringField
      DisplayLabel = 'PromoterLkup'
      FieldKind = fkLookup
      FieldName = 'SiglaPromoterLkup'
      LookupDataSet = DMdatiAge.QSelPromoter
      LookupKeyFields = 'COD_PRODUTTORE'
      LookupResultField = 'NOME'
      KeyFields = 'fatta_da'
      Size = 25
      Lookup = True
    end
    object Qpol_da_perfezEmissLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'EmissLkup'
      LookupDataSet = DMdatiAge.fdmtblTipoEmiss
      LookupKeyFields = 'CodEmmiss'
      LookupResultField = 'DesTipoEmiss'
      KeyFields = 'TipoEmis'
      Size = 3
      Lookup = True
    end
    object Qpol_da_perfezTipoEmis: TStringField
      FieldName = 'TipoEmis'
      Size = 1
    end
    object Qpol_da_perfezFrazionamentoLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'FrazionamentoLkup'
      LookupDataSet = DMdatiAge.fdmtblFrazionamento
      LookupKeyFields = 'CodFrazionamento'
      LookupResultField = 'DesFrazionamento'
      KeyFields = 'frazionam'
      Lookup = True
    end
    object Qpol_da_perfezSubAgeLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'SubAgeLkup'
      LookupDataSet = DMdatiAge.QSelPromoter
      LookupKeyFields = 'COD_PRODUTTORE'
      LookupResultField = 'SIGLA'
      KeyFields = 'fatta_da'
      Size = 5
      Lookup = True
    end
    object Qpol_da_perfezsub_promoter: TIntegerField
      FieldName = 'sub_promoter'
    end
    object Qpol_da_perfezSiglaSubPromoter: TStringField
      FieldKind = fkLookup
      FieldName = 'SiglaSubPromoter'
      LookupDataSet = DMdatiAge.QSelPromoter
      LookupKeyFields = 'cod_produttore'
      LookupResultField = 'sigla'
      KeyFields = 'sub_promoter'
      Size = 5
      Lookup = True
    end
  end
  object Qreg_pagamento_pol: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into scadenze'
      '('
      'tipo_titolo, DENOMINAZ, RAMO, FRAZIONAM, RATA,'
      'N_POLIZZA, STATO, DATA_PAG, DATA_REG_PAG, '
      'data_pag_ccp, INCASSATA_DA, LORDO, provvigioni,'
      'netto, ptasse, accessori, tasse, valuta,'
      'RIF_COD_COMPAGNIA, RIF_COD_POLIZZA, decorrenza,'
      'scadenza, RIF_CLIENTE'
      ')'
      'VALUES'
      '('
      ':tipo_titolo, :denominaz, :ramo, :frazionam, :rata,'
      ':n_polizza, '#39'P'#39', :data_pag, :data_reg_pag, '
      ':data_valuta, :incassata_da, :totale2, :provvigioni,'
      ':imponibile2, :ptasse, :accessori2, :imposte2, :valuta,'
      ':rif_compagnia, :cod_polizza, :decorrenza, '
      ':SCAD_PRIMA_RATA, :cod_cli'
      ')')
    Params = <
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end
      item
        DataType = ftUnknown
        Name = 'denominaz'
      end
      item
        DataType = ftUnknown
        Name = 'ramo'
      end
      item
        DataType = ftUnknown
        Name = 'frazionam'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'data_pag'
      end
      item
        DataType = ftUnknown
        Name = 'data_reg_pag'
      end
      item
        DataType = ftUnknown
        Name = 'data_valuta'
      end
      item
        DataType = ftUnknown
        Name = 'incassata_da'
      end
      item
        DataType = ftUnknown
        Name = 'totale2'
      end
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'imponibile2'
      end
      item
        DataType = ftUnknown
        Name = 'ptasse'
      end
      item
        DataType = ftUnknown
        Name = 'accessori2'
      end
      item
        DataType = ftUnknown
        Name = 'imposte2'
      end
      item
        DataType = ftUnknown
        Name = 'valuta'
      end
      item
        DataType = ftUnknown
        Name = 'rif_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'decorrenza'
      end
      item
        DataType = ftUnknown
        Name = 'SCAD_PRIMA_RATA'
      end
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    Left = 40
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end
      item
        DataType = ftUnknown
        Name = 'denominaz'
      end
      item
        DataType = ftUnknown
        Name = 'ramo'
      end
      item
        DataType = ftUnknown
        Name = 'frazionam'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'data_pag'
      end
      item
        DataType = ftUnknown
        Name = 'data_reg_pag'
      end
      item
        DataType = ftUnknown
        Name = 'data_valuta'
      end
      item
        DataType = ftUnknown
        Name = 'incassata_da'
      end
      item
        DataType = ftUnknown
        Name = 'totale2'
      end
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'imponibile2'
      end
      item
        DataType = ftUnknown
        Name = 'ptasse'
      end
      item
        DataType = ftUnknown
        Name = 'accessori2'
      end
      item
        DataType = ftUnknown
        Name = 'imposte2'
      end
      item
        DataType = ftUnknown
        Name = 'valuta'
      end
      item
        DataType = ftUnknown
        Name = 'rif_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'decorrenza'
      end
      item
        DataType = ftUnknown
        Name = 'SCAD_PRIMA_RATA'
      end
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
  end
  object Qins_polizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      '-- start transaction;'
      ''
      'insert into "polizze"'
      
        '( RIF_TIPO_POL, CONTRAENTE, DECORRENZA, SCADENZA, data_emissione' +
        ','
      'FRAZIONAM, N_POLIZZA, inpdf, LORDO, netto,'
      'ptasse, tasse, accessori, valuta, NOTE,'
      'RIF_COMPAGNIA, COD_CLI, FATTA_DA, rif_referente, convenzione,'
      
        'N_POLIZZA_SOSTITUITA, data_ini_indice, RIF_COD_RAMO, INTERMEDIAT' +
        'A_DA, sub_promoter,'
      
        'STATO, PROVVIGIONI, PERC_PROVVIGIONI, TIPO_PROVVIGIONI, DATA_EMI' +
        'SSIONE, RAGIONE_SOC,'
      'DATI1, DATI2, DATI3, DATI4, DATI5,'
      'SPECIAL, ORDINE, DATI_VARI, DT_LAST_MOD, ID_GEN_SLP_AGE )'
      ''
      'VALUES'
      ''
      
        '( :RIF_TIPO_POL, :CONTRAENTE, :DECORRENZA, :SCADENZA, :data_emis' +
        'sione,'
      ':FRAZIONAM, :N_POLIZZA, :inpdf, :LORDO, :netto,'
      ':ptasse, :tasse, :accessori, :valuta, :NOTE,'
      
        ':RIF_COMPAGNIA, :COD_CLI, :FATTA_DA, :rif_referente, :convenzion' +
        'e,'
      
        ':N_POLIZZA_SOSTITUITA, :data_ini_indice, :RIF_COD_RAMO, :INTERME' +
        'DIATA_DA, :sub_promoter,'
      
        #39'V'#39', :PROVVIGIONI, :PERC_PROVVIGIONI, :TIPO_PROVVIGIONI, :DATA_E' +
        'MISSIONE, :RAGIONE_SOC,'
      ':DATI1, :DATI2, :DATI3, :DATI4, :DATI5,'
      ':SPECIAL, :ORDINE, :DATI_VARI, :DT_LAST_MOD, :ID_GEN_SLP_AGE );'
      ''
      '--select cod_polizza from polizze where n_polizza= :n_polizza;'
      '-- commit flush;')
    Params = <
      item
        DataType = ftUnknown
        Name = 'RIF_TIPO_POL'
      end
      item
        DataType = ftUnknown
        Name = 'CONTRAENTE'
      end
      item
        DataType = ftUnknown
        Name = 'DECORRENZA'
      end
      item
        DataType = ftUnknown
        Name = 'SCADENZA'
      end
      item
        DataType = ftUnknown
        Name = 'data_emissione'
      end
      item
        DataType = ftUnknown
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftUnknown
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'inpdf'
      end
      item
        DataType = ftUnknown
        Name = 'LORDO'
      end
      item
        DataType = ftUnknown
        Name = 'netto'
      end
      item
        DataType = ftUnknown
        Name = 'ptasse'
      end
      item
        DataType = ftUnknown
        Name = 'tasse'
      end
      item
        DataType = ftUnknown
        Name = 'accessori'
      end
      item
        DataType = ftUnknown
        Name = 'valuta'
      end
      item
        DataType = ftUnknown
        Name = 'NOTE'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftUnknown
        Name = 'COD_CLI'
      end
      item
        DataType = ftUnknown
        Name = 'FATTA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'rif_referente'
      end
      item
        DataType = ftUnknown
        Name = 'convenzione'
      end
      item
        DataType = ftUnknown
        Name = 'N_POLIZZA_SOSTITUITA'
      end
      item
        DataType = ftUnknown
        Name = 'data_ini_indice'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COD_RAMO'
      end
      item
        DataType = ftUnknown
        Name = 'INTERMEDIATA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'sub_promoter'
      end
      item
        DataType = ftUnknown
        Name = 'PROVVIGIONI'
      end
      item
        DataType = ftUnknown
        Name = 'PERC_PROVVIGIONI'
      end
      item
        DataType = ftUnknown
        Name = 'TIPO_PROVVIGIONI'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftUnknown
        Name = 'RAGIONE_SOC'
      end
      item
        DataType = ftUnknown
        Name = 'DATI1'
      end
      item
        DataType = ftUnknown
        Name = 'DATI2'
      end
      item
        DataType = ftUnknown
        Name = 'DATI3'
      end
      item
        DataType = ftUnknown
        Name = 'DATI4'
      end
      item
        DataType = ftUnknown
        Name = 'DATI5'
      end
      item
        DataType = ftUnknown
        Name = 'SPECIAL'
      end
      item
        DataType = ftUnknown
        Name = 'ORDINE'
      end
      item
        DataType = ftUnknown
        Name = 'DATI_VARI'
      end
      item
        DataType = ftUnknown
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 40
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'RIF_TIPO_POL'
      end
      item
        DataType = ftUnknown
        Name = 'CONTRAENTE'
      end
      item
        DataType = ftUnknown
        Name = 'DECORRENZA'
      end
      item
        DataType = ftUnknown
        Name = 'SCADENZA'
      end
      item
        DataType = ftUnknown
        Name = 'data_emissione'
      end
      item
        DataType = ftUnknown
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftUnknown
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'inpdf'
      end
      item
        DataType = ftUnknown
        Name = 'LORDO'
      end
      item
        DataType = ftUnknown
        Name = 'netto'
      end
      item
        DataType = ftUnknown
        Name = 'ptasse'
      end
      item
        DataType = ftUnknown
        Name = 'tasse'
      end
      item
        DataType = ftUnknown
        Name = 'accessori'
      end
      item
        DataType = ftUnknown
        Name = 'valuta'
      end
      item
        DataType = ftUnknown
        Name = 'NOTE'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftUnknown
        Name = 'COD_CLI'
      end
      item
        DataType = ftUnknown
        Name = 'FATTA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'rif_referente'
      end
      item
        DataType = ftUnknown
        Name = 'convenzione'
      end
      item
        DataType = ftUnknown
        Name = 'N_POLIZZA_SOSTITUITA'
      end
      item
        DataType = ftUnknown
        Name = 'data_ini_indice'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COD_RAMO'
      end
      item
        DataType = ftUnknown
        Name = 'INTERMEDIATA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'sub_promoter'
      end
      item
        DataType = ftUnknown
        Name = 'PROVVIGIONI'
      end
      item
        DataType = ftUnknown
        Name = 'PERC_PROVVIGIONI'
      end
      item
        DataType = ftUnknown
        Name = 'TIPO_PROVVIGIONI'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftUnknown
        Name = 'RAGIONE_SOC'
      end
      item
        DataType = ftUnknown
        Name = 'DATI1'
      end
      item
        DataType = ftUnknown
        Name = 'DATI2'
      end
      item
        DataType = ftUnknown
        Name = 'DATI3'
      end
      item
        DataType = ftUnknown
        Name = 'DATI4'
      end
      item
        DataType = ftUnknown
        Name = 'DATI5'
      end
      item
        DataType = ftUnknown
        Name = 'SPECIAL'
      end
      item
        DataType = ftUnknown
        Name = 'ORDINE'
      end
      item
        DataType = ftUnknown
        Name = 'DATI_VARI'
      end
      item
        DataType = ftUnknown
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object Qins_assicurato: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      '-- start trasaction;'
      ''
      'insert into assicurati'
      '(                                                '
      'dt_last_mod, tipo, cod_polizza, tipo_veicolo, targa,'
      'modello, marca, hp_ql, patente, categoria_pat,'
      
        'data_rilascio, data_scadenza, denominazione, cod_fisc_iva, indir' +
        'izzo,'
      'citta, cap, prov, note, cifra, entrata'
      ')'
      'VALUES'
      '('
      ':oggi, :tipo, :cod_polizza, :tipo_veicolo, :targa,'
      ':modello, :marca, :hp_ql, :patente, :categoria_pat,'
      
        ':data_rilascio, :data_scadenza, :denominazione, :cod_fisc_iva, :' +
        'indirizzo,'
      ':citta, :cap, :prov, :note, :cifra, :entrata'
      ') ;'
      ''
      ''
      '-- commit flush;'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'oggi'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_veicolo'
      end
      item
        DataType = ftUnknown
        Name = 'targa'
      end
      item
        DataType = ftUnknown
        Name = 'modello'
      end
      item
        DataType = ftUnknown
        Name = 'marca'
      end
      item
        DataType = ftUnknown
        Name = 'hp_ql'
      end
      item
        DataType = ftUnknown
        Name = 'patente'
      end
      item
        DataType = ftUnknown
        Name = 'categoria_pat'
      end
      item
        DataType = ftUnknown
        Name = 'data_rilascio'
      end
      item
        DataType = ftUnknown
        Name = 'data_scadenza'
      end
      item
        DataType = ftUnknown
        Name = 'denominazione'
      end
      item
        DataType = ftUnknown
        Name = 'cod_fisc_iva'
      end
      item
        DataType = ftUnknown
        Name = 'indirizzo'
      end
      item
        DataType = ftUnknown
        Name = 'citta'
      end
      item
        DataType = ftUnknown
        Name = 'cap'
      end
      item
        DataType = ftUnknown
        Name = 'prov'
      end
      item
        DataType = ftUnknown
        Name = 'note'
      end
      item
        DataType = ftUnknown
        Name = 'cifra'
      end
      item
        DataType = ftUnknown
        Name = 'entrata'
      end>
    Left = 40
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'oggi'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_veicolo'
      end
      item
        DataType = ftUnknown
        Name = 'targa'
      end
      item
        DataType = ftUnknown
        Name = 'modello'
      end
      item
        DataType = ftUnknown
        Name = 'marca'
      end
      item
        DataType = ftUnknown
        Name = 'hp_ql'
      end
      item
        DataType = ftUnknown
        Name = 'patente'
      end
      item
        DataType = ftUnknown
        Name = 'categoria_pat'
      end
      item
        DataType = ftUnknown
        Name = 'data_rilascio'
      end
      item
        DataType = ftUnknown
        Name = 'data_scadenza'
      end
      item
        DataType = ftUnknown
        Name = 'denominazione'
      end
      item
        DataType = ftUnknown
        Name = 'cod_fisc_iva'
      end
      item
        DataType = ftUnknown
        Name = 'indirizzo'
      end
      item
        DataType = ftUnknown
        Name = 'citta'
      end
      item
        DataType = ftUnknown
        Name = 'cap'
      end
      item
        DataType = ftUnknown
        Name = 'prov'
      end
      item
        DataType = ftUnknown
        Name = 'note'
      end
      item
        DataType = ftUnknown
        Name = 'cifra'
      end
      item
        DataType = ftUnknown
        Name = 'entrata'
      end>
  end
  object Qins_link_pol_assic: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into polizze_assicurati'
      '(                                                '
      'rif_cod_polizza, rif_cod_assicurato'
      ')'
      'VALUES'
      '('
      ':cod_polizza, :cod_assicurati'
      ')'
      '                                    ')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'cod_assicurati'
      end>
    Left = 168
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'cod_assicurati'
      end>
  end
  object Qins_garanzie: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into garanzie'
      '('
      'MASSIMALE, PREMIO, DATA_IN, RIF_COD_POLIZZA, RIF_GAR_BASE,'
      'FRANCHIGIA, PERCENTUALE'
      ')'
      'VALUES'
      '('
      ':MASSIMALE, :PREMIO, :DATA_IN, :COD_POLIZZA, :RIF_GAR_BASE,'
      ':FRANCHIGIA, :PERCENTUALE'
      ')')
    Params = <
      item
        DataType = ftUnknown
        Name = 'MASSIMALE'
      end
      item
        DataType = ftUnknown
        Name = 'PREMIO'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_IN'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_GAR_BASE'
      end
      item
        DataType = ftUnknown
        Name = 'FRANCHIGIA'
      end
      item
        DataType = ftUnknown
        Name = 'PERCENTUALE'
      end>
    Left = 112
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'MASSIMALE'
      end
      item
        DataType = ftUnknown
        Name = 'PREMIO'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_IN'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_GAR_BASE'
      end
      item
        DataType = ftUnknown
        Name = 'FRANCHIGIA'
      end
      item
        DataType = ftUnknown
        Name = 'PERCENTUALE'
      end>
  end
  object Qassicurati: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from slp_tassicurati '
      'where cod_polizza=:cod_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    Left = 136
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
  object Qgaranzie: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from slp_tgaranzie'
      'where rif_cod_polizza=:cod_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    Left = 264
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
  object Qpolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from slp_tpolizze'
      'where cod_polizza=:cod_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    Left = 200
    Top = 17
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
  object QpolizzaPortaf: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from polizze'
      'where n_polizza=:n_polizza'
      'and rif_compagnia=1')
    Params = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end>
    Left = 272
    Top = 81
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end>
  end
  object QassicuratiPortaf: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select max(cod_assicurati) as codass '
      'from assicurati '
      'where cod_polizza=:cod_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    Left = 368
    Top = 113
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
  object Qcambia_stato: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update slp_tpolizze'
      'set status= :status, data_perfez= :data_perfez'
      'where cod_polizza=:cod_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'status'
      end
      item
        DataType = ftUnknown
        Name = 'data_perfez'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    Left = 344
    Top = 201
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'status'
      end
      item
        DataType = ftUnknown
        Name = 'data_perfez'
      end
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
  object Qcancella_logico: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update slp_tpolizze'
      'set status= '#39'C'#39' , data_canc = CURRENT_TIMESTAMP()'
      'where cod_polizza=:cod_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    Left = 248
    Top = 249
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
end
