unit UDMDatiPolizzaPP;

interface

uses
  SysUtils, Classes, UDMDatiBasePolizza, dbisamtb, Data.DB, UTipoSalvataggioDocumento;

type
  TDMDatiPolizzaPP = class(TDMDatiBasePolizza)
  private
    { Private declarations }
  protected
    procedure RiprendiAssicurato(QAssPol, DataSetAssicurati: TDataSet); override;

    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); override;
    procedure SalvaFamiliari(DataSetPolizza: TDataSet); override;
    procedure SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer); override;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); override;
    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): integer; override;

  public
    { Public declarations }
  end;

function DMDatiPolizzaPP: TDMDatiPolizzaPP;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule;

function DMDatiPolizzaPP: TDMDatiPolizzaPP;
begin
  Result := TDMDatiPolizzaPP(UniMainModule.GetModuleInstance(TDMDatiPolizzaPP));
end;

{ TDMDatiPolizzaPP }

procedure TDMDatiPolizzaPP.RiprendiAssicurato(QAssPol, DataSetAssicurati: TDataSet);
var
  st: TMemoryStream;
begin
  inherited;
  if not(DataSetAssicurati.State in [dsInsert, dsEdit]) then
    DataSetAssicurati.Edit;

  DataSetAssicurati.FieldByName('SELECTEDGARANZIE').asString := QAssPol.FieldByName('DENOMINAZIONE').asString;
  st := TMemoryStream.Create;
  try
    (QAssPol.FieldByName('GARANZIE') as TBlobField).SaveToStream(st);
    st.Position := 0;
    (DataSetAssicurati.FieldByName('SELGARSTREAM') as TBlobField).LoadFromStream(st);
    DataSetAssicurati.Post;
  finally
    st.free;
  end;

end;

procedure TDMDatiPolizzaPP.SalvaAssicurato(DataSetAssicurati: TDataSet);
var
  st: TMemoryStream;
begin
  inherited;
  QInsAssicurato.ParamByName('DENOMINAZIONE').asString := DataSetAssicurati.FieldByName('SELECTEDGARANZIE').asString;
  st                                                   := TMemoryStream.Create;
  try
    (DataSetAssicurati.FieldByName('SelGarStream') as TBlobField).SaveToStream(st);
    st.Position := 0;
    QInsAssicurato.ParamByName('GARANZIE').LoadFromStream(st, ftTypedBinary);
    QInsAssicurato.ExecSQL;
  finally
    st.free;
  end;
end;

procedure TDMDatiPolizzaPP.SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer);
var
  massimaleGaranzia: string;

begin
  if DataSetPolizza.FieldByName('PREMIONETTOSLPGARANZIA' + ix.ToString).asCurrency <> 0.0 then
  begin
    QInsAssicurato.ParamByName('DENOMINAZIONE').asString :=
      DataSetPolizza.FieldByName('SELECTEDGARANZIA' + ix.ToString).asString;
    QInsAssicurato.ParamByName('TIPO').asString := 'C'; // Contratto

    QInsAssicurato.ParamByName('COD_FISC_IVA').asString :=
      DataSetPolizza.FieldByName('NPOLIZZAGARANZIA' + ix.ToString).asString;
    QInsAssicurato.ParamByName('CITTA').asString :=
      DataSetPolizza.FieldByName('COMPEMITTGARANZIA' + ix.ToString).asString;
    QInsAssicurato.ParamByName('INDIRIZZO').asString :=
      DataSetPolizza.FieldByName('AGENZIAGARANZIA' + ix.ToString).asString;
    QInsAssicurato.ParamByName('PREMIO').asCurrency :=
      DataSetPolizza.FieldByName('PREMIOANNUOLORDOGARANZIA' + ix.ToString).asCurrency;
    QInsAssicurato.ParamByName('DATA_RILASCIO').asDateTime :=
      DataSetPolizza.FieldByName('DECORRENZAGARANZIA' + ix.ToString).asDateTime;
    QInsAssicurato.ParamByName('DATA_SCADENZA').asDateTime :=
      DataSetPolizza.FieldByName('SCADENZAGARANZIA' + ix.ToString).asDateTime;
    QInsAssicurato.ParamByName('PREMIO_SLP').asCurrency :=
      DataSetPolizza.FieldByName('PREMIONETTOSLPGARANZIA' + ix.ToString).asCurrency;
    QInsAssicurato.ParamByName('MODELLO').asString :=
      DataSetPolizza.FieldByName('SELECTEDGARANZIA' + ix.ToString).asString;

    QInsAssicurato.ParamByName('NOTE').Clear;
    QInsAssicurato.ParamByName('ENTRATA').Clear;
    QInsAssicurato.ParamByName('USCITA').Clear;
    QInsAssicurato.ParamByName('CAUSALE_USCITA').Clear;
    QInsAssicurato.ParamByName('SOSTITUITO_DA').Clear;
    QInsAssicurato.ParamByName('PATENTE').Clear;
    QInsAssicurato.ParamByName('CATEGORIA_PAT').Clear;
    QInsAssicurato.ParamByName('DATA_RILASCIO').Clear;
    QInsAssicurato.ParamByName('RILASCIATA_DA').Clear;
    QInsAssicurato.ParamByName('DATA_SCADENZA').Clear;
    QInsAssicurato.ParamByName('TIPO_VEICOLO').Clear;
    QInsAssicurato.ParamByName('TARGA').Clear;
    QInsAssicurato.ParamByName('MARCA').Clear;
    QInsAssicurato.ParamByName('HP_QL').Clear;
    // QInsAssicurato.ParamByName('RIF_COD_TIPO_ASSICURATO').AsInteger :=
    // DataSetAssicurati.FieldByName('RIFCODTIPOASSICURATO').AsInteger;
    // QInsAssicurato.ParamByName('TIPO').AsString := DataSetAssicurati.FieldByName('TIPO').AsString;
    // QInsAssicurato.ParamByName('COD_POLIZZA').AsString :=  DataSetAssicurati.FieldByName('CITTA').AsString;
    QInsAssicurato.ParamByName('DATA_SCAD_REVISIONE').Clear;
    QInsAssicurato.ParamByName('CLASSE').Clear;
    QInsAssicurato.ParamByName('TELAIO').Clear;
    QInsAssicurato.ParamByName('CC').Clear;
    QInsAssicurato.ParamByName('DATA_IMMATR').Clear;
    QInsAssicurato.ParamByName('PREMIO').Clear;

    massimaleGaranzia := DataSetPolizza.FieldByName('MASSIMALEGARANZIA' + ix.ToString).asString;
    massimaleGaranzia := StringReplace(massimaleGaranzia, '.', '', [rfReplaceAll]);

    QInsAssicurato.ParamByName('MASSIMALE').asCurrency := StrToCurr(massimaleGaranzia);

    QInsAssicurato.ParamByName('PREMIO_SLP').asCurrency :=
      DataSetPolizza.FieldByName('PREMIONETTOSLPGARANZIA' + ix.ToString).asCurrency;
    QInsAssicurato.ParamByName('ID_GEN_SLP_AGE').asInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

    QInsAssicurato.ExecSQL;
  end;
end;

procedure TDMDatiPolizzaPP.SalvaFamiliari(DataSetPolizza: TDataSet);
begin
  // for i := 1 to 2 do
  // SalvaFamiliare(DataSetPolizza, i);

end;

procedure TDMDatiPolizzaPP.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  inherited;
  QInsGaranzia.ExecSQL;
end;

function TDMDatiPolizzaPP.SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio): integer;
begin
  SalvaBasePolizza(DataSetPolizza, DataPreventivo, TipoSalvataggio);
  QInsPolizza.ParamByName('NUMERO_ASSICURATI').asInteger := DataSetPolizza.FieldByName('NUMEROASSICURATI').asInteger;
  // if DataSetPolizza.FindField('NUMEROASSICURATII') <> nil then
  // QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger  := 1;
  QInsPolizza.ExecSQL;

  Result := getLastCodPolizza;

end;

initialization

RegisterClass(TDMDatiPolizzaPP);

end.
