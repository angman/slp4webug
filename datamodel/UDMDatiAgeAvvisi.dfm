inherited DMDatiAgeAvvisi: TDMDatiAgeAvvisi
  OldCreateOrder = True
  Height = 323
  Width = 402
  inherited QGetLastID: TDBISAMQuery
    SessionName = 'SessionAvvisi_1'
    Left = 26
    Top = 64
  end
  inherited tblTemp: TDBISAMTable
    SessionName = 'SessionAvvisi_1'
    Left = 144
    Top = 72
  end
  object QAvvisoAgenziaStopLogin: TDBISAMQuery
    DatabaseName = 'DBaccessoAvvisi'
    SessionName = 'SessionAvvisi_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select descrizione, dt_operativita, dt_fine, n_ricorrenze, n_let' +
        'ture, tipo, testo, livello_msg '
      'from avvisi_age_msg msg'
      'left join avvisi_age age on msg.Cod_msg = age.Rif_cod_msg'
      'where dt_operativita <= CURRENT_DATE '
      '   and dt_fine >= CURRENT_DATE '
      '   and (age.cod_age = :CodAgenzia or per_tutti='#39'S'#39')'
      '  and cod_utente=:CodUtente'
      '  and  livello_msg = 0')
    Params = <
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
    Left = 72
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
  end
  object QUpdDataLastLettura: TDBISAMQuery
    DatabaseName = 'DBaccessoAvvisi'
    SessionName = 'SessionAvvisi_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update avvisi_age'
      'set data_ultima_lettura = current_timestamp'
      '--      n_letture = n_letture + 1'
      'where rif_cod_msg = :cod_msg'
      'and cod_age = :CodAgenzia'
      'and '
      
        '(data_ultima_lettura is null or (HOURSFROMMSECS(current_timestam' +
        'p - data_ultima_lettura) > 4))'
      ' and cod_utente=:CodUtente'
      ''
      '-- da provare'
      '--update avvisi_age'
      '--set data_ultima_lettura = current_timestamp,'
      '--      n_letture = n_letture + 1'
      
        '--from avvisi_age  left outer join avvisi_age_msg  on avvisi_age' +
        '_msg.cod_msg = avvisi_age.rif_cod_msg'
      '--where rif_cod_msg = :cod_msg'
      '--and   n_letture < n_ricorrenze'
      
        '--and data_ultima_lettura is null or HOURSFROMMSECS(current_time' +
        'stamp - data_ultima_lettura) > 4')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_msg'
      end
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
    Left = 200
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_msg'
      end
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
  end
  object QUpdDataFirstLettura: TDBISAMQuery
    DatabaseName = 'DBaccessoAvvisi'
    SessionName = 'SessionAvvisi_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update avvisi_age'
      'set data_prima_lettura = current_timestamp'
      '--       n_letture = n_letture + 1'
      'where rif_cod_msg = :cod_msg'
      'and cod_age = :CodAgenzia'
      '      and data_prima_lettura is null'
      '  and cod_utente=:CodUtente')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_msg'
      end
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
    Left = 192
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_msg'
      end
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
  end
  object QAvvvisiCriticiAgenzia: TDBISAMQuery
    DatabaseName = 'DBaccessoAvvisi'
    SessionName = 'SessionAvvisi_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select Cod_msg, descrizione, dt_operativita, dt_fine, n_ricorren' +
        'ze, n_letture, tipo, testo, livello_msg '
      'from avvisi_age_msg msg'
      'left join avvisi_age age on msg.Cod_msg = age.Rif_cod_msg'
      'where dt_operativita <= CURRENT_DATE '
      'and'
      '( '
      '   (age.cod_age = :CodAgenzia '
      '   and n_letture <  n_ricorrenze'
      '   and livello_msg = 1'
      '  and cod_utente=:CodUtente'
      ''
      '    )'
      '   or  '
      '   (per_tutti='#39'S'#39
      '   and dt_fine>= CURRENT_DATE '
      '   and livello_msg = 1)    '
      ')')
    Params = <
      item
        DataType = ftString
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
    Left = 72
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
  end
  object QAvvisiAgenzia: TDBISAMQuery
    DatabaseName = 'DBaccessoAvvisi'
    SessionName = 'SessionAvvisi_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select Cod_msg, descrizione, dt_operativita, dt_fine, n_ricorren' +
        'ze, n_letture, tipo, testo, livello_msg '
      'from avvisi_age_msg msg'
      'join avvisi_age age on msg.Cod_msg = age.Rif_cod_msg'
      'where dt_operativita <= CURRENT_DATE '
      
        '-- questi messaggi devono essere ri'#242'leggibile quante volte si vu' +
        'ole'
      'and'
      '( '
      '   (age.cod_age = :CodAgenzia '
      '   and livello_msg > 1)'
      '   or  '
      '   (per_tutti='#39'S'#39
      '   and livello_msg > 1)    '
      ')')
    Params = <
      item
        DataType = ftString
        Name = 'CodAgenzia'
      end>
    Left = 64
    Top = 232
    ParamData = <
      item
        DataType = ftString
        Name = 'CodAgenzia'
      end>
  end
  object QFindAvvisoByCod: TDBISAMQuery
    DatabaseName = 'DBaccessoAvvisi'
    SessionName = 'SessionAvvisi_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select cod_msg, testo from avvisi_age_msg msg'
      'where cod_msg = :CodMsg')
    Params = <
      item
        DataType = ftUnknown
        Name = 'CodMsg'
      end>
    Left = 184
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CodMsg'
      end>
  end
  object DBaccessoAvvisi: TDBISAMDatabase
    EngineVersion = '4.37 Build 3'
    DatabaseName = 'DBaccessoAvvisi'
    Directory = 'C:\SLP\Slp4Web'
    SessionName = 'SessionAvvisi_1'
    Left = 147
    Top = 8
  end
  object SessionAvvisi: TDBISAMSession
    EngineVersion = '4.37 Build 3'
    AutoSessionName = True
    PrivateDir = 'c:\SLP\Slp4Web\SLPDATI\temp\'
    RemoteEncryptionPassword = 'elevatesoft'
    RemoteAddress = '127.0.0.1'
    Left = 40
    Top = 10
  end
  object QUpdN_letture: TDBISAMQuery
    DatabaseName = 'DBaccessoAvvisi'
    SessionName = 'SessionAvvisi_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update avvisi_age'
      'set n_letture = n_letture + 1'
      'where rif_cod_msg = :cod_msg'
      'and cod_age = :CodAgenzia'
      '  and cod_utente=:CodUtente')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_msg'
      end
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
    Left = 304
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_msg'
      end
      item
        DataType = ftUnknown
        Name = 'CodAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'CodUtente'
      end>
  end
end
