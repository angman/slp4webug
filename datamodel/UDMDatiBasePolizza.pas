unit UDMDatiBasePolizza;

interface

uses
  System.SysUtils, System.Classes, UDMMaster, dbisamtb, Data.DB, FireDAC.Comp.Client, UTipoSalvataggioDocumento;

type

  TDMDatiBasePolizza = class(TDMMaster)
    QPolizze: TDBISAMQuery;
    QSLP_TPolizze: TDBISAMQuery;
    QInsPolizza: TDBISAMQuery;
    QInsAssicurato: TDBISAMQuery;
    QInsGaranzia: TDBISAMQuery;
    Qgar_base: TDBISAMQuery;
    Qgar_baseT: TDBISAMQuery;
    QUpdMemoPolizza: TDBISAMQuery;
    QUpdPolizza: TDBISAMQuery;
    QDelAssicurati: TDBISAMQuery;
    QDelGaranzie: TDBISAMQuery;
    QDelPolizza: TDBISAMQuery;
    QGetLastCodPolizza: TDBISAMQuery;
    Qupd_varfrp_pol: TDBISAMQuery;
    QFindPolizzaByCod: TDBISAMQuery;
    QAssicuratoPolizza: TDBISAMQuery;
    QGaranziaPolizza: TDBISAMQuery;
  private
    FinPDF: boolean;
    FDescrUtente: string;
    { Private declarations }
    procedure SalvaAssicurati(DataSetAssicurati: TDataSet);
    procedure setDescrUtente(const Value: string);
  protected
    procedure RiprendiBasePolizza(QSLPTPolizze, DataSetPolizza, QAssPol: TDataSet);
    procedure RiprendiPolizza(QSLPTPolizze, DataSetPolizza, QAssPol: TDataSet); virtual; abstract;
    procedure RiprendiAssicurati(QAssPol, DataSetAssicurati: TDataSet); virtual;
    procedure RiprendiAssicurato(QAssPol, DataSetAssicurati: TDataSet); virtual;
    function  RiprendiFamiliari(QAssPol, DataSetPolizza, DataSetAssicurati: TDataSet): boolean; virtual;
    procedure RiprendiFamiliare(QAssPol, DataSetPolizza: TDataSet; ix: Integer); virtual; abstract;
    procedure RiprendiGaranzie(QGarPol, DataSetPolizza, DataSetGaranzie: TDataSet); virtual;
    procedure RiprendiGaranzia(QGarPol, DataSetPolizza, DataSetGaranzie: TDataSet); virtual;

    procedure SalvaBasePolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza);
    function getLastCodPolizza: Integer;
    // --------------------------------------------------------------------------------------------------------------
    // Metodi da ridefinire nelle classi figlie
    procedure SalvaFamiliare(DataSetPolizza: TDataSet; ix: Integer); virtual;
    procedure SalvaGaranzie(DataSetGaranzie: TDataSet); virtual;
    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): Integer; virtual; abstract;

    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); virtual;
    procedure SalvaFamiliari(DataSetPolizza: TDataSet); virtual;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); virtual;
    // -------------------------------------------------------------------------------------------------------------

    procedure SalvaDatasetToMemoField(tmpTable: TFDMemTable; const ParamName: string);
    procedure LoadDatasetFromMemoField(tmpTable: TFDMemTable; const FieldName: string);
  public
    { Public declarations }
    function cercaGaranzia(xcod_tipo_contratto: Integer; xsigla: string): boolean; overload;
    function cercaGaranzia(sigla_contratto: string; xsigla: string): boolean; overload;
    procedure SalvaDatiDocumento(DataSetPolizza, DataSetAssicurati, DataSetGaranzie: TFDMemTable;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza);
    procedure CancellaDocumento(CodPolizza: Integer; execCommit: boolean = false);
    procedure UpdDatiStreams(DataSetPolizza, DataSetAssicurati, DataSetGaranzie: TFDMemTable; CodPolizza: Integer);
    function RiprendiDocumento(DataSetPolizza, DataSetAssicurati, DataSetGaranzie: TFDMemTable; CodPolizza: Integer)
      : TTipoSalvataggio;

    procedure RiprendiDocumentoFromQueries(DataSetPolizza, DataSetAssicurati, DataSetGaranzie: TFDMemTable;
      CodPolizza: Integer);

    function controlladata(Data: TdateTime): TDateTime;

    procedure salvaInPrintHouse(CodPolizza: Integer);

    property inPDF: boolean read FinPDF write FinPDF;

    property DescrUtente: string write setDescrUtente;

  end;

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

uses MainModule, UdmdatiAge, FireDAC.Stan.Intf, UCodiciErroriPolizza, System.Math, libSLP, libreria;

{$R *.dfm}

{ TDMDatiBasePolizza }
function TDMDatiBasePolizza.cercaGaranzia(xcod_tipo_contratto: Integer; xsigla: string): boolean;
begin
  Qgar_base.Close;
  Qgar_base.ParamByName('COD_TIPO_CONTRATTO').asInteger := xcod_tipo_contratto;
  Qgar_base.ParamByName('SIGLA').asString               := xsigla;
  Qgar_base.Open;
  Result := not Qgar_base.IsEmpty;
end;

procedure TDMDatiBasePolizza.CancellaDocumento(CodPolizza: Integer; execCommit: boolean);
var
  tablesList: TStringList;
  lDatabase: TDBISAMDatabase;

begin
  tablesList := TStringList.Create;
  try
    lDatabase := QInsPolizza.DBSession.Databases[0];
    tablesList.Append('SLP_TPOLIZZE');
    tablesList.Append('SLP_TASSICURATI');
    tablesList.Append('SLP_TGARANZIE');
    if not lDatabase.InTransaction then
      lDatabase.StartTransaction(tablesList);
    try
      QDelGaranzie.ParamByName('COD_POLIZZA').asInteger := CodPolizza;
      QDelGaranzie.ExecSQL;
      QDelAssicurati.ParamByName('COD_POLIZZA').asInteger := CodPolizza;
      QDelAssicurati.ExecSQL;
      QDelPolizza.ParamByName('COD_POLIZZA').asInteger := CodPolizza;
      QDelPolizza.ExecSQL;
      if execCommit then
        lDatabase.Commit(True);
    except
      on EDBISAMEngineError do
        if execCommit then
          lDatabase.Rollback;

    end;
  finally
    tablesList.Free;
  end;
end;

function TDMDatiBasePolizza.cercaGaranzia(sigla_contratto, xsigla: string): boolean;
begin
  Qgar_baseT.Close;
  Qgar_baseT.ParamByName('SIGLA_SLP').asInteger := sigla_contratto.ToInteger;
  Qgar_baseT.ParamByName('SIGLA').asString      := xsigla;
  Qgar_baseT.Open;
  Result := not Qgar_baseT.IsEmpty;

end;


function TDMDatiBasePolizza.controlladata(Data: TdateTime): TDateTime;
begin
  // per evitare le date 30/12/1899
  if year(data)>1980 then result:= data
  else result := 0.0;
end;


function TDMDatiBasePolizza.getLastCodPolizza: Integer;
begin
  QGetLastCodPolizza.Open;
  Result := QGetLastCodPolizza.FieldByName('LAST_CODPOLIZZA').asInteger;
  QGetLastCodPolizza.Close;
  // Result := getLastID('POLIZZE');
end;

procedure TDMDatiBasePolizza.SalvaDatasetToMemoField(tmpTable: TFDMemTable; const ParamName: string);
var
  stream: TMemorystream;

begin
  if not tmpTable.IsEmpty then
  begin
    stream := TMemorystream.Create;
    try
      tmpTable.SaveToStream(stream, sfBinary);
      QUpdMemoPolizza.ParamByName(ParamName).LoadFromStream(stream, ftBlob);
    finally
      stream.Free;
    end;
  end;
end;

function TDMDatiBasePolizza.RiprendiDocumento(DataSetPolizza, DataSetAssicurati, DataSetGaranzie: TFDMemTable;
  CodPolizza: Integer): TTipoSalvataggio;

  function getTipoSalvataggioFromStatus: TTipoSalvataggio;
  var
    lStatus: string;
  begin
    lStatus := Trim(UniMainModule.DMBozzePreventivi.QPosizionaBozzaPreventivo.FieldByName('STATUS').asString);
    if (lStatus = '') or (lStatus = 'P') then
      Result := tsPolizza
    else
      if lStatus = 'B' then
        Result := tsBozza
      else
        if lStatus = 'F' then
          Result := tsPreventivo
        else
          if lStatus = 'FD' then
            Result := tsPreventivoDir
          else
            if lStatus = 'PD' then
              Result := tsPolizzaDir
            else
              Result := tsNone;
  end;

  procedure OpenDataset(AMemTable: TFDMemTable);
  begin
    AMemTable.DisableControls;
    try
      AMemTable.Open;
      AMemTable.Edit;
    finally
      AMemTable.EnableControls;
    end;

  end;

  procedure posizionaReferente;
  begin
    if DataSetPolizza.FieldByName('RIFREFERENTE').asInteger > 0 then
      UniMainModule.DMdatiAgeClienti.posizionaReferente(DataSetPolizza.FieldByName('RIFREFERENTE').asInteger);

  end;

begin
  Result := UniMainModule.DMBozzePreventivi.PosizionaBozzaPreventivo(CodPolizza);
  if Result = tsPreventivoDir then
  begin
    RiprendiDocumentoFromQueries(DataSetPolizza, DataSetAssicurati, DataSetGaranzie, CodPolizza);
    posizionaReferente;
    UniMainModule.DMdatiAgeClienti.ApriTelefoniDocumenti(DataSetPolizza.FieldByName('RIFCODCLI').asInteger)
  end
  else
    if Result <> tsNone then
    begin
      try
        LoadDatasetFromMemoField(DataSetPolizza, 'MEM_POLIZZA');
        DataSetPolizza.Edit;
        DataSetPolizza.FieldByName('SAVCODPOLIZZA').asInteger := CodPolizza;
        DataSetPolizza.FieldByName('NPOLIZZA').asString := '';
        DataSetPolizza.Post;
        LoadDatasetFromMemoField(DataSetAssicurati, 'MEM_ASSICURATI');
        LoadDatasetFromMemoField(DataSetGaranzie, 'MEM_GARANZIE');
        posizionaReferente;
        UniMainModule.DMdatiAgeClienti.ApriTelefoniDocumenti(DataSetPolizza.FieldByName('RIFCODCLI').asInteger);
      finally
        OpenDataset(DataSetPolizza);
        OpenDataset(DataSetAssicurati);
      end;
    end;

end;

procedure TDMDatiBasePolizza.RiprendiDocumentoFromQueries(DataSetPolizza, DataSetAssicurati,
  DataSetGaranzie: TFDMemTable; CodPolizza: Integer);

  function PosizionaPolizza: boolean;
  begin
    QFindPolizzaByCod.Close;
    QFindPolizzaByCod.ParamByName('CODPOLIZZA').asInteger := CodPolizza;
    QFindPolizzaByCod.Open;
    Result := not QFindPolizzaByCod.IsEmpty;
  end;

  function PosizionaAssicurato: boolean;
  begin
    QAssicuratoPolizza.Close;
    QAssicuratoPolizza.ParamByName('COD_POLIZZA').asInteger := CodPolizza;
    QAssicuratoPolizza.Open;
    Result := not QAssicuratoPolizza.IsEmpty;
  end;

  function PosizionaGaranzia: boolean;
  begin
    QGaranziaPolizza.Close;
    QGaranziaPolizza.ParamByName('CODPOLIZZA').asInteger := CodPolizza;
    QGaranziaPolizza.Open;
    Result := not QGaranziaPolizza.IsEmpty;
  end;

begin
  // MB 04112020
  if not (DataSetPolizza.State in [dsEdit, dsInsert]) then DataSetPolizza.Edit;
  if not (DataSetAssicurati.State in [dsEdit, dsInsert]) then DataSetAssicurati.Edit;

  if PosizionaPolizza then
  begin
    PosizionaAssicurato;
    RiprendiPolizza(QFindPolizzaByCod, DataSetPolizza, QAssicuratoPolizza);
  end;

  if PosizionaAssicurato then
  begin
    if not RiprendiFamiliari(QAssicuratoPolizza, DataSetPolizza, DataSetAssicurati) then
      RiprendiAssicurati(QAssicuratoPolizza, DataSetAssicurati);
  end;
  DataSetAssicurati.First;

  if PosizionaGaranzia then
  begin
    RiprendiGaranzie(QGaranziaPolizza, DataSetPolizza, DataSetGaranzie);
  end;
  DataSetPolizza.Edit;
  DataSetAssicurati.Edit;
  DataSetGaranzie.edit;
  UniMainModule.DMDatiTempPolizza.calcolaPremio;
end;

function TDMDatiBasePolizza.RiprendiFamiliari(QAssPol, DataSetPolizza, DataSetAssicurati: TDataSet): boolean;
var
  i: Integer;
begin
  Result := false;
  i      := 0;
  RiprendiAssicurato(QAssPol, DataSetAssicurati);
  QAssPol.Next;
  while not QAssPol.Eof do
  begin
    inc(i);
    RiprendiFamiliare(QAssPol, DataSetPolizza, i);
    Result := True;
    QAssPol.Next;
  end;
end;

procedure TDMDatiBasePolizza.RiprendiGaranzia(QGarPol, DataSetPolizza, DataSetGaranzie: TDataSet);
  procedure SetFieldChecked;
  var
    FieldName: string;
    FieldChangeEvt: TFieldNotifyEvent;
    FieldFormaChangeEvt: TFieldNotifyEvent;
    Field: TField;
  begin
    if assigned(DataSetPolizza.FindField(QGarPol.FieldByName('NFIELDTEMPTAB').asString)) then
    begin
      FieldName := QGarPol.FieldByName('NFIELDTEMPTAB').asString;
      begin
        Field               := DataSetPolizza.FindField(FieldName);
        FieldChangeEvt      := nil;
        FieldFormaChangeEvt := nil;
        try
          if assigned(Field.OnChange) then
          begin
            FieldChangeEvt := Field.OnChange;
            Field.OnChange := nil;
          end;
          DataSetPolizza.Edit;
          if UpperCase(FieldName) = 'SIGLAMASSIMALE' then
          begin
            FieldFormaChangeEvt                          := FieldFormaChangeEvt;
            DataSetPolizza.FieldByName('FORMA').OnChange := nil;
            try
              DataSetPolizza.FieldByName('FORMA').asString := QGarPol.FieldByName('FORMA').asString;
            finally
              DataSetPolizza.FieldByName('FORMA').OnChange := FieldFormaChangeEvt;
            end;
            DataSetPolizza.FieldByName('SIGLAMASSIMALE').asString := QGarPol.FieldByName('SIGLA').asString
          end
          else
            if UpperCase(FieldName) = 'SIGLARIMBORSOSPESEPATPUNTI' then     // SIGLARIMBORSOSPESEPATPUNTI
              DataSetPolizza.FieldByName('SIGLARIMBORSOSPESEPATPUNTI').asString := QGarPol.FieldByName('SIGLA').asString
            else
               if UpperCase(FieldName) = 'DIFPENVPRIVATA' then
                 DataSetPolizza.FieldByName('DIFPENVPRIVATA').asBoolean := True //QGarPol.FieldByName('SIGLA').asString
               else
                 DataSetPolizza.FieldByName(QGarPol.FieldByName('NFIELDTEMPTAB').asString).AsBoolean := True;
          DataSetPolizza.Post;


        finally
          if assigned(FieldChangeEvt) then
            Field.OnChange := FieldChangeEvt;
        end;
      end;
    end;

  end;

begin
  SetFieldChecked;

  DataSetGaranzie.Append;

  DataSetGaranzie.Edit;
  DataSetGaranzie.FieldByName('MASSIMALE').AsCurrency := QGarPol.FieldByName('MASSIMALE').AsCurrency;
  DataSetGaranzie.FieldByName('PREMIO').AsCurrency    := QGarPol.FieldByName('PREMIO').AsCurrency;
  DataSetGaranzie.FieldByName('DATAIN').AsDateTime    := QGarPol.FieldByName('DATA_IN').AsDateTime;
  DataSetGaranzie.FieldByName('DATAOUT').AsDateTime   := QGarPol.FieldByName('DATA_OUT').AsDateTime;
  DataSetGaranzie.FieldByName('VALOREASSICURATO').AsCurrency := QGarPol.FieldByName('VALORE_ASSICURATO').AsCurrency;

  DataSetGaranzie.FieldByName('VALOREIF').AsCurrency       := QGarPol.FieldByName('VALORE_IF').AsCurrency;
  DataSetGaranzie.FieldByName('RIFGARBASE').asInteger      := QGarPol.FieldByName('RIF_GAR_BASE').asInteger;
  DataSetGaranzie.FieldByName('FRANCHIGIA').asString       := QGarPol.FieldByName('FRANCHIGIA').asString;
  DataSetGaranzie.FieldByName('RIFCODMASSIMALE').asInteger := QGarPol.FieldByName('RIF_COD_MASSIMALE').asInteger;
  DataSetGaranzie.FieldByName('PERCENTUALE').AsCurrency    := QGarPol.FieldByName('PERCENTUALE').AsCurrency;
  DataSetGaranzie.FieldByName('SIGLAGARANZIA').asString    := QGarPol.FieldByName('SIGLA').asString;
  DataSetGaranzie.Post;
end;

procedure TDMDatiBasePolizza.RiprendiGaranzie(QGarPol, DataSetPolizza, DataSetGaranzie: TDataSet);
begin
  QGarPol.First;
  while not QGarPol.Eof do
  begin
    RiprendiGaranzia(QGarPol, DataSetPolizza, DataSetGaranzie);
    QGarPol.Next;
  end;
end;

procedure TDMDatiBasePolizza.LoadDatasetFromMemoField(tmpTable: TFDMemTable; const FieldName: string);
var
  stream: TMemorystream;

begin
  if not UniMainModule.DMBozzePreventivi.QPosizionaBozzaPreventivo.FieldByName(FieldName).IsNull then
  begin
    stream := TMemorystream.Create;
    tmpTable.DisableControls;
    try
      // tmpTable.EmptyDataSet;
      TBlobField(UniMainModule.DMBozzePreventivi.QPosizionaBozzaPreventivo.FieldByName(FieldName)).SaveToStream(stream);
      stream.Position := 0;
      tmpTable.LoadFromStream(stream, sfBinary);
    finally
      stream.Free;
      tmpTable.EnableControls;
    end;
  end;

end;

procedure TDMDatiBasePolizza.SalvaDatiDocumento(DataSetPolizza, DataSetAssicurati, DataSetGaranzie: TFDMemTable;
  TipoSalvataggio: TTipoSalvataggio);
var
  Database: TDBISAMDatabase;
  lastCodPolizza: Integer;
  lDataPreventivo: TDateTime;
  tablesList: TStringList;

  procedure AggiornaCodicePolizza;
  begin
    if not(DataSetPolizza.State in [dsInsert, dsEdit]) then
      DataSetPolizza.Edit;
    DataSetPolizza.FieldByName('CODPOLIZZA').asInteger := lastCodPolizza;
    UniMainModule.NumPolizza                           := QGetLastCodPolizza.ToString;
    // DataSetPolizza.Post;
  end;

begin
  UniMainModule.NumPolizza := '';

  // Log('DMDatiBasePolizza.SalvaDatiDocumento prima di staccaNumero (2) '+DateTimeToStr(now));

  case TipoSalvataggio of
    tsPolizza:
      begin
        UniMainModule.NumPolizza := UniMainModule.DMdatiAge.staccaNumeroPolizza(UniMainModule.SiglaPolizza,
          True).ToString;
        UniMainModule.DMdatiAgePolizze.CheckNumeroPolizzaDuplicato(UniMainModule.NumPolizza);
      end
  else
    UniMainModule.NumPolizza := '';
  end;

  // Log('DMDatiBasePolizza.SalvaDatiDocumento dopo di staccaNumero (3) '+DateTimeToStr(now));

  tablesList := TStringList.Create;
  try
    tablesList.Append('SLP_TPOLIZZE');
    tablesList.Append('SLP_TASSICURATI');
    tablesList.Append('SLP_TGARANZIE');
    tablesList.Append('CLIENTI');

    Database := QInsPolizza.DBSession.Databases[0];
    if not Database.InTransaction then
      Database.StartTransaction(tablesList);
    try

      lDataPreventivo := MinDateTime;
      case TipoSalvataggio of
        tsBozza, tsPreventivo, tsNuovoPreventivo:
          begin
            lDataPreventivo := DataSetPolizza.FieldByName('DT_PREVENTIVO').AsDateTime;
            CancellaDocumento(DataSetPolizza.FieldByName('SAVCODPOLIZZA').asInteger, false);
          end;
      else
        ;
      end;

      lastCodPolizza := SalvaPolizza(DataSetPolizza, lDataPreventivo, TipoSalvataggio);
      UniMainModule.DMdatiAgeClienti.AggProfessioneAttivita(DataSetPolizza.FieldByName('RIFCODCLI').asInteger,
        DataSetPolizza.FieldByName('PROFESSIONE').asInteger, DataSetPolizza.FieldByName('DESCRIZIONEATTIVITA')
        .asString);
      SalvaAssicurati(DataSetAssicurati);
      // SalvaFamiliari(DataSetPolizza);
      SalvaGaranzie(DataSetGaranzie);
      // AggiornaCodicePolizza;
      UpdDatiStreams(DataSetPolizza, DataSetAssicurati, DataSetGaranzie, lastCodPolizza);
      Database.Commit(True);

      // if TipoSalvataggio = tsPolizza then
      // begin
      // // usa lastCodPolizza per fare l'update su slp_tpolizze e trasformare il preventivo in una polizza
      // salvaInPrintHouse(lastCodPolizza);
      // end;

    except
      on EDatabaseError do
        Database.Rollback;
    end;
  finally
    tablesList.Free;
  end;
end;

procedure TDMDatiBasePolizza.UpdDatiStreams(DataSetPolizza, DataSetAssicurati, DataSetGaranzie: TFDMemTable;
  CodPolizza: Integer);
begin
  SalvaDatasetToMemoField(DataSetPolizza, 'MEM_POLIZZA');
  SalvaDatasetToMemoField(DataSetAssicurati, 'MEM_ASSICURATI');
  SalvaDatasetToMemoField(DataSetGaranzie, 'MEM_GARANZIE');
  QUpdMemoPolizza.ParamByName('COD_POLIZZA').asInteger := CodPolizza;
  QUpdMemoPolizza.ExecSQL;
end;

procedure TDMDatiBasePolizza.SalvaFamiliari(DataSetPolizza: TDataSet);
var
  i: Integer;
  iMax: Integer;
begin
  if DataSetPolizza.FindField('NUMEROASSICURATI') <> nil then
    iMax := DataSetPolizza.FieldByName('NUMEROASSICURATI').asInteger
  else
    iMax := 0;
  for i  := 1 to iMax do
    SalvaFamiliare(DataSetPolizza, i);
end;

procedure TDMDatiBasePolizza.SalvaFamiliare(DataSetPolizza: TDataSet; ix: Integer);
begin
  QInsAssicurato.ParamByName('DENOMINAZIONE').asString := DataSetPolizza.FieldByName('NOME' + ix.ToString).asString;
  // QInsAssicurato.ParamByName('INDIRIZZO').AsString       := DataSetAssicurati.FieldByName('INDIRIZZO').AsString;
  // QInsAssicurato.ParamByName('CITTA').AsString           := DataSetAssicurati.FieldByName('CITTA').AsString;
  // QInsAssicurato.ParamByName('CAP').AsString             := DataSetAssicurati.FieldByName('CAP').AsString;
  // QInsAssicurato.ParamByName('PROV').AsString            := DataSetAssicurati.FieldByName('PROV').AsString;
  QInsAssicurato.ParamByName('COD_FISC_IVA').Clear;
  QInsAssicurato.ParamByName('NOTE').Clear;
  QInsAssicurato.ParamByName('ENTRATA').Clear;
  QInsAssicurato.ParamByName('USCITA').Clear;
  QInsAssicurato.ParamByName('CAUSALE_USCITA').Clear;
  QInsAssicurato.ParamByName('SOSTITUITO_DA').Clear;
  QInsAssicurato.ParamByName('PATENTE').asString       := DataSetPolizza.FieldByName('PATENTE' + ix.ToString).asString;
  QInsAssicurato.ParamByName('CATEGORIA_PAT').asString :=
    DataSetPolizza.FieldByName('CATPATENTE' + ix.ToString).asString;
  QInsAssicurato.ParamByName('DATA_RILASCIO').Clear;
  QInsAssicurato.ParamByName('RILASCIATA_DA').Clear;
  QInsAssicurato.ParamByName('DATA_SCADENZA').Clear;
  QInsAssicurato.ParamByName('TIPO_VEICOLO').Clear;
  QInsAssicurato.ParamByName('TARGA').Clear;
  QInsAssicurato.ParamByName('MARCA').Clear;
  QInsAssicurato.ParamByName('HP_QL').Clear;
  // QInsAssicurato.ParamByName('RIF_COD_TIPO_ASSICURATO').AsInteger :=
  // DataSetAssicurati.FieldByName('RIFCODTIPOASSICURATO').AsInteger;
  QInsAssicurato.ParamByName('DATA_SCAD_REVISIONE').Clear;
  QInsAssicurato.ParamByName('MODELLO').Clear;
  QInsAssicurato.ParamByName('CLASSE').Clear;
  QInsAssicurato.ParamByName('TELAIO').Clear;
  QInsAssicurato.ParamByName('CC').Clear;
  QInsAssicurato.ParamByName('DATA_IMMATR').Clear;
  QInsAssicurato.ParamByName('PREMIO').Clear;
  QInsAssicurato.ParamByName('MASSIMALE').Clear;
  QInsAssicurato.ParamByName('PREMIO_SLP').Clear;
  QInsAssicurato.ParamByName('OGG_AGGIUNTIVO').Clear;
  QInsAssicurato.ParamByName('ID_GEN_SLP_AGE').asInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

end;

procedure TDMDatiBasePolizza.SalvaBasePolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio);

  procedure AggiornaDataPreventivo;
  begin
    if not(DataSetPolizza.State in [dsInsert, dsEdit]) then
      DataSetPolizza.Edit;
    DataSetPolizza.FieldByName('DT_PREVENTIVO').AsDateTime := Now;
    // DataSetPolizza.Post;
  end;

  procedure AggiornaNumeroPolizza;
  begin
    if not(DataSetPolizza.State in [dsInsert, dsEdit]) then
      DataSetPolizza.Edit;
    DataSetPolizza.FieldByName('NPOLIZZA').asString := QInsPolizza.ParamByName('N_POLIZZA').asString;
  end;

  procedure imposta_data_param(query: TDBISAMQuery; campo: string; valore: TDateTime);
  begin
    if year(valore) < 1910 then
      query.ParamByName(campo).Clear
    else
      query.ParamByName(campo).AsDateTime := valore;
  end;

begin
  QInsPolizza.ParamByName('RIF_TIPO_POL').asInteger := DataSetPolizza.FieldByName('RIFTIPOPOL').asInteger;
  QInsPolizza.ParamByName('SIGLA_POL').asString     := UniMainModule.SiglaPolizza;
  QInsPolizza.ParamByName('CONTRAENTE').asString    := DataSetPolizza.FieldByName('CONTRAENTE').asString;
  QInsPolizza.ParamByName('DECORRENZA').AsDate      := DataSetPolizza.FieldByName('DECORRENZA').AsDateTime;
  QInsPolizza.ParamByName('SCADENZA').AsDate        := DataSetPolizza.FieldByName('SCADENZA').AsDateTime;
  QInsPolizza.ParamByName('FRAZIONAM').asString     := DataSetPolizza.FieldByName('FRAZIONAMENTO').asString;
  if TipoSalvataggio = tsPolizza then
    QInsPolizza.ParamByName('N_POLIZZA').asString := UniMainModule.NumPolizza
  else
    QInsPolizza.ParamByName('N_POLIZZA').asString := '';

  AggiornaNumeroPolizza;

  // mb 09052020
  QInsPolizza.ParamByName('LORDO').AsCurrency := DataSetPolizza.FieldByName('TOTALE1').AsCurrency *
    dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  QInsPolizza.ParamByName('NETTO').AsCurrency := DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency *
    dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  QInsPolizza.ParamByName('TASSE').AsCurrency :=
    (DataSetPolizza.FieldByName('TOTALE1').AsCurrency * dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO')
    .asString)) - (DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency *
    dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString));

  QInsPolizza.ParamByName('PTASSE').AsCurrency := DataSetPolizza.FieldByName('PTASSE').AsCurrency;

  QInsPolizza.ParamByName('ACCESSORI').AsCurrency      := DataSetPolizza.FieldByName('ACCESSORI').AsCurrency;
  QInsPolizza.ParamByName('VALUTA').asString           := DataSetPolizza.FieldByName('VALUTA').asString;
  QInsPolizza.ParamByName('NOTE').AsMemo               := DataSetPolizza.FieldByName('NOTE').asString;
  QInsPolizza.ParamByName('RIF_COMPAGNIA').asInteger   := DataSetPolizza.FieldByName('RIFCOMPAGNIA').asInteger;
  QInsPolizza.ParamByName('COD_CLI').asInteger         := DataSetPolizza.FieldByName('RIFCODCLI').asInteger;
  QInsPolizza.ParamByName('FATTA_DA').asInteger        := DataSetPolizza.FieldByName('fatta_da').asInteger;
  QInsPolizza.ParamByName('intermediata_da').asInteger := DataSetPolizza.FieldByName('INTERMEDIATADA').asInteger;
  QInsPolizza.ParamByName('INTERMEDIATA_DA_SIGLA').asString :=
    DataSetPolizza.FieldByName('INTERMEDIATADASIGLA').asString;

  QInsPolizza.ParamByName('RIF_COD_RAMO').asInteger      := DataSetPolizza.FieldByName('RIFCODRAMO').asInteger;
  QInsPolizza.ParamByName('STATO').asString              := DataSetPolizza.FieldByName('STATO').asString;
  QInsPolizza.ParamByName('PROVVIGIONI').AsCurrency      := DataSetPolizza.FieldByName('PROVVIGIONI').AsCurrency;
  QInsPolizza.ParamByName('PERC_PROVVIGIONI').AsCurrency := DataSetPolizza.FieldByName('PPROVVIGIONI').AsCurrency;
  QInsPolizza.ParamByName('CLASSE').asInteger            := DataSetPolizza.FieldByName('CLASSE').asInteger;
  QInsPolizza.ParamByName('RAMO').asString               := '17';
  QInsPolizza.ParamByName('AGENZIA').asString            := DataSetPolizza.FieldByName('AGENZIA').asString;
  QInsPolizza.ParamByName('SUB_AGE').asString            := DataSetPolizza.FieldByName('SUBAGENZIA').asString;
  QInsPolizza.ParamByName('RATINO_1_RATA').AsBoolean     := DataSetPolizza.FieldByName('RATINO1RATA').AsBoolean;
  QInsPolizza.ParamByName('SCONTO_DURATA').AsBoolean     := DataSetPolizza.FieldByName('SCONTODURATA').AsBoolean;
  QInsPolizza.ParamByName('UTENTE').asInteger            := UniMainModule.utenteCodice;
  QInsPolizza.ParamByName('DATA_INSERIMENTO').AsDate     := DataSetPolizza.FieldByName('DATAINSERIMENTO').AsDateTime;
  // QInsPolizza.ParamByName('NUM_ASSICURATI').asInteger    := DataSetPolizza.FieldByName('NUMASSICURATI').asInteger;
  QInsPolizza.ParamByName('NETTO1').AsCurrency      := DataSetPolizza.FieldByName('NETTO1').AsCurrency;
  QInsPolizza.ParamByName('ACCESSORI1').AsCurrency  := DataSetPolizza.FieldByName('ACCESSORI1').AsCurrency;
  QInsPolizza.ParamByName('INT_FRAZ').AsCurrency    := DataSetPolizza.FieldByName('INTERESSIFRAZIONAMENTO').AsCurrency;
  QInsPolizza.ParamByName('IMPONIBILE1').AsCurrency := DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency;
  QInsPolizza.ParamByName('IMPOSTE1').AsCurrency    := DataSetPolizza.FieldByName('IMPOSTE1').AsCurrency;
  QInsPolizza.ParamByName('TOTALE1').AsCurrency     := DataSetPolizza.FieldByName('TOTALE1').AsCurrency;
  QInsPolizza.ParamByName('NETTO2').AsCurrency      := DataSetPolizza.FieldByName('NETTO2').AsCurrency;
  QInsPolizza.ParamByName('ACCESSORI2').AsCurrency  := DataSetPolizza.FieldByName('ACCESSORI2').AsCurrency;
  QInsPolizza.ParamByName('RIMBORSO_SOST').AsCurrency := DataSetPolizza.FieldByName('RIMBORSOSOST').AsCurrency;
  QInsPolizza.ParamByName('IMPONIBILE2').AsCurrency        := DataSetPolizza.FieldByName('IMPONIBILE2').AsCurrency;
  QInsPolizza.ParamByName('IMPOSTE2').AsCurrency           := DataSetPolizza.FieldByName('IMPOSTE2').AsCurrency;
  QInsPolizza.ParamByName('TOTALE2').AsCurrency            := DataSetPolizza.FieldByName('TOTALE2').AsCurrency;
  QInsPolizza.ParamByName('COMBINAZIONE').asString         := DataSetPolizza.FieldByName('COMBINAZIONE').asString;
  QInsPolizza.ParamByName('FORMA').asString                := DataSetPolizza.FieldByName('FORMA').asString;
  QInsPolizza.ParamByName('ESTENSIONE_OM').asString        := DataSetPolizza.FieldByName('ESTENSIONEOM').asString;
  QInsPolizza.ParamByName('N_POLIZZA_SOSTITUITA').asString :=
    DataSetPolizza.FieldByName('NUMPOLIZZASOSTITUITA').asString;

  // QInsPolizza.ParamByName('SCAD_POL_SOSTITUITA').AsDate := DataSetPolizza.FieldByName('SCADENZAPOLIZZASOSTITUITA')
  // .AsDateTime;
  imposta_data_param(QInsPolizza, 'SCAD_POL_SOSTITUITA', DataSetPolizza.FieldByName('SCADENZAPOLIZZASOSTITUITA')
    .AsDateTime);

  QInsPolizza.ParamByName('SCAD_PRIMA_RATA').AsDate := DataSetPolizza.FieldByName('SCADENZAPRIMARATA').AsDateTime;
  if DataSetPolizza.FindField('INDICIZZATA') <> nil then
    QInsPolizza.ParamByName('INDICIZZATA').AsBoolean := DataSetPolizza.FieldByName('INDICIZZATA').AsBoolean;

  // if DataSetPolizza.FindField('NUMEROASSICURATII') <> nil then
  // QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger  := DataSetPolizza.FieldByName('NUMEROASSICURATII').AsInteger;

  QInsPolizza.ParamByName('STATUS').asString := getStatus(TipoSalvataggio);
  QInsPolizza.ParamByName('DATA_PERFEZ').Clear;
  // QInsPolizza.ParamByName('DATA_CANC').AsDate          := DataSetPolizza.FieldByName('DATACANCELLAZIONE').AsDateTime;
  imposta_data_param(QInsPolizza, 'DATA_CANC', DataSetPolizza.FieldByName('DATACANCELLAZIONE').AsDateTime);
  QInsPolizza.ParamByName('VOLUME_AFFARI').AsCurrency  := DataSetPolizza.FieldByName('VOLUMEAFFARI').AsCurrency;
  QInsPolizza.ParamByName('TIPO_PROVVIGIONI').asString := DataSetPolizza.FieldByName('PROVVIG').asString;
  QInsPolizza.ParamByName('PERC_ACCESSORI').AsCurrency := DataSetPolizza.FieldByName('PERCACCESSORI').AsCurrency;
  // QInsPolizza.ParamByName('RAGIONE_SOC').AsString :=   ???????   � usata per le polizze mediche  da trasferire dove usato
  QInsPolizza.ParamByName('DATI1').asString      := DataSetPolizza.FieldByName('DATI1').asString;
  QInsPolizza.ParamByName('DATI2').asString      := DataSetPolizza.FieldByName('DATI2').asString;
  QInsPolizza.ParamByName('DATI3').asString      := DataSetPolizza.FieldByName('DATI3').asString;
  QInsPolizza.ParamByName('DATI4').asString      := DataSetPolizza.FieldByName('DATI4').asString;
  QInsPolizza.ParamByName('ESTENSIONI').asString := DataSetPolizza.FieldByName('ESTENSIONI').asString;
  // QInsPolizza.ParamByName('SIGLA_POL').AsString  := DataSetPolizza.FieldByName('SIGLAPOLIZZA').AsString;
  // QInsPolizza.ParamByName('TIPO_PROV').AsString :=     non usato
  QInsPolizza.ParamByName('TIPOLOGIA').asString    := DataSetPolizza.FieldByName('TIPOLOGIA').asString;
  QInsPolizza.ParamByName('DATA_EMISSIONE').AsDate := DataSetPolizza.FieldByName('DATAEMISSIONE').AsDateTime;
  QInsPolizza.ParamByName('DATI_VARI').AsMemo      := DataSetPolizza.FieldByName('DATIVARI').asString;
  QInsPolizza.ParamByName('ORDINE').asString       := DataSetPolizza.FieldByName('ORDINE').asString;
  QInsPolizza.ParamByName('SPECIAL').asString      := DataSetPolizza.FieldByName('SPECIAL').asString;
  //QInsPolizza.ParamByName('DATA_IMPORTAZIONE').Clear;
  //imposta_data_param(QInsPolizza, 'DATA_IMPORTAZIONE', DataSetPolizza.FieldByName('DATAIMPORTAZIONE').AsDateTime);
  QInsPolizza.ParamByName('CODICE_IMPORTAZIONE').asInteger := DataSetPolizza.FieldByName('CODICEIMPORTAZIONE')
    .asInteger;

  QInsPolizza.ParamByName('CONVENZIONE').asInteger   := DataSetPolizza.FieldByName('CONVENZIONE').asInteger;
  QInsPolizza.ParamByName('RIF_REFERENTE').asInteger := DataSetPolizza.FieldByName('RIFREFERENTE').asInteger;
  QInsPolizza.ParamByName('VARFRP').AsMemo           := DataSetPolizza.FieldByName('VARFRP').asString;
  // QInsPolizza.ParamByName('APP_REPORT').AsBlob :=   non usare     ??? DataSetPolizza.FieldByName('APP_REPORT').AsString;
  QInsPolizza.ParamByName('CON_APPENDICE').asString := DataSetPolizza.FieldByName('CONAPPENDICE').asString;
  QInsPolizza.ParamByName('APP_NUMERO').asInteger   := DataSetPolizza.FieldByName('APPNUMERO').asInteger;
  // QInsPolizza.ParamByName('APP_DAL').AsDate          := DataSetPolizza.FieldByName('APPDAL').AsDateTime;
  // QInsPolizza.ParamByName('APP_AL').AsDate           := DataSetPolizza.FieldByName('APPAL').AsDateTime;
  imposta_data_param(QInsPolizza, 'APP_DAL', DataSetPolizza.FieldByName('APPDAL').AsDateTime);
  imposta_data_param(QInsPolizza, 'APP_AL', DataSetPolizza.FieldByName('APPAL').AsDateTime);
  QInsPolizza.ParamByName('APP_OGGETTO').asString    := DataSetPolizza.FieldByName('APPOGGETTO').asString;
  QInsPolizza.ParamByName('P_SCONTO').asInteger      := DataSetPolizza.FieldByName('PSCONTO').asInteger;
  QInsPolizza.ParamByName('INPDF').asString          := DataSetPolizza.FieldByName('INPDF').asString;
  QInsPolizza.ParamByName('SUB_PROMOTER').asInteger  := DataSetPolizza.FieldByName('SUBPROMOTER').asInteger;
  QInsPolizza.ParamByName('DATI5').asString          := DataSetPolizza.FieldByName('DATI5').asString;
  QInsPolizza.ParamByName('N_S_P').asInteger         := DataSetPolizza.FieldByName('NSP').asInteger;
  QInsPolizza.ParamByName('TIPO_EMISSIONE').asString := DataSetPolizza.FieldByName('TIPOEMISSIONE').asString;
  if DataPreventivo <> MinDateTime then
    // QInsPolizza.ParamByName('DT_PREVENTIVO').AsDateTime := DataPreventivo
    imposta_data_param(QInsPolizza, 'DT_PREVENTIVO', DataPreventivo)
  else
  begin
    AggiornaDataPreventivo;
    QInsPolizza.ParamByName('DT_PREVENTIVO').AsDateTime := DataSetPolizza.FieldByName('DT_PREVENTIVO').AsDateTime;
  end;
  QInsPolizza.ParamByName('DT_LAST_MOD').Clear;
  QInsPolizza.ParamByName('TELEFONO').asString    := DataSetPolizza.FieldByName('TELEFONO').asString;
  QInsPolizza.ParamByName('EMAIL').asString       := DataSetPolizza.FieldByName('EMAIL').asString;
  QInsPolizza.ParamByName('PERALLEGATI').asString := DataSetPolizza.FieldByName('PERALLEGATI').asString;
  if FDescrUtente <> '' then
    QInsPolizza.ParamByName('DESCRIZIONE').asString := FDescrUtente
  else
    QInsPolizza.ParamByName('DESCRIZIONE').Clear;

  QInsPolizza.ParamByName('sconto_durata').AsBoolean := (DataSetPolizza.FieldByName('sconto').AsCurrency<>0);
  QInsPolizza.ParamByName('sconto_app').AsCurrency := DataSetPolizza.FieldByName('sconto').AsCurrency;

  QInsPolizza.ParamByName('ID_GEN_SLP_AGE').asInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

end;

procedure TDMDatiBasePolizza.RiprendiAssicurati(QAssPol, DataSetAssicurati: TDataSet);
begin
  QAssPol.First;
  RiprendiAssicurato(QAssPol, DataSetAssicurati);
  QAssPol.Next; // Il primo � il contraente
  while not QAssPol.Eof do
  begin
    RiprendiAssicurato(QAssPol, DataSetAssicurati);
    QAssPol.Next;
  end;

end;

procedure TDMDatiBasePolizza.RiprendiAssicurato(QAssPol, DataSetAssicurati: TDataSet);
begin
  DataSetAssicurati.Append;
  if not (DataSetAssicurati.State = dsInsert) then
     DataSetAssicurati.Edit;
  DataSetAssicurati.FieldByName('DENOMINAZIONE').asString  := QAssPol.FieldByName('DENOMINAZIONE').asString;
  DataSetAssicurati.FieldByName('INDIRIZZO').asString      := QAssPol.FieldByName('INDIRIZZO').asString;
  DataSetAssicurati.FieldByName('CITTA').asString          := QAssPol.FieldByName('CITTA').asString;
  DataSetAssicurati.FieldByName('CAP').asString            := QAssPol.FieldByName('CAP').asString;
  DataSetAssicurati.FieldByName('PROVINCIA').asString      := QAssPol.FieldByName('PROV').asString;
  DataSetAssicurati.FieldByName('CODFISCIVAS').asString    := QAssPol.FieldByName('COD_FISC_IVA').asString;
  DataSetAssicurati.FieldByName('NOTE').asString           := QAssPol.FieldByName('NOTE').asString;
  DataSetAssicurati.FieldByName('ENTRATA').AsDateTime      := QAssPol.FieldByName('ENTRATA').AsDateTime;
  DataSetAssicurati.FieldByName('USCITA').AsDateTime       := QAssPol.FieldByName('USCITA').AsDateTime;
  DataSetAssicurati.FieldByName('CAUSALEUSCITA').asInteger := QAssPol.FieldByName('CAUSALE_USCITA').asInteger;
  DataSetAssicurati.FieldByName('SOSTITUITODA').asInteger  := QAssPol.FieldByName('SOSTITUITO_DA').asInteger;
  DataSetAssicurati.FieldByName('PATENTE').asString        := QAssPol.FieldByName('PATENTE').asString;
  DataSetAssicurati.FieldByName('CATEGORIAPATENTE').asString := QAssPol.FieldByName('CATEGORIA_PAT').asString;
  DataSetAssicurati.FieldByName('DATARILASCIO').AsDateTime := QAssPol.FieldByName('DATA_RILASCIO').AsDateTime;
  DataSetAssicurati.FieldByName('RILASCIATADA').asString   := QAssPol.FieldByName('RILASCIATA_DA').asString;
  DataSetAssicurati.FieldByName('DATASCADENZA').AsDateTime := QAssPol.FieldByName('DATA_SCADENZA').AsDateTime;
  DataSetAssicurati.FieldByName('TIPOVEICOLO').asString    := QAssPol.FieldByName('TIPO_VEICOLO').asString;
  DataSetAssicurati.FieldByName('TARGA').asString          := QAssPol.FieldByName('TARGA').asString;
  DataSetAssicurati.FieldByName('MARCA').asString          := QAssPol.FieldByName('MARCA').asString;
  DataSetAssicurati.FieldByName('HPQL').asString           := QAssPol.FieldByName('HP_QL').asString;

  DataSetAssicurati.FieldByName('RIFCODTIPOASSICURATO').asInteger := QAssPol.FieldByName('RIF_COD_TIPO_ASSICURATO')
    .asInteger;
  DataSetAssicurati.FieldByName('TIPO').asString := QAssPol.FieldByName('TIPO').asString;
  // QAssPol.FieldByName('COD_POLIZZA').AsString :=  DataSetAssicurati.FieldByName('CITTA').AsString;
  DataSetAssicurati.FieldByName('DATASCADREVISIONE').AsDateTime := QAssPol.FieldByName('DATA_SCAD_REVISIONE')
    .AsDateTime;
  DataSetAssicurati.FieldByName('MODELLO').asString := QAssPol.FieldByName('MODELLO').asString;
  DataSetAssicurati.FieldByName('CLASSE').asInteger := QAssPol.FieldByName('CLASSE').asInteger;
  DataSetAssicurati.FieldByName('TELAIO').asString  := QAssPol.FieldByName('TELAIO').asString;
  DataSetAssicurati.FieldByName('CITTA').asString   := QAssPol.FieldByName('CC').asString;
  DataSetAssicurati.FieldByName('DATAIMMATRICOLAZIONE').AsDateTime := QAssPol.FieldByName('DATA_IMMATR').AsDateTime;
  DataSetAssicurati.FieldByName('PREMIO').AsCurrency      := QAssPol.FieldByName('PREMIO').AsCurrency;
  DataSetAssicurati.FieldByName('MASSIMALE').AsCurrency   := QAssPol.FieldByName('MASSIMALE').AsCurrency;
  DataSetAssicurati.FieldByName('PREMIOSLP').AsCurrency   := QAssPol.FieldByName('PREMIO_SLP').AsCurrency;
  DataSetAssicurati.FieldByName('OGGAGGIUNTIVO').asString := QAssPol.FieldByName('OGG_AGGIUNTIVO').asString;

  DataSetAssicurati.Post;
end;

procedure TDMDatiBasePolizza.RiprendiBasePolizza(QSLPTPolizze, DataSetPolizza, QAssPol: TDataSet);
begin
  DataSetPolizza.Edit;
  DataSetPolizza.FieldByName('RIFTIPOPOL').asInteger := QSLPTPolizze.FieldByName('RIF_TIPO_POL').asInteger;
  UniMainModule.SiglaPolizza                         := QSLPTPolizze.FieldByName('SIGLA_POL').asString;
  DataSetPolizza.FieldByName('CONTRAENTE').asString  := QSLPTPolizze.FieldByName('CONTRAENTE').asString;
  // se non faccio questa verifica, il programma non carica la polizza in quanto con decorrenza troppo vecchia !!!
  if Abs(Date - QSLPTPolizze.FieldByName('DECORRENZA').AsDateTime) > 30 then
  begin
    { TODO : sitemare in questo modo: decorrenza oggi e scadenza quella originale ripresa + il ratino }
    DataSetPolizza.FieldByName('DECORRENZA').AsDateTime := Date;
    DataSetPolizza.FieldByName('SCADENZA').AsDateTime   := IncMonth(Date, 12);
    DataSetPolizza.FieldByName('DATAEMISSIONE').AsDateTime := Date;
  end
  else
  begin
    DataSetPolizza.FieldByName('DECORRENZA').AsDateTime := QSLPTPolizze.FieldByName('DECORRENZA').AsDateTime;
    DataSetPolizza.FieldByName('SCADENZA').AsDateTime   := QSLPTPolizze.FieldByName('SCADENZA').AsDateTime;
    DataSetPolizza.FieldByName('DATAEMISSIONE').AsDateTime := QSLPTPolizze.FieldByName('DATA_EMISSIONE').AsDateTime;
  end;


  DataSetPolizza.FieldByName('FRAZIONAMENTO').asString := QSLPTPolizze.FieldByName('FRAZIONAM').asString;
  // if TipoSalvataggio = tsPolizza then
  // QSLP_TPolizze.FieldByName('N_POLIZZA').asString := UniMainModule.NumPolizza
  // else
  // QSLP_TPolizze.FieldByName('N_POLIZZA').asString := '';

  // AggiornaNumeroPolizza;

  // QSLP_TPolizze.FieldByName('LORDO').AsCurrency := DataSetPolizza.FieldByName('TOTALE1').AsCurrency *
  // dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  // // DataSetPolizza.FieldByName('LORDO').AsCurrency;
  // QSLP_TPolizze.FieldByName('NETTO').AsCurrency := DataSetPolizza.FieldByName('NETTO1').AsCurrency *
  // dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  // QSLP_TPolizze.FieldByName('TASSE').AsCurrency := DataSetPolizza.FieldByName('TOTALE1').AsCurrency -
  // QSLP_TPolizze.FieldByName('NETTO').AsCurrency;

  // QSLP_TPolizze.FieldByName('LORDO').AsCurrency := DataSetPolizza.FieldByName('TOTALE1').AsCurrency *
  // dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  // QSLP_TPolizze.FieldByName('NETTO').AsCurrency := DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency *
  // dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  // QSLP_TPolizze.FieldByName('TASSE').AsCurrency :=
  // (DataSetPolizza.FieldByName('TOTALE1').AsCurrency * dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO')
  // .asString)) - (DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency *
  // dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString));

  // DataSetPolizza.FieldByName('PTASSE').AsCurrency := QSLP_TPolizze.FieldByName('PTASSE').AsCurrency;

  DataSetPolizza.FieldByName('ACCESSORI').AsCurrency     := QSLPTPolizze.FieldByName('ACCESSORI').AsCurrency;
  DataSetPolizza.FieldByName('VALUTA').asString          := QSLPTPolizze.FieldByName('VALUTA').asString;
  DataSetPolizza.FieldByName('NOTE').asString            := QSLPTPolizze.FieldByName('NOTE').asString;
  DataSetPolizza.FieldByName('RIFCOMPAGNIA').asInteger   := QSLPTPolizze.FieldByName('RIF_COMPAGNIA').asInteger;
  DataSetPolizza.FieldByName('RIFCODCLI').asInteger      := QSLPTPolizze.FieldByName('COD_CLI').asInteger;
  DataSetPolizza.FieldByName('FATTA_DA').asInteger       := QSLPTPolizze.FieldByName('FATTA_DA').asInteger;
  DataSetPolizza.FieldByName('INTERMEDIATADA').asInteger := QSLPTPolizze.FieldByName('INTERMEDIATA_DA').asInteger;
  DataSetPolizza.FieldByName('INTERMEDIATADASIGLA').asString :=
    QSLPTPolizze.FieldByName('INTERMEDIATA_DA_SIGLA').asString;

  DataSetPolizza.FieldByName('RIFCODRAMO').asInteger    := QSLPTPolizze.FieldByName('rif_cod_ramo').asInteger; // RAMO
  DataSetPolizza.FieldByName('STATO').asString          := QSLPTPolizze.FieldByName('STATO').asString;
  DataSetPolizza.FieldByName('PROVVIGIONI').AsCurrency  := QSLPTPolizze.FieldByName('PROVVIGIONI').AsCurrency;
  DataSetPolizza.FieldByName('PPROVVIGIONI').AsCurrency := QSLPTPolizze.FieldByName('PERC_PROVVIGIONI').AsCurrency;
  DataSetPolizza.FieldByName('CLASSE').asInteger        := QSLPTPolizze.FieldByName('CLASSE').asInteger;
  DataSetPolizza.FieldByName('AGENZIA').asString        := QSLPTPolizze.FieldByName('AGENZIA').asString;
  DataSetPolizza.FieldByName('SUBAGENZIA').asString     := QSLPTPolizze.FieldByName('SUB_AGE').asString;
  DataSetPolizza.FieldByName('RATINO1RATA').AsBoolean   := QSLPTPolizze.FieldByName('RATINO_1_RATA').AsBoolean;
  DataSetPolizza.FieldByName('SCONTODURATA').AsBoolean  := QSLPTPolizze.FieldByName('SCONTO_DURATA').AsBoolean;
  // UniMainModule.utenteCodice;
  // QSLP_TPolizze.FieldByName('UTENTE').asInteger;
  DataSetPolizza.FieldByName('DATAINSERIMENTO').AsDateTime := QSLPTPolizze.FieldByName('DATA_INSERIMENTO').AsDateTime;
  // QSLP_TPolizze.FieldByName('NUM_ASSICURATI').asInteger    := DataSetPolizza.FieldByName('NUMASSICURATI').asInteger;
  // QSLP_TPolizze.FieldByName('NETTO1').AsCurrency     := DataSetPolizza.FieldByName('NETTO1').AsCurrency;
  // QSLP_TPolizze.FieldByName('ACCESSORI1').AsCurrency := DataSetPolizza.FieldByName('ACCESSORI1').AsCurrency;
  // QSLP_TPolizze.FieldByName('INT_FRAZ').AsCurrency := DataSetPolizza.FieldByName('INTERESSIFRAZIONAMENTO').AsCurrency;
  // QSLP_TPolizze.FieldByName('IMPONIBILE1').AsCurrency   := DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency;
  // QSLP_TPolizze.FieldByName('IMPOSTE1').AsCurrency      := DataSetPolizza.FieldByName('IMPOSTE1').AsCurrency;
  // QSLP_TPolizze.FieldByName('TOTALE1').AsCurrency       := DataSetPolizza.FieldByName('TOTALE1').AsCurrency;
  // QSLP_TPolizze.FieldByName('NETTO2').AsCurrency        := DataSetPolizza.FieldByName('NETTO2').AsCurrency;
  // QSLP_TPolizze.FieldByName('ACCESSORI2').AsCurrency    := DataSetPolizza.FieldByName('ACCESSORI2').AsCurrency;
  // QSLP_TPolizze.FieldByName('RIMBORSO_SOST').AsCurrency := DataSetPolizza.FieldByName('RIMBORSOSOST').AsCurrency;
  // QSLP_TPolizze.FieldByName('IMPONIBILE2').AsCurrency   := DataSetPolizza.FieldByName('IMPONIBILE2').AsCurrency;
  // QSLP_TPolizze.FieldByName('IMPOSTE2').AsCurrency      := DataSetPolizza.FieldByName('IMPOSTE2').AsCurrency;
  // QSLP_TPolizze.FieldByName('TOTALE2').AsCurrency       := DataSetPolizza.FieldByName('TOTALE2').AsCurrency;
  DataSetPolizza.FieldByName('COMBINAZIONE').asString := QSLPTPolizze.FieldByName('COMBINAZIONE').asString;
  DataSetPolizza.FieldByName('FORMA').asString        := QSLPTPolizze.FieldByName('FORMA').asString;
  DataSetPolizza.FieldByName('ESTENSIONEOM').asString := QSLPTPolizze.FieldByName('ESTENSIONE_OM').asString;
  DataSetPolizza.FieldByName('NUMPOLIZZASOSTITUITA').asString :=
    QSLPTPolizze.FieldByName('N_POLIZZA_SOSTITUITA').asString;

  // QSLP_TPolizze.FieldByName('SCAD_POL_SOSTITUITA').AsDate := DataSetPolizza.FieldByName('SCADENZAPOLIZZASOSTITUITA')
  // .AsDateTime;
  DataSetPolizza.FieldByName('SCADENZAPOLIZZASOSTITUITA').AsDateTime := QSLPTPolizze.FieldByName('SCAD_POL_SOSTITUITA')
    .AsDateTime;

  DataSetPolizza.FieldByName('SCADENZAPRIMARATA').AsDateTime := QSLPTPolizze.FieldByName('SCAD_PRIMA_RATA').AsDateTime;
  if DataSetPolizza.FindField('INDICIZZATA') <> nil then
    DataSetPolizza.FieldByName('INDICIZZATA').AsBoolean := QSLPTPolizze.FieldByName('INDICIZZATA').AsBoolean;

  // if DataSetPolizza.FindField('NUMEROASSICURATII') <> nil then
  // QSLP_TPolizze.FieldByName('NUMERO_ASSICURATI').AsInteger  := DataSetPolizza.FieldByName('NUMEROASSICURATII').AsInteger;

  // QSLP_TPolizze.FieldByName('STATUS').asString := getStatus(TipoSalvataggio);
  // QSLP_TPolizze.FieldByName('DATA_PERFEZ').Clear;
  // QSLP_TPolizze.FieldByName('DATA_CANC').AsDate          := DataSetPolizza.FieldByName('DATACANCELLAZIONE').AsDateTime;
  DataSetPolizza.FieldByName('DATACANCELLAZIONE').AsDateTime := QSLPTPolizze.FieldByName('DATA_CANC').AsDateTime;
  DataSetPolizza.FieldByName('VOLUMEAFFARI').AsCurrency := QSLPTPolizze.FieldByName('VOLUME_AFFARI').AsCurrency;
  DataSetPolizza.FieldByName('PROVVIG').asString         := QSLPTPolizze.FieldByName('TIPO_PROVVIGIONI').asString;
  DataSetPolizza.FieldByName('PERCACCESSORI').AsCurrency := QSLPTPolizze.FieldByName('PERC_ACCESSORI').AsCurrency;
  // QSLPTPolizze.FieldByName('RAGIONE_SOC').AsString :=   ???????   � usata per le polizze mediche  da trasferire dove usato
  DataSetPolizza.FieldByName('DATI1').asString      := QSLPTPolizze.FieldByName('DATI1').asString;
  DataSetPolizza.FieldByName('DATI2').asString      := QSLPTPolizze.FieldByName('DATI2').asString;
  DataSetPolizza.FieldByName('DATI3').asString      := QSLPTPolizze.FieldByName('DATI3').asString;
  DataSetPolizza.FieldByName('DATI4').asString      := QSLPTPolizze.FieldByName('DATI4').asString;
  DataSetPolizza.FieldByName('ESTENSIONI').asString := QSLPTPolizze.FieldByName('ESTENSIONI').asString;
  // QSLPTPolizze.FieldByName('SIGLA_POL').AsString  := DataSetPolizza.FieldByName('SIGLAPOLIZZA').AsString;
  // QSLPTPolizze.FieldByName('TIPO_PROV').AsString :=     non usato
  DataSetPolizza.FieldByName('TIPOLOGIA').asString       := QSLPTPolizze.FieldByName('TIPOLOGIA').asString;

  DataSetPolizza.FieldByName('DATIVARI').asString        := QSLPTPolizze.FieldByName('DATI_VARI').asString;
  DataSetPolizza.FieldByName('ORDINE').asString          := QSLPTPolizze.FieldByName('ORDINE').asString;
  DataSetPolizza.FieldByName('SPECIAL').asString         := QSLPTPolizze.FieldByName('SPECIAL').asString;
  DataSetPolizza.FieldByName('DATAIMPORTAZIONE').AsDateTime := QSLPTPolizze.FieldByName('DATA_IMPORTAZIONE').AsDateTime;
  DataSetPolizza.FieldByName('CODICEIMPORTAZIONE').asInteger := QSLPTPolizze.FieldByName('CODICE_IMPORTAZIONE')
    .asInteger;

  DataSetPolizza.FieldByName('CONVENZIONE').asInteger  := QSLPTPolizze.FieldByName('CONVENZIONE').asInteger;
  DataSetPolizza.FieldByName('RIFREFERENTE').asInteger := QSLPTPolizze.FieldByName('RIF_REFERENTE').asInteger;
  DataSetPolizza.FieldByName('VARFRP').asString        := QSLPTPolizze.FieldByName('VARFRP').asString;
  // QSLPTPolizze.FieldByName('APP_REPORT').AsBlob :=   non usare     ??? DataSetPolizza.FieldByName('APP_REPORT').AsString;
  DataSetPolizza.FieldByName('CONAPPENDICE').asString := QSLPTPolizze.FieldByName('CON_APPENDICE').asString;
  DataSetPolizza.FieldByName('APPNUMERO').asInteger   := QSLPTPolizze.FieldByName('APP_NUMERO').asInteger;
  // QSLPTPolizze.FieldByName('APP_DAL').AsDate          := DataSetPolizza.FieldByName('APPDAL').AsDateTime;
  // QSLPTPolizze.FieldByName('APP_AL').AsDate           := DataSetPolizza.FieldByName('APPAL').AsDateTime;
  DataSetPolizza.FieldByName('APPDAL').AsDateTime := QSLPTPolizze.FieldByName('APP_DAL').AsDateTime;
  DataSetPolizza.FieldByName('APPAL').AsDateTime  := QSLPTPolizze.FieldByName('APP_AL').AsDateTime;
  // DataSetPolizza.FieldByName('APPOGGETTO').asString    := QSLPTPolizze.FieldByName('APP_OGGETTO').asString;
  DataSetPolizza.FieldByName('PSCONTO').asInteger          := QSLPTPolizze.FieldByName('P_SCONTO').asInteger;
  DataSetPolizza.FieldByName('INPDF').asString             := QSLPTPolizze.FieldByName('INPDF').asString;
  DataSetPolizza.FieldByName('SUBPROMOTER').asInteger      := QSLPTPolizze.FieldByName('SUB_PROMOTER').asInteger;
  DataSetPolizza.FieldByName('DATI5').asString             := QSLPTPolizze.FieldByName('DATI5').asString;
  DataSetPolizza.FieldByName('NSP').asInteger              := QSLPTPolizze.FieldByName('N_S_P').asInteger;
  DataSetPolizza.FieldByName('TIPOEMISSIONE').asString     := QSLPTPolizze.FieldByName('TIPO_EMISSIONE').asString;
  DataSetPolizza.FieldByName('DATAINSERIMENTO').AsDateTime := QSLPTPolizze.FieldByName('DT_PREVENTIVO').AsDateTime;
  // if DataPreventivo <> MinDateTime then
  // // QSLPTPolizze.FieldByName('DT_PREVENTIVO').AsDateTime := DataPreventivo
  // imposta_data_param(QSLPTPolizze, 'DT_PREVENTIVO', DataPreventivo)
  // else
  // begin
  // AggiornaDataPreventivo;
  // QSLPTPolizze.FieldByName('DT_PREVENTIVO').AsDateTime := DataSetPolizza.FieldByName('DT_PREVENTIVO').AsDateTime;
  // end;
  // QSLPTPolizze.FieldByName('DT_LAST_MOD').Clear;
  DataSetPolizza.FieldByName('TELEFONO').asString    := QSLPTPolizze.FieldByName('TELEFONO').asString;
  DataSetPolizza.FieldByName('EMAIL').asString       := QSLPTPolizze.FieldByName('EMAIL').asString;
  DataSetPolizza.FieldByName('PERALLEGATI').asString := QSLPTPolizze.FieldByName('PERALLEGATI').asString;

  // imposta il selettore dell'oggetto assicurato 0= patente  1=veicolo
  if QAssPol.FieldByName('TARGA').asString > '' then
    DataSetPolizza.FieldByName('TipoAssicurato').asString := 'V'
  else
    DataSetPolizza.FieldByName('TipoAssicurato').asString := 'P';


  // QSLPTPolizze.FieldByName('ID_GEN_SLP_AGE').asInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

end;

procedure TDMDatiBasePolizza.SalvaAssicurati(DataSetAssicurati: TDataSet);
begin
  DataSetAssicurati.DisableControls;
  try
    DataSetAssicurati.First;
    while not DataSetAssicurati.Eof do
    begin
      SalvaAssicurato(DataSetAssicurati);
      DataSetAssicurati.Next;
    end;
  finally
    DataSetAssicurati.EnableControls;
  end;
end;

procedure TDMDatiBasePolizza.SalvaAssicurato(DataSetAssicurati: TDataSet);
begin
  // QInsAssicurato.ParamByName(' cod_assicurati  /
  QInsAssicurato.ParamByName('DENOMINAZIONE').asString   := DataSetAssicurati.FieldByName('DENOMINAZIONE').asString;
  QInsAssicurato.ParamByName('INDIRIZZO').asString       := DataSetAssicurati.FieldByName('INDIRIZZO').asString;
  QInsAssicurato.ParamByName('CITTA').asString           := DataSetAssicurati.FieldByName('CITTA').asString;
  QInsAssicurato.ParamByName('CAP').asString             := DataSetAssicurati.FieldByName('CAP').asString;
  QInsAssicurato.ParamByName('PROV').asString            := DataSetAssicurati.FieldByName('PROVINCIA').asString;
  QInsAssicurato.ParamByName('COD_FISC_IVA').asString    := DataSetAssicurati.FieldByName('CODFISCIVAS').asString;
  QInsAssicurato.ParamByName('NOTE').asString            := DataSetAssicurati.FieldByName('NOTE').asString;
  QInsAssicurato.ParamByName('ENTRATA').AsDate           := DataSetAssicurati.FieldByName('ENTRATA').AsDateTime;
  QInsAssicurato.ParamByName('USCITA').AsDate            := DataSetAssicurati.FieldByName('USCITA').AsDateTime;
  QInsAssicurato.ParamByName('CAUSALE_USCITA').asInteger := DataSetAssicurati.FieldByName('CAUSALEUSCITA').asInteger;
  QInsAssicurato.ParamByName('SOSTITUITO_DA').asInteger  := DataSetAssicurati.FieldByName('SOSTITUITODA').asInteger;
  QInsAssicurato.ParamByName('PATENTE').asString         := DataSetAssicurati.FieldByName('PATENTE').asString;
  QInsAssicurato.ParamByName('CATEGORIA_PAT').asString   := DataSetAssicurati.FieldByName('CATEGORIAPATENTE').asString;
  QInsAssicurato.ParamByName('DATA_RILASCIO').AsDate     := DataSetAssicurati.FieldByName('DATARILASCIO').AsDateTime;
  QInsAssicurato.ParamByName('RILASCIATA_DA').asString   := DataSetAssicurati.FieldByName('RILASCIATADA').asString;
  QInsAssicurato.ParamByName('DATA_SCADENZA').AsDate     := DataSetAssicurati.FieldByName('DATASCADENZA').AsDateTime;
  QInsAssicurato.ParamByName('TIPO_VEICOLO').asString    := DataSetAssicurati.FieldByName('TIPOVEICOLO').asString;
  QInsAssicurato.ParamByName('TARGA').asString           := DataSetAssicurati.FieldByName('TARGA').asString;
  QInsAssicurato.ParamByName('MARCA').asString           := DataSetAssicurati.FieldByName('MARCA').asString;
  QInsAssicurato.ParamByName('HP_QL').asString           := DataSetAssicurati.FieldByName('HPQL').asString;
  QInsAssicurato.ParamByName('RIF_COD_TIPO_ASSICURATO').asInteger :=
    DataSetAssicurati.FieldByName('RIFCODTIPOASSICURATO').asInteger;
//        DataSetPolizza.FieldByName('TipoAssicurato').asString := 'V'
  QInsAssicurato.ParamByName('TIPO').asString := DataSetAssicurati.FieldByName('TIPO').asString;
  // QInsAssicurato.ParamByName('COD_POLIZZA').AsString :=  DataSetAssicurati.FieldByName('CITTA').AsString;
  QInsAssicurato.ParamByName('DATA_SCAD_REVISIONE').AsDate := DataSetAssicurati.FieldByName('DATASCADREVISIONE')
    .AsDateTime;
  QInsAssicurato.ParamByName('MODELLO').asString   := DataSetAssicurati.FieldByName('MODELLO').asString;
  QInsAssicurato.ParamByName('CLASSE').asInteger   := DataSetAssicurati.FieldByName('CLASSE').asInteger;
  QInsAssicurato.ParamByName('TELAIO').asString    := DataSetAssicurati.FieldByName('TELAIO').asString;
  QInsAssicurato.ParamByName('CC').asString        := DataSetAssicurati.FieldByName('CITTA').asString;
  QInsAssicurato.ParamByName('DATA_IMMATR').AsDate := DataSetAssicurati.FieldByName('DATAIMMATRICOLAZIONE').AsDateTime;
  QInsAssicurato.ParamByName('PREMIO').AsCurrency  := DataSetAssicurati.FieldByName('PREMIO').AsCurrency;
  QInsAssicurato.ParamByName('MASSIMALE').AsCurrency := DataSetAssicurati.FieldByName('MASSIMALE').AsCurrency;
  QInsAssicurato.ParamByName('PREMIO_SLP').AsCurrency    := DataSetAssicurati.FieldByName('PREMIOSLP').AsCurrency;
  QInsAssicurato.ParamByName('OGG_AGGIUNTIVO').asString  := DataSetAssicurati.FieldByName('OGGAGGIUNTIVO').asString;
  QInsAssicurato.ParamByName('ID_GEN_SLP_AGE').asInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

end;

procedure TDMDatiBasePolizza.SalvaGaranzie(DataSetGaranzie: TDataSet);
begin
  DataSetGaranzie.DisableControls;
  try
    DataSetGaranzie.First;
    while not DataSetGaranzie.Eof do
    begin
      SalvaGaranzia(DataSetGaranzie);
      DataSetGaranzie.Next;
    end;
  finally
    DataSetGaranzie.EnableControls;
  end;

end;

procedure TDMDatiBasePolizza.salvaInPrintHouse(CodPolizza: Integer);
begin
  // genera e salva la documentazione contrattuale, compresa la polizza , relativamente all polizza codPolizza

end;

procedure TDMDatiBasePolizza.setDescrUtente(const Value: string);
begin
  FDescrUtente := Value;
end;

procedure TDMDatiBasePolizza.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  QInsGaranzia.ParamByName('MASSIMALE').AsCurrency         := DataSetGaranzie.FieldByName('MASSIMALE').AsCurrency;
  QInsGaranzia.ParamByName('PREMIO').AsCurrency            := DataSetGaranzie.FieldByName('PREMIO').AsCurrency;
  QInsGaranzia.ParamByName('DATA_IN').AsDate               := DataSetGaranzie.FieldByName('DATAIN').AsDateTime;
  QInsGaranzia.ParamByName('DATA_OUT').AsDate              := DataSetGaranzie.FieldByName('DATAOUT').AsDateTime;
  QInsGaranzia.ParamByName('VALORE_ASSICURATO').AsCurrency := DataSetGaranzie.FieldByName('VALOREASSICURATO')
    .AsCurrency;
  QInsGaranzia.ParamByName('VALORE_IF').AsCurrency        := DataSetGaranzie.FieldByName('VALOREIF').AsCurrency;
  QInsGaranzia.ParamByName('RIF_GAR_BASE').asInteger      := DataSetGaranzie.FieldByName('RIFGARBASE').asInteger;
  QInsGaranzia.ParamByName('FRANCHIGIA').asString         := DataSetGaranzie.FieldByName('FRANCHIGIA').asString;
  QInsGaranzia.ParamByName('RIF_COD_MASSIMALE').asInteger := DataSetGaranzie.FieldByName('RIFCODMASSIMALE').asInteger;
  QInsGaranzia.ParamByName('PERCENTUALE').AsCurrency      := DataSetGaranzie.FieldByName('PERCENTUALE').AsCurrency;
  QInsGaranzia.ParamByName('ID_GEN_SLP_AGE').asInteger    := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);
end;

end.
