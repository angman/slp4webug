unit UDMDatiTempPolizzaCondTP;

interface

uses
  SysUtils, Classes, UDMDatiTempPolizza, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, frxClass, frxGZip, frxExportBaseDialog, frxExportPDF, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Math;

type
  TDMDatiTempPolizzaCondTP = class(TDMDatiTempPolizza)
    fdmtblPolizzaindicizzata: TBooleanField;
    fdmtblPolizzaUnitaImmobiliari: TIntegerField;
    fdmtblPolizzaNumeroBox: TIntegerField;
    fdmtblAssicuratoPremioAnnuoLordo: TCurrencyField;
    procedure fdmtblPolizzaUnitaImmobiliariChange(Sender: TField);
    procedure fdmtblPolizzaNumeroBoxChange(Sender: TField);
    procedure fdmtblAssicuratoPremioAnnuoLordoChange(Sender: TField);
    procedure fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
    procedure fdmtblAssicuratoNewRecord(DataSet: TDataSet);
    procedure fdmtblPolizzaindicizzataChange(Sender: TField);
    procedure fdmtblPolizzaNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    // inc_max, inc_punti, inc_perizie,   inc_dp, inc_cc,
    // inc_max_dp: currency;
    imp_risc_acc, imp_est_c, imp_base, imp_pp: currency;
    p_unita, p_box, PRE_perizie: currency;

  protected
    procedure AzzeraCampiReport; override;
    procedure preparaReport; override;

    procedure MemorizzaTempAssicurati(CodPolizza: Integer); override;
    procedure MemorizzaTempAssicurato(CodPolizza: Integer); override;

    procedure MemorizzaTempGaranzie(TipoPolizza: Integer); override;

  public
    { Public declarations }
    procedure cal_premio; override;
    function ReadTipoAssicurato: string; override;
    procedure SetDatiAssicuratoDaContraente(EnablePatente: boolean); override;

  end;

function DMDatiTempPolizzaCondTP: TDMDatiTempPolizzaCondTP;

implementation

{$R *.dfm}

uses
{$IFDEF  TEMPTABLES}
  UVediTempTables, uniGUIApplication,
{$ENDIF}
  UniGUIVars, uniGUIMainModule, MainModule, libreria, ServerModule, libSLP, System.StrUtils, System.DateUtils;

function DMDatiTempPolizzaCondTP: TDMDatiTempPolizzaCondTP;
begin
  Result := TDMDatiTempPolizzaCondTP(UniMainModule.GetModuleInstance(TDMDatiTempPolizzaCondTP));
end;

{ TDMDatiTempPolizzaCondTP }

procedure TDMDatiTempPolizzaCondTP.AzzeraCampiReport;
var
  i: Integer;
begin
  stringatest := QuotedStr('');

  for i := 0 to frxPolizzaGen.Variables.count - 1 do
  begin
    frxPolizzaGen.Variables.Items[i].Value := stringatest;
  end;
  frxPolizzaGen.Variables['versione']  := QuotedStr(leggiVersione);
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('N');
  frxPolizzaGen.Variables['RAMO']      := QuotedStr(UniServerModule.ramoSLP);

end;

procedure TDMDatiTempPolizzaCondTP.cal_premio;
begin
  imp_risc_acc := 0;
  imp_est_c    := 0;
  imp_base     := 0;
  imp_pp       := 0;
  p_unita      := 0;
  p_box        := 0;
  PRE_perizie  := 0;

  nettoAnnuoTotNONScontato := 0;

  if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, siglaMassimale) then
  begin
    nettoAnnuoTotNONScontato := UniMainModule.DMDatiBasePolizza.Qgar_base.fieldbyname('PREMIO').asFloat;
    imp_base                 := nettoAnnuoTotNONScontato;
  end;

  // Unit� immobiliari
  AggiornaNettoPerEstensione(fdmtblPolizzaUnitaImmobiliari.AsInteger > 0, componiSiglaMassimaleFromStringa('U',
    fdmtblPolizzaSiglaMassimale.AsString), p_unita);
  p_unita := IfThen(fdmtblPolizzaUnitaImmobiliari.AsInteger > 10,
    p_unita * (fdmtblPolizzaUnitaImmobiliari.AsInteger - 9), p_unita);

  // Box
  AggiornaNettoPerEstensione(fdmtblPolizzaNumeroBox.AsInteger > 0, componiSiglaMassimaleFromStringa('B',
    fdmtblPolizzaSiglaMassimale.AsString), p_box);
  p_box := p_box * fdmtblPolizzaNumeroBox.AsInteger;

  // perizia di parte
  if fdmtblAssicuratoPremioAnnuoLordo.AsCurrency > 0 then
    if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza,
      componiSiglaMassimaleFromStringa('E', fdmtblPolizzaSiglaMassimale.AsString)) then
    begin
      PRE_perizie := fdmtblAssicuratoPremioAnnuoLordo.AsCurrency / 100 *
        UniMainModule.DMDatiBasePolizza.Qgar_base.fieldbyname('percentuale').asFloat;
      PRE_perizie := IfThen(PRE_perizie < 15, 15, PRE_perizie);
    end;

  nettoAnnuoTotNONScontato              := imp_base + p_unita + p_box + PRE_perizie;
  fdmtblPolizzaNettoAnnuoTot.AsCurrency := nettoAnnuoTotNONScontato;
  fdmtblAssicuratoPremioSLP.AsCurrency  := nettoAnnuoTotNONScontato;

end;

procedure TDMDatiTempPolizzaCondTP.fdmtblPolizzaindicizzataChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaCondTP.fdmtblPolizzaNewRecord(DataSet: TDataSet);
begin
  inherited;
  fdmtblPolizzaUnitaImmobiliari.AsInteger := 1;
end;

procedure TDMDatiTempPolizzaCondTP.fdmtblPolizzaNumeroBoxChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaCondTP.fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
begin
  fdmtblPolizzaSiglaMassimale.OnChange := nil;
  try
    calcolaPremio;
  finally
    fdmtblPolizzaSiglaMassimale.OnChange := fdmtblPolizzaSiglaMassimaleChange;
  end;

end;

procedure TDMDatiTempPolizzaCondTP.fdmtblPolizzaUnitaImmobiliariChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaCondTP.MemorizzaTempAssicurati(CodPolizza: Integer);
begin
  // fdmtblAssicurato.First;
  // MemorizzaTempAssicurato(CodPolizza);
end;

procedure TDMDatiTempPolizzaCondTP.MemorizzaTempAssicurato(CodPolizza: Integer);
begin
  // if not(fdmtblAssicurato.State in [dsInsert, dsEdit]) then
  // fdmtblAssicurato.Edit;
  //
  // fdmtblAssicuratoDenominazione.AsString := fdmtblPolizzaContraente.AsString;
  // fdmtblAssicuratoIndirizzo.AsString     := fdmtblPolizzaIndirizzo.AsString;
  // fdmtblAssicuratoCitta.AsString         := fdmtblPolizzaCitta.AsString;
  // fdmtblAssicuratocap.AsString           := fdmtblPolizzaCap.AsString;
  // fdmtblAssicuratoProvincia.AsString     := fdmtblPolizzaProvincia.AsString;
  // fdmtblAssicuratoCodFiscIvas.AsString   := fdmtblPolizzaPartitaIva.AsString;
  // fdmtblAssicuratoNote.Clear;
  // fdmtblAssicuratoEntrata.Clear;
  // fdmtblAssicuratoUscita.Clear;
  // fdmtblAssicuratoCausaleUscita.Clear;
  // fdmtblAssicuratoSostituitoDa.Clear;
  // fdmtblAssicuratoPatente.Clear;
  // fdmtblAssicuratoCategoriaPatente.Clear;
  // fdmtblAssicuratoDataRilascio.Clear;
  // fdmtblAssicuratoRilasciataDa.Clear;
  // fdmtblAssicuratoDataScadenza.Clear;
  // fdmtblAssicuratoTipoVeicolo.Clear;
  // fdmtblAssicuratoTarga.Clear;
  // fdmtblAssicuratoMarca.Clear;
  // fdmtblAssicuratoHpQl.Clear;
  // fdmtblAssicuratoRifCodTipoAssicurato.Clear;
  // fdmtblAssicuratoTipo.Clear;
  // fdmtblAssicuratoCodPolizza.AsInteger := CodPolizza;
  // fdmtblAssicuratoDataScadRevisione.Clear;
  // fdmtblAssicuratoModello.Clear;
  // fdmtblAssicuratoClasse.Clear;
  // fdmtblAssicuratoTelaio.Clear;
  // fdmtblAssicuratoCc.Clear;
  // fdmtblAssicuratoDataImmatricolazione.Clear;
  // fdmtblAssicuratoPremio.Clear;
  // fdmtblAssicuratoMassimale.Clear;
  // fdmtblAssicuratoPremioSLP.Clear;
  // fdmtblAssicuratoOggAggiuntivo.Clear;
  // fdmtblAssicuratoIdGenSlpAge.Clear;
  //
  // fdmtblAssicurato.post;

end;

procedure TDMDatiTempPolizzaCondTP.MemorizzaTempGaranzie(TipoPolizza: Integer);
  procedure memorizzaGaranziaInAssicurato;
  begin
    if not(fdmtblAssicurato.State in [dsInsert, dsEdit]) then
      fdmtblAssicurato.Edit;
    fdmtblAssicuratoDenominazione.AsString := 'Globale fabbricati';
    fdmtblAssicuratoMassimale.AsCurrency   := fdmtblGaranziaMassimale.AsCurrency;
    fdmtblAssicuratoPremioSLP.AsCurrency   := fdmtblGaranziaPremio.AsCurrency;
    fdmtblAssicuratoCodPolizza.AsInteger   := CodPolizza;

    fdmtblAssicurato.Post;

  end;

begin
  inherited;

  // unit� immobiliari
  if fdmtblPolizzaUnitaImmobiliari.AsInteger > 0 then
    MemorizzaTempGaranzia(CodPolizza, componiSiglaMassimaleFromStringa('U',
      fdmtblPolizzaSiglaMassimale.AsString), p_unita);
  // box
  if fdmtblPolizzaNumeroBox.AsInteger > 0 then
    MemorizzaTempGaranzia(CodPolizza, componiSiglaMassimaleFromStringa('B',
      fdmtblPolizzaSiglaMassimale.AsString), p_box);

  // perizia di parte
  if fdmtblAssicuratoPremioAnnuoLordo.AsCurrency > 0 then
  begin
    { TODO 1 -oAngelo -cBugs : Deve aggiungere all' assicurato i dati della granzia }
    MemorizzaTempGaranzia(CodPolizza, componiSiglaMassimaleFromStringa('E', fdmtblPolizzaSiglaMassimale.AsString),
      PRE_perizie);
    memorizzaGaranziaInAssicurato;
  end;
end;

procedure TDMDatiTempPolizzaCondTP.fdmtblAssicuratoNewRecord(DataSet: TDataSet);
begin
  inherited;
  fdmtblAssicuratoTipo.AsString := 'C'; // contratto
end;

procedure TDMDatiTempPolizzaCondTP.fdmtblAssicuratoPremioAnnuoLordoChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaCondTP.preparaReport;
var
  QueryRef: TDataSet;
begin
  inherited;
{$IFDEF TEMPTABLES}
  // fdmtblPolizza.SaveToFile(TPath.combine(SLPDatiAge.Dati_gen.dir_temp, 'TempPolizza.txt'), sfXML);
  TFmVediTempTables.ShowGaranzie(TUniGUIApplication(UniMainModule.uniApplication),
    UniMainModule.DMDatiTempPolizza.fdmtblPolizza, UniMainModule.DMDatiTempPolizza.fdmtblAssicurato,
    UniMainModule.DMDatiTempPolizza.fdmtblGaranzia);
  Exit;
{$ENDIF}
  // imposta i campi del report
  AzzeraCampiReport;

  frxPolizzaGen.Variables['barcode1'] := QuotedStr('00');
  frxPolizzaGen.Variables['barcode2'] := QuotedStr('00');

  SLPDatiAge                          := UniMainModule.DMdatiAge.SLPdati_age;
  frxPolizzaGen.EngineOptions.TempDir := SLPDatiAge.dati_gen.dir_temp;
  // frxPolizzaGen.Variables['versione']     := QuotedStr(leggiVersione);
  // frxPolizzaGen.Variables['facsimile']    := QuotedStr('N');
  frxPolizzaGen.Variables['peragenzia']   := QuotedStr('N');
  frxPolizzaGen.Variables['in_garanzia']  := QuotedStr('');
  frxPolizzaGen.Variables['in_garanzia1'] := QuotedStr('');
  frxPolizzaGen.Variables['polizza']      := QuotedStr('=======');

  // if facsimile then
  // begin
  // frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
  // frxPolizzaGen.Variables['POLIZZA']   := QuotedStr('Fac-Simile');
  // end;

  // frxPolizzaGen.Variables['ramo']        := QuotedStr(ramo_slp);

  frxPolizzaGen.Variables['agenzia']     := QuotedStr(SLPDatiAge.agenzia);
  frxPolizzaGen.Variables['citta_age']   := QuotedStr(SLPDatiAge.nome);
  frxPolizzaGen.Variables['descrizione'] := QuotedStr(SLPDatiAge.Denominazione);
  frxPolizzaGen.Variables['suba']        := QuotedStr(fdmtblPolizzaSubAgenzia.AsString);

  frxPolizzaGen.Variables['subP']     := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);
  frxPolizzaGen.Variables['Psost']    := QuotedStr(fdmtblPolizzaNumPolizzaSostituita.AsString);
  frxPolizzaGen.Variables['scade_il'] := QuotedStr(fdmtblPolizzaScadenzaPolizzaSostituita.AsString);
  frxPolizzaGen.Variables['effetto']  := QuotedStr(fdmtblPolizzaDecorrenza.AsString);
  frxPolizzaGen.Variables['scadenza'] := QuotedStr(fdmtblPolizzaScadenza.AsString);
  frxPolizzaGen.Variables['durata']   := QuotedStr('-' + fdmtblPolizzaDurataAnni.AsString + '-');

  frxPolizzaGen.Variables['giorni'] := QuotedStr(IfThen(fdmtblPolizzaDurataGiorni.AsString = '0', stringatest,
    fdmtblPolizzaDurataGiorni.AsString));

  frxPolizzaGen.Variables['fraz']       := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['scad1rata']  := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['emissione']  := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  frxPolizzaGen.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString);
  frxPolizzaGen.Variables['nato_a']     := QuotedStr(fdmtblPolizzaLocNascita.AsString);
  frxPolizzaGen.Variables['nato_il']    := QuotedStr(IfThen(yearOf(fdmtblPolizzaDataNascita.AsDateTime) > 1900,
    fdmtblPolizzaDataNascita.AsString, ''));

  frxPolizzaGen.Variables['residente_in'] :=
    QuotedStr(fdmtblPolizzaIndirizzo.AsString + ' - ' + fdmtblPolizzaCitta.AsString + ' - ' +
    fdmtblPolizzaProvincia.AsString);

  frxPolizzaGen.Variables['cap']    := QuotedStr(fdmtblPolizzaCap.AsString);
  frxPolizzaGen.Variables['cf_iva'] := QuotedStr(IfThen(fdmtblPolizzaPartitaIva.AsString > '',
    fdmtblPolizzaPartitaIva.AsString, fdmtblPolizzaCodiceFiscale.AsString));

  if fdmtblPolizzaRifReferente.AsInteger > 0 then
  begin
    QueryRef := UniMainModule.DMdatiAgeClienti.QFindReferente;
    UniMainModule.DMdatiAgeClienti.posizionaReferente(fdmtblPolizzaRifReferente.AsInteger);
    frxPolizzaGen.Variables['AMMINISTRATORE_PRO_TEMPORE'] := QuotedStr(QueryRef.fieldbyname('DENOMINAZ').AsString);
    frxPolizzaGen.Variables['AMMINISTRATORE_TEL']         := QuotedStr(fdmtblPolizzaTelefono.AsString);
    frxPolizzaGen.Variables['AMMINISTRATORE_CON_STUDIO_IN'] := QuotedStr(QueryRef.fieldbyname('INDIRIZZO').AsString);
    frxPolizzaGen.Variables['AMMINISTRATORE_RESIDENTE_IN'] := QuotedStr(QueryRef.fieldbyname('CITTA').AsString);
  end;
  frxPolizzaGen.Variables['NUMERO_DELLE_UNITA_IMMOBILIARI'] :=
    QuotedStr(fdmtblPolizzaUnitaImmobiliari.AsInteger.ToString);
  frxPolizzaGen.Variables['NUMERO_DEI_BOX'] := QuotedStr(fdmtblPolizzaNumeroBox.AsInteger.ToString);

  // if ((Epol1.text > '') and (Ecomp1.text > '') and (Eprem1.AsCurrency > 0) and (year(Edeco1.date) > 2000) and
  // (year(Escad1.date) > 2000)) then
  if fdmtblAssicuratoPremioAnnuoLordo.AsCurrency > 0.0 then
  begin
    frxPolizzaGen.Variables['npol1']   := QuotedStr(fdmtblAssicuratoCodFiscIvas.AsString);
    frxPolizzaGen.Variables['emessa1'] := QuotedStr(dtoc(fdmtblAssicuratoDataRilascio.AsDateTime));
    frxPolizzaGen.Variables['age1']    := QuotedStr(fdmtblAssicuratoCitta.AsString);
    frxPolizzaGen.Variables['plordo1'] := QuotedStr(zero(fdmtblAssicuratoPremioAnnuoLordo.AsCurrency));
    frxPolizzaGen.Variables['deco1']   := QuotedStr(dtoc(fdmtblAssicuratoDataRilascio.AsDateTime));
    frxPolizzaGen.Variables['scad1']   := QuotedStr(dtoc(fdmtblAssicuratoDataRilascio.AsDateTime));
    frxPolizzaGen.Variables['gar1']    := QuotedStr('GLOBALE FABBRICATI');
  end
  else
  begin
    frxPolizzaGen.Variables['npol1']   := QuotedStr('========');
    frxPolizzaGen.Variables['emessa1'] := QuotedStr('========');
    frxPolizzaGen.Variables['age1']    := QuotedStr('========');
    frxPolizzaGen.Variables['plordo1'] := QuotedStr('========');
    frxPolizzaGen.Variables['deco1']   := QuotedStr('========');
    frxPolizzaGen.Variables['scad1']   := QuotedStr('========');
    frxPolizzaGen.Variables['gar1']    := QuotedStr('========');
  end;

  // adeguamento ISTAT
  if fdmtblPolizzaindicizzata.asboolean then
    frxPolizzaGen.Variables['ADEGSI'] := QuotedStr('SI')
  else
    frxPolizzaGen.Variables['ADEGSI'] := QuotedStr('NO');

  // frxPolizzaGen.Variables['sconto']  := StringaTest;
  // if sconto>0 then frxPolizzaGen.Variables['sconto'] := QuotedStr(zero(sconto));

  frxPolizzaGen.Variables['ssconto'] := QuotedStr('');
  {
    if Opremio.scontoDurataBase>0 then
    begin
    frxPolizzaGen.Variables['sc1'] := quotedstr('-' + transform([Opremio.scontoDurataBase ],'999.99'));
    frxPolizzaGen.Variables['nettoScontato'] := quotedstr('#'+ zero(Opremio.nettoTotaleRicalcolato - Opremio.scontoDurataBase )+'#');
    frxPolizzaGen.Variables['ssconto'] := quotedstr(floattostr( Opremio.perc_sconto_durata)+'%');
    end else begin
    frxPolizzaGen.Variables['nettoScontato'] :=QuotedStr(' // // //');
    end;
  }
  frxPolizzaGen.Variables['ssconto']   := QuotedStr(floattostr(-Opremio.perc_sconto_durata));
  frxPolizzaGen.Variables['nettoBase'] :=
    QuotedStr('#' + zero(Opremio.nettoTotaleRicalcolato + Opremio.scontoDurataBase - PRE_perizie) + '#');

  frxPolizzaGen.Variables['nettoTotale'] := QuotedStr(zero(Opremio.nettoTotaleRicalcolato + Opremio.scontoDurataBase));
  if Opremio.perc_sconto_durata > 0 then
  begin
    frxPolizzaGen.Variables['nettoScontato'] := QuotedStr(zero(Opremio.nettoTotaleRicalcolato));
    // frxPolizzaGen.Variables['sc1'] := quotedstr('-' + transform([Opremio.scontoDurataBase ],'999.99'));

  end
  else
  begin
    frxPolizzaGen.Variables['nettoScontato'] := QuotedStr('/////');
    // frxPolizzaGen.Variables['nettoBase'] := quotedstr('#'+ zero(Opremio.nettoTotaleRicalcolato + Opremio.scontoDurataBase - PRE_perizie )+'#');
    // frxPolizzaGen.Variables['ssconto'] := quotedstr(floattostr( Opremio.perc_sconto_durata)+'%');
    // frxPolizzaGen.Variables['nettoBase']  := QuotedStr( '#'+zero(Opremio.nettoTotaleBaseArrotondato - PRE_perizie)+'#' );
  end;

  // frxPolizzaGen.Variables['maxBase']  := QuotedStr( '#'+zero(strToInt(copy(CBmassimale.Text,5,20)))+'#' );
  // frxPolizzaGen.Variables['maxBase'] := QuotedStr('#' + alltrim(copy(CBmassimale.text, 5, 20)) + ',00#');
  //
  // frxPolizzaGen.Variables['X1'] := QuotedStr('');
  // frxPolizzaGen.Variables['X2'] := QuotedStr('');
  // if ((CBmax1.ItemIndex > -1) and (PRE_perizie > 0)) then
  // begin
  // frxPolizzaGen.Variables['maxEst'] :=
  // QuotedStr('#' + zero(strToInt(StringReplace(copy(CBmax1.text, 5, 20), '.', '', [rfReplaceAll]))) + '#');
  // frxPolizzaGen.Variables['nettoEst'] := QuotedStr('#' + zero(PRE_perizie) + '#');
  // frxPolizzaGen.Variables['X1']       := QuotedStr('X');
  // end
  // else
  // begin
  // frxPolizzaGen.Variables['maxEst']   := QuotedStr(' // // //');
  // frxPolizzaGen.Variables['nettoEst'] := QuotedStr(' // // //');
  // end;

  // frxPolizzaGen.Variables['nettoScontato']  := QuotedStr(zero(Opremio.nettoTotaleRicalcolato));// QuotedStr(zero(Enettoannuo.asFloat));

  // parte finale con i premi
  frxPolizzaGen.Variables['rate_succ']   := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['netto1']      := QuotedStr(zero(fdmtblPolizzanetto1.AsCurrency));
  frxPolizzaGen.Variables['acc1']        := QuotedStr(zero(fdmtblPolizzaAccessori1.AsCurrency));
  frxPolizzaGen.Variables['int_fraz']    := QuotedStr(zero(fdmtblPolizzaInteressiFrazionamento1.AsCurrency));
  frxPolizzaGen.Variables['imponibile1'] := QuotedStr(zero(fdmtblPolizzaImponibile1.AsCurrency));
  frxPolizzaGen.Variables['tasse1']      := QuotedStr(zero(fdmtblPolizzaImposte1.AsCurrency));
  frxPolizzaGen.Variables['totale1']     := QuotedStr(zero(fdmtblPolizzaTotale1.AsCurrency));
  frxPolizzaGen.Variables['alla_firma']  := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['netto2']      := QuotedStr(zero(fdmtblPolizzanetto2.AsCurrency));
  frxPolizzaGen.Variables['acc2']        := QuotedStr(zero(fdmtblPolizzaaccessori2.AsCurrency));
  frxPolizzaGen.Variables['rimborso']    := QuotedStr(zero(fdmtblPolizzaRimborsoSost.AsCurrency));
  frxPolizzaGen.Variables['imponibile2'] := QuotedStr(zero(fdmtblPolizzaimponibile2.AsCurrency));
  frxPolizzaGen.Variables['tasse2']      := QuotedStr(zero(fdmtblPolizzaimposte2.AsCurrency));
  frxPolizzaGen.Variables['totale2']     := QuotedStr(zero(fdmtblPolizzatotale2.AsCurrency));
  frxPolizzaGen.Variables['emissione']   := QuotedStr(fdmtblPolizzaDataEmissione.AsString);

end;

function TDMDatiTempPolizzaCondTP.ReadTipoAssicurato: string;
begin
  Result := 'P';
end;

procedure TDMDatiTempPolizzaCondTP.SetDatiAssicuratoDaContraente(EnablePatente: boolean);
begin
  if not(fdmtblAssicurato.State in [dsInsert, dsEdit]) then
    fdmtblAssicurato.Edit;
  fdmtblAssicuratoDenominazione.AsString := fdmtblPolizzaContraente.AsString;
  fdmtblAssicuratoIndirizzo.AsString     := fdmtblPolizzaIndirizzo.AsString;
  fdmtblAssicuratoCitta.AsString         := fdmtblPolizzaCitta.AsString;
  fdmtblAssicuratocap.AsString           := fdmtblPolizzaCap.AsString;
  fdmtblAssicuratoProvincia.AsString     := fdmtblPolizzaProvincia.AsString;
  fdmtblAssicuratoCodFiscIvas.AsString   := fdmtblPolizzaCodiceFiscale.AsString;
  fdmtblAssicuratoEntrata.AsDateTime     := Date;
  fdmtblAssicurato.Post;
  fdmtblAssicurato.Append;

end;

initialization

RegisterClass(TDMDatiTempPolizzaCondTP);

end.
