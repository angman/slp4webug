inherited DMdatiAgePolizze: TDMdatiAgePolizze
  OldCreateOrder = True
  Height = 411
  Width = 628
  inherited QGetLastID: TDBISAMQuery
    Left = 298
    Top = 64
  end
  inherited tblTemp: TDBISAMTable
    Left = 168
    Top = 64
  end
  object QPolizze: TDBISAMQuery
    BeforeOpen = QPolizzeBeforeOpen
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from polizze '
      'where n_polizza > '#39#39
      'and id_gen_slp_age = :id_gen_slp_age')
    Params = <
      item
        DataType = ftString
        Name = 'id_gen_slp_age'
      end>
    Left = 24
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'id_gen_slp_age'
      end>
  end
  object QPolizzaEdit: TDBISAMQuery
    BeforeOpen = QPolizzaEditBeforeOpen
    AfterInsert = QPolizzaEditAfterInsert
    BeforePost = QPolizzaEditBeforePost
    OnNewRecord = QPolizzaEditNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from polizze '
      'where cod_polizza = :cod_polizza'
      '-- where n_polizza = :n_polizza'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end>
    Left = 96
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end>
  end
  object QFindPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select N_POLIZZA'
      'from polizze '
      'where n_polizza = :n_polizza'
      'and Rif_compagnia = :rif_compagnia')
    Params = <
      item
        DataType = ftString
        Name = 'n_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_compagnia'
      end>
    Left = 176
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'n_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_compagnia'
      end>
  end
  object Qlast_pag_s: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select * '
      ''
      'from s_scadenze'
      ''
      'where n_polizza=:polizza and tipo_titolo in ('#39'10'#39','#39'11'#39','#39'20'#39')'
      ''
      'order by rata desc'
      'top 1')
    Params = <
      item
        DataType = ftUnknown
        Name = 'polizza'
      end>
    Left = 329
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'polizza'
      end>
  end
  object Qlast_pag: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select decorrenza, lordo, rata, scadenza'
      'from scadenze'
      'where rif_cod_polizza=:rif_cod_polizza'
      'and tipo_titolo in ('#39'10'#39','#39'11'#39','#39'20'#39')'
      'and not (data_pag is null)'
      ''
      'union'
      ''
      'select decorrenza, lordo, rata , scadenza'
      'from s_scadenze'
      'where rif_cod_polizza=:rif_cod_polizza'
      'and tipo_titolo in ('#39'10'#39','#39'11'#39','#39'20'#39')'
      ''
      'order by rata desc'
      'top 1')
    Params = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end>
    Left = 409
    Top = 12
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end>
  end
  object Qpol_da_perfez: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from slp_tpolizze'
      'where n_polizza > '#39#39
      'and (status= '#39#39' or status = null )'
      'order by n_polizza')
    Params = <>
    Left = 32
    Top = 80
  end
  object DSpol_da_perfez: TDataSource
    DataSet = Qpol_da_perfez
    Left = 33
    Top = 137
  end
  object Qgar_base: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from gar_base'
      'where cod_tipo_contratto= :cod_tipo_contratto'
      '-- and sigla= :sigla')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_contratto'
      end>
    Left = 161
    Top = 121
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_contratto'
      end>
  end
  object Qtipo_pol: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_pol'
      'where cod_tipo_pol=:cod_tipo_pol')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_pol'
      end>
    Left = 217
    Top = 89
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_pol'
      end>
  end
  object QgaranzieLista: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select descrizione, massimale, premio, note, cod_gar_base, codic' +
        'e_slp, sigla, tipo_riga'
      'from gar_base'
      'where cod_tipo_contratto=:cod_tipo_contratto'
      'and tipo_riga=:tipo_riga'
      'order by sigla')
    Params = <
      item
        DataType = ftInteger
        Name = 'cod_tipo_contratto'
        Value = 0
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end>
    ReadOnly = True
    Left = 337
    Top = 105
    ParamData = <
      item
        DataType = ftInteger
        Name = 'cod_tipo_contratto'
        Value = 0
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end>
  end
  object Qgar_baseT: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from gar_base')
    Params = <>
    Left = 113
    Top = 137
  end
  object QNumeriDisponibiliAgenzia: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select count(*) as num_disp'
      ''
      'from ind_polizze'
      'where '
      'tipo_polizza=:sigla'
      '-- and data_stampa is null'
      'and '
      '('
      '(data_stampa is null) or (EXTRACT(YEAR FROM data_stampa)<1950)'
      ')')
    Params = <
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
    Left = 208
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
  end
  object QNumPolizzaDuplicate: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select n_polizza'
      'from slp_tpolizze '
      'where n_polizza = :npolizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'npolizza'
      end>
    Left = 41
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'npolizza'
      end>
  end
  object QgaranziePolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select '
      '   g.descrizione "Decrizione",  g2.premio "PremioImponibile", '
      '   g2.valore_assicurato "ValoreAss", g2.valore_if "ValoreIF", '
      
        '   g.sigla "Sigla", g2.data_in "DataIngresso" , g2.cod_garanzia ' +
        '"cod_garanzia",'
      
        '  g2.franchigia "Franchigia", g2.massimale "Massimale", g2.rif_g' +
        'ar_base,'
      
        '  g.perc_tasse "PTasse", g2.lordo "Lordo", g2.tasse "Tasse", g2.' +
        'accessori "Accessori", g2.ssn "SSN"'
      ''
      'FROM  garanzie g2, gar_base g   '
      ''
      'WHERE  g2.rif_cod_polizza=:rif_cod_polizza'
      '   and g2.rif_gar_base=g.cod_gar_base')
    Params = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end>
    Left = 168
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end>
  end
  object QAssicuratiPolizza: TDBISAMQuery
    AfterScroll = QAssicuratiPolizzaAfterScroll
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select'
      '      COD_POLIZZA,'
      '      CASE'
      
        '      WHEN a4.tipo='#39'C'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Pol' +
        'izza: '#39' || a4.cod_fisc_iva || '#39' - '#39' || cast(a4.data_rilascio as ' +
        'char(10)) || '#39' - '#39' ||'
      
        '                                               cast(data_scadenz' +
        'a as char(10)) || '#39' - '#39' || citta || '#39' - '#39' || denominazione || '#39' ' +
        '- Age: '#39' || indirizzo || '#39' - Premio: '#39' || cast(cifra as char(10)' +
        ') '
      
        '      WHEN a4.tipo='#39'V'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Vei' +
        'colo: '#39' || a4.targa || '#39' - '#39' || a4.marca || '#39' - '#39'  || a4.modello' +
        ' || '#39' - '#39' || a4.hp_ql '
      ''
      
        '      WHEN a4.tipo='#39'P'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Pat' +
        'ente: '#39' || a4.patente || '#39' - Cat.: '#39' || a4.categoria_pat || '#39' - ' +
        #39' ||  '#39'Assicurato: '#39'  || a4.denominazione'
      ''
      
        '      WHEN a4.tipo='#39'N'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      ''
      
        '                                                || '#39' - '#39' ||  '#39'No' +
        'me: '#39' || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39' ' +
        '- '#39' || a4.citta || '#39' - '#39' || '
      
        '                                                a4.cap || '#39' - '#39' ' +
        '|| a4.prov || '#39' - '#39' || a4.cod_fisc_iva'
      
        '      WHEN a4.tipo='#39'A'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Var' +
        'ie: '#39'  || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39 +
        ' - '#39' || a4.citta || '#39' - '#39' || a4.cap || '#39' - '#39' || a4.prov '
      '      END AS "OggettoAssicurato",'
      '      cod_assicurati, '#39'U'#39' as movim,'
      '     tipo'
      ''
      'FROM  assicurati a4'
      ''
      'WHERE  cod_polizza = :rif_cod_polizza'
      ''
      '       and uscita >= '#39'1970-01-01'#39
      ''
      'union'
      ''
      'select'
      '      COD_POLIZZA,'
      '      CASE'
      
        '      WHEN a4.tipo='#39'C'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Pol' +
        'izza: '#39' || a4.cod_fisc_iva || '#39' - '#39' || cast(a4.data_rilascio as ' +
        'char(10)) || '#39' - '#39' || '
      
        '                                               cast(data_scadenz' +
        'a as char(10)) || '#39' - '#39' || citta || '#39' - '#39' || denominazione || '#39' ' +
        '- Age: '#39' || indirizzo || '#39' - Premio: '#39' || cast(cifra as char(10)' +
        ') '
      
        '      WHEN a4.tipo='#39'V'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Vei' +
        'colo: '#39' || a4.targa || '#39' - '#39' || a4.marca || '#39' - '#39' || a4.hp_ql ||' +
        ' '#39' - '#39' || a4.modello'
      ''
      
        '      WHEN a4.tipo='#39'P'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Pat' +
        'ente: '#39' || a4.patente || '#39' - Cat.: '#39' || a4.categoria_pat ||  '#39' -' +
        ' Ril. il '#39' || '
      
        '                                               cast(a4.data_rila' +
        'scio as char(10)) || '#39' - Scad.: '#39' || cast(data_scadenza as char(' +
        '10)) || '#39' - '#39' ||  '#39'Assicurato: '#39'  || a4.denominazione'
      ''
      
        '      WHEN a4.tipo='#39'N'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4)) '
      
        '                                                || '#39' - '#39' ||  '#39'No' +
        'me: '#39' || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39' ' +
        '- '#39' || a4.citta || '#39' - '#39' || '
      
        '                                                a4.cap || '#39' - '#39' ' +
        '|| a4.prov || '#39' - '#39' || a4.cod_fisc_iva'
      
        '      WHEN a4.tipo='#39'A'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Var' +
        'ie: '#39'  || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39 +
        ' - '#39' || a4.citta || '#39' - '#39' || a4.cap || '#39' - '#39' || a4.prov '
      '      END AS "OggettoAssicurato",'
      '      cod_assicurati, '#39'E'#39' as movim,'
      '     Tipo'
      ''
      'FROM   assicurati a4'
      'WHERE  a4.cod_polizza = :rif_cod_polizza'
      '               --a4.cod_assicurati =p2.rif_cod_assicurato'
      '               and a4.uscita is null')
    Params = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end>
    Left = 260
    Top = 225
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end>
  end
  object QtitoliPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select '
      
        '      scadenze.stato Stato,  scadenze.n_polizza Polizza, scadenz' +
        'e.denominaz Nome,'
      
        '     scadenze.scadenza Decorrenza,  round(scadenze.lordo,2) Lord' +
        'o,'
      
        '      scadenze.data_pag "Data pag.",       scadenze.Frazionam Fr' +
        'az, '
      '      compagni.nome Compagnia, '
      '      tipo_pol.descrizione "Tipo pol.",         '
      '      rami.descrizione Ramo,'
      
        '      scadenze.cod_scadenza, scadenze.data_pag, scadenze.rif_cod' +
        '_compagnia,'
      '      fogli_cassa.numero_fc "Numero FC", '
      '      scadenze.data_ann "Data ann.", '
      '      if(1>2,'#39' '#39','#39' '#39') "Causale ann.",'
      ''
      '      CASE'
      '      WHEN scadenze.tipo_titolo='#39'10'#39' THEN '#39'Pol nuova'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'11'#39' THEN '#39'Pol sost.'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'12'#39' THEN '#39'Appendic.'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'15'#39'THEN '#39'Inc. premio'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'50'#39' THEN '#39'Rilievo'#39
      ''
      '      END AS "Tipo Tit."'
      ''
      ''
      ''
      '   from '
      '      scadenze'
      
        '   LEFT OUTER JOIN compagni ON compagni.cod_compagnia = scadenze' +
        '.rif_cod_compagnia'
      
        '   LEFT OUTER JOIN polizze  ON polizze.cod_polizza = scadenze.ri' +
        'f_cod_polizza and '
      'polizze.stato='#39'V'#39
      ''
      '   LEFT OUTER JOIN rami ON rami.cod_ramo=polizze.rif_cod_ramo'
      
        '   LEFT OUTER JOIN tipo_pol ON tipo_pol.cod_tipo_pol=polizze.rif' +
        '_tipo_pol'
      
        '   LEFT OUTER JOIN fogli_cassa ON scadenze.foglio_cassa = fogli_' +
        'cassa.codice_foglio'
      'where rif_cod_polizza = :rif_cod_pol')
    Params = <
      item
        DataType = ftInteger
        Name = 'rif_cod_pol'
      end>
    Left = 340
    Top = 226
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rif_cod_pol'
      end>
  end
  object Qappendice: TDBISAMQuery
    AfterScroll = QappendiceAfterScroll
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      ''
      'from appendici'
      ''
      'where rif_polizza = :rif_polizza'
      ''
      'order by numero')
    Params = <
      item
        DataType = ftInteger
        Name = 'rif_polizza'
      end>
    Left = 36
    Top = 274
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rif_polizza'
      end>
  end
  object QdettAppendice: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select'
      '      CASE'
      
        '      WHEN a4.tipo='#39'C'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Pol' +
        'izza: '#39' || a4.cod_fisc_iva || '#39' - '#39' || cast(a4.data_rilascio as ' +
        'char(10)) || '#39' - '#39' || '
      
        '                                               cast(data_scadenz' +
        'a as char(10)) || '#39' - '#39' || citta || '#39' - '#39' || denominazione || '#39' ' +
        '- Age: '#39' || indirizzo || '#39' - Premio: '#39' || cast(cifra as char(10)' +
        ') '
      
        '      WHEN a4.tipo='#39'V'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Vei' +
        'colo: '#39' || a4.targa || '#39' - '#39' || a4.marca || '#39' - '#39'  || a4.modello' +
        ' || '#39' - '#39' || a4.hp_ql '
      ''
      
        '      WHEN a4.tipo='#39'P'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Pat' +
        'ente: '#39' || a4.patente || '#39' - Cat.: '#39' || a4.categoria_pat || '#39' - ' +
        #39' ||  '#39'Assicurato: '#39'  || a4.denominazione'
      ''
      
        '      WHEN a4.tipo='#39'N'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                                || '#39' - '#39' ||  '#39'No' +
        'me: '#39' || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39' ' +
        '- '#39' || a4.citta || '#39' - '#39' || '
      
        '                                                a4.cap || '#39' - '#39' ' +
        '|| a4.prov || '#39' - '#39' || a4.cod_fisc_iva'
      
        '      WHEN a4.tipo='#39'A'#39' THEN '#39'Uscita il: '#39' || cast(extract(DAY, a' +
        '4.Uscita) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.Uscita) as' +
        ' char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.Uscita) as char(4)) '
      
        '                                               || '#39' - '#39' ||  '#39'Var' +
        'ie: '#39'  || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39 +
        ' - '#39' || a4.citta || '#39' - '#39' || a4.cap || '#39' - '#39' || a4.prov '
      '      END AS "In garanzia Tit.",'
      '      cod_assicurati, '#39'U'#39' as movim'
      ''
      ''
      'FROM  assicurati a4'
      ''
      'WHERE  cod_polizza=:cod_polizza'
      '               and  rif_appendice_out=:rif_appendice'
      ''
      ''
      'union'
      ''
      'select'
      '      CASE'
      
        '      WHEN a4.tipo='#39'C'#39' THEN '#39'Entrata il: '#39' || cast(extract(DAY, ' +
        'a4.entrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata)' +
        ' as char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))' +
        ' || '#39' - '#39' ||  '#39'Polizza: '#39' || a4.cod_fisc_iva || '#39' - '#39' || cast(a4' +
        '.data_rilascio as char(10)) || '#39' - '#39' || cast(data_scadenza as ch' +
        'ar(10)) || '#39' - '#39' || citta || '#39' - '#39' || denominazione || '#39' - Age: ' +
        #39' || indirizzo || '#39' - Premio: '#39' || cast(cifra as char(10)) '
      
        '      WHEN a4.tipo='#39'V'#39' THEN '#39'Entrata il: '#39' || cast(extract(DAY, ' +
        'a4.entrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata)' +
        ' as char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))' +
        ' || '#39' - '#39' ||  '#39'Veicolo: '#39' || a4.targa || '#39' - '#39' || a4.marca || '#39' ' +
        '- '#39' || a4.modello || '#39' - '#39'  || a4.hp_ql '
      
        '      WHEN a4.tipo='#39'P'#39' THEN '#39'Entrata il: '#39' || cast(extract(DAY, ' +
        'a4.entrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata)' +
        ' as char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))' +
        ' || '#39' - '#39' ||  '#39'Patente: '#39' || a4.patente || '#39' - Cat.: '#39' || a4.cat' +
        'egoria_pat   || '#39' - '#39' ||  '#39'Assicurato: '#39'  || a4.denominazione'
      
        '      WHEN a4.tipo='#39'N'#39' THEN '#39'Entrata il: '#39' || cast(extract(DAY, ' +
        'a4.entrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata)' +
        ' as char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))' +
        ' || '#39' - '#39' ||  '#39'Nome: '#39' || a4.denominazione   || '#39' - Ind.: '#39' || a' +
        '4.indirizzo || '#39' - '#39' || a4.citta || '#39' - '#39' || a4.cap || '#39' - '#39' || ' +
        'a4.prov || '#39' - '#39' || a4.cod_fisc_iva'
      
        '      WHEN a4.tipo='#39'A'#39' THEN '#39'Entrata il: '#39' || cast(extract(DAY, ' +
        'a4.entrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata)' +
        ' as char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))' +
        ' || '#39' - '#39' ||  '#39'Varie: '#39'  || a4.denominazione   || '#39' - Ind.: '#39' ||' +
        ' a4.indirizzo || '#39' - '#39' || a4.citta || '#39' - '#39' || a4.cap || '#39' - '#39' |' +
        '| a4.prov '
      '      END AS "garanziaTit",'
      '      cod_assicurati, '#39'E'#39' as movim'
      ''
      ''
      'FROM  assicurati a4'
      ''
      'WHERE  cod_polizza=:cod_polizza'
      '                and rif_appendice_in=:rif_appendice'
      ''
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_appendice'
      end
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_appendice'
      end>
    Left = 156
    Top = 279
    ParamData = <
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_appendice'
      end
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_appendice'
      end>
  end
  object QAssicuratiPolizzaEdit: TDBISAMQuery
    BeforePost = QAssicuratiPolizzaEditBeforePost
    AfterPost = QAssicuratiPolizzaEditAfterPost
    OnNewRecord = QAssicuratiPolizzaEditNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select * '
      'from assicurati'
      'where COD_POLIZZA = :COD_POLIZZA'
      '    AND COD_ASSICURATI = :COD_ASSICURATI'
      '    AND USCITA IS NULL')
    Params = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end
      item
        DataType = ftInteger
        Name = 'COD_ASSICURATI'
      end>
    Left = 268
    Top = 297
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end
      item
        DataType = ftInteger
        Name = 'COD_ASSICURATI'
      end>
  end
  object QInsLKPolizzaAssicurato: TDBISAMQuery
    AfterPost = QAssicuratiPolizzaEditAfterPost
    OnNewRecord = QAssicuratiPolizzaEditNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'insert into polizze_assicurati'
      
        '(rif_cod_polizza, rif_cod_assicurato, data_ingresso, ID_GEN_SLP_' +
        'AGE)'
      
        'values (:rif_cod_polizza, :rif_cod_assicurato, :data_ingresso, :' +
        'ID_GEN_SLP_AGE)'
      '     ')
    Params = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_cod_assicurato'
      end
      item
        DataType = ftDate
        Name = 'data_ingresso'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 396
    Top = 289
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rif_cod_polizza'
      end
      item
        DataType = ftInteger
        Name = 'rif_cod_assicurato'
      end
      item
        DataType = ftDate
        Name = 'data_ingresso'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QAggTogliAssicurato: TDBISAMQuery
    OnNewRecord = QAssicuratiPolizzaEditNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'update assicurati'
      'set uscita = :dataUscita,'
      'dt_last_mod = current_date'
      'where  cod_assicurati = :cod_assicurato')
    Params = <
      item
        DataType = ftUnknown
        Name = 'dataUscita'
      end
      item
        DataType = ftUnknown
        Name = 'cod_assicurato'
      end>
    Left = 468
    Top = 201
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'dataUscita'
      end
      item
        DataType = ftUnknown
        Name = 'cod_assicurato'
      end>
  end
  object QDelLKPolizzaAssicurato: TDBISAMQuery
    AfterPost = QAssicuratiPolizzaEditAfterPost
    OnNewRecord = QAssicuratiPolizzaEditNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'delete from polizze_assicurati'
      'where rif_cod_polizza = :cod_polizza'
      '  and rif_cod_assicurato = :cod_assicurato     ')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'cod_assicurato'
      end>
    Left = 476
    Top = 249
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'cod_assicurato'
      end>
  end
  object QUpdPolizzaEdit: TDBISAMQuery
    BeforeOpen = QPolizzaEditBeforeOpen
    AfterInsert = QPolizzaEditAfterInsert
    BeforePost = QPolizzaEditBeforePost
    OnNewRecord = QPolizzaEditNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'update polizze '
      'set appendice = :NAppendice'
      'where Cod_polizza = :CodPolizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'NAppendice'
      end
      item
        DataType = ftUnknown
        Name = 'CodPolizza'
      end>
    Left = 248
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NAppendice'
      end
      item
        DataType = ftUnknown
        Name = 'CodPolizza'
      end>
  end
  object QInsScadenza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into scadenze (DENOMINAZ,'
      'RAMO,'
      'DESCRIZ,'
      'FRAZIONAM,'
      'LORDO,'
      'RATA,'
      'COD_CLI,'
      'N_POLIZZA,'
      'provvigioni,'
      'netto,'
      'ptasse,'
      'tasse,'
      'RIF_COD_COMPAGNIA,'
      'RIF_COD_POLIZZA,'
      'inc_indice,'
      'DECORRENZA,'
      'SCADENZA,'
      'TIPO_TITOLO,'
      'VALUTA,'
      'RIF_CLIENTE,'
      'ID_GEN_SLP_AGE)'
      'values ('
      ':DENOMINAZ,'
      ':RAMO,'
      ':DESCRIZ,'
      ':FRAZIONAM,'
      ':LORDO,'
      ':RATA,'
      ':COD_CLI,'
      ':N_POLIZZA,'
      ':provvigioni,'
      ':netto,'
      ':ptasse,'
      ':tasse,'
      ':RIF_COD_COMPAGNIA,'
      ':RIF_COD_POLIZZA,'
      ':inc_indice,'
      ':DECORRENZA,'
      ':SCADENZA,'
      ':TIPO_TITOLO,'
      ':VALUTA,'
      ':RIF_CLIENTE,'
      ':ID_GEN_SLP_AGE'
      ')')
    Params = <
      item
        DataType = ftString
        Name = 'DENOMINAZ'
      end
      item
        DataType = ftInteger
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'DESCRIZ'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'RATA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftCurrency
        Name = 'netto'
      end
      item
        DataType = ftCurrency
        Name = 'ptasse'
      end
      item
        DataType = ftCurrency
        Name = 'tasse'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'inc_indice'
      end
      item
        DataType = ftDate
        Name = 'DECORRENZA'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA'
      end
      item
        DataType = ftString
        Name = 'TIPO_TITOLO'
      end
      item
        DataType = ftString
        Name = 'VALUTA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 420
    Top = 90
    ParamData = <
      item
        DataType = ftString
        Name = 'DENOMINAZ'
      end
      item
        DataType = ftInteger
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'DESCRIZ'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'RATA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftCurrency
        Name = 'netto'
      end
      item
        DataType = ftCurrency
        Name = 'ptasse'
      end
      item
        DataType = ftCurrency
        Name = 'tasse'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'inc_indice'
      end
      item
        DataType = ftDate
        Name = 'DECORRENZA'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA'
      end
      item
        DataType = ftString
        Name = 'TIPO_TITOLO'
      end
      item
        DataType = ftString
        Name = 'VALUTA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QInsAppendice: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'INSERT INTO APPENDICI'
      '(  COD_APPENDICE,'
      '   TIPO_APPENDICE,'
      '   DESCRIZIONE,'
      '   RIF_TITOLO,'
      '   DATA_EMISSIONE,'
      '   NUMERO,'
      '   RIF_POLIZZA,'
      '   RIF_COD_CLI,'
      '   N_POLIZZA,'
      '   COMPAGNIA,'
      '   TESTO1,'
      '   TESTO2,'
      '   TESTO3,'
      '   FRAZ1,'
      '   FRAZ2,'
      '   SCADENZA1,'
      '   SCADENZA2,'
      '   TESTO,'
      '   IMPORTO,'
      '   NEXT_RATA,'
      '   RIF_COMPAGNIA,'
      '   DAL,'
      '   AL,'
      '   OPERATORE,'
      '   REPORT,'
      '   DT_LAST_MOD,'
      '   ID_GEN_SLP_AGE'
      ')'
      'VALUES'
      '('
      '  :COD_APPENDICE,'
      '  :TIPO_APPENDICE,'
      '  :DESCRIZIONE,'
      '  :RIF_TITOLO,'
      '  :DATA_EMISSIONE,'
      '  :NUMERO,'
      '  :RIF_POLIZZA,'
      '  :RIF_COD_CLI,'
      '  :N_POLIZZA,'
      '  :COMPAGNIA,'
      '  :TESTO1,'
      '  :TESTO2,'
      '  :TESTO3,'
      '  :FRAZ1,'
      '  :FRAZ2,'
      '  :SCADENZA1,'
      '  :SCADENZA2,'
      '  :TESTO,'
      '  :IMPORTO,'
      '  :NEXT_RATA,'
      '  :RIF_COMPAGNIA,'
      '  :DAL,'
      '  :AL,'
      '  :OPERATORE,'
      '  :REPORT,'
      '  :DT_LAST_MOD,'
      '  :ID_GEN_SLP_AGE'
      ')')
    Params = <
      item
        DataType = ftAutoInc
        Name = 'COD_APPENDICE'
      end
      item
        DataType = ftInteger
        Name = 'TIPO_APPENDICE'
      end
      item
        DataType = ftString
        Name = 'DESCRIZIONE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_TITOLO'
      end
      item
        DataType = ftDate
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftInteger
        Name = 'NUMERO'
      end
      item
        DataType = ftInteger
        Name = 'RIF_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftString
        Name = 'COMPAGNIA'
      end
      item
        DataType = ftString
        Name = 'TESTO1'
      end
      item
        DataType = ftString
        Name = 'TESTO2'
      end
      item
        DataType = ftString
        Name = 'TESTO3'
      end
      item
        DataType = ftString
        Name = 'FRAZ1'
      end
      item
        DataType = ftString
        Name = 'FRAZ2'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA1'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA2'
      end
      item
        DataType = ftMemo
        Name = 'TESTO'
      end
      item
        DataType = ftCurrency
        Name = 'IMPORTO'
      end
      item
        DataType = ftCurrency
        Name = 'NEXT_RATA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftDate
        Name = 'DAL'
      end
      item
        DataType = ftDate
        Name = 'AL'
      end
      item
        DataType = ftInteger
        Name = 'OPERATORE'
      end
      item
        DataType = ftBlob
        Name = 'REPORT'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 500
    Top = 98
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'COD_APPENDICE'
      end
      item
        DataType = ftInteger
        Name = 'TIPO_APPENDICE'
      end
      item
        DataType = ftString
        Name = 'DESCRIZIONE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_TITOLO'
      end
      item
        DataType = ftDate
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftInteger
        Name = 'NUMERO'
      end
      item
        DataType = ftInteger
        Name = 'RIF_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftString
        Name = 'COMPAGNIA'
      end
      item
        DataType = ftString
        Name = 'TESTO1'
      end
      item
        DataType = ftString
        Name = 'TESTO2'
      end
      item
        DataType = ftString
        Name = 'TESTO3'
      end
      item
        DataType = ftString
        Name = 'FRAZ1'
      end
      item
        DataType = ftString
        Name = 'FRAZ2'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA1'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA2'
      end
      item
        DataType = ftMemo
        Name = 'TESTO'
      end
      item
        DataType = ftCurrency
        Name = 'IMPORTO'
      end
      item
        DataType = ftCurrency
        Name = 'NEXT_RATA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftDate
        Name = 'DAL'
      end
      item
        DataType = ftDate
        Name = 'AL'
      end
      item
        DataType = ftInteger
        Name = 'OPERATORE'
      end
      item
        DataType = ftBlob
        Name = 'REPORT'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QAutoincAppendici: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'SELECT DISTINCT LASTAUTOINC('#39'APPENDICI'#39') AS CODAPP FROM APPENDIC' +
        'I')
    Params = <>
    Left = 408
    Top = 152
  end
  object QGetLastCodPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select distinct lastautoinc('#39'slp_tpolizze'#39') as LAST_CODPOLIZZA f' +
        'rom slp_tpolizze')
    Params = <>
    Left = 320
    Top = 168
  end
  object QSLPPolizzeCliente: TDBISAMQuery
    BeforeOpen = QPolizzeBeforeOpen
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select n_polizza, RIF_COMPAGNIA, cod_polizza'
      'from polizze '
      'where n_polizza > '#39#39
      'and rif_compagnia = 1'
      'and cod_cli = :cod_cli'
      'and stato = '#39'V'#39
      'and id_gen_slp_age = :id_gen_slp_age')
    Params = <
      item
        DataType = ftInteger
        Name = 'cod_cli'
      end
      item
        DataType = ftString
        Name = 'id_gen_slp_age'
      end>
    Left = 496
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'cod_cli'
      end
      item
        DataType = ftString
        Name = 'id_gen_slp_age'
      end>
  end
  object QFindPolizzaSostituita: TDBISAMQuery
    BeforeOpen = QPolizzaEditBeforeOpen
    AfterInsert = QPolizzaEditAfterInsert
    BeforePost = QPolizzaEditBeforePost
    OnNewRecord = QPolizzaEditNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from polizze '
      'where n_polizza = :n_polizza'
      'and rif_compagnia=1')
    Params = <
      item
        DataType = ftString
        Name = 'n_polizza'
      end>
    Left = 88
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'n_polizza'
      end>
  end
  object DBISAMQuery1: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select '
      
        '      scadenze.stato Stato,  scadenze.n_polizza Polizza, scadenz' +
        'e.denominaz Nome,'
      
        '     scadenze.scadenza Decorrenza,  round(scadenze.lordo,2) Lord' +
        'o,'
      
        '      scadenze.data_pag "Data pag.",       scadenze.Frazionam Fr' +
        'az, '
      '      compagni.nome Compagnia, '
      '      tipo_pol.descrizione "Tipo pol.",         '
      '      rami.descrizione Ramo,'
      
        '      scadenze.cod_scadenza, scadenze.data_pag, scadenze.rif_cod' +
        '_compagnia,'
      '      fogli_cassa.numero_fc "Numero FC", '
      '      scadenze.data_ann "Data ann.", '
      '      if(1>2,'#39' '#39','#39' '#39') "Causale ann.",'
      ''
      '      CASE'
      '      WHEN scadenze.tipo_titolo='#39'10'#39' THEN '#39'Pol nuova'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'11'#39' THEN '#39'Pol sost.'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'12'#39' THEN '#39'Appendic.'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'15'#39'THEN '#39'Inc. premio'#39
      ''
      '      WHEN scadenze.tipo_titolo='#39'50'#39' THEN '#39'Rilievo'#39
      ''
      '      END AS "Tipo Tit."'
      ''
      ''
      ''
      '   from '
      '      scadenze'
      
        '   LEFT OUTER JOIN compagni ON compagni.cod_compagnia = scadenze' +
        '.rif_cod_compagnia'
      
        '   LEFT OUTER JOIN polizze  ON polizze.cod_polizza = scadenze.ri' +
        'f_cod_polizza and '
      'polizze.stato='#39'V'#39
      ''
      '   LEFT OUTER JOIN rami ON rami.cod_ramo=polizze.rif_cod_ramo'
      
        '   LEFT OUTER JOIN tipo_pol ON tipo_pol.cod_tipo_pol=polizze.rif' +
        '_tipo_pol'
      
        '   LEFT OUTER JOIN fogli_cassa ON scadenze.foglio_cassa = fogli_' +
        'cassa.codice_foglio'
      'where rif_cod_polizza = :rif_cod_pol')
    Params = <
      item
        DataType = ftInteger
        Name = 'rif_cod_pol'
      end>
    Left = 524
    Top = 338
    ParamData = <
      item
        DataType = ftInteger
        Name = 'rif_cod_pol'
      end>
  end
  object QgetPromoter: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      
        'from polizze left join clienti on clienti.cod_cliente=polizze.co' +
        'd_cli'
      'where cod_polizza=:cod_polizza'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    Left = 504
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
  object QricalcolaPremio: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select sum(premio) as premiotot, sum(ssn) as tot_ssn, sum(tasse)' +
        ' as tot_tasse'
      'from garanzie'
      'where rif_cod_polizza =:cod_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
    ReadOnly = True
    Left = 427
    Top = 338
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_polizza'
      end>
  end
  object Qins_pol_new: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      '-- START TRANSACTION;'
      'INSERT INTO polizze (stato, DATA_EMISSIONE, ID_GEN_SLP_AGE) '
      'VALUES ('#39'V'#39' , :data , :ID_AGE);'
      '-- COMMIT FLUSH;'
      
        'select distinct lastautoinc('#39'polizze'#39') as LAST_CODPOLIZZA from p' +
        'olizze')
    Params = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'ID_AGE'
      end>
    Left = 96
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'ID_AGE'
      end>
  end
end
