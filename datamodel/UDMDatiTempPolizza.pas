/// <summary>
/// contiene i dataset in memoria per l'editing della polizza base
/// </summary>
unit UDMDatiTempPolizza;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Generics.Collections, UToPremio, UdmdatiAge, UDMDatiBasePolizza,
  UTipoPol, UTipoSalvataggioDocumento, frxExportImage, frxClass, frxExportBaseDialog, frxExportPDF, frxGZip,
  frxRich;

type

  /// <summary>
  /// Exception base per errori
  /// </summary>
  /// <remarks>
  /// contiene il codice dell'errore ed il messaggio
  /// </remarks>

  /// <summary>
  /// Evento utilizzato durante la fase di validazione per segnalare gli
  /// errori
  /// </summary>
  TOnSendValidationMessage = procedure(ACodMsg: Integer; const AMessage: string) of object;

  TproceduraVediPH = procedure(n_polizza: string) of object;

  TEnumEstensioniPolizza = (epNone, epIndicizzata, epRischioAccessorio, epAppControversie, epProblematicaRCA,
    epPerizieParte, epMancatoIntRCA, epProtezioneFamiglia, epPatente);

  TSetEstensioniPolizza = set of TEnumEstensioniPolizza;

  TDMDatiTempPolizza = class(TDataModule)

    /// <summary>
    /// Tabella in memoria contenente i dati della polizza e le scelte fatte
    /// nell'interfaccia grafica
    /// </summary>
    fdmtblPolizza: TFDMemTable;
    fdmtblPolizzaRifTipoPol: TIntegerField;
    fdmtblPolizzaDecorrenza: TDateField;
    fdmtblPolizzaScadenza: TDateField;
    fdmtblPolizzaFrazionamento: TStringField;
    fdmtblPolizzaNPolizza: TStringField;
    fdmtblPolizzaLordo: TCurrencyField;
    fdmtblPolizzaNetto: TCurrencyField;
    fdmtblPolizzaPTasse: TCurrencyField;
    fdmtblPolizzaAccessori: TCurrencyField;
    fdmtblPolizzaValuta: TStringField;
    fdmtblPolizzaTasse: TCurrencyField;
    fdmtblPolizzaNote: TMemoField;
    fdmtblPolizzaRifCompagnia: TIntegerField;
    fdmtblPolizzaRifCodCli: TIntegerField;
    fdmtblPolizzaCollaboratore: TIntegerField;
    fdmtblPolizzaRifCodRamo: TIntegerField;
    fdmtblPolizzaStato: TStringField;
    fdmtblPolizzaProvvigioni: TCurrencyField;
    fdmtblPolizzaPProvvigioni: TCurrencyField;
    fdmtblPolizzaClasse: TIntegerField;
    fdmtblPolizzaAgenzia: TStringField;
    fdmtblPolizzaSubAgenzia: TStringField;
    fdmtblPolizzaRatino1Rata: TBooleanField;
    fdmtblPolizzaScontoDurata: TBooleanField;
    fdmtblPolizzaUtente: TIntegerField;
    fdmtblPolizzaDataInserimento: TDateField;
    // -------------------------------------------------------
    // parte per la compilazione delle righe di totalizzazione del report delle polizze
    fdmtblPolizzaNetto1: TCurrencyField;
    fdmtblPolizzaAccessori1: TCurrencyField;
    fdmtblPolizzaInteressiFrazionamento1: TCurrencyField;
    fdmtblPolizzaImponibile1: TCurrencyField;
    fdmtblPolizzaImposte1: TCurrencyField;
    fdmtblPolizzaTotale1: TCurrencyField;
    fdmtblPolizzaNetto2: TCurrencyField;
    fdmtblPolizzaAccessori2: TCurrencyField;
    fdmtblPolizzaRimborsoSost: TCurrencyField;
    fdmtblPolizzaImponibile2: TCurrencyField;
    fdmtblPolizzaImposte2: TCurrencyField;
    fdmtblPolizzaTotale2: TCurrencyField;

    // parte nuova 12/03/2020 MB
    fdmtblPolizzaNetto1A: TCurrencyField;
    fdmtblPolizzaAccessori1A: TCurrencyField;
    fdmtblPolizzaInteressiFrazionamento1A: TCurrencyField;
    fdmtblPolizzaImponibile1A: TCurrencyField;
    fdmtblPolizzaImposte1A: TCurrencyField;
    fdmtblPolizzaTotale1A: TCurrencyField;
    fdmtblPolizzaNetto2A: TCurrencyField;
    fdmtblPolizzaAccessori2A: TCurrencyField;
    fdmtblPolizzaRimborsoSostA: TCurrencyField;
    fdmtblPolizzaImponibile2A: TCurrencyField;
    fdmtblPolizzaImposte2A: TCurrencyField;
    fdmtblPolizzaTotale2A: TCurrencyField;

    // ----------------------------------------------
    fdmtblPolizzaCombinazione: TStringField;
    fdmtblPolizzaForma: TStringField;
    fdmtblPolizzaEstensioneOM: TStringField;
    fdmtblPolizzaNumPolizzaSostituita: TStringField;
    fdmtblPolizzaScadenzaPolizzaSostituita: TDateField;

    fdmtblPolizzaScadenzaPrimaRata: TDateField;

    fdmtblPolizzaPolizzaStatus: TStringField;
    fdmtblPolizzaPolizzaDataPerfezionamento: TDateField;
    fdmtblPolizzaDataCancellazione: TDateField;
    fdmtblPolizzaVolumeAffari: TCurrencyField;
    fdmtblPolizzaPercAccessori: TCurrencyField;
    fdmtblPolizzaRagioneSociale: TStringField;
    fdmtblPolizzaDati1: TStringField;
    fdmtblPolizzaDati2: TStringField;
    fdmtblPolizzaDati3: TStringField;
    fdmtblPolizzaDati4: TStringField;
    fdmtblPolizzaEstensioni: TStringField;
    fdmtblPolizzaSiglaMassimale: TStringField;
    fdmtblPolizzaTipologia: TStringField;
    fdmtblPolizzaDatiVari: TMemoField;
    fdmtblPolizzaOrdine: TStringField;
    fdmtblPolizzaSpecial: TStringField;
    fdmtblPolizzaDataImportazione: TDateTimeField;
    fdmtblPolizzaCodiceImportazione: TIntegerField;
    fdmtblPolizzaIntermediataDa: TIntegerField;
    fdmtblPolizzaConvenzione: TIntegerField;
    fdmtblPolizzaRifReferente: TIntegerField;
    fdmtblPolizzaVarFrp: TMemoField;
    fdmtblPolizzaAppReport: TBlobField;
    fdmtblPolizzaConAppendice: TStringField;
    fdmtblPolizzaAppNumero: TIntegerField;
    fdmtblPolizzaAppDal: TDateField;
    fdmtblPolizzaAppAL: TDateField;
    fdmtblPolizzaAppOggetto: TStringField;
    fdmtblPolizzaPSconto: TIntegerField;
    fdmtblPolizzaInpDf: TStringField;
    fdmtblPolizzaSubPromoter: TIntegerField;
    fdmtblPolizzaDati5: TStringField;
    fdmtblPolizzaNSP: TIntegerField;
    fdmtblPolizzaTipoEmissione: TStringField;
    fdmtblPolizzaDtLastMod: TDateField;
    fdmtblPolizzaIdGenSlpAge: TIntegerField;
    fdmtblPolizzaAgenziaNome: TStringField;
    fdmtblPolizzaDurataAnni: TIntegerField;
    fdmtblPolizzaDurataMesi: TIntegerField;
    fdmtblPolizzaDurataGiorni: TIntegerField;
    fdmtblPolizzaNomeCliente: TStringField;
    fdmtblPolizzaCodCliente: TIntegerField;
    fdmtblPolizzaCodiceFiscale: TStringField;
    fdmtblPolizzaLocNascita: TStringField;
    fdmtblPolizzaDataNascita: TDateField;
    fdmtblPolizzaIndirizzo: TStringField;
    fdmtblPolizzaCap: TStringField;
    fdmtblPolizzaCitta: TStringField;
    fdmtblPolizzaProfessione: TIntegerField;
    fdmtblPolizzaDescrizioneAttivita: TStringField;
    fdmtblPolizzaTacitoRinnovo: TBooleanField;
    fdmtblPolizzaContraente: TStringField;
    fdmtblPolizzaDataEmissione: TDateField;
    fdmtbPolizzaCollaboratoreDescr: TStringField;

    fdmtblGaranzia: TFDMemTable;
    fdmtblGaranziaMassimale: TCurrencyField;
    fdmtblGaranziaPremio: TCurrencyField;
    fdmtblGaranziaDataIn: TDateField;
    fdmtblGaranziaDataOut: TDateField;
    fdmtblGaranziaRifCodPolizza: TIntegerField;
    fdmtblGaranziaValoreAssicurato: TCurrencyField;
    fdmtblGaranziaValoreIf: TCurrencyField;
    fdmtblGaranziaRifGarBase: TIntegerField;
    fdmtblGaranziaFranchigia: TStringField;
    fdmtblGaranziaRifCodMassimale: TIntegerField;
    fdmtblGaranziaPercentuale: TCurrencyField;
    fdmtblGaranziaIdGenSlpAge: TIntegerField;

    fdmtblAssicurato: TFDMemTable;
    fdmtblAssicuratoCodAssicurato: TFDAutoIncField;
    fdmtblAssicuratoDenominazione: TStringField;
    fdmtblAssicuratoIndirizzo: TStringField;
    fdmtblAssicuratoCitta: TStringField;
    fdmtblAssicuratocap: TStringField;
    fdmtblAssicuratoProvincia: TStringField;
    fdmtblAssicuratoCodFiscIvas: TStringField;
    fdmtblAssicuratoNote: TMemoField;
    fdmtblAssicuratoEntrata: TDateField;
    fdmtblAssicuratoUscita: TDateField;
    fdmtblAssicuratoCausaleUscita: TIntegerField;
    fdmtblAssicuratoSostituitoDa: TIntegerField;
    fdmtblAssicuratoPatente: TStringField;
    fdmtblAssicuratoCategoriaPatente: TStringField;
    fdmtblAssicuratoDataRilascio: TDateField;
    fdmtblAssicuratoRilasciataDa: TStringField;
    fdmtblAssicuratoDataScadenza: TDateField;
    fdmtblAssicuratoTipoVeicolo: TStringField;
    fdmtblAssicuratoTarga: TStringField;
    fdmtblAssicuratoMarca: TStringField;
    fdmtblAssicuratoHpQl: TStringField;
    fdmtblAssicuratoRifCodTipoAssicurato: TIntegerField;
    fdmtblAssicuratoTipo: TStringField;
    fdmtblAssicuratoCodPolizza: TIntegerField;
    fdmtblAssicuratoDataScadRevisione: TDateField;
    fdmtblAssicuratoModello: TStringField;
    fdmtblAssicuratoClasse: TIntegerField;
    fdmtblAssicuratoTelaio: TStringField;
    fdmtblAssicuratoCc: TStringField;
    fdmtblAssicuratoDataImmatricolazione: TDateField;
    fdmtblAssicuratoPremio: TCurrencyField;
    fdmtblAssicuratoMassimale: TCurrencyField;
    fdmtblAssicuratoPremioSLP: TCurrencyField;
    fdmtblAssicuratoOggAggiuntivo: TStringField;
    fdmtblAssicuratoIdGenSlpAge: TIntegerField;
    fdmtblPolizzaPartitaIva: TStringField;
    fdmtblPolizzadt_preventivo: TDateTimeField;
    fdmtblPolizzaProvvig: TStringField;
    fdmtblPolizzaNPercAccessori: TIntegerField;
    frxpdfxprt: TfrxPDFExport;
    fdmtblGaranziaSiglaGaranzia: TStringField;
    fdmtblPolizzaNettoAnnuoTot: TCurrencyField;
    fdmtblPolizzaSconto: TCurrencyField;
    fdmtblPolizzaPremioScontato: TCurrencyField;
    frxPolizzaGen: TfrxReport;
    fdmtblPolizzaProvincia: TStringField;
    fdmtblPolizzaSubPromoterSigla: TStringField;
    fdmtblPolizzaCodPolizza: TFDAutoIncField;
    fdmtblGaranziaCodGaranzia: TFDAutoIncField;
    fdmtblPolizzaTotaleAB: TCurrencyField;

    fdmtblPolizzaArrotondamento: TCurrencyField;
    fdmtblPolizzaTelefono: TStringField;
    fdmtblPolizzaEMail: TStringField;
    frxAllegatiPOL: TfrxReport;
    fdmtblPolizzaSavCodPolizza: TIntegerField;
    frxGzipCompr: TfrxGZipCompressor;
    fdmtblPolizzaprofessioneDescriz: TStringField;
    fdmtblPolizzaTotaleAB0: TCurrencyField;
    fdmtblPolizzafatta_da: TIntegerField;
    fdmtblPolizzaintermediataDaSigla: TStringField;
    frxReport1: TfrxReport;

    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure fdmtblPolizzaDecorrenzaChange(Sender: TField);
    procedure fdmtblPolizzaScadenzaChange(Sender: TField);
    procedure fdmtblPolizzaNewRecord(DataSet: TDataSet);
    procedure fdmtblGaranziaNewRecord(DataSet: TDataSet);
    procedure fdmtblAssicuratoNewRecord(DataSet: TDataSet);
    procedure fdmtblPolizzaDataNascitaChange(Sender: TField);
    procedure fdmtblPolizzaBeforePost(DataSet: TDataSet);
    procedure fdmtblPolizzaAfterPost(DataSet: TDataSet);
    procedure fdmtblPolizzaFrazionamentoChange(Sender: TField);
    procedure fdmtblPolizzaRatino1RataChange(Sender: TField);
    procedure fdmtblPolizzaNPercAccessoriChange(Sender: TField);
    procedure fdmtblPolizzaPercAccessoriChange(Sender: TField);
    procedure fdmtblPolizzaAfterScroll(DataSet: TDataSet);
    procedure fdmtblPolizzaProvvigChange(Sender: TField);
  private
    { Private declarations }
    Dati_gen: TDati_gen_rec;

    FOpremio: ToPremio;
    FOpremioBis: ToPremio;

    listaScadenze: TstringList;
    // lstDataScad: TList<TDate>;
    FOnSendValidationMessage: TOnSendValidationMessage;
    FnettoAnnuoTotNONScontatoBis: currency;
    FnettoAnnuoTotNONScontato: currency;
    Fveicolo: Integer;
    FPtasse: currency;
    FPremio_da_rimborsare: currency;
    FSigla_pol: string;
    Data: TDate;
    FSelectedEstensioniPolizza: TSetEstensioniPolizza;
    SavSiglaMassimaleChangeEvt: TFieldNotifyEvent;
    FInNewRecord: boolean;
    FSLPDatiAge: TSLPdati_age_rec;
    FstringaTest: string;
    FStringaChecked: string;
    FCodPolizza: Integer;

    procedure calcolaDate;
    procedure calcolaProv;

    procedure OpenDatasets;

    procedure SendValidationMessage(ACodMsg: Integer; const AMessage: string);
    procedure sviluppaPremio(premio_base: currency; premio_baseBis: currency = 0);

    // function GetSiglaMassimale(SelectedMassimale: string; delimiter: string): string;
    function getSiglaMassimale: string;
    function GetContraenteIsSocieta: boolean;

  protected
    inCalcolaPremio: boolean;

    procedure AzzeraCampiReport; virtual;

    procedure AggiornaNettoPerEstensione(AField: TBooleanField; SiglaEstensione: string; var Importo: currency;
      NettoBis: boolean = false); overload;
    procedure AggiornaNettoPerEstensione(GaranziaChecked: boolean; SiglaEstensione: string; var Importo: currency;
      NettoBis: boolean = false); overload;

    function GetImportoGaranzia(SiglaGaranzia: string): currency;
    procedure AddImportoGaranzia(SiglaGaranzia: string; valore: currency);

    function getSiglaMassimaleFromStringa(StringaMassimale: string): string;

    function genera_cbPOL(polizza: string; pagina: Integer): string;

    procedure salva_dati_varfrp(pol: string);

    // --------------------------------------------------------------------------------------------------------------
    // Metodi da ridefinire nelle classi figlie
    procedure MemorizzaTempGaranzie(TipoPolizza: Integer); virtual;
    procedure MemorizzaTempGaranzia(CodPolizza: Integer; const Sigla: string; PremioCalcolato: currency = 0.0); virtual;
    // procedure MemorizzaTempGaranzia(CodPolizza: Integer; const Sigla: string); overload; virtual;

    procedure MemorizzaTempAssicurati(CodPolizza: Integer); virtual;
    procedure MemorizzaTempAssicurato(CodPolizza: Integer); overload; virtual; abstract;
    procedure MemorizzaTempAssicurato(CodPolizza: Integer; Ix: Integer); overload; virtual; abstract;

    procedure preparaReport; virtual;

    function per_allegati: string; virtual;

    // --------------------------------------------------------------------------------------------------------------

    property CodPolizza: Integer read FCodPolizza write FCodPolizza;
    property SiglaMassimale: string read getSiglaMassimale;
    property nettoAnnuoTotNONScontato: currency read FnettoAnnuoTotNONScontato write FnettoAnnuoTotNONScontato;
    property nettoAnnuoTotNONScontatoBis: currency read FnettoAnnuoTotNONScontatoBis write FnettoAnnuoTotNONScontatoBis;
    property Opremio: ToPremio read FOpremio write FOpremio;
    property OpremioBis: ToPremio read FOpremioBis write FOpremioBis;

    property ptasse: currency read FPtasse write FPtasse;
    property premio_da_rimborsare: currency read FPremio_da_rimborsare write FPremio_da_rimborsare;
    property Sigla_pol: string read FSigla_pol write FSigla_pol;
    Property InNewRecord: boolean read FInNewRecord write FInNewRecord;

    property veicolo: Integer read Fveicolo write Fveicolo;
    property SLPDatiAge: TSLPdati_age_rec read FSLPDatiAge write FSLPDatiAge;
    property stringaTest: string read FstringaTest write FstringaTest;
    property StringaChecked: string read FStringaChecked write FStringaChecked;

  public
    { Public declarations }
    OTipo_pol: Ttipo_pol;

    procedure AggiungiPolizza;
    procedure AnteprimaPolizza;

    procedure calcolaPremio;
    // --------------------------------------------------------------------------------------------------------------
    // Metodi da ridefinire nelle classi figlie
    procedure cal_premio; virtual; abstract;
    procedure DoControlli(BaseOnly: boolean = false); virtual;
    // -------------------------------------------------------------------------------------------------------------

    function checkPremioUnico: Integer;
    procedure CopiaDatiContraente(ACodCliente: Integer); virtual;
    procedure MemorizzaDocumento(TipoSalvataggioDocumento: TTipoSalvataggio; MostraReportPolizza: boolean; vediPH: TproceduraVediPH);
    function isBozza: boolean;
    function isPreventivo: boolean;
    function isPolizza: boolean;
    procedure impostaDatiPolizzaSostituita(ARimborso: currency; AScadenzaPolizzaSostituita: TDateTime);

    function isPersonaFisica(const CodFiscPartitaIva: string): boolean;

    function ReadTipoAssicurato: string; virtual;
    function RiprendiDocumento(CodPolizza: Integer): TTipoSalvataggio;
    procedure SetDatiAssicuratoDaContraente(EnablePatente: boolean); virtual;
    procedure SetPolizzaSostituita(ASelectedItem: Integer; const ASelectedDescription: string);
    function SLP_StampaPolizza(anteprima: boolean = false): Integer; virtual;

    function memorizzaReport(nomefile: string; polizza: boolean = false; in_stampa_in_serie: boolean = false;
      npol: string = ''; tipor: string = ''; data_d: TdateTime = 0.0; ncopie: Integer = 0): Integer;

    procedure memorizzaAllegatiPolizza(npol, Sigla_pol: string);
    function carica_ModuliVari(tipo_modulo, Sigla_pol: string; report: TfrxReport; xstream: TMemoryStream = nil): boolean;
    procedure valorizzaAllegatiPolizza(npol, tipo_modulo, Sigla_pol: string; report: TfrxReport);
    function genera_cbALL(polizza: string; num_pagine, pagina: Integer; tipo: string): string;
    function componiSiglaMassimaleFromStringa(const Prefix: string; StringaMassimale: string): string;

    property SelectedEstensioniPolizza: TSetEstensioniPolizza read FSelectedEstensioniPolizza;
    property OnSendValidationMessage: TOnSendValidationMessage read FOnSendValidationMessage
      write FOnSendValidationMessage;
    property ContraenteIsSocieta: boolean read GetContraenteIsSocieta;


  end;

const
  MAX_NUM_ASSICURATI = 4;

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

uses
  System.DateUtils, UCodiciErroriPolizza, date360, dbisamtb, System.Math, libreria, System.StrUtils, uniGUIApplication,
  MainModule, libSLP, ServerModule, System.IOUtils, UDMDatiReport, UStampePDFViewer, UVediTempTables,
  UPolizzaExceptions, System.RegularExpressions;

procedure TDMDatiTempPolizza.calcolaDate;
var
  DataEffetto, DataScadenza: TDate;
  p: Integer;
begin
  if InNewRecord then
    exit;

  DataEffetto  := fdmtblPolizzaDecorrenza.AsDateTime;
  DataScadenza := fdmtblPolizzaScadenza.AsDateTime;
  listaScadenze.Clear;
  if (DataEffetto > 0) and (DataScadenza > 0) then
  begin
    date360.dai_scadenze(DataEffetto, DataScadenza, dai_frazionamento(fdmtblPolizzaFrazionamento.AsString),
      listaScadenze);
    fdmtblPolizzaDurataGiorni.AsString        := listaScadenze.Strings[listaScadenze.count - 1];
    fdmtblPolizzaScadenzaPrimaRata.AsDateTime := strTodateTime(listaScadenze.Strings[listaScadenze.count - 4]);
    if fdmtblPolizzaRatino1Rata.AsBoolean then
    begin
      // se esiste un ratino inglobalo nella prima rata di polizza
      if fdmtblPolizzaDurataGiorni.asInteger > 0 then
      begin
        if listaScadenze.count >= 5 then
          fdmtblPolizzaScadenzaPrimaRata.AsDateTime := strTodateTime(listaScadenze.Strings[listaScadenze.count - 5])
        else
        begin
          fdmtblPolizzaScadenzaPrimaRata.AsDateTime := strTodateTime(listaScadenze.Strings[listaScadenze.count - 4]);
          fdmtblPolizzaRatino1Rata.AsBoolean        := false; // la polizza dura meno di un anno !!!!
        end;
      end;
    end;
    // fdmtblPolizzaRataAllaFirma.AsDateTime := fdmtblPolizzaScadenzaPrimaRata.AsDateTime;
    p                                := pos('+', listaScadenze.Strings[listaScadenze.count - 2]);
    fdmtblPolizzaDurataAnni.AsString := copy(listaScadenze.Strings[listaScadenze.count - 2], 1, p - 1);
    fdmtblPolizzaDurataMesi.AsString := copy(listaScadenze.Strings[listaScadenze.count - 2], p + 1, 2);
    // verifica se per l'eventuale polizza sostituita ci sono dei
    // rimborsi da fare per premi pagati e non ancora interamente goduti
    // if Epol_sost.text > '' then
    // begin
    // // if sost_scad_ultima_rata_pag>Eeffetto.Date then begin
    // giorni_rimborso:=date360.dist360(Eeffetto.date,sost_scad_ultima_rata_pag);
    // end;
    // end;
  end;

end;

procedure TDMDatiTempPolizza.calcolaPremio;
begin
  // if (not facsimile) and (not primavolta) then // il fac simile deve uscire a premio 0 !!!!
  // begin
  // calcola i premi ...
  if not inCalcolaPremio and not InNewRecord then
  begin
    inCalcolaPremio := true;
    try
      if fdmtblPolizza.State <> dsInsert then
        fdmtblPolizza.Edit;

      cal_premio;

      // adesso in nettoAnnuoTotNONScontato  trovo il premio annuo netto per la polizza corrente
      fdmtblPolizzaNetto.AsCurrency := nettoAnnuoTotNONScontato;
      fdmtblPolizzaPSconto.AsCurrency := 0;

      if nettoAnnuoTotNONScontato > 0 then
      begin

        sviluppaPremio(nettoAnnuoTotNONScontato, nettoAnnuoTotNONScontatoBis);
        // adesso in Opremio trovo tutte le cifre necessarie alla stampa della polizza
        // in Opremio.nettoTotaleBaseArrontondato  trovo il premio annuo effettivo della polizza (NON scontato)

        fdmtblPolizzaPSconto.AsCurrency := Opremio.perc_sconto_durata;
        fdmtblPolizzaSconto.AsCurrency:= RoundTo(Opremio.scontoDurataBase,-2);     // sconto totalae applicato
        fdmtblPolizzaNetto.AsCurrency:=RoundTo(Opremio.nettoTotaleBaseArrotondato, -2)  ;  // premio annuo totale non scontato
        fdmtblPolizzaNettoAnnuoTot.AsCurrency:=RoundTo(Opremio.nettoTotaleRicalcolato , -2);    // premio annuo totale scontato
        fdmtblPolizzaSconto.AsCurrency:= fdmtblPolizzaNetto.AsCurrency - fdmtblPolizzaNettoAnnuoTot.AsCurrency ;

        fdmtblPolizzaTotaleAB.AsCurrency  := 0;
        fdmtblPolizzaTotaleAB0.AsCurrency := 0;

        fdmtblPolizzaArrotondamento.AsCurrency := Opremio.arrotondamento;
        if nettoAnnuoTotNONScontatoBis > 0 then
        begin
          // caso di doppia tassazione ========================================================================
          // fdmtblPolizzaAccessori

          fdmtblPolizzaPSconto.AsCurrency := Opremio.perc_sconto_durata;
          fdmtblPolizzaSconto.AsCurrency:= RoundTo(OpremioBis.scontoDurataBase + Opremio.scontoDurataBase,-2);     // sconto totalae applicato
          fdmtblPolizzaNetto.AsCurrency:=RoundTo(OpremioBis.nettoTotaleBaseArrotondato + Opremio.nettoTotaleBaseArrotondato, -2)  ;  // premio annuo totale non scontato
          fdmtblPolizzaNettoAnnuoTot.AsCurrency:=RoundTo(OpremioBis.nettoTotaleRicalcolato + Opremio.nettoTotaleRicalcolato , -2);    // premio annuo totale scontato
          fdmtblPolizzaSconto.AsCurrency:= fdmtblPolizzaNetto.AsCurrency - fdmtblPolizzaNettoAnnuoTot.AsCurrency ;

          fdmtblPolizzaNetto1.AsCurrency               := Opremio.nettoRata;
          fdmtblPolizzaAccessori1.AsFloat              := Opremio.AccessoriRata;
          fdmtblPolizzaInteressiFrazionamento1.AsFloat := Opremio.InteressiFraz;
          fdmtblPolizzaImponibile1.AsFloat             := Opremio.ImponibileRata;
          fdmtblPolizzaImposte1.AsFloat                := Opremio.TasseRata;
          fdmtblPolizzaTotale1.AsFloat                 := Opremio.LordoRata;

          fdmtblPolizzaNetto2.AsFloat          := Opremio.NettoRataB;
          fdmtblPolizzaAccessori2.AsFloat      := Opremio.AccessoriRataB;
          fdmtblPolizzaImponibile2.AsFloat     := Opremio.ImponibileRataB;
          fdmtblPolizzaImposte2.AsFloat        := Opremio.TasseRataB;
          fdmtblPolizzaTotale2.AsFloat         := Opremio.LordoRataB;
          fdmtblPolizzaRimborsoSost.AsCurrency := Opremio.premio_rimborsato;

          if true then // (OpremioBis.lordoRata>0) or (OpremioBis.lordoRataB>0) then
          begin
            // se presente doppia tassazione nella polizza evidenzia tutti gli importi
            fdmtblPolizzaNetto1A.AsCurrency               := OpremioBis.nettoRata;
            fdmtblPolizzaAccessori1A.AsFloat              := OpremioBis.AccessoriRata;
            fdmtblPolizzaInteressiFrazionamento1A.AsFloat := OpremioBis.InteressiFraz;
            fdmtblPolizzaImponibile1A.AsFloat             := OpremioBis.ImponibileRata;
            fdmtblPolizzaImposte1A.AsFloat                := OpremioBis.TasseRata;
            fdmtblPolizzaTotale1A.AsFloat                 := OpremioBis.LordoRata;

            fdmtblPolizzaNetto2A.AsFloat          := OpremioBis.NettoRataB;
            fdmtblPolizzaAccessori2A.AsFloat      := OpremioBis.AccessoriRataB;
            fdmtblPolizzaImponibile2A.AsFloat     := OpremioBis.ImponibileRataB;
            fdmtblPolizzaImposte2A.AsFloat        := OpremioBis.TasseRataB;
            fdmtblPolizzaTotale2A.AsFloat         := OpremioBis.LordoRataB;
            fdmtblPolizzaRimborsoSostA.AsCurrency := OpremioBis.premio_rimborsato;

            fdmtblPolizzaTotaleAB0.AsCurrency := Opremio.LordoRata + OpremioBis.LordoRata;
            fdmtblPolizzaTotaleAB.AsCurrency  := Opremio.LordoRataB + OpremioBis.LordoRataB;
          end
          else
          begin

          end;
          {
            // codice prima di modifiche 12/03/2020 MB
            fdmtblPolizzaNetto1.AsCurrency               := Opremio.nettoRata + OpremioBis.nettoRata;
            fdmtblPolizzaAccessori1.AsFloat              := Opremio.AccessoriRata + OpremioBis.AccessoriRata;
            fdmtblPolizzaInteressiFrazionamento1.AsFloat := Opremio.InteressiFraz + OpremioBis.InteressiFraz;
            fdmtblPolizzaImponibile1.AsFloat             := Opremio.ImponibileRata + OpremioBis.ImponibileRata;
            fdmtblPolizzaImposte1.AsFloat                := Opremio.TasseRata + OpremioBis.TasseRata;
            fdmtblPolizzaTotale1.AsFloat                 := Opremio.LordoRata + OpremioBis.LordoRata;

            fdmtblPolizzaNetto2.AsFloat     := Opremio.NettoRataB + OpremioBis.NettoRataB;
            fdmtblPolizzaAccessori2.AsFloat := Opremio.AccessoriRataB + OpremioBis.AccessoriRataB;

            fdmtblPolizzaImponibile2.AsFloat     := Opremio.ImponibileRataB + OpremioBis.ImponibileRataB;
            fdmtblPolizzaImposte2.AsFloat        := Opremio.TasseRataB + OpremioBis.TasseRataB;
            fdmtblPolizzaTotale2.AsFloat         := Opremio.LordoRataB + OpremioBis.LordoRataB;
            fdmtblPolizzaRimborsoSost.AsCurrency := Opremio.premio_rimborsato + OpremioBis.premio_rimborsato;
          }
        end
        else
        begin
          // caso di singola tassazione ========================================================================
          fdmtblPolizzaNetto1.AsCurrency               := Opremio.nettoRata;
          fdmtblPolizzaAccessori1.AsCurrency           := Opremio.AccessoriRata;
          fdmtblPolizzaInteressiFrazionamento1.AsFloat := Opremio.InteressiFraz;
          fdmtblPolizzaImponibile1.AsFloat             := Opremio.ImponibileRata;
          fdmtblPolizzaImposte1.AsFloat                := Opremio.TasseRata;
          fdmtblPolizzaTotale1.AsFloat                 := Opremio.LordoRata;

          fdmtblPolizzaNetto2.AsFloat     := Opremio.NettoRataB;
          fdmtblPolizzaAccessori2.AsFloat := Opremio.AccessoriRataB;

          fdmtblPolizzaImponibile2.AsFloat     := Opremio.ImponibileRataB;
          fdmtblPolizzaImposte2.AsFloat        := Opremio.TasseRataB;
          fdmtblPolizzaTotale2.AsFloat         := Opremio.LordoRataB;
          fdmtblPolizzaRimborsoSost.AsCurrency := Opremio.premio_rimborsato;

          // mb 12/05/2020
          fdmtblPolizzaNetto1A.AsCurrency               := OpremioBis.nettoRata;
          fdmtblPolizzaAccessori1A.AsFloat              := OpremioBis.AccessoriRata;
          fdmtblPolizzaInteressiFrazionamento1A.AsFloat := OpremioBis.InteressiFraz;
          fdmtblPolizzaImponibile1A.AsFloat             := OpremioBis.ImponibileRata;
          fdmtblPolizzaImposte1A.AsFloat                := OpremioBis.TasseRata;
          fdmtblPolizzaTotale1A.AsFloat                 := OpremioBis.LordoRata;

          fdmtblPolizzaNetto2A.AsFloat          := OpremioBis.NettoRataB;
          fdmtblPolizzaAccessori2A.AsFloat      := OpremioBis.AccessoriRataB;
          fdmtblPolizzaImponibile2A.AsFloat     := OpremioBis.ImponibileRataB;
          fdmtblPolizzaImposte2A.AsFloat        := OpremioBis.TasseRataB;
          fdmtblPolizzaTotale2A.AsFloat         := OpremioBis.LordoRataB;
          fdmtblPolizzaRimborsoSostA.AsCurrency := OpremioBis.premio_rimborsato;

          fdmtblPolizzaTotaleAB.AsCurrency  := Opremio.LordoRataB + OpremioBis.LordoRataB;
          fdmtblPolizzaTotaleAB0.AsCurrency := Opremio.LordoRata + OpremioBis.LordoRata;

        end;
      end
      else
      begin
        fdmtblPolizzaNetto1.AsFloat                  := 0;
        fdmtblPolizzaAccessori1.AsFloat              := 0;
        fdmtblPolizzaInteressiFrazionamento1.AsFloat := 0;
        fdmtblPolizzaImponibile1.AsFloat             := 0;
        fdmtblPolizzaImposte1.AsFloat                := 0;
        fdmtblPolizzaTotale1.AsFloat                 := 0;

        fdmtblPolizzaNetto2.AsFloat       := 0;
        fdmtblPolizzaAccessori2.AsFloat   := 0;
        fdmtblPolizzaRimborsoSost.AsFloat := 0;
        fdmtblPolizzaImponibile2.AsFloat  := 0;
        fdmtblPolizzaImposte2.AsFloat     := 0;
        fdmtblPolizzaTotale2.AsFloat      := 0;

        // modifiche 12/03/2020 MB
        fdmtblPolizzaNetto1A.AsCurrency               := 0;
        fdmtblPolizzaAccessori1A.AsFloat              := 0;
        fdmtblPolizzaInteressiFrazionamento1A.AsFloat := 0;
        fdmtblPolizzaImponibile1A.AsFloat             := 0;
        fdmtblPolizzaImposte1A.AsFloat                := 0;
        fdmtblPolizzaTotale1A.AsFloat                 := 0;

        fdmtblPolizzaNetto2A.AsFloat     := 0;
        fdmtblPolizzaAccessori2A.AsFloat := 0;

        fdmtblPolizzaImponibile2A.AsFloat     := 0;
        fdmtblPolizzaImposte2A.AsFloat        := 0;
        fdmtblPolizzaTotale2A.AsFloat         := 0;
        fdmtblPolizzaRimborsoSostA.AsCurrency := 0;

      end;
//      fdmtblPolizzaNetto.AsCurrency := Opremio.NettoTotaleRicalcolato;
      calcolaProv;
      fdmtblPolizza.Post;
      fdmtblPolizza.Edit;
    finally
      inCalcolaPremio := false;
    end;
  end;
end;

procedure TDMDatiTempPolizza.calcolaProv;
var
  p_acc, p_provv: currency;
  provvigioni, ttnetto, ttacc: currency;
  per_ratino, storno: single;
  enetto, eaccessori, perc: single;
  durata_anni: Integer;
  data1, data2: TdateTime;
  h, k: Integer;
  tt: string;
  qq_1, qq_2, qq_3: currency;

  function LeggiRappel(var ttnetto, ttacc, provvigioni: currency): boolean;
  begin
    if fdmtblPolizzaTacitoRinnovo.AsBoolean then
      // se STR salta qualsiasi incentivo !!!!!
      exit(false);

    Result := true;
    if (pos(trim(UniMainModule.SiglaPolizza), sigle_CIRCOLAZ_PERIZIE) > 0) and (not(fdmtblPolizzaNumPolizzaSostituita.AsString > '')) and
      ((fdmtblPolizzaProvvig.AsString = 'R')) then
    begin
      h := pos('[r', Dati_gen.sativ);
      if (h > 0) and (not(fdmtblPolizzaNumPolizzaSostituita.AsString > '')) then
      begin
        // flag per iniziative con il 50% delle provvigioni su OM DPPP e PERIZIE
        k := posex(']', Dati_gen.sativ, h + 1);
        if k > 0 then
        begin
          tt    := copy(Dati_gen.sativ, h, 19);
          data1 := stod(copy(tt, 3, 8));
          data2 := stod(copy(tt, 11, 8));
          // if ((poliennale='S') and (.sfldPolizzaTipoProvvigioni=0) and (Edurata_anni.AsInteger>1)) then
          if ((fdmtblPolizzaProvvig.asString = 'R') and (fdmtblPolizzaDurataAnni.asInteger > 1)) then
          begin
            if fdmtblPolizzaDurataAnni.asInteger = 2 then
              perc := 52;
            if fdmtblPolizzaDurataAnni.asInteger = 3 then
              perc := 53;
            if fdmtblPolizzaDurataAnni.asInteger >= 5 then
              perc := 54;
          end
          else
          begin
            if copy(tt, 19, 1) <> ']' then
            begin
              // caso in cui � stata indicata la percentuale della super provvigione da usare per le polizze
              tt   := copy(Dati_gen.sativ, h, 21);
              perc := strtoint(copy(tt, 19, 2));
            end
            else
            begin
              perc := 50;
            end;
          end;
          if (Date >= data1) and (Date <= data2) then
          begin
            // Eprovvigioni.AsFloat:=Eimponibile2.AsCurrency/100*perc;
            // parte per la gestione del ratino in caso ri premio 50% aggiunta il 18/01/2012 v 1.4.5.3
            if (fdmtblPolizzaDurataGiorni.asInteger > 0) then
            // in caso di ratino lascia tutto come � !!! sul ratino resta il 30 e non il 50
            begin
              // esiste un ratino
              if fdmtblPolizzaRatino1Rata.AsBoolean then
              begin
                ttnetto     := enetto - fdmtblPolizzaNetto1.AsCurrency;
                ttacc       := eaccessori - fdmtblPolizzaAccessori1.AsCurrency;
                per_ratino  := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                provvigioni := (fdmtblPolizzaNetto1.AsCurrency / 100 * perc) +
                  (fdmtblPolizzaAccessori1.AsCurrency / 100 * p_acc);
              end
              else
              begin
                ttnetto    := enetto;
                ttacc      := eaccessori;
                per_ratino := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
              end;
              fdmtblPolizzaProvvigioni.AsCurrency := provvigioni + per_ratino;
            end
            else
            begin
               fdmtblPolizzaProvvigioni.AsCurrency := fdmtblPolizzaImponibile2.AsCurrency / 100 * perc;
            end;
          end;
        end;
      end
      else
      begin
        h := pos('[#rA', Dati_gen.sativ);
        if (h > 0) and (not(fdmtblPolizzaNumPolizzaSostituita.AsString > '') and
          (pos(trim(UniMainModule.SiglaPolizza), sigle_CIRCOLAZ) > 0)) then
        begin
          // flag per iniziative con il 50% delle provvigioni su OM DPPP
          k := posex(']', Dati_gen.sativ, h + 1);
          if k > 0 then
          begin
            tt    := copy(Dati_gen.sativ, h, 19 + 2);
            data1 := stod(copy(tt, 3 + 2, 8));
            data2 := stod(copy(tt, 11 + 2, 8));
            if ((fdmtblPolizzaProvvig.asString = 'R') and (fdmtblPolizzaDurataAnni.asInteger > 1)) then
            begin
              if fdmtblPolizzaDurataAnni.asInteger = 2 then
                perc := 52;
              if fdmtblPolizzaDurataAnni.asInteger = 3 then
                perc := 53;
              if fdmtblPolizzaDurataAnni.asInteger >= 5 then
                perc := 54;
            end
            else
            begin
              if copy(tt, 19 + 2, 1) <> ']' then
              begin
                // caso in cui � stata indicata la percentuale della super provvigione da usare per le polizze
                tt   := copy(Dati_gen.sativ, h, 21 + 2);
                perc := strtoint(copy(tt, 19 + 2, 2));
              end
              else
              begin
                perc := 50;
              end;
            end;
            if (Date >= data1) and (Date <= data2) then
            begin
              // fdmtblPolizzaProvvigioni.AsCurrency:=fdmtblPolizzaImponibile2.AsCurrency/100*perc;
              // parte per la gestione del ratino in caso ri premio 50% aggiunta il 18/01/2012 v 1.4.5.3
              if (fdmtblPolizzaDurataGiorni.asInteger > 0) then
              // in caso di ratino lascia tutto come � !!! sul ratino resta il 30 e non il 50
              begin
                // esiste un ratino
                if fdmtblPolizzaRatino1Rata.AsBoolean then
                begin
                  ttnetto     := enetto - fdmtblPolizzaNetto1.AsCurrency;
                  ttacc       := eaccessori - fdmtblPolizzaAccessori1.AsCurrency;
                  per_ratino  := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                  provvigioni := (fdmtblPolizzaNetto1.AsCurrency / 100 * perc) +
                    (fdmtblPolizzaAccessori1.AsCurrency / 100 * p_acc);
                end
                else
                begin
                  ttnetto    := enetto;
                  ttacc      := eaccessori;
                  per_ratino := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                end;
                fdmtblPolizzaProvvigioni.AsCurrency := provvigioni + per_ratino;
              end
              else
              begin
                fdmtblPolizzaProvvigioni.AsCurrency := fdmtblPolizzaImponibile2.AsCurrency / 100 * perc;
              end;
            end;
          end;
        end;

        h := pos('[#rP', Dati_gen.sativ);
        if (h > 0) and (not(fdmtblPolizzaNumPolizzaSostituita.AsString > '') and
          (pos(trim(UniMainModule.SiglaPolizza), sigla_PERIZIE) > 0)) then
        begin
          // flag per iniziative con il 40% delle provvigioni su PP
          k := posex(']', Dati_gen.sativ, h + 1);
          if k > 0 then
          begin
            tt    := copy(Dati_gen.sativ, h, 19 + 2);
            data1 := stod(copy(tt, 3 + 2, 8));
            data2 := stod(copy(tt, 11 + 2, 8));
            if ((fdmtblPolizzaProvvig.asString = 'R') and (fdmtblPolizzaDurataAnni.asInteger > 1)) then
            begin
              if fdmtblPolizzaDurataAnni.asInteger = 2 then
                perc := 42;
              if fdmtblPolizzaDurataAnni.asInteger = 3 then
                perc := 43;
              if fdmtblPolizzaDurataAnni.asInteger >= 5 then
                perc := 44;
            end
            else
            begin
              if copy(tt, 19 + 2, 1) <> ']' then
              begin
                // caso in cui � stata indicata la percentuale della super provvigione da usare per le polizze
                tt   := copy(Dati_gen.sativ, h, 21 + 2);
                perc := strtoint(copy(tt, 19 + 2, 2));
              end
              else
              begin
                perc := 40;
              end;
            end;
            if (Date >= data1) and (Date <= data2) then
            begin
              // fdmtblPolizzaProvvigioni.AsCurrency:=fdmtblPolizzaImponibile2.AsCurrency/100*perc;
              // parte per la gestione del ratino in caso ri premio 50% aggiunta il 18/01/2012 v 1.4.5.3
              if (fdmtblPolizzaDurataGiorni.asInteger > 0) then
              // in caso di ratino lascia tutto come � !!! sul ratino resta il 30 e non il 50
              begin
                // esiste un ratino
                if fdmtblPolizzaRatino1Rata.AsBoolean then
                begin
                  ttnetto     := enetto - fdmtblPolizzaNetto1.AsCurrency;
                  ttacc       := eaccessori - fdmtblPolizzaAccessori1.AsCurrency;
                  per_ratino  := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                  provvigioni := (fdmtblPolizzaNetto1.AsCurrency / 100 * perc) +
                    (fdmtblPolizzaAccessori1.AsCurrency / 100 * p_acc);
                end
                else
                begin
                  ttnetto    := enetto;
                  ttacc      := eaccessori;
                  per_ratino := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                end;
                fdmtblPolizzaProvvigioni.AsCurrency := provvigioni + per_ratino;
              end
              else
              begin
                fdmtblPolizzaProvvigioni.AsCurrency := fdmtblPolizzaImponibile2.AsCurrency / 100 * perc;
              end;
            end;
          end;
        end;

        // rappel per auto con 40% su premio netto e accessori normali
        h := pos('[#ra', Dati_gen.sativ);
        if (h > 0) and (not(fdmtblPolizzaNumPolizzaSostituita.AsString > '') and
          (pos(trim(UniMainModule.SiglaPolizza), sigle_CIRCOLAZ) > 0)) then
        begin
          // flag per iniziative con il 40% delle provvigioni su OM DPPP
          k := posex(']', Dati_gen.sativ, h + 1);
          if k > 0 then
          begin
            tt    := copy(Dati_gen.sativ, h, 19 + 2);
            data1 := stod(copy(tt, 3 + 2, 8));
            data2 := stod(copy(tt, 11 + 2, 8));
            if ((fdmtblPolizzaProvvig.asString = 'R') and (fdmtblPolizzaDurataAnni.asInteger > 1)) then
            begin
              if fdmtblPolizzaDurataAnni.asInteger = 2 then
                perc := 42;
              if fdmtblPolizzaDurataAnni.asInteger = 3 then
                perc := 43;
              if fdmtblPolizzaDurataAnni.asInteger >= 5 then
                perc := 44;
            end
            else
            begin
              if copy(tt, 19 + 2, 1) <> ']' then
              begin
                // caso in cui � stata indicata la percentuale della super provvigione da usare per le polizze
                tt   := copy(Dati_gen.sativ, h, 21 + 2);
                perc := strtoint(copy(tt, 19 + 2, 2));
              end
              else
              begin
                perc := 50;
              end;
            end;
            if (Date >= data1) and (Date <= data2) then
            begin
              // parte per la gestione del ratino in caso ri premio 50% aggiunta il 18/01/2012 v 1.4.5.3
              if (fdmtblPolizzaDurataGiorni.asInteger > 0) then
              // in caso di ratino lascia tutto come � !!! sul ratino resta il 30 e non il 50
              begin
                // esiste un ratino
                if fdmtblPolizzaRatino1Rata.AsBoolean then
                begin
                  ttnetto     := enetto - fdmtblPolizzaNetto1.AsCurrency;
                  ttacc       := eaccessori - fdmtblPolizzaAccessori1.AsCurrency;
                  per_ratino  := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                  provvigioni := (fdmtblPolizzaNetto1.AsCurrency / 100 * perc) +
                    (fdmtblPolizzaAccessori1.AsCurrency / 100 * p_acc);
                end
                else
                begin
                  ttnetto    := enetto;
                  ttacc      := eaccessori;
                  per_ratino := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                end;
                fdmtblPolizzaProvvigioni.AsCurrency := provvigioni + per_ratino;
              end
              else
              begin
                fdmtblPolizzaProvvigioni.AsCurrency := (fdmtblPolizzaNetto2.AsCurrency / 100 * perc) +
                  (fdmtblPolizzaAccessori2.AsCurrency / 100 * p_acc);
              end;
            end;
          end;
        end;

        // rappel per perizie di parte con 40% su premio netto e accessori normali
        h := pos('[#rp', Dati_gen.sativ);
        if (h > 0) and (not(fdmtblPolizzaNumPolizzaSostituita.AsString > '') and
          (pos(trim(UniMainModule.SiglaPolizza), sigla_PERIZIE) > 0)) then
        begin
          // flag per iniziative con il 40% delle provvigioni su OM DPPP
          k := posex(']', Dati_gen.sativ, h + 1);
          if k > 0 then
          begin
            tt    := copy(Dati_gen.sativ, h, 19 + 2);
            data1 := stod(copy(tt, 3 + 2, 8));
            data2 := stod(copy(tt, 11 + 2, 8));
            if ((fdmtblPolizzaProvvig.asString = 'R') and (fdmtblPolizzaDurataAnni.asInteger > 1)) then
            begin
              if fdmtblPolizzaDurataAnni.asInteger = 2 then
                perc := 42;
              if fdmtblPolizzaDurataAnni.asInteger = 3 then
                perc := 43;
              if fdmtblPolizzaDurataAnni.asInteger >= 5 then
                perc := 44;
            end
            else
            begin
              if copy(tt, 19 + 2, 1) <> ']' then
              begin
                // caso in cui � stata indicata la percentuale della super provvigione da usare per le polizze
                tt   := copy(Dati_gen.sativ, h, 21 + 2);
                perc := strtoint(copy(tt, 19 + 2, 2));
              end
              else
              begin
                perc := 50;
              end;
            end;
            if (Date >= data1) and (Date <= data2) then
            begin
              // parte per la gestione del ratino in caso ri premio 50% aggiunta il 18/01/2012 v 1.4.5.3
              if (fdmtblPolizzaDurataGiorni.asInteger > 0) then
              // in caso di ratino lascia tutto come � !!! sul ratino resta il 30 e non il 50
              begin
                // esiste un ratino
                if fdmtblPolizzaRatino1Rata.AsBoolean then
                begin
                  ttnetto     := enetto - fdmtblPolizzaNetto1.AsCurrency;
                  ttacc       := eaccessori - fdmtblPolizzaAccessori1.AsCurrency;
                  per_ratino  := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                  provvigioni := (fdmtblPolizzaNetto1.AsCurrency / 100 * perc) +
                    (fdmtblPolizzaAccessori1.AsCurrency / 100 * p_acc);
                end
                else
                begin
                  ttnetto    := enetto;
                  ttacc      := eaccessori;
                  per_ratino := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                end;
                fdmtblPolizzaProvvigioni.AsCurrency := provvigioni + per_ratino;
              end
              else
              begin
                fdmtblPolizzaProvvigioni.AsCurrency := (fdmtblPolizzaNetto2.AsCurrency / 100 * perc) +
                  (fdmtblPolizzaAccessori2.AsCurrency / 100 * p_acc);
              end;
            end;
          end;
        end;

        // rappel per perizie di parte + cirlcaz con 40% su premio netto e accessori normali
        h := pos('[#rX', Dati_gen.sativ);
        if (h > 0) and (not(fdmtblPolizzaNumPolizzaSostituita.AsString > '') and
          ((pos(trim(UniMainModule.SiglaPolizza), sigla_PERIZIE) > 0) or (pos(trim(UniMainModule.SiglaPolizza), sigle_CIRCOLAZ) > 0))) then
        begin
          // flag per iniziative con il 40% delle provvigioni su OM DPPP
          k := posex(']', Dati_gen.sativ, h + 1);
          if k > 0 then
          begin
            tt    := copy(Dati_gen.sativ, h, 19 + 2);
            data1 := stod(copy(tt, 3 + 2, 8));
            data2 := stod(copy(tt, 11 + 2, 8));
            if ((fdmtblPolizzaProvvig.asString = 'R') and (fdmtblPolizzaDurataAnni.asInteger > 1)) then
            begin
              if fdmtblPolizzaDurataAnni.asInteger = 2 then
                perc := 42;
              if fdmtblPolizzaDurataAnni.asInteger = 3 then
                perc := 43;
              if fdmtblPolizzaDurataAnni.asInteger >= 5 then
                perc := 44;
            end
            else
            begin
              if copy(tt, 19 + 2, 1) <> ']' then
              begin
                // caso in cui � stata indicata la percentuale della super provvigione da usare per le polizze
                tt   := copy(Dati_gen.sativ, h, 21 + 2);
                perc := strtoint(copy(tt, 19 + 2, 2));
              end
              else
              begin
                perc := 50;
              end;
            end;
            if (Date >= data1) and (Date <= data2) then
            begin
              // parte per la gestione del ratino in caso ri premio 50% aggiunta il 18/01/2012 v 1.4.5.3
              if (fdmtblPolizzaDurataGiorni.asInteger > 0) then
              // in caso di ratino lascia tutto come � !!! sul ratino resta il 30 e non il 50
              begin
                // esiste un ratino
                if fdmtblPolizzaRatino1Rata.AsBoolean then
                begin
                  ttnetto     := enetto - fdmtblPolizzaNetto1.AsCurrency;
                  ttacc       := eaccessori - fdmtblPolizzaAccessori1.AsCurrency;
                  per_ratino  := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                  provvigioni := (fdmtblPolizzaNetto1.AsCurrency / 100 * perc) +
                    (fdmtblPolizzaAccessori1.AsCurrency / 100 * p_acc);
                end
                else
                begin
                  ttnetto    := enetto;
                  ttacc      := eaccessori;
                  per_ratino := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
                end;
                fdmtblPolizzaProvvigioni.AsCurrency := provvigioni + per_ratino;
              end
              else
              begin
                fdmtblPolizzaProvvigioni.AsCurrency := (fdmtblPolizzaNetto2.AsCurrency / 100 * perc) +
                  (fdmtblPolizzaAccessori2.AsCurrency / 100 * p_acc);
              end;
            end;
          end;
        end;
      end;

    end;
  end;

begin
  // calcola le provvigioni relaive alla polizza che si sta preparando
  // --- CASO POLIZZA NUOVA
  // -- senza ratino
  // in base alla durata , al tipo di polizza e al tipo di provvigione scelta
  // (Ric Prec) recupera l'aliquota da applicare al premio netto e se ci sono
  // accessori recupera la percentuale degli accesori

  // dal 07-02-2007 non permettiamo pi� di trattenere provvigioni preceontate o poliennali
  // in genere visto che le polizza hanno tutte per legge la rescindibilit� annuale
  // durata_anni:=Edurata_anni.AsInteger;

  // modifica del 21/07/2011 ver 1.4.4.0 per reintrodurre la polienalit� per chi la richiede

  // if true then
  durata_anni := fdmtblPolizzaDurataAnni.asInteger;
  // else
  // durata_anni := 1; // Edurata_anni.AsInteger;

  // if is_sub_agenzia_097 then
  // durata_anni := 1; // messo il 2/09/2012

  if (durata_anni > 5) and (veicolo = 1) then
    // trattasi di veicolo: provvigione bloccata a 5 anni !!!!
    durata_anni := 5;

  UniMainModule.DMdatiAge.leggiAccessoriProvvigioni(UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger, p_acc,
    p_provv, 1, fdmtblPolizzaProvvig.AsString[1], durata_anni);

  // if conRischioB then
  // rischio := 'B'
  // else
  // rischio := 'A';

  // if durata_anni < 2 then
  // begin
  // // sempre e comunque ricorrente !!
  // p_provv := UniMainModule.DMdatiAge.leggiProvvigioni(UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger, 'R', 1);
  // // esegui la riduzione relativo all'eventuale  rischio B
  // if conRischioB then
  // p_provv := p_provv - 5;
  // end else begin
  // if fdmtblPolizzaProvvig.AsInteger = 0 then
  // begin
  // // provvigione ricorrente poliennale
  // p_provv := UniMainModule.DMdatiAge.leggiProvvigioni(UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger, 'R', durata_anni);
  // // esegui la riduzione relativo all'eventuale  rischio B
  // if conRischioB then
  // p_provv := p_provv - 4;
  // end else begin
  // // provvigione precontata
  // p_provv := UniMainModule.DMdatiAge.leggiProvvigioni(UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger, 'P', durata_anni);
  // // esegui la riduzione relativo all'eventuale  rischio B
  //
  // if conRischioB then
  // begin
  // if durata_anni in [2, 3, 4, 10] then
  // p_provv := p_provv - 10
  // else
  // if durata_anni in [5, 6, 7, 8, 9] then
  // p_provv := p_provv - 20
  // end;
  // end;
  // end;

  enetto     := fdmtblPolizzaNetto2.AsCurrency;
  eaccessori := fdmtblPolizzaAccessori2.AsCurrency;
  if fdmtblPolizzaNetto2A.AsCurrency > 0 then
  begin
    enetto     := enetto + fdmtblPolizzaNetto2A.AsCurrency;
    eaccessori := eaccessori + fdmtblPolizzaAccessori2A.AsCurrency;
  end;

  if fdmtblPolizzaRimborsoSost.AsCurrency > 0 then
  begin
    // calcola lo storno inteso anche come minor provvigione relativa a polizza ricorrente
    // ma per la quale si incassa meno di una rata a seguito di una restituzione di premio
    // al contraente per la polizza precedente sostituita
    if fdmtblPolizzaNumPolizzaSostituita.AsString > '' then
    begin
      enetto := fdmtblPolizzaImponibile2.AsCurrency; // - ErimborsoSost.Value;
      if fdmtblPolizzaNetto2A.AsCurrency > 0 then
        enetto := enetto + fdmtblPolizzaImponibile2A.AsCurrency;

      // calcola il nuovo importo relativo agli accessori
      enetto := roundTo(enetto / (1 + fdmtblPolizzaPercAccessori.AsCurrency / 100), -2);
      if fdmtblPolizzaNetto2A.AsCurrency > 0 then
        eaccessori := (fdmtblPolizzaImponibile2.AsCurrency + fdmtblPolizzaImponibile2A.AsCurrency) - enetto
      else
        eaccessori := fdmtblPolizzaImponibile2.AsCurrency - enetto;
    end;
  end;

  // if (copy(senza_tacito_rinnovo, 1, 1) = 'S') and (CBsenzaTacitoR.Checked) then
  // begin
  // // attivato senza tacito rinnovo !!!!
  // // provvigione fissa standard per STR = 22%
  // if copy(senza_tacito_rinnovo, 7, 1) = 'N' then
  // begin
  // p_provv := strTofloatDef(copy(senza_tacito_rinnovo, 2, 5), 22);
  // // p_acc:=22;   lascio gli accessori nativi dell'agenzia
  // end else begin
  // p_provv := strTofloatDef(copy(senza_tacito_rinnovo, 2, 5), 22);
  // p_acc   := strTofloatDef(copy(senza_tacito_rinnovo, 2, 5), 22);
  // // forzo il valore degli accessori al valore delle provvigioni STR
  // end;
  // end;

  provvigioni := 0;
  per_ratino  := 0;
  storno      := 0;

  if (fdmtblPolizzaDurataGiorni.asInteger > 0) then
  begin
    // esiste un ratino
    // beccati il 30% fisso della provvigione !
    if fdmtblPolizzaRatino1Rata.AsBoolean then
    begin
      ttnetto     := enetto - fdmtblPolizzaNetto1.AsCurrency;
      ttacc       := eaccessori - fdmtblPolizzaAccessori1.AsCurrency;
      per_ratino  := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
      provvigioni := (fdmtblPolizzaNetto1.AsCurrency / 100 * p_provv) +
        (fdmtblPolizzaAccessori1.AsCurrency / 100 * p_acc);
    end
    else
    begin
      ttnetto    := enetto;
      ttacc      := eaccessori;
      per_ratino := (ttnetto / 100 * 30) + (ttacc / 100 * p_acc);
    end;

  end
  else
  begin
    // caso senza ratino
    provvigioni := (enetto / 100 * p_provv) + (eaccessori / 100 * p_acc);
  end;

  fdmtblPolizzaProvvigioni.AsCurrency := provvigioni + per_ratino - storno;
  // devi calcolare la parte di provvigione percepita ma da restituire ....

  // CASO SOSTITUZIONE
  // if Epol_sost.text > '' then
  // begin
  // if DMdatiAge.Tpolizze.locate('n_polizza;rif_compagnia', vararrayof([Epol_sost.text, 1]), []) then
  // begin
  // // vedi cosa fare ....
  // if DMdatiAge.Tpolizze.FieldByName('tipo_provvigioni').AsString = 'R' then
  // begin
  // // non devi fare nessuno storno provvigionale ...
  //
  // end else begin
  // // prov precontate ! controlla la durata della polizza
  // if Eeffetto.Date - DMdatiAge.Tpolizze.FieldByName('decorrenza').AsDateTime < 90 then
  // begin
  // // polizza vecchia ha gi� passato il periodo di precondo: niente storno
  //
  // end else begin
  // // devi fare lo storno provvigionale !
  //
  // end;
  // end;
  //
  // end;
  // end;

  // se la polizza � nuovo e siamo nel periodo del rappel della circoare 195 del 07/04/2009....
  // se � una polizza di circolazione stradale o perizie di parte allora :
  // if false and (Edurata_giorni.asInteger > 0) then
  // // in caso di ratino lascia tutto come � !!! sul ratino resta il 30 e non il 50
  // begin
  //
  // end else
  // if (pos(alltrim(sigla_pol),'OR.JV.DU.OQ')>0) and (not(Epol_sost.Text>'')) and ((RGprovvig.ItemIndex=0)) then

  LeggiRappel(ttnetto, ttacc, provvigioni);

  // devi fare le considerazioni per il caso di sub_agente o persona non abilitata a vedere le provvigioni
  {
    // NOOOOOOO bisogna prevedere delle provvigioni solo da vedere a video nei casi di collaboratori, ma quelle
    // che vengono memorizzate devono sempre essere quelle di agenzia !!!!!
    if UniMainModule.DMdatiAge.is_utente_promoter then
    begin
    // se � un collaboratore allora ricalcola le provvigioni in base alle aliquote caricate nella sceda del
    // collaboratore
    fdmtblPolizzaProvvigioni.AsCurrency := fdmtblPolizzaProvvigioni.AsCurrency /100 * UniMainModule.DMdatiAge.get_provv_collab;
    end;
  }
end;

{$MESSAGE WARN 'Non viene mai chiamata'}

function TDMDatiTempPolizza.checkPremioUnico: Integer;
begin
  // if CBpremioUnico.Checked then
  // begin
  if fdmtblPolizzaFrazionamento.AsString <> 'A' then
  begin
    // MessageDlg('Attenzione ! In una polizza a premio unico il frazionamento DEVE essere ANNUALE !', mtInformation, [mbOK], 0);
    Result := 1;
    raise EPolizzaError.Create(CERR_PREUNI_FRAZ_NO_ANN, MSG_PREUNI_FRAZ_NO_ANN);
  end;
  // controlla che la durata non sia maggiore ai 3 anni !!!
  if fdmtblPolizzaDurataAnni.asInteger > 3 then
  begin
    // MessageDlg('ATTENZIONE ! La durata massima di una polizza a premio unico � di TRE anni.', mtInformation, [mbOK], 0);
    Result := 1;
    raise EPolizzaError.Create(CERR_PREUNI_DUR_GT_3, MSG_PREUNI_DUR_GT_3);
  end;
  Result := fdmtblPolizzaDurataAnni.asInteger;
  // fdmtblPolizzaRataAllaFirma.AsDateTime     := fdmtblPolizzaScadenza.AsDateTime;
  fdmtblPolizzaScadenzaPrimaRata.AsDateTime := fdmtblPolizzaScadenza.AsDateTime;
  // end;

end;

function TDMDatiTempPolizza.componiSiglaMassimaleFromStringa(const Prefix: string; StringaMassimale: string): string;
var
  ArrStr: TArray<string>;
begin
  Result := '';
  ArrStr := TRegEx.Split(StringaMassimale, '\D+');
  if Length(ArrStr) > 1 then
    Result := Prefix + ArrStr[1];

end;

procedure TDMDatiTempPolizza.SetDatiAssicuratoDaContraente(EnablePatente: boolean);
begin
  if not(fdmtblAssicurato.State in [dsInsert, dsEdit]) then
    fdmtblAssicurato.Edit;
  if EnablePatente and isPersonaFisica(fdmtblPolizzaCodiceFiscale.AsString) then
  begin
    fdmtblAssicuratoDenominazione.AsString := fdmtblPolizzaContraente.AsString;
    fdmtblAssicuratoCodFiscIvas.AsString   := fdmtblPolizzaCodiceFiscale.AsString;
  end
  else
  begin
    fdmtblAssicuratoDenominazione.Clear;
    fdmtblAssicuratoCodFiscIvas.Clear;
  end;
  fdmtblAssicurato.Post;
  fdmtblAssicurato.Edit;
end;

procedure TDMDatiTempPolizza.SetPolizzaSostituita(ASelectedItem: Integer; const ASelectedDescription: string);
begin
  // fdmtblPolizzaNumPolizzaSostituita.AsString        := ASelectedItem.ToString;
  fdmtblPolizzaNumPolizzaSostituita.AsString := UniMainModule.DMdatiAgePolizze.QPolizze.FieldByName
    ('n_polizza').AsString;
  fdmtblPolizzaScadenzaPolizzaSostituita.AsDateTime := UniMainModule.DMdatiAgePolizze.QPolizze.FieldByName('SCADENZA')
    .AsDateTime;
end;

procedure TDMDatiTempPolizza.CopiaDatiContraente(ACodCliente: Integer);
var
  QueryCliente: TDBISAMQuery;
  SavSiglaMassimaleChangeEvt: TFieldNotifyEvent;
begin

  QueryCliente := UniMainModule.DMdatiAgeClienti.QeditCliente;
  UniMainModule.DMdatiAgeClienti.PosizionaQuery(ACodCliente);
  if not(fdmtblPolizza.State in [dsInsert, dsEdit]) then
    fdmtblPolizza.Edit;
  SavSiglaMassimaleChangeEvt           := fdmtblPolizzaSiglaMassimale.OnChange;
  fdmtblPolizzaSiglaMassimale.OnChange := nil;
  try
    fdmtblPolizzaCodCliente.asInteger   := ACodCliente;
    fdmtblPolizzaRifCodCli.asInteger    := ACodCliente;
    fdmtblPolizzaContraente.AsString    := QueryCliente.FieldByName('DENOMINAZ').AsString;
    fdmtblPolizzaCodiceFiscale.AsString := QueryCliente.FieldByName('COD_FISC').AsString;

    // mb 12/05/2020
    if QueryCliente.FieldByName('P_IVA').AsString <> '' then
      fdmtblPolizzaPartitaIva.AsString := QueryCliente.FieldByName('P_IVA').AsString
    else
      fdmtblPolizzaPartitaIva.Clear;

    fdmtblPolizzaLocNascita.AsString         := QueryCliente.FieldByName('NATO_A').AsString;
    fdmtblPolizzaDataNascita.AsString        := QueryCliente.FieldByName('NATO_IL').AsString;
    fdmtblPolizzaIndirizzo.AsString          := QueryCliente.FieldByName('INDIRIZZO').AsString;
    fdmtblPolizzaCap.AsString                := QueryCliente.FieldByName('CAP').AsString;
    fdmtblPolizzaCitta.AsString              := QueryCliente.FieldByName('CITTA').AsString;
    fdmtblPolizzaProvincia.AsString          := QueryCliente.FieldByName('PROVINCIA').AsString;
    fdmtblPolizzaProfessione.asInteger       := QueryCliente.FieldByName('PROFESSION').asInteger;
    fdmtblPolizzaprofessioneDescriz.AsString := UniMainModule.DMdatiAgeClienti.getDescrizProfessione
      (fdmtblPolizzaProfessione.asInteger);
    fdmtblPolizzaDescrizioneAttivita.AsString := QueryCliente.FieldByName('DESCRIZIONE_ATTIVITA').AsString;

    fdmtblPolizzafatta_da.asInteger    := QueryCliente.FieldByName('PROMOTER').asInteger;;
    fdmtblPolizzaSubPromoter.asInteger := QueryCliente.FieldByName('SUB_PROMOTER').asInteger;
    if QueryCliente.FieldByName('RIF_REFERENTE').asInteger > 0 then
    begin
      UniMainModule.DMdatiAgeClienti.posizionaReferente(QueryCliente.FieldByName('RIF_REFERENTE').asInteger);
      fdmtblPolizzaRifReferente.asInteger := QueryCliente.FieldByName('RIF_REFERENTE').asInteger;
      UniMainModule.DMdatiAgeClienti.ApriTelefoniDocumenti(QueryCliente.FieldByName('RIF_REFERENTE').asInteger);
    end
    else
      UniMainModule.DMdatiAgeClienti.ApriTelefoniDocumenti(ACodCliente);

    fdmtblPolizzaTelefono.AsString := UniMainModule.DMdatiAgeClienti.QTelefoni.FieldByName('NUMERO').AsString;
    fdmtblPolizzaEMail.AsString    := UniMainModule.DMdatiAgeClienti.QMail.FieldByName('NUMERO').AsString;

    fdmtblPolizza.Post;

    // {$MESSAGE WARN 'Sarebbe meglio gestire Tutto da qu�'}
    // if QueryCliente.FieldByName('P_IVA').IsNull then
    //
    // UniMainModule.ContraenteIsSocieta := false
    // else
    // begin
    // UniMainModule.ContraenteIsSocieta := TryStrToInt(QueryCliente.FieldByName('P_IVA').AsString, lCod) = true;
    // end;

  finally
    fdmtblPolizzaSiglaMassimale.OnChange := SavSiglaMassimaleChangeEvt;
  end;

end;

procedure TDMDatiTempPolizza.AggiungiPolizza;
begin
  fdmtblPolizza.DisableControls;
  fdmtblAssicurato.DisableControls;
  fdmtblGaranzia.DisableControls;
  try
    OpenDatasets;
    fdmtblGaranzia.EmptyDataSet;
    fdmtblGaranzia.Append;
    // fdmtblGaranzia.Post;
    fdmtblAssicurato.EmptyDataSet;
    fdmtblAssicurato.Append;
    // fdmtblAssicurato.Post;
    fdmtblPolizza.EmptyDataSet;
    fdmtblPolizza.Append;
    UniMainModule.NumPolizza := '';
  finally
    fdmtblPolizza.EnableControls;
    fdmtblAssicurato.EnableControls;
    fdmtblGaranzia.EnableControls;
  end;
  // fdmtblPolizza.Post;
end;

procedure TDMDatiTempPolizza.AnteprimaPolizza;
var
  PrintHouseCodReport: Integer;
  ReportFileName: string;
  bCanPrint: boolean;

  procedure AnteprimaDocNonMemorizzato;
  var
    RandNum: Integer;
  begin
    // if True then //UniMainModule.DMdatiAge.VerAutGenAgenzia('PPO_STA') or UniMainModule.DMdatiAge.VerAutGenAgenzia('PPO_ANT') then
    if UniMainModule.VerificaAutorizzazione('PPO', 'ANT') then
    begin
      preparaReport;

      RandNum                  := Random(100000);
      UniMainModule.NumPolizza := RandNum.ToString;
      // if copy(tipor, 1, 3) <> 'SET' then
      try
        bCanPrint := false;
        // non possono stampare un antepriam !!!! UniMainModule.VerificaAutorizzazione('PPO', 'STA');
        frxPolizzaGen.PrepareReport(true);

        ReportFileName := 'T' + UniMainModule.NumPolizza + '-' + FormatDateTime('yyyymmddhhmmss', now) + '.PDF';
        // esporta nel file pdf da visualizzare

        frxpdfxprt.FileName := TPath.combine(TPath.combine(UniServerModule.FilesFolderPath, 'PDF'), ReportFileName);
        frxpdfxprt.ExportNotPrintable := not bCanPrint;
        frxPolizzaGen.Export(frxpdfxprt);

        {
          gtPDFEngine1.FileName := TPath.combine(TPath.combine(UniServerModule.FilesFolderPath, 'PDF'), ReportFileName);
          gtFRExportInterface1.Engine := gtPDFEngine1;
          //Exporting a FastReport
          gtFRExportInterface1.RenderDocument(frxPolizzaGen, true, FALSE);
        }

        TFPDFViewer.ShowReport(uniApplication, ReportFileName, UniServerModule.FilesFolderURL + 'PDF/',
          TPath.combine(UniServerModule.FilesFolderPath, 'PDF'), bCanPrint, bCanPrint);
      finally
        UniMainModule.NumPolizza := '';
      end;
    end;
  end;

  procedure AnteprimaDocMemorizzato;
  begin
    PrintHouseCodReport := SLP_StampaPolizza;
    if (PrintHouseCodReport <> 0) then
    begin
      ReportFileName := UniMainModule.DMDatiReport.SalvaTempDocument(TPath.combine(UniServerModule.FilesFolderPath,
        'PDF'), PrintHouseCodReport, false, tdPrintHouse);
      if true then
      // UniMainModule.DMdatiAge.VerAutGenAgenzia('PPO_STA') or UniMainModule.DMdatiAge.VerAutGenAgenzia('PPO_ANT') then
      begin
        bCanPrint := UniMainModule.VerificaAutorizzazione('PPO', 'STA');
        TFPDFViewer.ShowReport(uniApplication, ReportFileName, UniServerModule.FilesFolderURL + 'PDF/',
          TPath.combine(UniServerModule.FilesFolderPath, 'PDF'), bCanPrint, bCanPrint);
      end;
    end;

  end;

begin
  // if fdmtblAssicurato.IsEmpty then
  MemorizzaTempAssicurati(fdmtblPolizzaCodPolizza.asInteger);

  // if fdmtblGaranzia.IsEmpty then
  MemorizzaTempGaranzie(UniMainModule.TipoPolizza);

{$IFDEF TEMPTABLES}
  // fdmtblPolizza.SaveToFile(TPath.combine(SLPDatiAge.Dati_gen.dir_temp, 'TempPolizza.txt'), sfXML);
  TFmVediTempTables.ShowGaranzie(uniApplication, UniMainModule.DMDatiTempPolizza.fdmtblPolizza,
    UniMainModule.DMDatiTempPolizza.fdmtblAssicurato, UniMainModule.DMDatiTempPolizza.fdmtblGaranzia);
  exit;
{$ENDIF}
  if UniMainModule.NumPolizza = '' then
    AnteprimaDocNonMemorizzato
  else
    AnteprimaDocMemorizzato;
  // MB 26102020
  OpenDatasets;
end;

procedure TDMDatiTempPolizza.OpenDatasets;
begin
  fdmtblPolizza.Open;
  fdmtblGaranzia.Open;
  fdmtblAssicurato.Open;
end;

function TDMDatiTempPolizza.per_allegati: string;
begin
  Result := '';
end;

procedure TDMDatiTempPolizza.preparaReport;
begin

  stringaTest    := QuotedStr(' ');
  StringaChecked := QuotedStr('X');
  AzzeraCampiReport;

  SLPDatiAge                          := UniMainModule.DMdatiAge.SLPdati_age;
  frxPolizzaGen.EngineOptions.TempDir := SLPDatiAge.Dati_gen.dir_temp;
  frxPolizzaGen.Variables['versione'] := QuotedStr(leggiVersione);

  frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');

  if isPolizza then
  begin
    frxPolizzaGen.Variables['polizza']  := QuotedStr(UniMainModule.NumPolizza);
    frxPolizzaGen.Variables['barcode1'] := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 1));
    frxPolizzaGen.Variables['barcode2'] := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 2));
  end
  else
    if isPreventivo then
    begin
      frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
      frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
    end;

end;

function TDMDatiTempPolizza.getSiglaMassimale: string;
begin
  Result := getSiglaMassimaleFromStringa(fdmtblPolizzaSiglaMassimale.AsString);
end;

function TDMDatiTempPolizza.getSiglaMassimaleFromStringa(StringaMassimale: string): string;
var
  ArrStr: TArray<string>;
begin
  Result := '';
  ArrStr := TRegEx.Split(StringaMassimale, '\W+');
  if Length(ArrStr) > 0 then
    Result := ArrStr[0];
end;

function TDMDatiTempPolizza.ReadTipoAssicurato: string;
begin
  Result := '';
end;

function TDMDatiTempPolizza.RiprendiDocumento(CodPolizza: Integer): TTipoSalvataggio;
begin
  Result := UniMainModule.DMDatiBasePolizza.RiprendiDocumento(fdmtblPolizza, fdmtblAssicurato, fdmtblGaranzia,
    CodPolizza);
  calcolaPremio;
end;

procedure TDMDatiTempPolizza.SendValidationMessage(ACodMsg: Integer; const AMessage: string);
begin
  if Assigned(FOnSendValidationMessage) then
    FOnSendValidationMessage(ACodMsg, AMessage);
end;

procedure TDMDatiTempPolizza.DataModuleCreate(Sender: TObject);
begin
  listaScadenze := TstringList.Create;
  // lstDataScad   := TList<TDate>.Create;
  Opremio    := ToPremio.Create;
  OpremioBis := ToPremio.Create;

  OTipo_pol := Ttipo_pol.Create(UniMainModule.TipoPolizza, UniMainModule.DMdatiAge.QTipo_pol);

  fdmtblPolizza.CreateDataSet;
  fdmtblGaranzia.CreateDataSet;
  fdmtblAssicurato.CreateDataSet;
  Dati_gen := UniMainModule.DMdatiAge.Dati_gen;
  UniMainModule.DMdatiAge.LoadModelloReport(frxPolizzaGen);
  Randomize;

end;

procedure TDMDatiTempPolizza.DataModuleDestroy(Sender: TObject);
begin
  listaScadenze.free;
  // lstDataScad.free;
  Opremio.free;
  OpremioBis.free;
  OTipo_pol.free;
  FOnSendValidationMessage := nil;

end;

procedure TDMDatiTempPolizza.fdmtblPolizzaAfterPost(DataSet: TDataSet);
begin
  fdmtblPolizzaSiglaMassimale.OnChange := SavSiglaMassimaleChangeEvt;
  SavSiglaMassimaleChangeEvt           := nil;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaAfterScroll(DataSet: TDataSet);
begin
  fdmtblPolizza.Edit;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaBeforePost(DataSet: TDataSet);
begin
  SavSiglaMassimaleChangeEvt           := fdmtblPolizzaSiglaMassimale.OnChange;
  fdmtblPolizzaSiglaMassimale.OnChange := nil;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaDataNascitaChange(Sender: TField);
begin
  if (yearOf(fdmtblPolizzaDataNascita.AsDateTime) > 1900) and
    ((yearOf(Date) - yearOf(fdmtblPolizzaDataNascita.AsDateTime) > 75)) and (fdmtblPolizzaDurataAnni.asInteger > 1) then
    // ShowMessage('Durata troppo lunga per questo Contraente.');
    raise EPolDurataAnniTooLong.Create(CERR_DURATA_ANNI_TOO_LONG, MSG_DURATA_ANNI_TOO_LONG);

  if yearOf(Date) - yearOf(fdmtblPolizzaDataNascita.AsDateTime) < 16 then
    raise EPolContraenteTooYoung.Create(CERR_CONTRAENTE_TOO_YOUNG, MSG_CONTRAENTE_TOO_YOUNG);
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaDecorrenzaChange(Sender: TField);
var
  dd: Integer;
  dayDecoRetro: string;
const
  DAY_DECO_RETRO: string   = 'DAY_DECO_RETRO';
  DAY_PSOST_RETRO: string  = 'DAY_PSOST_RETRO';
  DAY_DECO_TOO_FWD: string = 'DAY_DECO_TOO_FWD';
begin
  if not UniMainModule.DMdatiAge.isDirezione then
  begin
    dd           := round(Date - fdmtblPolizzaDecorrenza.AsDateTime);
    dayDecoRetro := DAY_DECO_RETRO;
    if dd >= UniMainModule.DMdatiAge.get_codiceDef(DAY_DECO_RETRO, 18) then
    // era 32 corretto 01/07/2016 in quanto in perfeziona polizze questi sono i limiti
    begin
      // avvisa che la polizza parte con data di effetto troppo vecchia  !!!
      if (Length(trim(fdmtblPolizzaNumPolizzaSostituita.AsString)) < 5) then
        // 'Attenzione! Non � possibile emettere polizze con effetto cos� vecchio !'
        raise EPolDataEffettoError.Create(CERR_DATA_EFFETTO_TOO_OLD, MSG_DATA_EFFETTO_TOO_OLD);
      if dd >= UniMainModule.DMdatiAge.get_codiceDef(DAY_PSOST_RETRO, 45) then
        // 'Attenzione! Non � possibile emettere polizze SOSTITUITE con effetto cos� vecchio !');
        raise EPolDataEffettoError.Create(CERR_POL_SOST_DATA_EFFETTO_TOO_OLD, MSG_POL_SOST_DATA_EFFETTO_TOO_OLD);
    end
    else
      if (dd <= UniMainModule.DMdatiAge.get_codiceDef(DAY_PSOST_RETRO, -45)) then
        // and not UniMainModule.DMdatiAge.isAssitorino or (dd <= -61) then
        // 'Attenzione! Non � possibile emettere polizze con effetto cos� spostato in avanti !');
        raise EPolDataEffettoError.Create(CERR_DATA_EFFETTO_TOO_FORWARD, MSG_DATA_EFFETTO_TOO_FORWARD);
  end;

end;

procedure TDMDatiTempPolizza.fdmtblPolizzaFrazionamentoChange(Sender: TField);
begin
  // checkPremioUnico;
  calcolaDate;
  calcolaPremio;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaScadenzaChange(Sender: TField);
var
  diff_mesi: Integer;
  DataScadPolSost, DataScadenza, DataEffetto: TDate;

  procedure reimpostaScadenza(NewScadenza: TDate);
  begin
    fdmtblPolizzaScadenza.OnChange := nil;
    try
      fdmtblPolizzaScadenza.AsDateTime := NewScadenza;

    finally
      fdmtblPolizzaScadenza.OnChange := fdmtblPolizzaScadenzaChange;
    end;
  end;

begin
  DataScadPolSost := fdmtblPolizzaScadenzaPolizzaSostituita.AsDateTime;
  DataScadenza    := fdmtblPolizzaScadenza.AsDateTime;
  DataEffetto     := fdmtblPolizzaDecorrenza.AsDateTime;

  // se la data di scadenza non � > data decorrenza avvisa che non si pu� fare ...
  // if (Year(Escad_pol_sost.Date)>1980) and ( DaysBetween(Escadenza.Date,Escad_pol_sost.Date)>300) then
  if (yearOf(DataScadPolSost) > 1980) and
    ((DataScadPolSost > DataScadenza) and (DaysBetween(DataScadPolSost, DataScadenza) > 300)) then
    // 'Non � possibile anticipare la scadenza della polizza sostituita!'
    raise EPolDataScadPolSostError.Create(CERR_DATA_SCAD_ANTICIP_POLSOST, MSG_DATA_SCAD_ANTICIP_POLSOST);


  if DataEffetto >= DataScadenza then
  begin
    // La data di decorrenza deve essere precedente a quella di scadenza !'
    reimpostaScadenza(IncYear(fdmtblPolizzaDecorrenza.AsDateTime, 1));
    calcolaDate;
    calcolaPremio;
    raise EPolDataScadPolSostError.Create(CERR_DATA_EFFETTO_GT_DATA_SCADENZA, MSG_DATA_EFFETTO_GT_DATA_SCADENZA);
  end;

  if (DaysBetween(DataEffetto, DataScadenza) < 364) and (not(Length( fdmtblPolizzaNumPolizzaSostituita.AsString) >= 5))
  then begin
    // La durata minima di una polizza � di un anno !
    reimpostaScadenza(IncYear(fdmtblPolizzaDecorrenza.AsDateTime, 1));
    calcolaDate;
    calcolaPremio;
    raise EPolDataScadPolSostError.Create(CERR_DURATA_MINIMA_POL, MSG_DURATA_MINIMA_POL);
  end;

  if DaysBetween(DataEffetto, DataScadenza) < 728 then
    fdmtblPolizzaProvvig.AsString := 'R';

  diff_mesi := diffMesi30(DataEffetto, DataScadenza);

  // mb 01112020  blocca le polizze poliennali per ora ....
  // if ((diff_mesi >= 48) and (diff_mesi <= 59)) then
  // if  (diff_mesi > 23) then
  // if not UniMainModule.DMdatiAge.find_durata(trunc(YearSpan(DataEffetto, DataScadenza)),fdmtblPolizzaProvvig.AsString ) then
  if not UniMainModule.DMdatiAge.find_durata(trunc(diff_mesi/12),fdmtblPolizzaProvvig.AsString ) then
  begin
    // ('Durata non prevista! La polizza pu� essere o di TRE o di CINQUE anni. Durata corretta in automatico a TRE anni');
    // reimpostaScadenza(IncYear(fdmtblPolizzaScadenza.AsDateTime, -1));
    SendValidationMessage(CERR_DURATA_POL_GT_MAX, MSG_DURATA_POL_GT_MAX);
    reimpostaScadenza(IncYear(fdmtblPolizzaDecorrenza.AsDateTime, 1));
    calcolaDate;
    calcolaPremio;
  end
  else
  begin
    // modifica che prende per buone anche le polizze di 1 anno e x mesi ...
    if UniMainModule.DMdatiAge.durataMax >= trunc(YearSpan(DataEffetto, DataScadenza)) then
    begin
      calcolaDate;
      calcolaPremio;
    end
    else
    begin
      // MessageDlg('La durata della polizza � superiore alla durata massima utilizzabile da mandato', mtWarning,
      // [mbOK], 0);
      SendValidationMessage(CERR_DURATA_POL_GT_MAX, MSG_DURATA_POL_GT_MAX);
      reimpostaScadenza(IncYear(fdmtblPolizzaDecorrenza.AsDateTime, 1));
      calcolaDate;
      calcolaPremio;
    end;
  end;
  // if (DaysBetween(Eeffetto.date,Escadenza.date)<60) then
{$IFNDEF direzione}
  if (fdmtblPolizzaDurataGiorni.asInteger > 0) and (fdmtblPolizzaDurataGiorni.asInteger < 60) then
  begin
    // polizza con ratino corto !!!!
    // showmessage('Attenzione, la polizza ha un ratino inferiore a 60 giorni. Viene selezionata ' +
    // 'automaticamente l''opzione che incorpora l''incasso del ratino e della prima rata. ' +
    // 'Nel caso in cui si volesse procedere all''incasso del solo ratino ricordarsi di avvisare ' +
    // 'l''ufficio portafoglio della SLP affinch� venga emessa e spedita in agenzia la relativa quietanza.');
    SendValidationMessage(CERR_RATINO_POL_LT_60GG, MSG_RATINO_POL_LT_60GG);

    fdmtblPolizzaRatino1Rata.AsBoolean := true;
  end;
{$ENDIF}
end;

function TDMDatiTempPolizza.genera_cbPOL(polizza: string; pagina: Integer): string;
var
  NPolizza: Integer;
begin
  // genera il codice a barre per l a polizza
  // 35214
  // '10'  identificatore fisso per le polizze
  // 02 che indica il numero di pagine del documento
  // 01 02  numero della pagina corrente
  // nnnnnnn 7 cifre paddatate a sinistra con 0 per il numero di polizza
  Result   := '35214' + '10';
  Result   := Result + '02' + Format('%2.2d', [pagina]);
  NPolizza := StrToIntDef(polizza, 1);
  Result   := Result + Format('%7.7d', [NPolizza]);
end;

procedure TDMDatiTempPolizza.MemorizzaDocumento(TipoSalvataggioDocumento: TTipoSalvataggio;
  MostraReportPolizza: boolean; vediPH: TproceduraVediPH);
var
  ReportFileName: string;
  PrintHouseCodReport: Integer;
  // PDFFolderPath: string;

  procedure aggiornaStatoDocumento;
  begin
    if not(fdmtblPolizza.State in [dsInsert, dsEdit]) then
      fdmtblPolizza.Edit;

    if not(fdmtblAssicurato.State in [dsInsert, dsEdit]) then
      fdmtblAssicurato.Edit;

    fdmtblPolizzaStato.AsString := getStatus(TipoSalvataggioDocumento);
    fdmtblPolizza.Post;

  end;

  procedure ClearNumeroPolizza;
  begin
    if not(fdmtblPolizza.State in [dsInsert, dsEdit]) then
      fdmtblPolizza.Edit;
    fdmtblPolizzaNPolizza.AsString := '';
    fdmtblPolizza.Post;

  end;

begin
  // modifiche MB 28/03/2020
  DoControlli(TipoSalvataggioDocumento in [tsBozza, tsNuovaBozza]);
  // DoControlli(TipoSalvataggioDocumento in [tsPreventivo, tsNuovoPreventivo]);
  MemorizzaTempAssicurati(fdmtblPolizzaCodPolizza.asInteger);
  MemorizzaTempGaranzie(UniMainModule.TipoPolizza);
  aggiornaStatoDocumento;
  // Log('dmDatiTempPolizza.MemorizzaDocumento prima di salvaDatidocumento (1) '+DateTimeToStr(now));

  UniMainModule.DMDatiBasePolizza.SalvaDatiDocumento(fdmtblPolizza, fdmtblAssicurato, fdmtblGaranzia,
    TipoSalvataggioDocumento);

  // Log('dmDatiTempPolizza.MemorizzaDocumento dopo di salvaDatidocumento (4) '+DateTimeToStr(now));

  case TipoSalvataggioDocumento of
    tsPreventivo, tsNuovoPreventivo, tsPolizza:
      begin
        PrintHouseCodReport := SLP_StampaPolizza(TipoSalvataggioDocumento in [tsPreventivo, tsNuovoPreventivo]);

        // Log('dmDatiTempPolizza.MemorizzaDocumento dopo di SLP_StampaPolizza (5) '+DateTimeToStr(now));
        if TipoSalvataggioDocumento=tsPolizza then
           UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' MEMPOL-' + UniMainModule.SiglaPolizza + ' Np'+
            UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPolizza.AsString
            + ' ' + FormatDateTime('dd-mm-yyy', UniMainModule.DMDatiTempPolizza.fdmtblPolizzaDecorrenza.AsDateTime),
            'CK', '', '', '', 'MPO', UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPolizza.AsString,
            UniMainModule.DMDatiTempPolizza.fdmtblPolizzaDecorrenza.AsDateTime);

        if (PrintHouseCodReport > 0) then
          if MostraReportPolizza then
          begin

             ReportFileName := UniMainModule.DMDatiReport.SalvaTempDocument(TPath.combine(UniServerModule.FilesFolderPath,
               'PDF'), PrintHouseCodReport, false, tdPrintHouse);
             // if UniMainModule.DMdatiAge.VerAutGenAgenzia('PPO_STA') or UniMainModule.DMdatiAge.VerAutGenAgenzia('PPO_ANT')
             if true then
             begin

               MostraReportPolizza := UniMainModule.VerificaAutorizzazione('PPO', 'STA');

               // mb 06122020
               if (TipoSalvataggioDocumento = tsPolizza) and MostraReportPolizza then
                  vediPH(UniMainModule.DMDatiTempPolizza.fdmtblPolizzaNPolizza.AsString)
               else
                  TFPDFViewer.ShowReport(uniApplication, ReportFileName, UniServerModule.FilesFolderURL + 'PDF/',
                  TPath.combine(UniServerModule.FilesFolderPath, 'PDF'), MostraReportPolizza, MostraReportPolizza);
             end;

             ClearNumeroPolizza;
          end;
      end;

  end;
end;

procedure TDMDatiTempPolizza.MemorizzaTempAssicurati(CodPolizza: Integer);
begin
  if fdmtblAssicurato.State in [dsInsert, dsEdit] then
    fdmtblAssicurato.Post;

  if not fdmtblAssicurato.IsEmpty then
  begin
    fdmtblAssicurato.First;
    fdmtblAssicurato.Next;
    while fdmtblAssicurato.RecNo > 1 do
      fdmtblAssicurato.Delete;
  end;

end;

procedure TDMDatiTempPolizza.MemorizzaTempGaranzia(CodPolizza: Integer; const Sigla: string; PremioCalcolato: currency);
begin
  if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, Sigla) then
  begin
    fdmtblGaranzia.Append;
    fdmtblGaranziaSiglaGaranzia.AsString    := Sigla;
    fdmtblGaranziaRifCodMassimale.asInteger := UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('COD_GAR_BASE')
      .asInteger;
    fdmtblGaranziaMassimale.AsCurrency := UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('MASSIMALE').AsCurrency;
    fdmtblGaranziaValoreAssicurato.AsCurrency := UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName
      ('VALORE_ASSICURATO').AsCurrency;
    fdmtblGaranziaPremio.AsCurrency := IfThen(PremioCalcolato = 0,
      UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('PREMIO').AsCurrency, PremioCalcolato);
    fdmtblGaranziaDataIn.AsDateTime       := Date;
    fdmtblGaranziaRifCodPolizza.asInteger := CodPolizza;
    fdmtblGaranziaRifGarBase.asInteger    := UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('COD_GAR_BASE')
      .asInteger;
    fdmtblGaranziaFranchigia.AsString    := 'N';
    fdmtblGaranziaPercentuale.AsCurrency := 0;
    fdmtblGaranzia.Post;
  end;

end;

procedure TDMDatiTempPolizza.MemorizzaTempGaranzie(TipoPolizza: Integer);
begin
  CodPolizza := fdmtblPolizzaCodPolizza.asInteger;
  fdmtblGaranzia.EmptyDataSet;
  MemorizzaTempGaranzia(CodPolizza, SiglaMassimale);
end;

procedure TDMDatiTempPolizza.fdmtblAssicuratoNewRecord(DataSet: TDataSet);
begin
  fdmtblAssicuratoDenominazione.AsString := '';
  fdmtblAssicuratoIndirizzo.AsString     := '';
  fdmtblAssicuratoCitta.AsString         := '';
  fdmtblAssicuratocap.AsString           := '';
  fdmtblAssicuratoProvincia.AsString     := '';
  fdmtblAssicuratoNote.AsString          := '';
  fdmtblAssicuratoEntrata.AsDateTime     := Data;
  fdmtblAssicuratoUscita.Clear;
  fdmtblAssicuratoCodFiscIvas.AsString      := '';
  fdmtblAssicuratoCausaleUscita.AsString    := '';
  fdmtblAssicuratoSostituitoDa.asInteger    := 0;
  fdmtblAssicuratoPatente.AsString          := '';
  fdmtblAssicuratoCategoriaPatente.AsString := '';
  fdmtblAssicuratoRilasciataDa.AsString     := '';
  fdmtblAssicuratoDataScadenza.Clear;
  fdmtblAssicuratoTipoVeicolo.AsString           := '';
  fdmtblAssicuratoTarga.AsString                 := '';
  fdmtblAssicuratoMarca.AsString                 := '';
  fdmtblAssicuratoHpQl.AsString                  := '';
  fdmtblAssicuratoRifCodTipoAssicurato.asInteger := 0;
  fdmtblAssicuratoTipo.AsString                  := '';
  fdmtblAssicuratoCodPolizza.asInteger           := 0;
  fdmtblAssicuratoDataScadRevisione.Clear;
  fdmtblAssicuratoModello.AsString := '';
  fdmtblAssicuratoClasse.asInteger := 0;
  fdmtblAssicuratoTelaio.AsString  := '';
  fdmtblAssicuratoCc.AsString      := '';
  fdmtblAssicuratoDataImmatricolazione.Clear;
  fdmtblAssicuratoPremio.AsCurrency      := 0;
  fdmtblAssicuratoMassimale.AsCurrency   := 0;
  fdmtblAssicuratoPremioSLP.AsCurrency   := 0;
  fdmtblAssicuratoOggAggiuntivo.AsString := '';
  fdmtblAssicuratoIdGenSlpAge.asInteger  := 0;
end;

procedure TDMDatiTempPolizza.fdmtblGaranziaNewRecord(DataSet: TDataSet);
begin
  fdmtblGaranziaMassimale.AsCurrency        := 0;
  fdmtblGaranziaPremio.AsCurrency           := 0;
  fdmtblGaranziaDataIn.AsDateTime           := Data;
  fdmtblGaranziaDataOut.AsDateTime          := Data;
  fdmtblGaranziaRifCodPolizza.asInteger     := 0;
  fdmtblGaranziaValoreAssicurato.AsCurrency := 0;
  fdmtblGaranziaValoreIf.AsCurrency         := 0;
  fdmtblGaranziaFranchigia.AsString         := '';
  fdmtblGaranziaRifCodMassimale.asInteger   := 0;
  fdmtblGaranziaIdGenSlpAge.asInteger       := 0;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaNewRecord(DataSet: TDataSet);
begin
  InNewRecord := true;
  Data        := Date;
  // fdmtblPolizzaScadenza.OnChange       := nil;
  // fdmtblPolizzaDecorrenza.OnChange     := nil;
  // fdmtblPolizzaDataNascita.OnChange    := nil;
  // fdmtblPolizzaSiglaMassimale.OnChange := nil;
  // fdmtblPolizzaRatino1Rata.OnChange    := nil;
  // fdmtblPolizzaPercAccessori.OnChange  := nil;
  fdmtblPolizzaForma.OnChange := nil;
  try
    fdmtblPolizzaDataInserimento.AsDateTime   := now;
    fdmtblPolizzaRifTipoPol.asInteger         := UniMainModule.TipoPolizza;
    fdmtblPolizzaUtente.asInteger             := UniMainModule.utentecodice;
    fdmtblPolizzaDecorrenza.AsDateTime        := Data;
    fdmtblPolizzaScadenza.AsDateTime          := IncYear(Data);
    fdmtblPolizzaScadenzaPrimaRata.AsDateTime := fdmtblPolizzaScadenza.AsDateTime;
    // fdmtblPolizzaRataAllaFirma.AsDateTime     := fdmtblPolizzaScadenza.AsDateTime;
    fdmtblPolizzaFrazionamento.AsString := 'A';
    fdmtblPolizzaNPolizza.AsString      := '';
    fdmtblPolizzaLordo.AsCurrency       := 0;
    fdmtblPolizzaNetto.AsCurrency       := 0;
    fdmtblPolizzaPTasse.AsCurrency      := OTipo_pol.PERC_TASSE;
    fdmtblPolizzaAccessori.AsCurrency   := 0;
    fdmtblPolizzaValuta.AsString        := 'E';
    fdmtblPolizzaTasse.AsCurrency       := 0;
    fdmtblPolizzaNote.AsString          := '';
    fdmtblPolizzaRifCompagnia.asInteger := 1;
    fdmtblPolizzaRifCodCli.asInteger    := 0;
    fdmtblPolizzaCollaboratore.Clear;
    fdmtblPolizzaRifCodRamo.asInteger               := 1; // per ora fisso per TUtela LEgale
    fdmtblPolizzaStato.AsString                     := '';
    fdmtblPolizzaProvvigioni.AsCurrency             := 0;
    fdmtblPolizzaClasse.asInteger                   := 0;
    fdmtblPolizzaAgenzia.AsString                   := UniMainModule.DMdatiAge.SLPdati_age.agenzia;
    fdmtblPolizzaSubAgenzia.AsString                := '';
    fdmtblPolizzaRatino1Rata.AsBoolean              := false;
    fdmtblPolizzaScontoDurata.AsBoolean             := false;
    fdmtblPolizzaUtente.asInteger                   := 0;
    fdmtblPolizzaDataInserimento.AsDateTime         := Data;
    fdmtblPolizzaNetto1.AsCurrency                  := 0;
    fdmtblPolizzaAccessori1.AsCurrency              := 0;
    fdmtblPolizzaInteressiFrazionamento1.AsCurrency := 0;
    fdmtblPolizzaImponibile1.AsCurrency             := 0;
    fdmtblPolizzaImposte1.AsCurrency                := 0;
    fdmtblPolizzaTotale1.AsCurrency                 := 0;
    fdmtblPolizzaNetto2.AsCurrency                  := 0;
    fdmtblPolizzaAccessori2.AsCurrency              := 0;
    fdmtblPolizzaRimborsoSost.AsCurrency            := 0;
    fdmtblPolizzaImponibile2.AsCurrency             := 0;
    fdmtblPolizzaImposte2.AsCurrency                := 0;
    fdmtblPolizzaTotale2.AsCurrency                 := 0;

    fdmtblPolizzaNetto1A.AsCurrency                  := 0;
    fdmtblPolizzaAccessori1A.AsCurrency              := 0;
    fdmtblPolizzaInteressiFrazionamento1A.AsCurrency := 0;
    fdmtblPolizzaImponibile1A.AsCurrency             := 0;
    fdmtblPolizzaImposte1A.AsCurrency                := 0;
    fdmtblPolizzaTotale1A.AsCurrency                 := 0;
    fdmtblPolizzaNetto2A.AsCurrency                  := 0;
    fdmtblPolizzaAccessori2A.AsCurrency              := 0;
    fdmtblPolizzaRimborsoSostA.AsCurrency            := 0;
    fdmtblPolizzaImponibile2A.AsCurrency             := 0;
    fdmtblPolizzaImposte2A.AsCurrency                := 0;
    fdmtblPolizzaTotale2A.AsCurrency                 := 0;

    fdmtblPolizzaCombinazione.AsString         := '';
    fdmtblPolizzaEstensioneOM.AsString         := '';
    fdmtblPolizzaNumPolizzaSostituita.AsString := '';
    fdmtblPolizzaScadenzaPolizzaSostituita.Clear;
    // fdmtblPolizzaRataAllaFirma.Clear;
    fdmtblPolizzaPolizzaStatus.AsString := '';
    fdmtblPolizzaPolizzaDataPerfezionamento.Clear;
    fdmtblPolizzaDataCancellazione.Clear;
    fdmtblPolizzaVolumeAffari.AsCurrency  := 0;
    fdmtblPolizzaPercAccessori.AsCurrency := 0;
    fdmtblPolizzaRagioneSociale.AsString  := '';
    fdmtblPolizzaDati1.AsString           := '';
    fdmtblPolizzaDati2.AsString           := '';
    fdmtblPolizzaDati3.AsString           := '';
    fdmtblPolizzaDati4.AsString           := '';
    fdmtblPolizzaEstensioni.AsString      := '';
    fdmtblPolizzaSiglaMassimale.AsString  := '';
    fdmtblPolizzaTipologia.AsString := 'A'; // fisso in quanto tutte le polizze emesse dall'agenzia devono avere A fisso
    fdmtblPolizzaDatiVari.AsString := '';
    fdmtblPolizzaOrdine.AsString   := '';
    fdmtblPolizzaSpecial.AsString  := '';

    fdmtblPolizzaDataImportazione.AsDateTime  := Data;
    fdmtblPolizzaCodiceImportazione.asInteger := 0;
    fdmtblPolizzaIntermediataDa.asInteger     := 0;
    fdmtblPolizzaConvenzione.asInteger        := 0;
    fdmtblPolizzaRifReferente.asInteger       := 0;
    fdmtblPolizzaVarFrp.AsString              := '';
    fdmtblPolizzaAppReport.AsString           := '';
    fdmtblPolizzaConAppendice.AsString        := '';
    fdmtblPolizzaAppNumero.asInteger          := 0;
    fdmtblPolizzaAppDal.Clear;
    fdmtblPolizzaAppAL.Clear;
    fdmtblPolizzaAppOggetto.AsString    := '';
    fdmtblPolizzaPSconto.AsCurrency     := 0;
    fdmtblPolizzaInpDf.AsString         := '';
    fdmtblPolizzaSubPromoter.asInteger  := 0;
    fdmtblPolizzaDati5.AsString         := '';
    fdmtblPolizzaDati5.AsString         := '';
    fdmtblPolizzaNSP.AsString           := '';
    fdmtblPolizzaTipoEmissione.AsString := '';
    fdmtblPolizzaTelefono.AsString      := '';
    fdmtblPolizzaEMail.AsString         := '';
    fdmtblPolizzaDtLastMod.Clear;
    fdmtblPolizzaAgenzia.AsString     := UniMainModule.DMdatiAge.SLPdati_age.agenzia;
    fdmtblPolizzaAgenziaNome.AsString := UniMainModule.DMdatiAge.SLPdati_age.nome;

    fdmtblPolizzaDurataAnni.asInteger := 1;
    fdmtblPolizzaDurataMesi.Clear;
    fdmtblPolizzaDurataGiorni.Clear;

    fdmtblPolizzaNomeCliente.AsString   := '';
    fdmtblPolizzaCodCliente.asInteger   := 0;
    fdmtblPolizzaCodiceFiscale.AsString := '';
    fdmtblPolizzaPartitaIva.Clear;
    fdmtblPolizzaLocNascita.AsString := '';
    fdmtblPolizzaDataNascita.Clear;
    fdmtblPolizzaIndirizzo.AsString           := '';
    fdmtblPolizzaCap.AsString                 := '';
    fdmtblPolizzaCitta.AsString               := '';
    fdmtblPolizzaProvvig.AsString             := 'R';
    fdmtblPolizzaProfessione.asInteger        := 0;
    fdmtblPolizzaDescrizioneAttivita.AsString := '';
    fdmtblPolizzaContraente.AsString          := '';
    fdmtblPolizzaForma.AsString               := 'A';
    fdmtblPolizzaTacitoRinnovo.AsBoolean      := false;
    fdmtblPolizzadt_preventivo.AsDateTime     := now;
  finally
    // fdmtblPolizzaScadenza.OnChange      := fdmtblPolizzaScadenzaChange;
    // fdmtblPolizzaDecorrenza.OnChange    := fdmtblPolizzaDecorrenzaChange;
    // fdmtblPolizzaDataNascita.OnChange   := fdmtblPolizzaDataNascitaChange;
    // fdmtblPolizzaRatino1Rata.OnChange   := fdmtblPolizzaRatino1RataChange;
    // fdmtblPolizzaPercAccessori.OnChange := fdmtblPolizzaPercAccessoriChange;
    fdmtblPolizzaScadenzaChange(fdmtblPolizzaScadenza);

    InNewRecord := false;

  end;

end;

procedure TDMDatiTempPolizza.fdmtblPolizzaNPercAccessoriChange(Sender: TField);
begin
  fdmtblPolizzaPercAccessori.AsCurrency := fdmtblPolizzaNPercAccessori.asInteger * 0.25;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaPercAccessoriChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaProvvigChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizza.fdmtblPolizzaRatino1RataChange(Sender: TField);
begin
  if fdmtblPolizzaRatino1Rata.AsBoolean then
  begin
    // se esiste un ratino inglobalo nella prima rata di polizza
    if fdmtblPolizzaDurataGiorni.asInteger > 0 then
    begin

      if listaScadenze.count >= 5 then
        fdmtblPolizzaScadenzaPrimaRata.AsDateTime := strTodateTime(listaScadenze.Strings[listaScadenze.count - 5])
      else
        fdmtblPolizzaScadenzaPrimaRata.AsDateTime := strTodateTime(listaScadenze.Strings[listaScadenze.count - 4]);
    end;
  end;
  calcolaDate;
  calcolaPremio;

end;

procedure TDMDatiTempPolizza.DoControlli(BaseOnly: boolean);
begin
  // esegue ic ontrolli uguali per tutte le polizze:
  // 1- il numero di polizza deve essere indicato
  // 2- la decorrenza, la scadenza e il frazionamento devono essere indicati
  // 3- la polizza non pu� durare meno di un anno !!!
  // ListErrori.Clear;

  // devono essere differenziati a seconda che sia una bozza o meno !!!!!
  if not BaseOnly then
  begin

    if not(fdmtblPolizzaContraente.AsString > '') then
      raise EPolizzaError.Create(CERR_POL_CONTRAENTE_MISSED, MSG_POL_CONTRAENTE_MISSED);

    if not(yearOf(fdmtblPolizzaDecorrenza.AsDateTime) > 2017) then
      raise EPolizzaError.Create(CERR_DATA_EFFETTO_TOO_LOW, MSG_DATA_EFFETTO_TOO_LOW);

    if not(yearOf(fdmtblPolizzaDataEmissione.AsDateTime) > 2000) then
      raise EPolizzaError.Create(CERR_DATA_EMISSIONE_TOO_LOW, MSG_DATA_EMISSIONE_TOO_LOW);

    if not(fdmtblPolizzaImposte1.AsCurrency > 0) then
      raise EPolizzaError.Create(CERR_TASSA_ZERO, MSG_TASSA_ZERO);

    if yearOf(fdmtblPolizzaDataEmissione.AsDateTime) > 1920 then
    begin
      if fdmtblPolizzaDataEmissione.AsDateTime > fdmtblPolizzaDecorrenza.AsDateTime then
      begin
        fdmtblPolizzaDataEmissione.AsDateTime := fdmtblPolizzaDecorrenza.AsDateTime;
        raise EPolizzaError.Create(CERR_DATA_EMISS_GT_DATA_EFFETTO, MSG_DATA_EMISS_GT_DATA_EFFETTO);
      end;
      // mb02122020
      if (fdmtblPolizzaDataEmissione.AsDateTime > Date) then
      begin
        fdmtblPolizzaDataEmissione.AsDateTime := date;
        raise EPolizzaError.Create(CERR_DATA_EMISS_GT_DATA_EFFETTO, 'La data di emissione non pu� essere successiva alla data odierna.');
      end;

    end;

    // if not ((year(Escadenza.date)-year(Eeffetto.date))>=1) then errori:=errori+'La polizza deve durare almeno un anno - ';
    if ((date360.dist360(fdmtblPolizzaDecorrenza.AsDateTime, fdmtblPolizzaScadenza.AsDateTime) < 360) and
      ((Length(fdmtblPolizzaNumPolizzaSostituita.AsString) < 5))) then
      raise EPolizzaError.Create(CERR_DURATA_MINIMA_POL, MSG_DURATA_MINIMA_POL);
    // Non necessario in quanto impostato per default as A ed assegnato tramite combo non editabile
    // if fdmtblPolizzaFrazionamento.AsString = '' then
    // raise.EPolizzaError('Deve essere indicato un frazionamento');

    if (fdmtblPolizzaTotale1.Value < 0) or (fdmtblPolizzaTotale2.Value < 0) then
      raise EPolizzaError.Create(CERR_PREMIO_LT_ZERO, MSG_PREMIO_LT_ZERO);
  end;
end;

procedure TDMDatiTempPolizza.salva_dati_varfrp(pol: string);
var
  varfrp: TMemoryStream;
begin
  // salva il contenuto delle vvariabili del fast report nel campo di slp_tpolizze('varfrp').asString (tipo memo)
  varfrp := TMemoryStream.Create;
  try
    frxPolizzaGen.Variables.SaveToStream(varfrp);
    UniMainModule.DMDatiBasePolizza.Qupd_varfrp_pol.Close;
    UniMainModule.DMDatiBasePolizza.Qupd_varfrp_pol.ParamByName('n_polizza').AsString := pol;
    UniMainModule.DMDatiBasePolizza.Qupd_varfrp_pol.ParamByName('varfrp').LoadFromStream(varfrp, ftMemo);
    UniMainModule.DMDatiBasePolizza.Qupd_varfrp_pol.ExecSQL;
  finally
    varfrp.free;
  end;

end;

function TDMDatiTempPolizza.memorizzaReport(nomefile: string; polizza, in_stampa_in_serie: boolean; npol, tipor: string;
  data_d: TdateTime; ncopie: Integer): Integer;
var
  stream: TMemoryStream;

begin
  // funzione che sovraintende alla stampa dei report legati alle polizze:
  // simpli, condizioni generali, appendici, allegati
  Result := 0;

  if frxPolizzaGen.Variables.IndexOf('peragenzia') > -1 then
  begin
    frxPolizzaGen.Variables['peragenzia'] := QuotedStr('N');
  end;

  // DMdatiAge.get_codice('SALVOPDFAGESLP',procedi);
  // UniMainModule.DMdatiAge.get_codice('SALVOPDFSLP', procedi);
  if copy(tipor, 1, 3) <> 'SET' then
    frxPolizzaGen.PrepareReport(true);
  // salva il tutto nel formato proprietario come frxPolizzaGen predigerito ....
  // ins_stampa;
  stream := TMemoryStream.Create;

  try
    frxPolizzaGen.PreviewPages.SaveToStream(stream);
    Result := UniMainModule.DMdatiAge.MemorizzaReportPolizza(stream, npol, tipor, data_d, polizza);
    if (polizza and (npol > '')) then
      salva_dati_varfrp(npol);
  finally
    stream.free;

  end;
end;

procedure TDMDatiTempPolizza.sviluppaPremio(premio_base: currency; premio_baseBis: currency);
begin
  // il primo premio � sempre con tasse al 12.50 e eventualmente il seconto ha le tasse al 21.25
  // istanzia Opremio
  // modifiche del 12/03/2020 MB
  if premio_baseBis > 0 then
  begin
    OpremioBis.ptasse               := 21.25;
    OpremioBis.nettoTotale          := premio_baseBis * OpremioBis.numero_assicurati;
    OpremioBis.nettoTotaleBase      := premio_baseBis * OpremioBis.numero_assicurati;
    OpremioBis.durata               := fdmtblPolizzaDurataAnni.asInteger;
    OpremioBis.frazionamento        := dai_frazionamento(fdmtblPolizzaFrazionamento.AsString);
    OpremioBis.paccessori           := fdmtblPolizzaPercAccessori.AsCurrency;
    OpremioBis.durataRatino         := fdmtblPolizzaDurataGiorni.asInteger;
    OpremioBis.ratino_su_prima_rata := fdmtblPolizzaRatino1Rata.AsBoolean;
    OpremioBis.premio_rimborsato    := premio_da_rimborsare;
    OpremioBis.exec;
  end
  else
  begin
    Opremio.ptasse                  := fdmtblPolizzaPTasse.AsCurrency;
    OpremioBis.nettoTotale          := 0;
    OpremioBis.nettoTotaleBase      := 0;
    OpremioBis.durata               := fdmtblPolizzaDurataAnni.asInteger;
    OpremioBis.frazionamento        := dai_frazionamento(fdmtblPolizzaFrazionamento.AsString);
    OpremioBis.paccessori           := 0;
    OpremioBis.durataRatino         := fdmtblPolizzaDurataGiorni.asInteger;
    OpremioBis.ratino_su_prima_rata := fdmtblPolizzaRatino1Rata.AsBoolean;
    OpremioBis.premio_rimborsato    := 0;
    OpremioBis.exec;

  end;

  Opremio.nettoTotale          := premio_base * Opremio.numero_assicurati;
  Opremio.nettoTotaleBase      := premio_base * Opremio.numero_assicurati;
  Opremio.durata               := fdmtblPolizzaDurataAnni.asInteger;
  Opremio.frazionamento        := dai_frazionamento(fdmtblPolizzaFrazionamento.AsString);
  Opremio.paccessori           := fdmtblPolizzaPercAccessori.AsCurrency;
  Opremio.durataRatino         := fdmtblPolizzaDurataGiorni.asInteger;
  Opremio.ratino_su_prima_rata := fdmtblPolizzaRatino1Rata.AsBoolean;
  Opremio.premio_rimborsato    := premio_da_rimborsare;
  Opremio.exec;

end;

procedure TDMDatiTempPolizza.AddImportoGaranzia(SiglaGaranzia: string; valore: currency);
begin
  // imposta il valore del premio (usato per proparare l'arrotondamento)
  fdmtblGaranzia.First;
  if fdmtblGaranzia.Locate('SIGLAGARANZIA', SiglaGaranzia, []) then
  begin
    fdmtblGaranzia.Edit;
    fdmtblGaranziaPremio.AsCurrency := fdmtblGaranziaPremio.AsCurrency + valore;
    fdmtblGaranzia.Post;
  end;

end;

function TDMDatiTempPolizza.GetContraenteIsSocieta: boolean;
begin
  Result := not isPersonaFisica(fdmtblPolizzaCodiceFiscale.AsString);
end;

function TDMDatiTempPolizza.GetImportoGaranzia(SiglaGaranzia: string): currency;
begin
  Result := 0.0;
  fdmtblGaranzia.First;
  if fdmtblGaranzia.Locate('SIGLAGARANZIA', trim(SiglaGaranzia), []) then
    Result := fdmtblGaranziaPremio.AsCurrency
  else
    raise EPolizzaError.Create('Garanzia non trovata - sigla = ' + SiglaGaranzia);
end;

procedure TDMDatiTempPolizza.impostaDatiPolizzaSostituita(ARimborso: currency; AScadenzaPolizzaSostituita: TDateTime);
begin
  if not(fdmtblPolizza.State in [dsInsert, dsEdit]) then
    fdmtblPolizza.Edit;
  // Opremio.premio_rimborsato := ARimborso;
  premio_da_rimborsare := ARimborso;
  fdmtblPolizzaRimborsoSost.AsCurrency := ARimborso;
  fdmtblPolizzaScadenzaPolizzaSostituita.AsDateTime :=  AScadenzaPolizzaSostituita;

end;

function TDMDatiTempPolizza.isBozza: boolean;
begin
  Result := fdmtblPolizzaStato.AsString = 'B';
end;

function TDMDatiTempPolizza.isPersonaFisica(const CodFiscPartitaIva: string): boolean;
const
  REG_EX_CF = '^[A-Z]{6}\d{2}[A-Z]\d{2}[A-Z]\d{3}[A-Z]$';
var
  regex: TRegEx;

begin
  regex  := TRegEx.Create(REG_EX_CF);
  Result := regex.Match(CodFiscPartitaIva).Success and (fdmtblPolizzaPartitaIva.AsString = '');
end;

function TDMDatiTempPolizza.isPolizza: boolean;
begin
  Result := (fdmtblPolizzaStato.AsString = '') and (fdmtblPolizzaNPolizza.AsString <> '');
end;

function TDMDatiTempPolizza.isPreventivo: boolean;
begin
  Result := fdmtblPolizzaStato.AsString = 'F';
end;

procedure TDMDatiTempPolizza.AggiornaNettoPerEstensione(GaranziaChecked: boolean; SiglaEstensione: string;
  var Importo: currency; NettoBis: boolean);
begin
  if GaranziaChecked then
  begin
    if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, SiglaEstensione) then
    begin
      if NettoBis then
        nettoAnnuoTotNONScontatoBis := nettoAnnuoTotNONScontatoBis +
          UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('PREMIO').AsCurrency
      else
        nettoAnnuoTotNONScontato := nettoAnnuoTotNONScontato + UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName
          ('PREMIO').AsCurrency;
      Importo := UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('PREMIO').AsCurrency;
    end
    else
    begin
      GaranziaChecked := false;
      Importo         := 0;
    end;
  end
  else
    Importo := 0;

end;

procedure TDMDatiTempPolizza.AggiornaNettoPerEstensione(AField: TBooleanField; SiglaEstensione: string;
  var Importo: currency; NettoBis: boolean = false);
begin
  AggiornaNettoPerEstensione(AField.AsBoolean, SiglaEstensione, Importo, NettoBis);
end;

function TDMDatiTempPolizza.genera_cbALL(polizza: string; num_pagine, pagina: Integer; tipo: string): string;
begin
  // genera il codice a barre per gli allegati
  // 35214
  // '0' + identificativo dell'allegato
  // 1 = 7A
  // 2 = 7B
  // 3 = adeguatezza
  // 4 = info precontrattuale
  // 5 = CGA
  // 6 = fascicolo info
  // 7 = nota informativa
  // 8 = privacy contraente
  // 02 che indica il numero di pagine del documento
  // 01 02  numero della pagina corrente
  // nnnnnnn 7 cifre paddatate a sinistra con 0 per il numero di polizza
  Result := '35214';
  Result := Result + '0';
  if tipo = 'CD' then
    Result := Result + '0'; // dichiarazione consegna documentazione
  if tipo = 'A3' then
    Result := Result + '1'; // all 3
  if tipo = 'A4' then
    Result := Result + '2'; // all 4
  if tipo = 'CO' then
    Result := Result + '3'; // coerenza
  if tipo = 'IP' then
    Result := Result + '4'; //
  if tipo = 'CG' then
    Result := Result + '5'; // cgp
  if tipo = 'FI' then
    Result := Result + '6';
  if tipo = 'NI' then
    Result := Result + '7';
  if tipo = 'PI' then
    Result := Result + '8';
  if tipo = 'DI' then
    Result := Result + '9';
  if (Length(polizza) < 5) then
    polizza := '0';

  Result := Result + PadL(IntToStr(num_pagine), '0', 2) + PadL(IntToStr(pagina), '0', 2);
  Result := Result + PadL(AllTrim(polizza), '0', 7);
end;

procedure TDMDatiTempPolizza.valorizzaAllegatiPolizza(npol, tipo_modulo, Sigla_pol: string; report: TfrxReport);
var
  tt: TstringList;
  i: Integer;
  sig, n_rui, intermediario: string;

  function dai_ruolo(cosa: string): string;
  begin
    Result := 'COLLABORATORE';
    if cosa = 'P' then
      Result := 'COLLABORATORE'
    else
      if cosa = 'S' then
        Result := 'SUB-AGENTE'
      else
        if cosa = 'A' then
          Result := 'AGENTE'
        else
          if cosa = 'O' then
            Result := 'COLLABORATORE'
          else
            if cosa = 'I' then
              Result := 'DIPENDENTE'
            else
              if cosa = 'Y' then
                Result := 'DIPENDENTE'
              else
                if cosa = 'Z' then
                  Result := 'COLLABORATORE'
                else
                  if cosa = 'X' then
                    Result := 'COLLABORAZIONE A/A';
    {
      P= PRODUTTORE ISCRITTO RUI
      S= SUB-AGENTE
      A= AGENTE / BROKER
      O= PRODUTTORE OCCASIONALE ISCRITTO RUI
      'I=IMPIEGATA/O / COLLAB. INTERNO NON ISCRITTO RUI
      Y= IMPIEGATA/O ISCRITTO RUI
      X= SEGNALATORE
      Z= COLLABORAZIONE A-AB
    }
  end;

{$MESSAGE HINT 'Sarebbe da spostare tutta in DMDatiAge''}
  procedure get_dati_intermediario;
  var
    n_rui_base, intermediario_base, ruolo: string;
  begin

    // prima fase: separa l'agenzia tra Societ� e Ditte indiduali (persone fisiche)
    // se ditta individuale non � possibile caricare il rui in dati_Agenzia (andr� caricato
    // in produtt.dat quando si caricher� l'agente come tipo A
    // seconda fase: se la persona che intermedia � n agente allora usa i suoi dati per l'intestazione
    // della coerenza
    // se la persona che intermedia NON � un agente allo crea la frase da scrivere
    // indicando chi intermedia e il suo rapporto con l'agente e/o la societ�

    // ricava il ruolo che ha la persona che intermedia in rapporto all'intermediario
    UniMainModule.DMdatiAge.QcercaProdutt.Close;
    UniMainModule.DMdatiAge.QcercaProdutt.ParamByName('cod_produttore').asInteger :=
      fdmtblPolizzaIntermediataDa.asInteger;
    UniMainModule.DMdatiAge.QcercaProdutt.Open;
    ruolo := dai_ruolo(UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('tipo').AsString);

    UniMainModule.DMdatiAge.QDati_agenzia.Open;
    if (UniMainModule.DMdatiAge.QDati_agenzia.FieldByName('tipo').AsString = 'S') then
    begin
      // caso di intermediario Societ�: devi prendere il numero Rui e il nome da qui in quanto tutti
      // quelli elencati in produtt.dat dipendona dalla societ�
      n_rui_base         := UniMainModule.DMdatiAge.QDati_agenzia.FieldByName('n_rui').AsString;
      intermediario_base := UniMainModule.DMdatiAge.QDati_agenzia.FieldByName('denominazione').AsString;

      // TODO: inseriamo anche il RUI della societ� sempre e comunque ??

      if copy(ruolo, 1, 1) = 'A' then // A S D C
      begin
        // agente:  riporta il suo rui personale ed il suo nome
        n_rui         := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('n_rui').AsString;
        intermediario := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('nome').AsString;
      end;
      if copy(ruolo, 1, 1) = 'S' then // A S D C
      begin
        // sub-agente:  riporta il suo rui , il suo nome , il suo ruole e il nome dell'agenzia
        n_rui         := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('n_rui').AsString;
        intermediario := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('nome').AsString + ' COLLABORATORE DI ' +
          intermediario_base;  //  + ' (' + n_rui_base + ')';
      end;
      if copy(ruolo, 1, 1) = 'D' then // A S D C
      begin
        // dipendente
        n_rui         := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('n_rui').AsString;
        intermediario := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('nome').AsString + ' DIPENDENTE DI ' +
          intermediario_base;  //  + ' (' + n_rui_base + ')';
      end;

    end
    else
    begin
      // caso di intermediario Ditta individuale : devi prendere il numero Rui dell'agente caricato in produtt.dat

      UniMainModule.DMdatiAge.QproduttAgente.Open;
      n_rui_base         := UniMainModule.DMdatiAge.QproduttAgente.FieldByName('n_rui').AsString;
      intermediario_base := UniMainModule.DMdatiAge.QproduttAgente.FieldByName('nome').AsString;

      if copy(ruolo, 1, 1) = 'A' then // A S D C
      begin
        // agente
        n_rui         := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('n_rui').AsString;
        intermediario := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('nome').AsString;
      end;
      if copy(ruolo, 1, 1) = 'S' then // A S D C
      begin
        // sub-agente
        n_rui         := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('n_rui').AsString;
        intermediario := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('nome').AsString + ' COLLABORATORE DI ' +
          intermediario_base;  //  + ' (' + n_rui_base + ')';
      end;
      if copy(ruolo, 1, 1) = 'D' then // A S D C
      begin
        // dipendente
        n_rui         := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('n_rui').AsString;
        intermediario := UniMainModule.DMdatiAge.QcercaProdutt.FieldByName('nome').AsString + ' DIPENDENTE DI ' +
          intermediario_base;  // + ' (' + n_rui_base + ')';
      end;

    end;

  end;

begin
  // istanzia i valori delle variabili negli allegati
  // variabili da istanziare nei moduli
  // CGP variabili => n_polizza contraente facsimile
  // COP variabili => nulla
  // DIP variabili => nulla
  // DIA variabili => nulla
  // ALL3 variabili => facsimile data_emi barcode1 contraente
  // ALL4 variabili => riga0..9  contraente n_polizza data_emi barcode1
  // PRI variabili => n_polizza contraente dati_contraente si no
  // INFO CONTRAENTE variabili => contraente data_emi
  // COER variabili => intermediario n_rui n_polizza contraente rag_soc nome_polizza
  // c1..9  p1..4  v1..3  a1 k1 k2 x1..8 y1 y2 z1 z2 data_emi

  // nome_polizza per la coerenza

  if tipo_modulo = 'DIP' then
  begin
    // non fare nulla
  end;
  if tipo_modulo = 'DIA' then
  begin
    // non fare nulla
  end;
  if tipo_modulo = 'COP' then
  begin
    // non fare nulla
  end;

  if tipo_modulo = 'CGP' then
  begin
    // CGP variabili => n_polizza contraente facsimile
    {
    if npol > '' then
    begin
      report.Variables['n_polizza'] := QuotedStr(npol);
      report.Variables['facsimile'] := QuotedStr('N');

    end
    else
    begin
      report.Variables['facsimile'] := QuotedStr('S');
      report.Variables['polizza']   := QuotedStr('Fac-Simile');
    end;
    report.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString);
    }
  end;

  if tipo_modulo = 'PRI' then
  begin
    // PRI variabili => n_polizza contraente
    if npol > '' then
    begin
      report.Variables['n_polizza'] := QuotedStr(npol);
      report.Variables['barcode1']  := QuotedStr(genera_cbALL(npol, 1, 1, 'PI'));
    end
    else
    begin
      report.Variables['polizza'] := QuotedStr('Fac-Simile');
    end;
    report.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString);
  end;

  if tipo_modulo = 'COER' then
  begin
    // COER variabili => intermediario n_rui n_polizza contraente rag_soc nome_polizza
    // c1..9  p1..4  v1..3  a1 k1 k2 x1..8 y1 y2 z1 z2 data_emi
    i   := pos('(', UniMainModule.DMdatiAgePolizze.getLabelDatiPolizza(UniMainModule.TipoPolizza));
    sig := copy(UniMainModule.DMdatiAgePolizze.getLabelDatiPolizza(UniMainModule.TipoPolizza), 1, i - 1);
    report.Variables['nome_polizza'] := QuotedStr(sig);
    if npol > '' then
    begin
      report.Variables['n_polizza'] := QuotedStr(npol);
      report.Variables['barcode1']  := QuotedStr(genera_cbALL(npol, 1, 1, 'CO'));
    end
    else
    begin
      report.Variables['polizza'] := QuotedStr('Fac-Simile');
    end;

    tt := TstringList.Create;

    tt.Delimiter     := '.'; // . CommaText
    tt.DelimitedText := fdmtblPolizza.FieldByName('PerAllegati').AsString;
    for i            := 0 to tt.count - 1 do
    begin
      sig := tt.Strings[i];
      if report.Variables.IndexOf(sig) > 0 then
        report.Variables[sig] := QuotedStr('X');
    end;
    tt.free;

    if isPersonaFisica(fdmtblPolizzaCodiceFiscale.AsString) then
      report.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString)
    else
      report.Variables['rag_soc'] := QuotedStr(fdmtblPolizzaContraente.AsString);

    report.Variables['data_emi'] := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
    get_dati_intermediario;
    report.Variables['n_rui']         := QuotedStr(n_rui);
    report.Variables['intermediario'] := QuotedStr(intermediario);

  end;

  if tipo_modulo = 'CODO' then
  begin
    // CODO variabili => n_polizza contraente
    if npol > '' then
    begin
      report.Variables['n_polizza'] := QuotedStr(npol);
      report.Variables['barcode1']  := QuotedStr(genera_cbALL(npol, 1, 1, 'CD'));
    end
    else
    begin
      report.Variables['polizza'] := QuotedStr('Fac-Simile');
    end;
    report.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString);
  end;

end;





procedure TDMDatiTempPolizza.memorizzaAllegatiPolizza(npol, Sigla_pol: string);
var
  stream: TMemoryStream;
  // report: TfrxReport;
begin
  // genera e meorizza tutti gli allegati che servono alla polizza indicata
  // usa il componente fastReport di comodo presente nella form

  // per ogni allegato:
  // 1- carica il documento da mod_slp
  // 2- se serve istanzia le varibili del fastreport
  // 3- genera l'anteprima e trasformala in uno stream
  // 4- salva lo stream in printhouse

  // variabili da istanziare nei moduli
  // CGP variabili => n_polizza contraente facsimile
  // COP variabili => nulla
  // DIP variabili => nulla
  // DIA variabili => nulla
  // ALL3 variabili => facsimile data_emi barcode1 contraente
  // ALL4 variabili => riga0..9  contraente n_polizza data_emi barcode1
  // PRI variabili => n_polizza contraente dati_contraente si no
  // INFO CONTRAENTE variabili => contraente data_emi
  // COER variabili => intermediario n_rui n_polizza contraente rag_soc nome_polizza
  // c1..9  p1..4  v1..3  a1 k1 k2 x1..8 y1 y2 z1 z2 data_emi

  // SET INFORMATIVO  ================================================================================
//  if carica_ModuliVari('COP', Sigla_pol, frxAllegatiPOL) then
  if True then
  begin
    // report := frxReport1;
    // TfrxReport.Create(self);
    // report.EngineOptions.MaxMemSize:=10;
    // report.EngineOptions.UseFileCache:=False;
    // frxReport1.Clear;
    stream := TMemoryStream.Create;

    // Log('DMDatiTempPolizza.memorizzaAllegatiPolizza prima valorizzaAllegatiPolizza(npol, COP (6.1) '+DateTimeToStr(now));
    {
    // vecchia parte che assemblava il SET partendo dai singoli pezzi
    // da ottobre 2020 tolta in quanto troppo lenda e aggiunto stream che contiene il SET completo per ogni polizza
    // avviamente � stata tolta ogni personalizzazione alle componenti del SET
    valorizzaAllegatiPolizza(npol, 'COP', Sigla_pol, frxReport1);
    frxAllegatiPOL.PrepareReport(true);
    if carica_ModuliVari('DIP', Sigla_pol, frxReport1) then
    begin

      Log('DMDatiTempPolizza.memorizzaAllegatiPolizza prima valorizzaAllegatiPolizza(npol, DIP (6.2) ' +
        DateTimeToStr(now));

      valorizzaAllegatiPolizza(npol, 'DIP', Sigla_pol, frxReport1);
      frxReport1.PrepareReport(true);

      Log('DMDatiTempPolizza.memorizzaAllegatiPolizza prima carica_ModuliVari(DIP (6.21) ' + DateTimeToStr(now));

      if carica_ModuliVari('DIA', Sigla_pol, frxReport1) then
      begin

        Log('DMDatiTempPolizza.memorizzaAllegatiPolizza prima valorizzaAllegatiPolizza(npol, DIA (6.3) ' +
          DateTimeToStr(now));

        valorizzaAllegatiPolizza(npol, 'DIA', Sigla_pol, frxReport1);
        frxReport1.PrepareReport(false);
        if carica_ModuliVari('CGP', Sigla_pol, frxReport1) then
        begin

          Log('DMDatiTempPolizza.memorizzaAllegatiPolizza prima valorizzaAllegatiPolizza(npol, CGP (6.4) ' +
            DateTimeToStr(now));

          valorizzaAllegatiPolizza(npol, 'CGP', Sigla_pol, frxReport1);
          frxReport1.PrepareReport(false);

          Log('DMDatiTempPolizza.memorizzaAllegatiPolizza prima valorizzaAllegatiPolizza(npol, CGP (6.41) ' +
            DateTimeToStr(now));

          frxAllegatiPOL.PreviewPages.AddFrom(frxReport1);
        end;
      end;
    end;
    }
    // stream := TmemoryStream.Create;
    try
      if carica_ModuliVari('SET', Sigla_pol, frxAllegatiPOL, stream) then
      begin
         // frxAllegatiPOL.PreviewPages.SaveToStream(stream);
         stream.Position := 0;
         UniMainModule.DMdatiAge.MemorizzaReportPolizza(stream, npol, 'SET' + Sigla_pol, Date, false);
      end;
    finally
      stream.free;
    end;
  end;

  // MODULO PRIVACY  ================================================================================
  if carica_ModuliVari('PRI', '', frxAllegatiPOL) then
  begin
    frxReport1.Clear;
    frxReport1.EngineOptions.SilentMode           := true;
    frxReport1.EngineOptions.EnableThreadSafe     := true;
    frxReport1.EngineOptions.DestroyForms         := false;
    frxReport1.EngineOptions.UseGlobalDataSetList := false;

    stream := TMemoryStream.Create;

    valorizzaAllegatiPolizza(npol, 'PRI', Sigla_pol, frxReport1);
    frxAllegatiPOL.PrepareReport(true);
    try
      frxAllegatiPOL.PreviewPages.SaveToStream(stream);
      UniMainModule.DMdatiAge.MemorizzaReportPolizza(stream, npol, 'PRI' + Sigla_pol, Date, false);
    finally
      stream.free;
    end;
  end;

  // ALLEGATO 4  ================================================================================
  {
    if carica_ModuliVari('ALL4', '', frxAllegatiPOL) then
    begin
    report:=TfrxReport.Create(self);
    stream := TmemoryStream.Create;

    valorizzaAllegatiPolizza(npol,'ALL4', sigla_pol, report);
    frxAllegatiPOL.PrepareReport(true);
    try
    frxAllegatiPOL.PreviewPages.SaveToStream(stream);
    UniMainModule.DMdatiAge.MemorizzaReportPolizza(stream, npol, 'ALL4'+sigla_pol, date, False);
    finally
    stream.free;
    report.Free
    end;
    end; }

  // COERENZA  ================================================================================
  if carica_ModuliVari('COER', '', frxAllegatiPOL) then
  begin
    stream := TMemoryStream.Create;

    // valorizzaAllegatiPolizza(npol, 'COER', Sigla_pol, report);
    valorizzaAllegatiPolizza(npol, 'COER', Sigla_pol, frxAllegatiPOL);
    frxAllegatiPOL.PrepareReport(true);
    try
      frxAllegatiPOL.PreviewPages.SaveToStream(stream);
      UniMainModule.DMdatiAge.MemorizzaReportPolizza(stream, npol, 'COER' + Sigla_pol, Date, false);
    finally
      stream.free;
    end;
  end;

  // DICHIARAZIONE CONSEGNA DOCUMENTAZIONE  ================================================================================
  if carica_ModuliVari('CODO', '', frxAllegatiPOL) then
  begin
    stream := TMemoryStream.Create;
    valorizzaAllegatiPolizza(npol, 'CODO', Sigla_pol, frxReport1);
    frxAllegatiPOL.PrepareReport(true);
    try
      frxAllegatiPOL.PreviewPages.SaveToStream(stream);
      UniMainModule.DMdatiAge.MemorizzaReportPolizza(stream, npol, 'CODO' + Sigla_pol, Date, false);
    finally
      stream.free;
    end;
  end;

  // ALLEGATO 3  ================================================================================

  // INFO PRECONTRATTUALE  ================================================================================

end;

function TDMDatiTempPolizza.carica_ModuliVari(tipo_modulo, Sigla_pol: string; report: TfrxReport; xstream: TMemoryStream): boolean;
var
  blobs: Tstream;
  codice_slp: string;
begin
  // carica il modulo giusto
  report.Clear;
  codice_slp := tipo_modulo + Sigla_pol;
  UniMainModule.DMdatiAge.Qmoduli.Close;
  UniMainModule.DMdatiAge.Qmoduli.ParamByName('tipo').AsString := 'S';
  UniMainModule.DMdatiAge.Qmoduli.ParamByName('codice_slp').AsString := codice_slp;
  UniMainModule.DMdatiAge.Qmoduli.Open;

  Result := false;

  if (UniMainModule.DMdatiAge.Qmoduli.RecordCount > 0) and (codice_slp > '') then
  begin
    // BlobS := Qmoduli.CreateBlobStream(Qmoduli.fieldbyname('testo'), bmRead);
    if UniMainModule.DMdatiAge.get_mod_slp_gen(UniMainModule.DMdatiAge.Qmoduli.FieldByName('cod_stampa').asInteger,
       blobs) then begin
       if tipo_modulo='SET' then
          xstream.LoadFromStream(blobs)
          // report.PreviewPages.LoadFromStream(blobs)
       else
          report.LoadFromStream(blobs);
    end;
    Result := true;
  end;

end;

function TDMDatiTempPolizza.SLP_StampaPolizza(anteprima: boolean = false): Integer;
begin
  preparaReport;

  Result := memorizzaReport(UniMainModule.NumPolizza + '-P.pdf', true, false, UniMainModule.NumPolizza,
    'P-' + UniMainModule.SiglaPolizza, fdmtblPolizzaDataEmissione.AsDateTime);

  Log('DMDatiTempPolizza.SLP_StampaPolizza dopo memorizzaReport (6) ' + DateTimeToStr(now));

  // memorizza anche gli altri documenti
  if not anteprima then
    memorizzaAllegatiPolizza(UniMainModule.NumPolizza, UniMainModule.SiglaPolizza);

  Log('DMDatiTempPolizza.SLP_StampaPolizza dopo memorizzaAllegatiPolizza (7) ' + DateTimeToStr(now));

end;

procedure TDMDatiTempPolizza.AzzeraCampiReport;
var
  indice: Integer;
begin

  for indice := 0 to frxPolizzaGen.Variables.count - 1 do
  begin
    frxPolizzaGen.Variables.Items[indice].Value := QuotedStr('');
  end;
  // frxPolizzaGen.Variables['versione']  := QuotedStr(leggiVersione);
  frxPolizzaGen.Variables['versione']  := QuotedStr('1.0.0');
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('N');
  // frxPolizzaGen.Variables['RAMO']      := QuotedStr(ramo_slp);
  frxPolizzaGen.Variables['RAMO']     := QuotedStr('17');
  frxPolizzaGen.Variables['barcode1'] := QuotedStr('00');
  frxPolizzaGen.Variables['barcode2'] := QuotedStr('00');

end;

end.
