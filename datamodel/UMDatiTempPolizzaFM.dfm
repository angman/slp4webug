inherited DMDatiTempPolizzaFM: TDMDatiTempPolizzaFM
  OldCreateOrder = True
  inherited fdmtblPolizza: TFDMemTable
    inherited fdmtblPolizzaSiglaMassimale: TStringField
      OnChange = fdmtblPolizzaSiglaMassimaleChange
    end
    object fdmtblPolizzaIndicizzata: TBooleanField [107]
      FieldName = 'Indicizzata'
      OnChange = fdmtblPolizzaIndicizzataChange
    end
    object fdmtblPolizzaEStensione12: TBooleanField [108]
      FieldName = 'EStensione12'
      OnChange = fdmtblPolizzaEStensione12Change
    end
    object fdmtblPolizzaEstensione13: TBooleanField [109]
      FieldName = 'Estensione13'
      OnChange = fdmtblPolizzaEstensione13Change
    end
    inherited fdmtblPolizzaNPercAccessori: TIntegerField
      OnChange = fdmtblPolizzaNPercAccessoriChange
    end
  end
  inherited fdmtblGaranzia: TFDMemTable
    object fdmtblGaranziaRischioB: TBooleanField
      FieldName = 'RischioB'
    end
    object fdmtblGaranziaRischioBDecremento: TCurrencyField
      FieldName = 'RischioBDecremento'
    end
  end
  inherited frxPolizzaGen: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxAllegatiPOL: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxReport1: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
end
