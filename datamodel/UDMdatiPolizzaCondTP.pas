unit UDMDatiPolizzaCondTP;

interface

uses
  SysUtils, Classes, UDMDatiBasePolizza, dbisamtb, Data.DB, UTipoSalvataggioDocumento;

type
  TDMDatiPolizzaCondTP = class(TDMDatiBasePolizza)
  private
    { Private declarations }
  protected
    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); override;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); override;

  public
    { Public declarations }
    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): Integer; override;

  end;

function DMDatiPolizzaCondTP: TDMDatiPolizzaCondTP;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule;

function DMDatiPolizzaCondTP: TDMDatiPolizzaCondTP;
begin
  Result := TDMDatiPolizzaCondTP(UniMainModule.GetModuleInstance(TDMDatiPolizzaCondTP));
end;

{ TDMDatiPolizzaCondTP }

procedure TDMDatiPolizzaCondTP.SalvaAssicurato(DataSetAssicurati: TDataSet);
begin
  inherited;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaCondTP.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  inherited;
  QInsGaranzia.ExecSQL;
end;

function TDMDatiPolizzaCondTP.SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio): Integer;
begin
  SalvaBasePolizza(DataSetPolizza, DataPreventivo, TipoSalvataggio);
  QInsPolizza.ParamByName('NUMERO_ASSICURATI').Clear;
  // if DataSetPolizza.FindField('NUMEROASSICURATII') <> nil then
  // QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger  := 1;
  QInsPolizza.ExecSQL;

  Result := getLastCodPolizza;

end;

initialization

RegisterClass(TDMDatiPolizzaCondTP);

end.
