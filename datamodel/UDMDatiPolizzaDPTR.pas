unit UDMDatiPolizzaDPTR;

interface

uses
  System.SysUtils, System.Classes, UDMDatiBasePolizza, dbisamtb, Data.DB, UTipoSalvataggioDocumento;

type
  TDMDatiPolizzaDPTR = class(TDMDatiBasePolizza)
  private
    { Private declarations }
  protected
    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): integer; override;
    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); override;
    procedure SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer); override;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); override;

  public
    { Public declarations }

  end;

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}
{ TDMDatiBasePolizzaOM }

procedure TDMDatiPolizzaDPTR.SalvaAssicurato(DataSetAssicurati: TDataSet);
begin
  inherited;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaDPTR.SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer);
begin
  inherited;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaDPTR.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  inherited;
  QInsGaranzia.ExecSQL;
end;

function TDMDatiPolizzaDPTR.SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio): integer;
begin
  SalvaBasePolizza(DataSetPolizza, DataPreventivo, TipoSalvataggio);
  QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger := DataSetPolizza.FieldByName('NUMEROASSICURATI').AsInteger;

  QInsPolizza.ExecSQL;

  Result := getLastCodPolizza;
end;

initialization

RegisterClass(TDMDatiPolizzaDPTR);

end.
