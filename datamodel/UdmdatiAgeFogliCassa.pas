unit UdmdatiAgeFogliCassa;

interface

uses
  SysUtils, Classes, UDMMaster, Data.DB, dbisamtb, UQueryElencoIntf;

type
  TDMDatiAgeFogliCassa = class(TDMMaster)
    QfogliCassa: TDBISAMQuery;
    QAnni: TDBISAMQuery;
    QgetNewNumeroFC: TDBISAMQuery;
    QgetTotaliFC: TDBISAMQuery;
    QinsFoglioCassa: TDBISAMQuery;
    QgetCodiceFC: TDBISAMQuery;
    QspostaInStorico: TDBISAMQuery;
    QaggCodFC_s_scad: TDBISAMQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 1; CodCollaboratore: integer = 0; codCompagnia: Integer=1); override;
    procedure chiusura_contabile(chiusuraDal, chiusuraAl: TDateTime);
  end;

  // function DMDatiAgeFogliCassa: TDMDatiAgeFogliCassa;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, System.DateUtils, System.StrUtils, libSLP;

// function DMDatiAgeFogliCassa: TDMDatiAgeFogliCassa;
// begin
// Result := TDMDatiAgeFogliCassa(UniMainModule.GetModuleInstance(TDMDatiAgeFogliCassa));
// end;

{ TDMDatiAgeFogliCassa }

procedure TDMDatiAgeFogliCassa.EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 1; CodCollaboratore: integer = 0; codCompagnia: Integer=1);
begin
  QfogliCassa.Close;
  QfogliCassa.SQL.Clear;
  QfogliCassa.SQL.Append
    ('select codice_foglio, periodo_dal, periodo_al, cast(data_chiusura as date) as data_chiusura, data_invio, tipo_invio,');
  QfogliCassa.SQL.Append('cod_compagnia, sigla_compagnia, numero_fc, versato, trattenuto , codice_foglio,');
  QfogliCassa.SQL.Append('extract(year from periodo_dal) as anno');

  QfogliCassa.SQL.Append('from fogli_cassa');

  if ValToSearch <> '' then
    QfogliCassa.SQL.Add('where extract(year from periodo_dal) = ' + ValToSearch);
  if ordFieldName <> '' then
    QfogliCassa.SQL.Add('order by ' + ordFieldName + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  QfogliCassa.open;

end;

procedure TDMDatiAgeFogliCassa.chiusura_contabile(chiusuraDal, chiusuraAl: TDateTime);
var
  // TXTsql: Tstrings;
  temMsg: string;
  n_fc: integer;
  prefc: string;

  tablesList: TStringList;
  lDatabase: TDBISAMDatabase;
  codice_fc: integer;

  function floatToStringa(n: Currency): string;
  begin
    Result := StringReplace(floatToStr(n), ', ', '.', [rfReplaceAll]);
  end;

begin
  // 1- richiedi il periodo di chiusura (proponilo in base ai fogli cassa precedenti e alle indicazioni sulla periodicit�
  // del foglio cassa inserite in compagnie
  // 2- estrai i dati + istanzia il numero di foglio cassa + sposta in s_scadenze + cancella da scadenze
  // fare tutto con unica query da inserire in transazione per avere certeza di tutto fatto o tutto fare

  // chiusura definitiva solo per SLP ??? ... possiamo anche mplementala in modo generalizzato
  // verificare se c'e' qualcumo che usa la chiusura per roba non SLP (715 ....)

  // prendi il numero del foglio cassa da compagni con cod_compagnia=1 (per ora solo chiusura fc SLP)
  // inserisci il record nel file fogli_cassa
  // prendi il codice del foglio cassa inserito
  // totoalizza gli importi del foglio cassa
  // memoriza i totali nel foglio cassa

  tablesList := TStringList.Create;
  try
    lDatabase := QspostaInStorico.DBSession.Databases[0];

    tablesList.Append('scadenze');
    tablesList.Append('fogli_cassa');
    tablesList.Append('compagni');
    tablesList.Append('s_scadenze');
    if not lDatabase.InTransaction then
      lDatabase.StartTransaction(tablesList);
    try
      // per prima cosa crea il nuovo record relativo al foglio cassa ...
      // UniMainModule.DMdatiAge.QgetCompagnia.ParamByName('cod_compagnia').AsInteger := 1;
      // UniMainModule.DMdatiAge.QgetCompagnia.Open;

      // UniMainModule.DMdatiAgeTitoli.Qinc_num_f_cassa.Close;
      // UniMainModule.DMdatiAgeTitoli.Qinc_num_f_cassa.ParamByName('cod_compagnia').AsInteger := 1;
      // UniMainModule.DMdatiAgeTitoli.Qinc_num_f_cassa.execsql;
      QgetNewNumeroFC.Close;
      QgetNewNumeroFC.ParamByName('cod_compagnia').AsInteger := 1;
      QgetNewNumeroFC.open;
      // prendi e incrementa il numero di foglio cassa da usare
      n_fc := QgetNewNumeroFC.FieldByName('numero_fc').AsInteger;

      QgetTotaliFC.Close;
      QgetTotaliFC.ParamByName('chiusuradal').AsDateTime  := chiusuraDal;
      QgetTotaliFC.ParamByName('chiusuraal').AsDateTime   := chiusuraAl;
      QgetTotaliFC.ParamByName('cod_compagnia').AsInteger := 1; // SLP
      QgetTotaliFC.open;

      // caso SLP
      prefc := '';
      prefc := UniMainModule.DMdatiAge.SLPdati_age.agenzia;
      if UniMainModule.DMdatiAge.SLPdati_age.sub_age > '' then
        prefc := prefc + UniMainModule.DMdatiAge.SLPdati_age.sub_age;

      QinsFoglioCassa.Close;
      QinsFoglioCassa.ParamByName('periodo_dal').AsDateTime := chiusuraDal;
      QinsFoglioCassa.ParamByName('periodo_al').AsDateTime := chiusuraAl;
      QinsFoglioCassa.ParamByName('data_chiusura').AsDateTime := now;
      QinsFoglioCassa.ParamByName('operatore').AsInteger := UniMainModule.utenteCodice;
      QinsFoglioCassa.ParamByName('tipo_foglio').AsString := 'S';
      QinsFoglioCassa.ParamByName('versato').asCurrency   := QgetTotaliFC.FieldByName('tot_lordo').asCurrency;

      QinsFoglioCassa.ParamByName('trattenuto').asCurrency := QgetTotaliFC.FieldByName('tot_provvigioni').asCurrency;

      QinsFoglioCassa.ParamByName('cod_compagnia').AsInteger := QgetNewNumeroFC.FieldByName('cod_compagnia').AsInteger;
      // UniMainModule.DMdatiAge.QgetCompagnia.FieldByName('cod_compagnia').AsInteger;

      QinsFoglioCassa.ParamByName('sigla_compagnia').AsString := QgetNewNumeroFC.FieldByName('codice_Agenzia').AsString;
      // UniMainModule.DMdatiAge.QgetCompagnia.FieldByName('codice_Agenzia').AsString;

      QinsFoglioCassa.ParamByName('numero_fc').AsString := prefc + IntToStr(n_fc).PadLeft(4, '0');
      QinsFoglioCassa.ParamByName('id_gen_slp_age').AsInteger := strToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

      QinsFoglioCassa.ExecSQL;

      QgetCodiceFC.Close;
      QgetCodiceFC.ParamByName('periodo_dal').AsDateTime := chiusuraDal;
      QgetCodiceFC.ParamByName('periodo_al').AsDateTime  := chiusuraAl;
      QgetCodiceFC.ParamByName('numero_fc').AsString     := prefc + IntToStr(n_fc).PadLeft(4, '0');
      QgetCodiceFC.open;

      codice_fc := QgetCodiceFC.FieldByName('codice_foglio').AsInteger;

      // ----------------------------------------------------------------------------------------------
      QspostaInStorico.ParamByName('chiusuraDal').AsDateTime := chiusuraDal;
      QspostaInStorico.ParamByName('chiusuraAl').AsDateTime := chiusuraAl;
      QspostaInStorico.ParamByName('codice_foglio').AsInteger := codice_fc;
      QspostaInStorico.ParamByName('cod_compagnia').AsInteger := 1; // SLP
      QspostaInStorico.ExecSQL;
      // ----------------------------------------------------------------------------------------------
      QaggCodFC_s_scad.Close;
      QaggCodFC_s_scad.ParamByName('numero_fc').AsString := prefc + IntToStr(n_fc).PadLeft(4, '0');
      QaggCodFC_s_scad.ParamByName('codice_foglio').AsInteger := codice_fc;
      QaggCodFC_s_scad.ExecSQL;

      lDatabase.Commit(True);
      // comunica la chiusura del foglio cassa !!!!
      temMsg := 'XFC-' + prefc + IntToStr(n_fc).PadLeft(4, '0') + '-' + dToS(chiusuraDal) +
                '-' + dToS(chiusuraAl);
      UniMainModule.comunicaDIR('FCA', temMsg, '', '', '', '', 'XFC','',chiusuraDal);

    except
      on EDBISAMEngineError do
        lDatabase.Rollback;
    end;
  finally
    tablesList.Free;
  end;
  Sleep(400);

  temMsg := 'Ricordiamo che � NECESSARIO inviare alla Direzione una copia del foglio cassa completa degli eventuali simpli di polizza,'
    + 'appendici e copia della contabile bancaria o ricevuta di CCP attestante l''avvenuto pagamento.';

  // WebApplication.ShowNotification(temMsg, ntSuccess);
  // lDatabase.Commit(True);
  UniMainModule.DMdatiAgeTitoli.Qtitoli.Close;
  UniMainModule.DMdatiAgeTitoli.Qtitoli.open;
  UniMainModule.DMdatiAgeTitoli.Qtitoli.Locate('COD_SCADENZA', UniMainModule.codScadenza, []);
  UniMainModule.DMdatiAgeTitoli.Qtitoli.Refresh;

end;

end.
