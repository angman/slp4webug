inherited DMBozzePreventivi: TDMBozzePreventivi
  OldCreateOrder = True
  object QBozzePreventivi: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'SELECT POL.COD_POLIZZA,  POL.DT_PREVENTIVO, POL.CONTRAENTE, POL.' +
        'DECORRENZA, TPOL.DESCRIZIONE DESTIPOPOL,'
      'POL.STATUS, Pol.lordo, pol.descr_utente as descrutente,'
      'case  when status = '#39#39'  then '#39'POLIZZA'#39
      '      when status = '#39'P'#39' then '#39'POLIZZA PERFEZIONATA'#39
      '      when status = '#39'B'#39' then '#39'BOZZA'#39
      '      when status = '#39'F'#39' then '#39'PREVENTIVO'#39
      '      when status = '#39'FD'#39' then '#39'PREVENTIVO DIR'#39
      'end as descrStatus'
      'FROM SLP_TPOLIZZE POL'
      'LEFT JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL'
      'WHERE STATUS IN ('#39#39', '#39'B'#39', '#39'F'#39', '#39'P'#39', '#39'FD'#39')'
      '--and ID_GEN_SLP_AGE = :codAgenzia')
    Params = <>
    Left = 80
    Top = 89
    object QBozzePreventiviCOD_POLIZZA: TAutoIncField
      FieldName = 'COD_POLIZZA'
      Origin = 'SLP_TPOLIZZE.COD_POLIZZA'
    end
    object QBozzePreventiviDT_PREVENTIVO: TDateTimeField
      FieldName = 'DT_PREVENTIVO'
      Origin = 'SLP_TPOLIZZE.DT_PREVENTIVO'
    end
    object QBozzePreventiviCONTRAENTE: TStringField
      FieldName = 'CONTRAENTE'
      Origin = 'SLP_TPOLIZZE.CONTRAENTE'
      Size = 50
    end
    object QBozzePreventiviDECORRENZA: TDateField
      FieldName = 'DECORRENZA'
      Origin = 'SLP_TPOLIZZE.DECORRENZA'
    end
    object QBozzePreventiviDESTIPOPOL: TStringField
      FieldName = 'DESTIPOPOL'
      Origin = 'TIPO_POL.DESCRIZIONE'
      Size = 30
    end
    object fdmtblPolizzaQBozzePreventiviSTATUS: TStringField
      FieldName = 'STATUS'
      Origin = 'SLP_TPOLIZZE.STATUS'
      Size = 2
    end
    object QBozzePreventivilordo: TCurrencyField
      FieldName = 'lordo'
      Origin = 'SLP_TPOLIZZE.lordo'
    end
    object QBozzePreventividescrutente: TStringField
      FieldName = 'descrutente'
      Size = 40
    end
    object QBozzePreventividescrStatus: TStringField
      FieldName = 'descrStatus'
    end
  end
  object QPosizionaBozzaPreventivo: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select * from SLP_TPOLIZZE'
      'where COD_POLIZZA = :COD_POLIZZA')
    Params = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
    Left = 224
    Top = 49
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
  end
  object fdmtblStatusPolizza: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 64
    Top = 176
    object QSTATUSPOLIZZAID: TStringField
      FieldName = 'ID'
      Size = 2
    end
    object QStatusPolizzaDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 30
    end
  end
  object QCancellaBozzePreventivi: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'DELETE '
      'FROM SLP_TPOLIZZE '
      'WHERE COD_POLIZZA IN (')
    Params = <>
    Left = 224
    Top = 121
  end
end
