unit UDMAgeCollaboratori;

interface

uses
  System.SysUtils, System.Classes, UDMMaster, UQueryElencoIntf, Data.DB, dbisamtb, System.Rtti;

type
  TEnOrdCollaboratori   = (ocNome, ocReferente, ocCitta);
  TEnStatoCollaboratore = (scTutti, scAttivi, scInattivi, scPrimoLivello);
  TEnTipoCollaboratore  = (tcProduttore, tcSubAgente, tcAgenteBroker, tcProduttoreOccasionale, tcInternoNoRui,
    tcInternoRui, tcSegnalatore);

  // PRODUTTORE ISCRITTO RUI=P
  // SUB-AGENTE=S
  // AGENTE / BROKER=A
  // PRODUTTORE OCCASIONALE ISCRITTO RUI=O
  // IMPIEGATA/O / COLLAB. INTERNO NON ISCRITTO RUI=I
  // IMPIEGATA/O ISCRITTO RUI=Y
  // SEGNALATORE=X

  TDMAgeCollaboratori = class(TDMMaster, IQueryElenco)
    Qpromoter: TDBISAMQuery;
    Qsu_clienti: TDBISAMQuery;
    QeditCollaboratore: TDBISAMQuery;
    QAllegatiCollaboratore: TDBISAMQuery;
    QTipoAllegato: TDBISAMQuery;
    dsEditCollaboratore: TDataSource;
    QLinkProduttori: TDBISAMQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QeditCollaboratoreBeforePost(DataSet: TDataSet);
    procedure QAllegatiCollaboratoreNewRecord(DataSet: TDataSet);
    procedure QTipoAllegatoAfterOpen(DataSet: TDataSet);
    procedure QAllegatiCollaboratoreBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    function isRuiRequired(ATipoCollaboratore: integer): boolean;
    procedure ImpostaQuery(TipoFiltro: integer; ValToSearch: string);

  public
    { Public declarations }
    function CheckRui(ATipoCollaboratore: integer): boolean;

    // Interfaccia IQueryElenco

    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0; codCompagnia: Integer=1); overload;
      override;

    procedure EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
      DirezioneOrdinamento: integer = 1; TipoFiltro: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 0); overload; override;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); override;

    procedure CaricaAllegato(AStream: TFileStream);
    procedure ApriLstProdutt(ACurrentProdutt: integer);
    procedure VisualizzaAllegato;
  end;

implementation

uses
  uniStrUtils, System.StrUtils, MainModule, System.IOUtils, ServerModule, UStampePDFViewer, uniGUIApplication,
  UCodiciErroriPolizza, UPolizzaExceptions;

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}
{ TDMCollaboratori }

procedure TDMAgeCollaboratori.ApriLstProdutt(ACurrentProdutt: integer);
begin
  QLinkProduttori.Close;
  QLinkProduttori.ParamByName('CODPRODUTTORE').AsInteger := ACurrentProdutt;
  QLinkProduttori.Open;
end;

procedure TDMAgeCollaboratori.CaricaAllegato(AStream: TFileStream);
begin
  if not(QAllegatiCollaboratore.State in [dsInsert, dsEdit]) then
    QAllegatiCollaboratore.Edit;
  TBlobField(QAllegatiCollaboratore.FieldByName('TESTO')).LoadFromStream(AStream);
  QAllegatiCollaboratore.Post;
end;

function TDMAgeCollaboratori.CheckRui(ATipoCollaboratore: integer): boolean;
begin
  Result := isRuiRequired(ATipoCollaboratore);
  if not Result then
  begin
    if not(QeditCollaboratore.State in [dsInsert, dsEdit]) then
      QeditCollaboratore.Edit;

    QeditCollaboratore.FieldByName('N_RUI').Clear;
    QeditCollaboratore.FieldByName('DATA_ISCRIZ_RUI').Clear;
  end;

end;

procedure TDMAgeCollaboratori.DataModuleCreate(Sender: TObject);
begin
  inherited;
  OrdList.Append('Nome');
  OrdList.Append('Referente');
  OrdList.Append('Citta');
end;

procedure TDMAgeCollaboratori.EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
  DirezioneOrdinamento: integer; TipoFiltro: integer; CodCollaboratore: integer; codCompagnia: integer);
var
  i: integer;
  pre: string;
begin
  ImpostaQuery(TipoFiltro, ValToSearch);
  pre := ' AND ';
  if Pos('WHERE ', Qpromoter.SQL.Text) = 0 then
  begin
    Qpromoter.SQL.Add(' WHERE ');
    pre := '';
  end;
  for i := 0 to AParametersList.Count - 1 do
  begin
    if Copy(AParametersList.Strings[i],1,1)='#' then
    begin
      // la riga deve essere presa com riga SQL da inserire e basta !!!
      Qpromoter.SQL.Delete(Qpromoter.SQL.Count-1 );
      Qpromoter.SQL.Add( Copy(AParametersList.Strings[i],2,200));
    end else
      Qpromoter.SQL.Add(pre + 'PR.' + AParametersList.Names[i] + ' = ' + AParametersList.Values
        [AParametersList.Names[i]]);

    pre := ' AND ';
  end;
  strOrdCol := ordFieldName;
  Qpromoter.SQL.Add('ORDER BY ' + strOrdCol + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  Qpromoter.Open;

end;

procedure TDMAgeCollaboratori.ImpostaQuery(TipoFiltro: integer; ValToSearch: string);
var
  strWhereOrAnd: string;
begin
  Qpromoter.Close;
  Qpromoter.SQL.Clear;
  Qpromoter.SQL.Add('SELECT PR.* ,');
  Qpromoter.SQL.Add('CASE ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''P'' THEN ''PRODUTTORE ISCRITTO RUI'' ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''S'' THEN ''SUB-AGENTE'' ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''A'' THEN ''AGENTE / BROKER'' ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''O'' THEN ''PRODUTTORE OCCASIONALE ISCRITTO RUI'' ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''I'' THEN ''IMPIEGATA/O / COLLAB. INTERNO NON ISCRITTO RUI'' ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''Y'' THEN ''IMPIEGATA/O ISCRITTO RUI'' ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''X'' THEN ''SEGNALATORE'' ');
  Qpromoter.SQL.Add('WHEN pr.tipo=''Z'' THEN ''COLLABORAZIONE A-AB'' ');
  Qpromoter.SQL.Add('END AS TipoDesc ');
  Qpromoter.SQL.Add('from produtt pr');
  strWhereOrAnd := 'WHERE ';
  case TEnStatoCollaboratore(TipoFiltro) of
    scAttivi:
      Qpromoter.SQL.Add('WHERE PR.COD_PRODUTTORE IN ');

    scInattivi:
      Qpromoter.SQL.Add('WHERE PR.COD_PRODUTTORE NOT IN ');

    scPrimoLivello:
      Qpromoter.SQL.Add('WHERE (pr.link_produtt=0 or pr.link_produtt is null )');
  end;
  if TipoFiltro > 0 then
  begin
    if TEnStatoCollaboratore(TipoFiltro) <> scPrimoLivello then
    begin
      Qpromoter.SQL.Add('(SELECT CLI.PROMOTER ');
      Qpromoter.SQL.Add('FROM CLIENTI CLI LEFT JOIN POLIZZE POL ON CLI.COD_CLIENTE = POL.COD_CLI ');
      Qpromoter.SQL.Add('WHERE N_POLIZZA > '''' AND CLI.DENOMINAZ > '''' ');
      Qpromoter.SQL.Add(' ) ');
    end;
    strWhereOrAnd := 'AND ';

  end;

  // parte che filtra in base a chia ha eseguito l'accesso:
  // - se utente normale -> mostra tutti i collaboratori
  // - se utente promoter devi mostrare solo gli utenti che sono collegati a lui
  // - il link lo trovo a primo livello in utenti.promoter
  // che contiene il codice di collegamento alla tabella produtt.dat
  // se esiste il secondo livello lo trovo in produtt.link_produtt che
  // indica a quale altro produtt � sottoposto
  // - se entro come produttore utente U collegato al produttore P potro vedere solo
  // i produttori che anno il campo produtt.link_produtt = P
  if UniMainModule.IsUtentePromoter then
  begin
    // Qpromoter.SQL.Add(strWhereOrAnd + 'link_produtt = ' + IntToStr(UniMainModule.UtentePromoter));
    Qpromoter.SQL.Add(strWhereOrAnd );
    Qpromoter.SQL.Add( 'link_produtt = ' + IntToStr(UniMainModule.UtentePromoter));
    strWhereOrAnd := 'AND ';
  end;

  if ValToSearch > '' then
    Qpromoter.SQL.Add(strWhereOrAnd + strOrdCol + ' LIKE ''' + ValToSearch + '%''');

end;

procedure TDMAgeCollaboratori.EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0; codCompagnia: Integer=1);
var
  strWhereOrAnd: string;
begin
  // Qpromoter.Close;
  // Qpromoter.SQL.Clear;
  // Qpromoter.SQL.Add('SELECT PR.* ,');
  // Qpromoter.SQL.Add('CASE ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''P'' THEN ''PRODUTTORE ISCRITTO RUI'' ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''S'' THEN ''SUB-AGENTE'' ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''A'' THEN ''AGENTE / BROKER'' ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''O'' THEN ''PRODUTTORE OCCASIONALE ISCRITTO RUI'' ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''I'' THEN ''IMPIEGATA/O / COLLAB. INTERNO NON ISCRITTO RUI'' ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''Y'' THEN ''IMPIEGATA/O ISCRITTO RUI'' ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''X'' THEN ''SEGNALATORE'' ');
  // Qpromoter.SQL.Add('WHEN pr.tipo=''Z'' THEN ''COLLABORAZIONE A-AB'' ');
  // Qpromoter.SQL.Add('END AS TipoDesc ');
  // Qpromoter.SQL.Add('from produtt pr');
  // strWhereOrAnd := 'WHERE ';
  // case TEnStatoCollaboratore(TipoFiltro) of
  // scAttivi:
  // Qpromoter.SQL.Add('WHERE PR.COD_PRODUTTORE IN ');
  //
  // scInattivi:
  // Qpromoter.SQL.Add('WHERE PR.COD_PRODUTTORE NOT IN ');
  //
  // end;
  // if TipoFiltro > 0 then
  // begin
  // Qpromoter.SQL.Add('(SELECT CLI.PROMOTER ');
  // Qpromoter.SQL.Add('FROM CLIENTI CLI LEFT JOIN POLIZZE POL ON CLI.COD_CLIENTE = POL.COD_CLI ');
  // Qpromoter.SQL.Add('WHERE N_POLIZZA > '''' AND CLI.DENOMINAZ > '''' ');
  // Qpromoter.SQL.Add(' ) ');
  // strWhereOrAnd := 'AND ';
  //
  // end;
  //
  // if ValToSearch > '' then
  // Qpromoter.SQL.Add(strWhereOrAnd + strOrdCol + ' LIKE ''' + ValToSearch + '%''');
  { TODO 3 -oAngelo -cRevision : Gestire il link al produttore }
  // if link_padre > 0 then
  // begin
  // // bisogna filtrare i collaboratori in modo da mostrare solo quelli sotto stanti
  // // al collaboratore con link_padre = cod_produttore
  // if RGstato.ItemIndex > 0 then
  // Qpromoter.SQL.Add('and link_produtt=' + intTostr(link_padre))
  // else
  // Qpromoter.SQL.Add('where link_produtt=' + intTostr(link_padre))
  // end;
  ImpostaQuery(TipoFiltro, ValToSearch);
  strOrdCol := ordFieldName;
  Qpromoter.SQL.Add('ORDER BY ' + strOrdCol + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  Qpromoter.Open;

end;

function TDMAgeCollaboratori.isRuiRequired(ATipoCollaboratore: integer): boolean;
begin
  case TEnTipoCollaboratore(ATipoCollaboratore) of
    tcProduttore, tcSubAgente, tcAgenteBroker, tcProduttoreOccasionale, tcInternoRui:
      Result := true;
  else
    Result := false;
  end;
end;

procedure TDMAgeCollaboratori.PosizionaQuery(AValue: TValue);
begin
  QeditCollaboratore.Close;
  QeditCollaboratore.ParamByName('COD_PRODUTTORE').AsInteger := AValue.AsInteger;
  QeditCollaboratore.Open;
end;

procedure TDMAgeCollaboratori.QAllegatiCollaboratoreBeforePost(DataSet: TDataSet);
begin
  if QAllegatiCollaboratore.FieldByName('DESCRIZIONE').AsString = '' then
    raise EPolizzaError.Create(CERR_DESCR_ALLEGATO, MSG_DESCR_ALLEGATO);

  if QAllegatiCollaboratore.FieldByName('TIPO').AsInteger = 0 then
    raise EPolizzaError.Create(CERR_TIPO_ALLEGATO, MSG_TIPO_ALLEGATO);

  if QAllegatiCollaboratore.FieldByName('DT_INZIO_VALIDITA').AsDateTime = StrToDate('30/12/1899') then
    raise EPolizzaError.Create(CERR_DT_VAL_ALLEGATO, MSG_DT_VAL_ALLEGATO);

end;

procedure TDMAgeCollaboratori.QAllegatiCollaboratoreNewRecord(DataSet: TDataSet);
begin
  QAllegatiCollaboratore.FieldByName('DT_INS').AsDateTime := Date;
  QAllegatiCollaboratore.FieldByName('RIF_COLLABORATORE').AsInteger :=
    QAllegatiCollaboratore.ParamByName('COD_PRODUTTORE').AsInteger;
  QAllegatiCollaboratore.FieldByName('LIVELLO').AsInteger := 0;
  QAllegatiCollaboratore.FieldByName('ID_GEN_SLP_AGE').AsInteger :=
    UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;

end;

procedure TDMAgeCollaboratori.QeditCollaboratoreBeforePost(DataSet: TDataSet);

  function GetTipoCollaboratore(ATipoCollabStr: string): integer;
  const
    EnumLength = 7;
  const
    TipoCollStrArray: array [0 .. EnumLength - 1] of string = ('P', 'S', 'A', 'O', 'I', 'Y', 'X');
  var
    i: integer;
  begin
    Result := -1;
    for i  := Low(TipoCollStrArray) to High(TipoCollStrArray) do
    begin
      if ATipoCollabStr = TipoCollStrArray[i] then
      begin
        Result := i;
        Break;
      end;

    end;

  end;

begin
  if (QeditCollaboratore.FieldByName('SIGLA').AsString = '') or
    (QeditCollaboratore.FieldByName('SIGLA').AsString.Length < 2) then
    raise EPolizzaError.Create(CERR_COLLAB_SIGLA_MISSED, MSG_COLLAB_SIGLA_MISSED);
  if QeditCollaboratore.FieldByName('N_RUI').IsNull and
    isRuiRequired(GetTipoCollaboratore(QeditCollaboratore.FieldByName('TIPO').AsString)) then
    raise EPolizzaError.Create(CERR_NRUI_MISSED, MSG_NRUI_MISSED);
end;

procedure TDMAgeCollaboratori.QTipoAllegatoAfterOpen(DataSet: TDataSet);
begin
  QTipoAllegato.ParamByName('ID_GEN_SLP_AGE').AsInteger := UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;
end;

procedure TDMAgeCollaboratori.VisualizzaAllegato;
var
  ReportFileName: string;
  PDFFolder: string;
begin
  PDFFolder      := TPath.combine(UniServerModule.FilesFolderPath, 'PDF');
  ReportFileName := 'ALL_' + FormatDateTime('yyyymmddhhmmss', Now) + '.PDF';
  if QAllegatiCollaboratore.FieldByName('TESTO').IsNull then
    raise EPolizzaError.Create(CERR_CLI_CODFISC_ERR, MSG_ALLEGATO_NOT_FOUND);

  TBlobField(QAllegatiCollaboratore.FieldByName('TESTO')).savetofile(TPath.combine(PDFFolder, ReportFileName));
  TFPDFViewer.ShowReport(uniApplication, ReportFileName, UniServerModule.FilesFolderURL + 'PDF/', PDFFolder,
    true, true);
end;

end.
