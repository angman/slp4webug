inherited DMAgeCollaboratori: TDMAgeCollaboratori
  OldCreateOrder = True
  Width = 368
  object Qpromoter: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select pr.*,'
      'CASE'
      'WHEN pr.tipo='#39'P'#39' THEN '#39'PRODUTTORE ISCRITTO RUI'#39
      'WHEN pr.tipo='#39'S'#39' THEN '#39'SUB-AGENTE'#39
      'WHEN pr.tipo='#39'A'#39' THEN '#39'AGENTE / BROKER'#39
      'WHEN pr.tipo='#39'O'#39' THEN '#39'PRODUTTORE OCCASIONALE ISCRITTO RUI'#39
      
        'WHEN pr.tipo='#39'I'#39' THEN '#39'IMPIEGATA/O / COLLAB. INTERNO NON ISCRITT' +
        'O RUI'#39
      'WHEN pr.tipo='#39'Y'#39' THEN '#39'IMPIEGATA/O ISCRITTO RUI'#39
      'WHEN pr.tipo='#39'X'#39' THEN '#39'SEGNALATORE'#39
      'WHEN pr.tipo='#39'Z'#39' THEN '#39'COLLABORAZIONE A-AB'#39
      'END AS TipoDesc'
      ''
      'from produtt pr'
      ''
      'order by pr.nome'
      ''
      ''
      ''
      '')
    Params = <>
    Left = 66
    Top = 28
  end
  object Qsu_clienti: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select denominaz from clienti'
      'where promoter=:promoter')
    Params = <
      item
        DataType = ftUnknown
        Name = 'promoter'
      end>
    Left = 154
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'promoter'
      end>
  end
  object QeditCollaboratore: TDBISAMQuery
    BeforePost = QeditCollaboratoreBeforePost
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from produtt'
      'where COD_PRODUTTORE= :COD_PRODUTTORE')
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_PRODUTTORE'
        Value = '0'
      end>
    Left = 61
    Top = 88
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COD_PRODUTTORE'
        Value = '0'
      end>
  end
  object QAllegatiCollaboratore: TDBISAMQuery
    BeforePost = QeditCollaboratoreBeforePost
    OnNewRecord = QAllegatiCollaboratoreNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    DataSource = dsEditCollaboratore
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from allegati_age'
      'where rif_collaboratore = :COD_PRODUTTORE')
    Params = <
      item
        DataType = ftUnknown
        Name = 'COD_PRODUTTORE'
      end>
    Left = 221
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'COD_PRODUTTORE'
      end>
  end
  object dsEditCollaboratore: TDataSource
    DataSet = QeditCollaboratore
    Left = 64
    Top = 144
  end
  object QTipoAllegato: TDBISAMQuery
    AfterOpen = QTipoAllegatoAfterOpen
    BeforePost = QeditCollaboratoreBeforePost
    OnNewRecord = QAllegatiCollaboratoreNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    DataSource = dsEditCollaboratore
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from Tipo_documenti'
      'where ID_GEN_SLP_AGE = :ID_GEN_SLP_AGE')
    Params = <
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 229
    Top = 72
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QLinkProduttori: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select * from produtt'
      'where tipo not in ('#39'I'#39',  '#39'O'#39',  '#39'Y'#39',  '#39'X'#39')'
      'and Cod_produttore <> :CodProduttore')
    Params = <
      item
        DataType = ftUnknown
        Name = 'CodProduttore'
      end>
    Left = 266
    Top = 28
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CodProduttore'
      end>
  end
end
