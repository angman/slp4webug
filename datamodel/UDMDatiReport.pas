unit UDMDatiReport;

interface

uses
  System.SysUtils, System.Classes, Data.DB, dbisamtb,
  IWApplication, UdmdatiAge, System.DateUtils, frxClass, frxExportPDF, frxDBSet,
  frxBarcode, System.StrUtils, frxExportImage, frxExportText, frxRich, UDMMaster, frxGZip,
  frxExportBaseDialog, UShowAppendiceRec, System.Types, JvStringHolder;

type
  TipoDocEnum            = (tdDocHouse, tdPrintHouse, tdModuli, tdAppendice);
  TipoOrdQuietanziamenti = (toqPolizza, toqPolizzaContraente, toqCollaboratorePolizza, toqCollabContraente,
    toqScadenzaContraente, toqScadenzaPolizza);
  TipoCopieQuietanziamenti = (tcqStessaPagina, tcqDuePagine);
  TipoStampaIncassi = (tsiTotIncassi, tsiDettIncassi, tsiTotPolizzeMese, tsiDettPolizze, tsiDettIncassiPromoter);

  TRecDate = record
    DataDa: string;
    DataA: string;
  end;

  TDMDatiReport = class(TDMMaster)
    QRistampaFC: TDBISAMQuery;
    QFindCliente: TDBISAMQuery;
    QFogliCassa: TDBISAMQuery;
    DSFogliCassa: TDataSource;
    Qincassi: TDBISAMQuery;
    QContratti: TDBISAMQuery;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxDBfogliCassa: TfrxDBDataset;
    frxPDFExport1: TfrxPDFExport;
    QChiusuraContabile_FCProvvisorio: TDBISAMQuery;
    frxFoglioCassa: TfrxReport;
    ExpTXT: TfrxSimpleTextExport;
    frxDBAppendice: TfrxDBDataset;
    Rep_appendice: TfrxReport;
    QAppendice: TDBISAMQuery;
    QAppendici: TDBISAMQuery;
    QAssicurati: TDBISAMQuery;
    QcompagniSLP: TDBISAMQuery;
    frxRichObject1: TfrxRichObject;
    QdocHouse: TDBISAMQuery;
    QaggVIS_docHouse: TDBISAMQuery;
    QaggSTA_docHouse: TDBISAMQuery;
    frxrprtPDF: TfrxReport;
    frxGzipCompr: TfrxGZipCompressor;
    sfldQdocHousedescrizione: TStringField;
    sfldQdocHousetipo_doc: TStringField;
    fldQdocHousecodice_doc: TIntegerField;
    QdocHousetesto: TBlobField;
    dtfldQdocHousedata_doc: TDateField;
    QdocHousedata_carico: TDateTimeField;
    QdocHousedata_lettura: TDateTimeField;
    QdocHousedata_stampa: TDateTimeField;
    sfldQdocHouseope_carico: TStringField;
    fldQdocHouseope_lettura: TIntegerField;
    fldQdocHouseope_stampa: TIntegerField;
    sfldQdocHousevisibile: TStringField;
    sfldQdocHousevisibile_collab: TStringField;
    fldQdocHouseID_GEN_SLP_AGE: TIntegerField;
    mfldQdocHousenote: TMemoField;
    fldQdocHousecod_direzione: TIntegerField;
    fldQdocHouseCAT_DOC: TIntegerField;
    fldQdocHouseLIVELLO: TIntegerField;
    dtfldQdocHouseData_circolare: TDateField;
    QDocPrintHouse: TDBISAMQuery;
    QAggVisPrintHouse: TDBISAMQuery;
    QLoadDocument: TDBISAMQuery;
    QDocPrinthousePolizza: TDBISAMQuery;
    QcheckDataChiusuraContabile: TDBISAMQuery;
    QcheckNumeroTitoliIncassati: TDBISAMQuery;
    QClienti: TDBISAMQuery;
    Qquietanza: TDBISAMQuery;
    QDati_agenzia: TDBISAMQuery;
    frxDBquietanza: TfrxDBDataset;
    frxQuietanza: TfrxReport;
    QmoduliDisponibili: TDBISAMQuery;
    QStatisticheClienti: TDBISAMQuery;
    QStatisticheContratti: TDBISAMQuery;
    QStatisticheRami: TDBISAMQuery;
    frxClienti: TfrxReport;
    frxDBDsClienti: TfrxDBDataset;
    frxContratti: TfrxReport;
    frxDBDsContratti: TfrxDBDataset;
    frxDBDoggetti: TfrxDBDataset;
    QStatisticheAssicurati: TDBISAMQuery;
    RepAppendiceCliente: TfrxReport;
    QStampaScadenze: TDBISAMQuery;
    frxSequenza: TfrxReport;
    QUpdScadenza: TDBISAMQuery;
    jvmstrh: TJvMultiStringHolder;
    frxrRepInsoluti: TfrxReport;
    frxdbDsIncassi: TfrxDBDataset;
    frxIncassi: TfrxReport;
    frxPolizze: TfrxReport;
    frxDettagli: TfrxReport;
    frxDettagliPromoter: TfrxReport;
    QtipoStampe: TDBISAMQuery;
    QfcProvvisorioRiporti: TDBISAMQuery;
    frxQuietanziamenti: TfrxReport;
    frxdbDsQuietanziamenti: TfrxDBDataset;
    
    procedure QContrattiAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    function frxFoglioCassaUserFunction(const MethodName: string; var Params: Variant): Variant;
    procedure QdocHouseCalcFields(DataSet: TDataSet);
    function frxQuietanzaUserFunction(const MethodName: string; var Params: Variant): Variant;

  private
    { Private declarations }
    PDFFolderPath: string;
    PDFUrl: string;
    TitoloReport: string;
    ReportFileName: string;
    MostraPolizzeSospese: Boolean;
    RecDate: TRecDate;
    ActualReport: TfrxReport;

    function DataToSql(ADate: string): TRecDate;
    function hasDocuments: Boolean;
    function SQLDataToStr(ADateStr: string): string;

  const
    FMT_DATA_IN_FILENAME = 'yyyymmddhhmmss';
  public
    { Public declarations }
    codice_fc: integer;
    function hasDocumentsToShow: Boolean;
    procedure getDocuments(ADocDataset: TDataSet; ComingFromLogin: Boolean = false);
    procedure prepara_query_clienti_stampa(promoter, compagnia, profess, gruppo: integer;
      tipoCli, tipologia, citta, cap, provincia, ordinamento: string);

    procedure prepara_query_contratti_stampa(promoter, compagnia, ramo, tipoPolizza, strutAppoggio, convenzione,
      gruppo: integer; ordinamento, citta, cap, provincia, sospese, effettoDal, effettoAl, scadenzaDal, scadenzaAl,
      dataAnndal, dataAnnAl: string; garanzia, polizze_morte: Boolean);

    procedure PreparaQueryStatisticheClienti(AParameterList: TStrings);
    procedure PreparaQueryStatisticheContratti(AParameterList: TStrings; PolizzeMorte: Boolean);
    procedure PreparaQueryStatisticheTitoli(AParameterList: TStrings);
    procedure PreparaQueryStatisticheIncassi(AParameterList: TStrings; ATipoStampaIncassi: TipoStampaIncassi;
      bExportToExcel: Boolean);

    procedure AnteprimaStatisticheClienti;
    procedure AnteprimaStatisticheContratti(MostraOggInGaranzia: Boolean);
    procedure AnteprimaStatisticheTitoli;
    procedure AnteprimaStatisticheIncassi(AReport: TfrxReport);
    procedure mostraAnteprimaStatistica(AReport: TfrxReport);

    function fai_cb(p1, p2: string): string;

    function fai_cbq(pol: string; scad: TdateTime): string;
    function genera_cbFC(numero: string; num_pagine, pagina: integer; dal, al: TdateTime): string;
    function get_dati_pagamentoFC: string;
    function OpenPrinthousePolizza(NPolizza: string = ''): Boolean;
    procedure StampaAppendice(ABlobField: TBlobField; SavePath: string; NPolizza: string; NAppendice: integer;
      bPrintable: Boolean);
    function SalvaTempDocument(SavePath: string; ACodDoc: integer; bPrintable: Boolean; TipoDoc: TipoDocEnum): string;
    function StampaFoglioCassa(CodFoglioCassa: integer): string;
    procedure StampaFoglioCassaProvvisorio(dal, al: TdateTime);
    function PrepareReportAppendice(QAppendiceX: TDBISAMQuery; var AppRec: TShowAppendiceRec): string;
    function PrepareReportAppendiceCliente(QAppSLPCliente, QPolizzeCliente: TDBISAMQuery;
      var AppRec: TShowAppendiceRec): string;
    procedure ShowReportAppendice(const ReportFileName: string; bCanPrint: Boolean);
    procedure UpdDataVisualizzazioneDoc(ACodDoc: integer);
    procedure UpdDataStampaDoc(ACodDoc: integer);

    function checkDataChiusuraContabile(periodo_dal: TdateTime): Boolean;
    function contaTitoliIncassati(tipo_titolo: string): integer;

    function StampaQuietanza(CodQuietanza: integer): string;
    procedure stampaQuietanziamentiSLP(AAnno, AMese, codCollaboratore: integer; ATipoOrd: TipoOrdQuietanziamenti;
      ATipoCopieQuietanziamenti: TipoCopieQuietanziamenti);

    function OpenModuliDisponibili(indice: integer): Boolean;
    procedure AzzeraCampiReport(report: TfrxReport);
    procedure ExportReport(SavePath, AFileName: string; bPrintable: Boolean);
{$MESSAGE HINT 'Si puo mettere in overload'}
    procedure exportReportEDOC(report: TfrxReport; nomefile: string; bPrintable: Boolean);
    procedure openDocHouse(filtro: string);
    procedure ExportToExcelAndSend(ADataToExport: TDBISAMQuery);

    procedure ShowReportQuietanziamento;
  end;

  // var
  // DMDatiReport: TDMDatiReport;

implementation

uses libreria, System.IOUtils, MainModule, ServerModule, System.Variants, uniGUIApplication, UCodiciErroriPolizza,
  System.RegularExpressions, libSLP, UStampePDFViewer, UPolizzaExceptions, System.Math;
{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

function TDMDatiReport.checkDataChiusuraContabile(periodo_dal: TdateTime): Boolean;
begin
  QcheckDataChiusuraContabile.Close;
  QcheckDataChiusuraContabile.ParamByName('periodo_dal').AsDateTime := periodo_dal;
  QcheckDataChiusuraContabile.ParamByName('compagnia').AsInteger := 1; // per ora solo SLP
  QcheckDataChiusuraContabile.Open;
  Result := QcheckDataChiusuraContabile.IsEmpty;
end;

function TDMDatiReport.contaTitoliIncassati(tipo_titolo: string): integer;
begin
  QcheckNumeroTitoliIncassati.Close;
  QcheckNumeroTitoliIncassati.ParamByName('compagnia').AsInteger := 1; // SLP
  QcheckNumeroTitoliIncassati.ParamByName('tipo_titolo').AsString := tipo_titolo;
  QcheckNumeroTitoliIncassati.Open;
  Result := QcheckNumeroTitoliIncassati.FieldByName('numero').AsInteger;
end;

procedure TDMDatiReport.DataModuleCreate(Sender: TObject);
begin
  inherited;
  codice_fc     := 0;
  PDFUrl        := UniServerModule.FilesFolderURL + 'PDF/';
  PDFFolderPath := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  // DBaccessoBase.Directory := UniServerModule.PosizioneDati;
  frxQuietanza.AddFunction('function fai_cb(pol: string; scad: TdateTime): string');
end;

procedure TDMDatiReport.ExportReport(SavePath, AFileName: string; bPrintable: Boolean);
begin
  frxPDFExport1.FileName           := TPath.Combine(SavePath, AFileName);
  frxPDFExport1.ExportNotPrintable := not bPrintable;
  frxrprtPDF.Export(frxPDFExport1);

end;

procedure TDMDatiReport.exportReportEDOC(report: TfrxReport; nomefile: string; bPrintable: Boolean);
// var aPDFExport : TWPDF_FrPDFExport;
begin

  frxPDFExport1.FileName           := nomefile;
  frxPDFExport1.ExportNotPrintable := bPrintable;
  report.Export(frxPDFExport1);

  {
    WPDF_Start('SLP Assicurazioni Spa','A1r6_h22dm9h2q4hlg54@88FBD411');
    aPDFExport := TWPDF_FrPDFExport.Create(nil);
    try
    // Use a instance of TWPPDFPrinter1 which was dropped on the form
    aPDFExport.ShowDialog:=False;
    aPDFExport.PDFPrinter := WPPDFPrinter1;
    // Set a filename
    WPPDFPrinter1.Filename := nomefile;
    // Just for debugging
    // WPPDFPrinter1.AutoLaunch := true;
    // Prepare the report
    // frxReport1.PrepareReport(true);
    // and export it using our filter
    Report.Export( aPDFExport );
    finally
    aPDFExport.Free;
    end;
  }

  {
    // versione usando eDocEngine
    // impostare tutti gli *show* di gtPDFengine e gtFRExportInterface a false !!!
    gtPDFEngine1.FileName := nomefile;  //'C:\ExportFromFR';
    gtFRExportInterface1.Engine := gtPDFEngine1;
    // esporta il report in PDF
    gtFRExportInterface1.RenderDocument(report, False, True);
  }
end;

procedure TDMDatiReport.prepara_query_clienti_stampa(promoter, compagnia, profess, gruppo: integer;
  tipoCli, tipologia, citta, cap, provincia, ordinamento: string);
var
  TXTsql: TstringList;
begin
  TXTsql          := TstringList.create;
  QClienti.active := false;
  // Qclienti.SessionName := UniMainModule.DMdatiAge.SessioneAccesso.SessionName;
  QClienti.sql.Clear;

  TXTsql.Add('select * from clienti');
  TXTsql.Add(' where 1 = 1');

  if promoter <> -1 then
  begin
    TXTsql.Add(' and promoter = ' + IntToStr(promoter));
  end;
  if compagnia <> -1 then
  begin
    TXTsql.Add(' and compagnia = ' + IntToStr(compagnia));
  end;
  if profess <> -1 then
  begin
    TXTsql.Add(' and profession = ' + IntToStr(profess));
  end;
  if gruppo <> -1 then
  begin
    TXTsql.Add(' and gruppo = ' + IntToStr(gruppo));
  end;
  if tipoCli <> ('Tutti') then
  begin
    if tipoCli = ('Potenziali') then
    begin
      TXTsql.Add(' and tipo_cli = 0');
    end
    else
      if tipoCli = 'Attivi' then
      begin
        TXTsql.Add(' and tipo_cli > 0');
      end
      else
      begin
        TXTsql.Add(' and tipo_cli < 0');
      end;
  end;
  if tipologia <> 'Tutti' then
  begin
    if tipologia = 'Persone fisiche' then
    begin
      TXTsql.Add(' and tipo_pf_giuridica = ''P''');
    end
    else
    begin
      TXTsql.Add(' and tipo_pf_giuridica = ''G''');
    end;
  end;
  if not citta.Equals(string.Empty) then
  begin
    TXTsql.Add(' and citta = ' + QuotedStr(citta.ToUpper));
  end;
  if not cap.Equals(string.Empty) then
  begin
    TXTsql.Add(' and cap = ' + QuotedStr(cap));
  end;
  if not provincia.Equals(string.Empty) then
  begin
    TXTsql.Add(' and provincia = ' + QuotedStr(provincia.ToUpper));
  end;
  if not ordinamento.Equals(string.Empty) then
  begin
    TXTsql.Add(' order by ' + ordinamento);
  end;

  QClienti.sql := TXTsql;
  QClienti.Open;
end;

procedure TDMDatiReport.prepara_query_contratti_stampa(promoter, compagnia, ramo, tipoPolizza, strutAppoggio,
  convenzione, gruppo: integer; ordinamento, citta, cap, provincia, sospese, effettoDal, effettoAl, scadenzaDal,
  scadenzaAl, dataAnndal, dataAnnAl: string; garanzia, polizze_morte: Boolean);
var
  tSql: TstringList;
begin
  tSql := TstringList.create;
  try
    tSql.Add('select n_polizza, contraente, decorrenza, scadenza, frazionam, lordo, compagni.codice_agenzia,');
    tSql.Add(' rami.descrizione as Ramo, rami.sigla as SiglaR,');
    tSql.Add(' tipo_pol.descrizione as "Tipo pol.", cod_polizza, data_morte, appoggio.nome, sospeso_dal, sospeso_al');
    tSql.Add(' from polizze');
    tSql.Add(' LEFT OUTER JOIN clienti ON clienti.cod_cliente=polizze.cod_cli');
    tSql.Add(' LEFT OUTER JOIN rami ON rami.cod_ramo=polizze.rif_cod_ramo');
    tSql.Add(' LEFT OUTER JOIN tipo_pol ON tipo_pol.cod_tipo_pol=polizze.rif_tipo_pol');
    tSql.Add(' LEFT OUTER JOIN compagni ON compagni.cod_compagnia=polizze.rif_compagnia');
    tSql.Add(' LEFT OUTER JOIN appoggio ON appoggio.cod_appoggio=polizze.appoggiata_a');
    // tsql.Add('where stato<>''C''');
    if polizze_morte then
      tSql.Add('where stato=''C''')
    else
      tSql.Add('where stato=''V''');

    // filtro su collaboratore
    if promoter <> -1 then
    begin
      tSql.Add(' and  (fatta_da = ' + IntToStr(promoter));
      tSql.Add(' or clienti.promoter = ' + IntToStr(promoter) + ')');
      // tsql.Add('   or ifnull(fatta_da,1=1,1=0)');
    end;

    if not sospese.Equals('T') then
    begin
      if sospese.Equals('S') then
      begin
        // elenca solo polizze sospese
        tSql.Add(' and not (sospeso_dal = null or (sospeso_dal <> null and sospeso_al <= current_date))');
      end
      else
      begin
        // elenca solo polizze NON sospese
        tSql.Add(' and (sospeso_dal = null or (sospeso_dal <> null and sospeso_al <= current_date))');
      end;
    end;

    if not citta.Equals(string.Empty) then
    begin
      // filtro sulla citta
      tSql.Add(' and clienti.citta = ' + QuotedStr(citta));
    end;

    if not cap.Equals(string.Empty) then
    begin
      // filtro sul cap
      if Length(cap) = 5 then
        tSql.Add(' and clienti.cap = ' + QuotedStr(cap))
      else
        tSql.Add(' and left(clienti.cap, ' + IntToStr(Length(cap)) + ') = ' + QuotedStr(cap));
    end;

    if not provincia.Equals(string.Empty) then
    begin
      // filtro sulla ptovincia
      tSql.Add(' and clienti.provincia=' + QuotedStr(provincia));
    end;

    if (not effettoDal.Equals(string.Empty)) and (not effettoAl.Equals(string.Empty)) then
    begin
      if ((YearOf(StrToDate(effettoDal)) > 1980) and (YearOf(StrToDate(effettoAl)) > 1980)) then
      begin
        if (StrToDate(effettoDal) <= StrToDate(effettoAl)) then
        begin
          tSql.Add(' and decorrenza >= ' + QuotedStr(DtoSql(StrToDate(effettoDal))) + ' and decorrenza <= ' +
            QuotedStr(DtoSql(StrToDate(effettoAl))));
        end;
      end;
    end;

    if (not scadenzaDal.Equals(string.Empty)) and (not scadenzaAl.Equals(string.Empty)) then
    begin
      if ((YearOf(StrToDate(scadenzaDal)) > 1980) and (YearOf(StrToDate(scadenzaAl)) > 1980)) then
      begin
        if (StrToDate(scadenzaDal) <= StrToDate(scadenzaAl)) then
        begin
          tSql.Add(' and decorrenza >= ' + QuotedStr(DtoSql(StrToDate(scadenzaDal))) + ' and decorrenza <= ' +
            QuotedStr(DtoSql(StrToDate(scadenzaAl))));
        end;
      end;
    end;

    if (not dataAnndal.Equals(string.Empty)) and (not dataAnnAl.Equals(string.Empty)) then
    begin
      if ((YearOf(StrToDate(dataAnndal)) > 1980) and (YearOf(StrToDate(dataAnnAl)) > 1980)) then
      begin
        if (StrToDate(dataAnndal) <= StrToDate(dataAnnAl)) then
        begin
          tSql.Add(' and data_morte >= ' + QuotedStr(DtoSql(StrToDate(dataAnndal))) + ' and data_morte <= ' +
            QuotedStr(DtoSql(StrToDate(dataAnnAl))));
        end;
      end;
    end;

    // filtro su ramo
    if ramo <> -1 then
    begin
      tSql.Add(' and polizze.rif_cod_ramo = ' + IntToStr(ramo));
    end;

    if gruppo <> -1 then
    begin
      tSql.Add(' and clienti.gruppo = ' + IntToStr(gruppo));
    end;

    if convenzione <> -1 then
    begin
      tSql.Add(' and polizze.convenzione = ' + IntToStr(convenzione));
    end;

    // filtro su tipo polizza
    if tipoPolizza <> -1 then
    begin
      tSql.Add(' and polizze.rif_tipo_pol = ' + IntToStr(tipoPolizza));
    end
    else
    begin
      // filtro su compagnia
      if compagnia <> -1 then
      begin
        // filtro su ramo
        tSql.Add(' and polizze.rif_compagnia = ' + IntToStr(compagnia));
      end;
    end;

    if strutAppoggio <> -1 then
    begin
      tSql.Add(' and polizze.appoggiata_a = ' + IntToStr(strutAppoggio));
    end;

    // ordinamento
    if not ordinamento.Equals(string.Empty) then
      tSql.Add(' order by ' + ordinamento);

    UniMainModule.DMdatiAgeClienti.QAssicurati.Close;
    UniMainModule.DMdatiAgeClienti.QAssicurati.Prepare;
    QContratti.sql.Clear;
    QContratti.sql := tSql;
    QContratti.Open;
  finally
    tSql.free;
    // Tassicurati.close;
    QContratti.Close;
  end;
end;

procedure TDMDatiReport.QContrattiAfterScroll(DataSet: TDataSet);
begin
  UniMainModule.DMdatiAgeClienti.QAssicurati.Close;
  UniMainModule.DMdatiAgeClienti.QAssicurati.ParamByName('cod_polizza').AsInteger :=
    QContratti.FieldByName('cod_polizza').AsInteger;
  UniMainModule.DMdatiAgeClienti.QAssicurati.Open;
end;

procedure TDMDatiReport.QdocHouseCalcFields(DataSet: TDataSet);
var
  ImagesPath: string;
  imgFileName: string;
begin
  ImagesPath := 'Immagini';
  if QdocHouse.FieldByName('DATA_LETTURA').IsNull and QdocHouse.FieldByName('DATA_STAMPA').IsNull then
    imgFileName := TPath.Combine(ImagesPath, 'flag_mark_red.png')
  else
    imgFileName := TPath.Combine(ImagesPath, 'flag_mark_green.png');

  (QdocHouse.FieldByName('IMGDOCSTATE') as TBlobField).LoadFromFile(imgFileName);
end;

function TDMDatiReport.SalvaTempDocument(SavePath: string; ACodDoc: integer; bPrintable: Boolean;
  TipoDoc: TipoDocEnum): string;
var
  DocStream: TStream;
begin

  frxrprtPDF.EngineOptions.SilentMode := True;
  frxrprtPDF.EngineOptions.EnableThreadSafe := True;
  frxrprtPDF.EngineOptions.DestroyForms := False;
  frxrprtPDF.EngineOptions.UseGlobalDataSetList := False;

  QLoadDocument.Close;
  QLoadDocument.sql.Clear;
  if TipoDoc = tdDocHouse then
  begin
    QLoadDocument.sql.Append('SELECT CODICE_DOC, TIPO_DOC, DESCRIZIONE, DATA_DOC, TESTO');
    QLoadDocument.sql.Append('FROM DOCHOUSE WHERE CODICE_DOC = :CODICE_DOC');
    QLoadDocument.ParamByName('CODICE_DOC').AsInteger := ACodDoc;
    QLoadDocument.Open;
    Result := '';
    if not QLoadDocument.IsEmpty then
    begin

      DocStream := QLoadDocument.CreateBlobStream(QLoadDocument.FieldByName('TESTO'), bmRead);
      try
        DocStream.Position := 0;

        frxrprtPDF.PreviewPages.LoadFromStream(DocStream);

        Result := QLoadDocument.FieldByName('CODICE_DOC').AsString + '_' +
          Trim(QLoadDocument.FieldByName('DESCRIZIONE').AsString) + '_' + FormatDateTime('ddmmyyyhhmmss',
          QLoadDocument.FieldByName('DATA_DOC').AsDateTime);
        Result := TRegEx.Replace(Result, '\W+', ''); // elimina caratteri speciali
        Result := ChangeFileExt(Result, '.pdf');

        // frxPDFExport1.FileName           := stringreplace(TPath.Combine(SavePath, Result), '%', 'p', [rfReplaceAll]);
        // frxPDFExport1.ExportNotPrintable := not bPrintable;
        // frxrprtPDF.Export(frxPDFExport1);

        exportReportEDOC(frxrprtPDF, stringreplace(TPath.Combine(SavePath, Result), '%', 'p', [rfReplaceAll]),
          not bPrintable);

      finally
        DocStream.free;
      end;

      // Result := '../Reprint_Samples.pdf';
    end;
    QLoadDocument.Close;
  end
  else
    if TipoDoc = tdPrintHouse then
    begin
      Result := '';
      QLoadDocument.sql.Append('SELECT CODICE, TIPO_DOC, N_POLIZZA, DATA_DOC, DOC');
      QLoadDocument.sql.Append('FROM PRINTHOUSE WHERE CODICE = :CODICE');
      QLoadDocument.ParamByName('CODICE').AsInteger := ACodDoc;
      QLoadDocument.Open;
      if not QLoadDocument.IsEmpty then
      begin
        DocStream := QLoadDocument.CreateBlobStream(QLoadDocument.FieldByName('DOC'), bmRead);
        try
          DocStream.Position := 0;
          // frxPDFExport1.FileName           := stringreplace(TPath.Combine(SavePath, Result), '%', 'p', [rfReplaceAll]);
          // frxPDFExport1.ExportNotPrintable := not bPrintable;
          // frxrprtPDF.PreviewPages.Export(frxPDFExport1);

          Result := QLoadDocument.FieldByName('CODICE').AsString + '_' +
            Trim(QLoadDocument.FieldByName('N_POLIZZA').AsString) + '_' + FormatDateTime('ddmmyyyhhmmss',
            QLoadDocument.FieldByName('DATA_DOC').AsDateTime);
          Result := TRegEx.Replace(Result, '\W+', ''); // elimina caratteri speciali
          Result := ChangeFileExt(Result, '.pdf');

          // se si tratta di uno stream di pdf allora procedi solo al salvataggio nel file temporaneao per la visuazlizzazione
          if (Copy(QLoadDocument.FieldByName('TIPO_DOC').AsString, 1, 3) = 'PDF') then
          begin
            TBlobField(QLoadDocument.FieldByName('DOC')).savetofile(stringreplace(TPath.Combine(SavePath, Result), '%',
              'p', [rfReplaceAll]));
          end
          else
          begin
            // se si tratta di un stream di preview fastreport esegui la trasformazione in pdf
            frxrprtPDF.PreviewPages.LoadFromStream(DocStream);

            exportReportEDOC(frxrprtPDF, stringreplace(TPath.Combine(SavePath, Result), '%', 'p', [rfReplaceAll]),
              not bPrintable);
          end;
        finally
          DocStream.free;
        end;
      end;
      QLoadDocument.Close;
    end
    else
      if TipoDoc = tdModuli then
      begin
        Result := '';
        QLoadDocument.sql.Append('SELECT COD_STAMPA, DESCRIZIONE, TESTO, TIPO, CODICE_SLP');
        QLoadDocument.sql.Append('FROM MOD_SLP WHERE COD_STAMPA = :CODICE');
        QLoadDocument.ParamByName('CODICE').AsInteger := ACodDoc;
        QLoadDocument.Open;
        if not QLoadDocument.IsEmpty then
        begin
          DocStream := QLoadDocument.CreateBlobStream(QLoadDocument.FieldByName('TESTO'), bmRead);
          try

            DocStream.Position := 0;
            if (Copy(QLoadDocument.FieldByName('CODICE_SLP').AsString,1,3)='SET') then
            begin
               frxrprtPDF.PreviewPages.LoadFromStream(DocStream);
               frxrprtPDF.PrintOptions.Copies := 1;
            end else begin
               frxrprtPDF.LoadFromStream(DocStream);
               AzzeraCampiReport(frxrprtPDF);
               frxrprtPDF.PrintOptions.Copies := 1;

               if (QLoadDocument.FieldByName('tipo').AsString = 'P') then
               begin
                 if frxrprtPDF.Variables.IndexOf('facsimile') > -1 then
                   frxrprtPDF.Variables['facsimile'] := QuotedStr('S');
                 if frxrprtPDF.Variables.IndexOf('polizza') > -1 then
                   frxrprtPDF.Variables['polizza'] := QuotedStr('Fac-Simile');
                 if frxrprtPDF.Variables.IndexOf('agenzia') > -1 then
                   frxrprtPDF.Variables['agenzia'] := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia);
                 if frxrprtPDF.Variables.IndexOf('citta_age') > -1 then
                   frxrprtPDF.Variables['citta_age'] := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.citta);
               end;

               frxrprtPDF.PrepareReport(true);
            end;

            if (QLoadDocument.FieldByName('TIPO').AsString = 'P') then
            begin
              if frxrprtPDF.Variables.IndexOf('facsimile') > -1 then
                frxrprtPDF.Variables['facsimile'] := QuotedStr('S');
              if frxrprtPDF.Variables.IndexOf('polizza') > -1 then
                frxrprtPDF.Variables['polizza'] := QuotedStr('Fac-Simile');
              if frxrprtPDF.Variables.IndexOf('agenzia') > -1 then
                frxrprtPDF.Variables['agenzia'] := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia);
              if frxrprtPDF.Variables.IndexOf('citta_age') > -1 then
                frxrprtPDF.Variables['citta_age'] := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.citta);
            end;

            // frxrprtPDF.PrepareReport(true);
            Result := 'MOD_' + QLoadDocument.FieldByName('COD_STAMPA').AsString + '_' +
              FormatDateTime(FMT_DATA_IN_FILENAME, Now);
            Result := TRegEx.Replace(Result, '\W+', ''); // elimina caratteri speciali
            Result := ChangeFileExt(Result, '.pdf');
            // frxPDFExport1.FileName := stringreplace(TPath.Combine(SavePath, Result), '%', 'p', [rfReplaceAll]);
            // frxPDFExport1.ExportNotPrintable := not bPrintable;
            // frxrprtPDF.Export(frxPDFExport1);
            exportReportEDOC(frxrprtPDF, stringreplace(TPath.Combine(SavePath, Result), '%', 'p', [rfReplaceAll]),
              not bPrintable);
          finally
            DocStream.free;
          end;
        end;
        QLoadDocument.Close;
      end
      else
        if TipoDoc = tdAppendice then
        begin
          // recupera l'appendice di variazione che ti serve !!!
          Result := '';
          QLoadDocument.sql.Append
            ('SELECT COD_APPENDICE AS CODICE, TIPO_APPENDICE, N_POLIZZA, DATA_EMISSIONE AS DATA_DOC, REPORT AS DOC');
          QLoadDocument.sql.Append('FROM APPENDICI WHERE COD_APPENDICE = :CODICE');
          QLoadDocument.ParamByName('CODICE').AsInteger := ACodDoc;
          QLoadDocument.Open;
          if not QLoadDocument.IsEmpty then
          begin
            DocStream := QLoadDocument.CreateBlobStream(QLoadDocument.FieldByName('DOC'), bmRead);
            try
              DocStream.Position := 0;
              frxrprtPDF.PreviewPages.LoadFromStream(DocStream);

              Result := QLoadDocument.FieldByName('CODICE').AsString + '_' +
                Trim(QLoadDocument.FieldByName('N_POLIZZA').AsString) + '_' + FormatDateTime('ddmmyyyhhmmss',
                QLoadDocument.FieldByName('DATA_DOC').AsDateTime);
              Result := TRegEx.Replace(Result, '\W+', ''); // elimina caratteri speciali
              Result := ChangeFileExt(Result, '.pdf');

              exportReportEDOC(frxrprtPDF, stringreplace(TPath.Combine(SavePath, Result), '%', 'p', [rfReplaceAll]),
                not bPrintable);

            finally
              DocStream.free;
            end;
          end;
          QLoadDocument.Close;
        end;

end;

procedure TDMDatiReport.ShowReportAppendice(const ReportFileName: string; bCanPrint: Boolean);
begin
  TFPDFViewer.ShowReport(uniApplication, ReportFileName, UniServerModule.FilesFolderURL + 'PDF/',
    TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'), bCanPrint, bCanPrint);

end;

procedure TDMDatiReport.ShowReportQuietanziamento;
var
  ReportFileName: string;
begin
  frxQuietanziamenti.PrepareReport(True);
  ReportFileName := 'QUIET_' + '_' + FormatDateTime(FMT_DATA_IN_FILENAME, Now) + '.PDF';
  frxPDFExport1.FileName := TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'), ReportFileName);

  frxQuietanziamenti.Export(frxPDFExport1);

  TFPDFViewer.ShowReport(uniApplication, ReportFileName, UniServerModule.FilesFolderURL + 'PDF/',
    TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'), True, True);

end;

function TDMDatiReport.SQLDataToStr(ADateStr: string): string;
var
  lDate: TDate;
  YYYY, MM, DD: word;
begin
  YYYY  := StrToInt(Copy(ADateStr, 1, 4));
  MM    := StrToInt(Copy(ADateStr, 6, 2));
  DD    := StrToInt(Copy(ADateStr, 9, 2));
  lDate := encodeDate(YYYY, MM, DD);
  DateTimeToString(Result, 'dd/mm/yyyy', lDate);
end;

function TDMDatiReport.PrepareReportAppendice(QAppendiceX: TDBISAMQuery; var AppRec: TShowAppendiceRec): string;
var
  SLPdati_age: TSLPdati_age_rec;
  // tasse, tasseR: Currency;
  tasseR: Currency;
  ReportFileName, rfn: string;
begin
  SLPdati_age := UniMainModule.DMdatiAge.SLPdati_age;

  QAppendice.Close;
  QAppendice.ParamByName('COD_POLIZZA').AsInteger := AppRec.CodPolizza;
  QAppendice.ParamByName('DATA1').AsDateTime      := AppRec.DtDalAppendice;
  QAppendice.ParamByName('DATA2').AsDateTime      := AppRec.DtAlAppendice;
  QAppendice.Open;

  QFindCliente.Close;
  QFindCliente.ParamByName('CODCLIENTE').AsInteger := AppRec.CodCliente;
  QFindCliente.Open;

  Rep_appendice.Variables['tipo_a'] := QuotedStr('ACO'); // appendice cambio oggetto in garanzia
  Rep_appendice.Variables['titolo'] := QuotedStr(uppercase(dai_titolo(QFindCliente.FieldByName('COD_FISC').AsString)));
  Rep_appendice.Variables['indirizzo'] := QuotedStr(QFindCliente.FieldByName('INDIRIZZO').AsString);
  Rep_appendice.Variables['nome']  := QuotedStr(QFindCliente.FieldByName('DENOMINAZ').AsString);
  Rep_appendice.Variables['citta'] := QuotedStr(QFindCliente.FieldByName('CAP').AsString + ' - ' +
    QFindCliente.FieldByName('CITTA').AsString + ' (' + QFindCliente.FieldByName('PROVINCIA').AsString + ')');

  Rep_appendice.Variables['intestazione'] := QuotedStr('A POLIZZA N. ' + AppRec.NPolizza + ' RAMO 17   AGENZIA ' +
    SLPdati_age.citta + ' ' + 'COD. ' + SLPdati_age.agenzia + '\' + SLPdati_age.sub_age);
  Rep_appendice.Variables['numero'] := QuotedStr(AppRec.NAppendice.ToString);
  Rep_appendice.Variables['data']   := QuotedStr(dtoc(AppRec.DataAppendice));

  AppRec.tasse := AppRec.Importo - (AppRec.Importo / (1 + (AppRec.PTasse / 100)));
  tasseR       := AppRec.NextRata - (AppRec.NextRata / (1 + (AppRec.PTasse / 100)));

  Rep_appendice.Variables['importo'] := QuotedStr(FormatCurr('##,##0.00', AppRec.Importo));
  if AppRec.ImportoModificato then
  begin
    Rep_appendice.Variables['riga1'] :=
      QuotedStr('Modificandosi il rischio il ' + 'premio viene modificato come segue:');
    Rep_appendice.Variables['riga2'] := QuotedStr('- alla firma della presente ' + 'appendice � ' +
      FormatCurr('##,##0.00', AppRec.Importo) + ' (di cui tasse � ' + FormatCurr('##,##0.00', AppRec.tasse) + ')');
    Rep_appendice.Variables['riga3'] := QuotedStr('- alle rate ' + 'successive � ' + FormatCurr('##,##0.00',
      AppRec.NextRata) + ' (di cui tasse ' + FormatCurr('##,##0.00', tasseR) + ' � )');
  end
  else
  begin
    Rep_appendice.Variables['riga1'] := QuotedStr('Non modificandosi il rischio il ' + 'premio resta invariato:');
    Rep_appendice.Variables['riga2'] := QuotedStr('- alla firma della presente ' + 'appendice � ' +
      FormatCurr('##,##0.00', AppRec.Importo) + ' (di cui tasse � ' + FormatCurr('##,##0.00', AppRec.tasse) + ')');
    Rep_appendice.Variables['riga3'] := QuotedStr('');
    // Rep_appendice.Variables['riga3']:=quotedstr('- alle rate ' +
    // 'successive � '+formatfloat('0.00',Enext_rata.value)+
    // ' (di cui tasse ' + formatfloat('0.00',tasseR)+' � )');
  end;
  Rep_appendice.Variables['barcode'] := QuotedStr(fai_cbq(AppRec.NPolizza, AppRec.DataAppendice));

  Rep_appendice.PrepareReport(true);

  rfn := 'APP_' + AppRec.NPolizza + '_' + FormatDateTime(FMT_DATA_IN_FILENAME, Now) ;
  ReportFileName := rfn + '.FR3';

  Rep_appendice.PreviewPages.savetofile(TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'),
    ReportFileName));
  ReportFileName := rfn + '.PDF';
  exportReportEDOC(Rep_appendice, TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'), ReportFileName),
    not AppRec.bCanPrint);

  Result := ReportFileName;
end;


function TDMDatiReport.PrepareReportAppendiceCliente(QAppSLPCliente, QPolizzeCliente: TDBISAMQuery;
  var AppRec: TShowAppendiceRec): string;
var SLPdatiAge: TSLPdati_age_rec;
    rfn: string;

  function getStringaPolizze: string;
  var
    Suffix: string;
  begin
    Suffix := '';
    Result := '';
    QPolizzeCliente.First;
    while not QPolizzeCliente.Eof do
    begin
      Result := Result + Suffix + QPolizzeCliente.FieldByName('N_POLIZZA').AsString;
      Suffix := ' - ';
      QPolizzeCliente.Next;
    end;
  end;

begin
  SLPdatiAge                              := UniMainModule.DMdatiAge.SLPdati_age;
  RepAppendiceCliente.Variables['tipo_a'] := QuotedStr('ACI'); // cambio indirizzo
  RepAppendiceCliente.Variables['titolo'] :=
    QuotedStr(uppercase(dai_titolo(QAppSLPCliente.FieldByName('COD_FISC').AsString)));
  RepAppendiceCliente.Variables['indirizzo'] := QuotedStr(QAppSLPCliente.FieldByName('INDIRIZZO').OldValue);
  RepAppendiceCliente.Variables['nome']      := QuotedStr(QAppSLPCliente.FieldByName('DENOMINAZ').OldValue);
  RepAppendiceCliente.Variables['citta']     := QuotedStr(QAppSLPCliente.FieldByName('CAP').OldValue + '-' +
    QAppSLPCliente.FieldByName('CITTA').OldValue + ' (' + QAppSLPCliente.FieldByName('PROVINCIA').OldValue + ')');

  RepAppendiceCliente.Variables['intestazione'] := quotedstr('A POLIZZA N. ' + getStringaPolizze +
                  ' - RAMO 17 - AGENZIA ' + SLPdatiAge.citta + ' ' + 'COD. ' +
                  SLPdatiAge.agenzia + '\' + SLPdatiAge.sub_age)  ;

  // RepAppendiceCliente.Variables['numero'] := QuotedStr('');
  RepAppendiceCliente.Variables['data'] := QuotedStr(dtoc(AppRec.DataAppendice));

  RepAppendiceCliente.Variables['riga1'] := QuotedStr('Non modificandosi il rischio il ' + 'premio resta invariato:');


  // RepAppendiceCliente.Variables['riga3']:=quotedstr('');

  RepAppendiceCliente.Variables['new_indirizzo'] := QuotedStr(QAppSLPCliente.FieldByName('INDIRIZZO').newValue);
  RepAppendiceCliente.Variables['new_citta']     := QuotedStr(QAppSLPCliente.FieldByName('CAP').newValue + '  ' +
    alltrim(QAppSLPCliente.FieldByName('citta').newValue) + '  (' + QAppSLPCliente.FieldByName('PROVINCIA')
    .newValue + ')');
  RepAppendiceCliente.Variables['xbarcode'] := QuotedStr('00');
  RepAppendiceCliente.PrepareReport(true);

  {
  ReportFileName := 'APP_' + AppRec.CodCliente.ToString + '_' + FormatDateTime(FMT_DATA_IN_FILENAME, Now) + '.PDF';

  exportReportEDOC(RepAppendiceCliente, TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'),
    ReportFileName), not AppRec.bCanPrint);
  }

  // mb 11112020
  rfn := 'APP_' + AppRec.CodCliente.ToString + '_' + FormatDateTime(FMT_DATA_IN_FILENAME, Now);
  ReportFileName := rfn + '.FR3';
  RepAppendiceCliente.PreviewPages.savetofile(TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'),
    ReportFileName));
  ReportFileName := rfn + '.PDF';

  exportReportEDOC(RepAppendiceCliente, TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'),
    ReportFileName), not AppRec.bCanPrint);

  Result := ReportFileName;

end;

procedure TDMDatiReport.StampaAppendice(ABlobField: TBlobField; SavePath: string; NPolizza: string; NAppendice: integer;
  bPrintable: Boolean);
// var
// AppStream: TStream;
begin
{$MESSAGE WARN 'Verificare se ancora da implementare'}
  // try
  // AppStream.Position := 0;
  // frxrprtPDF.PreviewPages.LoadFromStream(AppStream);
  //
  // Result := QLoadDocument.FieldByName('CODICE_DOC').AsString + '_' +
  // Trim(QLoadDocument.FieldByName('DESCRIZIONE').AsString) + '_' +
  // FormatDateTime('ddmmyyy', QLoadDocument.FieldByName('DATA_DOC').AsDateTime);
  // Result                           := ChangeFileExt(Result, '.pdf');
  // frxPDFExport1.FileName           := stringreplace(TPath.combine(SavePath, Result), '%', 'p', [rfReplaceAll]);
  // frxPDFExport1.ExportNotPrintable := not bPrintable;
  // frxrprtPDF.Export(frxPDFExport1);
  //
  // finally
  // AppStream.free;
  // end;

end;

function TDMDatiReport.StampaFoglioCassa(CodFoglioCassa: integer): string;
var
  nomeRep: string;
  SLPdatiAge: TSLPdati_age_rec;
  Tt: string;
begin
  Result := '';
  Sleep(500);
  frxFoglioCassa.AddFunction('function fai_cb(p1,p2: string): string');
  // TXTsql := QChiusuraContabile_FCProvvisorio.sql;
  QFogliCassa.Open;

  UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.Close;
  UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.ParamByName('codice_foglio').AsInteger := CodFoglioCassa;
  UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.Open;

  QChiusuraContabile_FCProvvisorio.Close;
  QChiusuraContabile_FCProvvisorio.sql.Clear;

  QChiusuraContabile_FCProvvisorio.sql.Add('select ');
  QChiusuraContabile_FCProvvisorio.sql.Add
    ('scadenze.progressivo, scadenze.decorrenza, scadenze.data_pag, scadenze.n_polizza,scadenze.denominaz, ');
  QChiusuraContabile_FCProvvisorio.sql.Add('scadenze.lordo lordo, scadenze.provvigioni, ');

  QChiusuraContabile_FCProvvisorio.sql.Add
    ('compagni.nome, compagni.codice_agenzia, clienti.promoter, scadenze.cod_scadenza, ' +
    'compagni.cod_compagnia , tipo_titolo, valuta, data_reg_pag, produtt.sigla as sigla_prod, scadenze.provvigioni_sub, polizze.inpdf ');
  QChiusuraContabile_FCProvvisorio.sql.Add('from ');
  QChiusuraContabile_FCProvvisorio.sql.Add('s_scadenze as scadenze');
  QChiusuraContabile_FCProvvisorio.sql.Add
    ('LEFT OUTER JOIN compagni ON compagni.cod_compagnia = scadenze.rif_cod_compagnia ');
  QChiusuraContabile_FCProvvisorio.sql.Add
    ('LEFT OUTER JOIN polizze  ON polizze.cod_polizza = scadenze.rif_cod_polizza ');
  QChiusuraContabile_FCProvvisorio.sql.Add('LEFT OUTER JOIN clienti  ON clienti.cod_cliente = polizze.cod_cli ');
  QChiusuraContabile_FCProvvisorio.sql.Add('LEFT OUTER JOIN appoggio ON polizze.appoggiata_a = appoggio.cod_appoggio ');
  QChiusuraContabile_FCProvvisorio.sql.Add('LEFT OUTER JOIN produtt ON clienti.promoter = produtt.cod_produttore ');
  QChiusuraContabile_FCProvvisorio.sql.Add('where ');
  // filtro sul foglio cassa ...
  QChiusuraContabile_FCProvvisorio.sql.Add(' scadenze.foglio_cassa = ' + CodFoglioCassa.ToString);
  QChiusuraContabile_FCProvvisorio.sql.Add(' ORDER BY DATA_PAG');

  QChiusuraContabile_FCProvvisorio.Open;

  UniMainModule.DMdatiAge.QCompagnie.Open;
  try
    if QFogliCassa.FieldByName('cod_compagnia').AsInteger = 1 then
    begin
      SLPdatiAge := UniMainModule.DMdatiAge.SLPdati_age;
      // frxFoglioCassa.EngineOptions.TempDir:=dati_gen.dir_loc;
      frxFoglioCassa.EngineOptions.TempDir := SLPdatiAge.dati_gen.dir_temp;
      // dati_gen.dir_loc; modificato 07/10/2013
      frxFoglioCassa.Variables['versione'] := QuotedStr('Rv. web');
      // + LeggiVersione);

      frxFoglioCassa.Variables['data_chiusura'] :=
        QuotedStr(dateTimetostr(UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.FieldByName('data_chiusura').AsDateTime));
      Tt := 'Foglio cassa n� ';
      UniMainModule.DMdatiAge.QCompagnie.Locate('cod_compagnia', 1, []);

      frxFoglioCassa.Variables['FoglioCassa'] :=
        QuotedStr(Tt + UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.FieldByName('numero_fc').AsString + ' ');
      frxFoglioCassa.Variables['agenzia'] := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia + '\' +
        UniMainModule.DMdatiAge.SLPdati_age.sub_age + '  ' + UniMainModule.DMdatiAge.SLPdati_age.citta);
      frxFoglioCassa.Variables['age']     := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia);
      frxFoglioCassa.Variables['sub']     := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.sub_age);
      frxFoglioCassa.Variables['Periodo'] :=
        QuotedStr('periodo dal ' + UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.FieldByName('periodo_dal').AsString +
        ' al ' + UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.FieldByName('periodo_al').AsString);
      frxFoglioCassa.Variables['provvisorio'] := QuotedStr('N');
      frxFoglioCassa.Variables['IBAN']        := QuotedStr(UniMainModule.DMdatiAge.get_dati_pagamentoFC);

      nomeRep := UniMainModule.DMdatiAgeTitoli.QgetFoglioCassa.FieldByName('numero_fc').AsString + '-' +
        FormatDateTime('yyyymmddhhmmss', Now);
      nomeRep := UniMainModule.DMdatiAge.SLPdati_age.agenzia + '-FCd-' + nomeRep + '.pdf';

      frxPDFExport1.FileName := stringreplace(TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'),
        nomeRep), '%', 'p', [rfReplaceAll]);

      // frxPDFExport1.FileName := SLPdatiAge.dati_gen.dir_temp + Trim(nomeRep);

      NullStrictConvert := false;
      // Self.LockOnSubmit := True;
      frxFoglioCassa.PrepareReport(true);

      // frxFoglioCassa.Export(frxPDFExport1);
      Result := frxPDFExport1.FileName;

      NullStrictConvert := true;
      // Self.LockOnSubmit := False;

      exportReportEDOC(frxFoglioCassa, stringreplace(TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath,
        'PDF'), nomeRep), '%', 'p', [rfReplaceAll]), false);

    end;
  finally
    QChiusuraContabile_FCProvvisorio.Close;
    // TXTsql.free;
  end;

end;

procedure TDMDatiReport.StampaFoglioCassaProvvisorio(dal, al: TdateTime);
var
  SLPdatiAge: TSLPdati_age_rec;
  Tt: string;
  nomeReport: string;
begin
  Sleep(500);
  frxFoglioCassa.AddFunction('function fai_cb(p1,p2: string): string');

  QFogliCassa.Open;
  UniMainModule.DMdatiAge.QProdut.Open;
  UniMainModule.DMdatiAge.QProdut.Locate('cod_produttore', UniMainModule.utentePromoter, []);

  QChiusuraContabile_FCProvvisorio.Close;
  QChiusuraContabile_FCProvvisorio.sql.Clear;

  QChiusuraContabile_FCProvvisorio.sql.Add('select ');
  QChiusuraContabile_FCProvvisorio.sql.Add
    ('scadenze.progressivo, scadenze.decorrenza, scadenze.data_pag, scadenze.n_polizza,scadenze.denominaz, ');
  // QChiusuraContabile_FCProvvisorio.sql.add('      round(scadenze.lordo,2) lordo, scadenze.provvigioni, ');
  if UniMainModule.DMdatiAge.is_utente_promoter then
    QChiusuraContabile_FCProvvisorio.sql.Add('scadenze.lordo lordo, (scadenze.provvigioni / 100*' +
      floatToStr(UniMainModule.DMdatiAge.QProdut.FieldByName('perc_provv_age').AsCurrency) + ') as provvigioni, ')
  else
    QChiusuraContabile_FCProvvisorio.sql.Add('scadenze.lordo lordo, scadenze.provvigioni, ');
  if UniMainModule.DMdatiAge.is_utente_promoter then
    QChiusuraContabile_FCProvvisorio.sql.Add('scadenze.cod_fc_sub_age, ');

  QChiusuraContabile_FCProvvisorio.sql.Add
    ('compagni.nome, compagni.codice_agenzia, clienti.promoter, scadenze.cod_scadenza, ' +
    'compagni.cod_compagnia , tipo_titolo, valuta, data_reg_pag, produtt.sigla as sigla_prod, scadenze.provvigioni_sub, polizze.inpdf ');
  QChiusuraContabile_FCProvvisorio.sql.Add('from ');
  QChiusuraContabile_FCProvvisorio.sql.Add('scadenze ');
  QChiusuraContabile_FCProvvisorio.sql.Add
    ('LEFT OUTER JOIN compagni ON compagni.cod_compagnia = scadenze.rif_cod_compagnia ');
  QChiusuraContabile_FCProvvisorio.sql.Add
    ('LEFT OUTER JOIN polizze  ON polizze.cod_polizza = scadenze.rif_cod_polizza ');
  QChiusuraContabile_FCProvvisorio.sql.Add('LEFT OUTER JOIN clienti  ON clienti.cod_cliente = polizze.cod_cli ');
  QChiusuraContabile_FCProvvisorio.sql.Add('LEFT OUTER JOIN appoggio ON polizze.appoggiata_a = appoggio.cod_appoggio ');
  QChiusuraContabile_FCProvvisorio.sql.Add('LEFT OUTER JOIN produtt ON clienti.promoter = produtt.cod_produttore ');
  QChiusuraContabile_FCProvvisorio.sql.Add('where ');
  QChiusuraContabile_FCProvvisorio.sql.Add('scadenze.stato = ''P'' ');
  if UniMainModule.DMdatiAge.is_utente_promoter then
    QChiusuraContabile_FCProvvisorio.sql.Add('and clienti.promoter = ' +
      IntToStr(UniMainModule.DMdatiAge.QProdut.FieldByName('cod_produttore').AsInteger));
  // filtro sul periodo ...
  QChiusuraContabile_FCProvvisorio.sql.Add(' and scadenze.data_pag >= ' + QuotedStr(DtoSql(dal)) +
    ' and scadenze.data_pag <= ' + QuotedStr(DtoSql(al)));
  QChiusuraContabile_FCProvvisorio.sql.Add(' ORDER BY DATA_PAG');

  QChiusuraContabile_FCProvvisorio.Open;

  UniMainModule.DMdatiAge.QCompagnie.Open;
  try
    // modificato il 17/07/2018 per far stampare anche i fogli cassa provvisori a 0
    if true then // not QChiusuraContabile_FCProvvisorio.IsEmpty then
    begin
      if true then // QFogliCassa.FieldByName('cod_compagnia').AsInteger = 1 then
      begin


        QfcProvvisorioRiporti.Close;
        QfcProvvisorioRiporti.ParamByName('data').AsDateTime:= dal;
        QfcProvvisorioRiporti.Open;

        SLPdatiAge := UniMainModule.DMdatiAge.SLPdati_age;
        // frxFoglioCassa.EngineOptions.TempDir:=dati_gen.dir_loc;
        frxFoglioCassa.EngineOptions.TempDir := SLPdatiAge.dati_gen.dir_temp;
        // dati_gen.dir_loc; modificato 07/10/2013
        frxFoglioCassa.Variables['versione'] := QuotedStr('Rv. web'); // + LeggiVersione);
        if not QFogliCassa.FieldByName('data_chiusura').IsNull then
          frxFoglioCassa.Variables['data_chiusura'] :=
            QuotedStr(dateTimetostr(QFogliCassa.FieldByName('data_chiusura').AsDateTime))
        else
          frxFoglioCassa.Variables['data_chiusura'] := QuotedStr('');
        Tt                                          := 'Foglio cassa Provvisorio n� ';

        UniMainModule.DMdatiAge.QCompagnie.Locate('cod_compagnia', 1, []);
        frxFoglioCassa.Variables['FoglioCassa'] := QuotedStr(Tt + UniMainModule.DMdatiAge.get_n_fc + ' ');
        frxFoglioCassa.Variables['agenzia']     := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia + '\' +
          UniMainModule.DMdatiAge.SLPdati_age.sub_age + '  ' + UniMainModule.DMdatiAge.SLPdati_age.citta);
        frxFoglioCassa.Variables['age']         := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia);
        frxFoglioCassa.Variables['sub']         := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.sub_age);
        frxFoglioCassa.Variables['Periodo']     := QuotedStr('periodo dal ' + dateToStr(dal) + ' al ' + dateToStr(al));
        frxFoglioCassa.Variables['provvisorio'] := QuotedStr('S');

        frxFoglioCassa.Variables['ripo_lordo'] := QuotedStr( CurrToStr(QfcProvvisorioRiporti.FieldByName('lordo').AsCurrency) );
        if UniMainModule.DMdatiAge.is_utente_promoter then
           frxFoglioCassa.Variables['ripo_prov'] := QuotedStr('0.0')
        else
           frxFoglioCassa.Variables['ripo_prov'] := QuotedStr( CurrToStr(QfcProvvisorioRiporti.FieldByName('provvigioni').AsCurrency) );

        nomeReport := Trim(UniMainModule.DMdatiAge.SLPdati_age.agenzia + '-FC-' + ReplaceStr(dateToStr(date), '/', '-')
          + '-' + FormatDateTime('yyyymmddhhmmss', Now) + '.pdf');
        // frxPDFExport1.FileName := SLPdatiAge.dati_gen.dir_temp + Trim(nomeReport);
        frxPDFExport1.FileName := nomeReport;
        frxPDFExport1.FileName := stringreplace(TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'),
          nomeReport), '%', 'p', [rfReplaceAll]);

        NullStrictConvert := false;

        frxPDFExport1.ProtectionFlags := [ePrint];

        frxFoglioCassa.PrepareReport(true);
        frxFoglioCassa.Export(frxPDFExport1);

        if (frxPDFExport1.FileName <> '') and FileExists(frxPDFExport1.FileName) then
        begin
          TFPDFViewer.ShowReport(uniApplication, nomeReport, PDFUrl, PDFFolderPath, true, true);
        end;

        // UniSession.SendFile(frxPDFExport1.FileName); // , nomeRep);
        NullStrictConvert := true;

      end;
    end
    else
    begin
      raise EPolizzaError.create(CERR_NO_DATI_EXISTS, MSG_NO_DATI_EXISTS);
    end;
  finally
    QChiusuraContabile_FCProvvisorio.active := false;
  end;

end;

function TDMDatiReport.StampaQuietanza(CodQuietanza: integer): string;
var
  tipoq, nomeRep: string;
  prosegui, is_q_appendice, quiet_alf_page: Boolean;
  premio: Currency;
  app_pagata: Boolean;
begin
  // stampa la quietanza con codice interno CodQuietanza
  //
  // carica il modello da mod_slp
  // istanzia i valori con i dati di scadenze.dat polizze.dat e clienti.dat
  // mostra anteprima e permetti la stampa
  // memorizza la stampa
  prosegui := false;
  app_pagata:=false;

  if not QDati_agenzia.active then
    QDati_agenzia.Open;
  Qquietanza.Close;
  Qquietanza.ParamByName('COD_SCADENZA').AsInteger := CodQuietanza;
  Qquietanza.Open;
  quiet_alf_page := false;
  frxQuietanza.Clear;
  is_q_appendice := UniMainModule.DMdatiAgeTitoli.quietanza_con_appendice(Qquietanza.FieldByName('N_POLIZZA').AsString,
    Qquietanza.FieldByName('decorrenza').AsDateTime, app_pagata);
  if (is_q_appendice and app_pagata) then
  begin
    // carica il modello di quietanza appendice
    tipoq    := 'XAP' + alltrim(Qquietanza.FieldByName('OLD_COMPAGNIA').AsString);
    prosegui := UniMainModule.DMDatiTempPolizza.carica_ModuliVari(tipoq, '', frxQuietanza);
  end
  else
  begin
    // carica di ufficio il modello con copia contraente e copia agenzia sulla stessa pagina
    prosegui       := UniMainModule.DMDatiTempPolizza.carica_ModuliVari('XQUAP', '', frxQuietanza);
    quiet_alf_page := true;
  end;

  frxPDFExport1.EmbeddedFonts := true;

  if prosegui then
  begin
    frxQuietanza.Variables['agenzia']       := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia);
    frxQuietanza.Variables['age_nome']      := QuotedStr(QDati_agenzia.FieldByName('DENOMINAZIONE').AsString);
    frxQuietanza.Variables['age_citta']     := QuotedStr(QDati_agenzia.FieldByName('CITTA').AsString);
    frxQuietanza.Variables['age_indirizzo'] := QuotedStr(QDati_agenzia.FieldByName('INDIRIZZO').AsString);
    frxQuietanza.Variables['duplicato']     := QuotedStr('');
    frxQuietanza.Variables['copiax']        := QuotedStr('');

    frxQuietanza.Variables['sub_age']     := QuotedStr(Qquietanza.FieldByName('sigla').AsString);
    frxQuietanza.Variables['n_polizza']   := QuotedStr(Qquietanza.FieldByName('n_polizza').AsString);
    frxQuietanza.Variables['contraente']  := QuotedStr(Qquietanza.FieldByName('denominaz').AsString);
    frxQuietanza.Variables['indirizzo']   := QuotedStr(Qquietanza.FieldByName('indirizzo').AsString);
    frxQuietanza.Variables['cap']         := QuotedStr(Qquietanza.FieldByName('cap').AsString);
    frxQuietanza.Variables['citta']       := QuotedStr(Qquietanza.FieldByName('citta').AsString);
    frxQuietanza.Variables['decorrenza']  := QuotedStr(Qquietanza.FieldByName('decorrenza').AsString);
    frxQuietanza.Variables['scadenza']    := QuotedStr(Qquietanza.FieldByName('scadenza').AsString);
    frxQuietanza.Variables['nettoQ']      := QuotedStr(Qquietanza.FieldByName('pnetto').AsString);
    frxQuietanza.Variables['accessoriQ']  := QuotedStr(Qquietanza.FieldByName('accessori').AsString);
    frxQuietanza.Variables['imponibileQ'] := QuotedStr(Qquietanza.FieldByName('imponibile').AsString);
    frxQuietanza.Variables['imposteQ']    := QuotedStr(Qquietanza.FieldByName('tasse').AsString);
    frxQuietanza.Variables['da_pagareQ']  := QuotedStr(Qquietanza.FieldByName('lordo').AsString);
    frxQuietanza.Variables['inc_indice']  := QuotedStr(ifthen(Qquietanza.FieldByName('inc_indice').AsCurrency > 0,
      'SI', 'NO'));
    frxQuietanza.Variables['tipo_ogg'] := QuotedStr(Qquietanza.FieldByName('tipo_ogg').AsString);
    frxQuietanza.Variables['oggetto']  := QuotedStr(Qquietanza.FieldByName('oggetto').AsString);
    frxQuietanza.Variables['settoreX'] := QuotedStr(Qquietanza.FieldByName('settoreX').AsString);

    if Qquietanza.FieldByName('stato').AsString = 'P' then
    begin
      // provvedi a mettere i dati di incasso nella quietanza / appendice
      if frxQuietanza.Variables.IndexOf('premio_pag') > -1 then
      begin
        if (is_q_appendice and app_pagata) then
        begin
          if UniMainModule.DMdatiAgeTitoli.appendice_pagata(Qquietanza.FieldByName('N_POLIZZA').AsString,
            Qquietanza.FieldByName('DECORRENZA').AsDateTime, '15', premio) then
          begin
            premio                               := Qquietanza.FieldByName('LORDO').AsCurrency + premio;
            frxQuietanza.Variables['premio_pag'] := QuotedStr( FormatCurr('##,##0.00',premio));
            frxQuietanza.Variables['x_pag']      := QuotedStr('X');
          end
          else
          begin
            // ha incassato solo la parte relativa alla quietanza
            premio                               := Qquietanza.FieldByName('LORDO').AsCurrency;
            frxQuietanza.Variables['premio_pag'] := QuotedStr(FormatCurr('##,##0.00',Qquietanza.FieldByName('LORDO').AsCurrency));
            frxQuietanza.Variables['x_no_pag']   := QuotedStr('X');
          end;
        end
        else
          frxQuietanza.Variables['premio_pag'] := QuotedStr(  FormatCurr('##,##0.00', Qquietanza.FieldByName('LORDO').AsCurrency));
        frxQuietanza.Variables['data_pag']     := QuotedStr(dateToStr(Qquietanza.FieldByName('DATA_PAG').AsDateTime));
        frxQuietanza.Variables['fc_pag']       := QuotedStr(''); // quotedstr( get_n_fc );
      end;
    end;

    // frxQuietanza.PrintOptions.ShowDialog:=true;
    if (is_q_appendice and app_pagata) then
    begin
      if true then
      begin
        frxQuietanza.Variables['barcode1'] :=
          QuotedStr(fai_cbq(Qquietanza.FieldByName('n_polizza').AsString, Qquietanza.FieldByName('decorrenza')
          .AsDateTime));

        frxQuietanza.Variables['da_pagareA'] := QuotedStr(Qquietanza.FieldByName('APPlordo').AsString);
        frxQuietanza.Variables['imposteA']   := QuotedStr(Qquietanza.FieldByName('APPtasse').AsString);

        frxQuietanza.Variables['nettoQA'] :=
          QuotedStr(currTostr(Qquietanza.FieldByName('pnetto').AsCurrency + Qquietanza.FieldByName('APPnetto')
          .AsCurrency));
        frxQuietanza.Variables['accessoriQA']  := QuotedStr(currTostr(Qquietanza.FieldByName('accessori').AsCurrency));
        frxQuietanza.Variables['imponibileQA'] :=
          QuotedStr(currTostr(Qquietanza.FieldByName('imponibile').AsCurrency + Qquietanza.FieldByName('APPnetto')
          .AsCurrency));
        frxQuietanza.Variables['imposteQA'] :=
          QuotedStr(currTostr(Qquietanza.FieldByName('tasse').AsCurrency + Qquietanza.FieldByName('APPtasse')
          .AsCurrency));
        frxQuietanza.Variables['da_pagareQA'] :=
          QuotedStr(currTostr(Qquietanza.FieldByName('lordo').AsCurrency + Qquietanza.FieldByName('APPlordo')
          .AsCurrency));

        frxQuietanza.Variables['copiax'] := QuotedStr('Copia per il Contraente');
        frxQuietanza.PrepareReport(true);
        frxQuietanza.Variables['copiax'] := QuotedStr('Copia per l''Intermediario');
        frxQuietanza.PrepareReport(false);
        frxQuietanza.Variables['copiax'] := QuotedStr('Copia per la Direzione');
        frxQuietanza.PrepareReport(false);
        prosegui := true;
        // frxQuietanza.ShowPreparedReport;
      end;
    end
    else
    begin
      if quiet_alf_page then
      begin
        frxQuietanza.PrepareReport(true);
        prosegui := true;
        // frxQuietanza.ShowReport;
      end
      else
      begin
        frxQuietanza.Variables['copiax'] := QuotedStr('Copia per il Contraente');
        frxQuietanza.PrepareReport(true);
        frxQuietanza.Variables['copiax'] := QuotedStr('Copia per l''Intermediario');
        frxQuietanza.PrepareReport(false);
        prosegui := true;
        // frxQuietanza.showPreparedreport;
      end;
    end;

    if prosegui then
    begin
      {
        // registra la stampa
        if not is_direzione2 then
        begin
        if is_q_appendice then procedi:=' STAQUA '
        else procedi:=' STAQUI ';
        menubase.comunica('CKA', datetimetostr(now)+ procedi +' Np' +
        DMdatiAge.Tscadenze.fieldbyname('N_POLIZZA').asString+' '+dataNormale(DMdatiAge.Tscadenze.fieldbyname('decorrenza').asDateTime) ,
        'CK','','','','STQ',
        DMdatiAge.Tscadenze.fieldbyname('N_POLIZZA').asString,DMdatiAge.Tscadenze.fieldbyname('decorrenza').asDateTime);
        end;
        t:=padr(DMdatiAge.Tscadenze.fieldbyname('N_POLIZZA').asString,' ',15) + dtos(DMdatiAge.Tscadenze.fieldbyname('RATA').asDateTime);
        if is_q_appendice then
        traccia('STAQUIAP',DMdatiAge.Tscadenze.fieldbyname('COD_SCADENZA').asInteger,'SCADENZE',t)
        else
        traccia('STAQUIET',DMdatiAge.Tscadenze.fieldbyname('COD_SCADENZA').asInteger,'SCADENZE',t);
        DMdatiAge.Tscadenze.edit;
        DMdatiAge.Tscadenze.fieldbyname('INVIO_1_SOLLECITO').asDateTime:=date;
        DMdatiAge.Tscadenze.fieldbyname('N_STAMPE_Q1').asInteger:=DMdatiAge.Tscadenze.fieldbyname('N_STAMPE_Q1').asInteger + 1;
        DMdatiAge.Tscadenze.post;
      }
      NullStrictConvert := false;
      nomeRep := UniMainModule.DMdatiAge.SLPdati_age.agenzia + '-QUIET-' + Qquietanza.FieldByName('n_polizza').AsString
        + '-' + FormatDateTime('yyyymmddhhmmss', Now) + '.pdf';

      frxPDFExport1.FileName := stringreplace(TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'),
        nomeRep), '%', 'p', [rfReplaceAll]);
      frxQuietanza.Export(frxPDFExport1);
      // UniSession.SendFile(frxPDFExport1.FileName);

      if (frxPDFExport1.FileName <> '') and FileExists(frxPDFExport1.FileName) then
      begin
        TFPDFViewer.ShowReport(uniApplication, nomeRep, PDFUrl, PDFFolderPath, true, true);
      end;
      NullStrictConvert := true;
    end;

  end;

end;

procedure TDMDatiReport.stampaQuietanziamentiSLP(AAnno, AMese, codCollaboratore: integer;
  ATipoOrd: TipoOrdQuietanziamenti; ATipoCopieQuietanziamenti: TipoCopieQuietanziamenti);

var
  nomeRep: string;

  procedure impostaOrdinamento;
  begin
    QStampaScadenze.sql.Append('ORDER BY ');
    case ATipoOrd of
      toqPolizza:
        QStampaScadenze.sql.Append('SCA.N_POLIZZA');
      toqPolizzaContraente:
        QStampaScadenze.sql.Append('SCA.DENOMINAZ');
      toqCollaboratorePolizza:
        QStampaScadenze.sql.Append('PRODUTT.SIGLA, SCA.N_POLIZZA');
      toqCollabContraente:
        QStampaScadenze.sql.Append('PRODUTT.SIGLA, SCA.DENOMINAZ');
      toqScadenzaContraente:
        QStampaScadenze.sql.Append('SCA.DECORRENZA , SCA.DENOMINAZ');
    else
      QStampaScadenze.sql.Append('SCA.DECORRENZA , SCA.N_POLIZZA');
    end;
  end;

  procedure stampa_quietanzaSLP(report: TfrxReport; sequenza: integer);
  var
    tipoq: string;
    is_q_appendice, quiet_alf_page: Boolean;

    // SLP_DatiAge: TSLPdati_age_rec;
  begin
    // stampa la quietanza corrente;
    // prepara pacchetto dati per la stampa della quietanza
    // verifica se esiste autorizzazione alla stampa !!!!
    // if DMdatiAge.Tscadenze.Locate('cod_scadenza', Qscadenze.FieldByName('cod_scadenza').AsInteger, []) then
    Qquietanza.Close;
    Qquietanza.ParamByName('COD_SCADENZA').AsInteger := QStampaScadenze.FieldByName('COD_SCADENZA').AsInteger;
    Qquietanza.Open;
    if not Qquietanza.IsEmpty then
    begin
      if ((Qquietanza.FieldByName('TIPO_TITOLO').AsString = '20')) then
      // (Qscadenze.FieldByName('tipo_titolo').asString='15'))
      begin

        Qquietanza.Close;
        Qquietanza.ParamByName('COD_SCADENZA').AsInteger := QStampaScadenze.FieldByName('COD_SCADENZA').AsInteger;
        Qquietanza.Open;

        quiet_alf_page := false;
        is_q_appendice := false;
        report.Clear;

        if QStampaScadenze.FieldByName('APPlordo').AsCurrency > 0 then
        begin
          is_q_appendice := true;
          tipoq          := 'XAP' + alltrim(Qquietanza.FieldByName('old_compagnia').AsString);
        end
        else
        begin
          if (ATipoCopieQuietanziamenti = tcqStessaPagina) then
          begin
            tipoq          := 'XQUAP';
            quiet_alf_page := true;
          end
          else
            tipoq := 'XQUFP';
        end;
        if UniMainModule.DMDatiTempPolizza.carica_ModuliVari(tipoq, '', report) then
        begin

          report.PrintOptions.ShowDialog := false;

          // SLP_DatiAge                 := UniMainModule.DMdatiAge.SLPdati_age;
          report.Variables['agenzia']       := QuotedStr(UniMainModule.DMdatiAge.SLPdati_age.agenzia);
          report.Variables['age_nome']      := QuotedStr(QDati_agenzia.FieldByName('DENOMINAZIONE').AsString);
          report.Variables['age_citta']     := QuotedStr(QDati_agenzia.FieldByName('CITTA').AsString);
          report.Variables['age_indirizzo'] := QuotedStr(QDati_agenzia.FieldByName('INDIRIZZO').AsString);

          // report.Variables['duplicato']:=quotedstr('');
          report.Variables['copiax'] := QuotedStr('');

          report.Variables['sub_age']     := QuotedStr(Qquietanza.FieldByName('SIGLA').AsString);
          report.Variables['n_polizza']   := QuotedStr(Qquietanza.FieldByName('N_POLIZZA').AsString);
          report.Variables['contraente']  := QuotedStr(Qquietanza.FieldByName('DENOMINAZ').AsString);
          report.Variables['indirizzo']   := QuotedStr(Qquietanza.FieldByName('INDIRIZZO').AsString);
          report.Variables['cap']         := QuotedStr(Qquietanza.FieldByName('CAP').AsString);
          report.Variables['citta']       := QuotedStr(Qquietanza.FieldByName('CITTA').AsString);
          report.Variables['decorrenza']  := QuotedStr(Qquietanza.FieldByName('DECORRENZA').AsString);
          report.Variables['scadenza']    := QuotedStr(Qquietanza.FieldByName('SCADENZA').AsString);
          report.Variables['nettoQ']      := QuotedStr(Qquietanza.FieldByName('PNETTO').AsString);
          report.Variables['accessoriQ']  := QuotedStr(Qquietanza.FieldByName('ACCESSORI').AsString);
          report.Variables['imponibileQ'] := QuotedStr(Qquietanza.FieldByName('IMPONIBILE').AsString);
          report.Variables['imposteQ']    := QuotedStr(Qquietanza.FieldByName('TASSE').AsString);
          report.Variables['da_pagareQ']  := QuotedStr(Qquietanza.FieldByName('LORDO').AsString);
          report.Variables['inc_indice']  := QuotedStr(ifthen(Qquietanza.FieldByName('INC_INDICE').AsCurrency > 0,
            'SI', 'NO'));
          report.Variables['tipo_ogg'] := QuotedStr(Qquietanza.FieldByName('TIPO_OGG').AsString);
          report.Variables['oggetto']  := QuotedStr(Qquietanza.FieldByName('OGGETTO').AsString);
          report.Variables['settoreX'] := QuotedStr(Qquietanza.FieldByName('SETTOREX').AsString);
          // report.Variables['num_ass']:=quotedstr(Qquietanza.FieldByName('').asString);

          if is_q_appendice then
          begin

            report.Variables['barcode1'] := QuotedStr(fai_cb(Qquietanza.FieldByName('N_POLIZZA').AsString,
              dateToStr(Qquietanza.FieldByName('DECORRENZA').AsDateTime)));

            report.Variables['da_pagareA'] := QuotedStr(Qquietanza.FieldByName('APPLORDO').AsString);
            report.Variables['imposteA']   := QuotedStr(Qquietanza.FieldByName('APPTASSE').AsString);

            report.Variables['nettoQA'] :=
              QuotedStr(currTostr(Qquietanza.FieldByName('PNETTO').AsCurrency + Qquietanza.FieldByName('APPNETTO')
              .AsCurrency));
            report.Variables['accessoriQA']  := QuotedStr(currTostr(Qquietanza.FieldByName('ACCESSORI').AsCurrency));
            report.Variables['imponibileQA'] :=
              QuotedStr(currTostr(Qquietanza.FieldByName('IMPONIBILE').AsCurrency + Qquietanza.FieldByName('APPNETTO')
              .AsCurrency));
            report.Variables['IMPOSTEQA'] :=
              QuotedStr(currTostr(Qquietanza.FieldByName('TASSE').AsCurrency + Qquietanza.FieldByName('APPTASSE')
              .AsCurrency));
            report.Variables['da_pagareQA'] :=
              QuotedStr(currTostr(Qquietanza.FieldByName('LORDO').AsCurrency + Qquietanza.FieldByName('APPLORDO')
              .AsCurrency));

            report.Variables['copiax'] := QuotedStr('Copia per il Contraente');
            report.PrepareReport(true);

            report.Variables['copiax'] := QuotedStr('Copia per l''Intermediario');
            report.PrepareReport(false);

            report.Variables['copiax'] := QuotedStr('Copia per la Direzione');
            report.PrepareReport(false);

          end
          else
          begin
            if quiet_alf_page then
            begin
              report.PrepareReport(true);
            end
            else
            begin
              report.Variables['copiax'] := QuotedStr('Copia per il Contraente');
              report.PrepareReport(true);
              report.Variables['copiax'] := QuotedStr('Copia per l''Intermediario');
              report.PrepareReport(false);
            end;
          end;

          tipoq := padr(Qquietanza.FieldByName('N_POLIZZA').AsString, ' ', 15) +
            dtos(Qquietanza.FieldByName('RATA').AsDateTime);
          if is_q_appendice then
            UniMainModule.traccia('STAQUIAP', Qquietanza.FieldByName('COD_SCADENZA').AsInteger, 'SCADENZE', tipoq)
          else
            UniMainModule.traccia('STAQUIET', Qquietanza.FieldByName('COD_SCADENZA').AsInteger, 'SCADENZE', tipoq);
          QUpdScadenza.ParamByName('COD_SCADENZA').AsInteger := Qquietanza.FieldByName('COD_SCADENZA').AsInteger;
          QUpdScadenza.execSQL;

        end;
      end;
    end;
  end;

begin

  QStampaScadenze.sql.Clear;
  QStampaScadenze.sql := jvmstrh.ItemByName['Scadenze'].Strings;

  if UniMainModule.IsUtentePromoter then
  begin
    // aggiungi il filtro su il promoter
    // filtra solo le sue quietanze
    QStampaScadenze.sql.Add('AND POL.FATTA_DA = ' + UniMainModule.utentePromoter.ToString);
  end
  else
  begin
    if codCollaboratore > 0 then
      QStampaScadenze.sql.Add('AND POL.FATTA_DA = ' + IntToStr(codCollaboratore));
  end;

  impostaOrdinamento;

  if not QDati_agenzia.active then
    QDati_agenzia.Open;

  QStampaScadenze.ParamByName('ANNO').AsInteger := AAnno;
  QStampaScadenze.ParamByName('MESE').AsInteger := AMese;
  QStampaScadenze.Open;
  if QStampaScadenze.IsEmpty then
    raise EPolizzaError.create(CERR_QUIETANZE_NOT_FOUND, MSG_QUIETANZE_NOT_FOUND);

  frxSequenza.Clear;
  // frxSequenza.PrepareReport(true);
  // if cose = 'DEF' then
  // begin
  // if not DMdatiAge.Tdati_agenzia.active then
  // DMdatiAge.Tdati_agenzia.Open;

  // if true then // PrinterSetupDialog1.Execute then
  // begin
  // intercetta il tipo di stampante selezionato !!!!
  // frxQuietanza.PrintOptions.Printer:= Printer.printers[printer.printerindex];
  // frxSequenza.PrintOptions.Printer:= Printer.printers[printer.printerindex];
  // i := 0;
  while not QStampaScadenze.Eof do
  begin

    if QStampaScadenze.RecNo = 1 then
      stampa_quietanzaSLP(frxSequenza, 1)
    else
    begin
      stampa_quietanzaSLP(frxQuietanza, 1);
      frxSequenza.PreviewPages.AddFrom(frxQuietanza);
    end;
    QStampaScadenze.Next;
  end;
  nomeRep := UniMainModule.DMdatiAge.SLPdati_age.agenzia + '-QUIET-' + IntToStr(AMese) + '-' + IntToStr(AAnno) + '-' +
    FormatDateTime('yyyymmddhhmmss', Now) + '.pdf';

  frxPDFExport1.FileName := stringreplace(TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'), nomeRep),
    '%', 'p', [rfReplaceAll]);

  frxSequenza.Export(frxPDFExport1);
  // UniSession.SendFile(frxPDFExport1.FileName);

  if (frxPDFExport1.FileName <> '') and FileExists(frxPDFExport1.FileName) then
  begin
    TFPDFViewer.ShowReport(uniApplication, nomeRep, PDFUrl, PDFFolderPath, true, true);
  end;
  // frxSequenza.Clear;

  // end
  // else
  // MessageDlg('Il mese delle quietanze da stampare non pu� essere cos� distante dal mese corrente !', mtWarning,
  // [mbOK], 0);

end;

procedure TDMDatiReport.UpdDataStampaDoc(ACodDoc: integer);
begin
  QaggSTA_docHouse.ParamByName('CODICE_DOC').AsInteger := ACodDoc;
  QaggSTA_docHouse.execSQL;

end;

procedure TDMDatiReport.UpdDataVisualizzazioneDoc(ACodDoc: integer);
begin
  QaggVIS_docHouse.ParamByName('CODICE_DOC').AsInteger := ACodDoc;
  QaggVIS_docHouse.execSQL;

end;

function TDMDatiReport.fai_cb(p1, p2: string): string;
begin
  // result:=  genera_cbFC(RightStr(UniMainModule.DMDatiReport.QFogliCassa.FieldByName('NUMERO_FC').AsString, 4),
  // StrToInt(p1),StrToInt(p2),
  // UniMainModule.DMDatiReport.QFogliCassa.FieldByName('periodo_dal').AsDateTime,
  // UniMainModule.DMDatiReport.QFogliCassa.FieldByName('periodo_al').AsDateTime);
end;

function TDMDatiReport.frxFoglioCassaUserFunction(const MethodName: string; var Params: Variant): Variant;
begin
  inherited;
  if MethodName = 'FAI_CB' then
    Result := fai_cb(Params[0], Params[1]);
end;

function TDMDatiReport.frxQuietanzaUserFunction(const MethodName: string; var Params: Variant): Variant;
begin
  if MethodName = 'FAI_CB' then
    Result := fai_cbq(Params[0], Params[1]);
end;

function TDMDatiReport.genera_cbFC(numero: string; num_pagine, pagina: integer; dal, al: TdateTime): string;
begin
  // genera il codice a barre per la polizza
  //
  // '10'  identificatore fisso per le polizze
  // 02 che indica il numero di pagine del documento
  // 01 02  numero della pagina corrente
  // nnnnnnn 7 cifre paddatate a sinistra con 0 per il numero di polizza

  // result := '35214' + '70' ;
  // result := result + IntToStr(num_pagine).PadLeft(2, '0') + IntToStr(pagina).PadLeft(2, '0');
  // result := result + Trim(UniMainModule.DMdatiAge.SLPdati_age.cod_intZ);
  // result := result + Trim(numero).PadLeft(4, '0');
  // result := result + DtoS(dal) + DtoS(al);

  Result := '';
end;

procedure TDMDatiReport.getDocuments(ADocDataset: TDataSet; ComingFromLogin: Boolean);
begin
  if ADocDataset = QdocHouse then
  begin
    if not ComingFromLogin then
    begin
      QdocHouse.Close;
      QdocHouse.sql.Clear;
      QdocHouse.sql.Append('select *, data_carico as data_circolare,');
      QdocHouse.sql.Append('ifnull (data_lettura then false else true) as letto');
      QdocHouse.sql.Append('from dochouse');
      QdocHouse.sql.Append('where visibile = ''S'' ');
      QdocHouse.sql.Append('order by data_carico desc, descrizione');

      QdocHouse.Open;
    end;
  end
  else
  begin
    QDocPrintHouse.Open;
  end;

end;


procedure TDMDatiReport.openDocHouse(filtro: string);
begin
   QdocHouse.Close;
   QdocHouse.sql.Clear;
   QdocHouse.sql.Append('select *, data_carico as data_circolare,');
   QdocHouse.sql.Append('ifnull (data_lettura then false else true) as letto');
   QdocHouse.sql.Append('from dochouse');
   QdocHouse.sql.Append('where visibile = ''S'' ');
   if filtro>'' then
      QdocHouse.sql.Append('and tipo_doc='+QuotedStr(filtro));

   QdocHouse.sql.Append('order by data_carico desc, descrizione');

   QdocHouse.Open;

end;


function TDMDatiReport.get_dati_pagamentoFC: string;
begin
  // carica i dati relativi
  QcompagniSLP.Open;
  Result := QcompagniSLP.FieldByName('PAGFC_banca').AsString + ', IBAN ' + QcompagniSLP.FieldByName('PAGFC_IBAN')
    .AsString + ' oppure    C.C. postale IBAN IT24  B076 0101 000 0000 10778108';

  QcompagniSLP.Close;
end;

function TDMDatiReport.hasDocuments: Boolean;
begin
  QdocHouse.sql.Clear;
  QdocHouse.sql.Append('select *, data_carico as data_circolare, false as letto ');
  QdocHouse.sql.Append('from dochouse');
  QdocHouse.sql.Append('where visibile = ''S'' ');
  QdocHouse.sql.Append('and data_lettura is null');
  QdocHouse.sql.Append('order by data_carico desc, descrizione');

  QdocHouse.Open;
  Result := not QdocHouse.IsEmpty;
end;

function TDMDatiReport.hasDocumentsToShow: Boolean;
begin
  Result := hasDocuments and not UniMainModule.DMdatiAge.is_utente_promoter;
end;

procedure TDMDatiReport.mostraAnteprimaStatistica(AReport: TfrxReport);
begin
  AReport.Variables['titolo'] := QuotedStr(TitoloReport);
  AReport.PrepareReport(true);

  ReportFileName := ReportFileName + '_' + FormatDateTime('yyyymmddhhmmss', Now) + '.PDF';

  frxPDFExport1.FileName := TPath.Combine(TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'), ReportFileName);
  frxPDFExport1.ExportNotPrintable := false;
  AReport.Export(frxPDFExport1);
  TFPDFViewer.ShowReport(uniApplication, ReportFileName, UniServerModule.FilesFolderURL + 'PDF/',
    TPath.Combine(UniServerModule.FilesFolderPath, 'PDF'), true, true);

end;



function TDMDatiReport.OpenModuliDisponibili(indice: integer): Boolean;
begin
  // apri la tabella dei moduli disponibili
  QmoduliDisponibili.Close;
  QmoduliDisponibili.sql.Clear;

  case indice of
    0:
      begin
        // polizze
        QmoduliDisponibili.sql.Add('select cod_Stampa, descrizione, tipo, codice_slp ');
        QmoduliDisponibili.sql.Add('from mod_slp left join tipo_pol on tipo_pol.sigla=codice_slp ');
        QmoduliDisponibili.sql.Add('where tipo=''P'' --and visibie_per_utenti=''1'' ');
        QmoduliDisponibili.sql.Add('and tipo_pol.stampa_slp4web=''S'' ');
        QmoduliDisponibili.sql.Add('order by descrizione ');
      end;
    1:
      begin
        // modulistica
        QmoduliDisponibili.sql.Add('select cod_Stampa, descrizione, tipo, codice_slp ');
        QmoduliDisponibili.sql.Add('from mod_slp ');
        QmoduliDisponibili.sql.Add('where tipo=''S'' and visibile_per_utenti=''1'' ');
        QmoduliDisponibili.sql.Add('order by descrizione ');

      end;
    2:
      begin
        // circolari
        QmoduliDisponibili.sql.Add('select cod_Stampa, descrizione, tipo, codice_slp, data_circolare ');
        QmoduliDisponibili.sql.Add('from mod_slp ');
        QmoduliDisponibili.sql.Add('where tipo=''C'' and visibile_per_utenti=''1'' ');
        QmoduliDisponibili.sql.Add('order by data_CIRCOLARE desc  --,descrizione ');
      end;
  end;
  QmoduliDisponibili.Open;
  Result := not QmoduliDisponibili.IsEmpty;
  {
    // videocorsi
    select descrizione, data_video as data_circolare,link
    from videocorsi
    where visibile_per_utenti='1'
    order by descrizione
  }

end;

function TDMDatiReport.OpenPrinthousePolizza(NPolizza: string): Boolean;
begin
  if NPolizza>'' then
  begin
     QDocPrinthousePolizza.Close;
     QDocPrinthousePolizza.sql.Clear;
     QDocPrinthousePolizza.sql.Append('select codice as codice_doc, tipo_doc, data_doc as effetto, n_polizza, descrizione');
     QDocPrinthousePolizza.sql.Append('from printhouse');
     QDocPrinthousePolizza.sql.Append('where');
     if NPolizza <> '' then
     begin
       if Copy(NPolizza,1,2)='PA' then
          QDocPrinthousePolizza.sql.Append('n_preventivo = :NPOLIZZA')
       else
          QDocPrinthousePolizza.sql.Append('n_polizza = :NPOLIZZA');
     end else
     begin
       QDocPrinthousePolizza.sql.Append('n_polizza > ''0''');
       QDocPrinthousePolizza.sql.Append('order by n_polizza');
     end;

     if NPolizza <> '' then
       QDocPrinthousePolizza.ParamByName('NPOLIZZA').AsString := NPolizza;

     QDocPrinthousePolizza.Open;
     Result := not QDocPrinthousePolizza.IsEmpty;
  end else Result:=false;
end;

procedure TDMDatiReport.PreparaQueryStatisticheClienti(AParameterList: TStrings);
begin
  TitoloReport := 'ELENCO CLIENTI';

  QStatisticheClienti.Close;
  QStatisticheClienti.sql.Clear;
  QStatisticheClienti.sql.Add('SELECT * ');
  QStatisticheClienti.sql.Add('FROM CLIENTI CL');
  if AParameterList.Values['Professione'] <> '' then
    QStatisticheClienti.sql.Add('LEFT OUTER JOIN PROFESS PR ON PR.COD_PROFESSIONE = CL.PROFESSION');
  QStatisticheClienti.sql.Add('WHERE 0 = 0');

  if AParameterList.Values['StatoCli'] = 'Attivi' then
    QStatisticheClienti.sql.Add('AND STATO > 0') // attivi;
  else
    if AParameterList.Values['StatoCli'] = 'Potenziali' then
      QStatisticheClienti.sql.Add('AND STATO < 0')
      // potenziali;
    else
      if AParameterList.Values['StatoCli'] = 'Ex clienti' then
        QStatisticheClienti.sql.Add('AND STATO = 0');
  // ex-clienti;

  if AParameterList.Values['NaturaGiuridica'] = 'Persone fisiche' then
    QStatisticheClienti.sql.Add('AND SESSO = ''P''') // giuridiche
  else
    if AParameterList.Values['NaturaGiuridica'] = 'Persone giuridiche' then
      QStatisticheClienti.sql.Add('AND SESSO <> ''P'''); // fisiche

  if AParameterList.Values['Professione'] <> '' then
    QStatisticheClienti.sql.Add('AND CL.PROFESSION = ' + AParameterList.Values['Professione']);

  if AParameterList.Values['Compagnia'] <> '' then
    QStatisticheClienti.sql.Add
      ('AND CL.COD_CLIENTE IN (SELECT POLIZZE.COD_CLI FROM POLIZZE WHERE POLIZZE.RIF_COMPAGNIA =' +
      AParameterList.Values['Compagnia'] + ')');

  if AParameterList.Values['Promoter'] <> '' then
  begin
    QStatisticheClienti.sql.Add('AND CL.PROMOTER = ' + AParameterList.Values['Promoter']);
    TitoloReport := TitoloReport + '- collaboratore: ' + AParameterList.Values['PromoterNome'];
  end;

  if UniMainModule.IsUtentePromoter then
  begin
    QStatisticheClienti.sql.Add('AND CL.PROMOTER = ' + UniMainModule.UtentePromoter.ToString);
    TitoloReport := TitoloReport + '- collaboratore: ' + AParameterList.Values['PromoterNome'];
  end;

  // if not CBgruppi.Checked then
  // QStatisticheClienti.SQL.Add('and cl.gruppo= ' + Qgruppi.FieldByName('cod_gruppo').AsString);

  if AParameterList.Values['Cap'] <> '' then
  begin
    QStatisticheClienti.sql.Add('AND CL.CAP LIKE ' + QuotedStr(AParameterList.Values['Cap'] + '%'));
    TitoloReport := TitoloReport + ' - cap: ' + AParameterList.Values['Cap'];
  end;

  if AParameterList.Values['Citta'] <> '' then
  begin
    // filtro sulla citta
    QStatisticheClienti.sql.Add('AND CL.CITTA = ' + QuotedStr(AParameterList.Values['Citta']));
    TitoloReport := TitoloReport + ' - citt�: ' + AParameterList.Values['Citta'];
  end;

  if AParameterList.Values['Provincia'] <> '' then
  begin
    // filtro sulla ptovincia
    QStatisticheClienti.sql.Add('AND CL.PROVINCIA = ' + QuotedStr(AParameterList.Values['Provincia']));
    TitoloReport := TitoloReport + ' - prov.: ' + AParameterList.Values['Provincia'];
  end;

  QStatisticheClienti.sql.Add('ORDER BY');
  if AParameterList.Values['Ordinamento'] = 'Nome' then
    QStatisticheClienti.sql.Add('DENOMINAZ')
  else
    if AParameterList.Values['Ordinamento'] = 'Citt�' then
      QStatisticheClienti.sql.Add('CITTA, DENOMINAZ')
    else
      QStatisticheClienti.sql.Add('REFERENTE');

  QStatisticheClienti.Open;

end;

procedure TDMDatiReport.PreparaQueryStatisticheContratti(AParameterList: TStrings; PolizzeMorte: Boolean);

begin
  TitoloReport := ifthen(PolizzeMorte, 'POLIZZE ANNULLATE - ELENCO CONTRATTI', 'ELENCO CONTRATTI');

  QStatisticheContratti.Close;
  QStatisticheContratti.sql.Clear;
  QStatisticheContratti.sql.Add
    ('SELECT N_POLIZZA, CONTRAENTE, DECORRENZA, SCADENZA, FRAZIONAM, LORDO, CP.CODICE_AGENZIA,');
  QStatisticheContratti.sql.Add('RM.DESCRIZIONE AS RAMO, RM.SIGLA AS SIGLAR,');
  QStatisticheContratti.sql.Add
    ('TP.DESCRIZIONE AS "TIPO POL.", COD_POLIZZA, DATA_MORTE, AP.NOME, SOSPESO_DAL, SOSPESO_AL, ');

  QStatisticheContratti.sql.Add('CASE ');
  QStatisticheContratti.sql.Add('WHEN sc.data_pag>ssc.data_pag THEN sc.data_pag ');
  QStatisticheContratti.sql.Add('ELSE ssc.data_pag ');
  QStatisticheContratti.sql.Add('END AS dataIncasso ');

  QStatisticheContratti.sql.Add('FROM POLIZZE POL');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN CLIENTI CL ON CL.COD_CLIENTE = POL.COD_CLI');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN RAMI RM ON RM.COD_RAMO = POL.RIF_COD_RAMO');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN TIPO_POL TP ON TP.COD_TIPO_POL = POL.RIF_TIPO_POL');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN COMPAGNI CP ON CP.COD_COMPAGNIA = POL.RIF_COMPAGNIA');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN APPOGGIO AP ON AP.COD_APPOGGIO = POL.APPOGGIATA_A');
  QStatisticheContratti.sql.Add
    ('LEFT OUTER JOIN SCADENZE SC ON SC.N_POLIZZA = POL.N_POLIZZA AND (SC.TIPO_TITOLO=''10'' or SC.TIPO_TITOLO=''11'') ');
  QStatisticheContratti.sql.Add
    ('LEFT OUTER JOIN S_SCADENZE SSC ON SSC.N_POLIZZA = POL.N_POLIZZA AND (SSC.TIPO_TITOLO=''10'' or SSC.TIPO_TITOLO=''11'') ');
  // tsql.Add('where stato<>''C''');
  if PolizzeMorte then
    QStatisticheContratti.sql.Add('WHERE STATO = ''C''')
  else
    QStatisticheContratti.sql.Add('WHERE STATO = ''V''');
  // filtro su collaboratore
  // livello := 0;

  if UniMainModule.IsUtentePromoter then
  begin
    QStatisticheContratti.sql.Add('AND  (fatta_Da =' + UniMainModule.UtentePromoter.ToString);
    QStatisticheContratti.sql.Add('OR CL.PROMOTER =' + UniMainModule.UtentePromoter.ToString + ')');
    // tsql.Add('   or ifnull(fatta_da,1=1,1=0)');
    TitoloReport := TitoloReport + '- collaboratore: ' + UniMainModule.UtenteNome;
  end
  else
  begin
    if AParameterList.Values['Promoter'] <> '' then
    begin
      QStatisticheContratti.sql.Add('AND  (FATTA_DA =' + AParameterList.Values['Promoter']);
      QStatisticheContratti.sql.Add('OR CL.PROMOTER =' + AParameterList.Values['Promoter'] + ')');
      TitoloReport := TitoloReport + '- collaboratore: ' + AParameterList.Values['PromoterNome'];
      // inc(livello);
    end;
  end;

  // parte relativa all'eventuale sottolivello
  if AParameterList.Values['SubPromoter'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND  (sub_promoter =' + AParameterList.Values['SubPromoter']);
    QStatisticheContratti.sql.Add('OR CL.sub_PROMOTER =' + AParameterList.Values['SubPromoter'] + ')');
    TitoloReport := TitoloReport + '- sub-collaboratore: ' + AParameterList.Values['SubPromoterNome'];
  end;

  if AParameterList.Values['intermediataDa'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND intermediata_da =' + AParameterList.Values['intermediataDa']);
    TitoloReport := TitoloReport + '- intermediati da: ' + AParameterList.Values['intermediataDaNome'];
  end;

  MostraPolizzeSospese := false;
  if AParameterList.Values['StatoPolizze'] = 'Sospese' then
  begin
    // elenca solo polizze sospese
    QStatisticheContratti.sql.Add('AND SOSPESO_DAL <> NULL AND SOSPESO_AL <= CURRENT_DATE');
    MostraPolizzeSospese := true;
  end
  else
    if AParameterList.Values['StatoPolizze'] = 'Non sospese' then
      // elenca solo polizze NON sospese
      QStatisticheContratti.sql.Add('AND (SOSPESO_DAL = NULL OR (SOSPESO_DAL <> NULL AND SOSPESO_AL <= CURRENT_DATE))');

  if AParameterList.Values['NuoviContratti'] = 'Nuovi' then
    // solo polizze nuove 10
    QStatisticheContratti.sql.Add('AND N_POLIZZA_SOSTITUITA IS NULL')
  else
    if AParameterList.Values['NuoviContratti'] = 'In sostituzione' then
      // solo polizze nuove 11
      QStatisticheContratti.sql.Add('AND N_POLIZZA_SOSTITUITA > '''' ');

  if AParameterList.Values['Cap'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND CL.CAP LIKE ' + QuotedStr(AParameterList.Values['Cap'] + '%'));
    TitoloReport := TitoloReport + ' - cap: ' + AParameterList.Values['Cap'];
    // inc(livello);
  end;

  if AParameterList.Values['Citta'] <> '' then
  begin
    // filtro sulla citta
    QStatisticheContratti.sql.Add('AND CL.CITTA = ' + QuotedStr(AParameterList.Values['Citta']));
    TitoloReport := TitoloReport + ' - citt�: ' + AParameterList.Values['Citta'];
    // inc(livello);
  end;

  if AParameterList.Values['Provincia'] <> '' then
  begin
    // filtro sulla ptovincia
    QStatisticheContratti.sql.Add('AND CL.PROVINCIA = ' + QuotedStr(AParameterList.Values['Provincia']));
    TitoloReport := TitoloReport + ' - prov.: ' + AParameterList.Values['Provincia'];
    // inc(livello);
  end;
  // if ((year(EeffettoDA.Date) > 1980) and (year(EeffettoA.Date) > 1980)) then
  // begin
  // if (EeffettoDA.Date <= EeffettoA.Date) then
  // begin
  // QStatisticheContratti.sql.Add('   and  decorrenza>=' + QuotedStr(DtoSql(EeffettoDA.Date)) + ' and decorrenza<=' +
  // QuotedStr(DtoSql(EeffettoA.Date)));
  // end;
  // end;

  if AParameterList.Values['DataEffetto'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataEffetto']);
    QStatisticheContratti.sql.Add('AND DECORRENZA BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)');

  end;

  // if ((year(EscadenzaDA.Date) > 1980) and (year(EscadenzaA.Date) > 1980)) then
  // begin
  // if (EscadenzaDA.Date <= EscadenzaA.Date) then
  // begin
  // QStatisticheContratti.sql.Add('   and  scadenza>=' + QuotedStr(DtoSql(EscadenzaDA.Date)) + ' and scadenza<=' +
  // QuotedStr(DtoSql(EscadenzaA.Date)));
  // end;
  // end;

  if AParameterList.Values['DataScadenza'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataScadenza']);
    QStatisticheContratti.sql.Add('AND SCADENZA BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)');

  end;

  // if ((year(EannulataDa.Date) > 1980) and (year(EannulataA.Date) > 1980)) then
  // begin
  // if (EannulataDa.Date <= EannulataA.Date) then
  // begin
  // QStatisticheContratti.sql.Add('   and  data_morte>=' + QuotedStr(DtoSql(EannulataDa.Date)) + ' and data_morte<=' +
  // QuotedStr(DtoSql(EannulataA.Date)));
  // end;
  // end;

  if AParameterList.Values['DataAnnullamento'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataScadenza']);
    QStatisticheContratti.sql.Add('AND DATA_MORTE BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)');
  end;

  if AParameterList.Values['DataIncasso'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataIncasso']);
    // QStatisticheContratti.sql.Add('AND dataIncasso BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
    // QuotedStr(RecDate.DataA) + ' AS DATE)');

    QStatisticheContratti.sql.Add('AND ((sc.data_pag BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST('
      + QuotedStr(RecDate.DataA) + ' AS DATE)) or ');

    QStatisticheContratti.sql.Add('( ssc.data_pag BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)))');
    TitoloReport := TitoloReport + ' - data incasso: ' + RecDate.DataDa + ' al ' + RecDate.DataA;
  end;

  // filtro su ramo
  if AParameterList.Values['RamoPolizza'] <> '' then
  begin
    QStatisticheContratti.sql.Add(' AND POL.RIF_COD_RAMO = ' + AParameterList.Values['RamoPolizza']);
    TitoloReport := TitoloReport + ' - ramo: ' + AParameterList.Values['RamoPolizzaNome'];
  end;

  // if not CBgruppi.Checked then
  // begin
  // if Qgruppi.RecordCount > 0 then
  // begin
  // tSql.Add('and clienti.gruppo= ' + Qgruppi.FieldByName('cod_gruppo').AsString);
  // gruppo := ' - gruppo cli.: ' + Qgruppi.FieldByName('descrizione').AsString;
  // end;
  // end;

  // if not CBconvenzioni.Checked then
  // begin
  // if Qconvenzioni.RecordCount > 0 then
  // begin
  // tSql.Add('and polizze.convenzione= ' + Qconvenzioni.FieldByName('cod_convenzione').AsString);
  // convenzione := ' - convenz.: ' + Qconvenzioni.FieldByName('DESCRIZIONE').AsString;
  // end;
  // end;

  // filtro su tipo polizza
  if AParameterList.Values['TipoPolizza'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND POL.RIF_TIPO_POL = ' + AParameterList.Values['TipoPolizza']);
    TitoloReport := TitoloReport + ' - tipo polizza: ' + AParameterList.Values['TipoPolizzaNome'];
    // tipo_polizza := ' - tipo polizza: ' + Qtipo_pol.FieldByName('DESCRIZIONE').AsString;
  end;

  // filtro su compagnia
  if AParameterList.Values['Compagnia'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND POL.RIF_COMPAGNIA = ' + AParameterList.Values['Compagnia'] + ')');
    TitoloReport := TitoloReport + ' - della mandante: ' + UniMainModule.DMdatiAge.QCompagnie.FieldByName
      ('nome').AsString;
  end;


  // if not CBappoggio.Checked then
  // begin
  // tSql.Add('   and  ');
  // tSql.Add('( polizze.appoggiata_a= ' + IntToStr(Qappoggio.FieldByName('cod_appoggio').AsInteger) + ')');
  // appoggio := ' - struttura di appoggio: ' + Qappoggio.FieldByName('NOME').AsString;
  // end;

  // ordinamento
  QStatisticheContratti.sql.Add('ORDER BY');
  if AParameterList.Values['Ordinamento'] = 'Contraente' then
    QStatisticheContratti.sql.Add('CONTRAENTE')
  else
    if AParameterList.Values['Ordinamento'] = 'Polizza' then
      QStatisticheContratti.sql.Add('N_POLIZZA')
    else
      QStatisticheContratti.sql.Add('SCADENZA');

  QStatisticheContratti.Open;
end;

procedure TDMDatiReport.PreparaQueryStatisticheIncassi(AParameterList: TStrings; ATipoStampaIncassi: TipoStampaIncassi;
  bExportToExcel: Boolean);
var
  lProduttore: string;

  function CheckPromoter(AQuery: string): string;
  begin
    if AParameterList.Values['Promoter'] = '' then
    begin
      lProduttore := '';
      Result      := AQuery;
    end
    else
    begin
      lProduttore := 'Prod';
      Result      := AQuery + '_PROD';
    end;
  end;

  procedure CheckPeriodo;
  begin
    if AParameterList.Values['Periodo'] <> '' then
    begin
      QStatisticheContratti.sql.Append('AND DATA_PAG BETWEEN :DATADA AND :DATAA');
    end;
  end;

  procedure ExportToExcelAndSend;
  var
    FilesFolderPath: string;
    absFileName: string;
  begin
    FilesFolderPath := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
    ReportFileName  := ReportFileName + '_' + FormatDateTime('ddmmyyyhhmmss', Now);
    absFileName     := TPath.Combine(FilesFolderPath, ChangeFileExt(ReportFileName, '.csv'));
    QStatisticheContratti.ExportTable(absFileName, ';');
    UniSession.SendFile(absFileName);
    sleep(2000);
    TFile.delete(absFileName);

  end;

begin
  QStatisticheContratti.Close;
  QStatisticheContratti.sql.Clear;
  lProduttore := '';
  case ATipoStampaIncassi of
    tsiTotIncassi:
      begin
        QStatisticheContratti.sql := jvmstrh.StringsByName[CheckPromoter('INCASSI')];
        ReportFileName            := 'ElelncoIncassi' + lProduttore;
        ActualReport              := frxIncassi;
        CheckPeriodo;
        QStatisticheContratti.sql.Append('GROUP BY DESCRIZIONE');
      end;

    tsiDettIncassi:
      begin
        QStatisticheContratti.sql := jvmstrh.StringsByName[CheckPromoter('DETT_INCASSI')];
        ReportFileName            := 'DettaglioIncassi' + lProduttore;
        ActualReport              := frxDettagli;
        CheckPeriodo;
        QStatisticheContratti.sql.Append('ORDER BY FOGLIO_CASSA, DATA_PAG');
      end;

    tsiTotPolizzeMese:
      begin
        QStatisticheContratti.sql := jvmstrh.StringsByName[CheckPromoter('POLIZZE')];
        ReportFileName            := 'ElelncoPolizze' + lProduttore;
        ActualReport              := frxPolizze;
        CheckPeriodo;
        QStatisticheContratti.sql.Append('GROUP BY ANNO, MESE, DESCRIZIONE');
      end;

    tsiDettPolizze:
      begin
        QStatisticheContratti.sql := jvmstrh.StringsByName[CheckPromoter('DETT_POLIZZE')];
        ReportFileName            := 'DettaglioPolizze' + lProduttore;
        ActualReport              := frxDettagli;
        CheckPeriodo;
        QStatisticheContratti.sql.Append('ORDER BY FOGLIO_CASSA, DATA_PAG');
      end
  else
    begin
      QStatisticheContratti.sql        := jvmstrh.StringsByName['DETT_INCASSI_PER_PROMOTER'];
      ReportFileName                   := 'DettaglioIncassiProm' + lProduttore;
      ActualReport                     := frxDettagliPromoter;
      ActualReport.Variables['titolo'] := QuotedStr(' ');
      CheckPeriodo;
      QStatisticheContratti.sql.Append('ORDER BY PR.SIGLA, DATA_PAG');
    end;

  end;

  // QStatisticheContratti.ParamByName('DATADA').DataType := ftString;
  // QStatisticheContratti.ParamByName('DATAA').DataType  := ftString;

  if AParameterList.Values['Periodo'] <> '' then
  begin
    RecDate                                              := DataToSql(AParameterList.Values['Periodo']);
    QStatisticheContratti.ParamByName('DATADA').AsString := RecDate.DataDa;
    QStatisticheContratti.ParamByName('DATAA').AsString  := RecDate.DataA;
  end;

  if AParameterList.Values['Promoter'] <> '' then
  begin
    QStatisticheContratti.ParamByName('INCASSATA_DA').AsInteger := StrToInt(AParameterList.Values['Promoter']);
    ActualReport.Variables['titolo'] := QuotedStr('(promoter: ' + AParameterList.Values['PromoterNome']);
  end;

  QStatisticheContratti.ParamByName('comp').AsInteger := StrToIntDef(AParameterList.Values['Compagnia'], 0);

  QStatisticheContratti.Open;
  if not QStatisticheContratti.IsEmpty then
  begin
    if bExportToExcel then
    begin
      ExportToExcelAndSend;

    end
    else
      AnteprimaStatisticheIncassi(ActualReport);
  end
  else
    raise EPolizzaError.create(CERR_QUIETANZE_NOT_FOUND,MSG_NO_DATA_FOUND);

end;
procedure TDMDatiReport.ExportToExcelAndSend(ADataToExport: TDBISAMQuery);
var
  FilesFolderPath: string;
  absFileName: string;
begin
  FilesFolderPath := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  ReportFileName  := ReportFileName + '_' + FormatDateTime('ddmmyyyhhmmss', Now);
  absFileName     := TPath.Combine(FilesFolderPath, ChangeFileExt(ReportFileName, '.csv'));
  ADataToExport.ExportTable(absFileName, ';');
  UniSession.SendFile(absFileName);
  Sleep(2000);
  TFile.delete(absFileName);

end;
function TDMDatiReport.DataToSql(ADate: string): TRecDate;
var
  DateStrArray: TStringDynArray;
begin
  if ADate <> '' then
  begin
    DateStrArray  := SplitString(ADate, ';');
    Result.DataDa := DateStrArray[0];
    Result.DataA  := DateStrArray[1];
  end
  else
  begin
    Result.DataDa := '';
    Result.DataA  := '';
  end;
end;

procedure TDMDatiReport.PreparaQueryStatisticheTitoli(AParameterList: TStrings);
var
  RecDate: TRecDate;
  lDataDa, lDataA: string;

  function getDataStr(ADateStr: string): string;
  var
    lDate: TDate;
    YYYY, MM, DD: word;
  begin
    YYYY  := StrToInt(Copy(ADateStr, 1, 4));
    MM    := StrToInt(Copy(ADateStr, 6, 2));
    DD    := StrToInt(Copy(ADateStr, 9, 2));
    lDate := encodeDate(YYYY, MM, DD);
    DateTimeToString(Result, 'dd/mm/yyyy', lDate);
  end;

begin
  TitoloReport := 'Elenco titoli';

  QStatisticheContratti.Close;
  QStatisticheContratti.sql.Clear;
  QStatisticheContratti.sql.Add('SELECT SCA.PROGRESSIVO, SCA.DECORRENZA, SCA.DATA_PAG, SCA.N_POLIZZA,SCA.DENOMINAZ, ');
  QStatisticheContratti.sql.Add('SCA.LORDO LORDO, COMP.NOME, COMP.CODICE_AGENZIA, CLI.PROMOTER,');
  QStatisticheContratti.sql.Add('SCA.N_POLIZZA, SCA.DENOMINAZ, SCA.LORDO LORDO,');
  QStatisticheContratti.sql.Add('COMP.NOME, COMP.CODICE_AGENZIA, CLI.PROMOTER, SCA.COD_SCADENZA,');
  QStatisticheContratti.sql.Add('COMP.COD_COMPAGNIA , TIPO_TITOLO, VALUTA, DATA_REG_PAG, PROD.SIGLA AS SIGLA_PROD,');
  QStatisticheContratti.sql.Add('SCA.PROVVIGIONI_SUB, POL.INPDF,');

  QStatisticheContratti.sql.Add(ifthen(UniMainModule.IsUtentePromoter, 'SCA.PROVVIGIONI / 100 * PROD.PERC_PROVV_AGE as provvigioni',
    'SCA.PROVVIGIONI'));

  QStatisticheContratti.sql.Add('FROM SCADENZE SCA');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN COMPAGNI COMP ON COMP.COD_COMPAGNIA = SCA.RIF_COD_COMPAGNIA');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN POLIZZE POL ON POL.COD_POLIZZA = SCA.RIF_COD_POLIZZA');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN CLIENTI CLI ON CLI.COD_CLIENTE = POL.COD_CLI');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN APPOGGIO APP ON POL.APPOGGIATA_A = APP.COD_APPOGGIO');
  QStatisticheContratti.sql.Add('LEFT OUTER JOIN PRODUTT PROD ON CLI.PROMOTER = PROD.COD_PRODUTTORE');
  QStatisticheContratti.sql.Add('WHERE 1 = 1');
  // tsql.Add('where stato<>''C''');
  // if PolizzeMorte then
  // QStatisticheContratti.sql.Add('WHERE STATO = ''C''')
  // else
  // QStatisticheContratti.sql.Add('WHERE STATO = ''V''');
  // filtro su collaboratore
  // livello := 0;

  if UniMainModule.IsUtentePromoter then
  begin
    QStatisticheContratti.sql.Add('AND  (FATTA_DA =' + UniMainModule.UtentePromoter.ToString);
    QStatisticheContratti.sql.Add('OR CLI.PROMOTER =' + UniMainModule.UtentePromoter.ToString + ')');
    // tsql.Add('   or ifnull(fatta_da,1=1,1=0)');
    TitoloReport := TitoloReport + '- collaboratore: ' + UniMainModule.UtenteNome;
  end
  else
  begin
    if AParameterList.Values['Promoter'] <> '' then
    begin
      QStatisticheContratti.sql.Add('AND  (FATTA_DA =' + AParameterList.Values['Promoter']);
      QStatisticheContratti.sql.Add('OR CLI.PROMOTER =' + AParameterList.Values['Promoter'] + ')');
      TitoloReport := TitoloReport + '- collaboratore: ' + AParameterList.Values['PromoterNome'];
      // inc(livello);
    end;
  end;

  // parte relativa all'eventuale sottolivello
  if AParameterList.Values['SubPromoter'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND  (sub_promoter =' + AParameterList.Values['SubPromoter']);
    QStatisticheContratti.sql.Add('OR CLI.sub_PROMOTER =' + AParameterList.Values['SubPromoter'] + ')');
    TitoloReport := TitoloReport + '- sub-collaboratore: ' + AParameterList.Values['SubPromoterNome'];
  end;

  if AParameterList.Values['Cap'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND CLI.CAP LIKE ' + QuotedStr(AParameterList.Values['Cap'] + '%'));
    TitoloReport := TitoloReport + ' - cap: ' + AParameterList.Values['Cap'];
    // inc(livello);
  end;

  if AParameterList.Values['Citta'] <> '' then
  begin
    // filtro sulla citta
    QStatisticheContratti.sql.Add('AND CLI.CITTA = ' + QuotedStr(AParameterList.Values['Citta']));
    TitoloReport := TitoloReport + ' - citt�: ' + AParameterList.Values['Citta'];
    // inc(livello);
  end;

  if AParameterList.Values['Provincia'] <> '' then
  begin
    // filtro sulla ptovincia
    QStatisticheContratti.sql.Add('AND CLI.PROVINCIA = ' + QuotedStr(AParameterList.Values['Provincia']));
    TitoloReport := TitoloReport + ' - prov.: ' + AParameterList.Values['Provincia'];
    // inc(livello);
  end;

  if AParameterList.Values['DataEffetto'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataEffetto']);
    QStatisticheContratti.sql.Add('AND DECORRENZA BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)');
    TitoloReport := TitoloReport + ' per il periodo dal ' + SQLDataToStr(RecDate.DataDa) + ' al ' +
      SQLDataToStr(RecDate.DataA);
  end;

  if AParameterList.Values['DataScadenza'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataScadenza']);
    QStatisticheContratti.sql.Add('AND SCADENZA BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)');

  end;

  if AParameterList.Values['DataAnnullamento'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataScadenza']);
    QStatisticheContratti.sql.Add('AND DATA_MORTE BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)');
  end;

  if AParameterList.Values['DataIncasso'] <> '' then
  begin
    RecDate := DataToSql(AParameterList.Values['DataIncasso']);
    QStatisticheContratti.sql.Add('AND SCA.DATA_PAG BETWEEN CAST(' + QuotedStr(RecDate.DataDa) + ' AS DATE) AND CAST(' +
      QuotedStr(RecDate.DataA) + ' AS DATE)');
    TitoloReport := TitoloReport + ' per il periodo dal ' + SQLDataToStr(RecDate.DataDa) + ' al ' +
      SQLDataToStr(RecDate.DataA);
  end;

  // filtro su ramo
  if AParameterList.Values['RamoPolizza'] <> '' then
  begin
    QStatisticheContratti.sql.Add(' AND POL.RIF_COD_RAMO = ' + AParameterList.Values['RamoPolizza']);
    TitoloReport := TitoloReport + ' - ramo: ' + AParameterList.Values['RamoPolizzaNome'];
  end;

  // filtro su tipo polizza
  if AParameterList.Values['TipoPolizza'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND POL.RIF_TIPO_POL = ' + AParameterList.Values['TipoPolizza']);
    TitoloReport := TitoloReport + ' - tipo polizza: ' + AParameterList.Values['TipoPolizzaNome'];
    // tipo_polizza := ' - tipo polizza: ' + Qtipo_pol.FieldByName('DESCRIZIONE').AsString;
  end;

  // filtro su compagnia
  if AParameterList.Values['Compagnia'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND POL.RIF_COMPAGNIA = ' + AParameterList.Values['Compagnia'] + ')');
    TitoloReport := TitoloReport + ' - della mandante: ' + UniMainModule.DMdatiAge.QCompagnie.FieldByName
      ('nome').AsString;
  end;

  if AParameterList.Values['StatoPagamento'] <> '' then
  begin
    QStatisticheContratti.sql.Add('AND SCA.STATO ' + ifthen(AParameterList.Values['StatoPagamento'] = 'Incassati', '= ',
      '<> ') + '''P''');
    if AParameterList.Values['StatoPagamento'] <> 'Incassati' then
       QStatisticheContratti.sql.Add('AND SCA.tipo_titolo = ''20'' ');
  end;

  // ordinamento
  QStatisticheContratti.sql.Add('ORDER BY');
  if AParameterList.Values['Ordinamento'] = 'Decorrenza' then
    QStatisticheContratti.sql.Add('SCA.DECORRENZA')
  else
    if AParameterList.Values['Ordinamento'] = 'Denominazione' then
      QStatisticheContratti.sql.Add('SCA.DENOMINAZ')
    else
      QStatisticheContratti.sql.Add('SCA.N_POLIZZA');

  QStatisticheContratti.Open;
end;

function TDMDatiReport.fai_cbq(pol: string; scad: TdateTime): string;
var
  age, pag: string;
  NPolizza: integer;
begin
  // genera il codice a barre per l'appendice / quietanza
  // 35214
  // '80'  identificatore fisso per le quietanze  OPPURE  '60'  identificatore fisso per le quiet/app da direzione 15
  // 02 che indica il numero di pagine del documento
  // 01   numero della pagina corrente
  // 000  codice agenzia
  // nnnnnnn 7 cifre paddatate a sinistra con 0 per il numero di polizza
  // aaaammgg  data di scadenza all'americana
  // esempio  35214 80 0201 159 0123456 20140501    quietanza
  // esempio  35214 60 0201 159 0123456 20140501    quiet/app

  Result := '35214';

  Result := Result + '60'; // quiet/appendice 15
  age    := UniMainModule.DMdatiAge.SLPdati_age.agenzia;

  // result:=result + PadL(p1,'0',2) + PadL(p2,'0',2);
  // le pagine sono sempre a coppie: se numero dispari � la pagina 1 se numero pari � la pagina 2
  pag      := '01';
  Result   := Result + '01' + pag;
  Result   := Result + age;
  NPolizza := StrToIntDef(pol, 1);
  Result   := Result + Format('%7.7d', [NPolizza]);
  Result   := Result + dtos(scad);
end;

procedure TDMDatiReport.AnteprimaStatisticheClienti;
begin
  if QStatisticheClienti.IsEmpty then
    raise ESLP4WebException.create(MSG_NO_DATA_FOUND);
  ReportFileName                       := 'ElelncoClienti';
  frxClienti.Variables['titoloReport'] := QuotedStr(TitoloReport);
  mostraAnteprimaStatistica(frxClienti);
end;

procedure TDMDatiReport.AnteprimaStatisticheContratti(MostraOggInGaranzia: Boolean);
begin
  if QStatisticheContratti.IsEmpty then
    raise ESLP4WebException.create(MSG_NO_DATA_FOUND);

  ReportFileName := 'ElelncoContratti';

  if MostraOggInGaranzia then
  begin
    // Tassicurati.Open;
    QStatisticheAssicurati.Open;
    frxContratti.Variables['assicurati'] := QuotedStr('S');
  end
  else
    frxContratti.Variables['assicurati'] := QuotedStr('N');
  frxContratti.Variables['sospesi']      := ifthen(MostraPolizzeSospese, QuotedStr('S'), QuotedStr('N'));
  frxContratti.Variables['titoloReport'] := QuotedStr(TitoloReport);

  mostraAnteprimaStatistica(frxContratti);

end;

procedure TDMDatiReport.AnteprimaStatisticheIncassi(AReport: TfrxReport);
var
  mTitoloReport: tfrxMemoView;

begin
  mTitoloReport := ActualReport.findobject('memo4') as tfrxMemoView;
  // mTitoloReport.Text := TitoloReport;
  mostraAnteprimaStatistica(ActualReport);

end;

procedure TDMDatiReport.AnteprimaStatisticheTitoli;
var
  mTitoloReport: tfrxMemoView;

begin
  if QStatisticheContratti.IsEmpty then
    raise ESLP4WebException.create(MSG_NO_DATA_FOUND);

  ReportFileName     := 'ElelncoInsoluti';
  mTitoloReport      := frxrRepInsoluti.findobject('memo4') as tfrxMemoView;
  mTitoloReport.Text := TitoloReport;

  mostraAnteprimaStatistica(frxrRepInsoluti);

end;

procedure TDMDatiReport.AzzeraCampiReport(report: TfrxReport);
var
  indice: integer;
begin

  for indice := 0 to report.Variables.count - 1 do
  begin
    report.Variables.Items[indice].Value := QuotedStr('');
  end;
  // frxPolizzaGen.Variables['versione']  := QuotedStr(leggiVersione);
  report.Variables['versione']  := QuotedStr('1.0.0');
  report.Variables['facsimile'] := QuotedStr('N');
  // frxPolizzaGen.Variables['RAMO']      := QuotedStr(ramo_slp);
  report.Variables['RAMO']     := QuotedStr('17');
  report.Variables['barcode1'] := QuotedStr('00');
  report.Variables['barcode2'] := QuotedStr('00');

end;

initialization

registerClass(TDMDatiReport);

end.
