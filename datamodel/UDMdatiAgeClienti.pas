unit UDMdatiAgeClienti;

interface

uses
  System.SysUtils, System.Classes, System.Rtti, Data.DB, dbisamtb, UDMMaster, UQueryElencoIntf,
  UQueryEditIntf, UShowAppendiceRec, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TEnOrdClienti  = (ocNome, ocReferente, ocCitta);
  TEnTipoCliente = (tcTutti, tcAttivi, tcPotenziali, tcExClienti);
  TEnTipoTelefonoMail = (ttmTelefono, ttmFax, ttmEmail, ttmIndirizzoSito, ttmCellulare);

  TDMdatiAgeClienti = class(TDMMaster, IQueryElenco, IQueryEdit)
    QAssicurati: TDBISAMQuery;
    DSAssicurati: TDataSource;
    Qclienti: TDBISAMQuery;
    DSclienti: TDataSource;
    DSClienteTitoli: TDataSource;
    QClienteTitoli: TDBISAMQuery;
    QeditCliente: TDBISAMQuery;
    DSeditCliente: TDataSource;
    Qlivello_cli: TDBISAMQuery;
    DSlivello_cli: TDataSource;
    QFindReferente: TDBISAMQuery;
    QClienteHasPolizze: TDBISAMQuery;
    QCancellaCliente: TDBISAMQuery;
    QUpdCliente: TDBISAMQuery;
    Qprofessioni: TDBISAMQuery;
    QProfessioniAll: TDBISAMQuery;
    QTelefoniMail: TDBISAMQuery;
    QDocuments: TDBISAMQuery;
    fdmtblTipo: TFDMemTable;
    fdmtblTipoTipoTelMail: TIntegerField;
    fdmtblTipoTelMailDescr: TStringField;
    QTelefoniMailNumero: TStringField;
    QTelefoniMailDescrizione: TStringField;
    QTelefoniMailCategoria: TStringField;
    QTelefoniMailTitolo: TStringField;
    QTelefoniMailcod_cli: TIntegerField;
    QTelefoniMailrif_operatore: TIntegerField;
    QTelefoniMailDescrTitolo: TStringField;
    QTelefoniMailvalore_default: TStringField;
    QTipoDocumento: TDBISAMQuery;
    QContattoCliente: TDBISAMQuery;
    QTipoContatto: TDBISAMQuery;
    QContattoClienteDATA: TDateField;
    QContattoClienteTIPO: TIntegerField;
    QContattoClienteMOTIVO: TStringField;
    QContattoClienteFATTO_DA: TStringField;
    QContattoClientetesto: TMemoField;
    QContattoClientecategoria: TStringField;
    QContattoClientemodifica: TDateField;
    QContattoClientereferente: TStringField;
    QContattoClienterif_operatore: TIntegerField;
    QContattoClienteID_GEN_SLP_AGE: TIntegerField;
    QContattoClienteDescrTipoCont: TStringField;
    QMailPref: TDBISAMQuery;
    QUpdTelefoniMail: TDBISAMQuery;
    QTelefoniMailcod_telefoni: TIntegerField;
    QTelefoni: TDBISAMQuery;
    QMail: TDBISAMQuery;
    dsTelefoni: TDataSource;
    dsMail: TDataSource;
    QTelefoniMailIDGENSLPAGE: TIntegerField;
    QContattoClienteCOD_CLI: TIntegerField;
    Qins_cli_new: TDBISAMQuery;
    QcontrattiCliente: TDBISAMQuery;
    DateField1: TDateField;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    MemoField1: TMemoField;
    StringField4: TStringField;
    DateField2: TDateField;
    StringField5: TStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QeditClienteBeforePost(DataSet: TDataSet);
    procedure QeditClienteNewRecord(DataSet: TDataSet);
    procedure QeditClienteAfterOpen(DataSet: TDataSet);
    procedure QTelefoniMailNewRecord(DataSet: TDataSet);
    procedure QTelefoniMailBeforePost(DataSet: TDataSet);
    procedure QeditClienteAfterPost(DataSet: TDataSet);
    procedure QeditClienteAfterCancel(DataSet: TDataSet);
    procedure QDocumentsNewRecord(DataSet: TDataSet);
    procedure QDocumentsBeforePost(DataSet: TDataSet);
    procedure QContattoClienteNewRecord(DataSet: TDataSet);
    procedure QTelefoniMailAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    QPolSLPCli: TDBISAMQuery;

    procedure CodFiscaleChanged(Sender: TField);

  public
    { Public declarations }
    function GetReferenteDescription(ACodReferente: integer): string;
    function posizionaReferente(ACodReferente: integer): string;
    // Interfaccia IQueryElenco
    // procedure EseguiQuery(TipoOrdinamento: integer; TipoFiltro: integer = 0; ValToSearch: string = '';
    // StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0); override;

    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 1); override;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); override;

    procedure ApriTelefoniDocumenti(CodCliente: integer);

    procedure AggProfessioneAttivita(CodCliente, Professione: integer; DescrAttivita: string);
    function canDeleteCli(CodCli: integer): Boolean;
    procedure CancellaCliente(CodCli: integer);
    function mustShowDenominazione: Boolean;
    function getDescrizProfessione(cod_professione: integer): string;
    function getTelefonoMailpref(CodCli: integer; TipoDoc: TEnTipoTelefonoMail): string;
    function NeedGenerateAppendice: Boolean;
    function PrepareAppendiceCliente(var AppRec: TShowAppendiceRec): string;
    function ins_cli_new: integer;
    procedure set_post_ins_cliente(attiva: Boolean);
  end;

const
  MAIL_REG_EXPR = '[A-z0-9\.\+_-]+@[A-z0-9\._-]+\.[A-z]{2,6}';

implementation

uses libreria, System.StrUtils, MainModule, UCodiciErroriPolizza, UPolizzaExceptions, System.Math,
  System.RegularExpressions;
{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}
{ TDMdatiAgeClienti }

procedure TDMdatiAgeClienti.AggProfessioneAttivita(CodCliente, Professione: integer; DescrAttivita: string);
begin
  QUpdCliente.ParamByName('CODCLIENTE').AsInteger   := CodCliente;
  QUpdCliente.ParamByName('PROFESSIONE').AsInteger  := Professione;
  QUpdCliente.ParamByName('DESCRATTIVITA').AsString := DescrAttivita;
  QUpdCliente.ExecSQL;
end;

procedure TDMdatiAgeClienti.ApriTelefoniDocumenti(CodCliente: integer);
begin
  QTelefoniMail.Close;
  QTelefoniMail.ParamByName('COD_CLI').AsInteger := CodCliente;
  QTelefoniMail.Open;

  QTelefoni.Close;
  QTelefoni.ParamByName('COD_CLI').AsInteger := CodCliente;
  QTelefoni.Open;

  QMail.Close;
  QMail.ParamByName('COD_CLI').AsInteger := CodCliente;
  QMail.Open;

  QDocuments.Close;
  QDocuments.ParamByName('COD_CLI').AsInteger := CodCliente;
  QDocuments.Open;

  QContattoCliente.Close;
  QContattoCliente.ParamByName('COD_CLI').AsInteger := CodCliente;
  QContattoCliente.Open;

  QTipoContatto.Open;

end;

procedure TDMdatiAgeClienti.CancellaCliente(CodCli: integer);
var lRecNumber: Integer;
begin
  lRecNumber := ifThen((Qclienti.RecNo -1) > 0, Qclienti.RecNo -1, 1);
  QCancellaCliente.ParamByName('COD_CLIENTE').AsInteger := CodCli;
  QCancellaCliente.ExecSQL;
  PosizionaElenco(Qclienti, lRecNumber);
end;

function TDMdatiAgeClienti.canDeleteCli(CodCli: integer): Boolean;
begin
  QClienteHasPolizze.Close;
  QClienteHasPolizze.ParamByName('CODCLI').AsInteger := CodCli;
  QClienteHasPolizze.Open;
  Result := true;
  while not QClienteHasPolizze.Eof do
  begin
    if QClienteHasPolizze.FieldByName('NUM_POLIZZE').AsInteger > 0 then
    begin
      Result := false;
      Break;
    end;
    QClienteHasPolizze.Next;
  end;
end;

procedure TDMdatiAgeClienti.CodFiscaleChanged(Sender: TField);
var
  nato_il: TdateTime;
  nato_a: string;
begin
  // controlla se devi istanziare i dati relativi a nato_a e nato_il
  if (Sender.AsString > '') then
  begin
    if verificacodicefiscale(Sender.AsString) then
    begin
      if (QeditCliente.FieldByName('NATO_A').AsString = '') or
        (QeditCliente.FieldByName('NATO_A').AsDateTime = MinDateTime) then
      begin
        UniMainModule.DMdatiAge.decodeCF(Sender.AsString, nato_il, nato_a);
        QeditCliente.FieldByName('NATO_IL').AsDateTime := nato_il;
        QeditCliente.FieldByName('NATO_A').AsString    := nato_a;
      end;
      // if (operazione = 'INS') then
      // begin
      // // verifica che il codice fiscale non sia gia' inserito
      // tcli := '';
      // if DMdatiAge.cli_duplicato(tcli, 'x', Ecod_fisc.Text, 'x', DMdatiAge.Tclienti.fieldbyname('COD_CLIENTE')
      // .asInteger) then
      // begin
      // application.MessageBox(pchar('Il codice fiscale inserito � gi� presente in archivio ! Vedi cliente ' + tcli),
      // 'Attenzione', mb_OK);
      //
      // end;
      // end;
    end
    else
    begin
      // MessageBox(0, 'Il Codice Fiscale inserito � errato.', 'Attenzione', MB_ICONWARNING or MB_OK);
      raise EPolizzaError.Create(CERR_CLI_CODFISC_ERR, MSG_CLI_CODFISC_ERR);
    end;
  end;

end;

procedure TDMdatiAgeClienti.DataModuleCreate(Sender: TObject);
begin
  inherited;
  OrdList.Append('Denominaz');
  OrdList.Append('Referente');
  OrdList.Append('Citta');

  fdmtblTipo.CreateDataSet;
  fdmtblTipo.edit;
  fdmtblTipo.AppendRecord([0, 'Telefono']);
  fdmtblTipo.AppendRecord([1, 'Fax']);
  fdmtblTipo.AppendRecord([2, 'E-mail']);
  fdmtblTipo.AppendRecord([3, 'Indirizzo sito']);
  fdmtblTipo.AppendRecord([4, 'Cellulare']);

end;

// procedure TDMdatiAgeClienti.EseguiQuery(TipoOrdinamento: integer; TipoFiltro: integer; ValToSearch: string;
// StatoPagamento: integer; DirezioneOrdinamento: integer; CodCollaboratore: integer);
//
// var
// cc: integer;
// begin
// strOrdCol := '';
// if Qclienti.state = dsInactive then
// Qclienti.Open;
//
// cc := Qclienti.FieldByName('cod_cliente').AsInteger;
// Qclienti.Close;
// Qclienti.SQL.Clear;
// Qclienti.SQL.Add('select cod_cliente, denominaz, referente, indirizzo, citta ');
// Qclienti.SQL.Add('from clienti cl left join produtt pr on cl.promoter = pr.COD_PRODUTTORE');
// Qclienti.SQL.Add('where denominaz > '''' ');
// Qclienti.SQL.Add('and stato <> -99 ');
//
// if UniMainModule.DMdatiAge.is_utente_promoter then
// begin
// Qclienti.SQL.Add('and promoter = ' + UniMainModule.utentePromoter.ToString);
// if UniMainModule.UtenteSubPromoter>0 then
// Qclienti.SQL.Add('and sub_promoter = ' + UniMainModule.UtenteSubPromoter.ToString);
// end;
//
// if ValToSearch > '' then
// Qclienti.SQL.Add('AND ' + strOrdCol + ' LIKE ''' + ValToSearch + '%''');
//
// case TEnTipoCliente(TipoFiltro) of
// tcAttivi:
// Qclienti.SQL.Add('and stato > 0'); // attivi
// tcPotenziali:
// Qclienti.SQL.Add('and stato < 0'); // potenziali
// tcExClienti:
// Qclienti.SQL.Add('and stato = 0'); // ex-clienti
// end;
//
// strOrdCol := OrdList[TipoOrdinamento];
// Qclienti.SQL.Add('order by ' + strOrdCol + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));
//
// Qclienti.Open;
// Qclienti.Locate('cod_cliente', cc, []);
//
// end;

procedure TDMdatiAgeClienti.EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
  StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0;
  codCompagnia: integer = 1);
var
  cc: integer;
begin
  if Qclienti.state = dsInactive then
    Qclienti.Open;

  cc := Qclienti.FieldByName('cod_cliente').AsInteger;
  Qclienti.Close;
  Qclienti.SQL.Clear;
  Qclienti.SQL.Add('select cod_cliente, denominaz, referente, indirizzo, citta, pr.sigla, spr.sigla as siglaSubColl ');
  Qclienti.SQL.Add('from clienti cl left join produtt pr on pr.COD_PRODUTTORE = cl.promoter');
  Qclienti.SQL.Add('                left join produtt spr on spr.COD_PRODUTTORE = cl.sub_promoter');

  Qclienti.SQL.Add('where denominaz > '''' ');
  Qclienti.SQL.Add('and stato <> -99 ');
  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    Qclienti.SQL.Add('and promoter = ' + UniMainModule.utentePromoter.ToString);
    if UniMainModule.UtenteSubPromoter > 0 then
      Qclienti.SQL.Add('and sub_promoter = ' + UniMainModule.UtenteSubPromoter.ToString);
  end;

  if ValToSearch > '' then
    Qclienti.SQL.Add('AND ' + strOrdCol + ' LIKE ''' + ValToSearch + '%''');

  case TEnTipoCliente(TipoFiltro) of
    tcAttivi:
      Qclienti.SQL.Add('and stato > 0'); // attivi
    tcPotenziali:
      Qclienti.SQL.Add('and stato < 0'); // potenziali
    tcExClienti:
      Qclienti.SQL.Add('and stato = 0'); // ex-clienti
  end;

  Qclienti.SQL.Add('order by ' + ordFieldName + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  Qclienti.Open;
  Qclienti.Locate('cod_cliente', cc, []);

end;

function TDMdatiAgeClienti.getDescrizProfessione(cod_professione: integer): string;
begin
  Qprofessioni.Close;
  Qprofessioni.ParamByName('cod_professione').AsInteger := cod_professione;
  Qprofessioni.Open;
  Result := '';
  if (not Qprofessioni.IsEmpty) then
    Result := Qprofessioni.FieldByName('descriz').AsString;

end;

function TDMdatiAgeClienti.GetReferenteDescription(ACodReferente: integer): string;
begin
  QFindReferente.Close;
  QFindReferente.ParamByName('cod_cliente').AsInteger := ACodReferente;
  QFindReferente.Open;
  Result := IfThen(not QFindReferente.IsEmpty, QFindReferente.FieldByName('DENOMINAZ').AsString, '');
  QFindReferente.Close;
end;

function TDMdatiAgeClienti.getTelefonoMailpref(CodCli: integer; TipoDoc: TEnTipoTelefonoMail): string;
begin
  Result := '';
  QMailPref.Close;
  QMailPref.ParamByName('COD_CLI').AsInteger := CodCli;
  QMailPref.ParamByName('TITOLO').AsString   := IntToStr(Integer(TipoDoc));
  QMailPref.Open;
  if not QMailPref.IsEmpty then
    Result := QMailPref.FieldByName('NUMERO').AsString;
end;

function TDMdatiAgeClienti.ins_cli_new: integer;
begin
   Qins_cli_new.Close;
   Qins_cli_new.ParamByName('ID_AGE').AsInteger:= UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;
   Qins_cli_new.ParamByName('data').AsDateTime:= now;
   Qins_cli_new.Open;
   Result:=Qins_cli_new.FieldByName('LAST_CODCLIENTE').AsInteger;
   Qins_cli_new.Close;
end;

function TDMdatiAgeClienti.mustShowDenominazione: Boolean;
begin
  Result := (QeditCliente.FieldByName('COGNOME').AsString = '') and (QeditCliente.FieldByName('NOME').AsString = '');
end;

function TDMdatiAgeClienti.NeedGenerateAppendice: Boolean;
  function IsChangedIndirizzo: Boolean;
  begin
    Result := (QeditCliente.FieldByName('INDIRIZZO').OldValue <> QeditCliente.FieldByName('INDIRIZZO').NewValue) or
      (QeditCliente.FieldByName('CITTA').OldValue <> QeditCliente.FieldByName('CITTA').NewValue) or
      (QeditCliente.FieldByName('CAP').OldValue <> QeditCliente.FieldByName('CAP').NewValue) or
      (QeditCliente.FieldByName('PROVINCIA').OldValue <> QeditCliente.FieldByName('PROVINCIA').NewValue);
  end;

begin
  Result := IsChangedIndirizzo and UniMainModule.DMdatiAgePolizze.getPolizzeSLPCliente
    (QeditCliente.FieldByName('COD_CLIENTE').AsInteger, QPolSLPCli);
end;

procedure TDMdatiAgeClienti.PosizionaQuery(AValue: TValue);
begin
  QeditCliente.Close;
  QeditCliente.ParamByName('cod_cliente').AsInteger := AValue.AsInteger;
  QeditCliente.Open;
end;

function TDMdatiAgeClienti.posizionaReferente(ACodReferente: integer): string;
begin
  QFindReferente.Close;
  QFindReferente.ParamByName('RIF_REFERENTE').AsInteger := ACodReferente;
  QFindReferente.Open;
  if (not QFindReferente.IsEmpty) then
    Result := QFindReferente.FieldByName('DENOMINAZ').AsString;
end;

function TDMdatiAgeClienti.PrepareAppendiceCliente(var AppRec: TShowAppendiceRec): string;
begin
  AppRec.CodCompagnia      := QPolSLPCli.FieldByName('RIF_COMPAGNIA').AsInteger;
  AppRec.CodPolizza        := QPolSLPCli.FieldByName('cod_polizza').AsInteger;
  AppRec.NPolizza          := QPolSLPCli.FieldByName('n_polizza').AsString;
  AppRec.CodCliente        := QeditCliente.FieldByName('COD_CLIENTE').AsInteger;
  AppRec.NAppendice        := 0;
  AppRec.DataAppendice     := Date;
  AppRec.PTasse            := 0;
  AppRec.DtDalAppendice    := date;
  AppRec.DtAlAppendice     := Date;
  AppRec.ImportoModificato := false;
  AppRec.bCanPrint         := true;
  AppRec.Importo           := 0;
  AppRec.NextRata          := 0;
  AppRec.tipo_titolo       := '12';
  AppRec.denominazione     := QeditCliente.FieldByName('denominaz').AsString;
  AppRec.tipoApp           := 'CI';

  Result := UniMainModule.DMDatiReport.PrepareReportAppendiceCliente(QeditCliente, QPolSLPCli, AppRec);

end;

procedure TDMdatiAgeClienti.QContattoClienteNewRecord(DataSet: TDataSet);
begin
  QContattoCliente.FieldByName('COD_CLI').AsInteger := QeditCliente.FieldByName('COD_CLIENTE').AsInteger;
end;

procedure TDMdatiAgeClienti.QDocumentsBeforePost(DataSet: TDataSet);
begin
  QDocuments.FieldByName('DESCRIZIONE_DOC').AsString := QTipoDocumento.FieldByName('DESCRIZIONE').AsString;
end;

procedure TDMdatiAgeClienti.QDocumentsNewRecord(DataSet: TDataSet);
begin
  QDocuments.ParamByName('RIF_COD_CLI').AsInteger := QeditCliente.FieldByName('COD_CLIENTE').AsInteger;
end;

procedure TDMdatiAgeClienti.QeditClienteAfterCancel(DataSet: TDataSet);
begin
  if QTelefoniMail.state in [dsInsert, dsEdit] then
    QTelefoniMail.Cancel;

end;

procedure TDMdatiAgeClienti.QeditClienteAfterOpen(DataSet: TDataSet);
var
  CFiscField: TField;
begin
  CFiscField := QeditCliente.findfield('COD_FISC');
  if Assigned(CFiscField) then
    CFiscField.OnChange := CodFiscaleChanged;

end;

procedure TDMdatiAgeClienti.QeditClienteAfterPost(DataSet: TDataSet);
begin
  if QTelefoniMail.state in [dsInsert, dsEdit] then
    QTelefoniMail.Post;
end;

procedure TDMdatiAgeClienti.QeditClienteBeforePost(DataSet: TDataSet);

  procedure SegnalaDirezione;
  begin
    // cognome nome, codice fiscale p.iva, seguito da, indirizzo
    if (QeditCliente.state = dsEdit) and
      ((QeditCliente.FieldByName('COGNOME').OldValue <> QeditCliente.FieldByName('COGNOME').NewValue) or
      (QeditCliente.FieldByName('NOME').OldValue <> QeditCliente.FieldByName('NOME').NewValue) or
      (QeditCliente.FieldByName('COD_FISC').OldValue <> QeditCliente.FieldByName('COD_FISC').NewValue) or
      (QeditCliente.FieldByName('P_IVA').OldValue <> QeditCliente.FieldByName('P_IVA').NewValue) or
      (QeditCliente.FieldByName('PROMOTER').OldValue <> QeditCliente.FieldByName('PROMOTER').NewValue) or
      (QeditCliente.FieldByName('INDIRIZZO').OldValue <> QeditCliente.FieldByName('INDIRIZZO').NewValue)) then
      UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' MODD Np' + Qclienti.FieldByName('denominaz').AsString +
        '  cod:' + Qclienti.FieldByName('cod_cliente').AsString, 'CK', '', '', '', 'MOC', '', Date);
  end;

begin
  if ((QeditCliente.FieldByName('SESSO').AsString = 'M') or (QeditCliente.FieldByName('SESSO').AsString = 'F')) and
    ((QeditCliente.FieldByName('COGNOME').AsString = '') or (QeditCliente.FieldByName('NOME').AsString = '')) then
    raise EPolizzaError.Create(CERR_CLI_NO_SURNAME_NAME, MSG_CLI_NO_SURNAME_NAME);

  if (QeditCliente.FieldByName('SESSO').AsString = 'M') and (QeditCliente.FieldByName('COGNOME').AsString = '') then
    // raise EPolizzaError.Create('Attenzione, � obbligatorio inserire il cognome/ragione sociale del cliente !');
    raise EPolizzaError.Create(CERR_CLI_NO_SURNAME, MSG_CLI_NO_SURNAME);

  if (QeditCliente.FieldByName('COD_FISC').AsString = '') and (QeditCliente.FieldByName('P_IVA').AsString = '') then
    raise EPolizzaError.Create(CERR_CLI_CF_PIVA_MISSED, MSG_CLI_CF_PIVA_MISSED);

  if (QeditCliente.FieldByName('INDIRIZZO').AsString = '') or (QeditCliente.FieldByName('CITTA').AsString = '') or
    (QeditCliente.FieldByName('CAP').AsString = '') or (QeditCliente.FieldByName('PROVINCIA').AsString = '') then
    // raise EPolizzaError.Create('Attenzione, � obbligatorio inserire l''indirizzo in tutte le sue parti: Indirizzo + citt� + CAP + provincia !');
    raise EPolizzaError.Create(CERR_CLI_ADDRESS_INCOMPLETE, MSG_CLI_ADDRESS_INCOMPLETE);

  QeditCliente.FieldByName('DENOMINAZ').AsString := Trim(QeditCliente.FieldByName('COGNOME').AsString) + ' ' +
    Trim(QeditCliente.FieldByName('NOME').AsString);
  QeditCliente.FieldByName('MODIFICA').AsDateTime := now;

  SegnalaDirezione;
end;

procedure TDMdatiAgeClienti.QeditClienteNewRecord(DataSet: TDataSet);
begin
  QeditCliente.FieldByName('STATO').AsInteger := 0;
end;

procedure TDMdatiAgeClienti.QTelefoniMailAfterPost(DataSet: TDataSet);

  procedure UpdTelefoniMail;
  begin
    QUpdTelefoniMail.SQL.Clear;
    QUpdTelefoniMail.SQL.Append('UPDATE TELEFONI');
    QUpdTelefoniMail.SQL.Append('SET VALORE_DEFAULT = :VALORE_DEFAULT');
    QUpdTelefoniMail.SQL.Append('WHERE CATEGORIA = ''C''');
    QUpdTelefoniMail.SQL.Append('AND COD_CLI = :COD_CLI');
    QUpdTelefoniMail.SQL.Append('AND TITOLO = :TIPO');
    QUpdTelefoniMail.SQL.Append('AND COD_TELEFONI <> :COD_TELEFONI');

    QUpdTelefoniMail.ParamByName('COD_CLI').AsInteger      := QeditCliente.FieldByName('COD_CLIENTE').AsInteger;
    QUpdTelefoniMail.ParamByName('TIPO').AsString          := QTelefoniMailTitolo.AsString;
    QUpdTelefoniMail.ParamByName('COD_TELEFONI').AsInteger := QTelefoniMailcod_telefoni.AsInteger;
    QUpdTelefoniMail.ParamByName('VALORE_DEFAULT').AsString := 'N';
    QUpdTelefoniMail.ExecSQL;

    // setUpdTelefoniMail;
    // QUpdTelefoniMail.SQL.Append('AND COD_TELEFONI = :COD_TELEFONI');
    // QUpdTelefoniMail.ParamByName('COD_CLI').AsInteger      := QeditCliente.FieldByName('COD_CLIENTE').AsInteger;
    // QUpdTelefoniMail.ParamByName('TIPO').AsString          := QTelefoniMailTitolo.AsString;
    // QUpdTelefoniMail.ParamByName('COD_TELEFONI').AsInteger := QTelefoniMailcod_telefoni.AsInteger;
    // QUpdTelefoniMail.ParamByName('VALORE_DEFAULT').AsString := 'S';
    // QUpdTelefoniMail.ExecSQL;
  end;

begin
  if QTelefoniMail.FieldByName('VALORE_DEFAULT').AsString = 'S' then
  begin
    UpdTelefoniMail;
    QTelefoniMail.Close;
    QTelefoniMail.Open;
  end;

end;

procedure TDMdatiAgeClienti.QTelefoniMailBeforePost(DataSet: TDataSet);

  function CheckMail: Boolean;
  begin
    Result := TRegEx.IsMatch(QTelefoniMailNumero.AsString, MAIL_REG_EXPR);
  end;

begin
  if (QTelefoniMailTitolo.AsInteger = Integer(ttmEmail)) and not CheckMail then
    raise EPolizzaError.Create(CERR_INVALID_MAIL, MSG_INVALID_MAIL);

  // if not CheckCountTelMail then
  // raise EPolizzaError.Create(CERR_MAXTELPREF_GT_ONE, Format(MSG_MAXTELPREF_GT_ONE,
  // [QTelefoniMailDescrTitolo.AsString]));
end;

procedure TDMdatiAgeClienti.QTelefoniMailNewRecord(DataSet: TDataSet);
begin
  QTelefoniMailcod_cli.AsInteger  := QeditCliente.FieldByName('COD_CLIENTE').AsInteger;
  QTelefoniMailIDGENSLPAGE.AsInteger :=  UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;
  QTelefoniMailCategoria.AsString := 'C';
end;

procedure TDMdatiAgeClienti.set_post_ins_cliente(attiva: Boolean);
begin
   if attiva then QEditcliente.BeforePost := QeditClienteBeforePost
   else QeditCliente.BeforePost := nil;
end;

end.
