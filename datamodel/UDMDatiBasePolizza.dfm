inherited DMDatiBasePolizza: TDMDatiBasePolizza
  OldCreateOrder = True
  Height = 321
  Width = 466
  object QPolizze: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select * from POLIZZE'
      '--where ID_GEN_SLP_AGE = :codAgenzia')
    Params = <>
    Left = 16
    Top = 17
  end
  object QSLP_TPolizze: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select * from POLIZZE'
      '--where ID_GEN_SLP_AGE = :codAgenzia')
    Params = <>
    Left = 88
    Top = 17
  end
  object QInsPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'INSERT INTO SLP_TPOLIZZE'
      '('
      ' RIF_TIPO_POL,'
      ' SIGLA_POL,'
      '  CONTRAENTE,'
      '  DECORRENZA,'
      '  SCADENZA,'
      '  FRAZIONAM,'
      '  N_POLIZZA,'
      '  LORDO,'
      '  NETTO,'
      '  PTASSE,'
      '  ACCESSORI,'
      '  VALUTA,'
      '  TASSE,'
      '  NOTE,'
      '  RIF_COMPAGNIA,'
      '  COD_CLI,'
      '  FATTA_DA,'
      '  RIF_COD_RAMO,'
      '  STATO,'
      '  PROVVIGIONI,'
      '  PERC_PROVVIGIONI,'
      '  CLASSE,'
      '  RAMO,'
      '  AGENZIA,'
      '  SUB_AGE,'
      '  RATINO_1_RATA,'
      '  SCONTO_DURATA,'
      '  UTENTE,'
      '  DATA_INSERIMENTO,'
      '  NUM_ASSICURATI,'
      '  NETTO1,'
      '  ACCESSORI1,'
      '  INT_FRAZ,'
      '  IMPONIBILE1,'
      '  IMPOSTE1,'
      '  TOTALE1,'
      '  NETTO2,'
      '  ACCESSORI2,'
      '  RIMBORSO_SOST,'
      '  IMPONIBILE2,'
      '  IMPOSTE2,'
      '  TOTALE2,'
      '  COMBINAZIONE,'
      '  FORMA,'
      '  ESTENSIONE_OM,'
      '  N_POLIZZA_SOSTITUITA,'
      '  SCAD_POL_SOSTITUITA,'
      '  SCAD_PRIMA_RATA,'
      '  INDICIZZATA,'
      '  STATUS,'
      '  DATA_PERFEZ,'
      '  DATA_CANC,'
      '  VOLUME_AFFARI,'
      '  NUMERO_ASSICURATI,'
      '  TIPO_PROVVIGIONI,'
      '  PERC_ACCESSORI,'
      '  RAGIONE_SOC,'
      '  DATI1,'
      '  DATI2,'
      '  DATI3,'
      '  DATI4,'
      '  ESTENSIONI,'
      '  SIGLA_POL,'
      '  TIPO_PROV,'
      '  TIPOLOGIA,'
      '  DATA_EMISSIONE,'
      '  DATI_VARI,'
      '  ORDINE,'
      '  SPECIAL,'
      '  CODICE_IMPORTAZIONE,'
      '  INTERMEDIATA_DA,'
      '  INTERMEDIATA_DA_SIGLA,'
      '  CONVENZIONE,'
      '  RIF_REFERENTE,'
      '  VARFRP,'
      '  -- APP_REPORT,'
      '  CON_APPENDICE,'
      '  APP_NUMERO,'
      '  APP_DAL,'
      '  APP_AL,'
      '  APP_OGGETTO,'
      '  P_SCONTO,'
      '  INPDF,'
      '  SUB_PROMOTER,'
      '  DATI5,'
      '  N_S_P,'
      '  TIPO_EMISSIONE,'
      '  DT_LAST_MOD,'
      '  DT_PREVENTIVO,'
      '  TELEFONO,'
      '  EMAIL,'
      '  perAllegati,'
      '  ID_GEN_SLP_AGE,'
      '  descrizione,'
      '  sconto_durata,'
      '  sconto_app'
      ')'
      'VALUES ('
      '  :RIF_TIPO_POL,'
      '  :SIGLA_POL,'
      '  :CONTRAENTE,'
      '  :DECORRENZA,'
      '  :SCADENZA,'
      '  :FRAZIONAM,'
      '  :N_POLIZZA,'
      '  :LORDO,'
      '  :NETTO,'
      '  :PTASSE,'
      '  :ACCESSORI,'
      '  :VALUTA,'
      '  :TASSE,'
      '  :NOTE,'
      '  :RIF_COMPAGNIA,'
      '  :COD_CLI,'
      '  :FATTA_DA,'
      '  :RIF_COD_RAMO,'
      '  :STATO,'
      '  :PROVVIGIONI,'
      '  :PERC_PROVVIGIONI,'
      '  :CLASSE,'
      '  :RAMO,'
      '  :AGENZIA,'
      '  :SUB_AGE,'
      '  :RATINO_1_RATA,'
      '  :SCONTO_DURATA,'
      '  :UTENTE,'
      '  :DATA_INSERIMENTO,'
      '  :NUM_ASSICURATI,'
      '  :NETTO1,'
      '  :ACCESSORI1,'
      '  :INT_FRAZ,'
      '  :IMPONIBILE1,'
      '  :IMPOSTE1,'
      '  :TOTALE1,'
      '  :NETTO2,'
      '  :ACCESSORI2,'
      '  :RIMBORSO_SOST,'
      '  :IMPONIBILE2,'
      '  :IMPOSTE2,'
      '  :TOTALE2,'
      '  :COMBINAZIONE,'
      '  :FORMA,'
      '  :ESTENSIONE_OM,'
      '  :N_POLIZZA_SOSTITUITA,'
      '  :SCAD_POL_SOSTITUITA,'
      '  :SCAD_PRIMA_RATA,'
      '  :INDICIZZATA,'
      '  :STATUS,'
      '  :DATA_PERFEZ,'
      '  :DATA_CANC,'
      '  :VOLUME_AFFARI,'
      '  :NUMERO_ASSICURATI,'
      '  :TIPO_PROVVIGIONI,'
      '  :PERC_ACCESSORI,'
      '  :RAGIONE_SOC,'
      '  :DATI1,'
      '  :DATI2,'
      '  :DATI3,'
      '  :DATI4,'
      '  :ESTENSIONI,'
      '  :SIGLA_POL,'
      '  :TIPO_PROV,'
      '  :TIPOLOGIA,'
      '  :DATA_EMISSIONE,'
      '  :DATI_VARI,'
      '  :ORDINE,'
      '  :SPECIAL,'
      '  :CODICE_IMPORTAZIONE,'
      '  :INTERMEDIATA_DA,'
      '  :INTERMEDIATA_DA_SIGLA,'
      '  :CONVENZIONE,'
      '  :RIF_REFERENTE,'
      '  :VARFRP,'
      '  --:APP_REPORT,'
      '  :CON_APPENDICE,'
      '  :APP_NUMERO,'
      '  :APP_DAL,'
      '  :APP_AL,'
      '  :APP_OGGETTO,'
      '  :P_SCONTO,'
      '  :INPDF,'
      '  :SUB_PROMOTER,'
      '  :DATI5,'
      '  :N_S_P,'
      '  :TIPO_EMISSIONE,'
      '  :DT_LAST_MOD,'
      '  :DT_PREVENTIVO,'
      '  :TELEFONO,'
      '  :EMAIL,'
      '  :perAllegati,'
      '  :ID_GEN_SLP_AGE,'
      '  :descrizione,'
      '  :sconto_durata,'
      '  :sconto_app'
      ')')
    Params = <
      item
        DataType = ftInteger
        Name = 'RIF_TIPO_POL'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'CONTRAENTE'
      end
      item
        DataType = ftDate
        Name = 'DECORRENZA'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO'
      end
      item
        DataType = ftCurrency
        Name = 'PTASSE'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI'
      end
      item
        DataType = ftFixedChar
        Name = 'VALUTA'
      end
      item
        DataType = ftCurrency
        Name = 'TASSE'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftInteger
        Name = 'FATTA_DA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_RAMO'
      end
      item
        DataType = ftFixedChar
        Name = 'STATO'
      end
      item
        DataType = ftCurrency
        Name = 'PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_PROVVIGIONI'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftFixedChar
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'AGENZIA'
      end
      item
        DataType = ftString
        Name = 'SUB_AGE'
      end
      item
        DataType = ftBoolean
        Name = 'RATINO_1_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'SCONTO_DURATA'
      end
      item
        DataType = ftInteger
        Name = 'UTENTE'
      end
      item
        DataType = ftDate
        Name = 'DATA_INSERIMENTO'
      end
      item
        DataType = ftInteger
        Name = 'NUM_ASSICURATI'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO1'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI1'
      end
      item
        DataType = ftCurrency
        Name = 'INT_FRAZ'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE1'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE1'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE1'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO2'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI2'
      end
      item
        DataType = ftUnknown
        Name = 'RIMBORSO_SOST'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE2'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE2'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE2'
      end
      item
        DataType = ftString
        Name = 'COMBINAZIONE'
      end
      item
        DataType = ftString
        Name = 'FORMA'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONE_OM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_POL_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_PRIMA_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'INDICIZZATA'
      end
      item
        DataType = ftString
        Name = 'STATUS'
      end
      item
        DataType = ftDate
        Name = 'DATA_PERFEZ'
      end
      item
        DataType = ftDate
        Name = 'DATA_CANC'
      end
      item
        DataType = ftCurrency
        Name = 'VOLUME_AFFARI'
      end
      item
        DataType = ftUnknown
        Name = 'NUMERO_ASSICURATI'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_ACCESSORI'
      end
      item
        DataType = ftString
        Name = 'RAGIONE_SOC'
      end
      item
        DataType = ftString
        Name = 'DATI1'
      end
      item
        DataType = ftString
        Name = 'DATI2'
      end
      item
        DataType = ftString
        Name = 'DATI3'
      end
      item
        DataType = ftString
        Name = 'DATI4'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONI'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROV'
      end
      item
        DataType = ftString
        Name = 'TIPOLOGIA'
      end
      item
        DataType = ftDate
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftMemo
        Name = 'DATI_VARI'
      end
      item
        DataType = ftString
        Name = 'ORDINE'
      end
      item
        DataType = ftString
        Name = 'SPECIAL'
      end
      item
        DataType = ftInteger
        Name = 'CODICE_IMPORTAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'INTERMEDIATA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'INTERMEDIATA_DA_SIGLA'
      end
      item
        DataType = ftInteger
        Name = 'CONVENZIONE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_REFERENTE'
      end
      item
        DataType = ftMemo
        Name = 'VARFRP'
      end
      item
        DataType = ftString
        Name = 'CON_APPENDICE'
      end
      item
        DataType = ftInteger
        Name = 'APP_NUMERO'
      end
      item
        DataType = ftDate
        Name = 'APP_DAL'
      end
      item
        DataType = ftDate
        Name = 'APP_AL'
      end
      item
        DataType = ftString
        Name = 'APP_OGGETTO'
      end
      item
        DataType = ftInteger
        Name = 'P_SCONTO'
      end
      item
        DataType = ftString
        Name = 'INPDF'
      end
      item
        DataType = ftInteger
        Name = 'SUB_PROMOTER'
      end
      item
        DataType = ftString
        Name = 'DATI5'
      end
      item
        DataType = ftInteger
        Name = 'N_S_P'
      end
      item
        DataType = ftString
        Name = 'TIPO_EMISSIONE'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'DT_PREVENTIVO'
      end
      item
        DataType = ftUnknown
        Name = 'TELEFONO'
      end
      item
        DataType = ftUnknown
        Name = 'EMAIL'
      end
      item
        DataType = ftUnknown
        Name = 'perAllegati'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end
      item
        DataType = ftString
        Name = 'descrizione'
      end
      item
        DataType = ftBoolean
        Name = 'sconto_durata'
      end
      item
        DataType = ftUnknown
        Name = 'sconto_app'
      end>
    Left = 200
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'RIF_TIPO_POL'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'CONTRAENTE'
      end
      item
        DataType = ftDate
        Name = 'DECORRENZA'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO'
      end
      item
        DataType = ftCurrency
        Name = 'PTASSE'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI'
      end
      item
        DataType = ftFixedChar
        Name = 'VALUTA'
      end
      item
        DataType = ftCurrency
        Name = 'TASSE'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftInteger
        Name = 'FATTA_DA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_RAMO'
      end
      item
        DataType = ftFixedChar
        Name = 'STATO'
      end
      item
        DataType = ftCurrency
        Name = 'PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_PROVVIGIONI'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftFixedChar
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'AGENZIA'
      end
      item
        DataType = ftString
        Name = 'SUB_AGE'
      end
      item
        DataType = ftBoolean
        Name = 'RATINO_1_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'SCONTO_DURATA'
      end
      item
        DataType = ftInteger
        Name = 'UTENTE'
      end
      item
        DataType = ftDate
        Name = 'DATA_INSERIMENTO'
      end
      item
        DataType = ftInteger
        Name = 'NUM_ASSICURATI'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO1'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI1'
      end
      item
        DataType = ftCurrency
        Name = 'INT_FRAZ'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE1'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE1'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE1'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO2'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI2'
      end
      item
        DataType = ftUnknown
        Name = 'RIMBORSO_SOST'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE2'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE2'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE2'
      end
      item
        DataType = ftString
        Name = 'COMBINAZIONE'
      end
      item
        DataType = ftString
        Name = 'FORMA'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONE_OM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_POL_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_PRIMA_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'INDICIZZATA'
      end
      item
        DataType = ftString
        Name = 'STATUS'
      end
      item
        DataType = ftDate
        Name = 'DATA_PERFEZ'
      end
      item
        DataType = ftDate
        Name = 'DATA_CANC'
      end
      item
        DataType = ftCurrency
        Name = 'VOLUME_AFFARI'
      end
      item
        DataType = ftUnknown
        Name = 'NUMERO_ASSICURATI'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_ACCESSORI'
      end
      item
        DataType = ftString
        Name = 'RAGIONE_SOC'
      end
      item
        DataType = ftString
        Name = 'DATI1'
      end
      item
        DataType = ftString
        Name = 'DATI2'
      end
      item
        DataType = ftString
        Name = 'DATI3'
      end
      item
        DataType = ftString
        Name = 'DATI4'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONI'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROV'
      end
      item
        DataType = ftString
        Name = 'TIPOLOGIA'
      end
      item
        DataType = ftDate
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftMemo
        Name = 'DATI_VARI'
      end
      item
        DataType = ftString
        Name = 'ORDINE'
      end
      item
        DataType = ftString
        Name = 'SPECIAL'
      end
      item
        DataType = ftInteger
        Name = 'CODICE_IMPORTAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'INTERMEDIATA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'INTERMEDIATA_DA_SIGLA'
      end
      item
        DataType = ftInteger
        Name = 'CONVENZIONE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_REFERENTE'
      end
      item
        DataType = ftMemo
        Name = 'VARFRP'
      end
      item
        DataType = ftString
        Name = 'CON_APPENDICE'
      end
      item
        DataType = ftInteger
        Name = 'APP_NUMERO'
      end
      item
        DataType = ftDate
        Name = 'APP_DAL'
      end
      item
        DataType = ftDate
        Name = 'APP_AL'
      end
      item
        DataType = ftString
        Name = 'APP_OGGETTO'
      end
      item
        DataType = ftInteger
        Name = 'P_SCONTO'
      end
      item
        DataType = ftString
        Name = 'INPDF'
      end
      item
        DataType = ftInteger
        Name = 'SUB_PROMOTER'
      end
      item
        DataType = ftString
        Name = 'DATI5'
      end
      item
        DataType = ftInteger
        Name = 'N_S_P'
      end
      item
        DataType = ftString
        Name = 'TIPO_EMISSIONE'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'DT_PREVENTIVO'
      end
      item
        DataType = ftUnknown
        Name = 'TELEFONO'
      end
      item
        DataType = ftUnknown
        Name = 'EMAIL'
      end
      item
        DataType = ftUnknown
        Name = 'perAllegati'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end
      item
        DataType = ftString
        Name = 'descrizione'
      end
      item
        DataType = ftBoolean
        Name = 'sconto_durata'
      end
      item
        DataType = ftUnknown
        Name = 'sconto_app'
      end>
  end
  object QInsAssicurato: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'INSERT INTO SLP_TASSICURATI'
      '('
      ' -- COD_ASSICURATI,'
      '  DENOMINAZIONE,'
      '  INDIRIZZO,'
      '  CITTA,'
      '  CAP,'
      '  PROV,'
      '  COD_FISC_IVA,'
      '  NOTE,'
      '  ENTRATA,'
      '  USCITA,'
      '  CAUSALE_USCITA,'
      '  SOSTITUITO_DA,'
      '  PATENTE,'
      '  CATEGORIA_PAT,'
      '  DATA_RILASCIO,'
      '  RILASCIATA_DA,'
      '  DATA_SCADENZA,'
      '  TIPO_VEICOLO,'
      '  TARGA,'
      '  MARCA,'
      '  HP_QL,'
      '  RIF_COD_TIPO_ASSICURATO,'
      '  TIPO,'
      '  COD_POLIZZA,'
      '  DATA_SCAD_REVISIONE,'
      '  MODELLO,'
      '  CLASSE,'
      '  TELAIO,'
      '  CC,'
      '  DATA_IMMATR,'
      '  PREMIO,'
      '  MASSIMALE,'
      '  PREMIO_SLP,'
      '  OGG_AGGIUNTIVO,'
      '  ID_GEN_SLP_AGE'
      ''
      ')'
      'VALUES'
      '('
      ' -- :COD_ASSICURATI,'
      '  :DENOMINAZIONE,'
      '  :INDIRIZZO,'
      '  :CITTA,'
      '  :CAP,'
      '  :PROV,'
      '  :COD_FISC_IVA,'
      '  :NOTE,'
      '  :ENTRATA,'
      '  :USCITA,'
      '  :CAUSALE_USCITA,'
      '  :SOSTITUITO_DA,'
      '  :PATENTE,'
      '  :CATEGORIA_PAT,'
      '  :DATA_RILASCIO,'
      '  :RILASCIATA_DA,'
      '  :DATA_SCADENZA,'
      '  :TIPO_VEICOLO,'
      '  :TARGA,'
      '  :MARCA,'
      '  :HP_QL,'
      '  :RIF_COD_TIPO_ASSICURATO,'
      '  :TIPO,'
      ' LASTAUTOINC('#39'SLP_TPOLIZZE'#39'),'
      '  :DATA_SCAD_REVISIONE,'
      '  :MODELLO,'
      '  :CLASSE,'
      '  :TELAIO,'
      '  :CC,'
      '  :DATA_IMMATR,'
      '  :PREMIO,'
      '  :MASSIMALE,'
      '  :PREMIO_SLP,'
      '  :OGG_AGGIUNTIVO,'
      '  :ID_GEN_SLP_AGE'
      ')')
    Params = <
      item
        DataType = ftString
        Name = 'DENOMINAZIONE'
      end
      item
        DataType = ftString
        Name = 'INDIRIZZO'
      end
      item
        DataType = ftString
        Name = 'CITTA'
      end
      item
        DataType = ftFixedChar
        Name = 'CAP'
      end
      item
        DataType = ftFixedChar
        Name = 'PROV'
      end
      item
        DataType = ftString
        Name = 'COD_FISC_IVA'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftDate
        Name = 'ENTRATA'
      end
      item
        DataType = ftDate
        Name = 'USCITA'
      end
      item
        DataType = ftInteger
        Name = 'CAUSALE_USCITA'
      end
      item
        DataType = ftInteger
        Name = 'SOSTITUITO_DA'
      end
      item
        DataType = ftString
        Name = 'PATENTE'
      end
      item
        DataType = ftString
        Name = 'CATEGORIA_PAT'
      end
      item
        DataType = ftDate
        Name = 'DATA_RILASCIO'
      end
      item
        DataType = ftString
        Name = 'RILASCIATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCADENZA'
      end
      item
        DataType = ftString
        Name = 'TIPO_VEICOLO'
      end
      item
        DataType = ftString
        Name = 'TARGA'
      end
      item
        DataType = ftString
        Name = 'MARCA'
      end
      item
        DataType = ftString
        Name = 'HP_QL'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_TIPO_ASSICURATO'
      end
      item
        DataType = ftFixedChar
        Name = 'TIPO'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCAD_REVISIONE'
      end
      item
        DataType = ftString
        Name = 'MODELLO'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftString
        Name = 'TELAIO'
      end
      item
        DataType = ftString
        Name = 'CC'
      end
      item
        DataType = ftDate
        Name = 'DATA_IMMATR'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO'
      end
      item
        DataType = ftCurrency
        Name = 'MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO_SLP'
      end
      item
        DataType = ftString
        Name = 'OGG_AGGIUNTIVO'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 264
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'DENOMINAZIONE'
      end
      item
        DataType = ftString
        Name = 'INDIRIZZO'
      end
      item
        DataType = ftString
        Name = 'CITTA'
      end
      item
        DataType = ftFixedChar
        Name = 'CAP'
      end
      item
        DataType = ftFixedChar
        Name = 'PROV'
      end
      item
        DataType = ftString
        Name = 'COD_FISC_IVA'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftDate
        Name = 'ENTRATA'
      end
      item
        DataType = ftDate
        Name = 'USCITA'
      end
      item
        DataType = ftInteger
        Name = 'CAUSALE_USCITA'
      end
      item
        DataType = ftInteger
        Name = 'SOSTITUITO_DA'
      end
      item
        DataType = ftString
        Name = 'PATENTE'
      end
      item
        DataType = ftString
        Name = 'CATEGORIA_PAT'
      end
      item
        DataType = ftDate
        Name = 'DATA_RILASCIO'
      end
      item
        DataType = ftString
        Name = 'RILASCIATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCADENZA'
      end
      item
        DataType = ftString
        Name = 'TIPO_VEICOLO'
      end
      item
        DataType = ftString
        Name = 'TARGA'
      end
      item
        DataType = ftString
        Name = 'MARCA'
      end
      item
        DataType = ftString
        Name = 'HP_QL'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_TIPO_ASSICURATO'
      end
      item
        DataType = ftFixedChar
        Name = 'TIPO'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCAD_REVISIONE'
      end
      item
        DataType = ftString
        Name = 'MODELLO'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftString
        Name = 'TELAIO'
      end
      item
        DataType = ftString
        Name = 'CC'
      end
      item
        DataType = ftDate
        Name = 'DATA_IMMATR'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO'
      end
      item
        DataType = ftCurrency
        Name = 'MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO_SLP'
      end
      item
        DataType = ftString
        Name = 'OGG_AGGIUNTIVO'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QInsGaranzia: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'INSERT INTO SLP_TGARANZIE'
      '('
      ' -- COD_GARANZIA,'
      '  MASSIMALE,'
      '  PREMIO,'
      '  DATA_IN,'
      '  DATA_OUT,'
      '  RIF_COD_POLIZZA,'
      '  VALORE_ASSICURATO,'
      '  VALORE_IF,'
      '  RIF_GAR_BASE,'
      '  FRANCHIGIA,'
      '  RIF_COD_MASSIMALE,'
      '  PERCENTUALE,'
      '  ID_GEN_SLP_AGE'
      ')'
      'VALUES'
      '('
      '--  :COD_GARANZIA,'
      '  :MASSIMALE,'
      '  :PREMIO,'
      '  :DATA_IN,'
      '  :DATA_OUT,'
      '   LASTAUTOINC('#39'SLP_TPOLIZZE'#39'),'
      '  :VALORE_ASSICURATO,'
      '  :VALORE_IF,'
      '  :RIF_GAR_BASE,'
      '  :FRANCHIGIA,'
      '  :RIF_COD_MASSIMALE,'
      '  :PERCENTUALE,'
      '  :ID_GEN_SLP_AGE'
      ')')
    Params = <
      item
        DataType = ftCurrency
        Name = 'MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO'
      end
      item
        DataType = ftDate
        Name = 'DATA_IN'
      end
      item
        DataType = ftDate
        Name = 'DATA_OUT'
      end
      item
        DataType = ftCurrency
        Name = 'VALORE_ASSICURATO'
      end
      item
        DataType = ftCurrency
        Name = 'VALORE_IF'
      end
      item
        DataType = ftInteger
        Name = 'RIF_GAR_BASE'
      end
      item
        DataType = ftFixedChar
        Name = 'FRANCHIGIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PERCENTUALE'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 352
    Top = 16
    ParamData = <
      item
        DataType = ftCurrency
        Name = 'MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO'
      end
      item
        DataType = ftDate
        Name = 'DATA_IN'
      end
      item
        DataType = ftDate
        Name = 'DATA_OUT'
      end
      item
        DataType = ftCurrency
        Name = 'VALORE_ASSICURATO'
      end
      item
        DataType = ftCurrency
        Name = 'VALORE_IF'
      end
      item
        DataType = ftInteger
        Name = 'RIF_GAR_BASE'
      end
      item
        DataType = ftFixedChar
        Name = 'FRANCHIGIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PERCENTUALE'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object Qgar_base: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from gar_base'
      'where cod_tipo_contratto= :cod_tipo_contratto'
      'and sigla= :sigla')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_contratto'
      end
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
    Left = 231
    Top = 169
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_contratto'
      end
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
  end
  object Qgar_baseT: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from gar_base'
      'where sigla_slp = :sigla_slp'
      'and sigla = :sigla')
    Params = <
      item
        DataType = ftUnknown
        Name = 'sigla_slp'
      end
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
    Left = 231
    Top = 105
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'sigla_slp'
      end
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
  end
  object QUpdMemoPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'UPDATE SLP_TPOLIZZE'
      'SET MEM_POLIZZA = :MEM_POLIZZA,'
      '      MEM_ASSICURATI = :MEM_ASSICURATI,'
      '      MEM_GARANZIE    = :MEM_GARANZIE'
      'WHERE COD_POLIZZA = :cod_polizza;')
    Params = <
      item
        DataType = ftBlob
        Name = 'MEM_POLIZZA'
      end
      item
        DataType = ftBlob
        Name = 'MEM_ASSICURATI'
      end
      item
        DataType = ftBlob
        Name = 'MEM_GARANZIE'
      end
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end>
    Left = 368
    Top = 112
    ParamData = <
      item
        DataType = ftBlob
        Name = 'MEM_POLIZZA'
      end
      item
        DataType = ftBlob
        Name = 'MEM_ASSICURATI'
      end
      item
        DataType = ftBlob
        Name = 'MEM_GARANZIE'
      end
      item
        DataType = ftInteger
        Name = 'cod_polizza'
      end>
  end
  object QUpdPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'UPDATE SLP_TPOLIZZE'
      'SET '
      ' MEM_POLIZZA = :MEM_POLIZZA,'
      '      MEM_ASSICURATI = :MEM_ASSICURATI,'
      '      MEM_GARANZIE    = :MEM_GARANZIE'
      'WHERE COD_POLIZZA = :COD_POLIZZA'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'MEM_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'MEM_ASSICURATI'
      end
      item
        DataType = ftUnknown
        Name = 'MEM_GARANZIE'
      end
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
    Left = 200
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'MEM_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'MEM_ASSICURATI'
      end
      item
        DataType = ftUnknown
        Name = 'MEM_GARANZIE'
      end
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
  end
  object QDelAssicurati: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'DELETE FROM SLP_TASSICURATI'
      'WHERE COD_POLIZZA = :COD_POLIZZA')
    Params = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
    Left = 272
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
  end
  object QDelGaranzie: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'DELETE FROM SLP_TGARANZIE '
      'WHERE RIF_COD_POLIZZA = :COD_POLIZZA')
    Params = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
    Left = 360
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
  end
  object QDelPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'DELETE FROM SLP_TPOLIZZE'
      'WHERE COD_POLIZZA = :COD_POLIZZA')
    Params = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
    Left = 128
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'COD_POLIZZA'
      end>
  end
  object QGetLastCodPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select distinct lastautoinc('#39'slp_tpolizze'#39') as LAST_CODPOLIZZA f' +
        'rom slp_tpolizze')
    Params = <>
    Left = 360
    Top = 176
  end
  object Qupd_varfrp_pol: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update  SLP_TPOLIZZE'
      'set varfrp = :varfrp'
      'where n_polizza=:n_polizza')
    Params = <
      item
        DataType = ftMemo
        Name = 'VARFRP'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end>
    Left = 232
    Top = 240
    ParamData = <
      item
        DataType = ftMemo
        Name = 'VARFRP'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end>
  end
  object QFindPolizzaByCod: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select * from SLP_TPOLIZZE'
      'where cod_polizza = :CodPolizza')
    Params = <
      item
        DataType = ftInteger
        Name = 'CodPolizza'
      end>
    Left = 48
    Top = 145
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CodPolizza'
      end>
  end
  object QAssicuratoPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'SELECT *  FROM SLP_TASSICURATI'
      'WHERE COD_POLIZZA = :COD_POLIZZA')
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_POLIZZA'
      end>
    Left = 48
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COD_POLIZZA'
      end>
  end
  object QGaranziaPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'SELECT GA.*, GB.Nome_campo_temp_tab as NFIELDTEMPTAB, GB.sigla,'
      'GB.forma  '
      'FROM SLP_TGARANZIE GA'
      'JOIN GAR_BASE GB ON GB.COD_GAR_BASE = GA.RIF_GAR_BASE'
      'WHERE RIF_COD_POLIZZA = :CODPOLIZZA')
    Params = <
      item
        DataType = ftUnknown
        Name = 'CODPOLIZZA'
      end>
    Left = 48
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CODPOLIZZA'
      end>
  end
end
