/// <summary>
/// Data module relativo alle polizze Orsa Maggiore
/// </summary>
unit UDMDatiTempPolizzaDPTR;

interface

uses
  System.SysUtils, System.Classes, UDMDatiTempPolizza, FireDAC.Stan.Intf, FireDAC.Stan.Option, System.DateUtils,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, frxClass, frxGZip, frxExportBaseDialog, frxExportPDF,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TDMDatiTempPolizzaDPTR = class(TDMDatiTempPolizza)
    fdmtblPolizzaRimborsoSpesePatPunti: TBooleanField;
    fdmtblPolizzaAppControversie: TBooleanField;
    fdmtblPolizzaProblematicheRCA: TBooleanField;
    fdmtblPolizzaPerizieParte: TBooleanField;
    fdmtblPolizzaMancatoIntRCA: TBooleanField;
    fdmtblPolizzaProtezioneFamiglia: TBooleanField;
    fdmtblPolizzaEstPatente: TBooleanField;
    fdmtblPolizzaNumeroAssicurati: TIntegerField;
    fdmtblPolizzaIndicizzata: TBooleanField;
    fdmtblPolizzaTipoAssicurato: TStringField;
    fdmtblPolizzaNome1: TStringField;
    fdmtblPolizzaNome2: TStringField;
    fdmtblPolizzaNome3: TStringField;
    fdmtblPolizzaNome4: TStringField;
    fdmtblPolizzaPatente1: TStringField;
    fdmtblPolizzaPatente2: TStringField;
    fdmtblPolizzaPatente3: TStringField;
    fdmtblPolizzaPatente4: TStringField;
    fdmtblPolizzaCatPatente1: TStringField;
    fdmtblPolizzaCatPatente2: TStringField;
    fdmtblPolizzaCatPatente3: TStringField;
    fdmtblPolizzaCatPatente4: TStringField;
    fdmtblPolizzaSiglaRimborsoSpesePatPunti: TStringField;
    procedure fdmtblPolizzaNewRecord(DataSet: TDataSet);
    procedure fdmtblPolizzaTipoAssicuratoChange(Sender: TField);
    procedure fdmtblPolizzaProtezioneFamigliaChange(Sender: TField);
    procedure fdmtblPolizzaNumeroAssicuratiChange(Sender: TField);
    procedure fdmtblPolizzaEstPatenteChange(Sender: TField);
    procedure fdmtblPolizzaNome1Change(Sender: TField);
    procedure fdmtblPolizzaPatente1Change(Sender: TField);
    procedure fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange(Sender: TField);
    procedure fdmtblPolizzaFormaChange(Sender: TField);
    procedure fdmtblPolizzaAppControversieChange(Sender: TField);
    procedure fdmtblPolizzaProblematicheRCAChange(Sender: TField);
    procedure fdmtblPolizzaPerizieParteChange(Sender: TField);
    procedure fdmtblPolizzaMancatoIntRCAChange(Sender: TField);
    procedure fdmtblPolizzaIndicizzataChange(Sender: TField);
    procedure fdmtblPolizzaRimborsoSpesePatPuntiChange(Sender: TField);
    procedure fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
    procedure fdmtblPolizzaNPercAccessoriChange(Sender: TField);
    procedure fdmtblAssicuratoCategoriaPatenteChange(Sender: TField);
  private
    { Private declarations }
    inc_max, inc_diaria, inc_punti, inc_perizie, inc_rca, inc_famiglia, inc_taxi, inc_rimorchio, inc_dp, inc_cc,
      inc_max_dp: currency;
    imp_risc_acc, imp_rca, imp_est_c, imp_base, imp_pp, imp_fam: currency;

    procedure controlla_veicolo_forma;
  protected
    procedure MemorizzaTempAssicurati(CodPolizza: Integer); override;
    procedure MemorizzaTempAssicurato(CodPolizza: Integer; Ix: Integer); override;

    procedure MemorizzaTempGaranzie(TipoPolizza: Integer); override;
    procedure AzzeraCampiReport; override;
    procedure preparaReport; override;
    function per_allegati: string; override;

  public
    { Public declarations }
    procedure cal_premio; override;

    procedure ClearFamiliari(startFrom: Integer = 1);
    procedure CopiaDatiContraente(ACodCliente: Integer); override;
    procedure DoControlli(BaseOnly: Boolean = False); override;
    function ReadTipoAssicurato: string; override;
  end;

implementation

uses
  MainModule, UCodiciErroriPolizza, System.Math, UdmdatiAge, libreria,
  System.StrUtils, libSLP, ServerModule, System.Types, UPolizzaExceptions;

const
  SIGLA_PROBLEMATICHE_RCA     = 'ED1';
  SIGLA_PERIZIE_PARTE         = 'EE1';
  SIGLA_APPCONTROVERSIE       = 'ECC';
  SIGLA_PROTEZIONE_FAMIGLIA   = 'EF';
  SIGLA_RECUPERO_DANNI        = 'ERD';
  SIGLA_LEGALE_DOMICILIATARIO = 'ESD';
  SIGLA_RECUPERO_DANNI_FAM    = 'ASF';
  SIGLA_DIFPEN_VITAPRIVATA    = 'VP';
  SIGLA_PATENTE_CAP           = 'EP1';
  SIGLA_PATENTE_CAPCQC        = 'EP2';
  SIGLA_PATENTE_CAPCQCADR     = 'EP3';
  SIGLA_Est_Patente_taxi      = 'EXT';

  { %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}
  { TDMDatiTempPolizzaOM }

procedure TDMDatiTempPolizzaDPTR.AzzeraCampiReport;
begin
  inherited;
  frxPolizzaGen.Variables['barcode1'] := QuotedStr('00');
  frxPolizzaGen.Variables['barcode2'] := QuotedStr('00');
end;

procedure TDMDatiTempPolizzaDPTR.cal_premio;
begin
  // calcola il premio annuale netto in base alla tariffa relativa alla polizza corrente
  // non tenere conto di frazionamento, ratei o sconti vari: SOLO PREMIO NETTO ANNUO TECNICO
  imp_risc_acc  := 0;
  imp_rca       := 0;
  imp_est_c     := 0;
  imp_base      := 0;
  imp_pp        := 0;
  imp_fam       := 0;
  inc_max       := 0;
  inc_diaria    := 0;
  inc_punti     := 0;
  inc_perizie   := 0;
  inc_famiglia  := 0;
  inc_rimorchio := 0;
  inc_rca       := 0;
  inc_taxi      := 0;
  inc_dp        := 0;
  inc_cc        := 0;
  inc_max_dp    := 0;

  // preleva il premio dall'archivio garanzie della polizza corrente
  controlla_veicolo_forma;

  nettoAnnuoTotNONScontato := 0;
  if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, siglaMassimale) then
  begin
    nettoAnnuoTotNONScontato := UniMainModule.DMDatiBasePolizza.Qgar_base.fieldbyname('PREMIO').asFloat;
    imp_base                 := nettoAnnuoTotNONScontato;
  end;
  if fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean and (fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString <> '') then
  begin
    AggiornaNettoPerEstensione(fdmtblPolizzaRimborsoSpesePatPunti,
      Copy(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, 1, 2), imp_risc_acc);
    if fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean = False then
      fdmtblPolizzaSiglaRimborsoSpesePatPunti.Clear;
  end;

  // cerca le eventuali appendici

  // estensione per la problematica RCA
  AggiornaNettoPerEstensione(fdmtblPolizzaProblematicheRCA, 'ED1', imp_rca);

  // estensione per le perizie di parte ed arbitrati
  AggiornaNettoPerEstensione(fdmtblPolizzaPerizieParte, 'EE1', inc_perizie);

  // estensione per il mancato intervento della compagnia RCAuto in sede penale
  AggiornaNettoPerEstensione(fdmtblPolizzaMancatoIntRCA, 'EDP', inc_dp);

  // estensione per le controversie contrattuali
  AggiornaNettoPerEstensione(fdmtblPolizzaAppControversie, 'ECC', inc_cc);

  // estensione per la famiglia
  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
  begin
    // devi calcolare il 70% del premio raggiunto fino ad ora e moltiplicarlo per il numero degli assicurati - 1
    if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, 'EF') then
    begin
      imp_fam                  := nettoAnnuoTotNONScontato / 100 * 70;
      nettoAnnuoTotNONScontato := nettoAnnuoTotNONScontato + (imp_fam * fdmtblPolizzaNumeroAssicurati.AsInteger);
      inc_famiglia             := imp_fam * ifThen(fdmtblPolizzaNumeroAssicurati.AsInteger > 0,
        fdmtblPolizzaNumeroAssicurati.AsInteger, 1);
    end
    else
      fdmtblPolizzaProtezioneFamiglia.AsBoolean := False;
  end;

  // estensione per rimorchi etc ...
  // devi aggiungere 8,58 euro fissi
  AggiornaNettoPerEstensione(fdmtblPolizzaAppControversie, 'EXT', inc_rimorchio);

  veicolo := ifThen(fdmtblPolizzaTipoAssicurato.AsString = 'V', 1, 0);

  fdmtblPolizza.fieldbyname('PerAllegati').AsString := per_allegati;
end;

procedure TDMDatiTempPolizzaDPTR.controlla_veicolo_forma;

  function CheckTipoVeicoloFormaB: Boolean;
  begin
    Result := True;
    if ((fdmtblAssicuratoTipoVeicolo.AsString = 'AV') or (fdmtblAssicuratoTipoVeicolo.AsString = 'CM') and
      (fdmtblAssicuratoTipoVeicolo.AsString = 'MC')) and (fdmtblPolizzaForma.AsString = 'B') then
      Result := False;
  end;

begin
  // controlla che se la polizza � sul veicolo e il tipo veicolo � AV o MC o CM non permettere di selezionare la forma B
  {
    if (RGforma.ItemIndex=1) and (CBveicolo.Checked) and (pos(copy(CBtipoVeicolo.Text,1,2),'AV.CM.MC')>0) then
    begin
    showmessage('Non � possibile usare la forma B con AutoVeicoli, CicloMotori o MotoCicli !!!');
    RGforma.ItemIndex:=0;
    end;
  }
  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and not CheckTipoVeicoloFormaB then
    raise EPolizzaError.Create(CERR_TIPOVEICOLO_NOT_ALLOWED, MSG_TIPOVEICOLO_NOT_ALLOWED);
end;

procedure TDMDatiTempPolizzaDPTR.CopiaDatiContraente(ACodCliente: Integer);
begin
  inherited;
  if not(fdmtblPolizza.State in [dsInsert, dsEdit]) then
    fdmtblPolizza.Edit;
  fdmtblPolizzaTipoAssicurato.AsString := 'P';

end;

procedure TDMDatiTempPolizzaDPTR.DoControlli(BaseOnly: Boolean);

  function HpQlNeeded: Boolean;
  begin
    //Result := (fdmtblAssicuratoTipoVeicolo.AsString = 'AC') or (fdmtblAssicuratoTipoVeicolo.AsString = 'TR') or
    //  (fdmtblAssicuratoTipoVeicolo.AsString = 'MT');
    Result:=false;
  end;

  procedure ValidaFamiliari;
  var
    i: Integer;
  begin
    if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    begin
      if fdmtblPolizzaNumeroAssicurati.AsInteger = 0 then
        // Se si attiva l''estensione per il nucleo familiare BISOGNA inserire almeno una persona !');
        raise EPolizzaError.Create(CERR_ESTENSIONE_FAM_MISSED, MSG_ESTENSIONE_FAM_MISSED);

      // if (RGforma.ItemIndex = 1) or ((Pos('C', Ecategoria.text) > 0) or (Pos('D', Ecategoria.text) > 0) or
      // (Pos('E', Ecategoria.text) > 0)) then
      // begin
      // ShowMessage('Attenzione ! Con la forma B non si pu� inserire l''estensione Famiglia.');
      // esci := true;
      // end;
      for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
        if (fdmtblPolizza.fieldbyname('NOME' + i.ToString).AsString = '') or
        // (fdmtblPolizza.fieldbyname('PATENTE' + i.ToString).AsString = '') or    // mb 16052020
          (fdmtblPolizza.fieldbyname('CATPATENTE' + i.ToString).AsString = '') then
          raise EPolizzaError.Create(CERR_FAMILIARI_MANCANTI, MSG_FAMILIARI_MANCANTI);

    end;
  end;

begin
  inherited DoControlli(BaseOnly);
  if BaseOnly then
    Exit;

  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and (fdmtblAssicuratoTarga.AsString = '') then
    raise EPolizzaError.Create(CERR_TARGA_VEICOLO_MISSED, MSG_TARGA_VEICOLO_MISSED);

  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and HpQlNeeded and (fdmtblAssicuratoHpQl.AsString = '') then
    raise EPolizzaError.Create(CERR_HPQL_VEICOLO_MISSED, MSG_HPQL_VEICOLO_MISSED);
  // verifica che se la polizza � sul veicolo la forma sia coerente al veicolo inserito
  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and HpQlNeeded and (fdmtblPolizzaForma.AsString = 'A') and
    (fdmtblAssicuratoHpQl.AsInteger > 35) then
  begin
    fdmtblPolizzaForma.AsString := 'B';
    raise EPolizzaError.Create(CERR_FORMA_B_NEEDED, MSG_FORMA_B_NEEDED);
  end;
  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and (fdmtblPolizzaDurataAnni.AsInteger > 5) then
    raise EPolizzaError.Create(CERR_VEICOLO_DURATA_TOO_LONG, MSG_VEICOLO_DURATA_TOO_LONG);

  { TODO 1 -oAngelo -cRefactoring : Rivedere gestione veicolo aggiuntivo per patenti assicurate }

  { if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and fdmtblPolizzaAppControversie.asboolean then
    if (CBpatente.Checked) and (CBcontr_contr.Checked) then
    begin
    // se c'� lappendice per le controversie contrattuali e abbiamo una patente in garanzia
    // non permettere la stampa se non sono stati caricati i dati del veicolo oggetto dell'estensione
    if not(veicoloAggiuntivo.targa > '') then
    begin
    ShowMessage
    ('Con l''estensione alle controversie contrattuali e le garanzie di polizza sulla patente � necessario indicare i dati identificativi del veicolo oggetto dell''estensione.');
    Result := False;
    end;

    end; }
  ValidaFamiliari;

  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and
    ((fdmtblAssicuratoTarga.AsString = '') or (fdmtblAssicuratoTelaio.AsString = '')) then
    raise EPolizzaError.Create(CERR_TARGA_TELAIO_MISSED, MSG_TARGA_TELAIO_MISSED);

end;

procedure TDMDatiTempPolizzaDPTR.fdmtblAssicuratoCategoriaPatenteChange(Sender: TField);
var
  CategoriaPatente: string;
  i: Integer;
begin
  CategoriaPatente := fdmtblAssicuratoCategoriaPatente.AsString;
  if (fdmtblAssicurato.RecNo = 1) and (fdmtblPolizzaNumeroAssicurati.AsInteger > 1) and
    (fdmtblAssicuratoCategoriaPatente.AsString <> '') then
  begin
    for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      fdmtblPolizza.fieldbyname('CATPATENTE' + i.ToString).AsString := CategoriaPatente;
  end;

end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaAppControversieChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaEstPatenteChange(Sender: TField);
begin
  if not fdmtblPolizzaEstPatente.AsBoolean then
  begin
    ClearFamiliari;
    // fdmtblPolizzaNumeroAssicurati.AsInteger := 0;
  end;
  // else
  // fdmtblPolizzaNumeroAssicurati.AsInteger := 1;
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaFormaChange(Sender: TField);
begin
  { TODO 1 -oAngelo -crefactoring : Verificare differenze con slpwin }
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaIndicizzataChange(Sender: TField);
begin
  calcolaPremio;

end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaMancatoIntRCAChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaNewRecord(DataSet: TDataSet);
var
  i: Integer;
begin
  fdmtblPolizzaProtezioneFamiglia.OnChange         := nil;
  fdmtblPolizzaNumeroAssicurati.OnChange           := nil;
  fdmtblPolizzaEstPatente.OnChange                 := nil;
  fdmtblPolizzaNome1.OnChange                      := nil;
  fdmtblPolizzaPatente1.OnChange                   := nil;
  fdmtblPolizzaForma.OnChange                      := nil;
  fdmtblPolizzaAppControversie.OnChange            := nil;
  fdmtblPolizzaProblematicheRCA.OnChange           := nil;
  fdmtblPolizzaPerizieParte.OnChange               := nil;
  fdmtblPolizzaMancatoIntRCA.OnChange              := nil;
  fdmtblPolizzaFrazionamento.OnChange              := nil;
  fdmtblPolizzaProblematicheRCA.OnChange           := nil;
  fdmtblPolizzaIndicizzata.OnChange                := nil;
  fdmtblPolizzaSiglaRimborsoSpesePatPunti.OnChange := nil;
  fdmtblPolizzaRimborsoSpesePatPunti.OnChange      := nil;
  fdmtblPolizzaSiglaMassimale.OnChange             := nil;
  fdmtblPolizzaTipoAssicurato.OnChange             := nil;
  inherited;
  try
    fdmtblPolizzaEstPatente.OnChange             := nil;
    inNewRecord                                  := True;
    fdmtblPolizzaIndicizzata.AsBoolean           := False;
    fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean := False;
    fdmtblPolizzaAppControversie.AsBoolean       := False;
    fdmtblPolizzaProblematicheRCA.AsBoolean      := False;
    fdmtblAssicuratoTipoVeicolo.AsString         := 'TR';
    fdmtblPolizzaPerizieParte.AsBoolean          := False;
    fdmtblPolizzaMancatoIntRCA.AsBoolean         := False;
    fdmtblPolizzaEstPatente.AsBoolean            := False;
    fdmtblPolizzaTipoAssicurato.AsString         := 'V';
    fdmtblPolizzaNumeroAssicurati.AsInteger      := 0;
    fdmtblPolizzaNumeroAssicurati.OnChange       := nil;
    for i                                        := 1 to fdmtblPolizzaNumeroAssicurati.MaxValue do
    begin
      fdmtblPolizza.fieldbyname('NOME' + i.ToString).OnChange := nil;
      fdmtblPolizza.fieldbyname('PATENTE' + i.ToString).OnChange := nil;
    end;
    fdmtblPolizzaNumeroAssicurati.AsInteger := 0;
    ClearFamiliari;
  finally
    fdmtblPolizzaNumeroAssicurati.OnChange := fdmtblPolizzaNumeroAssicuratiChange;
    fdmtblPolizzaEstPatente.OnChange       := fdmtblPolizzaEstPatenteChange;
    for i                                  := 1 to fdmtblPolizzaNumeroAssicurati.MaxValue do
    begin
      fdmtblPolizza.fieldbyname('NOME' + i.ToString).OnChange := fdmtblPolizzaNome1Change;
      fdmtblPolizza.fieldbyname('PATENTE' + i.ToString).OnChange := fdmtblPolizzaPatente1Change;
    end;
    fdmtblPolizzaSiglaRimborsoSpesePatPunti.OnChange := fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange;
    fdmtblPolizzaRimborsoSpesePatPunti.OnChange      := fdmtblPolizzaRimborsoSpesePatPuntiChange;
    fdmtblPolizzaProtezioneFamiglia.OnChange         := fdmtblPolizzaProtezioneFamigliaChange;
    fdmtblPolizzaEstPatente.OnChange                 := fdmtblPolizzaEstPatenteChange;
    fdmtblPolizzaNome1.OnChange                      := fdmtblPolizzaNome1Change;
    fdmtblPolizzaPatente1.OnChange                   := fdmtblPolizzaPatente1Change;
    fdmtblPolizzaForma.OnChange                      := fdmtblPolizzaFormaChange;
    fdmtblPolizzaAppControversie.OnChange            := fdmtblPolizzaAppControversieChange;
    fdmtblPolizzaProblematicheRCA.OnChange           := fdmtblPolizzaProblematicheRCAChange;
    fdmtblPolizzaPerizieParte.OnChange               := fdmtblPolizzaPerizieParteChange;
    fdmtblPolizzaMancatoIntRCA.OnChange              := fdmtblPolizzaMancatoIntRCAChange;
    fdmtblPolizzaFrazionamento.OnChange              := fdmtblPolizzaFrazionamentoChange;
    fdmtblPolizzaIndicizzata.OnChange                := fdmtblPolizzaIndicizzataChange;
    fdmtblPolizzaSiglaMassimale.OnChange             := fdmtblPolizzaSiglaMassimaleChange;
    fdmtblPolizzaTipoAssicurato.OnChange             := fdmtblPolizzaTipoAssicuratoChange;

    inNewRecord := False;

  end;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaNome1Change(Sender: TField);

  procedure ControllaNomiFamiliari;
  var
    i: Integer;
    j: Integer;
  begin
    if TStringField(Sender).AsString <> '' then
      for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      begin
        if (fdmtblAssicuratoDenominazione.AsString <> '') and
          (fdmtblAssicuratoDenominazione.AsString = fdmtblPolizza.fieldbyname('Nome' + i.ToString).AsString) then
          // familiare corrispondente al contraente
          raise EPolizzaError.Create(CERR_FAMILIARE_EQ_CONTRAENTE, Format(MSG_FAMILIARE_EQ_CONTRAENTE, [i]));

        for j := i + 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
        begin
          if (fdmtblPolizza.fieldbyname('Nome' + i.ToString) <> Sender) and
            (fdmtblPolizza.fieldbyname('Nome' + i.ToString).AsString = fdmtblPolizza.fieldbyname('Nome' + j.ToString)
            .AsString) and (fdmtblPolizza.fieldbyname('Nome' + i.ToString).AsString <> '') then
            raise EPolizzaError.Create(CERR_NOME_FAMILIARE_DUP, Format(MSG_NOME_FAMILIARE_DUP, [i, j]));

        end;
      end;
  end;

begin
  ControllaNomiFamiliari;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaNPercAccessoriChange(Sender: TField);
begin
  inherited;
  //
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaNumeroAssicuratiChange(Sender: TField);

  procedure setCategoriaPatenteFamiliari;
  var
    i: Integer;
  begin
    for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      fdmtblPolizza.fieldbyname('CatPatente' + i.ToString).AsString := fdmtblAssicuratoCategoriaPatente.AsString;
  end;

begin
  if (fdmtblPolizzaNumeroAssicurati.AsInteger > 0) and (fdmtblPolizzaNumeroAssicurati.AsInteger <= MAX_NUM_ASSICURATI)
  then
  begin
    ClearFamiliari(fdmtblPolizzaNumeroAssicurati.AsInteger + 1);
    setCategoriaPatenteFamiliari;
    calcolaPremio;
  end
  else
    if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    begin
      if fdmtblPolizzaNumeroAssicurati.AsInteger = 0 then
        raise EPolizzaError.Create(CERR_NUM_ASSICURATI_ZERO, MSG_NUM_ASSICURATI_ZERO)
      else
        raise EPolizzaError.Create(CERR_NUM_ASSICURATI, Format(MSG_NUM_ASSICURATI, [MAX_NUM_ASSICURATI]));
    end;
end;

procedure TDMDatiTempPolizzaDPTR.ClearFamiliari(startFrom: Integer = 1);
var
  i: Integer;
begin
  for i := startFrom to MAX_NUM_ASSICURATI do
  begin
    fdmtblPolizza.fieldbyname('Nome' + i.ToString).Clear;
    fdmtblPolizza.fieldbyname('Patente' + i.ToString).Clear;
    fdmtblPolizza.fieldbyname('CatPatente' + i.ToString).Clear;
  end;

end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaPatente1Change(Sender: TField);

  procedure ControllaPatenteFamiliari;
  var
    i: Integer;
    j: Integer;
  begin
    if TStringField(Sender).AsString <> '' then
      for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      begin
        if (fdmtblAssicuratoPatente.AsString <> '') and
          (fdmtblAssicuratoPatente.AsString = fdmtblPolizza.fieldbyname('Patente' + i.ToString).AsString) then
          // familiare corrispondente al contraente
          raise EPolizzaError.Create(CERR_PAT_FAMILIARE_EQ_CONTRAENTE, Format(MSG_PAT_FAMILIARE_EQ_CONTRAENTE, [i]));

        for j := i + 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
          if (fdmtblPolizza.fieldbyname('Patente' + i.ToString) <> Sender) and
            (fdmtblPolizza.fieldbyname('Patente' + i.ToString).AsString = fdmtblPolizza.fieldbyname('Patente' +
            j.ToString).AsString) and (fdmtblPolizza.fieldbyname('Patente' + i.ToString).AsString <> '') then
            raise EPolizzaError.Create(CERR_PATENTE_FAMILIARE_DUP, Format(MSG_PATENTE_FAMILIARE_DUP, [i, j]));

      end;
  end;

begin
  ControllaPatenteFamiliari;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaPerizieParteChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaProblematicheRCAChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaProtezioneFamigliaChange(Sender: TField);
begin
  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    fdmtblPolizzaNumeroAssicurati.AsInteger := 1
  else
  begin
    ClearFamiliari;
    fdmtblPolizzaNumeroAssicurati.AsInteger := 0;
  end;
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaRimborsoSpesePatPuntiChange(Sender: TField);
begin
  calcolaPremio
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
begin
  fdmtblPolizzaSiglaMassimale.OnChange := nil;
  try
    calcolaPremio;
  finally
    fdmtblPolizzaSiglaMassimale.OnChange := fdmtblPolizzaSiglaMassimaleChange;
  end;
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange(Sender: TField);
begin
  calcolaPremio
end;

procedure TDMDatiTempPolizzaDPTR.fdmtblPolizzaTipoAssicuratoChange(Sender: TField);
  procedure clearDatiVeicolo;
  begin
    fdmtblAssicuratoTipoVeicolo.Clear;
    fdmtblAssicuratoMarca.Clear;
    fdmtblAssicuratoModello.Clear;
    fdmtblAssicuratoHpQl.Clear;
    fdmtblAssicuratoTarga.Clear;
    fdmtblAssicuratoTelaio.Clear;
    fdmtblAssicuratoDataScadenza.Clear;
    fdmtblAssicuratoDataScadRevisione.Clear;
  end;

  procedure ClearDatiPatente;
  begin
    fdmtblAssicuratoCategoriaPatente.Clear;
    fdmtblAssicuratoCodFiscIvas.Clear;
    fdmtblAssicuratoDenominazione.Clear;
    fdmtblAssicuratoPatente.Clear;
    fdmtblAssicuratoRilasciataDa.Clear;
    fdmtblAssicuratoDataRilascio.Clear;
    fdmtblAssicuratoDataScadenza.Clear;
    fdmtblAssicuratoDataImmatricolazione.Clear;
    fdmtblAssicuratoDataScadRevisione.Clear;
  end;

begin
  // if fdmtblPolizzaTipoAssicurato.AsString = 'P' then
  fdmtblAssicuratoTipo.AsString := fdmtblPolizzaTipoAssicurato.AsString;
  if False then
  begin
    clearDatiVeicolo;
    UniMainModule.DMDatiTempPolizza.SetDatiAssicuratoDaContraente(True);
  end
  else
  begin
    ClearDatiPatente;
    fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean := False;
    fdmtblPolizzaSiglaRimborsoSpesePatPunti.Clear;
  end;
  calcolaPremio;
end;

function TDMDatiTempPolizzaDPTR.per_allegati: string;
begin
  // genera la stringa da passare a stamp_moduli per sapere quali crocette mettere
  // nel modulo dell'adeguatezza
  inherited;
  Result := Result + '.C1'; // crocetta per circolazione stradale
  Result := Copy(Result, 2, 40); // togli il punto iniziale ...
end;

procedure TDMDatiTempPolizzaDPTR.preparaReport;
var
  stringaTest: string;
  ext, tipoVeicolo, per_famiglia: string;
  PatPuntMin, PatPuntMax: string;
  SLPDatiAge: TSLPdati_age_rec;
  StringaChecked: string;

  procedure istanziaCampiAssicurati;
  var
    i: Integer;
  begin
    {
    frxPolizzaGen.Variables['assicurato'] := stringaTest;
    frxPolizzaGen.Variables['patenteN']   := stringaTest;
    frxPolizzaGen.Variables['cat']        := stringaTest;
    frxPolizzaGen.Variables['ril_il']     := stringaTest;
    frxPolizzaGen.Variables['pref']       := stringaTest;
    frxPolizzaGen.Variables['tipo']       := stringaTest;
    frxPolizzaGen.Variables['marca']      := stringaTest;
    frxPolizzaGen.Variables['targa']      := stringaTest;
    frxPolizzaGen.Variables['hp_ql']      := stringaTest;

    frxPolizzaGen.Variables['Xpat']  := ifThen(fdmtblPolizzaTipoAssicurato.AsString = 'P', 'X', stringaTest);
    frxPolizzaGen.Variables['Xveic'] := ifThen(fdmtblPolizzaTipoAssicurato.AsString = 'V', 'X', stringaTest);

    fdmtblAssicurato.First;
    frxPolizzaGen.Variables['in_garanzia'] := QuotedStr(alltrim(fdmtblPolizzaContraente.AsString) + '  Pat. n�:' +
      padr(fdmtblAssicuratoPatente.AsString, ' ', 20) + '  Cat.:' + padr(fdmtblAssicuratoCategoriaPatente.AsString, ' ',
      7) + '  Ril. il:' + (fdmtblAssicuratoDataRilascio.AsString) + ' ' + fdmtblAssicuratoRilasciataDa.AsString);

    frxPolizzaGen.Variables['in_garanzia1'] := QuotedStr(per_famiglia);
    // frxPolizzaGen.Variables['nettoAnnuoTot'] := QuotedStr(zero(Opremio.nettoTotaleRicalcolato + Opremio.scontoDurataBase));
    frxPolizzaGen.Variables['nettoAnnuoTot'] :=
      QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency + fdmtblPolizzaSconto.AsCurrency));

    frxPolizzaGen.Variables['n_fam'] := QuotedStr('N� familiari ' + fdmtblPolizzaNumeroAssicurati.AsString);

    per_famiglia := '';
    fdmtblAssicurato.Next;
    i := 0;

    while not fdmtblAssicurato.Eof do
    begin
      Inc(i);

      per_famiglia := per_famiglia + '(' + i.ToString + ') ' + fdmtblAssicuratoDenominazione.AsString + '  Pat. n�:' +
        padr(fdmtblAssicuratoPatente.AsString, ' ', 20) + '  Cat.:' +
        padr(fdmtblAssicuratoCategoriaPatente.AsString, ' ', 7);
      fdmtblAssicurato.Next;
    end;
    frxPolizzaGen.Variables['in_garanzia1'] := QuotedStr(per_famiglia);
    fdmtblAssicurato.First;
    }
  end;

  procedure istanziaCampiVeicolo;
  begin
    // frxPolizzaGen.Variables['Xpat'] := ifThen(fdmtblPolizzaTipoAssicurato.AsString = 'P', QuotedStr('X'), stringaTest);
    frxPolizzaGen.Variables['Xveic'] := QuotedStr('X'); //ifThen(fdmtblPolizzaTipoAssicurato.AsString = 'V', QuotedStr('X'), stringaTest);
    frxPolizzaGen.Variables['nettoAnnuoTot'] :=
      QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency + fdmtblPolizzaSconto.AsCurrency));

    fdmtblAssicurato.First;
    while not fdmtblAssicurato.Eof do
    begin
      tipoVeicolo := alltrim(Copy(fdmtblAssicuratoTipoVeicolo.AsString, 6, 30));

      if (Copy(fdmtblAssicuratoTipoVeicolo.AsString, 1, 2) = 'AC') then
        tipoVeicolo := tipoVeicolo + ifThen(fdmtblPolizzaEstPatente.AsBoolean, ' con RIMORCHIO', ' SENZA RIMORCHIO');

      frxPolizzaGen.Variables['tipo']  := QuotedStr(tipoVeicolo);
      frxPolizzaGen.Variables['marca'] := QuotedStr(alltrim(fdmtblAssicuratoMarca.AsString) + ' ' +
        fdmtblAssicuratoModello.AsString);
      frxPolizzaGen.Variables['targa'] := QuotedStr(alltrim(fdmtblAssicuratoTarga.AsString) + ' ' +
        alltrim(fdmtblAssicuratoTelaio.AsString));
      frxPolizzaGen.Variables['hp_ql'] := QuotedStr(fdmtblAssicuratoHpQl.AsString);

      frxPolizzaGen.Variables['in_garanzia'] := QuotedStr(tipoVeicolo + ' ' + alltrim(fdmtblAssicuratoMarca.AsString) +
        ' ' + alltrim(fdmtblAssicuratoModello.AsString) + '  Targa:' + alltrim(fdmtblAssicuratoTarga.AsString) + ' ' +
        alltrim(fdmtblAssicuratoTelaio.AsString) + '  HP/QL:' + fdmtblAssicuratoHpQl.AsString);
      fdmtblAssicurato.Next;
    end;
    frxPolizzaGen.Variables['nettoAnnuoTot'] :=
      QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency + fdmtblPolizzaSconto.AsCurrency));
    fdmtblAssicurato.First;

  end;

  procedure GetPatPuntMinMax(ASiglaMassimalePatPunt: string; var PatPuntMin, PatPuntMax: string);
  var
    MinMaxDynArray: TStringDynArray;

    function PatPuntFmt(StrToFmt: string): string;
    var
      valore: currency;
    begin
      valore := Trim(StrToFmt).ToDouble;
      Result := FormatFloat('###0.00', valore);
    end;

  begin
    MinMaxDynArray := SplitString(ASiglaMassimalePatPunt, '=+');
    if Length(MinMaxDynArray) = 3 then
    begin
      PatPuntMin := PatPuntFmt(MinMaxDynArray[1]);
      PatPuntMax := PatPuntFmt(MinMaxDynArray[2]);
    end;
  end;

begin
  // imposta i campi del frxPolizzaGen
  stringaTest    := QuotedStr(' ');
  StringaChecked := QuotedStr('X');
  AzzeraCampiReport;

  SLPDatiAge                          := UniMainModule.DMdatiAge.SLPdati_age;
  frxPolizzaGen.EngineOptions.TempDir := SLPDatiAge.dati_gen.dir_temp;

  frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
  if isPolizza then
  begin
    frxPolizzaGen.Variables['polizza']   := QuotedStr(UniMainModule.NumPolizza);
    frxPolizzaGen.Variables['barcode1']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 1));
    frxPolizzaGen.Variables['barcode2']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 2));
    frxPolizzaGen.Variables['facsimile'] := QuotedStr('N');
  end
  else
    if isPreventivo then
    begin
      frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
      frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
    end;

  frxPolizzaGen.Variables['versione']     := QuotedStr(leggiVersione);
  frxPolizzaGen.Variables['peragenzia']   := QuotedStr('N');
  frxPolizzaGen.Variables['in_garanzia']  := QuotedStr('');
  frxPolizzaGen.Variables['in_garanzia1'] := QuotedStr('');

  frxPolizzaGen.Variables['ramo']        := QuotedStr(UniServerModule.ramoSLP);
  frxPolizzaGen.Variables['agenzia']     := QuotedStr(SLPDatiAge.agenzia);
  frxPolizzaGen.Variables['citta_age']   := QuotedStr(SLPDatiAge.nome);
  frxPolizzaGen.Variables['descrizione'] := QuotedStr(SLPDatiAge.Denominazione);
  frxPolizzaGen.Variables['autostorica'] := stringaTest;
  frxPolizzaGen.Variables['camper']      := QuotedStr('N');

  frxPolizzaGen.Variables['suba']  := QuotedStr(fdmtblPolizzaSubAgenzia.AsString);
  if frxPolizzaGen.Variables.IndexOf('S_SUBA')>=0 then
     frxPolizzaGen.Variables['s_suba']     := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);
  if frxPolizzaGen.Variables.IndexOf('subP')>=0 then
     frxPolizzaGen.Variables['subP'] := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);

  frxPolizzaGen.Variables['Psost'] := QuotedStr(fdmtblPolizzaNumPolizzaSostituita.AsString);
  // QuotedStr(Epol_sost.text);
  frxPolizzaGen.Variables['scade_il'] := QuotedStr(fdmtblPolizzaScadenzaPolizzaSostituita.AsString);
  // QuotedStr(Escad_pol_sost.text);
  frxPolizzaGen.Variables['effetto']  := QuotedStr(fdmtblPolizzaDecorrenza.AsString); // QuotedStr(Eeffetto.text);
  frxPolizzaGen.Variables['scadenza'] := QuotedStr(fdmtblPolizzaScadenza.AsString); // QuotedStr(Escadenza.text);
  frxPolizzaGen.Variables['durata']   := QuotedStr('-' + fdmtblPolizzaDurataAnni.AsString + '-');

  frxPolizzaGen.Variables['giorni'] := QuotedStr(ifThen(fdmtblPolizzaDurataGiorni.AsString = '0', stringaTest,
    fdmtblPolizzaDurataGiorni.AsString));

  frxPolizzaGen.Variables['fraz'] := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  // QuotedStr(dai_frazionamentoE(CBfrazionamento.text));
  frxPolizzaGen.Variables['scad1rata'] := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  // QuotedStr(Escad_prima_rata.text);
  frxPolizzaGen.Variables['emissione'] := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  // QuotedStr(EdataEmissione.text);
  frxPolizzaGen.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString); // QuotedStr(Econtraente.text);
  frxPolizzaGen.Variables['nato_a']     := QuotedStr(fdmtblPolizzaLocNascita.AsString); // QuotedStr(Enato_a.text);
  frxPolizzaGen.Variables['nato_il']    := QuotedStr(ifThen(YearOf(fdmtblPolizzaDataNascita.AsDateTime) > 1900,
    fdmtblPolizzaDataNascita.AsString, ''));
  frxPolizzaGen.Variables['residente_in'] :=
    QuotedStr(fdmtblPolizzaIndirizzo.AsString + ' - ' + fdmtblPolizzaCitta.AsString + ' - ' +
    fdmtblPolizzaProvincia.AsString);
  // QuotedStr(Eindirizzo.text + ' - ' + Ecitta.text + ' (' + Eprovincia.text + ')');
  frxPolizzaGen.Variables['cap']         := QuotedStr(fdmtblPolizzaCap.AsString); // QuotedStr(Ecap.text);
  frxPolizzaGen.Variables['professione'] := QuotedStr(fdmtblPolizzaProfessioneDescriz.AsString);
  // QuotedStr(Eprofessione.text);
  // frxPolizzaGen.Variables['telefono'] := QuotedStr('Numero telefonico');
  { TODO -oMB : inserire il campo per telefono e email nella form di inserimento }
  // QuotedStr(alltrim(alltrim(Ee_mail.text) + ' ' + alltrim(Etelefono.text)));
  frxPolizzaGen.Variables['cf_iva'] := QuotedStr(ifThen(fdmtblPolizzaPartitaIva.AsString > '',
    fdmtblPolizzaPartitaIva.AsString, fdmtblPolizzaCodiceFiscale.AsString ));
  frxPolizzaGen.Variables['FORMA'] := QuotedStr(fdmtblPolizzaForma.AsString);

  frxPolizzaGen.Variables['X1'] := ifThen(fdmtblPolizzaForma.AsString = 'A', StringaChecked, stringaTest);
  frxPolizzaGen.Variables['X2'] := ifThen(fdmtblPolizzaForma.AsString = 'B', StringaChecked, stringaTest);

  frxPolizzaGen.Variables['ADEGNO'] := ifThen(fdmtblPolizzaIndicizzata.AsBoolean, QuotedStr('SI'), QuotedStr('NO'));;
  frxPolizzaGen.Variables['ADEGSI'] := ifThen(fdmtblPolizzaIndicizzata.AsBoolean, QuotedStr('SI'), QuotedStr('NO'));

  // if CBincMaxDP.Checked then
  // frxPolizzaGen.Variables['extMaxDP'] := QuotedStr('S');

  frxPolizzaGen.Variables['XE'] := stringaTest;
  frxPolizzaGen.Variables['XF'] := stringaTest;

  frxPolizzaGen.Variables['in_garanzia1'] := stringaTest;

  frxPolizzaGen.Variables['max6']       := QuotedStr('======');
  frxPolizzaGen.Variables['nettoAnnuo'] := QuotedStr('====');
  frxPolizzaGen.Variables['RA1']        := QuotedStr('======');
  frxPolizzaGen.Variables['RA2']        := QuotedStr('======');
  frxPolizzaGen.Variables['RA3']        := QuotedStr('======');
  frxPolizzaGen.Variables['ESTC']       := QuotedStr('======');
  frxPolizzaGen.Variables['ESTD']       := QuotedStr('======');
  frxPolizzaGen.Variables['ESTE']       := QuotedStr('======');
  frxPolizzaGen.Variables['ESTF']       := QuotedStr('======');

  ext := '';

  frxPolizzaGen.Variables['X3'] := stringaTest;
  frxPolizzaGen.Variables['X5'] := stringaTest;

  frxPolizzaGen.Variables['X4'] := stringaTest;
  frxPolizzaGen.Variables['X6'] := stringaTest;

  if fdmtblPolizzaProblematicheRCA.AsBoolean then
  begin
    frxPolizzaGen.Variables['X4']   := StringaChecked;
    frxPolizzaGen.Variables['ESTC'] := QuotedStr(zero(getImportoGaranzia(SIGLA_PROBLEMATICHE_RCA)));
    ext                             := ext + 'C';
  end;

  frxPolizzaGen.Variables['XE'] := stringaTest;
  if fdmtblPolizzaPerizieParte.AsBoolean then
  begin
    frxPolizzaGen.Variables['XE']   := StringaChecked;
    frxPolizzaGen.Variables['ESTD'] := QuotedStr(zero(getImportoGaranzia(SIGLA_PERIZIE_PARTE)));
    ext                             := ext + ' - D';
  end;

  frxPolizzaGen.Variables['XF'] := stringaTest;
  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
  begin
    frxPolizzaGen.Variables['XF']   := StringaChecked;
    frxPolizzaGen.Variables['ESTE'] := QuotedStr(zero(getImportoGaranzia(SIGLA_PROTEZIONE_FAMIGLIA)));
    ext                             := ext + ' - E';
  end;

  frxPolizzaGen.Variables['XG'] := stringaTest;
  if fdmtblPolizzaEstPatente.AsBoolean then
  begin
    frxPolizzaGen.Variables['XG']   := StringaChecked;
    frxPolizzaGen.Variables['ESTF'] := QuotedStr(zero(getImportoGaranzia(SIGLA_Est_Patente_taxi)));
    ext                             := ext + ' - F';
  end;

  frxPolizzaGen.Variables['EXT'] := ifThen(ext > '', QuotedStr(ext), QuotedStr('==========='));

  tipoVeicolo := '';

  frxPolizzaGen.Variables['varie'] := stringaTest;
  frxPolizzaGen.Variables['n_fam'] := stringaTest;

  frxPolizzaGen.Variables['perc_sconto'] := QuotedStr('-' + alltrim(zero(fdmtblPolizzaPSconto.AsCurrency)) + '%');

  frxPolizzaGen.Variables['XPP'] := stringaTest;
  if (fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString <> '') and (fdmtblPolizzaTipoAssicurato.AsString = 'P') then
  begin
    frxPolizzaGen.Variables['XPP'] := StringaChecked;
    frxPolizzaGen.Variables['RA3'] :=
      QuotedStr(zero(getImportoGaranzia(Copy(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, 1, 2))));
    GetPatPuntMinMax(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, PatPuntMin, PatPuntMax);
    frxPolizzaGen.Variables['RA1'] := QuotedStr(PatPuntMin);
    frxPolizzaGen.Variables['RA2'] := QuotedStr(PatPuntMax);
  end;
  getImportoGaranzia(Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 2));
  frxPolizzaGen.Variables['max6'] :=
    QuotedStr('=' + FormatCurr('##,###,##0', fdmtblGaranziaMassimale.AsCurrency) + '=');

  frxPolizzaGen.Variables['APPE']  := stringaTest;
  frxPolizzaGen.Variables['APPE1'] := stringaTest;

  if fdmtblPolizzaAppControversie.AsBoolean then
  begin
    // frxPolizzaGen.Variables['APPE']  := QuotedStr('VEDI APP. UNO (E. ' + zero(inc_cc) + ')');   { TODO -oMB : verificare caso di appendice contr contr se orsa maggiore }
    // frxPolizzaGen.Variables['APPE1'] := QuotedStr('+ APP. UNO     E. ' + zero(inc_cc));
  end;


  //if fdmtblPolizzaTipoAssicurato.AsString = 'P' then
  //  istanziaCampiAssicurati
  //else
    istanziaCampiVeicolo;

  // frxPolizzaGen.Variables['nettoAnnuo'] := QuotedStr(zero(getImportoGaranzia(fdmtblPolizzaSiglaMassimale.AsString)));
  frxPolizzaGen.Variables['nettoAnnuo'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));

  frxPolizzaGen.Variables['nettoAnnuoTot'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));

  // if Opremio.scontoDurataBase <> 0 then
  if fdmtblPolizzaSconto.AsCurrency<>0 then
  begin
    frxPolizzaGen.Variables['sconto'] := QuotedStr(zero(fdmtblPolizzaSconto.AsCurrency));  //QuotedStr(zero(Opremio.scontoDurataBase));
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
  end
  else
  begin
    frxPolizzaGen.Variables['sconto']     := QuotedStr('=======');
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr('=======');
  end;

  frxPolizzaGen.Variables['rate_succ']   := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['netto1']      := QuotedStr(zero(fdmtblPolizzaNetto1.AsCurrency));
  frxPolizzaGen.Variables['acc1']        := QuotedStr(zero(fdmtblPolizzaAccessori1.AsCurrency));
  frxPolizzaGen.Variables['int_fraz']    := QuotedStr(zero(fdmtblPolizzaInteressiFrazionamento1.AsCurrency));
  frxPolizzaGen.Variables['imponibile1'] := QuotedStr(zero(fdmtblPolizzaimponibile1.AsCurrency));
  frxPolizzaGen.Variables['tasse1']      := QuotedStr(zero(fdmtblPolizzaimposte1.AsCurrency));
  frxPolizzaGen.Variables['totale1']     := QuotedStr(zero(fdmtblPolizzatotale1.AsCurrency));
  frxPolizzaGen.Variables['alla_firma']  := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['netto2']      := QuotedStr(zero(fdmtblPolizzanetto2.AsCurrency));
  frxPolizzaGen.Variables['acc2']        := QuotedStr(zero(fdmtblPolizzaaccessori2.AsCurrency));
  frxPolizzaGen.Variables['rimborso']    := QuotedStr(zero(fdmtblPolizzarimborsoSost.AsCurrency));
  frxPolizzaGen.Variables['imponibile2'] := QuotedStr(zero(fdmtblPolizzaimponibile2.AsCurrency));
  frxPolizzaGen.Variables['tasse2']      := QuotedStr(zero(fdmtblPolizzaimposte2.AsCurrency));
  frxPolizzaGen.Variables['totale2']     := QuotedStr(zero(fdmtblPolizzatotale2.AsCurrency));
  frxPolizzaGen.Variables['emissione']   := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  if UniMainModule.DMdatiAge.TacitoRinnovoEnabled and fdmtblPolizzaTacitoRinnovo.AsBoolean then
    frxPolizzaGen.Variables['STR'] := QuotedStr('S');
end;

procedure TDMDatiTempPolizzaDPTR.MemorizzaTempAssicurati(CodPolizza: Integer);
var
  i: Integer;
begin
  inherited;
  for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
    MemorizzaTempAssicurato(CodPolizza, i);
end;

procedure TDMDatiTempPolizzaDPTR.MemorizzaTempAssicurato(CodPolizza, Ix: Integer);
begin

  fdmtblAssicurato.Append;
  fdmtblAssicurato.Edit;

  fdmtblAssicuratoDenominazione.AsString := fdmtblPolizza.fieldbyname('NOME' + Ix.ToString).AsString;
  fdmtblAssicuratoIndirizzo.Clear;
  fdmtblAssicuratoCitta.Clear;
  fdmtblAssicuratocap.Clear;
  fdmtblAssicuratoProvincia.Clear;
  fdmtblAssicuratoCodFiscIvas.Clear;
  fdmtblAssicuratoNote.Clear;
  fdmtblAssicuratoEntrata.Clear;
  fdmtblAssicuratoUscita.Clear;
  fdmtblAssicuratoCausaleUscita.Clear;
  fdmtblAssicuratoSostituitoDa.Clear;
  fdmtblAssicuratoPatente.Clear; //AsString          := fdmtblPolizza.fieldbyname('PATENTE' + Ix.ToString).AsString;
  fdmtblAssicuratoCategoriaPatente.Clear;  //AsString := fdmtblPolizza.fieldbyname('CATPATENTE' + Ix.ToString).AsString;
  fdmtblAssicuratoDataRilascio.Clear;
  fdmtblAssicuratoRilasciataDa.Clear;
  fdmtblAssicuratoDataScadenza.Clear;
  fdmtblAssicuratoTipoVeicolo.Clear;
  fdmtblAssicuratoTarga.Clear;
  fdmtblAssicuratoMarca.Clear;
  fdmtblAssicuratoHpQl.Clear;
  fdmtblAssicuratoRifCodTipoAssicurato.Clear;
  fdmtblAssicuratoTipo.AsString        := 'V';
  fdmtblAssicuratoCodPolizza.AsInteger := CodPolizza;
  fdmtblAssicuratoDataScadRevisione.Clear;
  fdmtblAssicuratoModello.Clear;
  fdmtblAssicuratoClasse.Clear;
  fdmtblAssicuratoTelaio.Clear;
  fdmtblAssicuratoCc.Clear;
  fdmtblAssicuratoDataImmatricolazione.Clear;
  fdmtblAssicuratoPremio.Clear;
  fdmtblAssicuratoMassimale.Clear;
  fdmtblAssicuratoPremioSLP.Clear;
  fdmtblAssicuratoOggAggiuntivo.Clear;
  fdmtblAssicuratoIdGenSlpAge.Clear;

  fdmtblAssicurato.post;

end;

procedure TDMDatiTempPolizzaDPTR.MemorizzaTempGaranzie(TipoPolizza: Integer);
begin
  inherited;

  // estensione per la problematica RCA
  if fdmtblPolizzaProblematicheRCA.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, 'ED1');

  // estensione per le perizie di parte ed arbitrati
  if fdmtblPolizzaPerizieParte.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, 'EE1');

  // estensione per le controversie contrattuali
  if fdmtblPolizzaMancatoIntRCA.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, 'EDP');

  // estensione per le controversie contrattuali
  if fdmtblPolizzaAppControversie.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, 'ECC');

  // estensione per la famiglia
  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, 'EF', inc_famiglia);
  // Rimborso spese patente a punti
  if fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean then
  begin
    MemorizzaTempGaranzia(CodPolizza, Copy(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, 1, 2), imp_risc_acc);
  end;

  // estensione rimorchi
  if fdmtblPolizzaEstPatente.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, 'EXT');

end;

function TDMDatiTempPolizzaDPTR.ReadTipoAssicurato: string;
begin
  Result := 'V'; //fdmtblPolizzaTipoAssicurato.AsString;
end;

initialization

RegisterClass(TDMDatiTempPolizzaDPTR);

end.
