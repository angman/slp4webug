inherited DMDatiPolizzaCondTP: TDMDatiPolizzaCondTP
  inherited QInsPolizza: TDBISAMQuery
    SQL.Strings = (
      'INSERT INTO SLP_TPOLIZZE'
      '('
      '  --COD_POLIZZA,'
      '  RIF_TIPO_POL,'
      ' SIGLA_POL,'
      '  CONTRAENTE,'
      '  DECORRENZA,'
      '  SCADENZA,'
      '  FRAZIONAM,'
      '  N_POLIZZA,'
      '  LORDO,'
      '  NETTO,'
      '  PTASSE,'
      '  ACCESSORI,'
      '  VALUTA,'
      '  TASSE,'
      '  NOTE,'
      '  RIF_COMPAGNIA,'
      '  COD_CLI,'
      '  FATTA_DA,'
      '  RIF_COD_RAMO,'
      '  STATO,'
      '  PROVVIGIONI,'
      '  PERC_PROVVIGIONI,'
      '  CLASSE,'
      '  RAMO,'
      '  AGENZIA,'
      '  SUB_AGE,'
      '  RATINO_1_RATA,'
      '  SCONTO_DURATA,'
      '  UTENTE,'
      '  DATA_INSERIMENTO,'
      '   NETTO1,'
      '  ACCESSORI1,'
      '  INT_FRAZ,'
      '  IMPONIBILE1,'
      '  IMPOSTE1,'
      '  TOTALE1,'
      '  NETTO2,'
      '  ACCESSORI2,'
      '  RIMBORSO_SOST,'
      '  IMPONIBILE2,'
      '  IMPOSTE2,'
      '  TOTALE2,'
      '  COMBINAZIONE,'
      '  FORMA,'
      '  ESTENSIONE_OM,'
      '  N_POLIZZA_SOSTITUITA,'
      '  SCAD_POL_SOSTITUITA,'
      '  SCAD_PRIMA_RATA,'
      '  INDICIZZATA,'
      '  STATUS,'
      '  DATA_PERFEZ,'
      '  DATA_CANC,'
      '  VOLUME_AFFARI,'
      '  NUMERO_ASSICURATI,'
      '  TIPO_PROVVIGIONI,'
      '  PERC_ACCESSORI,'
      '  RAGIONE_SOC,'
      '  DATI1,'
      '  DATI2,'
      '  DATI3,'
      '  DATI4,'
      '  ESTENSIONI,'
      '  SIGLA_POL,'
      '  TIPO_PROV,'
      '  TIPOLOGIA,'
      '  DATA_EMISSIONE,'
      '  DATI_VARI,'
      '  ORDINE,'
      '  SPECIAL,'
      '  DATA_IMPORTAZIONE,'
      '  CODICE_IMPORTAZIONE,'
      '  INTERMEDIATA_DA,'
      '  INTERMEDIATA_DA_SIGLA,'
      '  CONVENZIONE,'
      '  RIF_REFERENTE,'
      '  VARFRP,'
      '  APP_REPORT,'
      '  CON_APPENDICE,'
      '  APP_NUMERO,'
      '  APP_DAL,'
      '  APP_AL,'
      '  APP_OGGETTO,'
      '  P_SCONTO,'
      '  INPDF,'
      '  SUB_PROMOTER,'
      '  DATI5,'
      '  N_S_P,'
      '  TIPO_EMISSIONE,'
      '  DT_LAST_MOD,'
      '  DT_PREVENTIVO,'
      '  TELEFONO,'
      '  EMAIL,'
      '  perAllegati,'
      '  ID_GEN_SLP_AGE,'
      ' descrizione'
      ')'
      'VALUES ('
      '  :RIF_TIPO_POL,'
      '  :SIGLA_POL,'
      '  :CONTRAENTE,'
      '  :DECORRENZA,'
      '  :SCADENZA,'
      '  :FRAZIONAM,'
      '  :N_POLIZZA,'
      '  :LORDO,'
      '  :NETTO,'
      '  :PTASSE,'
      '  :ACCESSORI,'
      '  :VALUTA,'
      '  :TASSE,'
      '  :NOTE,'
      '  :RIF_COMPAGNIA,'
      '  :COD_CLI,'
      '  :FATTA_DA,'
      '  :RIF_COD_RAMO,'
      '  :STATO,'
      '  :PROVVIGIONI,'
      '  :PERC_PROVVIGIONI,'
      '  :CLASSE,'
      '  :RAMO,'
      '  :AGENZIA,'
      '  :SUB_AGE,'
      '  :RATINO_1_RATA,'
      '  :SCONTO_DURATA,'
      '  :UTENTE,'
      '  :DATA_INSERIMENTO,'
      '  :NETTO1,'
      '  :ACCESSORI1,'
      '  :INT_FRAZ,'
      '  :IMPONIBILE1,'
      '  :IMPOSTE1,'
      '  :TOTALE1,'
      '  :NETTO2,'
      '  :ACCESSORI2,'
      '  :RIMBORSO_SOST,'
      '  :IMPONIBILE2,'
      '  :IMPOSTE2,'
      '  :TOTALE2,'
      '  :COMBINAZIONE,'
      '  :FORMA,'
      '  :ESTENSIONE_OM,'
      '  :N_POLIZZA_SOSTITUITA,'
      '  :SCAD_POL_SOSTITUITA,'
      '  :SCAD_PRIMA_RATA,'
      '  :INDICIZZATA,'
      '  :STATUS,'
      '  :DATA_PERFEZ,'
      '  :DATA_CANC,'
      '  :VOLUME_AFFARI,'
      '  :NUMERO_ASSICURATI,'
      '  :TIPO_PROVVIGIONI,'
      '  :PERC_ACCESSORI,'
      '  :RAGIONE_SOC,'
      '  :DATI1,'
      '  :DATI2,'
      '  :DATI3,'
      '  :DATI4,'
      '  :ESTENSIONI,'
      '  :SIGLA_POL,'
      '  :TIPO_PROV,'
      '  :TIPOLOGIA,'
      '  :DATA_EMISSIONE,'
      '  :DATI_VARI,'
      '  :ORDINE,'
      '  :SPECIAL,'
      '  :DATA_IMPORTAZIONE,'
      '  :CODICE_IMPORTAZIONE,'
      '  :INTERMEDIATA_DA,'
      '  :INTERMEDIATA_DA_SIGLA,'
      '  :CONVENZIONE,'
      '  :RIF_REFERENTE,'
      '  :VARFRP,'
      '  :APP_REPORT,'
      '  :CON_APPENDICE,'
      '  :APP_NUMERO,'
      '  :APP_DAL,'
      '  :APP_AL,'
      '  :APP_OGGETTO,'
      '  :P_SCONTO,'
      '  :INPDF,'
      '  :SUB_PROMOTER,'
      '  :DATI5,'
      '  :N_S_P,'
      '  :TIPO_EMISSIONE,'
      '  :DT_LAST_MOD,'
      '  :DT_PREVENTIVO,'
      '  :TELEFONO,'
      '  :EMAIL,'
      '  :perAllegati,'
      '  :ID_GEN_SLP_AGE,'
      '  :descrizione'
      ')'
      '')
    Params = <
      item
        DataType = ftInteger
        Name = 'RIF_TIPO_POL'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'CONTRAENTE'
      end
      item
        DataType = ftDate
        Name = 'DECORRENZA'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO'
      end
      item
        DataType = ftCurrency
        Name = 'PTASSE'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI'
      end
      item
        DataType = ftFixedChar
        Name = 'VALUTA'
      end
      item
        DataType = ftCurrency
        Name = 'TASSE'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftInteger
        Name = 'FATTA_DA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_RAMO'
      end
      item
        DataType = ftFixedChar
        Name = 'STATO'
      end
      item
        DataType = ftCurrency
        Name = 'PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_PROVVIGIONI'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftFixedChar
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'AGENZIA'
      end
      item
        DataType = ftString
        Name = 'SUB_AGE'
      end
      item
        DataType = ftBoolean
        Name = 'RATINO_1_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'SCONTO_DURATA'
      end
      item
        DataType = ftInteger
        Name = 'UTENTE'
      end
      item
        DataType = ftDate
        Name = 'DATA_INSERIMENTO'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO1'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI1'
      end
      item
        DataType = ftCurrency
        Name = 'INT_FRAZ'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE1'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE1'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE1'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO2'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI2'
      end
      item
        DataType = ftUnknown
        Name = 'RIMBORSO_SOST'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE2'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE2'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE2'
      end
      item
        DataType = ftString
        Name = 'COMBINAZIONE'
      end
      item
        DataType = ftString
        Name = 'FORMA'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONE_OM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_POL_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_PRIMA_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'INDICIZZATA'
      end
      item
        DataType = ftString
        Name = 'STATUS'
      end
      item
        DataType = ftDate
        Name = 'DATA_PERFEZ'
      end
      item
        DataType = ftDate
        Name = 'DATA_CANC'
      end
      item
        DataType = ftCurrency
        Name = 'VOLUME_AFFARI'
      end
      item
        DataType = ftInteger
        Name = 'NUMERO_ASSICURATI'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_ACCESSORI'
      end
      item
        DataType = ftString
        Name = 'RAGIONE_SOC'
      end
      item
        DataType = ftString
        Name = 'DATI1'
      end
      item
        DataType = ftString
        Name = 'DATI2'
      end
      item
        DataType = ftString
        Name = 'DATI3'
      end
      item
        DataType = ftString
        Name = 'DATI4'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONI'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROV'
      end
      item
        DataType = ftString
        Name = 'TIPOLOGIA'
      end
      item
        DataType = ftDate
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftMemo
        Name = 'DATI_VARI'
      end
      item
        DataType = ftString
        Name = 'ORDINE'
      end
      item
        DataType = ftString
        Name = 'SPECIAL'
      end
      item
        DataType = ftTimeStamp
        Name = 'DATA_IMPORTAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'CODICE_IMPORTAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'INTERMEDIATA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'INTERMEDIATA_DA_SIGLA'
      end
      item
        DataType = ftInteger
        Name = 'CONVENZIONE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_REFERENTE'
      end
      item
        DataType = ftMemo
        Name = 'VARFRP'
      end
      item
        DataType = ftBlob
        Name = 'APP_REPORT'
      end
      item
        DataType = ftString
        Name = 'CON_APPENDICE'
      end
      item
        DataType = ftInteger
        Name = 'APP_NUMERO'
      end
      item
        DataType = ftDate
        Name = 'APP_DAL'
      end
      item
        DataType = ftDate
        Name = 'APP_AL'
      end
      item
        DataType = ftString
        Name = 'APP_OGGETTO'
      end
      item
        DataType = ftInteger
        Name = 'P_SCONTO'
      end
      item
        DataType = ftString
        Name = 'INPDF'
      end
      item
        DataType = ftInteger
        Name = 'SUB_PROMOTER'
      end
      item
        DataType = ftString
        Name = 'DATI5'
      end
      item
        DataType = ftInteger
        Name = 'N_S_P'
      end
      item
        DataType = ftString
        Name = 'TIPO_EMISSIONE'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'DT_PREVENTIVO'
      end
      item
        DataType = ftUnknown
        Name = 'TELEFONO'
      end
      item
        DataType = ftUnknown
        Name = 'EMAIL'
      end
      item
        DataType = ftUnknown
        Name = 'perAllegati'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end
      item
        DataType = ftString
        Name = 'descrizione'
      end>
    ParamData = <
      item
        DataType = ftInteger
        Name = 'RIF_TIPO_POL'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'CONTRAENTE'
      end
      item
        DataType = ftDate
        Name = 'DECORRENZA'
      end
      item
        DataType = ftDate
        Name = 'SCADENZA'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO'
      end
      item
        DataType = ftCurrency
        Name = 'PTASSE'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI'
      end
      item
        DataType = ftFixedChar
        Name = 'VALUTA'
      end
      item
        DataType = ftCurrency
        Name = 'TASSE'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftInteger
        Name = 'FATTA_DA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_RAMO'
      end
      item
        DataType = ftFixedChar
        Name = 'STATO'
      end
      item
        DataType = ftCurrency
        Name = 'PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_PROVVIGIONI'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftFixedChar
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'AGENZIA'
      end
      item
        DataType = ftString
        Name = 'SUB_AGE'
      end
      item
        DataType = ftBoolean
        Name = 'RATINO_1_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'SCONTO_DURATA'
      end
      item
        DataType = ftInteger
        Name = 'UTENTE'
      end
      item
        DataType = ftDate
        Name = 'DATA_INSERIMENTO'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO1'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI1'
      end
      item
        DataType = ftCurrency
        Name = 'INT_FRAZ'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE1'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE1'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE1'
      end
      item
        DataType = ftCurrency
        Name = 'NETTO2'
      end
      item
        DataType = ftCurrency
        Name = 'ACCESSORI2'
      end
      item
        DataType = ftUnknown
        Name = 'RIMBORSO_SOST'
      end
      item
        DataType = ftCurrency
        Name = 'IMPONIBILE2'
      end
      item
        DataType = ftCurrency
        Name = 'IMPOSTE2'
      end
      item
        DataType = ftCurrency
        Name = 'TOTALE2'
      end
      item
        DataType = ftString
        Name = 'COMBINAZIONE'
      end
      item
        DataType = ftString
        Name = 'FORMA'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONE_OM'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_POL_SOSTITUITA'
      end
      item
        DataType = ftDate
        Name = 'SCAD_PRIMA_RATA'
      end
      item
        DataType = ftBoolean
        Name = 'INDICIZZATA'
      end
      item
        DataType = ftString
        Name = 'STATUS'
      end
      item
        DataType = ftDate
        Name = 'DATA_PERFEZ'
      end
      item
        DataType = ftDate
        Name = 'DATA_CANC'
      end
      item
        DataType = ftCurrency
        Name = 'VOLUME_AFFARI'
      end
      item
        DataType = ftInteger
        Name = 'NUMERO_ASSICURATI'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROVVIGIONI'
      end
      item
        DataType = ftCurrency
        Name = 'PERC_ACCESSORI'
      end
      item
        DataType = ftString
        Name = 'RAGIONE_SOC'
      end
      item
        DataType = ftString
        Name = 'DATI1'
      end
      item
        DataType = ftString
        Name = 'DATI2'
      end
      item
        DataType = ftString
        Name = 'DATI3'
      end
      item
        DataType = ftString
        Name = 'DATI4'
      end
      item
        DataType = ftString
        Name = 'ESTENSIONI'
      end
      item
        DataType = ftString
        Name = 'SIGLA_POL'
      end
      item
        DataType = ftString
        Name = 'TIPO_PROV'
      end
      item
        DataType = ftString
        Name = 'TIPOLOGIA'
      end
      item
        DataType = ftDate
        Name = 'DATA_EMISSIONE'
      end
      item
        DataType = ftMemo
        Name = 'DATI_VARI'
      end
      item
        DataType = ftString
        Name = 'ORDINE'
      end
      item
        DataType = ftString
        Name = 'SPECIAL'
      end
      item
        DataType = ftTimeStamp
        Name = 'DATA_IMPORTAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'CODICE_IMPORTAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'INTERMEDIATA_DA'
      end
      item
        DataType = ftUnknown
        Name = 'INTERMEDIATA_DA_SIGLA'
      end
      item
        DataType = ftInteger
        Name = 'CONVENZIONE'
      end
      item
        DataType = ftInteger
        Name = 'RIF_REFERENTE'
      end
      item
        DataType = ftMemo
        Name = 'VARFRP'
      end
      item
        DataType = ftBlob
        Name = 'APP_REPORT'
      end
      item
        DataType = ftString
        Name = 'CON_APPENDICE'
      end
      item
        DataType = ftInteger
        Name = 'APP_NUMERO'
      end
      item
        DataType = ftDate
        Name = 'APP_DAL'
      end
      item
        DataType = ftDate
        Name = 'APP_AL'
      end
      item
        DataType = ftString
        Name = 'APP_OGGETTO'
      end
      item
        DataType = ftInteger
        Name = 'P_SCONTO'
      end
      item
        DataType = ftString
        Name = 'INPDF'
      end
      item
        DataType = ftInteger
        Name = 'SUB_PROMOTER'
      end
      item
        DataType = ftString
        Name = 'DATI5'
      end
      item
        DataType = ftInteger
        Name = 'N_S_P'
      end
      item
        DataType = ftString
        Name = 'TIPO_EMISSIONE'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'DT_PREVENTIVO'
      end
      item
        DataType = ftUnknown
        Name = 'TELEFONO'
      end
      item
        DataType = ftUnknown
        Name = 'EMAIL'
      end
      item
        DataType = ftUnknown
        Name = 'perAllegati'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end
      item
        DataType = ftString
        Name = 'descrizione'
      end>
  end
end
