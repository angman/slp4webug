inherited DMdatiAge: TDMdatiAge
  OldCreateOrder = True
  Height = 585
  Width = 918
  inherited QGetLastID: TDBISAMQuery
    SessionName = 'SessionAgenzia_1'
    Left = 130
    Top = 80
  end
  inherited tblTemp: TDBISAMTable
    SessionName = 'SessionAgenzia_1'
    Left = 184
    Top = 56
  end
  object DBagenti4web: TDBISAMDatabase
    EngineVersion = '4.37 Build 3'
    DatabaseName = 'DBagenti4web'
    Directory = 'c:\SLP\Slp4Web\SLPDATI'
    SessionName = 'SessionAgenzia_1'
    AfterConnect = DBagenti4webAfterConnect
    Left = 32
    Top = 16
  end
  object Qutenti: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select ut.*, '
      'pr.Nome as NomeCollaboratore, pr.sigla as SiglaCollaboratore,'
      
        'pr.link_produtt as LinkCollaboratore, pr.cod_produttore as CodCo' +
        'llaboratore'
      
        'from utenti ut left join produtt pr on ut.promoter = pr.cod_prod' +
        'uttore')
    Params = <>
    Left = 40
    Top = 72
  end
  object Qget_codice: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update codice'
      'set nome_campo = nome_campo+1 ;'
      ''
      'select nome_campo'
      'from codici'
      ''
      '')
    Params = <>
    Left = 424
    Top = 344
  end
  object Qget_codicenew: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from codicinew'
      'where ID_CAMPO= :id_campo')
    Params = <
      item
        DataType = ftString
        Name = 'id_campo'
      end>
    Left = 120
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'id_campo'
      end>
  end
  object Qins_codicenew: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into codicinew'
      
        '(id_campo, valore, VAL_BOOLEAN, VAL_DATE, VAL_INTEGER, VAL_CURRE' +
        'NCY, VAL_STRING)'
      'values'
      
        '(:id_campo, :valore, :VAL_BOOLEAN, :VAL_DATE,:VAL_INTEGER,:VAL_C' +
        'URRENCY,:VAL_STRING)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'id_campo'
      end
      item
        DataType = ftUnknown
        Name = 'valore'
      end
      item
        DataType = ftBoolean
        Name = 'VAL_BOOLEAN'
      end
      item
        DataType = ftDateTime
        Name = 'VAL_DATE'
      end
      item
        DataType = ftInteger
        Name = 'VAL_INTEGER'
      end
      item
        DataType = ftCurrency
        Name = 'VAL_CURRENCY'
      end
      item
        DataType = ftString
        Name = 'VAL_STRING'
      end>
    Left = 240
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'id_campo'
      end
      item
        DataType = ftUnknown
        Name = 'valore'
      end
      item
        DataType = ftBoolean
        Name = 'VAL_BOOLEAN'
      end
      item
        DataType = ftDateTime
        Name = 'VAL_DATE'
      end
      item
        DataType = ftInteger
        Name = 'VAL_INTEGER'
      end
      item
        DataType = ftCurrency
        Name = 'VAL_CURRENCY'
      end
      item
        DataType = ftString
        Name = 'VAL_STRING'
      end>
  end
  object Qmod_codicenew: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update codicinew'
      'set valore = :valore,'
      '    VAL_BOOLEAN = :VAL_BOOLEAN,'
      '    VAL_DATE = :VAL_DATE,'
      '    VAL_INTEGER = :VAL_INTEGER,'
      '    VAL_CURRENCY = VAL_CURRENCY,'
      '    VAL_STRING = VAL_STRING '
      'where ID_CAMPO= :id_campo')
    Params = <
      item
        DataType = ftUnknown
        Name = 'valore'
      end
      item
        DataType = ftUnknown
        Name = 'VAL_BOOLEAN'
      end
      item
        DataType = ftUnknown
        Name = 'VAL_DATE'
      end
      item
        DataType = ftUnknown
        Name = 'VAL_INTEGER'
      end
      item
        DataType = ftUnknown
        Name = 'id_campo'
      end>
    Left = 336
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'valore'
      end
      item
        DataType = ftUnknown
        Name = 'VAL_BOOLEAN'
      end
      item
        DataType = ftUnknown
        Name = 'VAL_DATE'
      end
      item
        DataType = ftUnknown
        Name = 'VAL_INTEGER'
      end
      item
        DataType = ftUnknown
        Name = 'id_campo'
      end>
  end
  object SessionAgenzia: TDBISAMSession
    EngineVersion = '4.37 Build 3'
    AutoSessionName = True
    PrivateDir = 'z:\VMShared\Slp4Web_2019\SLPDATI\DATI\'
    OnPassword = SessionAgenziaPassword
    RemoteEncryptionPassword = 'elevatesoft'
    RemoteAddress = '127.0.0.1'
    Left = 129
    Top = 16
  end
  object QClientiProfessioni: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select * '
      'from profess'
      'order by descriz')
    Params = <>
    Left = 368
    Top = 216
  end
  object dsClientiProfessioni: TDataSource
    DataSet = QClientiProfessioni
    Left = 472
    Top = 216
  end
  object QRami: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from rami')
    Params = <>
    Left = 528
    Top = 304
  end
  object DSPolizze: TDataSource
    DataSet = QPolizze
    Left = 256
    Top = 160
  end
  object DSCompagnie: TDataSource
    DataSet = QCompagnie
    Left = 296
    Top = 368
  end
  object QCompagnie: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from compagni'
      'order by nome'
      ''
      '')
    Params = <>
    Left = 224
    Top = 368
  end
  object DSRami: TDataSource
    DataSet = QRami
    Left = 576
    Top = 304
  end
  object QPolizze: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'SELECT  '
      
        'COD_POLIZZA, N_POLIZZA, CONTRAENTE, DECORRENZA, SCADENZA, FRAZIO' +
        'NAM , '
      'RIF_COMPAGNIA, RIF_TIPO_POL, RIF_COD_RAMO, CODICE,'
      'LORDO,  COD_CLI, FATTA_DA, STATO, CAUSA_MORTE, DATA_MORTE,'
      'PTASSE, NETTO, LORDO, PROVVIGIONI,TASSE,'
      
        ' sospeso_dal, sospeso_al ,compagni.codice_agenzia, tipo_pol.desc' +
        'rizione,'
      ' rami.descrizione, rif_referente'
      
        'from polizze left join compagni on compagni.cod_compagnia=rif_co' +
        'mpagnia'
      'left join tipo_pol on cod_tipo_pol=rif_tipo_pol'
      'left join rami on cod_ramo=rif_cod_ramo'
      '  '
      ''
      'where polizze.stato='#39'V'#39
      'and polizze.n_polizza>'#39#39
      'and rif_compagnia=1   -- solo per SLP'
      ' '
      'order by polizze.n_polizza   --clienti.denominaz  ')
    Params = <>
    Left = 200
    Top = 160
  end
  object QTipoPolizze: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_pol'
      'where STAMPA_SLP4WEB = '#39'S'#39
      'order by descrizione')
    Params = <>
    Left = 168
    Top = 216
  end
  object DSTipoPolizze: TDataSource
    DataSet = QTipoPolizze
    Left = 264
    Top = 224
  end
  object DSConvenzioni: TDataSource
    DataSet = QConvenzioni
    Left = 112
    Top = 440
  end
  object QConvenzioni: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from convenzioni'
      '')
    Params = <>
    Left = 32
    Top = 440
  end
  object QCausali: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from CAUSALI_ANN')
    Params = <>
    Left = 224
    Top = 440
  end
  object DSCausali: TDataSource
    DataSet = QCausali
    Left = 288
    Top = 440
  end
  object DSReferenti: TDataSource
    DataSet = QReferenti
    Left = 232
    Top = 80
  end
  object QReferenti: TDBISAMQuery
    Filtered = True
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from referenti')
    Params = <>
    Left = 152
    Top = 112
  end
  object QAppoggio: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from appoggio'
      '')
    Params = <>
    Left = 584
    Top = 152
  end
  object DSAppoggio: TDataSource
    DataSet = QAppoggio
    Left = 664
    Top = 152
  end
  object QTitoliNumPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_titoli'
      'order by descrizione')
    Params = <>
    Left = 416
    Top = 72
  end
  object QLog_Ope: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from log_ope')
    Params = <>
    Left = 336
    Top = 16
  end
  object QSelPromoter: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from produtt'
      'order by nome'
      '')
    Params = <>
    Left = 616
    Top = 392
  end
  object DSSelPromoter: TDataSource
    DataSet = QSelPromoter
    Left = 704
    Top = 392
  end
  object QPromoter_From_Polizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select  pro.sigla'
      '        , pro.nome'
      '        , pro.cod_produttore'
      'from polizze p'
      'left join clienti c'
      'on p.cod_cli = c.cod_cliente'
      'left join produtt pro'
      'on c.promoter = pro.cod_produttore'
      'where p.n_polizza = :n_polizza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end>
    Left = 644
    Top = 223
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end>
  end
  object QProdut: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from Produtt'
      'order by Nome')
    Params = <>
    Left = 32
    Top = 128
  end
  object DSProdut: TDataSource
    DataSet = QProdut
    Left = 88
    Top = 128
  end
  object QnumeroFC: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select numero_fc'
      'from compagni'
      'where cod_compagnia=1')
    Params = <>
    Left = 456
    Top = 24
  end
  object QDati_agenzia: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select * from dati_agenzia')
    Params = <>
    Left = 40
    Top = 375
  end
  object QcompagniSLP: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from compagni'
      'where cod_compagnia=1')
    Params = <>
    Left = 443
    Top = 412
  end
  object QgetCompagnia: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from compagni'
      'where cod_compagnia=:cod_compagnia')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
    Left = 475
    Top = 468
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
  end
  object QUtenteAutorizzazioni: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select Archivio, Operazione, Attivo'
      'from Utente_Autorizzazione'
      'where Rif_Utente = :utente')
    Params = <
      item
        DataType = ftUnknown
        Name = 'utente'
      end>
    Left = 593
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'utente'
      end>
  end
  object QAgenziaEdit: TDBISAMQuery
    BeforePost = QAgenziaEditBeforePost
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select * from DATI_AGENZIA '
      'where ID_GEN_SLP_AGE = :ID_GEN_SLP_AGE')
    Params = <
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 128
    Top = 351
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QTipoPolStampaPol: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from tipo_pol'
      'where stampa_slp4web='#39'S'#39
      'order by descrizione')
    Params = <>
    Left = 360
    Top = 160
  end
  object Qdurata_max: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select max(durata) as dmax'
      'from aliquote'
      'where rif_cod_compagnia=1')
    Params = <>
    Left = 634
    Top = 296
  end
  object QAliquote: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select * from aliquote'
      'where ID_GEN_SLP_AGE = :codAgenzia'
      'and rif_cod_compagnia = :codCompagnia'
      'and tipo = :tipo'
      'and durata <= :durata'
      'top 1')
    Params = <
      item
        DataType = ftUnknown
        Name = 'codAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'codCompagnia'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'durata'
      end>
    Left = 536
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'codAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'codCompagnia'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'durata'
      end>
  end
  object QCodici: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from codici'
      '--where compagnia = :codCompagnia')
    Params = <>
    Left = 536
    Top = 368
  end
  object Qtipo_veicolo: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_veicolo')
    Params = <>
    Left = 714
    Top = 296
  end
  object QTipo_pol: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_pol'
      'where cod_tipo_pol = :cod_tipo_pol')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_pol'
      end>
    Left = 192
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_tipo_pol'
      end>
  end
  object fdmtblFrazionamento: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 712
    Top = 48
    object fdmtblFrazionamentoCodFrazionamento: TStringField
      FieldName = 'CodFrazionamento'
      Size = 1
    end
    object Qpol_da_perfezFrazionamentoDesFrazionamento: TStringField
      FieldName = 'DesFrazionamento'
      Size = 18
    end
  end
  object dsFrazionamento: TDataSource
    DataSet = fdmtblFrazionamento
    Left = 712
    Top = 96
  end
  object fdmtblTipoEmiss: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 776
    Top = 128
    object fdmtblPolizzaTipoEmissCodEmmiss: TStringField
      FieldName = 'CodEmmiss'
      Size = 1
    end
    object fdmtblPolizzaTipoEmissDesTipoEmiss: TStringField
      FieldName = 'DesTipoEmiss'
      Size = 3
    end
    object fdmtblTipoEmissCodCompagnia: TIntegerField
      FieldName = 'CodCompagnia'
    end
  end
  object dsTipoEmiss: TDataSource
    DataSet = fdmtblTipoEmiss
    Left = 776
    Top = 184
  end
  object QtipoPolizzeAll: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_pol'
      'order by descrizione'
      '')
    Params = <>
    Left = 40
    Top = 264
  end
  object DStipoPolizzeAll: TDataSource
    DataSet = QtipoPolizzeAll
    Left = 32
    Top = 312
  end
  object QCapItalia: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select nome_comune from CAP_ITALIA'
      'where ID_COD_FISCALE = :IDCODFISCALE')
    Params = <
      item
        DataType = ftUnknown
        Name = 'IDCODFISCALE'
      end>
    Left = 600
    Top = 464
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'IDCODFISCALE'
      end>
  end
  object QinsStampa: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      '--START TRANSACTION;'
      'insert into printhouse'
      '(tipo_doc, data_stampa, data_stampa1, n_polizza, doc, visibile, '
      ' descrizione, data_doc, ID_GEN_SLP_AGE)'
      ''
      'VALUES'
      ''
      
        '(:tipo_doc, :data_stampa, :data_stampa1, :n_polizza, :doc, :visi' +
        'bile, '
      ' :descrizione, :data_doc, :ID_GEN_SLP_AGE)'
      ''
      
        '--SELECT DISTINCT LASTAUTOINC('#39'PRINTHOUSE'#39') AS CODDOC FROM PRINT' +
        'HOUSE;'
      '--COMMIT FLUSH;')
    Params = <
      item
        DataType = ftUnknown
        Name = 'tipo_doc'
      end
      item
        DataType = ftUnknown
        Name = 'data_stampa'
      end
      item
        DataType = ftUnknown
        Name = 'data_stampa1'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'doc'
      end
      item
        DataType = ftUnknown
        Name = 'visibile'
      end
      item
        DataType = ftUnknown
        Name = 'descrizione'
      end
      item
        DataType = ftUnknown
        Name = 'data_doc'
      end
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 690
    Top = 465
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'tipo_doc'
      end
      item
        DataType = ftUnknown
        Name = 'data_stampa'
      end
      item
        DataType = ftUnknown
        Name = 'data_stampa1'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'doc'
      end
      item
        DataType = ftUnknown
        Name = 'visibile'
      end
      item
        DataType = ftUnknown
        Name = 'descrizione'
      end
      item
        DataType = ftUnknown
        Name = 'data_doc'
      end
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QStaccaNumeroPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select numero '
      ''
      'from ind_polizze'
      'where '
      'tipo_polizza=:sigla'
      'and data_stampa is null'
      'order by numero'
      'top 1')
    Params = <
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
    Left = 40
    Top = 489
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
  end
  object QUpdNumeroPolizza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update ind_polizze'
      'set data_stampa = current_timestamp,'
      '      stampato = '#39'S'#39
      'where numero = :NumeroPolizza'
      '    ')
    Params = <
      item
        DataType = ftUnknown
        Name = 'NumeroPolizza'
      end>
    Left = 176
    Top = 489
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'NumeroPolizza'
      end>
  end
  object QModSLP: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from mod_slp'
      'where tipo=:tipo'
      'and codice_slp=:sigla_pol')
    Params = <
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_pol'
      end>
    Left = 245
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_pol'
      end>
  end
  object QgetLastCodDoc: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'SELECT DISTINCT LASTAUTOINC('#39'PRINTHOUSE'#39') AS CODDOC FROM PRINTHO' +
        'USE;')
    Params = <>
    Left = 762
    Top = 465
  end
  object Qmod_slp: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from mod_slp'
      'where cod_stampa=:cod_stampa')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_stampa'
      end>
    Left = 813
    Top = 381
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_stampa'
      end>
  end
  object Qmoduli: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from mod_slp'
      'where tipo=:tipo'
      'and codice_slp=:codice_slp')
    Params = <
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'codice_slp'
      end>
    Left = 309
    Top = 77
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'codice_slp'
      end>
  end
  object QgetDescrizioneVeicolo: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_veicolo'
      'where sigla = :sigla')
    Params = <
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
    Left = 367
    Top = 489
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'sigla'
      end>
  end
  object QproduttAgente: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from Produtt'
      'where tipo='#39'A'#39)
    Params = <>
    Left = 544
    Top = 424
  end
  object QcercaProdutt: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from Produtt'
      'where cod_produttore=:cod_produttore')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_produttore'
      end>
    Left = 792
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_produttore'
      end>
  end
  object QsubProdut: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from Produtt'
      'where link_produtt = :cod_produttore'
      'order by Nome')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_produttore'
      end>
    Left = 24
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_produttore'
      end>
  end
  object DSsubProdut: TDataSource
    DataSet = QsubProdut
    Left = 96
    Top = 184
  end
  object QintermediataDa: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from Produtt'
      'order by Nome')
    Params = <>
    Left = 32
    Top = 232
  end
  object DSintermediataDa: TDataSource
    DataSet = QintermediataDa
    Left = 104
    Top = 232
  end
  object Qfind_durata: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'SessionAgenzia_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from aliquote'
      'where rif_cod_compagnia=1'
      'and durata=:durata'
      'and tipo=:tipo')
    Params = <
      item
        DataType = ftUnknown
        Name = 'durata'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end>
    Left = 658
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'durata'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end>
  end
end
