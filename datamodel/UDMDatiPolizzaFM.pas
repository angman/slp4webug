unit UDMDatiPolizzaFM;

interface

uses
  SysUtils, Classes, UDMDatiBasePolizza, dbisamtb, Data.DB, UTipoSalvataggioDocumento;

type
  TDMDatiPolizzaFM = class(TDMDatiBasePolizza)
  private
    { Private declarations }
  protected
    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): integer; override;

    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); override;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); override;

  public
    { Public declarations }
  end;

function DMDatiPolizzaFM: TDMDatiPolizzaFM;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule;

function DMDatiPolizzaFM: TDMDatiPolizzaFM;
begin
  Result := TDMDatiPolizzaFM(UniMainModule.GetModuleInstance(TDMDatiPolizzaFM));
end;

{ TDMDatiPolizzaFM }

procedure TDMDatiPolizzaFM.SalvaAssicurato(DataSetAssicurati: TDataSet);
begin
  inherited;
  QInsAssicurato.ExecSQL;

end;

procedure TDMDatiPolizzaFM.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  inherited;
  QInsGaranzia.ExecSQL;

end;

function TDMDatiPolizzaFM.SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio): integer;
begin
  SalvaBasePolizza(DataSetPolizza, DataPreventivo, TipoSalvataggio);
  QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger := 0;
  QInsPolizza.ExecSQL;

  Result := getLastCodPolizza;
end;

initialization

RegisterClass(TDMDatiPolizzaFM);

end.
