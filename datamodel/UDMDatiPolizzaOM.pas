unit UDMDatiPolizzaOM;

interface

uses
  System.SysUtils, System.Classes, UDMDatiBasePolizza, dbisamtb, Data.DB, UTipoSalvataggioDocumento;

type
  TDMDatiPolizzaOM = class(TDMDatiBasePolizza)
  private
    { Private declarations }
  protected
    procedure RiprendiPolizza(QSLPTPolizze, DataSetPolizza, QassPol: TDataSet); override;
    procedure RiprendiFamiliare(QAssPol, DataSetPolizza: TDataSet; ix: Integer); override;
    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): integer; override;
    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); override;
    procedure SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer); override;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); override;

  public
    { Public declarations }

  end;

implementation
uses libslp;
{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}
{ TDMDatiBasePolizzaOM }

procedure TDMDatiPolizzaOM.SalvaAssicurato(DataSetAssicurati: TDataSet);
begin
  inherited;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaOM.SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer);
begin
  inherited;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaOM.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  inherited;
  QInsGaranzia.ExecSQL;
end;

function TDMDatiPolizzaOM.SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio): integer;
begin
  SalvaBasePolizza(DataSetPolizza, DataPreventivo, TipoSalvataggio);
  QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger := DataSetPolizza.FieldByName('NUMEROASSICURATI').AsInteger;

  QInsPolizza.ExecSQL;

  Result := getLastCodPolizza;
end;

procedure TDMDatiPolizzaOM.RiprendiPolizza(QSLPTPolizze, DataSetPolizza, QassPol: TDataSet);
begin
  RiprendiBasePolizza(QSLPTPolizze, DataSetPolizza, QassPol);
  DataSetPolizza.FieldByName('NUMEROASSICURATI').AsInteger := QSLPTPolizze.FieldByName('NUMERO_ASSICURATI').AsInteger;
  if year(DataSetPolizza.FieldByName('ScadenzaPolizzaSostituita').AsDateTime)<1950 then
     DataSetPolizza.FieldByName('ScadenzaPolizzaSostituita').Clear;
  DataSetPolizza.Post;
end;

procedure TDMDatiPolizzaOM.RiprendiFamiliare(QAssPol, DataSetPolizza: TDataSet; ix: Integer);
begin
  DataSetPolizza.Edit;
  DataSetPolizza.FieldByName('NOME' + ix.ToString).asString := QAssPol.FieldByName('DENOMINAZIONE').asString;
  // QInsAssicurato.ParamByName('COD_FISC_IVA').Clear;
  // QInsAssicurato.ParamByName('NOTE').Clear;
  // QInsAssicurato.ParamByName('ENTRATA').Clear;
  // QInsAssicurato.ParamByName('USCITA').Clear;
  // QInsAssicurato.ParamByName('CAUSALE_USCITA').Clear;
  // QInsAssicurato.ParamByName('SOSTITUITO_DA').Clear;
  DataSetPolizza.FieldByName('PATENTE' + ix.ToString).asString := QAssPol.FieldByName('PATENTE').asString;
  DataSetPolizza.FieldByName('CATPATENTE' + ix.ToString).asString := QAssPol.FieldByName('CATEGORIA_PAT').asString;
  DataSetPolizza.Post;
end;

initialization

RegisterClass(TDMDatiPolizzaOM);

end.
