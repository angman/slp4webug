unit UDMDatiTempPolizzaDP843;

interface

uses
  SysUtils, Classes, UDMDatiTempPolizza, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, frxClass, frxExportBaseDialog, frxExportPDF,
  // frxThreading,
  frxGZip, frxRich;

type
  TEnumCaratteristicaPatente = (cpNone, cpCap, cpCapCqc, cpCapCqcAdr);

  TDMDatiTempPolizzaDP843 = class(TDMDatiTempPolizza)
    fdmtblPolizzaTipoAssicurato: TStringField;
    fdmtblPolizzaAppControversie: TBooleanField;
    fdmtblPolizzaProblematicheRCA: TBooleanField;
    fdmtblPolizzaPerizieParte: TBooleanField;
    fdmtblPolizzaProtezioneFamiglia: TBooleanField;
    fdmtblPolizzaNumeroAssicurati: TIntegerField;
    fdmtblPolizzaIndicizzata: TBooleanField;
    fdmtblPolizzaSiglaRimborsoSpesePatPunti: TStringField;
    fdmtblPolizzaNome1: TStringField;
    fdmtblPolizzaNome2: TStringField;
    fdmtblPolizzaNome3: TStringField;
    fdmtblPolizzaNome4: TStringField;
    fdmtblPolizzaPatente1: TStringField;
    fdmtblPolizzaPatente2: TStringField;
    fdmtblPolizzaPatente3: TStringField;
    fdmtblPolizzaPatente4: TStringField;
    fdmtblPolizzaCatPatente1: TStringField;
    fdmtblPolizzaCatPatente2: TStringField;
    fdmtblPolizzaCatPatente3: TStringField;
    fdmtblPolizzaCatPatente4: TStringField;
    fdmtblPolizzaRecuperoDanni: TBooleanField;
    fdmtblPolizzaLegaleDomiciliatario: TBooleanField;
    fdmtblPolizzaRecuperoDanniFam: TBooleanField;
    fdmtblPolizzaDifPenVitaPrivata: TBooleanField;
    fdmtblPolizzaPatenteHaCAP: TBooleanField;
    fdmtblPolizzaPatenteHaCAPCQC: TBooleanField;
    fdmtblPolizzaPatenteHaCAPCQCADR: TBooleanField;
    fdmtblPolizzaRimborsoSpesePatPunti: TBooleanField;
    frxRichObject1: TfrxRichObject;

    procedure fdmtblPolizzaNewRecord(DataSet: TDataSet);
    procedure fdmtblPolizzaNumeroAssicuratiChange(Sender: TField);
    procedure fdmtblPolizzaNome1Change(Sender: TField);
    procedure fdmtblPolizzaPatente1Change(Sender: TField);
    procedure fdmtblPolizzaAppControversieChange(Sender: TField);
    procedure fdmtblPolizzaProblematicheRCAChange(Sender: TField);
    procedure fdmtblPolizzaPerizieParteChange(Sender: TField);
    procedure fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
    procedure fdmtblPolizzaNPercAccessoriChange(Sender: TField);
    procedure fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange(Sender: TField);
    procedure fdmtblPolizzaRecuperoDanniChange(Sender: TField);
    procedure fdmtblPolizzaLegaleDomiciliatarioChange(Sender: TField);
    procedure fdmtblPolizzaDifPenVitaPrivataChange(Sender: TField);
    procedure fdmtblPolizzaProtezioneFamigliaChange(Sender: TField);
    procedure fdmtblPolizzaTipoAssicuratoChange(Sender: TField);
    procedure fdmtblPolizzaRimborsoSpesePatPuntiChange(Sender: TField);
    procedure fdmtblPolizzaRecuperoDanniFamChange(Sender: TField);
    procedure fdmtblAssicuratoCategoriaPatenteChange(Sender: TField);
    procedure fdmtblPolizzaFormaChange(Sender: TField);

  private
    { Private declarations }
    inc_max, inc_diaria, inc_punti, inc_perizie, inc_rca, inc_famiglia, inc_taxi, imp_risc_acc, imp_rca, imp_est_c,
      imp_base, imp_pp, imp_fam, inc_rec_danni: currency;
    inc_legale, inc_dp, inc_cc, inc_max_dp, inc_rdFam, inc_dpvpFam, inc_estPAT: currency;
    pr_acc: currency;

    SavCategoriaPatente: string;

  protected
    procedure MemorizzaTempAssicurati(CodPolizza: Integer); override;
    procedure MemorizzaTempAssicurato(CodPolizza: Integer; Ix: Integer); override;

    procedure preparaReport; override;
    function per_allegati: string; override;

  public
    { Public declarations }
    procedure cal_premio; override;

    procedure ClearFamiliari(startFrom: Integer = 1);
    procedure CopiaDatiContraente(ACodCliente: Integer); override;
    function getCaratteristicaPatente: TEnumCaratteristicaPatente;
    procedure DoControlli(BaseOnly: Boolean = False); override;
    procedure ImpostaCaratteristicaPatente(AcaratteristicaPatente: TEnumCaratteristicaPatente);
    function LeggiCaratteristicaPatente: Integer;
    procedure MemorizzaTempGaranzie(TipoPolizza: Integer); override;
    function ReadTipoAssicurato: string; override;
    function forza_premi: Currency;

  end;

function DMDatiTempPolizzaDP843: TDMDatiTempPolizzaDP843;

implementation

{$R *.dfm}

uses UniGUIVars, MainModule, UCodiciErroriPolizza, System.Math, libreria, ServerModule, UdmdatiAge, uniGUIApplication,
  System.StrUtils, libSLP, System.DateUtils, UPolizzaExceptions;

const
  SIGLA_PROBLEMATICHE_RCA     = 'ED1';
  SIGLA_PERIZIE_PARTE         = 'EE1';
  SIGLA_APPCONTROVERSIE       = 'ECC';
  SIGLA_PROTEZIONE_FAMIGLIA   = 'EF';
  SIGLA_RECUPERO_DANNI        = 'ERD';
  SIGLA_LEGALE_DOMICILIATARIO = 'ESD';
  SIGLA_RECUPERO_DANNI_FAM    = 'ASF';
  SIGLA_DIFPEN_VITAPRIVATA    = 'VP';
  SIGLA_PATENTE_CAP           = 'EP1';
  SIGLA_PATENTE_CAPCQC        = 'EP2';
  SIGLA_PATENTE_CAPCQCADR     = 'EP3';
  SIGLA_Est_Patente_taxi      = 'EXT';

function DMDatiTempPolizzaDP843: TDMDatiTempPolizzaDP843;
begin
  Result := TDMDatiTempPolizzaDP843(UniMainModule.GetModuleInstance(TDMDatiTempPolizzaDP843));
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaNewRecord(DataSet: TDataSet);
var
  i: Integer;
begin
  InNewRecord                                      := True;
  fdmtblPolizzaProtezioneFamiglia.OnChange         := nil;
  fdmtblPolizzaNumeroAssicurati.OnChange           := nil;
  fdmtblPolizzaNome1.OnChange                      := nil;
  fdmtblPolizzaPatente1.OnChange                   := nil;
  fdmtblPolizzaForma.OnChange                      := nil;
  fdmtblPolizzaAppControversie.OnChange            := nil;
  fdmtblPolizzaProblematicheRCA.OnChange           := nil;
  fdmtblPolizzaPerizieParte.OnChange               := nil;
  fdmtblPolizzaFrazionamento.OnChange              := nil;
  fdmtblPolizzaProblematicheRCA.OnChange           := nil;
  fdmtblPolizzaIndicizzata.OnChange                := nil;
  fdmtblPolizzaSiglaMassimale.OnChange             := nil;
  fdmtblPolizzaSiglaRimborsoSpesePatPunti.OnChange := nil;
  fdmtblPolizzaDifPenVitaPrivata.OnChange          := nil;
  inherited;
  try
    fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean := True;
    fdmtblPolizzaIndicizzata.AsBoolean           := False;
    fdmtblPolizzaAppControversie.AsBoolean       := False;
    fdmtblPolizzaProblematicheRCA.AsBoolean      := False;
    fdmtblPolizzaPerizieParte.AsBoolean          := False;
    fdmtblPolizzaPatenteHaCAP.AsBoolean          := False;
    fdmtblPolizzaPatenteHaCAPCQC.AsBoolean       := False;
    fdmtblPolizzaPatenteHaCAPCQCADR.AsBoolean    := False;
    fdmtblPolizzaTipoAssicurato.AsString         := 'P';
    fdmtblPolizzaNumeroAssicurati.AsInteger      := 0;
    // fdmtblPolizzaNumeroAssicurati.OnChange       := nil;
    for i := 1 to fdmtblPolizzaNumeroAssicurati.MaxValue do
    begin
      fdmtblPolizza.fieldbyname('NOME' + i.ToString).OnChange := nil;
      fdmtblPolizza.fieldbyname('PATENTE' + i.ToString).OnChange := nil;
    end;
    fdmtblPolizzaNumeroAssicurati.AsInteger := 0;
    ClearFamiliari;
  finally
    for i := 1 to fdmtblPolizzaNumeroAssicurati.MaxValue do
    begin
      fdmtblPolizza.fieldbyname('NOME' + i.ToString).OnChange := fdmtblPolizzaNome1Change;
      fdmtblPolizza.fieldbyname('PATENTE' + i.ToString).OnChange := fdmtblPolizzaPatente1Change;
    end;
    fdmtblPolizzaNome1.OnChange                      := fdmtblPolizzaNome1Change;
    fdmtblPolizzaPatente1.OnChange                   := fdmtblPolizzaPatente1Change;
    fdmtblPolizzaAppControversie.OnChange            := fdmtblPolizzaAppControversieChange;
    fdmtblPolizzaProblematicheRCA.OnChange           := fdmtblPolizzaProblematicheRCAChange;
    fdmtblPolizzaPerizieParte.OnChange               := fdmtblPolizzaPerizieParteChange;
    fdmtblPolizzaFrazionamento.OnChange              := fdmtblPolizzaFrazionamentoChange;
    fdmtblPolizzaSiglaMassimale.OnChange             := fdmtblPolizzaSiglaMassimaleChange;
    fdmtblPolizzaSiglaRimborsoSpesePatPunti.OnChange := fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange;
    fdmtblPolizzaNumeroAssicurati.OnChange           := fdmtblPolizzaNumeroAssicuratiChange;
    fdmtblPolizzaForma.OnChange                      := fdmtblPolizzaFormaChange;;
    fdmtblPolizzaProtezioneFamiglia.OnChange         := fdmtblPolizzaProtezioneFamigliaChange;
    fdmtblPolizzaDifPenVitaPrivata.OnChange          := fdmtblPolizzaDifPenVitaPrivataChange;

    InNewRecord := False;

  end;
end;

procedure TDMDatiTempPolizzaDP843.CopiaDatiContraente(ACodCliente: Integer);
begin
  inherited;
  if not(fdmtblPolizza.State in [dsInsert, dsEdit]) then
    fdmtblPolizza.Edit;
  fdmtblPolizzaTipoAssicurato.AsString := 'P';

end;

procedure TDMDatiTempPolizzaDP843.DoControlli(BaseOnly: Boolean);

  function HpQlNeeded: Boolean;
  begin
    Result := (fdmtblAssicuratoTipoVeicolo.AsString = 'AC') or (fdmtblAssicuratoTipoVeicolo.AsString = 'TR') or
      (fdmtblAssicuratoTipoVeicolo.AsString = 'MT');
  end;

  procedure ValidaFamiliari;
  var
    i: Integer;
  begin
    if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    begin
      if fdmtblPolizzaNumeroAssicurati.AsInteger = 0 then
        // Se si attiva l''estensione per il nucleo familiare BISOGNA inserire almeno una persona !');
        raise EPolizzaError.Create(CERR_ESTENSIONE_FAM_MISSED, MSG_ESTENSIONE_FAM_MISSED);

      // if (RGforma.ItemIndex = 1) or ((Pos('C', Ecategoria.text) > 0) or (Pos('D', Ecategoria.text) > 0) or
      // (Pos('E', Ecategoria.text) > 0)) then
      // begin
      // ShowMessage('Attenzione ! Con la forma B non si pu� inserire l''estensione Famiglia.');
      // esci := true;
      // end;

      for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
        if (fdmtblPolizza.fieldbyname('NOME' + i.ToString).AsString = '') or
        // (fdmtblPolizza.fieldbyname('PATENTE' + i.ToString).AsString = '') or   // mb 16052020
          (fdmtblPolizza.fieldbyname('CATPATENTE' + i.ToString).AsString = '') then
          raise EPolizzaError.Create(CERR_FAMILIARI_MANCANTI, MSG_FAMILIARI_MANCANTI);

    end;
  end;

begin
  inherited DoControlli(BaseOnly);
  if BaseOnly then
    Exit;
  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and (fdmtblAssicuratoTarga.AsString = '') then
    raise EPolizzaError.Create(CERR_TARGA_VEICOLO_MISSED, MSG_TARGA_VEICOLO_MISSED);

  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and HpQlNeeded and (fdmtblAssicuratoHpQl.AsString = '') then
    raise EPolizzaError.Create(CERR_HPQL_VEICOLO_MISSED, MSG_HPQL_VEICOLO_MISSED);
  // verifica che se la polizza � sul veicolo la forma sia coerente al veicolo inserito
  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and HpQlNeeded and (fdmtblPolizzaForma.AsString = 'A') and
    (fdmtblAssicuratoHpQl.AsInteger > 35) then
  begin
    fdmtblPolizzaForma.AsString := 'B';
    raise EPolizzaError.Create(CERR_FORMA_B_NEEDED, MSG_FORMA_B_NEEDED);
  end;
  if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and (fdmtblPolizzaDurataAnni.AsInteger > 5) then
    raise EPolizzaError.Create(CERR_VEICOLO_DURATA_TOO_LONG, MSG_VEICOLO_DURATA_TOO_LONG);

  { TODO 1 -oAngelo -cRefactoring : Rivedere gestione veicolo aggiuntivo per patenti assicurate }

  { if (fdmtblPolizzaTipoAssicurato.AsString = 'V') and fdmtblPolizzaAppControversie.asboolean then
    if (CBpatente.Checked) and (CBcontr_contr.Checked) then
    begin
    // se c'� lappendice per le controversie contrattuali e abbiamo una patente in garanzia
    // non permettere la stampa se non sono stati caricati i dati del veicolo oggetto dell'estensione
    if not(veicoloAggiuntivo.targa > '') then
    begin
    ShowMessage
    ('Con l''estensione alle controversie contrattuali e le garanzie di polizza sulla patente � necessario indicare i dati identificativi del veicolo oggetto dell''estensione.');
    Result := False;
    end;

    end; }
  ValidaFamiliari;

end;

procedure TDMDatiTempPolizzaDP843.fdmtblAssicuratoCategoriaPatenteChange(Sender: TField);
var
  CategoriaPatente: string;
  i: Integer;
begin
  CategoriaPatente := fdmtblAssicuratoCategoriaPatente.AsString;
  if (fdmtblAssicurato.RecNo = 1) and (fdmtblPolizzaNumeroAssicurati.AsInteger > 0) and
    (fdmtblAssicuratoCategoriaPatente.AsString <> '') then
  begin
    for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      fdmtblPolizza.fieldbyname('CATPATENTE' + i.ToString).AsString := CategoriaPatente;
  end;

end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaAppControversieChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaDifPenVitaPrivataChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaFormaChange(Sender: TField);
var
  sgMassimale: string;
begin
  sgMassimale                          := siglaMassimale;
  fdmtblPolizzaSiglaMassimale.AsString := Copy(sgMassimale, 1, Length(sgMassimale) - 1) + fdmtblPolizzaForma.AsString;;
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaNome1Change(Sender: TField);

  procedure ControllaNomiFamiliari;
  var
    i: Integer;
    j: Integer;
  begin
    if TStringField(Sender).AsString <> '' then
      for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      begin
        if (fdmtblAssicuratoDenominazione.AsString <> '') and
          (fdmtblAssicuratoDenominazione.AsString = fdmtblPolizza.fieldbyname('Nome' + i.ToString).AsString) then
          // familiare corrispondente al contraente
          raise EPolizzaError.Create(CERR_FAMILIARE_EQ_CONTRAENTE, Format(MSG_FAMILIARE_EQ_CONTRAENTE, [i]));

        for j := i + 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
        begin
          if (fdmtblPolizza.fieldbyname('Nome' + i.ToString) <> Sender) and
            (fdmtblPolizza.fieldbyname('Nome' + i.ToString).AsString = fdmtblPolizza.fieldbyname('Nome' + j.ToString)
            .AsString) and (fdmtblPolizza.fieldbyname('Nome' + i.ToString).AsString <> '') then
            raise EPolizzaError.Create(CERR_NOME_FAMILIARE_DUP, Format(MSG_NOME_FAMILIARE_DUP, [i, j]));

        end;
      end;
  end;

begin
  ControllaNomiFamiliari;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaNumeroAssicuratiChange(Sender: TField);

  procedure setCategoriaPatenteFamiliari;
  var
    i: Integer;
  begin
    for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      fdmtblPolizza.fieldbyname('CatPatente' + i.ToString).AsString := fdmtblAssicuratoCategoriaPatente.AsString;
  end;

begin
  if (fdmtblPolizzaNumeroAssicurati.AsInteger > 0) and (fdmtblPolizzaNumeroAssicurati.AsInteger <= MAX_NUM_ASSICURATI)
  then
  begin
    ClearFamiliari(fdmtblPolizzaNumeroAssicurati.AsInteger + 1);
    setCategoriaPatenteFamiliari;
    calcolaPremio;
  end
  else
    if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    begin
      if fdmtblPolizzaNumeroAssicurati.AsInteger = 0 then
        raise EPolizzaError.Create(CERR_NUM_ASSICURATI_ZERO, MSG_NUM_ASSICURATI_ZERO)
      else
        raise EPolizzaError.Create(CERR_NUM_ASSICURATI, Format(MSG_NUM_ASSICURATI, [MAX_NUM_ASSICURATI]));
    end
    else
      calcolaPremio;

end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaPatente1Change(Sender: TField);

  procedure ControllaPatenteFamiliari;
  var
    i: Integer;
    j: Integer;
  begin
    if TStringField(Sender).AsString <> '' then
      for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
      begin
        if (fdmtblAssicuratoPatente.AsString <> '') and
          (fdmtblAssicuratoPatente.AsString = fdmtblPolizza.fieldbyname('Patente' + i.ToString).AsString) then
          // familiare corrispondente al contraente
          raise EPolizzaError.Create(CERR_PAT_FAMILIARE_EQ_CONTRAENTE, Format(MSG_PAT_FAMILIARE_EQ_CONTRAENTE, [i]));

        for j := i + 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
          if (fdmtblPolizza.fieldbyname('Patente' + i.ToString) <> Sender) and
            (fdmtblPolizza.fieldbyname('Patente' + i.ToString).AsString = fdmtblPolizza.fieldbyname('Patente' +
            j.ToString).AsString) and (fdmtblPolizza.fieldbyname('Patente' + i.ToString).AsString <> '') then
            raise EPolizzaError.Create(CERR_PATENTE_FAMILIARE_DUP, Format(MSG_PATENTE_FAMILIARE_DUP, [i, j]));

      end;
  end;

begin
  ControllaPatenteFamiliari;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaPerizieParteChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaLegaleDomiciliatarioChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaProblematicheRCAChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaProtezioneFamigliaChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaRecuperoDanniChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaRecuperoDanniFamChange(Sender: TField);
begin
  calcolaPremio

end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaRimborsoSpesePatPuntiChange(Sender: TField);
begin
  calcolaPremio
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
begin
  fdmtblPolizzaSiglaMassimale.OnChange := nil;
  try
    calcolaPremio;
  finally
    fdmtblPolizzaSiglaMassimale.OnChange := fdmtblPolizzaSiglaMassimaleChange;
  end;
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange(Sender: TField);
begin
  calcolaPremio
end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaTipoAssicuratoChange(Sender: TField);
begin
  fdmtblAssicuratoTipo.AsString := fdmtblPolizzaTipoAssicurato.AsString;
  if fdmtblPolizzaTipoAssicurato.AsString = 'P' then
    UniMainModule.DMDatiTempPolizza.SetDatiAssicuratoDaContraente(True);

end;

function TDMDatiTempPolizzaDP843.forza_premi: Currency;
begin
   // forza i premi per i casi famiglia o single ....
    if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    begin
      // caso di single, carica i valori della polizza per i single
      nettoAnnuoTotNONScontato:=22.22;
      nettoAnnuoTotNONScontatoBis:=53.61;
    end else begin
      nettoAnnuoTotNONScontato:=35.56;
      nettoAnnuoTotNONScontatoBis:=65.98;
    end;
    Result:= nettoAnnuoTotNONScontato + nettoAnnuoTotNONScontatoBis;
end;

function TDMDatiTempPolizzaDP843.getCaratteristicaPatente: TEnumCaratteristicaPatente;
begin
  Result := cpNone;
  if fdmtblPolizzaPatenteHaCAP.AsBoolean then
    Result := cpCap
  else
    if fdmtblPolizzaPatenteHaCAPCQC.AsBoolean then
      Result := cpCapCqc
    else
      if fdmtblPolizzaPatenteHaCAPCQCADR.AsBoolean then
        Result := cpCapCqcAdr;

end;

procedure TDMDatiTempPolizzaDP843.ImpostaCaratteristicaPatente(AcaratteristicaPatente: TEnumCaratteristicaPatente);
begin
  fdmtblPolizzaPatenteHaCAP.AsBoolean       := AcaratteristicaPatente = cpCap;
  fdmtblPolizzaPatenteHaCAPCQC.AsBoolean    := AcaratteristicaPatente = cpCapCqc;
  fdmtblPolizzaPatenteHaCAPCQCADR.AsBoolean := AcaratteristicaPatente = cpCapCqcAdr;
  calcolaPremio;

end;

function TDMDatiTempPolizzaDP843.LeggiCaratteristicaPatente: Integer;
begin
  if fdmtblPolizzaPatenteHaCAP.AsBoolean then
    Result := Integer(cpCap)
  else
    if fdmtblPolizzaPatenteHaCAPCQC.AsBoolean then
      Result := Integer(cpCapCqc)
    else
      if fdmtblPolizzaPatenteHaCAPCQCADR.AsBoolean then
        Result := Integer(cpCapCqcAdr)
      else
        Result := Integer(cpNone);

end;

procedure TDMDatiTempPolizzaDP843.fdmtblPolizzaNPercAccessoriChange(Sender: TField);
begin
  inherited;
  //
end;

function TDMDatiTempPolizzaDP843.per_allegati: string;
begin
  // genera la stringa da passare a stamp_moduli per sapere quali crocette mettere
  // nel modulo dell'adeguatezza
  inherited;
  Result := Result + '.C1'; // crocetta per circolazione stradale
  if fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString > '' then
    Result := Result + '.C3'; // patente a punti

  // if fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean then result:=result+'.C3';   // patente a punti
  if fdmtblPolizzaProblematicheRCA.AsBoolean then
    Result := Result + '.C4'; // problematica RCA
  if fdmtblPolizzaPerizieParte.AsBoolean then
    Result := Result + '.C5'; // perizie di parte per furto incendio
  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    Result := Result + '.C6'; // famiglia

  if fdmtblPolizzaRecuperoDanni.AsBoolean then
    Result := Result + '.C2'; // recupero danni
  if fdmtblPolizzaLegaleDomiciliatario.AsBoolean then
    Result := Result + '.C7'; // domiciliatario
  if fdmtblPolizzaAppControversie.AsBoolean then
    Result := Result + '.C8'; // controversie contrattuali

  if fdmtblPolizzaRecuperoDanniFam.AsBoolean then
    Result := Result + '.C9'; // recupero dani subiti dai familiari
  if fdmtblPolizzaDifPenVitaPrivata.AsBoolean then
    Result := Result + '.V1'; // difesa penale vita privata
  if fdmtblPolizzaDifPenVitaPrivata.AsBoolean then
    Result := Result + '.V2'; // seconda crocetta difesa penale vita privata
  Result   := Copy(Result, 2, 40); // togli il punto iniziale ...
end;

procedure TDMDatiTempPolizzaDP843.cal_premio;
var sigmax: string;
begin
  // calcola il premio annuale netto in base alla tariffa relativa alla polizza corrente
  // non tenere conto di frazionamento, ratei o sconti vari: SOLO PREMIO NETTO ANNUO TECNICO
  imp_risc_acc := 0;

  imp_rca       := 0;
  imp_est_c     := 0;
  imp_base      := 0;
  imp_pp        := 0;
  imp_fam       := 0;
  inc_max       := 0;
  inc_rec_danni := 0;
  inc_diaria    := 0;
  inc_punti     := 0;
  inc_perizie   := 0;
  inc_famiglia  := 0;
  inc_legale    := 0;
  inc_rdFam     := 0;
  inc_rca       := 0;
  inc_taxi      := 0;
  inc_dp        := 0;
  inc_cc        := 0;
  inc_max_dp    := 0;
  inc_dpvpFam   := 0;
  inc_estPAT    := 0;

  // preleva i lpremio dall'archivio garanzie della polizza corrente
  // controlla_Veicolo_forma;
  // // in base alla forma A o B indicata in polizza
  // case RGforma.itemindex of
  // 0:
  // fo := 'A';
  // 1:
  // fo := 'B';
  // end;

  pr_acc                      := 0;
  nettoAnnuoTotNONScontato    := 0;
  nettoAnnuoTotNONScontatoBis := 0;

  // if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, siglaMassimale) then
  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then sigmax:='1AS'
  else sigmax:='1A';
  if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, sigmax) then
    nettoAnnuoTotNONScontato := UniMainModule.DMDatiBasePolizza.Qgar_base.fieldbyname('PREMIO').asFloat;
  imp_base                   := nettoAnnuoTotNONScontato;

  if fdmtblPolizzaDifPenVitaPrivata.AsBoolean then
  begin
     // estensione difesa penale per nucleo famigliare  al 21,25%
     if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
        AggiornaNettoPerEstensione(fdmtblPolizzaDifPenVitaPrivata, '1VPS', inc_dpvpFam, True)
     else
        AggiornaNettoPerEstensione(fdmtblPolizzaDifPenVitaPrivata, '1VP', inc_dpvpFam, True);
  end;

  if fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean and (fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString <> '') then
  begin
    AggiornaNettoPerEstensione(fdmtblPolizzaRimborsoSpesePatPunti,
      Copy(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, 1, 2), imp_risc_acc);
    if fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean = False then
      fdmtblPolizzaSiglaRimborsoSpesePatPunti.Clear;
  end
  else
    fdmtblPolizzaSiglaRimborsoSpesePatPunti.Clear;

  // estensione per legale domiciliatario
  // AggiornaNettoPerEstensione(fdmtblPolizzaLegaleDomiciliatario, SIGLA_LEGALE_DOMICILIATARIO, inc_legale);

  fdmtblPolizza.fieldbyname('PerAllegati').AsString := per_allegati;

end;

procedure TDMDatiTempPolizzaDP843.ClearFamiliari(startFrom: Integer = 1);
var
  i: Integer;
begin
  for i := startFrom to MAX_NUM_ASSICURATI do
  begin
    fdmtblPolizza.fieldbyname('Nome' + i.ToString).Clear;
    fdmtblPolizza.fieldbyname('Patente' + i.ToString).Clear;
    fdmtblPolizza.fieldbyname('CatPatente' + i.ToString).Clear;
  end;

end;

procedure TDMDatiTempPolizzaDP843.preparaReport;
var
  per_famiglia, tipoVeicolo, ext: string;
  ra3, ttu: currency;

  procedure istanziaCampiAssicurati;
  var
    i: Integer;
  begin
    frxPolizzaGen.Variables['Xpat']  := QuotedStr('X');
    frxPolizzaGen.Variables['Xveic'] := QuotedStr(' ');

    fdmtblAssicurato.First;
    frxPolizzaGen.Variables['in_garanzia'] := QuotedStr(alltrim(fdmtblAssicuratoDenominazione.AsString) + '  Pat. n�:' +
      padr(fdmtblAssicuratoPatente.AsString, ' ', 20) + '  Cat.:' + padr(fdmtblAssicuratoCategoriaPatente.AsString, ' ',
      7) + '  Ril. il:' + (fdmtblAssicuratoDataRilascio.AsString) + ' ' + fdmtblAssicuratoRilasciataDa.AsString);

    frxPolizzaGen.Variables['n_fam'] := QuotedStr('N� familiari ' + i.ToString);
    // frxPolizzaGen.Variables['nettoAnnuoTot'] :=
    // QuotedStr(zero(Opremio.nettoTotaleRicalcolato + Opremio.scontoDurataBase + inc_dpvpFam -
    // OpremioBis.arrotondamento));
    frxPolizzaGen.Variables['nettoAnnuoTot'] :=
      QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency + fdmtblPolizzaSconto.AsCurrency + inc_dpvpFam -
      OpremioBis.arrotondamento));

    per_famiglia := '';
    fdmtblAssicurato.Next;
    i := 0;
    while not fdmtblAssicurato.Eof do
    begin
      Inc(i);
      per_famiglia := per_famiglia + '(' + i.ToString + ') ' + fdmtblAssicuratoDenominazione.AsString + '  Pat. n�:' +
        padr(fdmtblAssicuratoPatente.AsString, ' ', 20) + '  Cat.:' +
        padr(fdmtblAssicuratoCategoriaPatente.AsString, ' ', 7);
      fdmtblAssicurato.Next;
      frxPolizzaGen.Variables['in_garanzia1'] := QuotedStr(per_famiglia);
    end;

    frxPolizzaGen.Variables['n_fam'] := QuotedStr('N� familiari ' + fdmtblPolizzaNumeroAssicurati.AsString);

    frxPolizzaGen.Variables['nettoAnnuoTot'] :=
      QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency + fdmtblPolizzaSconto.AsCurrency));
    frxPolizzaGen.Variables['Xpat']  := QuotedStr(' ');
    frxPolizzaGen.Variables['Xveic'] := QuotedStr('X');
    fdmtblAssicurato.First;
  end;

begin
  stringaTest                         := QuotedStr(' ');
  StringaChecked                      := QuotedStr('X');
  SLPDatiAge                          := UniMainModule.DMdatiAge.SLPdati_age;
  frxPolizzaGen.EngineOptions.TempDir := SLPDatiAge.dati_gen.dir_temp;
  AzzeraCampiReport;

  frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');

  if isPolizza then
  begin
    frxPolizzaGen.Variables['polizza']   := QuotedStr(UniMainModule.NumPolizza);
    frxPolizzaGen.Variables['barcode1']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 1));
    frxPolizzaGen.Variables['barcode2']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 2));
    frxPolizzaGen.Variables['facsimile'] := QuotedStr('N');
  end
  else
    if isPreventivo then
    begin
      frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
      frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
    end;
  // imposta i campi del frxPolizzaGen

  frxPolizzaGen.Variables['perc_sconto'] := QuotedStr('-' + alltrim(FloatToStr(fdmtblPolizzaPSconto.AsCurrency)) + '%');

  frxPolizzaGen.Variables['versione']     := QuotedStr(leggiVersione);
  frxPolizzaGen.Variables['peragenzia']   := QuotedStr('N');
  frxPolizzaGen.Variables['in_garanzia']  := QuotedStr('');
  frxPolizzaGen.Variables['in_garanzia1'] := QuotedStr('');

  frxPolizzaGen.Variables['ramo']        := QuotedStr(UniServerModule.ramoSLP);
  frxPolizzaGen.Variables['agenzia']     := QuotedStr(SLPDatiAge.agenzia);
  frxPolizzaGen.Variables['citta_age']   := QuotedStr(SLPDatiAge.nome);
  frxPolizzaGen.Variables['descrizione'] := QuotedStr(SLPDatiAge.Denominazione);
  frxPolizzaGen.Variables['autostorica'] := stringaTest;
  frxPolizzaGen.Variables['camper']      := QuotedStr('N');

  frxPolizzaGen.Variables['suba'] := QuotedStr(fdmtblPolizzaSubAgenzia.AsString);

  frxPolizzaGen.Variables['subP']  := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString); // QuotedStr(Esub_promoter.text);
  frxPolizzaGen.Variables['Psost'] := QuotedStr(fdmtblPolizzaNumPolizzaSostituita.AsString);
  // QuotedStr(Epol_sost.text);
  frxPolizzaGen.Variables['scade_il'] := QuotedStr(fdmtblPolizzaScadenzaPolizzaSostituita.AsString);
  // QuotedStr(Escad_pol_sost.text);
  frxPolizzaGen.Variables['effetto']  := QuotedStr(fdmtblPolizzaDecorrenza.AsString); // QuotedStr(Eeffetto.text);
  frxPolizzaGen.Variables['scadenza'] := QuotedStr(fdmtblPolizzaScadenza.AsString); // QuotedStr(Escadenza.text);
  frxPolizzaGen.Variables['durata']   := QuotedStr('-' + fdmtblPolizzaDurataAnni.AsString + '-');
  // QuotedStr('-' + trim(Edurata_anni.text) + '-');

  frxPolizzaGen.Variables['giorni'] := QuotedStr(ifThen(fdmtblPolizzaDurataGiorni.AsString = '0', stringaTest,
    fdmtblPolizzaDurataGiorni.AsString));

  frxPolizzaGen.Variables['fraz'] := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  // QuotedStr(dai_frazionamentoE(CBfrazionamento.text));
  frxPolizzaGen.Variables['scad1rata'] := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  // QuotedStr(Escad_prima_rata.text);
  frxPolizzaGen.Variables['emissione'] := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  // QuotedStr(EdataEmissione.text);
  frxPolizzaGen.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString); // QuotedStr(Econtraente.text);
  frxPolizzaGen.Variables['nato_a']     := QuotedStr(fdmtblPolizzaLocNascita.AsString); // QuotedStr(Enato_a.text);
  frxPolizzaGen.Variables['nato_il']    := QuotedStr(ifThen(YearOf(fdmtblPolizzaDataNascita.AsDateTime) > 1900,
    fdmtblPolizzaDataNascita.AsString, ''));
  frxPolizzaGen.Variables['residente_in'] :=
    QuotedStr(fdmtblPolizzaIndirizzo.AsString + ' - ' + fdmtblPolizzaCitta.AsString + ' - ' +
    fdmtblPolizzaProvincia.AsString);
  // QuotedStr(Eindirizzo.text + ' - ' + Ecitta.text + ' (' + Eprovincia.text + ')');
  frxPolizzaGen.Variables['cap']         := QuotedStr(fdmtblPolizzaCap.AsString); // QuotedStr(Ecap.text);
  frxPolizzaGen.Variables['professione'] := QuotedStr(fdmtblPolizzaProfessioneDescriz.AsString);

  frxPolizzaGen.Variables['cf_iva'] := QuotedStr(ifThen(fdmtblPolizzaPartitaIva.AsString > '',
    fdmtblPolizzaPartitaIva.AsString, fdmtblPolizzaCodiceFiscale.AsString ));
  frxPolizzaGen.Variables['FORMA'] := QuotedStr(fdmtblPolizzaForma.AsString);
  // QuotedStr(dai_forma(RGforma.ItemIndex));

  frxPolizzaGen.Variables['X1'] := ifThen(fdmtblPolizzaForma.AsString = 'A', StringaChecked, stringaTest);
  frxPolizzaGen.Variables['X2'] := ifThen(fdmtblPolizzaForma.AsString = 'B', StringaChecked, stringaTest);


  // if CBincMaxDP.Checked then frxPolizzaGen.Variables['extMaxDP']  := QuotedStr('S');

  frxPolizzaGen.Variables['ADEGNO'] := ifThen(fdmtblPolizzaIndicizzata.AsBoolean, QuotedStr('SI'), QuotedStr('NO'));;
  frxPolizzaGen.Variables['ADEGSI'] := ifThen(fdmtblPolizzaIndicizzata.AsBoolean, QuotedStr('SI'), QuotedStr('NO'));

  // frxPolizzaGen.Variables['X_PRECDANNI'] := stringaTest;
  // frxPolizzaGen.Variables['X_PERIZIE']   := stringaTest;
  // frxPolizzaGen.Variables['X_CC']        := stringaTest;
  // frxPolizzaGen.Variables['X_LEGDOM']    := stringaTest;
  // frxPolizzaGen.Variables['X_PATPUN']    := stringaTest;
  // frxPolizzaGen.Variables['X_RCA']       := stringaTest;
  // frxPolizzaGen.Variables['X_PATFAM']    := stringaTest;
  // frxPolizzaGen.Variables['X_RDFAM']     := stringaTest;
  // frxPolizzaGen.Variables['X_DPVPFAM']   := stringaTest;

  frxPolizzaGen.Variables['P_PRECDANNI'] := QuotedStr('=======');
  frxPolizzaGen.Variables['P_PERIZIE']   := QuotedStr('=======');
  frxPolizzaGen.Variables['P_CC']        := QuotedStr('=======');
  frxPolizzaGen.Variables['P_LEGDOM']    := QuotedStr('=======');
  frxPolizzaGen.Variables['P_PATPUN']    := QuotedStr('=======');
  frxPolizzaGen.Variables['P_RCA']       := QuotedStr('=======');
  frxPolizzaGen.Variables['P_PATFAM']    := QuotedStr('=======');
  frxPolizzaGen.Variables['P_RDFAM']     := QuotedStr('=======');
  frxPolizzaGen.Variables['P_DPVPFAM']   := QuotedStr('=======');
  frxPolizzaGen.Variables['P_RECDANNI']  := QuotedStr('=======');

  frxPolizzaGen.Variables['ESTC']         := stringaTest;
  frxPolizzaGen.Variables['ESTD']         := stringaTest;
  frxPolizzaGen.Variables['ESTE']         := stringaTest;
  frxPolizzaGen.Variables['ESTF']         := stringaTest;
  frxPolizzaGen.Variables['in_garanzia1'] := stringaTest;

  istanziaCampiAssicurati;

  ext := '';
  ttu := fdmtblPolizzaNetto.AsCurrency;

  if fdmtblPolizzaProblematicheRCA.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_RCA'] := StringaChecked;
    frxPolizzaGen.Variables['P_RCA'] := QuotedStr(zero(getImportoGaranzia(SIGLA_PROBLEMATICHE_RCA)));
    ext                              := ext + 'C';
  end;

  if fdmtblPolizzaPerizieParte.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_PERIZIE'] := StringaChecked;
    frxPolizzaGen.Variables['P_PERIZIE'] := QuotedStr(zero(getImportoGaranzia(SIGLA_PERIZIE_PARTE)));
    ext                                  := ext + ' - D';
  end;

  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_PATFAM'] := StringaChecked;
    frxPolizzaGen.Variables['P_PATFAM'] := QuotedStr(zero(getImportoGaranzia(SIGLA_PROTEZIONE_FAMIGLIA)));
    ext                                 := ext + ' - E';
  end;

  if fdmtblPolizzaRecuperoDanni.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_RECDANNI'] := StringaChecked;
    frxPolizzaGen.Variables['P_RECDANNI'] := QuotedStr(zero(getImportoGaranzia(SIGLA_RECUPERO_DANNI)));
  end;

  if fdmtblPolizzaAppControversie.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_CC'] := StringaChecked;
    frxPolizzaGen.Variables['P_CC'] := QuotedStr(zero(getImportoGaranzia(SIGLA_APPCONTROVERSIE)));
  end;

  if fdmtblPolizzaLegaleDomiciliatario.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_LEGDOM'] := StringaChecked;
    frxPolizzaGen.Variables['P_LEGDOM'] := QuotedStr(zero(getImportoGaranzia(SIGLA_LEGALE_DOMICILIATARIO)));
  end;

  if fdmtblPolizzaRecuperoDanniFam.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_RDFAM'] := StringaChecked;
    frxPolizzaGen.Variables['P_RDFAM'] := QuotedStr(zero(getImportoGaranzia(SIGLA_RECUPERO_DANNI_FAM)));
  end;

  if fdmtblPolizzaDifPenVitaPrivata.AsBoolean then
  begin
    frxPolizzaGen.Variables['X_DPVPFAM'] := StringaChecked;
    frxPolizzaGen.Variables['P_DPVPFAM'] :=
      QuotedStr(zero(getImportoGaranzia(Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 1) + SIGLA_DIFPEN_VITAPRIVATA) -
      OpremioBis.arrotondamento));
  end;

  if (fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString <> '') then
  begin
    ra3 := getImportoGaranzia(Copy(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, 1, 2));

    frxPolizzaGen.Variables['X_PATPUN'] := StringaChecked;
    frxPolizzaGen.Variables['RA1']      := QuotedStr(zero(fdmtblGaranziaMassimale.AsCurrency));
    frxPolizzaGen.Variables['RA2']      := QuotedStr(zero(fdmtblGaranziaValoreAssicurato.AsCurrency));
    ttu                                 := ttu - ra3;
    ra3                                 := imp_risc_acc;

    frxPolizzaGen.Variables['P_PATPUN'] := QuotedStr(zero(ra3));
  end;

  if fdmtblPolizzaPatenteHaCAP.AsBoolean then
    frxPolizzaGen.Variables['X_CAP'] := StringaChecked
  else
    if fdmtblPolizzaPatenteHaCAPCQC.AsBoolean then
      frxPolizzaGen.Variables['X_CQC'] := StringaChecked
    else
      if fdmtblPolizzaPatenteHaCAPCQCADR.AsBoolean then
        frxPolizzaGen.Variables['X_ADR'] := StringaChecked;

  getImportoGaranzia(Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 2));
  frxPolizzaGen.Variables['max6'] :=
    QuotedStr('=' + FormatCurr('##,###,##0', fdmtblGaranziaMassimale.AsCurrency) + '=');

  frxPolizzaGen.Variables['XG'] := stringaTest;

  if ext > '' then
    frxPolizzaGen.Variables['EXT'] := QuotedStr(ext)
  else
    frxPolizzaGen.Variables['EXT'] := QuotedStr('===========');

  getImportoGaranzia(Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 2));
  frxPolizzaGen.Variables['premiobase'] :=
    QuotedStr(zero(fdmtblGaranziaPremio.AsCurrency - fdmtblPolizzaArrotondamento.AsCurrency));

  tipoVeicolo := '';

  frxPolizzaGen.Variables['varie'] := stringaTest;
  frxPolizzaGen.Variables['n_fam'] := stringaTest;

  istanziaCampiAssicurati;

  // Opremio.nettoTotaleRicalcolato + Opremio.scontoDurataBase
  frxPolizzaGen.Variables['perc_sconto'] := QuotedStr('-' + alltrim(FloatToStr(fdmtblPolizzaPSconto.AsCurrency)) + '%');

  if Opremio.scontoDurataBase <> 0 then
  begin
    frxPolizzaGen.Variables['sconto'] := QuotedStr(zero(Opremio.scontoDurataBase));
    // frxPolizzaGen.Variables['p_scontato']:=StringaChecked(zero(Opremio.nettoTotaleBaseArrotondato - Opremio.scontoDurataBase));
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
  end
  else
  begin
    frxPolizzaGen.Variables['sconto']     := QuotedStr('=======');
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr('=======');
  end;

  ttu := fdmtblPolizzaNetto.AsCurrency;


  // frxPolizzaGen.Variables['ESTC'] := StringaChecked(zero(imp_rca));
  // frxPolizzaGen.Variables['ESTD'] := StringaChecked(zero(inc_perizie));
  // frxPolizzaGen.Variables['ESTE'] := StringaChecked(zero(inc_famiglia));

  frxPolizzaGen.Variables['nettoAnnuo']    := QuotedStr(zero(imp_base - Opremio.arrotondamento));
  frxPolizzaGen.Variables['nettoAnnuoTot'] := QuotedStr(zero(Opremio.nettoTotaleBaseArrotondato + inc_dpvpFam));

  frxPolizzaGen.Variables['rate_succ'] := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['netto1']    := QuotedStr(zero(fdmtblPolizzaNetto1.AsCurrency));
  frxPolizzaGen.Variables['acc1']      := QuotedStr(zero(fdmtblPolizzaAccessori1.AsCurrency));
  frxPolizzaGen.Variables['int_fraz']  := QuotedStr(zero(fdmtblPolizzaInteressiFrazionamento1.AsCurrency));
  { TODO -oAngelo -cCampiReport : verificare righe commentate }
  frxPolizzaGen.Variables['imponibile1'] := QuotedStr(zero(fdmtblPolizzaImponibile1.AsCurrency));
  frxPolizzaGen.Variables['tasse1']      := QuotedStr(zero(fdmtblPolizzaImposte1.AsCurrency));
  frxPolizzaGen.Variables['totale1']     := QuotedStr(zero(fdmtblPolizzaTotale1.AsCurrency));
  frxPolizzaGen.Variables['alla_firma']  := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['netto2']      := QuotedStr(zero(fdmtblPolizzaNetto2.AsCurrency));
  frxPolizzaGen.Variables['acc2']        := QuotedStr(zero(fdmtblPolizzaAccessori2.AsCurrency));
  frxPolizzaGen.Variables['rimborso']    := QuotedStr(zero(fdmtblPolizzaRimborsoSost.AsCurrency));
  frxPolizzaGen.Variables['imponibile2'] := QuotedStr(zero(fdmtblPolizzaImponibile2.AsCurrency));
  frxPolizzaGen.Variables['tasse2']      := QuotedStr(zero(fdmtblPolizzaImposte2.AsCurrency));
  frxPolizzaGen.Variables['totale2']     := QuotedStr(zero(fdmtblPolizzaTotale2.AsCurrency));
  frxPolizzaGen.Variables['emissione']   := QuotedStr(fdmtblPolizzaDataEmissione.AsString);

  frxPolizzaGen.Variables['totale1b'] :=
    QuotedStr(zero(fdmtblPolizzaTotale1.AsCurrency + fdmtblPolizzaTotale1A.AsCurrency));
  frxPolizzaGen.Variables['totale2b'] :=
    QuotedStr(zero(fdmtblPolizzaTotale2.AsCurrency + fdmtblPolizzaTotale2A.AsCurrency));

  frxPolizzaGen.Variables['netto1a']      := QuotedStr(zero(fdmtblPolizzaNetto1A.AsCurrency));
  frxPolizzaGen.Variables['acc1a']        := QuotedStr(zero(fdmtblPolizzaAccessori1A.AsCurrency));
  frxPolizzaGen.Variables['int_frazA']    := QuotedStr(zero(fdmtblPolizzaInteressiFrazionamento1A.AsCurrency));
  frxPolizzaGen.Variables['imponibile1a'] := QuotedStr(zero(fdmtblPolizzaImponibile1A.AsCurrency));
  frxPolizzaGen.Variables['tasse1a']      := QuotedStr(zero(fdmtblPolizzaImposte1A.AsCurrency));
  frxPolizzaGen.Variables['totale1a']     := QuotedStr(zero(fdmtblPolizzaTotale1A.AsCurrency));
  frxPolizzaGen.Variables['netto2a']      := QuotedStr(zero(fdmtblPolizzaNetto2A.AsCurrency));
  frxPolizzaGen.Variables['acc2a']        := QuotedStr(zero(fdmtblPolizzaAccessori2A.AsCurrency));
  frxPolizzaGen.Variables['rimborsoa']    := QuotedStr(zero(fdmtblPolizzaRimborsoSostA.AsCurrency));
  frxPolizzaGen.Variables['imponibile2a'] := QuotedStr(zero(fdmtblPolizzaImponibile2A.AsCurrency));
  frxPolizzaGen.Variables['tasse2a']      := QuotedStr(zero(fdmtblPolizzaImposte2A.AsCurrency));
  frxPolizzaGen.Variables['totale2a']     := QuotedStr(zero(fdmtblPolizzaTotale2A.AsCurrency));

  if UniMainModule.DMdatiAge.TacitoRinnovoEnabled and fdmtblPolizzaTacitoRinnovo.AsBoolean then
    frxPolizzaGen.Variables['STR'] := QuotedStr('S');

end;

procedure TDMDatiTempPolizzaDP843.MemorizzaTempAssicurati(CodPolizza: Integer);
var
  i: Integer;
begin
  inherited;
  for i := 1 to fdmtblPolizzaNumeroAssicurati.AsInteger do
    MemorizzaTempAssicurato(CodPolizza, i);
end;

procedure TDMDatiTempPolizzaDP843.MemorizzaTempAssicurato(CodPolizza: Integer; Ix: Integer);
begin
  fdmtblAssicurato.Append;
  fdmtblAssicurato.Edit;

  fdmtblAssicuratoDenominazione.AsString := fdmtblPolizza.fieldbyname('NOME' + Ix.ToString).AsString;
  fdmtblAssicuratoIndirizzo.Clear;
  fdmtblAssicuratoCitta.Clear;
  fdmtblAssicuratocap.Clear;
  fdmtblAssicuratoProvincia.Clear;
  fdmtblAssicuratoCodFiscIvas.Clear;
  fdmtblAssicuratoNote.Clear;
  fdmtblAssicuratoEntrata.Clear;
  fdmtblAssicuratoUscita.Clear;
  fdmtblAssicuratoCausaleUscita.Clear;
  fdmtblAssicuratoSostituitoDa.Clear;
  fdmtblAssicuratoPatente.AsString          := fdmtblPolizza.fieldbyname('PATENTE' + Ix.ToString).AsString;
  fdmtblAssicuratoCategoriaPatente.AsString := fdmtblPolizza.fieldbyname('CATPATENTE' + Ix.ToString).AsString;
  fdmtblAssicuratoDataRilascio.Clear;
  fdmtblAssicuratoRilasciataDa.Clear;
  fdmtblAssicuratoDataScadenza.Clear;
  fdmtblAssicuratoTipoVeicolo.Clear;
  fdmtblAssicuratoTarga.Clear;
  fdmtblAssicuratoMarca.Clear;
  fdmtblAssicuratoHpQl.Clear;
  fdmtblAssicuratoRifCodTipoAssicurato.Clear;
  fdmtblAssicuratoTipo.AsString        := 'P';
  fdmtblAssicuratoCodPolizza.AsInteger := CodPolizza;
  fdmtblAssicuratoDataScadRevisione.Clear;
  fdmtblAssicuratoModello.Clear;
  fdmtblAssicuratoClasse.Clear;
  fdmtblAssicuratoTelaio.Clear;
  fdmtblAssicuratoCc.Clear;
  fdmtblAssicuratoDataImmatricolazione.Clear;
  fdmtblAssicuratoPremio.Clear;
  fdmtblAssicuratoMassimale.Clear;
  fdmtblAssicuratoPremioSLP.Clear;
  fdmtblAssicuratoOggAggiuntivo.Clear;
  fdmtblAssicuratoIdGenSlpAge.Clear;

  fdmtblAssicurato.post;
end;

procedure TDMDatiTempPolizzaDP843.MemorizzaTempGaranzie(TipoPolizza: Integer);
begin
  inherited;
  // codPolizza     := fdmtblPolizzaCodPolizza.AsInteger;
  // siglaMassimale := Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 2);
  // MemorizzaTempGaranzia(codPolizza, siglaMassimale);

  // sig:=alltrim(inttostr(CBmassimale.itemindex+1))+fo;
  // if cerca_garanzia(tipo_pol,sig) then
  // begin

  if fdmtblPolizzaRimborsoSpesePatPunti.AsBoolean and (fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString <> '') then
  begin
    // siglaMassimale := Copy(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, 1, 2);
    MemorizzaTempGaranzia(CodPolizza, Copy(fdmtblPolizzaSiglaRimborsoSpesePatPunti.AsString, 1, 2));
  end;

  // cerca le eventuali appendici
  // sig
  // estensione per la problematica RCA
  if fdmtblPolizzaProblematicheRCA.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, SIGLA_PROBLEMATICHE_RCA);

  // estensione per le perizie di parte ed arbitrati
  if fdmtblPolizzaPerizieParte.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, SIGLA_PERIZIE_PARTE);

  // estensione per le controversie contrattuali
  if fdmtblPolizzaAppControversie.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, SIGLA_APPCONTROVERSIE);

  // estensione per la famiglia
  if fdmtblPolizzaProtezioneFamiglia.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, SIGLA_PROTEZIONE_FAMIGLIA, inc_famiglia);

  // estensione per il recupero dei danni
  if fdmtblPolizzaRecuperoDanni.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, SIGLA_RECUPERO_DANNI);

  // estensione per legale domiciliatario
  if fdmtblPolizzaLegaleDomiciliatario.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, SIGLA_LEGALE_DOMICILIATARIO);

  // estensione per recupero danni famigliari come trasportati
  if fdmtblPolizzaRecuperoDanniFam.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, SIGLA_RECUPERO_DANNI_FAM);

  // estensione difesa penale per nucleo famigliare  al 21,25%
  if fdmtblPolizzaDifPenVitaPrivata.AsBoolean then
  begin
    // siglaMassimale := Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 1);
    MemorizzaTempGaranzia(CodPolizza, Copy(siglaMassimale, 1, 1) + SIGLA_DIFPEN_VITAPRIVATA);
  end;

  // parte per la tipologia di patente ...
  // siglaMassimale := Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 1);

  if fdmtblPolizzaPatenteHaCAP.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, Copy(siglaMassimale, 1, 1) + SIGLA_PATENTE_CAP);

  if fdmtblPolizzaPatenteHaCAPCQC.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, Copy(siglaMassimale, 1, 1) + SIGLA_PATENTE_CAPCQC);

  if fdmtblPolizzaPatenteHaCAPCQCADR.AsBoolean then
    MemorizzaTempGaranzia(CodPolizza, Copy(siglaMassimale, 1, 1) + SIGLA_PATENTE_CAPCQCADR);
end;

function TDMDatiTempPolizzaDP843.ReadTipoAssicurato: string;
begin
  Result := fdmtblPolizzaTipoAssicurato.AsString;
end;

initialization

RegisterClass(TDMDatiTempPolizzaDP843);

end.
