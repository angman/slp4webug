unit UDatiTempPolizzaPM;

interface

uses
  SysUtils, Classes, UDMDatiTempPolizza, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, frxClass, frxExportBaseDialog, frxExportPDF, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, frxGZip, system.dateutils;

type
  TDMDatiTempPolizzaPM = class(TDMDatiTempPolizza)
    fdmtblPolizzaGruppoSpecializzazione: TStringField;
    fdmtblPolizzaEst13: TBooleanField;
    fdmtblPolizzaNDipAmmin: TIntegerField;
    fdmtblPolizzaIndicizzata: TBooleanField;
    procedure fdmtblPolizzaNewRecord(DataSet: TDataSet);
    procedure fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
    procedure fdmtblPolizzaGruppoSpecializzazioneChange(Sender: TField);
    procedure fdmtblPolizzaEst13Change(Sender: TField);
    procedure fdmtblPolizzaIndicizzataChange(Sender: TField);
    procedure fdmtblPolizzaDati4Change(Sender: TField);
    procedure fdmtblPolizzaNDipAmminChange(Sender: TField);
    procedure fdmtblPolizzaAppNumeroChange(Sender: TField);
    procedure fdmtblPolizzaNPercAccessoriChange(Sender: TField);
  private
    { Private declarations }
    imp_base: Currency;
    imp_dip_non, imp_estensione: Currency;
  protected

    procedure MemorizzaTempAssicurato(CodPolizza: Integer); override;
    procedure preparaReport; override;
    function per_allegati: string; override;
  public
    { Public declarations }
    codice_professione: Integer;
    procedure cal_premio; override;
    procedure DoControlli(BaseOnly: boolean = false); override;

  end;

function DMDatiTempPolizzaPM: TDMDatiTempPolizzaPM;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, UCodiciErroriPolizza, system.StrUtils, libSLP, ServerModule, libreria,
  UDatiPolizzaPM, UPolizzaExceptions;

function DMDatiTempPolizzaPM: TDMDatiTempPolizzaPM;
begin
  Result := TDMDatiTempPolizzaPM(UniMainModule.GetModuleInstance(TDMDatiTempPolizzaPM));
end;


function TDMDatiTempPolizzaPM.per_allegati: string;
begin
  inherited;
  Result := Result + 'A1'; // crocetta per attivit� lavorativa
end;


procedure TDMDatiTempPolizzaPM.cal_premio;
begin
  nettoAnnuoTotNONScontato := 0;
  imp_estensione           := 0;
  imp_dip_non              := 0;

  // siglaMassimale := Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 3);
  if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, siglaMassimale) then
  begin
    nettoAnnuoTotNONScontato := UniMainModule.DMDatiBasePolizza.Qgar_base.fieldbyname('PREMIO').asFloat;
    imp_base                 := nettoAnnuoTotNONScontato;
  end;

  // calcola il premio per i dipendenti non medici
  if fdmtblPolizzaNDipAmmin.AsInteger > 0 then
  begin
    if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, 'DN   ') then
    begin
      imp_dip_non := nettoAnnuoTotNONScontato / 100 * UniMainModule.DMDatiBasePolizza.Qgar_base.fieldbyname
        ('PERCENTUALE').asCurrency * fdmtblPolizzaNDipAmmin.AsInteger;
      nettoAnnuoTotNONScontato := nettoAnnuoTotNONScontato + imp_dip_non;
    end
    else
      fdmtblPolizzaNDipAmmin.AsInteger := 0;
  end;

  if fdmtblPolizzaEst13.AsBoolean then
  begin
    // siglaMassimale := 'E13';
    if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, 'E13') then
    begin
      imp_estensione := nettoAnnuoTotNONScontato / 100 * UniMainModule.DMDatiBasePolizza.Qgar_base.fieldbyname
        ('percentuale').asCurrency;
      nettoAnnuoTotNONScontato := nettoAnnuoTotNONScontato + imp_estensione;
    end
    else
      fdmtblPolizzaEst13.AsBoolean := false;
  end;
  fdmtblPolizza.fieldbyname('PerAllegati').AsString := per_allegati;
end;

procedure TDMDatiTempPolizzaPM.DoControlli(BaseOnly: boolean);
begin
  inherited DoControlli(BaseOnly);
  if BaseOnly then
    Exit;

  if fdmtblPolizzaDati5.AsString = '' then
    raise EPolizzaError.Create(CERR_SEDE_ATT_CONTRAENTE, MSG_SEDE_ATT_CONTRAENTE);

end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaAppNumeroChange(Sender: TField);
begin
   // MB 25/10/2020 usata di comodo per non perdere l'allinemaneto nella selezione della specializzazione
   if TDMDatiPolizzaPM(UniMainModule.DMDatiBasePolizza).QProfessioniParamedico.Active then
      fdmtblPolizzaGruppoSpecializzazione.AsString :=  TDMDatiPolizzaPM(UniMainModule.DMDatiBasePolizza)
       .QProfessioniParamedico.fieldbyname('gruppo').AsString;
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaDati4Change(Sender: TField);
begin
  if (fdmtblPolizzaDati4.AsString = 'D') or (fdmtblPolizzaDati4.AsString = 'P') then
    fdmtblPolizzaNDipAmmin.AsInteger := 0;
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaEst13Change(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaGruppoSpecializzazioneChange(Sender: TField);
begin

  fdmtblPolizzaGruppoSpecializzazione.OnChange := nil;
  try
    calcolaPremio;
  finally
    fdmtblPolizzaGruppoSpecializzazione.OnChange := fdmtblPolizzaSiglaMassimaleChange;
  end;
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaIndicizzataChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaNDipAmminChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaNewRecord(DataSet: TDataSet);
begin
  inherited;
  fdmtblPolizzaGruppoSpecializzazione.AsString := '';
  fdmtblPolizzaNDipAmmin.AsInteger             := 0;
  fdmtblPolizzaIndicizzata.AsBoolean           := false;
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaNPercAccessoriChange(
  Sender: TField);
begin
  inherited;
  //
end;

procedure TDMDatiTempPolizzaPM.fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
begin
  fdmtblPolizzaSiglaMassimale.OnChange := nil;
  try
    calcolaPremio;
  finally
    fdmtblPolizzaSiglaMassimale.OnChange := fdmtblPolizzaSiglaMassimaleChange;
  end;

end;

procedure TDMDatiTempPolizzaPM.MemorizzaTempAssicurato(CodPolizza: Integer);
begin
  // does nothing
end;

procedure TDMDatiTempPolizzaPM.preparaReport;
begin
  // imposta i campi del frxPolizzaGen
  stringaTest    := ' ';
  StringaChecked := 'X';
  AzzeraCampiReport;

  SLPDatiAge                          := UniMainModule.DMdatiAge.SLPdati_age;
  frxPolizzaGen.EngineOptions.TempDir := SLPDatiAge.dati_gen.dir_temp;

  frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');

  if isPolizza then
  begin
    frxPolizzaGen.Variables['polizza']   := QuotedStr(UniMainModule.NumPolizza);
    frxPolizzaGen.Variables['barcode1']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 1));
    frxPolizzaGen.Variables['barcode2']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 2));
    frxPolizzaGen.Variables['facsimile'] := QuotedStr('N');
  end
  else
    if isPreventivo then
    begin
      frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
      frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
    end;

  frxPolizzaGen.Variables['versione']     := QuotedStr(leggiVersione);
  frxPolizzaGen.Variables['peragenzia']   := QuotedStr('N');
  frxPolizzaGen.Variables['in_garanzia']  := QuotedStr('');
  frxPolizzaGen.Variables['in_garanzia1'] := QuotedStr('');

  frxPolizzaGen.Variables['ramo']        := QuotedStr(UniServerModule.ramoSLP);
  frxPolizzaGen.Variables['agenzia']     := QuotedStr(SLPDatiAge.agenzia);
  frxPolizzaGen.Variables['citta_age']   := QuotedStr(SLPDatiAge.nome);
  frxPolizzaGen.Variables['descrizione'] := QuotedStr(SLPDatiAge.Denominazione);
  frxPolizzaGen.Variables['autostorica'] := QuotedStr(stringaTest);
  frxPolizzaGen.Variables['camper']      := QuotedStr('N');

  frxPolizzaGen.Variables['suba']  := QuotedStr(fdmtblPolizzaSubAgenzia.AsString);
  if frxPolizzaGen.Variables.IndexOf('S_SUBA')>=0 then
     frxPolizzaGen.Variables['s_suba']     := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);
  if frxPolizzaGen.Variables.IndexOf('subP')>=0 then
     frxPolizzaGen.Variables['subP'] := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);


  frxPolizzaGen.Variables['Psost'] := QuotedStr(fdmtblPolizzaNumPolizzaSostituita.AsString);
  // QuotedStr(Epol_sost.text);
  frxPolizzaGen.Variables['scade_il'] := QuotedStr(fdmtblPolizzaScadenzaPolizzaSostituita.AsString);
  // QuotedStr(Escad_pol_sost.text);
  frxPolizzaGen.Variables['effetto']  := QuotedStr(fdmtblPolizzaDecorrenza.AsString); // QuotedStr(Eeffetto.text);
  frxPolizzaGen.Variables['scadenza'] := QuotedStr(fdmtblPolizzaScadenza.AsString); // QuotedStr(Escadenza.text);
  frxPolizzaGen.Variables['durata']   := QuotedStr('-' + fdmtblPolizzaDurataAnni.AsString + '-');

  frxPolizzaGen.Variables['giorni'] := QuotedStr(ifThen(fdmtblPolizzaDurataGiorni.AsString = '0', stringaTest,
    fdmtblPolizzaDurataGiorni.AsString));

  frxPolizzaGen.Variables['fraz'] := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  // QuotedStr(dai_frazionamentoE(CBfrazionamento.text));
  frxPolizzaGen.Variables['scad1rata'] := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  // QuotedStr(Escad_prima_rata.text);
  frxPolizzaGen.Variables['emissione'] := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  // QuotedStr(EdataEmissione.text);
  frxPolizzaGen.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString); // QuotedStr(Econtraente.text);
  frxPolizzaGen.Variables['nato_a']     := QuotedStr(fdmtblPolizzaLocNascita.AsString); // QuotedStr(Enato_a.text);
  frxPolizzaGen.Variables['nato_il']    := QuotedStr(ifThen(YearOf(fdmtblPolizzaDataNascita.AsDateTime) > 1900,
    fdmtblPolizzaDataNascita.AsString, ''));
  frxPolizzaGen.Variables['residente_in'] :=
    QuotedStr(fdmtblPolizzaIndirizzo.AsString + ' - ' + fdmtblPolizzaCitta.AsString + ' - ' +
    fdmtblPolizzaProvincia.AsString);
  // QuotedStr(Eindirizzo.text + ' - ' + Ecitta.text + ' (' + Eprovincia.text + ')');
  frxPolizzaGen.Variables['cap']         := QuotedStr(fdmtblPolizzaCap.AsString); // QuotedStr(Ecap.text);
  frxPolizzaGen.Variables['professione'] := QuotedStr(fdmtblPolizzaProfessioneDescriz.AsString);
  // QuotedStr(Eprofessione.text);
  // frxPolizzaGen.Variables['telefono'] := QuotedStr('Numero telefonico');
  { TODO -oMB : inserire il campo per telefono e email nella form di inserimento }
  // QuotedStr(alltrim(alltrim(Ee_mail.text) + ' ' + alltrim(Etelefono.text)));
  frxPolizzaGen.Variables['cf_iva'] := QuotedStr(ifThen(fdmtblPolizzaPartitaIva.AsString > '',
    fdmtblPolizzaPartitaIva.AsString, fdmtblPolizzaCodiceFiscale.AsString ));

  frxPolizzaGen.Variables['n_dip_non'] := QuotedStr(fdmtblPolizzaNDipAmmin.AsString );

  frxPolizzaGen.Variables['sedeattivita'] := QuotedStr(fdmtblPolizzaDati5.AsString);

  // combinazioni di polizza
  frxPolizzaGen.Variables['P1'] := QuotedStr(stringaTest);
  frxPolizzaGen.Variables['P2'] := QuotedStr(stringaTest);

  frxPolizzaGen.Variables['professione'] := QuotedStr(TDMDatiPolizzaPM(UniMainModule.DMDatiBasePolizza)
    .QProfessioniParamedico.fieldbyname('TITOLO').AsString);
  frxPolizzaGen.Variables['desc_garanzia'] :=
    QuotedStr('Garanzie valide per:' + TDMDatiPolizzaPM(UniMainModule.DMDatiBasePolizza)
    .QProfessioniParamedico.fieldbyname('DESCRIZ').AsString);
  frxPolizzaGen.Variables['telefono'] :=
    QuotedStr(alltrim(alltrim(fdmtblPolizzaEMail.AsString) + ' ' + alltrim(fdmtblPolizzaTelefono.AsString)));

  frxPolizzaGen.Variables['dip'] := QuotedStr(ifThen(Trim(fdmtblPolizzaDati4.AsString) = 'D', StringaChecked,
    stringaTest));
  frxPolizzaGen.Variables['lp'] := QuotedStr(ifThen(Trim(fdmtblPolizzaDati4.AsString) = 'L', StringaChecked,
    stringaTest));

  frxPolizzaGen.Variables['GRA'] := QuotedStr(stringaTest);
  frxPolizzaGen.Variables['GRB'] := QuotedStr(stringaTest);
  frxPolizzaGen.Variables['GRC'] := QuotedStr(stringaTest);
  frxPolizzaGen.Variables['GRD'] := QuotedStr(stringaTest);
         // dsprofesisoni.gruppo
  if fdmtblPolizzaGruppoSpecializzazione.AsString = 'A' then
    frxPolizzaGen.Variables['GRA'] := QuotedStr(StringaChecked)
  else if fdmtblPolizzaGruppoSpecializzazione.AsString = 'B' then
    frxPolizzaGen.Variables['GRB'] := QuotedStr(StringaChecked)
  else if fdmtblPolizzaGruppoSpecializzazione.AsString = 'C' then
    frxPolizzaGen.Variables['GRC'] := QuotedStr(StringaChecked)
  else if fdmtblPolizzaGruppoSpecializzazione.AsString = 'D' then
    frxPolizzaGen.Variables['GRD'] := QuotedStr(StringaChecked);


  frxPolizzaGen.Variables['ADEGSI'] := ifThen(fdmtblPolizzaIndicizzata.AsBoolean, QuotedStr('SI'), QuotedStr('NO'));

  getImportoGaranzia(siglaMassimale);
  frxPolizzaGen.Variables['massimale'] :=
    QuotedStr('=' + FormatCurr('##,###,##0', fdmtblGaranziaMassimale.AsCurrency) + '=');

  // estensione
  frxPolizzaGen.Variables['X4'] := QuotedStr(ifThen(fdmtblPolizzaEst13.AsBoolean, StringaChecked, stringaTest));

  frxPolizzaGen.Variables['pre_dip_non'] := QuotedStr(zero(imp_dip_non));
  frxPolizzaGen.Variables['pre_est']     := QuotedStr(zero(imp_estensione));

  frxPolizzaGen.Variables['nmedici'] := QuotedStr(stringaTest);
  if fdmtblPolizzaNDipAmmin.AsInteger > 1 then
    frxPolizzaGen.Variables['nmedici'] := QuotedStr('N� Paramed.: ' + intToStr(fdmtblPolizzaNDipAmmin.AsInteger));

  frxPolizzaGen.Variables['nettoAnnuoBase'] :=
    QuotedStr(zero(getImportoGaranzia(Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 3)) -
    fdmtblPolizzaArrotondamento.AsCurrency));

  frxPolizzaGen.Variables['nettoAnnuo'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));

  if fdmtblPolizzaSconto.AsCurrency<>0 then
  begin
    frxPolizzaGen.Variables['ssconto'] := QuotedStr('-' + alltrim(FloatToStr(fdmtblPolizzaPSconto.AsCurrency)) + '%');
    frxPolizzaGen.Variables['sc1'] := QuotedStr(zero(fdmtblPolizzaSconto.AsCurrency));  //QuotedStr(zero(Opremio.scontoDurataBase));
    frxPolizzaGen.Variables['nettoscontato'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
  end
  else
  begin
    frxPolizzaGen.Variables['sc1']     := QuotedStr('=======');
    frxPolizzaGen.Variables['nettoscontato'] := QuotedStr('=======');
  end;

  // parte finale con i premi
  frxPolizzaGen.Variables['rate_succ']   := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['netto1']      := QuotedStr(zero(fdmtblPolizzanetto1.asCurrency));
  frxPolizzaGen.Variables['acc1']        := QuotedStr(zero(fdmtblPolizzaAccessori1.asCurrency));
  frxPolizzaGen.Variables['int_fraz']    := QuotedStr(zero(fdmtblPolizzaInteressiFrazionamento1.asCurrency));
  frxPolizzaGen.Variables['imponibile1'] := QuotedStr(zero(fdmtblPolizzaImponibile1.asCurrency));
  frxPolizzaGen.Variables['tasse1']      := QuotedStr(zero(fdmtblPolizzaImposte1.asCurrency));
  frxPolizzaGen.Variables['totale1']     := QuotedStr(zero(fdmtblPolizzaTotale1.asCurrency));
  frxPolizzaGen.Variables['alla_firma']  := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['netto2']      := QuotedStr(zero(fdmtblPolizzanetto2.asCurrency));
  frxPolizzaGen.Variables['acc2']        := QuotedStr(zero(fdmtblPolizzaaccessori2.asCurrency));
  frxPolizzaGen.Variables['rimborso']    := QuotedStr(zero(fdmtblPolizzaRimborsoSost.asCurrency));
  frxPolizzaGen.Variables['imponibile2'] := QuotedStr(zero(fdmtblPolizzaimponibile2.asCurrency));
  frxPolizzaGen.Variables['tasse2']      := QuotedStr(zero(fdmtblPolizzaimposte2.asCurrency));
  frxPolizzaGen.Variables['totale2']     := QuotedStr(zero(fdmtblPolizzatotale2.asCurrency));
  frxPolizzaGen.Variables['emissione']   := QuotedStr(fdmtblPolizzaDataEmissione.AsString);

end;



initialization

RegisterClass(TDMDatiTempPolizzaPM);

end.
