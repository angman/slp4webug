unit UdmdatiStoricoIncassi;

interface

uses
  System.SysUtils, System.Classes, System.Rtti, Data.DB, dbisamtb, IWApplication, System.Math,
  UDMMaster, UQueryEditIntf, UQueryElencoIntf ;

type
  TDMDatiStoricoIncassi = class(TDMMaster, IQueryElenco, IQueryEdit)
    QStoricoIncassi: TDBISAMQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    //procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
    //  StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0; codCompagnia: Integer=1); override;
    procedure EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
      DirezioneOrdinamento: integer = 1; TipoFiltro: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 0); override;
  end;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, System.DateUtils, System.StrUtils;

{ TDMDatiAgeStoricoIncassi }

procedure TDMDatiStoricoIncassi.EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
  DirezioneOrdinamento, TipoFiltro, CodCollaboratore, codCompagnia: integer);
var
  strWhere: string;
  i: integer;

  function getWhere: string;
  begin
    Result   := IfThen(strWhere <> '', strWhere, ' AND ');
    strWhere := '';
  end;

begin
  strWhere := 'WHERE ';
  QStoricoIncassi.Close;
  QStoricoIncassi.SQL.Clear;
  QStoricoIncassi.SQL.Add('SELECT N_POLIZZA AS N_POLIZZA, SCA.DENOMINAZ, SCA.DECORRENZA DECORRENZA, scadenza, n_foglio_cassa,');
  QStoricoIncassi.SQL.Add('ROUND(SCA.LORDO,2) LORDO,');

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    if UniMainModule.UtenteSubPromoter > 0 then
      QStoricoIncassi.SQL.Add('ROUND(SCA.PROVVIGIONI / 100*' + floatToStr(UniMainModule.DMdatiAge.get_provv_subCollab) +
        ',2) AS PROVVIGIONI,')
    else
      QStoricoIncassi.SQL.Add('ROUND(SCA.PROVVIGIONI / 100*' + floatToStr(UniMainModule.DMdatiAge.get_provv_collab) +
        ',2) AS PROVVIGIONI,');
  end
  else
    QStoricoIncassi.SQL.Add('ROUND(SCA.PROVVIGIONI, 2) PROVVIGIONI,');

  QStoricoIncassi.SQL.Add
    ('SCA.DATA_PAG "DATA PAG.", SCA.FRAZIONAM FRAZ, SCA.COD_SCADENZA, SCA.DATA_PAG, SCA.RIF_COD_COMPAGNIA,');
  QStoricoIncassi.SQL.Add('POL.FATTA_DA, COD_FC_SUB_AGE,');
  QStoricoIncassi.SQL.Add('SCA.TIPO_TITOLO, SCA.NETTO, SCA.TASSE, RIF_COD_POLIZZA,');
  QStoricoIncassi.SQL.Add('COMP.CODICE_AGENZIA,');
  QStoricoIncassi.SQL.Add('POL.RIF_TIPO_POL AS RIF_TIPO_POLIZZA, TPOL.DESCRIZIONE AS TIPO_POLIZZA, PROVVIGIONI_SUB, DESCRIZ, ');
  QStoricoIncassi.SQL.Add('PRO.SIGLA AS SIGLASUBAGE, SPRO.SIGLA AS SIGLASUBCOLL, ');
  QStoricoIncassi.SQL.Add('din.data_operazione, din.importo, tpag.descrizione as descriz_pag, din.descrizione, din.banca, din.emesso_da, ');
  QStoricoIncassi.SQL.Add('din.descrizione_chiusura_sospeso, din.estremi1, din.estremi2, din.estremi3,');
  QStoricoIncassi.SQL.Add('if(extract(year,SCA.DATA_PAG_CCP)>2000 then sca.data_pag_ccp else null) as data_pag_ccp ');

  QStoricoIncassi.SQL.Add('FROM s_SCADENZE SCA LEFT JOIN POLIZZE POL ON COD_POLIZZA = RIF_COD_POLIZZA');
  QStoricoIncassi.SQL.Add('LEFT OUTER JOIN COMPAGNI COMP ON COMP.COD_COMPAGNIA = SCA.RIF_COD_COMPAGNIA');
  QStoricoIncassi.SQL.Add('LEFT JOIN CLIENTI CLI ON CLI.COD_CLIENTE = POL.COD_CLI');
  QStoricoIncassi.SQL.Add('LEFT JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL');
  QStoricoIncassi.SQL.Add('LEFT JOIN PRODUTT PRO  ON PRO.COD_PRODUTTORE = CLI.PROMOTER');
  QStoricoIncassi.SQL.Add('LEFT JOIN PRODUTT SPRO ON SPRO.COD_PRODUTTORE = CLI.SUB_PROMOTER');
  QStoricoIncassi.SQL.Add('left join dati_incasso din on din.rif_scadenza=sca.cod_scad_storica ');
  QStoricoIncassi.SQL.Add('left join tipo_pagamenti tpag on tpag.codice_pag=din.mezzo_pagamento ');
  if codCompagnia > 0 then
    QStoricoIncassi.SQL.Add(getWhere + ' SCA.RIF_COD_COMPAGNIA = ' + inttostr(codCompagnia));

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    // filtra solo le sue quietanze
    QStoricoIncassi.SQL.Add(getWhere + ' CLI.PROMOTER = ' + UniMainModule.utentePromoter.ToString);

    if UniMainModule.UtenteSubPromoter > 0 then
      QStoricoIncassi.SQL.Add('AND CLI.SUB_PROMOTER = ' + UniMainModule.UtenteSubPromoter.ToString);
    // vecchio filtro, sostituito il 18/06/2020
    // QStoricoIncassi.SQL.Add(strWhere + 'pol.fatta_da =' + inttostr(UniMainModule.utentePromoter));

    // metti il selettore per escludere i titoli gi� resocontati all'agenzia !!!!
    QStoricoIncassi.SQL.Add('AND ((COD_FC_SUB_AGE IS NULL) OR (COD_FC_SUB_AGE = 0)) ');
  end;

  if AParametersList <> nil then
    for i := 0 to AParametersList.Count - 1 do
    begin
        QStoricoIncassi.SQL.Add(getWhere + AParametersList.Names[i] + ' = ' +
          QuotedStr(AParametersList.Values[AParametersList.Names[i]]))
    end;

  QStoricoIncassi.SQL.Add('UNION ');  // ===========================================================================


  strWhere := 'WHERE ';
  QStoricoIncassi.SQL.Add('SELECT N_POLIZZA AS N_POLIZZA, SCA.DENOMINAZ, SCA.DECORRENZA DECORRENZA, scadenza, ''DA CHIUDERE'' as n_foglio_cassa,');
  QStoricoIncassi.SQL.Add('ROUND(SCA.LORDO,2) LORDO,');

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    if UniMainModule.UtenteSubPromoter > 0 then
      QStoricoIncassi.SQL.Add('ROUND(SCA.PROVVIGIONI / 100*' + floatToStr(UniMainModule.DMdatiAge.get_provv_subCollab) +
        ',2) AS PROVVIGIONI,')
    else
      QStoricoIncassi.SQL.Add('ROUND(SCA.PROVVIGIONI / 100*' + floatToStr(UniMainModule.DMdatiAge.get_provv_collab) +
        ',2) AS PROVVIGIONI,');
  end
  else
    QStoricoIncassi.SQL.Add('ROUND(SCA.PROVVIGIONI, 2) PROVVIGIONI,');

  QStoricoIncassi.SQL.Add
    ('SCA.DATA_PAG "DATA PAG.", SCA.FRAZIONAM FRAZ, SCA.COD_SCADENZA, SCA.DATA_PAG, SCA.RIF_COD_COMPAGNIA,');
  QStoricoIncassi.SQL.Add('POL.FATTA_DA, COD_FC_SUB_AGE,');
  QStoricoIncassi.SQL.Add('SCA.TIPO_TITOLO, SCA.NETTO, SCA.TASSE, RIF_COD_POLIZZA,');
  QStoricoIncassi.SQL.Add('COMP.CODICE_AGENZIA,');
  QStoricoIncassi.SQL.Add('POL.RIF_TIPO_POL AS RIF_TIPO_POLIZZA, TPOL.DESCRIZIONE AS TIPO_POLIZZA, PROVVIGIONI_SUB, DESCRIZ, ');
  QStoricoIncassi.SQL.Add('PRO.SIGLA AS SIGLASUBAGE, SPRO.SIGLA AS SIGLASUBCOLL, ');
  QStoricoIncassi.SQL.Add('din.data_operazione, din.importo, tpag.descrizione as descriz_pag, din.descrizione, din.banca, din.emesso_da, ');
  QStoricoIncassi.SQL.Add('din.descrizione_chiusura_sospeso, din.estremi1, din.estremi2, din.estremi3,');
  QStoricoIncassi.SQL.Add('if(extract(year,SCA.DATA_PAG_CCP)>2000 then sca.data_pag_ccp else null) as data_pag_ccp ');

  QStoricoIncassi.SQL.Add('FROM SCADENZE SCA LEFT JOIN POLIZZE POL ON COD_POLIZZA = RIF_COD_POLIZZA');
  QStoricoIncassi.SQL.Add('LEFT OUTER JOIN COMPAGNI COMP ON COMP.COD_COMPAGNIA = SCA.RIF_COD_COMPAGNIA');
  QStoricoIncassi.SQL.Add('LEFT JOIN CLIENTI CLI ON CLI.COD_CLIENTE = POL.COD_CLI');
  QStoricoIncassi.SQL.Add('LEFT JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL');
  QStoricoIncassi.SQL.Add('LEFT JOIN PRODUTT PRO  ON PRO.COD_PRODUTTORE = CLI.PROMOTER');
  QStoricoIncassi.SQL.Add('LEFT JOIN PRODUTT SPRO ON SPRO.COD_PRODUTTORE = CLI.SUB_PROMOTER');
  QStoricoIncassi.SQL.Add('left join dati_incasso din on din.rif_scadenza=sca.cod_scadenza ');
  QStoricoIncassi.SQL.Add('left join tipo_pagamenti tpag on tpag.codice_pag=din.mezzo_pagamento ');
  if codCompagnia > 0 then
    QStoricoIncassi.SQL.Add(getWhere + ' SCA.RIF_COD_COMPAGNIA = ' + inttostr(codCompagnia));

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    // filtra solo le sue quietanze
    QStoricoIncassi.SQL.Add(getWhere + ' CLI.PROMOTER = ' + UniMainModule.utentePromoter.ToString);

    if UniMainModule.UtenteSubPromoter > 0 then
      QStoricoIncassi.SQL.Add('AND CLI.SUB_PROMOTER = ' + UniMainModule.UtenteSubPromoter.ToString);
    // vecchio filtro, sostituito il 18/06/2020
    // QStoricoIncassi.SQL.Add(strWhere + 'pol.fatta_da =' + inttostr(UniMainModule.utentePromoter));

    // metti il selettore per escludere i titoli gi� resocontati all'agenzia !!!!
    QStoricoIncassi.SQL.Add('AND ((COD_FC_SUB_AGE IS NULL) OR (COD_FC_SUB_AGE = 0)) ');
  end;

  if AParametersList <> nil then
    for i := 0 to AParametersList.Count - 1 do
    begin
        QStoricoIncassi.SQL.Add(getWhere + AParametersList.Names[i] + ' = ' +
          QuotedStr(AParametersList.Values[AParametersList.Names[i]]))
    end;

    QStoricoIncassi.SQL.Add(getWhere + 'sca.stato=''P'' ');


  QStoricoIncassi.SQL.Add('ORDER BY ' + ordFieldName + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  QStoricoIncassi.Open;

end;


end.
