unit UDMDatiMandante;

interface

uses
  SysUtils, Classes, UDMMaster, Data.DB, dbisamtb, System.Rtti;

type
  TDMMandanti = class(TDMMaster)
    QCompagnie: TDBISAMQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    // Interfaccia IQueryElenco
    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0; codCompagnia: Integer=1); override;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); override;

  end;

function DMMandanti: TDMMandanti;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule;

function DMMandanti: TDMMandanti;
begin
  Result := TDMMandanti(UniMainModule.GetModuleInstance(TDMMandanti));
end;

{ TDMMandanti }

procedure TDMMandanti.EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0; codCompagnia: Integer=1);
begin
  QCompagnie.Open;
end;

procedure TDMMandanti.PosizionaQuery(AValue: TValue);
begin
  // -------------------
end;

end.
