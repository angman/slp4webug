unit UDMDatiAgeAvvisi;

interface

uses
  SysUtils, Classes, UDMMaster, Data.DB, dbisamtb, UdmdatiAge;

type
  TDMDatiAgeAvvisi = class(TDMMaster)
    QAvvisoAgenziaStopLogin: TDBISAMQuery;
    QUpdDataLastLettura: TDBISAMQuery;
    QUpdDataFirstLettura: TDBISAMQuery;
    QAvvvisiCriticiAgenzia: TDBISAMQuery;
    QAvvisiAgenzia: TDBISAMQuery;
    QFindAvvisoByCod: TDBISAMQuery;
    DBaccessoAvvisi: TDBISAMDatabase;
    SessionAvvisi: TDBISAMSession;
    QUpdN_letture: TDBISAMQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    SLPDatiAge: TSLPdati_age_rec;
    function openMessages(AMessageQuery: TDBISAMQuery): Boolean;
    function getCodUtente: Integer;
  public
    { Public declarations }
    function hasStopMessage: Boolean;
    function hasCriticalMessages(var AQyMessage: TDBISAMQuery): Boolean;
    function hasMessages: Boolean;
    function FindAvviso(ACodAvviso: Integer; var AQyMessage: TDBISAMQuery): Boolean;

    procedure UpdDateMessaggio(ACodMsg: Integer);
  end;

function DMDatiAgeAvvisi: TDMDatiAgeAvvisi;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, ServerModule, System.IOUtils;

function DMDatiAgeAvvisi: TDMDatiAgeAvvisi;
begin
  Result := TDMDatiAgeAvvisi(UniMainModule.GetModuleInstance(TDMDatiAgeAvvisi));
end;

{ TDMDatiAgeAvvisi }

procedure TDMDatiAgeAvvisi.DataModuleCreate(Sender: TObject);
begin
  // inherited;
  SLPDatiAge := UniMainModule.DMdatiAge.SLPdati_age;
  {
    SessionAvvisi.active     := False;
    SessionAvvisi.PrivateDir := TPath.Combine(UniServerModule.PosizioneDati, 'temp');

    DBaccessoAvvisi.Directory := UniServerModule.PosizioneDati;
    SessionAvvisi.active      := True;
  }

{$IFNDEF  SVILUPPO}
  // caso di accesso a server remoto
  Engine.ServerName              := 'DBSRVR';
  DBaccessoAvvisi.RemoteDatabase := 'DBscambio';

  SessionAvvisi.active                   := False;
  SessionAvvisi.RemoteEncryption         := true;
  SessionAvvisi.RemoteEncryptionPassword := UniServerModule.EncriptionPassword;
  SessionAvvisi.RemotePort               := UniServerModule.RemotePort;
  SessionAvvisi.SessionType              := stRemote;
  SessionAvvisi.RemoteAddress            := UniServerModule.RemoteAddress;
  SessionAvvisi.RemoteUser               := UniServerModule.RemoteUser;
  SessionAvvisi.RemotePassword           := UniServerModule.RemotePassword;

{$ELSE}
  SessionAvvisi.active      := False;
  SessionAvvisi.PrivateDir  := TPath.Combine(UniServerModule.PosizioneDati, 'temp');
  DBaccessoAvvisi.Directory := UniServerModule.PosizioneDati;
  SessionAvvisi.active      := true;

{$ENDIF}
  DBaccessoAvvisi.Open;

end;

function TDMDatiAgeAvvisi.FindAvviso(ACodAvviso: Integer; var AQyMessage: TDBISAMQuery): Boolean;
begin
  QFindAvvisoByCod.Close;
  QFindAvvisoByCod.ParamByName('CODMSG').AsInteger := ACodAvviso;
  QFindAvvisoByCod.Open;
  AQyMessage := QFindAvvisoByCod;
  Result     := not QFindAvvisoByCod.IsEmpty;
end;

function TDMDatiAgeAvvisi.getCodUtente: Integer;
begin
  if UniMainModule.IsUtentePromoter then Result := UniMainModule.utenteCodice
  else Result:=0;
end;

function TDMDatiAgeAvvisi.hasCriticalMessages(var AQyMessage: TDBISAMQuery): Boolean;
begin
  // if UniMainModule.IsUtentePromoter then
  //  QAvvvisiCriticiAgenzia.SQL.Append('and operatore_destinatario = :CodOperatore');

  QAvvvisiCriticiAgenzia.SQL.Append('order by livello_msg');

  AQyMessage := QAvvvisiCriticiAgenzia;
  Result     := openMessages(QAvvvisiCriticiAgenzia);
end;

function TDMDatiAgeAvvisi.hasMessages: Boolean;
begin
  // if UniMainModule.IsUtentePromoter then
  //  QAvvisiAgenzia.SQL.Append('and operatore_destinatario = :CodOperatore');
  if QAvvisiAgenzia.SQL.IndexOf('order by livello_msg') = -1 then
    QAvvisiAgenzia.SQL.Append('order by livello_msg');

  Result := openMessages(QAvvisiAgenzia);
end;

function TDMDatiAgeAvvisi.hasStopMessage: Boolean;
begin

  // if UniMainModule.IsUtentePromoter then
  //   QAvvisoAgenziaStopLogin.SQL.Append('and operatore_destinatario = :CodOperatore');
  Result := openMessages(QAvvisoAgenziaStopLogin);
end;

function TDMDatiAgeAvvisi.openMessages(AMessageQuery: TDBISAMQuery): Boolean;
begin

  AMessageQuery.Close;
  AMessageQuery.ParamByName('CODAGENZIA').AsString := SLPDatiAge.cod_int.Trim;
  if AMessageQuery.Params.FindParam('codUtente') <> nil then
     AMessageQuery.ParamByName('CodUtente').AsInteger := getCodUtente;

  AMessageQuery.Open;
  Result := not AMessageQuery.IsEmpty;

end;

procedure TDMDatiAgeAvvisi.UpdDateMessaggio(ACodMsg: Integer);
begin
  QUpdDataFirstLettura.Close;
  QUpdDataFirstLettura.ParamByName('COD_MSG').AsInteger := ACodMsg;
  QUpdDataFirstLettura.ParamByName('CodAgenzia').AsString := SLPDatiAge.cod_int.Trim;
  QUpdDataFirstLettura.ParamByName('codUtente').AsInteger := getCodUtente;
  QUpdDataFirstLettura.ExecSQL;


  QUpdDataLastLettura.Close;
  QUpdDataLastLettura.ParamByName('COD_MSG').AsInteger := ACodMsg;
  QUpdDataLastLettura.ParamByName('CodAgenzia').AsString := SLPDatiAge.cod_int.Trim;
  QUpdDataLastLettura.ParamByName('codUtente').AsInteger := getCodUtente;
  QUpdDataLastLettura.ExecSQL;
{$MESSAGE WARN 'Continua ad incrementare le letture anche se si � superato il numero di ricorrenze'}
  QUpdN_letture.Close;
  QUpdN_letture.ParamByName('COD_MSG').AsInteger := ACodMsg;
  QUpdN_letture.ParamByName('CodAgenzia').AsString := SLPDatiAge.cod_int.Trim;
  QUpdN_letture.ParamByName('codUtente').AsInteger := getCodUtente;
  QUpdN_letture.ExecSQL;

end;

initialization

// RegisterModuleClass(TDMDatiAgeAvvisi);

end.
