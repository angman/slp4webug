unit UDMMaster;

interface

uses
  System.SysUtils, System.Classes, System.Rtti, Data.DB, dbisamtb, UQueryElencoIntf, UQueryEditIntf,
  UPosizionaQueryElencoIntf;

type
  TDMMaster = class(TDataModule, IQueryElenco, IQueryEdit, IPosizionaQueryElenco)
    QGetLastID: TDBISAMQuery;
    tblTemp: TDBISAMTable;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FOrdList: TStringList;
    FStrOrdCol: string;

  protected

    procedure chiudi_tutti;

    function getLastID(ATableName: string): Largeint;

    property OrdList: TStringList read FOrdList;
    property StrOrdCol: string read FStrOrdCol write FStrOrdCol;

  public
    { Public declarations }
    // Interfaccia IQueryElenco

    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 1); overload; virtual;

    procedure EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
      DirezioneOrdinamento: integer = 1; TipoFiltro: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 0); overload; virtual;

    procedure CambiaOrdinamento(ATipoOrdinamento: integer; ATipoFiltro: integer = 0); virtual;
    procedure TogliFiltro; virtual;
    procedure CercaDato(ATipoOrdinamento: integer; ATipoFiltro: integer; AValueToSearch: string); virtual;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); virtual;

    // Interfaccia IPosizionaQueryElenco
    function PosizionaQueryElenco(AQueryElenco, AQueryEdit: TDataset): boolean; overload;

    procedure PosizionaElenco(AQueryElenco: TDataset; ABookmark: TBookmark = nil); overload;
    procedure PosizionaElenco(AQueryElenco: TDataset; ARecNo: integer = 0); overload;

  end;

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

uses MainModule, IWWebGrid, UPolizzaExceptions, System.RegularExpressions, System.StrUtils, System.Types;

procedure TDMMaster.CambiaOrdinamento(ATipoOrdinamento: integer; ATipoFiltro: integer);
begin
  // EseguiQuery(ATipoOrdinamento);
  raise EPolizzaError.Create('CambiaOrdinamento non implementato');
end;

procedure TDMMaster.CercaDato(ATipoOrdinamento: integer; ATipoFiltro: integer; AValueToSearch: string);
begin
  // EseguiQuery('',ATipoOrdinamento, ATipoFiltro, AValueToSearch);
end;

procedure TDMMaster.chiudi_tutti;
var
  component: TComponent;
  i: integer;
begin
  for i := 0 to ComponentCount - 1 do
  begin
    component := Components[i];
    if component is TDBISAMQuery then
    begin
      (component as TDBISAMQuery).Close;
      if UniMainModule <> nil then
        (component as TDBISAMQuery).SessionName := UniMainModule.DMdatiAge.SessionAgenzia.SessionName;
    end;
  end;
end;

procedure TDMMaster.DataModuleCreate(Sender: TObject);
var
  component: TComponent;
  i: integer;
begin
  FOrdList := TStringList.Create;

  for i := 0 to ComponentCount - 1 do
  begin
    component := Components[i];
    if component is TDBISAMQuery then
    begin
      (component as TDBISAMQuery).Close;
      if UniMainModule <> nil then
        (component as TDBISAMQuery).SessionName := UniMainModule.DMdatiAge.SessionAgenzia.SessionName;
    end;
  end;

  // chiudi_tutti;
end;

procedure TDMMaster.DataModuleDestroy(Sender: TObject);
begin
  // chiudi_tutti;
  OrdList.Free;
end;

procedure TDMMaster.EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
  DirezioneOrdinamento: integer; TipoFiltro: integer; CodCollaboratore: integer; codCompagnia: integer);
begin
  // placeholder
  raise EPolizzaError.Create('EseguiQuery con parametersList non implementato');

end;

function TDMMaster.getLastID(ATableName: string): Largeint;
begin
  Result := 0;
  QGetLastID.Close;
  QGetLastID.SQL.Clear;
  QGetLastID.SQL.Append('SELECT DISTINCT LASTAUTOINC(' + quotedStr(ATableName) + ') AS LAST_ID FROM '+ATableName);
  QGetLastID.Open;
  try
    Result := QGetLastID.FieldByName('LAST_ID').AsLargeInt;
  finally
    QGetLastID.Close;
  end;
end;

procedure TDMMaster.EseguiQuery(ordFieldName: string; TipoFiltro: integer; ValToSearch: string; StatoPagamento: integer;
  DirezioneOrdinamento: integer; CodCollaboratore: integer; codCompagnia: integer);
begin
  // Placeholder
  raise EPolizzaError.Create('EseguiQuery non implementato');

end;

procedure TDMMaster.PosizionaElenco(AQueryElenco: TDataset; ARecNo: integer);
begin
  AQueryElenco.Close;
  AQueryElenco.Open;
  if not AQueryElenco.IsEmpty then
    AQueryElenco.MoveBy(ARecNo - 1);

end;

procedure TDMMaster.PosizionaQuery(AValue: TValue);
begin
  // Placeholder
  raise EPolizzaError.Create('PosizionaQuery non implementato');

end;

function TDMMaster.PosizionaQueryElenco(AQueryElenco, AQueryEdit: TDataset): boolean;
var
  pkColumnName: string;
  lBookRec: TBookmark;
  lastID: Largeint;
  TableName: string;

  function extractPKColumnName: string;
  var
    i, j: integer;
    StrAr: TStringDynArray;
    SqlStr: string;

  begin
    Result := '';
    for i  := 0 to TDBISAMQuery(AQueryEdit).SQL.Count - 1 do
    begin
      SqlStr := TDBISAMQuery(AQueryEdit).SQL[i];
      if TRegex.ismatch(SqlStr, 'from', [roIgnoreCase]) then
      begin
        // StrAr := TRegex.split(SqlStr, '\w+');
        StrAr := SplitString(SqlStr, ' ');
        if Length(StrAr) > 1 then
        begin
          TableName := StrAr[1];
          if TableName = '' then
            TableName := Trim(TDBISAMQuery(AQueryEdit).SQL[i + 1]);
          break;
        end;
      end;
    end;
    tblTemp.TableName := TableName;
    with tblTemp do
    begin
      IndexDefs.Update;
      for j := 0 to IndexDefs.Count - 1 do
        if (ixPrimary in IndexDefs[j].Options) then
        begin
          Result := IndexDefs[j].Fields;
          break;
        end;
    end;

    if Result = '' then
      raise EPolizzaError.Create('Primary key column non trovata');

  end;

  procedure PosizionaElencoByPk;
  begin
    TDBISAMQuery(AQueryEdit).Database.StartTransaction();
    try
      AQueryEdit.post;
      lBookRec     := nil;
      lBookRec     := AQueryElenco.GetBookmark;
      pkColumnName := extractPKColumnName;
      if TableName <> '' then
      begin
        AQueryElenco.Close;
        AQueryElenco.Open;
        lastID := getLastID(TableName);
        if not AQueryElenco.Locate(pkColumnName, lastID, []) then
          PosizionaElenco(AQueryElenco, lBookRec);
      end
      else
        PosizionaElenco(AQueryElenco, AQueryEdit.RecNo);
    finally
      TDBISAMQuery(AQueryEdit).Database.Commit;
    end;
  end;

begin
  Result := True;
  if TDBISAMQuery(AQueryEdit).State = dsInsert then
    PosizionaElencoByPk
  else
  begin
    AQueryEdit.post;
    PosizionaElenco(AQueryElenco, AQueryElenco.RecNo);
  end;

end;

procedure TDMMaster.PosizionaElenco(AQueryElenco: TDataset; ABookmark: TBookmark);
var
  BookRec: TBookmark;

begin
  if ABookmark = nil then

    BookRec := AQueryElenco.GetBookmark
  else
    BookRec := ABookmark;

  try
    AQueryElenco.Close;
    AQueryElenco.Open;
    if AQueryElenco.BookmarkValid(BookRec) then
      AQueryElenco.GotoBookmark(BookRec);
  finally
    AQueryElenco.FreeBookmark(BookRec);
  end;

end;

procedure TDMMaster.TogliFiltro;
begin
  // Placeholder
  raise EPolizzaError.Create('TogliFiltro non implementato');

end;

end.
