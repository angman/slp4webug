inherited DMdatiGenerali: TDMdatiGenerali
  OldCreateOrder = True
  Height = 379
  Width = 555
  object QinsAccessi: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionAgenzia'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert '
      'into allAgeAccessi'
      
        '(data, indirizzoIP, browser, sistemaOP, utenteInterno, operazion' +
        'e, risOperazione, opeMessage, idAgenzia)'
      'VALUES'
      
        '(:data, :indirizzoIP, :browser, :sistemaOP, :utenteInterno, :ope' +
        'razione, :risOperazione, :opeMessage, :idAgenzia)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'indirizzoIP'
      end
      item
        DataType = ftUnknown
        Name = 'browser'
      end
      item
        DataType = ftUnknown
        Name = 'sistemaOP'
      end
      item
        DataType = ftUnknown
        Name = 'utenteInterno'
      end
      item
        DataType = ftUnknown
        Name = 'operazione'
      end
      item
        DataType = ftUnknown
        Name = 'risOperazione'
      end
      item
        DataType = ftUnknown
        Name = 'opeMessage'
      end
      item
        DataType = ftUnknown
        Name = 'idAgenzia'
      end>
    Left = 449
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'indirizzoIP'
      end
      item
        DataType = ftUnknown
        Name = 'browser'
      end
      item
        DataType = ftUnknown
        Name = 'sistemaOP'
      end
      item
        DataType = ftUnknown
        Name = 'utenteInterno'
      end
      item
        DataType = ftUnknown
        Name = 'operazione'
      end
      item
        DataType = ftUnknown
        Name = 'risOperazione'
      end
      item
        DataType = ftUnknown
        Name = 'opeMessage'
      end
      item
        DataType = ftUnknown
        Name = 'idAgenzia'
      end>
  end
  object QelencoAgenzie: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionAgenzia'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from allagenzie')
    Params = <>
    Left = 361
    Top = 24
  end
  object DBaccessoBase: TDBISAMDatabase
    EngineVersion = '4.37 Build 3'
    DatabaseName = 'DBaccessoBase'
    Directory = 'C:\Sviluppo\Sorgenti\Slp4weB\SLPDATI'
    SessionName = 'SessionAgenzia'
    Left = 163
    Top = 32
  end
  object SessioneAccessoBase: TDBISAMSession
    EngineVersion = '4.37 Build 3'
    AutoSessionName = True
    PrivateDir = 'C:\Users\MarcoOK\AppData\Local\Temp\'
    RemoteEncryptionPassword = 'elevatesoft'
    RemoteAddress = '127.0.0.1'
    Left = 48
    Top = 26
  end
  object QLog_Operazioni: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionAgenzia'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert '
      'into log_operazioni'
      
        '(utente, datatime, operazione,  val_preced, rif_operatore, rif_c' +
        'hiave, chiave_sec, idAgenzia)'
      'VALUES'
      
        '(:utente, :datatime, :operazione, :val_preced, :rif_operatore, :' +
        'rif_chiave, :chiave_sec, :idAgenzia)')
    Params = <
      item
        DataType = ftFixedChar
        Name = 'utente'
      end
      item
        DataType = ftDateTime
        Name = 'datatime'
      end
      item
        DataType = ftFixedChar
        Name = 'operazione'
      end
      item
        DataType = ftString
        Name = 'val_preced'
      end
      item
        DataType = ftInteger
        Name = 'rif_operatore'
      end
      item
        DataType = ftInteger
        Name = 'rif_chiave'
      end
      item
        DataType = ftString
        Name = 'chiave_sec'
      end
      item
        DataType = ftString
        Name = 'idAgenzia'
      end>
    Left = 377
    Top = 168
    ParamData = <
      item
        DataType = ftFixedChar
        Name = 'utente'
      end
      item
        DataType = ftDateTime
        Name = 'datatime'
      end
      item
        DataType = ftFixedChar
        Name = 'operazione'
      end
      item
        DataType = ftString
        Name = 'val_preced'
      end
      item
        DataType = ftInteger
        Name = 'rif_operatore'
      end
      item
        DataType = ftInteger
        Name = 'rif_chiave'
      end
      item
        DataType = ftString
        Name = 'chiave_sec'
      end
      item
        DataType = ftString
        Name = 'idAgenzia'
      end>
  end
  object DBscambio: TDBISAMDatabase
    EngineVersion = '4.37 Build 3'
    DatabaseName = 'DBscambio'
    Directory = 'C:\slp\dati\St\SCAMBIO'
    SessionName = 'SessionAgenzia'
    Left = 224
    Top = 144
  end
  object Qins_trasfDati: TDBISAMQuery
    DatabaseName = 'DBscambio'
    SessionName = 'SessionAgenzia'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into trasfDati'
      
        '(agenzia, cod_int, tipo, data, data_invio, testo, dati, versione' +
        ','
      
        ' varie, ip, polizza, tipo_riga, scadenza, rif_operatore, utente_' +
        'sigla)'
      'values'
      
        '(:agenzia, :cod_int, :tipo, :data, :data_invio, :testo, :dati, :' +
        'versione,'
      
        ' :varie, :ip, :polizza, :tipo_riga, :scadenza,:rif_operatore, :u' +
        'tente_sigla)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'scadenza'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'utente_sigla'
      end>
    Left = 136
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'scadenza'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'utente_sigla'
      end>
  end
  object QscambioINVIA: TDBISAMQuery
    DatabaseName = 'DBscambio'
    SessionName = 'SessionAgenzia'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'INSERT INTO trasfDati'
      
        '(agenzia, sub_age, cod_int, tipo, data_invio, testo, dati, versi' +
        'one, direzione_com, varie, rif_operatore, sigla_utente, tipo_rig' +
        'a, polizza, scadenza, ip)'
      
        'VALUES (:agenzia, :sub_age, :cod_int, :tipo, :data_invio, :testo' +
        ', :dati, :versione, :direzione_com, :varie, :rif_operatore, :sig' +
        'la_utente, :tipo_riga, :polizza, :data_doc, :ip)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'sub_age'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'direzione_com'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_utente'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'data_doc'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end>
    Left = 351
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'sub_age'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'direzione_com'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_utente'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'data_doc'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end>
  end
end
