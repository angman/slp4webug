unit UDMdatiAgePolizze;

interface

uses
  System.SysUtils, System.Classes, Data.DB, dbisamtb, UDMMaster, System.Rtti, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Oggetti, UShowAppendiceRec, UDatiPolSostituitaRec;

type
  EPolizzaDuplicataError = Exception;

  TEnStatoPolizza = (tpViva, tpAnnullata);

  TDMdatiAgePolizze = class(TDMMaster)
    QPolizze: TDBISAMQuery;
    QPolizzaEdit: TDBISAMQuery;
    QFindPolizza: TDBISAMQuery;
    Qlast_pag_s: TDBISAMQuery;
    Qlast_pag: TDBISAMQuery;
    Qpol_da_perfez: TDBISAMQuery;
    DSpol_da_perfez: TDataSource;
    Qgar_base: TDBISAMQuery;
    Qtipo_pol: TDBISAMQuery;
    QgaranzieLista: TDBISAMQuery;
    Qgar_baseT: TDBISAMQuery;
    QNumeriDisponibiliAgenzia: TDBISAMQuery;
    QNumPolizzaDuplicate: TDBISAMQuery;
    QgaranziePolizza: TDBISAMQuery;
    QAssicuratiPolizza: TDBISAMQuery;
    QtitoliPolizza: TDBISAMQuery;
    Qappendice: TDBISAMQuery;
    QdettAppendice: TDBISAMQuery;
    QAssicuratiPolizzaEdit: TDBISAMQuery;
    QInsLKPolizzaAssicurato: TDBISAMQuery;
    QAggTogliAssicurato: TDBISAMQuery;
    QDelLKPolizzaAssicurato: TDBISAMQuery;
    QUpdPolizzaEdit: TDBISAMQuery;
    QInsScadenza: TDBISAMQuery;
    QInsAppendice: TDBISAMQuery;
    QAutoincAppendici: TDBISAMQuery;
    QSLPPolizzeCliente: TDBISAMQuery;
    QFindPolizzaSostituita: TDBISAMQuery;
    DBISAMQuery1: TDBISAMQuery;
    QgetPromoter: TDBISAMQuery;
    QricalcolaPremio: TDBISAMQuery;
    Qins_pol_new: TDBISAMQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QPolizzaEditBeforeOpen(DataSet: TDataSet);
    procedure QPolizzaEditNewRecord(DataSet: TDataSet);
    procedure QPolizzaEditBeforePost(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QPolizzaEditAfterInsert(DataSet: TDataSet);
    procedure QPolizzeBeforeOpen(DataSet: TDataSet);
    procedure QappendiceAfterScroll(DataSet: TDataSet);
    procedure QAssicuratiPolizzaEditNewRecord(DataSet: TDataSet);
    procedure QAssicuratiPolizzaEditAfterPost(DataSet: TDataSet);
    procedure QAssicuratiPolizzaEditBeforePost(DataSet: TDataSet);
    procedure QAssicuratiPolizzaAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    calpremio: TPremio;
    FCurrentCodPolizza: Integer;

    procedure LordoFieldChanged(Sender: TField);

    function lordo: Currency;

    procedure AggiornaNAppendicePolizza(CodPolizza, CodAppendice: Integer);
    procedure InserisciScadenza(QCliente: TDBISAMQuery; const AppRec: TShowAppendiceRec);
    function registraAppendice(CodScadenza: Integer; AppRec: TShowAppendiceRec; const ReportFileName: string): Integer;
    procedure SicronizzaEditOggInGaranzia(CodPolizza, CodAssicurato: Integer);
  public
    { Public declarations }
    // Interfaccia IQueryElenco
    // procedure EseguiQuery(TipoOrdinamento: Integer; TipoFiltro: Integer = 0; ValToSearch: string = '';
    // StatoPagamento: Integer = 0; DirezioneOrdinamento: Integer = 0; CodCollaboratore: Integer = 0); override;

    procedure EseguiQuery(ordFieldName: string; TipoFiltro: Integer = 0; ValToSearch: string = '';
      StatoPagamento: Integer = 0; DirezioneOrdinamento: Integer = 0; CodCollaboratore: Integer = 0;
      codCompagnia: Integer = 0); override;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); override;
    // ----------------------------------------------------------
    procedure ApriOggAssicuratoPolizza(CodPolizza: Integer);
    procedure AggiungiOggAssicuratoPolizza(CodPolizza: Integer);
    procedure TogliOggAssicuratoPolizza(CodPolizza, CodAssicurato: Integer; data_uscita: TDateTime);
    procedure ApriAppendiciPolizza(CodPolizza: Integer);
    procedure ApriGaranziePolizza(CodPolizza: Integer);
    procedure ApriOggAssicuratiPolizza(CodPolizza: Integer);
    procedure ApriTitoliPolizza(CodPolizza: Integer; statoTitolo: Integer);
    procedure caricaTariffa(tipo_pol: Integer; elenco: Tstrings; cosa: string; Forma: string = '');
    procedure CheckNumeroPolizzaDuplicato(NumPolizza: string);
    procedure ClearAssicuratiPolizzaEdit;

    procedure getDatiPolizzaSostituita(ANumPolizza: string; out ADatiPolizzaSostituita: TDatiPolizzaSostituita);

    function getLabelDatiPolizza(TipoPolizza: Integer): string;
    function getMaxAccessoriPolizza(TipoPolizza: Integer): Currency;
    function getPolizzeSLPCliente(ACodCliente: Integer; var QPolSLPCli: TDBISAMQuery): Boolean;
    function getPromoter(AcodPolizza: Integer): Integer;


    function GetTipoProvvigioni: string;
    procedure carica_promoter(items: Tstringlist);
    function numeriDisponibili(sigla: string): Integer;
    procedure memorizzaAppendice(QCliente: TDBISAMQuery; const AppRec: TShowAppendiceRec; const ReportFileName: string;
      isAppendiceCliente: Boolean = false);
    procedure RistampaAppendice;

    procedure RicalcolaPremio(AcodPolizza: Integer; APtasse, APremio: Currency; out APremioNetto, ATasse: Currency);

    procedure ValidatePolizzaSostituita(CodPolSost: string);

    procedure set_post_ins_polizza(attiva: Boolean);

    function ins_polizza_new: integer;

    property CurrentCodPolizza: Integer read FCurrentCodPolizza;
  end;

implementation

uses libreria, UdmdatiAge, System.DateUtils, UCodiciErroriPolizza, System.Variants, MainModule, System.StrUtils,
  UPolizzaExceptions, System.Math, System.IOUtils, ServerModule, UStampePDFViewer, uniGUIApplication;
{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

{ TDMdatiAgePolizze }
resourcestring
  MSG_PolizzaDuplicata = 'N. Polizza: %s gi� esistente';

procedure TDMdatiAgePolizze.carica_promoter(items: Tstringlist);
begin
  UniMainModule.DMdatiAge.caricaPromoter(items);
end;

procedure TDMdatiAgePolizze.CheckNumeroPolizzaDuplicato(NumPolizza: string);
begin
  QNumPolizzaDuplicate.Close;
  QNumPolizzaDuplicate.ParamByName('NPOLIZZA').asString := NumPolizza;
  QNumPolizzaDuplicate.Open;
  if not QNumPolizzaDuplicate.IsEmpty then
  begin
    QNumPolizzaDuplicate.Close;
    raise ENumPolizzaDuplicate.Create(CERR_NUM_POLIZZA_DUPLICATE, Format(MSG_NUM_POLIZZA_DUPLICATE, [NumPolizza]));
  end;

end;

procedure TDMdatiAgePolizze.ClearAssicuratiPolizzaEdit;
begin
  if not(QAssicuratiPolizzaEdit.State in [dsInsert, dsEdit]) then
    QAssicuratiPolizzaEdit.Edit;

  QAssicuratiPolizzaEdit.FieldByName('DATA_RILASCIO').Clear;
  QAssicuratiPolizzaEdit.FieldByName('PRA_CITTA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('PRA_COD_FISC_IVA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('PRA_DENOMINAZIONE').Clear;
  QAssicuratiPolizzaEdit.FieldByName('PRA_INDIRIZZO').Clear;
  QAssicuratiPolizzaEdit.FieldByName('PRA_PROV').Clear;
  QAssicuratiPolizzaEdit.FieldByName('CAP').Clear;
  QAssicuratiPolizzaEdit.FieldByName('CATEGORIA_PAT').Clear;
  QAssicuratiPolizzaEdit.FieldByName('CIFRA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('COD_FISC_IVA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('DATA_IMMATR').Clear;
  QAssicuratiPolizzaEdit.FieldByName('DENOMINAZIONE').Clear;
  QAssicuratiPolizzaEdit.FieldByName('HP_QL').Clear;
  QAssicuratiPolizzaEdit.FieldByName('INDIRIZZO').Clear;
  QAssicuratiPolizzaEdit.FieldByName('MARCA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('MODELLO').Clear;
  QAssicuratiPolizzaEdit.FieldByName('PATENTE').Clear;
  QAssicuratiPolizzaEdit.FieldByName('PROV').Clear;
  QAssicuratiPolizzaEdit.FieldByName('TARGA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('TIPO_ALIMENTAZIONE').Clear;
  // QAssicuratiPolizzaEdit.FieldByName('COD_POLIZZA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('DATA_SCADENZA').Clear;
  QAssicuratiPolizzaEdit.FieldByName('DT_LAST_MOD').AsDateTime := Now;

end;

procedure TDMdatiAgePolizze.DataModuleCreate(Sender: TObject);
begin
  inherited;
  OrdList.Append('n_polizza');
  OrdList.Append('Contraente');
  calpremio          := TPremio.Create;
  FCurrentCodPolizza := 0;
end;

procedure TDMdatiAgePolizze.DataModuleDestroy(Sender: TObject);
begin
  if QPolizzaEdit.FindField('LORDO') <> nil then
    QPolizzaEdit.FieldByName('LORDO').OnChange := nil;

  calpremio.free;

  inherited;

end;

procedure TDMdatiAgePolizze.EseguiQuery(ordFieldName: string; TipoFiltro: Integer = 0; ValToSearch: string = '';
  StatoPagamento: Integer = 0; DirezioneOrdinamento: Integer = 0; CodCollaboratore: Integer = 0;
  codCompagnia: Integer = 0);
begin
  QPolizze.Close;
  QPolizze.SQL.Clear;
  QPolizze.SQL.Add
    ('SELECT COD_POLIZZA, N_POLIZZA, CONTRAENTE, DECORRENZA, SCADENZA, FRAZIONAM, PR.SIGLA, SPR.sigla as siglaSubColl,');
  QPolizze.SQL.Add('    RIF_COMPAGNIA, RIF_TIPO_POL, RIF_COD_RAMO, CODICE,');
  QPolizze.SQL.Add('   LORDO,  COD_CLI, FATTA_DA, STATO, CAUSA_MORTE, DATA_MORTE, SOSPESO_DAL, SOSPESO_AL,');
  QPolizze.SQL.Add(' CP.CODICE_AGENZIA, TP.DESCRIZIONE AS DESTIPOPOL, RAMI.DESCRIZIONE AS DESRAMO, RIF_REFERENTE');
  QPolizze.SQL.Add('FROM POLIZZE POL');
  QPolizze.SQL.Add('LEFT JOIN COMPAGNI CP ON CP.COD_COMPAGNIA = RIF_COMPAGNIA');
  QPolizze.SQL.Add('LEFT JOIN TIPO_POL TP ON COD_TIPO_POL = RIF_TIPO_POL');
  QPolizze.SQL.Add('LEFT JOIN RAMI ON COD_RAMO = RIF_COD_RAMO');
  QPolizze.SQL.Add('LEFT JOIN CLIENTI CLI ON CLI.COD_CLIENTE = POL.COD_CLI ');
  QPolizze.SQL.Add('LEFT JOIN PRODUTT PR ON pr.cod_produttore = cli.promoter');
  QPolizze.SQL.Add('LEFT JOIN PRODUTT SPR ON spr.cod_produttore = cli.SUB_PROMOTER');
  QPolizze.SQL.Add('WHERE N_POLIZZA > '''' ');

  // QPolizze.SQL.Add('AND POL.ID_GEN_SLP_AGE = :ID_GEN_SLP_AGE');  // DA RIATTIVARE CON FIREBIRD !!!!!!
  if codCompagnia > 0 then
    QPolizze.SQL.Add('AND rif_compagnia=' + IntToStr(codCompagnia));

  case TEnStatoPolizza(TipoFiltro) of
    tpViva:
      QPolizze.SQL.Add('AND STATO = ''V'' ');
  else
    QPolizze.SQL.Add('AND STATO = ''C'' ');
  end;

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    QPolizze.SQL.Add('AND CLI.PROMOTER = ' + UniMainModule.utentePromoter.ToString);
    if UniMainModule.UtenteSubPromoter > 0 then
      QPolizze.SQL.Add('and cli.sub_promoter = ' + UniMainModule.UtenteSubPromoter.ToString);
  end
  else
    if CodCollaboratore > 0 then
      QPolizze.SQL.Add(' AND CLI.PROMOTER = ' + CodCollaboratore.ToString);

  if ValToSearch > '' then
    QPolizze.SQL.Add('AND ' + strOrdCol + ' LIKE ''' + ValToSearch + '%''');

  strOrdCol := ordFieldName;
  QPolizze.SQL.Add('ORDER BY ' + strOrdCol + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  QPolizze.Open;

end;

procedure TDMdatiAgePolizze.getDatiPolizzaSostituita(ANumPolizza: string;
  out ADatiPolizzaSostituita: TDatiPolizzaSostituita);
begin
  QFindPolizzaSostituita.Close;
  QFindPolizzaSostituita.ParamByName('N_POLIZZA').asString := ANumPolizza;
  QFindPolizzaSostituita.Open;
  if not QFindPolizzaSostituita.IsEmpty then
  begin
    ADatiPolizzaSostituita.numeroPolizzaSostituita := ANumPolizza;
    ADatiPolizzaSostituita.frazionamento           := QFindPolizzaSostituita.FieldByName('FRAZIONAM').asString;
    ADatiPolizzaSostituita.decorrenza              := QFindPolizzaSostituita.FieldByName('DECORRENZA').AsDateTime;
    ADatiPolizzaSostituita.scadenza                := QFindPolizzaSostituita.FieldByName('SCADENZA').AsDateTime;
    ADatiPolizzaSostituita.percentualeTasse        := QFindPolizzaSostituita.FieldByName('PTASSE').AsCurrency;
    Qlast_pag.Close;
    Qlast_pag.ParamByName('rif_cod_polizza').AsInteger :=  QFindPolizzaSostituita.FieldByName('cod_polizza').AsInteger;
    Qlast_pag.Open;
    try
    if not Qlast_pag.IsEmpty then
    begin
      ADatiPolizzaSostituita.ScadenzaUltimoPagamentoDa := Qlast_pag.FieldByName('DECORRENZA').AsDateTime;
      ADatiPolizzaSostituita.ScadenzaUltimoPagamentoA  := Qlast_pag.FieldByName('scadenza').AsDateTime;
      ADatiPolizzaSostituita.lastPremio                := Qlast_pag.FieldByName('LORDO').AsCurrency;
    end;
    finally
      Qlast_pag.Close;
    end;
    // ADatiPolizzaSostituita.
  end;

end;

function TDMdatiAgePolizze.getLabelDatiPolizza(TipoPolizza: Integer): string;
begin
  Qtipo_pol.Close;
  Qtipo_pol.ParamByName('COD_TIPO_POL').AsInteger := TipoPolizza;
  Qtipo_pol.Open;
  Result := '';
  if not Qtipo_pol.IsEmpty then

    Result := Qtipo_pol.FieldByName('DESCRIZIONE').asString + ' (tasse: ' + Qtipo_pol.FieldByName('PERC_TASSE').asString
      + '%)   (n� disp. ' + IntToStr(numeriDisponibili(Qtipo_pol.FieldByName('SIGLA').asString)) + ')';
  Qtipo_pol.Close;

end;

function TDMdatiAgePolizze.getMaxAccessoriPolizza(TipoPolizza: Integer): Currency;
begin
  Qtipo_pol.Close;
  Qtipo_pol.ParamByName('COD_TIPO_POL').AsInteger := TipoPolizza;
  Qtipo_pol.Open;
  Result := 0;
  if not Qtipo_pol.IsEmpty then
    Result := Qtipo_pol.FieldByName('accessori_max').AsCurrency;
  Qtipo_pol.Close;
end;

function TDMdatiAgePolizze.getPolizzeSLPCliente(ACodCliente: Integer; var QPolSLPCli: TDBISAMQuery): Boolean;
begin
  QPolSLPCli := nil;
  QSLPPolizzeCliente.Close;
  QSLPPolizzeCliente.ParamByName('COD_CLI').AsInteger := ACodCliente;
  QSLPPolizzeCliente.ParamByName('ID_GEN_SLP_AGE').AsInteger := UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;
  QSLPPolizzeCliente.Open;
  Result := not QSLPPolizzeCliente.IsEmpty;
  if Result then
    QPolSLPCli := QSLPPolizzeCliente;
end;

function TDMdatiAgePolizze.getPromoter(AcodPolizza: Integer): Integer;
begin
  // restituisci il codice del promoter che segue questa polizza !!!!
  QgetPromoter.Close;
  QgetPromoter.ParamByName('COD_POLIZZA').AsInteger := AcodPolizza;
  QgetPromoter.Open;
  if QgetPromoter.IsEmpty then
    Result := 0
  else
    Result := QgetPromoter.FieldByName('PROMOTER').AsInteger;

end;

// procedure TDMdatiAgePolizze.EseguiQuery(TipoOrdinamento, TipoFiltro: Integer; ValToSearch: string;
// StatoPagamento, DirezioneOrdinamento: Integer; CodCollaboratore: Integer);
// begin
//
// QPolizze.Close;
// QPolizze.SQL.Clear;
// QPolizze.SQL.Add('SELECT COD_POLIZZA, N_POLIZZA, CONTRAENTE, DECORRENZA, SCADENZA, FRAZIONAM, PR.SIGLA,');
// QPolizze.SQL.Add('    RIF_COMPAGNIA, RIF_TIPO_POL, RIF_COD_RAMO, CODICE,');
// QPolizze.SQL.Add('   LORDO,  COD_CLI, FATTA_DA, STATO, CAUSA_MORTE, DATA_MORTE, SOSPESO_DAL, SOSPESO_AL,');
// QPolizze.SQL.Add(' CP.CODICE_AGENZIA, TP.DESCRIZIONE AS DESTIPOPOL, RAMI.DESCRIZIONE AS DESRAMO, RIF_REFERENTE ');
// QPolizze.SQL.Add(' ');
// QPolizze.SQL.Add('FROM POLIZZE POL');
// QPolizze.SQL.Add('LEFT JOIN COMPAGNI CP ON CP.COD_COMPAGNIA = RIF_COMPAGNIA');
// QPolizze.SQL.Add('LEFT JOIN TIPO_POL TP ON COD_TIPO_POL = RIF_TIPO_POL');
// QPolizze.SQL.Add('LEFT JOIN RAMI ON COD_RAMO = RIF_COD_RAMO');
// QPolizze.SQL.Add('LEFT JOIN CLIENTI CLI ON CLI.COD_CLIENTE = POL.COD_CLI ');
// QPolizze.SQL.Add('LEFT JOIN PRODUTT PR ON POL.FATTA_DA = PR.COD_PRODUTTORE');
// QPolizze.SQL.Add('WHERE N_POLIZZA > '''' ');
// QPolizze.SQL.Add('AND ID_GEN_SLP_AGE = :ID_GEN_SLP_AGE');
//
// case TEnStatoPolizza(TipoFiltro) of
// tpViva:
// QPolizze.SQL.Add('AND STATO = ''V'' ');
// else
// QPolizze.SQL.Add('AND STATO = ''C'' ');
// end;
//
// if UniMainModule.DMdatiAge.is_utente_promoter then
// begin
// QPolizze.SQL.Add('AND CLI.PROMOTER = ' + UniMainModule.utentePromoter.ToString);
// if UniMainModule.UtenteSubPromoter > 0 then
// QPolizze.SQL.Add('and cli.sub_promoter = ' + UniMainModule.UtenteSubPromoter.ToString);
// end
// else
// if CodCollaboratore > 0 then
// QPolizze.SQL.Add(' AND CLI.PROMOTER = ' + CodCollaboratore.ToString);
//
// if ValToSearch > '' then
// QPolizze.SQL.Add('AND ' + strOrdCol + ' LIKE ''' + ValToSearch + '%''');
//
// strOrdCol := OrdList[TipoOrdinamento];
// QPolizze.SQL.Add('ORDER BY ' + strOrdCol + GetOrdDirection(DirezioneOrdinamento));
//
// QPolizze.Open;
//
// end;

function TDMdatiAgePolizze.GetTipoProvvigioni: string;
begin
  Result := QPolizzaEdit.FieldByName('TIPO_PROVVIGIONI').asString;
end;

{$MESSAGE HINT 'Unificare con registraScadenza'}

procedure TDMdatiAgePolizze.InserisciScadenza(QCliente: TDBISAMQuery; const AppRec: TShowAppendiceRec);

  function getTipoTitolo: string;
  begin
    Result := IfThen(AppRec.ImportoModificato, '15', '12');
  end;

begin
  QInsScadenza.ParamByName('DENOMINAZ').asString          := QCliente.FieldByName('DENOMINAZ').asString;
  QInsScadenza.ParamByName('RAMO').AsInteger              := QPolizzaEdit.FieldByName('RIF_COD_RAMO').AsInteger;
  QInsScadenza.ParamByName('DESCRIZ').asString            := '';
  QInsScadenza.ParamByName('FRAZIONAM').asString          := QPolizzaEdit.FieldByName('FRAZIONAM').asString;
  QInsScadenza.ParamByName('LORDO').AsCurrency            := AppRec.Importo;
  QInsScadenza.ParamByName('RATA').AsDateTime             := AppRec.DataAppendice;
  QInsScadenza.ParamByName('COD_CLI').asString            := '';
  QInsScadenza.ParamByName('N_POLIZZA').asString          := QPolizzaEdit.FieldByName('N_POLIZZA').asString;
  QInsScadenza.ParamByName('PROVVIGIONI').AsCurrency      := (AppRec.Importo - AppRec.tasse) / 100 * 30;
  QInsScadenza.ParamByName('NETTO').AsCurrency            := AppRec.Importo - AppRec.tasse;
  QInsScadenza.ParamByName('PTASSE').AsCurrency           := AppRec.ptasse;
  QInsScadenza.ParamByName('TASSE').AsCurrency            := AppRec.tasse;
  QInsScadenza.ParamByName('RIF_COD_COMPAGNIA').AsInteger := QPolizzaEdit.FieldByName('RIF_COMPAGNIA').AsInteger;
  QInsScadenza.ParamByName('RIF_COD_POLIZZA').AsInteger   := QPolizzaEdit.FieldByName('cod_polizza').AsInteger;
  QInsScadenza.ParamByName('INC_INDICE').AsCurrency       := 0;
  QInsScadenza.ParamByName('DECORRENZA').AsDateTime       := AppRec.DataAppendice;
  QInsScadenza.ParamByName('SCADENZA').AsDateTime         := AppRec.DataAppendice;
  QInsScadenza.ParamByName('TIPO_TITOLO').asString        := getTipoTitolo;
  QInsScadenza.ParamByName('VALUTA').asString             := 'E';
  QInsScadenza.ParamByName('RIF_CLIENTE').AsInteger       := QCliente.FieldByName('COD_CLIENTE').AsInteger;
  QInsScadenza.ParamByName('ID_GEN_SLP_AGE').AsInteger    := UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;
  QInsScadenza.ExecSQL;
end;


function TDMdatiAgePolizze.ins_polizza_new: integer;
begin
   Qins_pol_new.Close;
   Qins_pol_new.ParamByName('ID_AGE').AsInteger:= UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;
   Qins_pol_new.ParamByName('data').AsDateTime:= now;
   Qins_pol_new.Open;
   Result:=Qins_pol_new.FieldByName('LAST_CODPOLIZZA').AsInteger;
   Qins_pol_new.Close;
end;


function TDMdatiAgePolizze.lordo: Currency;
begin
  calpremio.pSSN := 0;

  calpremio.ptasse := QPolizzaEdit.FieldByName('PTASSE').AsCurrency;
  calpremio.lordo  := StrToFloatDef(QPolizzaEdit.FieldByName('LORDO').Text, 0);

  Result := calpremio.lordo;
  // adesso nelle proprieta pubblicate di Tpremio trovo i valore che mi servono
  QPolizzaEdit.FieldByName('ssn').AsCurrency   := 0;
  QPolizzaEdit.FieldByName('netto').AsCurrency := calpremio.imponibile;
  QPolizzaEdit.FieldByName('tasse').AsCurrency := calpremio.tasse;

end;

procedure TDMdatiAgePolizze.LordoFieldChanged(Sender: TField);
begin
  lordo;
end;

procedure TDMdatiAgePolizze.memorizzaAppendice(QCliente: TDBISAMQuery; const AppRec: TShowAppendiceRec;
  const ReportFileName: string; isAppendiceCliente: Boolean);
var
  lDataBase: TDBISAMDatabase;
  tablesList: Tstringlist;
  CodScadenza: Integer;
  CodAppendice, prog: Integer;

  procedure RefreshAppendici;
  begin
    Qappendice.DisableControls;
    try
      Qappendice.Close;
      Qappendice.Open
    finally
      Qappendice.EnableControls;
    end;
  end;

begin
  tablesList := Tstringlist.Create;
  try
    lDataBase := QInsScadenza.DBSession.Databases[0];
    tablesList.Append('SCADENZE');
    tablesList.Append('APPENDICI');
    if not lDataBase.InTransaction then
      lDataBase.StartTransaction(tablesList);
    try

      CodScadenza  := UniMainModule.DMdatiAgeTitoli.registraScadenza(QPolizzaEdit, AppRec);
      // CodAppendice := registraAppendice(CodScadenza, AppRec, ReportFileName);
      // mb11112020
      CodAppendice := registraAppendice(CodScadenza, AppRec, ChangeFileExt(ReportFileName, '.FR3'));

      if not isAppendiceCliente then
        AggiornaNAppendicePolizza(QPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger, CodAppendice);

      if isAppendiceCliente then
      begin
        UniMainModule.DMdatiAge.get_codiceX('numero3', prog);
        UniMainModule.DMdatiAgeTitoli.incassaTitolo(CodScadenza, 0, '', date, date, 'P', 0, prog, '12');
      end;

      lDataBase.Commit(True);

      // MB 20112020
      UniMainModule.DMdatiAgeTitoli.QFindTitolo.Close;
      UniMainModule.DMdatiAgeTitoli.QFindTitolo.ParamByName('cod_SCADENZA').AsInteger:=CodScadenza;
      UniMainModule.DMdatiAgeTitoli.QFindTitolo.Open;
      if isAppendiceCliente then
      begin
         UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' PERFAPP' + ' Np '+ UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('n_polizza').AsString // QPolizzaEdit.FieldByName('N_POLIZZA').AsString
         + ' ' + FormatDateTime('dd-mm-yyy', UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('decorrenza').AsDateTime ),
         'CK', '', '', '', 'APP', UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('n_polizza').AsString,
         UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('rata').AsDateTime);
      end else begin
         UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' MEMAPP' + ' Np '+ QPolizzaEdit.FieldByName('N_POLIZZA').AsString
         + ' ' + FormatDateTime('dd-mm-yyy', UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('decorrenza').AsDateTime ),
         'CK', '', '', '', 'APP', UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('n_polizza').AsString,
         UniMainModule.DMdatiAgeTitoli.QFindTitolo.FieldByName('rata').AsDateTime);
      end;
      UniMainModule.DMdatiAgeTitoli.QFindTitolo.Close;

      RefreshAppendici;
    except
      on EDBISAMEngineError do
        lDataBase.Rollback;

    end;
  finally
    tablesList.free;
  end;

end;

function TDMdatiAgePolizze.numeriDisponibili(sigla: string): Integer;
begin
  QNumeriDisponibiliAgenzia.ParamByName('sigla').asString := sigla;
  QNumeriDisponibiliAgenzia.Open;
  Result := QNumeriDisponibiliAgenzia.FieldByName('NUM_DISP').AsInteger;
  QNumeriDisponibiliAgenzia.Close;
end;

procedure TDMdatiAgePolizze.PosizionaQuery(AValue: TValue);
begin

  QPolizzaEdit.Close;
  // mb 02052020
  // QPolizzaEdit.ParamByName('N_POLIZZA').asString := AValue.AsInteger.ToString;
  // QPolizzaEdit.ParamByName('cod_polizza').asInteger := QPolizze.FieldByName('cod_polizza').AsInteger;
  QPolizzaEdit.ParamByName('cod_polizza').AsInteger := AValue.AsInteger;
  QPolizzaEdit.Open;
  QPolizzaEdit.FieldByName('LORDO').OnChange  := LordoFieldChanged;
  QPolizzaEdit.FieldByName('PTASSE').OnChange := LordoFieldChanged;
  FCurrentCodPolizza                          := QPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger;

end;

procedure TDMdatiAgePolizze.QappendiceAfterScroll(DataSet: TDataSet);
begin
  QdettAppendice.Close;
  QdettAppendice.ParamByName('RIF_APPENDICE').AsInteger := Qappendice.FieldByName('COD_APPENDICE').AsInteger;
  QdettAppendice.ParamByName('COD_POLIZZA').AsInteger   := Qappendice.FieldByName('RIF_POLIZZA').AsInteger;
  QdettAppendice.Open;

end;

procedure TDMdatiAgePolizze.QAssicuratiPolizzaAfterScroll(DataSet: TDataSet);
begin
  SicronizzaEditOggInGaranzia(QAssicuratiPolizza.FieldByName('COD_POLIZZA').AsInteger,
    QAssicuratiPolizza.FieldByName('COD_ASSICURATI').AsInteger);

end;

procedure TDMdatiAgePolizze.QAssicuratiPolizzaEditAfterPost(DataSet: TDataSet);

  procedure refreshAssicuratiPolizza;
  var
    BMark: TBookmark;
  begin
    QAssicuratiPolizza.DisableControls;
    try
      BMark := QAssicuratiPolizza.GetBookmark;
      QAssicuratiPolizza.Close;
      QAssicuratiPolizza.Open;
    finally
      QAssicuratiPolizza.GotoBookmark(BMark);
      QAssicuratiPolizza.FreeBookmark(BMark);
    end;

  end;

begin
  QInsLKPolizzaAssicurato.ParamByName('RIF_COD_POLIZZA').AsInteger := QAssicuratiPolizzaEdit.FieldByName('COD_POLIZZA')
    .AsInteger;
  QInsLKPolizzaAssicurato.ParamByName('RIF_COD_ASSICURATO').AsInteger :=
    QAssicuratiPolizzaEdit.FieldByName('COD_ASSICURATI').AsInteger;
  QInsLKPolizzaAssicurato.ParamByName('DATA_INGRESSO').AsDateTime :=
    QAssicuratiPolizzaEdit.FieldByName('DATA_OPE_INGRESSO').AsDateTime;
  QInsLKPolizzaAssicurato.ExecSQL;
  refreshAssicuratiPolizza;
end;

procedure TDMdatiAgePolizze.QAssicuratiPolizzaEditBeforePost(DataSet: TDataSet);
begin
  // mettere validazioni
end;

procedure TDMdatiAgePolizze.QAssicuratiPolizzaEditNewRecord(DataSet: TDataSet);
begin
  QAssicuratiPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger := QPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger;
  QAssicuratiPolizzaEdit.FieldByName('TIPO').asString := 'P';
  QAssicuratiPolizzaEdit.FieldByName('ENTRATA').Clear;

  ClearAssicuratiPolizzaEdit;
end;

procedure TDMdatiAgePolizze.QPolizzaEditAfterInsert(DataSet: TDataSet);
begin
  QPolizzaEdit.FieldByName('LORDO').OnChange           := LordoFieldChanged;
  QPolizzaEdit.FieldByName('PTASSE').OnChange          := LordoFieldChanged;
  QPolizzaEdit.FieldByName('ID_GEN_SLP_AGE').AsInteger := UniMainModule.DMdatiAge.SLPdati_age.cod_int.ToInteger;
end;

procedure TDMdatiAgePolizze.QPolizzaEditBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  UniMainModule.DMdatiAge.QRami.Open;
  UniMainModule.DMdatiAge.QTipoPolizze.Open;
  UniMainModule.DMdatiAge.QCompagnie.Open;
  UniMainModule.DMdatiAge.QAppoggio.Open;
  UniMainModule.DMdatiAge.QConvenzioni.Open;
  UniMainModule.DMdatiAge.QCausali.Open;

end;

procedure TDMdatiAgePolizze.QPolizzaEditBeforePost(DataSet: TDataSet);

  procedure SegnalaDirezione;
  begin
    // tipo contratto, decorrenza, scadenza, frazionamento, contraente, numero polizza, fatto da, lordo,
    // data annullamento e causale annullamento
    if (QPolizzaEdit.State = dsEdit) and
      ((QPolizzaEdit.FieldByName('RIF_TIPO_POL').oldValue <> QPolizzaEdit.FieldByName('RIF_TIPO_POL').NewValue) or
      (QPolizzaEdit.FieldByName('DECORRENZA').oldValue <> QPolizzaEdit.FieldByName('DECORRENZA').NewValue) or
      (QPolizzaEdit.FieldByName('SCADENZA').oldValue <> QPolizzaEdit.FieldByName('SCADENZA').NewValue) or
      (QPolizzaEdit.FieldByName('FRAZIONAM').oldValue <> QPolizzaEdit.FieldByName('FRAZIONAM').NewValue) or
      (QPolizzaEdit.FieldByName('COD_CLI').oldValue <> QPolizzaEdit.FieldByName('COD_CLI').NewValue) or
      (QPolizzaEdit.FieldByName('N_POLIZZA').oldValue <> QPolizzaEdit.FieldByName('N_POLIZZA').NewValue) or
      (QPolizzaEdit.FieldByName('FATTA_DA').oldValue <> QPolizzaEdit.FieldByName('FATTA_DA').NewValue) or
      (QPolizzaEdit.FieldByName('LORDO').oldValue <> QPolizzaEdit.FieldByName('LORDO').NewValue) or
      (QPolizzaEdit.FieldByName('DATA_MORTE').oldValue <> QPolizzaEdit.FieldByName('DATA_MORTE').NewValue) or
      (QPolizzaEdit.FieldByName('CAUSA_MORTE').oldValue <> QPolizzaEdit.FieldByName('CAUSA_MORTE').NewValue)) then
      UniMainModule.comunicaDIR('CKA', datetimetostr(Now) + ' MODD Np' + QPolizzaEdit.FieldByName('N_POLIZZA').asString
        + '  cod:' + QPolizzaEdit.FieldByName('cod_polizza').asString, 'CK', '', '', '', 'MOP',
        QPolizzaEdit.FieldByName('N_POLIZZA').asString, QPolizzaEdit.FieldByName('decorrenza').AsDateTime);
  end;

begin
  if QPolizzaEdit.FieldByName('RIF_COMPAGNIA').IsNull then
    raise EPolizzaError.Create(CERR_COMPANY_MISSED, MSG_COMPANY_MISSED);

  if QPolizzaEdit.FieldByName('N_POLIZZA').IsNull then
    raise EPolizzaError.Create(CERR_NPOLIZZA_MISSED, MSG_NPOLIZZA_MISSED);

  if QPolizzaEdit.FieldByName('CONTRAENTE').asString = '' then
    raise EPolizzaError.Create(CERR_POL_CONTRAENTE_MISSED, MSG_POL_CONTRAENTE_MISSED);

  if (yearOf(QPolizzaEdit.FieldByName('DECORRENZA').AsDateTime) < 1960) or
    (yearOf(QPolizzaEdit.FieldByName('SCADENZA').AsDateTime) < 1960) then
    raise EPolizzaError.Create(CERR_DATE_MISSED, MSG_DATE_MISSED);

  if QPolizzaEdit.FieldByName('DECORRENZA').AsDateTime >= QPolizzaEdit.FieldByName('SCADENZA').AsDateTime then
    raise EPolizzaError.Create(CERR_DATA_EMISS_GT_DATA_EFFETTO, MSG_DATA_EMISS_GT_DATA_EFFETTO);

  if QPolizzaEdit.FieldByName('FRAZIONAM').asString = '' then
    raise EPolizzaError.Create(CERR_FRAZIONAMENTO_MISSED, MSG_FRAZIONAMENTO_MISSED);

  if QPolizzaEdit.State = dsInsert then
  begin
    QFindPolizza.Close;
    QFindPolizza.ParamByName('N_POLIZZA').asString      := QPolizzaEdit.FieldByName('N_POLIZZA').asString;
    QFindPolizza.ParamByName('RIF_COMPAGNIA').AsInteger := QPolizzaEdit.FieldByName('RIF_COMPAGNIA').AsInteger;
    QFindPolizza.Open;
    if not QFindPolizza.IsEmpty then
      raise EPolizzaDuplicataError.Create(Format(MSG_PolizzaDuplicata,
        [QPolizzaEdit.FieldByName('N_POLIZZA').asString]));
  end;

  SegnalaDirezione;
end;

procedure TDMdatiAgePolizze.QPolizzaEditNewRecord(DataSet: TDataSet);
begin
  inherited;
  QPolizzaEdit.FieldByName('STATO').asString            := 'V';
  QPolizzaEdit.FieldByName('DATA_EMISSIONE').AsDateTime := Now;
end;

procedure TDMdatiAgePolizze.QPolizzeBeforeOpen(DataSet: TDataSet);
begin
  // QPolizze.ParamByName('ID_GEN_SLP_AGE').AsInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);
end;

function TDMdatiAgePolizze.registraAppendice(CodScadenza: Integer; AppRec: TShowAppendiceRec;
  const ReportFileName: string): Integer;
var
  stream: TMemoryStream;

begin
  QInsAppendice.ParamByName('TIPO_APPENDICE').AsInteger := IfThen(AppRec.ImportoModificato, 15, 12);
  if (AppRec.tipo_titolo = '12') and (AppRec.tipoApp='CI') and (AppRec.CodCliente > 0) then
    QInsAppendice.ParamByName('DESCRIZIONE').asString := 'APP. CAMBIO INDIRIZZO'
  else
    QInsAppendice.ParamByName('DESCRIZIONE').asString := '';

  QInsAppendice.ParamByName('RIF_TITOLO').AsInteger      := CodScadenza;
  QInsAppendice.ParamByName('DATA_EMISSIONE').AsDateTime := AppRec.DataAppendice;
  QInsAppendice.ParamByName('NUMERO').AsInteger          := AppRec.NAppendice;
  QInsAppendice.ParamByName('RIF_POLIZZA').AsInteger     := AppRec.CodPolizza;
  QInsAppendice.ParamByName('N_POLIZZA').asString        := AppRec.NPolizza;
  QInsAppendice.ParamByName('RIF_COD_CLI').AsInteger     := AppRec.CodCliente;
  QInsAppendice.ParamByName('COMPAGNIA').asString        := '';
  QInsAppendice.ParamByName('TESTO1').asString           := '';
  QInsAppendice.ParamByName('TESTO2').asString           := '';
  QInsAppendice.ParamByName('TESTO3').asString           := '';
  QInsAppendice.ParamByName('FRAZ1').asString            := '';
  QInsAppendice.ParamByName('FRAZ2').asString            := '';
  QInsAppendice.ParamByName('SCADENZA1').Clear;
  QInsAppendice.ParamByName('SCADENZA2').Clear;
  QInsAppendice.ParamByName('TESTO').asString          := '';
  QInsAppendice.ParamByName('IMPORTO').AsCurrency      := AppRec.Importo;
  QInsAppendice.ParamByName('NEXT_RATA').AsCurrency    := AppRec.NextRata;
  QInsAppendice.ParamByName('RIF_COMPAGNIA').AsInteger := AppRec.codCompagnia;
  QInsAppendice.ParamByName('DAL').AsDateTime          := AppRec.DtDalAppendice;
  QInsAppendice.ParamByName('AL').AsDateTime           := AppRec.DtAlAppendice;
  QInsAppendice.ParamByName('OPERATORE').AsInteger     := UniMainModule.UtenteCodice;

  stream := TMemoryStream.Create;
  try
    stream.LoadFromFile(TPath.combine(TPath.combine(UniServerModule.FilesFolderPath, 'PDF'), ReportFileName));
    QInsAppendice.ParamByName('REPORT').LoadFromStream(stream, ftBlob);
  finally
    stream.free;
  end;

  QInsAppendice.ParamByName('DT_LAST_MOD').AsDateTime   := date;
  QInsAppendice.ParamByName('ID_GEN_SLP_AGE').AsInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

  QInsAppendice.ExecSQL;

  QAutoincAppendici.Close;
  QAutoincAppendici.Open;
  Result := QAutoincAppendici.FieldByName('CODAPP').AsInteger;
end;

procedure TDMdatiAgePolizze.RicalcolaPremio(AcodPolizza: Integer; APtasse, APremio: Currency;
  out APremioNetto, ATasse: Currency);
begin
  QricalcolaPremio.ParamByName('cod_polizza').AsInteger := AcodPolizza;
  QricalcolaPremio.Open;
  try
    if (QricalcolaPremio.FieldByName('premiotot').AsCurrency > 0) then
    begin
      // caso con premio su garanzie singole
      APremioNetto := QricalcolaPremio.FieldByName('premiotot').AsCurrency;
      ATasse       := QricalcolaPremio.FieldByName('tot_tasse').AsCurrency;
    end
    else
    begin
      // caso senza garanzie singole
      APremioNetto := roundTo(APremio / (1 + APtasse / 100), -2);
      ATasse       := APremio - APremioNetto;
    end;
  finally
    QricalcolaPremio.Close;
  end;

end;

procedure TDMdatiAgePolizze.RistampaAppendice;
var
  ReportFileName: string;
  PDFFolder: string;
begin
  PDFFolder      := TPath.combine(UniServerModule.FilesFolderPath, 'PDF');
  ReportFileName := 'APP_' + Qappendice.FieldByName('N_Polizza').asString + '_' + FormatDateTime('yyyymmddhhmmss',
    Now) + '.PDF';
  TblobField(Qappendice.FieldByName('report')).savetofile(TPath.combine(PDFFolder, ReportFileName));
  TFPDFViewer.ShowReport(TUniGuiApplication(Owner), ReportFileName, UniServerModule.FilesFolderURL + 'PDF/', PDFFolder,
    True, True);
end;

procedure TDMdatiAgePolizze.set_post_ins_polizza(attiva: Boolean);
begin
   if attiva then QPolizzaEdit.BeforePost := QPolizzaEditBeforePost
   else QPolizzaEdit.BeforePost := nil;
end;


procedure TDMdatiAgePolizze.SicronizzaEditOggInGaranzia(CodPolizza, CodAssicurato: Integer);
begin
  QAssicuratiPolizzaEdit.DisableControls;
  try
    QAssicuratiPolizzaEdit.Close;
    QAssicuratiPolizzaEdit.ParamByName('COD_POLIZZA').AsInteger := CodPolizza;
    QAssicuratiPolizzaEdit.ParamByName('COD_ASSICURATI').AsInteger := CodAssicurato;
    QAssicuratiPolizzaEdit.Open;
  finally
    QAssicuratiPolizzaEdit.EnableControls;
  end;

end;

procedure TDMdatiAgePolizze.TogliOggAssicuratoPolizza(CodPolizza, CodAssicurato: Integer; data_uscita: TDateTime);
var
  tablesList: Tstringlist;
  lDataBase: TDBISAMDatabase;
begin
  tablesList := Tstringlist.Create;
  try
    lDataBase := UniMainModule.DMdatiAgeTitoli.QspostaInStorico.DBSession.Databases[0];
    tablesList.Append('ASSICURATI');
    tablesList.Append('POLIZZE_ASSICURATI');
    if not lDataBase.InTransaction then
      lDataBase.StartTransaction(tablesList);
    try
      QAggTogliAssicurato.ParamByName('COD_ASSICURATO').AsInteger := CodAssicurato;
      QAggTogliAssicurato.ParamByName('DATAUSCITA').AsDateTime := data_uscita;

      QAggTogliAssicurato.ExecSQL;
      QDelLKPolizzaAssicurato.ParamByName('COD_POLIZZA').AsInteger := CodPolizza;
      QDelLKPolizzaAssicurato.ParamByName('COD_ASSICURATO').AsInteger := CodAssicurato;
      QDelLKPolizzaAssicurato.ExecSQL;

      lDataBase.Commit(True);

      SicronizzaEditOggInGaranzia(CodPolizza, CodAssicurato);

    except
      on EDBISAMEngineError do
        lDataBase.Rollback;
    end;

  finally
    tablesList.free;
  end;
end;

procedure TDMdatiAgePolizze.AggiornaNAppendicePolizza(CodPolizza, CodAppendice: Integer);
begin
  if not(QPolizzaEdit.State in [dsInsert, dsEdit]) then
    QPolizzaEdit.Edit;
  QPolizzaEdit.FieldByName('APPENDICE').AsInteger := CodAppendice;
  QPolizzaEdit.Post;
end;

procedure TDMdatiAgePolizze.AggiungiOggAssicuratoPolizza(CodPolizza: Integer);
begin
  QAssicuratiPolizzaEdit.Append;
  QAssicuratiPolizzaEdit.FieldByName('COD_POLIZZA').AsInteger := CodPolizza;
  if not(QAssicuratiPolizzaEdit.State in [dsInsert, dsEdit]) then
    QAssicuratiPolizzaEdit.Edit;

end;

procedure TDMdatiAgePolizze.ApriAppendiciPolizza(CodPolizza: Integer);
begin
  Qappendice.Close;
  Qappendice.ParamByName('RIF_POLIZZA').AsInteger := CodPolizza;
  Qappendice.Open;
end;

procedure TDMdatiAgePolizze.ApriGaranziePolizza(CodPolizza: Integer);
begin
  QgaranziePolizza.Close;
  QgaranziePolizza.ParamByName('RIF_COD_POLIZZA').AsInteger := CodPolizza;
  QgaranziePolizza.Open;
end;

procedure TDMdatiAgePolizze.ApriOggAssicuratiPolizza(CodPolizza: Integer);
begin
  QAssicuratiPolizza.Close;
  QAssicuratiPolizza.ParamByName('RIF_COD_POLIZZA').AsInteger := CodPolizza;
  QAssicuratiPolizza.Open;

end;

procedure TDMdatiAgePolizze.ApriOggAssicuratoPolizza(CodPolizza: Integer);
begin
  QAssicuratiPolizzaEdit.Close;
  QAssicuratiPolizzaEdit.ParamByName('COD_POLIZZA').AsInteger := CodPolizza;
  QAssicuratiPolizzaEdit.Open;
end;

procedure TDMdatiAgePolizze.ApriTitoliPolizza(CodPolizza: Integer; statoTitolo: Integer);
begin

  QtitoliPolizza.Close;
  QtitoliPolizza.SQL.Clear;

  // mostra tutti i titoli in giacenza
  QtitoliPolizza.SQL.Add('select');
  QtitoliPolizza.SQL.Add('      sc.n_polizza Polizza, sc.decorrenza Decorrenza,');
  QtitoliPolizza.SQL.Add('      sc.scadenza Scadenza, round(sc.lordo,2) Lordo,');
  QtitoliPolizza.SQL.Add('      sc.Frazionam Fraz, sc.data_pag "DataPagamento", ');
  // QtitoliPolizza.SQL.Add('      fogli_cassa.numero_fc "Numero FC", ');
  QtitoliPolizza.SQL.Add('      if(sc.foglio_cassa > 0, fogli_cassa.numero_fc, '''') "NumeroFC", ');
  QtitoliPolizza.SQL.Add
    ('      sc.data_ann "DataAnnullamento", if(1 > 2, '' '',''               '') "CausaleAnnullamento", ');
  // QtitoliPolizza.SQL.Add('      compagni.codice_agenzia, ');
  // QtitoliPolizza.SQL.Add('      tipo_pol.descrizione "Tipo pol.",');
  // QtitoliPolizza.SQL.Add('      rami.descrizione Ramo,');  // sc.tipo_titolo as "Tipo Tit."');
  QtitoliPolizza.SQL.Add('      CASE');
  QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = ''10'' THEN ''Pol nuova''');
  QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = ''11'' THEN ''Pol sost.''');
  QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = ''12'' THEN ''Appendic.''');
  QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = ''15'' THEN ''Inc. premio''');
  QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = ''50'' THEN ''Rilievo'' ');
  QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = ''20'' THEN ''Quietanza'' ');
  QtitoliPolizza.SQL.Add('      END AS "TipoTitolo"');

  QtitoliPolizza.SQL.Add(', din.data_operazione, din.importo, tpag.descrizione as descriz_pag, din.descrizione, din.banca, din.emesso_da, ');
  QtitoliPolizza.SQL.Add('din.descrizione_chiusura_sospeso, din.estremi1, din.estremi2, din.estremi3,');
  QtitoliPolizza.SQL.Add('if(extract(year,SC.DATA_PAG_CCP)>2000 then sc.data_pag_ccp else null) as data_pag_ccp ');

  QtitoliPolizza.SQL.Add('   from');
  QtitoliPolizza.SQL.Add('      scadenze sc');
  QtitoliPolizza.SQL.Add('   LEFT OUTER JOIN fogli_cassa ON sc.foglio_cassa = fogli_cassa.codice_foglio');

  QtitoliPolizza.SQL.Add('left join dati_incasso din on din.rif_scadenza=sc.cod_scadenza ');
  QtitoliPolizza.SQL.Add('left join tipo_pagamenti tpag on tpag.codice_pag=din.mezzo_pagamento ');
  // QtitoliPolizza.SQL.Add('   LEFT OUTER JOIN polizze  ON polizze.cod_polizza = sc.rif_cod_polizza and polizze.stato=''V''');

  // QtitoliPolizza.SQL.Add('   LEFT OUTER JOIN rami ON rami.cod_ramo=polizze.rif_cod_ramo');
  // QtitoliPolizza.SQL.Add('   LEFT OUTER JOIN tipo_pol ON tipo_pol.cod_tipo_pol=polizze.rif_tipo_pol');

  QtitoliPolizza.SQL.Add('   where sc.rif_cod_polizza = :rif_cod_pol');

  if statoTitolo = 0 then
  begin
    // mostra solo i titoli insoluti: togli da quelli in giacenza quelli gi� pagati
    QtitoliPolizza.SQL.Add('   and sc.stato <> ''P'' ');

  end
  else
    if statoTitolo >= 1 then
    begin
      // mostra anche tutti i titoli storici per il cliente corrente
      QtitoliPolizza.SQL.Add('UNION ');
      QtitoliPolizza.SQL.Add('select');
      QtitoliPolizza.SQL.Add('      sc.n_polizza Polizza, sc.decorrenza Decorrenza,');
      QtitoliPolizza.SQL.Add('      sc.scadenza Scadenza, round(sc.lordo,2) Lordo,');
      QtitoliPolizza.SQL.Add('      sc.Frazionam Fraz, sc.data_pag "DataPagamento", ');
      QtitoliPolizza.SQL.Add('      if(sc.foglio_cassa>0,fogli_cassa.numero_fc,''DIREZIONE'') "NumeroFC", ');
      QtitoliPolizza.SQL.Add
        ('      sc.data_ann "DataAnnullamento", if(1 > 2,'' '',''               '') "CausaleAnnullamento", ');
      // QtitoliPolizza.SQL.Add('      compagni.codice_agenzia, ');
      // QtitoliPolizza.SQL.Add('      tipo_pol.descrizione "Tipo pol.",');
      // QtitoliPolizza.SQL.Add('      rami.descrizione Ramo, '); //sc.tipo_titolo as "Tipo Tit."');
      QtitoliPolizza.SQL.Add('      CASE');
      QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo=''10'' THEN ''Pol nuova''');
      QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo=''11'' THEN ''Pol sost.''');
      QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo=''12'' THEN ''Appendic.''');
      QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo=''15'' THEN ''Inc. premio''');
      QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo=''50'' THEN ''Rilievo'' ');
      QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo=''20'' THEN ''Quietanza'' ');
      QtitoliPolizza.SQL.Add('      END AS "TipoTitolo"');

      QtitoliPolizza.SQL.Add(', din.data_operazione, din.importo, tpag.descrizione as descriz_pag, din.descrizione, din.banca, din.emesso_da, ');
      QtitoliPolizza.SQL.Add('din.descrizione_chiusura_sospeso, din.estremi1, din.estremi2, din.estremi3,');
      QtitoliPolizza.SQL.Add('if(extract(year,SC.DATA_PAG_CCP)>2000 then sc.data_pag_ccp else null) as data_pag_ccp ');

      QtitoliPolizza.SQL.Add('   from');
      QtitoliPolizza.SQL.Add('      s_scadenze sc');
      QtitoliPolizza.SQL.Add('   LEFT OUTER JOIN fogli_cassa ON sc.foglio_cassa = fogli_cassa.codice_foglio');

      QtitoliPolizza.SQL.Add('left join dati_incasso din on din.rif_scadenza=sc.cod_scadenza ');
      QtitoliPolizza.SQL.Add('left join tipo_pagamenti tpag on tpag.codice_pag=din.mezzo_pagamento ');

      QtitoliPolizza.SQL.Add('   where sc.rif_cod_polizza = :rif_cod_pol');

      if statoTitolo = 2 then
      begin
        // devi appiccicare anche i titoli annullati !!!!
        QtitoliPolizza.SQL.Add('UNION ');
        QtitoliPolizza.SQL.Add('select');
        QtitoliPolizza.SQL.Add('      sc.n_polizza Polizza, sc.decorrenza Decorrenza,');
        QtitoliPolizza.SQL.Add('      sc.scadenza Scadenza, round(sc.lordo,2) Lordo,');
        QtitoliPolizza.SQL.Add('      sc.Frazionam Fraz, if( 1 > 2,sc.data_pag,null) "DataPagamento", ');
        QtitoliPolizza.SQL.Add('      if( 2 > 1, ''ANNULLATA'', '''')  "Numero FC", ');
        QtitoliPolizza.SQL.Add('      sc.data_ann "Data ann.", causali_ann.descrizione "Causale ann.", ');
        QtitoliPolizza.SQL.Add('      CASE');
        QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = 10 THEN ''Pol nuova''');
        QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = 11 THEN ''Pol sost.''');
        QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = 12 THEN ''Appendic.''');
        QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = 15 THEN ''Inc. premio''');
        QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = 50 THEN ''Rilievo'' ');
        QtitoliPolizza.SQL.Add('      WHEN sc.tipo_titolo = 20 THEN ''Quietanza'' ');
        QtitoliPolizza.SQL.Add('      END AS "TipoTitolo"');

        QtitoliPolizza.SQL.Add(', din.data_operazione, din.importo, tpag.descrizione as descriz_pag, din.descrizione, din.banca, din.emesso_da, ');
        QtitoliPolizza.SQL.Add('din.descrizione_chiusura_sospeso, din.estremi1, din.estremi2, din.estremi3,');
        QtitoliPolizza.SQL.Add('if(extract(year,SC.DATA_PAG_CCP)>2000 then sc.data_pag_ccp else null) as data_pag_ccp ');

        QtitoliPolizza.SQL.Add('   from');
        QtitoliPolizza.SQL.Add('      scadenze1 sc');
        QtitoliPolizza.SQL.Add('   LEFT OUTER JOIN causali_ann ON sc.causale_annul = causali_ann.cod_causale');

        QtitoliPolizza.SQL.Add('left join dati_incasso din on din.rif_scadenza=sc.cod_scadenza ');
        QtitoliPolizza.SQL.Add('left join tipo_pagamenti tpag on tpag.codice_pag=din.mezzo_pagamento ');

        QtitoliPolizza.SQL.Add('   where sc.rif_cod_polizza = :rif_cod_pol');

      end;
    end;

  QtitoliPolizza.SQL.Add('   order by sc.decorrenza');
  QtitoliPolizza.ParamByName('RIF_COD_POL').AsInteger := CodPolizza;
  QtitoliPolizza.Open;

end;

procedure TDMdatiAgePolizze.caricaTariffa(tipo_pol: Integer; elenco: Tstrings; cosa: string; Forma: string);
var
  tt: string;
begin
  QgaranzieLista.Close;
  QgaranzieLista.SQL.Clear;
  QgaranzieLista.SQL.Append('SELECT DESCRIZIONE, MASSIMALE, SIGLA + '' - '' + DESCRIZIONE AS DESCRMASSIMALE, ');
  QgaranzieLista.SQL.Append('PREMIO, NOTE, COD_GAR_BASE, CODICE_SLP, SIGLA, TIPO_RIGA');
  QgaranzieLista.SQL.Append('FROM GAR_BASE');
  QgaranzieLista.SQL.Append('WHERE COD_TIPO_CONTRATTO = :COD_TIPO_CONTRATTO');
  QgaranzieLista.SQL.Append('AND TIPO_RIGA = :TIPO_RIGA');
  if Forma <> '' then
    QgaranzieLista.SQL.Append('AND FORMA = :FORMA');

  QgaranzieLista.SQL.Append('ORDER BY SIGLA');
  if Forma <> '' then
    QgaranzieLista.ParamByName('FORMA').asString := Forma;

  QgaranzieLista.ParamByName('COD_TIPO_CONTRATTO').AsInteger := tipo_pol;
  QgaranzieLista.ParamByName('TIPO_RIGA').asString := cosa;
  QgaranzieLista.Open;
  elenco.Clear;
  while not QgaranzieLista.Eof do
  begin
    tt := alltrim(QgaranzieLista.FieldByName('SIGLA').asString) + ' - ' +
      floatToStr(QgaranzieLista.FieldByName('MASSIMALE').AsCurrency) + '=' +
      QgaranzieLista.FieldByName('SIGLA').asString;
    elenco.Add(tt);
    QgaranzieLista.Next;
  end;

end;

{$MESSAGE WARN 'Verificare se serva ancora'}

procedure TDMdatiAgePolizze.ValidatePolizzaSostituita(CodPolSost: string);
begin
  // verifiche per la sostituzione di contratto
  // 1) cerca nel portafoglio la polizza da sostituire
  // se la trovi prendi i dati, altrimenti avvisa e lascia proseguire
  { if CodPolSost > '' then
    begin
    sost_fraz := '';
    if (QPolizze.locate('n_polizza;rif_compagnia',vararrayof([Epol_sost.Text,1]),[])) or
    (UniMainModule.DMdatiAge.isDirezione) then
    begin
    // controlla che non sia una polizza gi� all'annullamento !!!
    raise Exception.Create('Error Message');
    if yearOf(QPolizze.FieldByName('data_morte').AsDateTime) > 1970 then
    ShowMessage('Attenzione ! La polizza scelta � gi� annullata per il '+
    DToC(QPolizze.FieldByName('data_morte').AsDateTime) +
    ' ; verificare la correttezza della scelta.' );

    // chiedi se mantenere lo stesso contraente !

    // carica i dati salienti relativi a decorenza, scadenza, frazionam, ultimo pagamento
    scad_pol_sost := QPolizze.fieldbyname('scadenza').asDateTime;
    Escad_pol_sost.Date := QPolizze.fieldbyname('scadenza').asDateTime;
    sost_ptasse := QPolizze.fieldbyname('ptasse').asCurrency;
    Escad_pol_sost.Refresh;
    deco_pol_sost := QPolizze.fieldbyname('decorrenza').asDateTime;
    // sost_scad_ultima_rata_pag:= TdateTime;
    sost_fraz := QPolizze.fieldbyname('frazionam').asstring;
    // cerca l'ultima rata pagata prima in scadenze e poi in s_scadenze ..e alla fine vedi se
    // � una polizza appena emessa e non ancora con quietanze emesse

    // per semplificare l'operazione potrei mostrare una griglia popolata con una query che
    // mostra l'unione delle quietanze prese da scadenza e s_scadenza e lasciare l'indicazione
    // dell'ultima rata pagata all'operatore ....
    // select * from scadenze where rif_cod_polizza=  and pagata>0 and tipo_polizza='20'
    Qlast_pag.close;
    Qlast_pag.ParamByName('polizza').AsString := CodPolSost;
    Qlast_pag.open;
    if not Qlast_pag.IsEmpty then
    begin
    d:=Qlast_pag.fieldbyname('scadenza').asDateTime;
    if d<=Qlast_pag.fieldbyname('decorrenza').asDateTime then
    d:=incMonth(d,(12 div dai_frazionamento(Qlast_pag.fieldbyname('frazionam').asString)));
    end;

    Qlast_pag_s.close;
    Qlast_pag_s.ParamByName('polizza').AsString := CodPolSost;
    Qlast_pag_s.open;
    if not Qlast_pag_s.IsEmpty then
    begin
    ds:=Qlast_pag_s.fieldbyname('scadenza').asDateTime;
    if ds<=Qlast_pag_s.fieldbyname('decorrenza').asDateTime then
    ds:=incMonth(ds,(12 div dai_frazionamento(Qlast_pag_s.fieldbyname('frazionam').asString)));
    end;

    if ds>d then
    begin
    dd:=ds;
    sost_scad_ur_dal := Qlast_pag_s.fieldbyname('decorrenza').asDateTime;
    sost_scad_ur_al := ds;
    sost_last_premio := Qlast_pag_s.fieldbyname('lordo').asCurrency;
    end;

    if d>ds then
    begin
    dd:=d;
    sost_scad_ur_dal := Qlast_pag.fieldbyname('decorrenza').asDateTime;
    sost_scad_ur_al := d;
    sost_last_premio := Qlast_pag.fieldbyname('lordo').asCurrency;
    end;
    Qlast_pag.Close;
    Qlast_pag_s.Close;

    trovrata:=false;
    if Tscadenze.locate('rif_cod_polizza;tipo_titolo',vararrayof([QPolizze.fieldbyname('cod_polizza').asInteger,'20']),[]) then begin
    // qui dovresti trovarne una sola !!!
    // se pagata considera anche restituzione premio ...
    trovrata:=true;
    end;
    if (not trovrata) and Ts_scadenze.locate('rif_cod_polizza;tipo_titolo',vararrayof([QPolizze.fieldbyname('cod_polizza').asInteger,'20']),[]) then begin
    // cerca la pi� recente (potrebbe essere un codice 10 o 11) per vedere se c'e' un
    // rimborso da fare
    trovrata:=true;
    end;

    end else begin
    // richiedi il caricamento dei datisalienti direttamente dalla form dei dati della polizza
    // da sostituire
    // Bdati_sostClick(sender);
    // modifica in data 27/07/2012 =  la polizza da sostituire DEVE esistere in portafoglio !!!!! altrimenti non permettere
    // il caricamento
    if Epol_sost.Text='......' then Bdati_sostClick(sender)
    else begin
    showmessage('La polizza sostituita non esiste in portafoglio. Caricamento impossibile.');
    Epol_sost.Text:='';
    end;
    end;
    end else begin
    // azzera gli eventuali valori impostati con una precedente sostituzione
    premio_da_rimborsare:=0;
    calcola_premio;
    end; }

end;

end.
