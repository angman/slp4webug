unit UDatiPolizzaPM;

interface

uses
  SysUtils, Classes, UDMDatiBasePolizza, dbisamtb, Data.DB, UTipoSalvataggioDocumento;

type
  TDMDatiPolizzaPM = class(TDMDatiBasePolizza)
    QProfessioniParamedico: TDBISAMQuery;
  private
    { Private declarations }
  protected
    procedure SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer); override;
    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): integer; override;

    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); override;
    procedure SalvaFamiliari(DataSetPolizza: TDataSet); override;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); override;

  public
    { Public declarations }
  end;

function DMDatiPolizzaPM: TDMDatiPolizzaPM;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule;

function DMDatiPolizzaPM: TDMDatiPolizzaPM;
begin
  Result := TDMDatiPolizzaPM(UniMainModule.GetModuleInstance(TDMDatiPolizzaPM));
end;

{ TDMDatiPolizzaPM }

procedure TDMDatiPolizzaPM.SalvaAssicurato(DataSetAssicurati: TDataSet);
begin
  inherited;
  QInsAssicurato.ParamByName('TIPO').asString := 'A'; // altro
  QInsAssicurato.ParamByName('denominazione').asString := QProfessioniParamedico.FieldByName('descriz').AsString;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaPM.SalvaFamiliare(DataSetPolizza: TDataSet; ix: integer);
begin

end;

procedure TDMDatiPolizzaPM.SalvaFamiliari(DataSetPolizza: TDataSet);
begin
  inherited;

end;

procedure TDMDatiPolizzaPM.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  inherited;
  QInsGaranzia.ExecSQL;

end;

function TDMDatiPolizzaPM.SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio): integer;
begin
  SalvaBasePolizza(DataSetPolizza, DataPreventivo, TipoSalvataggio);
  QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger := DataSetPolizza.FieldByName('NDIPAMMIN').AsInteger;

  QInsPolizza.ExecSQL;

  Result := getLastCodPolizza;

end;

initialization

RegisterClass(TDMDatiPolizzaPM);

end.
