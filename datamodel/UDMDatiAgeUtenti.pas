unit UDMDatiAgeUtenti;

interface

uses
  System.SysUtils, System.Classes, UDMMaster, Data.DB, dbisamtb, UQueryElencoIntf, System.Rtti;

type
  TDMDatiAgeUtenti = class(TDMMaster, IQueryElenco)
    QUtenti: TDBISAMQuery;
  private
    procedure setCodReferente(const Value: Integer);
    { Private declarations }
  public
    { Public declarations }
    function CheckPassword(const APassword: string): Boolean;
    procedure ChangePassword(const ANewPassword: string);

    // Interfaccia IQueryElenco
    // procedure EseguiQuery(TipoOrdinamento: Integer; TipoFiltro: Integer = 0; ValToSearch: string = '';
    // StatoPagamento: Integer = 0; DirezioneOrdinamento: Integer = 0; CodCollaboratore: Integer = 0); override;

    procedure EseguiQuery(ordFieldName: string; TipoFiltro: Integer = 0; ValToSearch: string = '';
      StatoPagamento: Integer = 0; DirezioneOrdinamento: Integer = 0; CodCollaboratore: Integer = 0; codCompagnia: Integer=1); override;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); override;

    property CodReferente: Integer write setCodReferente;
  end;

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}
{ TDMDatiAgeUtenti }

procedure TDMDatiAgeUtenti.ChangePassword(const ANewPassword: string);
begin
  if ANewPassword <> '' then
  begin
    if QUtenti.State = dsBrowse then
      QUtenti.Edit;
    QUtenti.FieldByName('MPASSWORD').AsString := ANewPassword;
  end;
end;

function TDMDatiAgeUtenti.CheckPassword(const APassword: string): Boolean;
begin
  Result := APassword = QUtenti.FieldByName('MPASSWORD').AsString;
end;

procedure TDMDatiAgeUtenti.EseguiQuery(ordFieldName: string; TipoFiltro: Integer = 0; ValToSearch: string = '';
      StatoPagamento: Integer = 0; DirezioneOrdinamento: Integer = 0; CodCollaboratore: Integer = 0; codCompagnia: Integer=1);
begin
  if QUtenti.State = dsInactive then
    QUtenti.Open;

end;

// procedure TDMDatiAgeUtenti.EseguiQuery(TipoOrdinamento, TipoFiltro: Integer; ValToSearch: string;
// StatoPagamento, DirezioneOrdinamento: Integer; CodCollaboratore: Integer);
// begin
// inherited;
// if QUtenti.State = dsInactive then
// QUtenti.Open;
//
// end;

procedure TDMDatiAgeUtenti.PosizionaQuery(AValue: TValue);
begin
  if QUtenti.State = dsInactive then
    QUtenti.Open;

  QUtenti.Locate('PROGRESSIVO', AValue.AsInteger, []);
end;

procedure TDMDatiAgeUtenti.setCodReferente(const Value: Integer);
begin
  if QUtenti.State = dsBrowse then
    QUtenti.Edit;
  if Value = 0 then
    QUtenti.FieldByName('PROMOTER').Clear
  else
    QUtenti.FieldByName('PROMOTER').AsInteger := Value;
end;

end.
