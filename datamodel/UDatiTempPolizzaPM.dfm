inherited DMDatiTempPolizzaPM: TDMDatiTempPolizzaPM
  OldCreateOrder = True
  inherited fdmtblPolizza: TFDMemTable
    inherited fdmtblPolizzaDati4: TStringField
      OnChange = fdmtblPolizzaDati4Change
    end
    inherited fdmtblPolizzaDati5: TStringField [67]
    end
    inherited fdmtblPolizzaEstensioni: TStringField [68]
    end
    inherited fdmtblPolizzaSiglaMassimale: TStringField [69]
      OnChange = fdmtblPolizzaSiglaMassimaleChange
    end
    inherited fdmtblPolizzaTipologia: TStringField [70]
    end
    inherited fdmtblPolizzaDatiVari: TMemoField [71]
    end
    inherited fdmtblPolizzaOrdine: TStringField [72]
    end
    inherited fdmtblPolizzaSpecial: TStringField [73]
    end
    inherited fdmtblPolizzaDataImportazione: TDateTimeField [74]
    end
    inherited fdmtblPolizzaCodiceImportazione: TIntegerField [75]
    end
    inherited fdmtblPolizzaIntermediataDa: TIntegerField [76]
    end
    inherited fdmtblPolizzaConvenzione: TIntegerField [77]
    end
    inherited fdmtblPolizzaProvvig: TStringField [78]
    end
    inherited fdmtblPolizzaRifReferente: TIntegerField [79]
    end
    inherited fdmtblPolizzaVarFrp: TMemoField [80]
    end
    inherited fdmtblPolizzaAppReport: TBlobField [81]
    end
    inherited fdmtblPolizzaConAppendice: TStringField [82]
    end
    inherited fdmtblPolizzaAppNumero: TIntegerField [83]
      OnChange = fdmtblPolizzaAppNumeroChange
    end
    inherited fdmtblPolizzaAppDal: TDateField [84]
    end
    inherited fdmtblPolizzaAppAL: TDateField [85]
    end
    inherited fdmtblPolizzaAppOggetto: TStringField [86]
    end
    inherited fdmtblPolizzaPSconto: TIntegerField [87]
    end
    inherited fdmtblPolizzaInpDf: TStringField [88]
    end
    inherited fdmtblPolizzaSubPromoter: TIntegerField [89]
    end
    inherited fdmtblPolizzaSubPromoterSigla: TStringField [90]
    end
    inherited fdmtblPolizzaNPercAccessori: TIntegerField
      OnChange = fdmtblPolizzaNPercAccessoriChange
    end
    object fdmtblPolizzaGruppoSpecializzazione: TStringField [123]
      FieldName = 'GruppoSpecializzazione'
      OnChange = fdmtblPolizzaGruppoSpecializzazioneChange
      Size = 2
    end
    object fdmtblPolizzaEst13: TBooleanField [124]
      FieldName = 'Est13'
      OnChange = fdmtblPolizzaEst13Change
    end
    object fdmtblPolizzaNDipAmmin: TIntegerField [125]
      FieldName = 'NDipAmmin'
      OnChange = fdmtblPolizzaNDipAmminChange
    end
    object fdmtblPolizzaIndicizzata: TBooleanField [126]
      FieldName = 'Indicizzata'
      OnChange = fdmtblPolizzaIndicizzataChange
    end
  end
  inherited frxPolizzaGen: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxAllegatiPOL: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxReport1: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
end
