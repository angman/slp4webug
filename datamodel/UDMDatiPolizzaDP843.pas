unit UDMDatiPolizzaDP843;

interface

uses
  SysUtils, Classes, UDMDatiBasePolizza, dbisamtb, Data.DB, UTipoSalvataggioDocumento;

type
  TDMDatiPolizzaDP843 = class(TDMDatiBasePolizza)
  private
    { Private declarations }
  protected
    procedure RiprendiPolizza(QSLPTPolizze, DataSetPolizza, QassPol: TDataSet); override;
    procedure RiprendiFamiliare(QAssPol, DataSetPolizza: TDataSet; ix: Integer); override;

    function SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
      TipoSalvataggio: TTipoSalvataggio = tsPolizza): Integer; override;
    procedure SalvaAssicurato(DataSetAssicurati: TDataSet); override;
    procedure SalvaFamiliare(DataSetPolizza: TDataSet; ix: Integer); override;
    procedure SalvaGaranzia(DataSetGaranzie: TDataSet); override;

  public
    { Public declarations }
  end;

function DMDatiPolizzaDP843: TDMDatiPolizzaDP843;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, libSLP;

function DMDatiPolizzaDP843: TDMDatiPolizzaDP843;
begin
  Result := TDMDatiPolizzaDP843(UniMainModule.GetModuleInstance(TDMDatiPolizzaDP843));
end;

procedure TDMDatiPolizzaDP843.RiprendiFamiliare(QAssPol, DataSetPolizza: TDataSet; ix: Integer);
begin
  DataSetPolizza.Edit;
  DataSetPolizza.FieldByName('NOME' + ix.ToString).asString := QAssPol.FieldByName('DENOMINAZIONE').asString;
  // QInsAssicurato.ParamByName('COD_FISC_IVA').Clear;
  // QInsAssicurato.ParamByName('NOTE').Clear;
  // QInsAssicurato.ParamByName('ENTRATA').Clear;
  // QInsAssicurato.ParamByName('USCITA').Clear;
  // QInsAssicurato.ParamByName('CAUSALE_USCITA').Clear;
  // QInsAssicurato.ParamByName('SOSTITUITO_DA').Clear;
  DataSetPolizza.FieldByName('PATENTE' + ix.ToString).asString := QAssPol.FieldByName('PATENTE').asString;
  DataSetPolizza.FieldByName('CATPATENTE' + ix.ToString).asString := QAssPol.FieldByName('CATEGORIA_PAT').asString;
  DataSetPolizza.Post;
end;

procedure TDMDatiPolizzaDP843.RiprendiPolizza(QSLPTPolizze, DataSetPolizza, QassPol: TDataSet);
begin
  RiprendiBasePolizza(QSLPTPolizze, DataSetPolizza, QassPol);
  DataSetPolizza.FieldByName('NUMEROASSICURATI').AsInteger := QSLPTPolizze.FieldByName('NUMERO_ASSICURATI').AsInteger;
  DataSetPolizza.Post;
end;

procedure TDMDatiPolizzaDP843.SalvaAssicurato(DataSetAssicurati: TDataSet);
begin
  inherited;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaDP843.SalvaFamiliare(DataSetPolizza: TDataSet; ix: Integer);
begin
  inherited;
  QInsAssicurato.ExecSQL;
end;

procedure TDMDatiPolizzaDP843.SalvaGaranzia(DataSetGaranzie: TDataSet);
begin
  inherited;
  QInsGaranzia.ExecSQL;
end;

function TDMDatiPolizzaDP843.SalvaPolizza(DataSetPolizza: TDataSet; DataPreventivo: TDateTime;
  TipoSalvataggio: TTipoSalvataggio): Integer;
var
  tot, imp, tas: Currency;
begin
  SalvaBasePolizza(DataSetPolizza, DataPreventivo, TipoSalvataggio);
  QInsPolizza.ParamByName('NUMERO_ASSICURATI').AsInteger := DataSetPolizza.FieldByName('NUMEROASSICURATI').AsInteger;

  tot := (DataSetPolizza.FieldByName('TOTALE1').AsCurrency + DataSetPolizza.FieldByName('TOTALE1a').AsCurrency) *
    dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  imp := (DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency + DataSetPolizza.FieldByName('IMPONIBILE1a').AsCurrency)
    * dai_frazionamento(DataSetPolizza.FieldByName('FRAZIONAMENTO').asString);
  tas := tot - imp;

  QInsPolizza.ParamByName('LORDO').AsCurrency := tot;

  QInsPolizza.ParamByName('NETTO').AsCurrency := imp;

  QInsPolizza.ParamByName('TASSE').AsCurrency := tas;

  QInsPolizza.ParamByName('PTASSE').AsCurrency := DataSetPolizza.FieldByName('PTASSE').AsCurrency;

  QInsPolizza.ParamByName('ACCESSORI').AsCurrency := DataSetPolizza.FieldByName('ACCESSORI').AsCurrency;

  QInsPolizza.ParamByName('NETTO1').AsCurrency := DataSetPolizza.FieldByName('NETTO1').AsCurrency +
    DataSetPolizza.FieldByName('NETTO1a').AsCurrency;
  QInsPolizza.ParamByName('ACCESSORI1').AsCurrency := DataSetPolizza.FieldByName('ACCESSORI1').AsCurrency +
    DataSetPolizza.FieldByName('ACCESSORI1a').AsCurrency;
  QInsPolizza.ParamByName('INT_FRAZ').AsCurrency := DataSetPolizza.FieldByName('INTERESSIFRAZIONAMENTO').AsCurrency +
    DataSetPolizza.FieldByName('INTERESSIFRAZIONAMENTOa').AsCurrency;
  QInsPolizza.ParamByName('IMPONIBILE1').AsCurrency := DataSetPolizza.FieldByName('IMPONIBILE1').AsCurrency +
    DataSetPolizza.FieldByName('IMPONIBILE1a').AsCurrency;
  QInsPolizza.ParamByName('IMPOSTE1').AsCurrency := DataSetPolizza.FieldByName('IMPOSTE1').AsCurrency +
    DataSetPolizza.FieldByName('IMPOSTE1a').AsCurrency;
  QInsPolizza.ParamByName('TOTALE1').AsCurrency := DataSetPolizza.FieldByName('TOTALE1').AsCurrency +
    DataSetPolizza.FieldByName('TOTALE1a').AsCurrency;

  QInsPolizza.ParamByName('NETTO2').AsCurrency := DataSetPolizza.FieldByName('NETTO2').AsCurrency +
    DataSetPolizza.FieldByName('NETTO2a').AsCurrency;
  QInsPolizza.ParamByName('ACCESSORI2').AsCurrency := DataSetPolizza.FieldByName('ACCESSORI2').AsCurrency +
    DataSetPolizza.FieldByName('ACCESSORI2a').AsCurrency;
  QInsPolizza.ParamByName('RIMBORSO_SOST').AsCurrency := DataSetPolizza.FieldByName('RIMBORSOSOST').AsCurrency +
    DataSetPolizza.FieldByName('RIMBORSOSOSTa').AsCurrency;
  QInsPolizza.ParamByName('IMPONIBILE2').AsCurrency := DataSetPolizza.FieldByName('IMPONIBILE2').AsCurrency +
    DataSetPolizza.FieldByName('IMPONIBILE2a').AsCurrency;
  QInsPolizza.ParamByName('IMPOSTE2').AsCurrency := DataSetPolizza.FieldByName('IMPOSTE2').AsCurrency +
    DataSetPolizza.FieldByName('IMPOSTE2a').AsCurrency;
  QInsPolizza.ParamByName('TOTALE2').AsCurrency := DataSetPolizza.FieldByName('TOTALE2').AsCurrency +
    DataSetPolizza.FieldByName('TOTALE2a').AsCurrency;
  QInsPolizza.ExecSQL;

  Result := getLastCodPolizza;
end;

initialization

RegisterClass(TDMDatiPolizzaDP843);

end.
