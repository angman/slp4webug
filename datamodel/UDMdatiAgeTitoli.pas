unit UDMdatiAgeTitoli;

interface

uses
  System.SysUtils, System.Classes, System.Rtti, Data.DB, dbisamtb, IWApplication, UdmdatiAge, System.Math,
  UDMMaster, UQueryEditIntf, UQueryElencoIntf, UDMdatiAgeClienti, UShowAppendiceRec, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FireDAC.Comp.DataMove;

type

  TEnOrdTitoli = (otPolizza, otNome, otScadenza);

  TEnTipoTitolo = (ttTutti = 0, ttNewPolizza = 10, ttSubstPolizza = 11, ttModAppendice = 12, ttIncAppendice = 15,
    ttQuietanza = 20, ttRilContabile = 50, ttIncassoSpeseLegali = 51, ttVarie = 55);
  TEnStatoPagamento             = (spTutti, spPagati, spPagatiOggi, spInsoluti, spInsoluti30gg, spInsoluti45gg);
  TEnDirezioneOrdinamento       = (toAscending, toDescending, toNone);
  TEnTipoCopagniaQuietanzamento = (tcNone, tcSLP, tcNotSLP);
  TEnTipoQuietanziamento        = (tqSimulazione, tqQuietanzamento);

  ETitoloError = class(Exception);

  TDMdatiAgeTitoli = class(TDMMaster, IQueryElenco, IQueryEdit)
    Qtitoli: TDBISAMQuery;
    DStitoli: TDataSource;
    Qincassi: TDBISAMQuery;
    Qscadenze: TDBISAMQuery;
    QscadenzeStorico: TDBISAMQuery;
    Qupd_scadenze: TDBISAMQuery;
    DSTipoTitolo: TDataSource;
    QTipoTitolo: TDBISAMQuery;
    QincassaTitolo: TDBISAMQuery;
    QfogliCassa: TDBISAMQuery;
    DSfogliCassa: TDataSource;
    QgetFoglioCassa: TDBISAMQuery;
    QdispagaTitolo: TDBISAMQuery;
    QdispagaTitolo1011: TDBISAMQuery;
    QspostaInStorico: TDBISAMQuery;
    Qnew_titolo: TDBISAMQuery;
    Qtipo_pagamenti: TDBISAMQuery;
    DStipo_pagamenti: TDataSource;
    Qins_estremi_pag: TDBISAMQuery;
    QtitoliFrazionamentoLkup: TStringField;
    QtitoliStato: TStringField;
    Qtitolin_polizza: TStringField;
    Qtitolidenominaz: TStringField;
    QtitoliDecorrenza: TDateField;
    QtitoliLordo: TCurrencyField;
    Qtitoliprovvigioni: TCurrencyField;
    QtitoliData_pag: TDateField;
    QtitoliFraz: TStringField;
    Qtitolicod_scadenza: TAutoIncField;
    Qtitolirif_cod_compagnia: TIntegerField;
    Qtitolifatta_da: TIntegerField;
    Qtitolicod_fc_sub_age: TIntegerField;
    Qtitolitipo_titolo: TStringField;
    Qtitolinetto: TCurrencyField;
    Qtitolitasse: TCurrencyField;
    Qtitolirif_cod_polizza: TIntegerField;
    Qtitolidata_pag_ccp: TDateField;
    Qtitoliold_compagnia: TStringField;
    Qtitoliinpdf: TStringField;
    Qtitolicodice_agenzia: TStringField;
    Qtitolirif_tipo_polizza: TIntegerField;
    Qtitolitipo_polizza: TStringField;
    Qtitoliprovvigioni_sub: TCurrencyField;
    Qtitolidescriz: TStringField;
    QtitoliCompagniaLkup: TStringField;
    QtitoliTipoTitoloLkup: TStringField;
    QTitoloEdit: TDBISAMQuery;
    QFindTitolo: TDBISAMQuery;
    QTitoloEditCOD_SCADENZA: TAutoIncField;
    QTitoloEditDENOMINAZ: TStringField;
    QTitoloEditOLD_COMPAGNIA: TStringField;
    QTitoloEditRAMO: TIntegerField;
    QTitoloEditDESCRIZ: TStringField;
    QTitoloEditFRAZIONAM: TStringField;
    QTitoloEditLORDO: TCurrencyField;
    QTitoloEditRATA: TDateField;
    QTitoloEditCOD_CLI: TStringField;
    QTitoloEditN_POLIZZA: TStringField;
    QTitoloEditSTATO: TStringField;
    QTitoloEditDATA_PAG: TDateField;
    QTitoloEditFOGLIO_CASSA: TIntegerField;
    QTitoloEditDATA_ANN: TDateField;
    QTitoloEditMODIFICA: TDateTimeField;
    QTitoloEditprovvigioni: TCurrencyField;
    QTitoloEditnetto: TCurrencyField;
    QTitoloEditptasse: TCurrencyField;
    QTitoloEditaccessori: TCurrencyField;
    QTitoloEdittasse: TCurrencyField;
    QTitoloEditvaluta: TStringField;
    QTitoloEditRIF_COD_COMPAGNIA: TIntegerField;
    QTitoloEditRIF_COD_POLIZZA: TIntegerField;
    QTitoloEditCAUSALE_ANNUL: TIntegerField;
    QTitoloEditinc_indice: TCurrencyField;
    QTitoloEditPROGRESSIVO: TIntegerField;
    QTitoloEditdecorrenza: TDateField;
    QTitoloEditscadenza: TDateField;
    QTitoloEditINVIO_AVV_SCADENZA: TDateField;
    QTitoloEditINVIO_1_SOLLECITO: TDateField;
    QTitoloEditINVIO_2_SOLLECITO: TDateField;
    QTitoloEditTIPO_TITOLO: TStringField;
    QTitoloEditRIF_CLIENTE: TIntegerField;
    QTitoloEditDATA_COPERTURA: TDateField;
    QTitoloEditDATA_STAMPA: TDateField;
    QTitoloEditP_NOTA_NUMERO: TIntegerField;
    QTitoloEditP_NOTA_DATA: TDateField;
    QTitoloEditDATA_REG_PAG: TDateField;
    QTitoloEditDATA_PAG_CCP: TDateField;
    QTitoloEditINCASSATA_DA: TIntegerField;
    QTitoloEditDATA_IMPORTAZIONE: TDateTimeField;
    QTitoloEditCODICE_IMPORTAZIONE: TIntegerField;
    QTitoloEditprovvigioni_sub: TCurrencyField;
    QTitoloEditCOD_SUB_AGE: TIntegerField;
    QTitoloEditCOD_FC_SUB_AGE: TIntegerField;
    QTitoloEditDT_LAST_MOD: TDateField;
    QTitoloEditID_GEN_SLP_AGE: TIntegerField;
    QTitoloEditN_STAMPE_Q1: TIntegerField;
    QTitoloEditN_STAMPE_Q2: TIntegerField;
    QTitoloEditN_STAMPE_A1: TIntegerField;
    QTitoloEditDesFrazionamento: TStringField;
    QFindTitoloBis: TDBISAMQuery;
    QDispagScadenza: TDBISAMQuery;
    QAutoincScadenza: TDBISAMQuery;
    Qquiet_con_appendice: TDBISAMQuery;
    Qappendice_pagata: TDBISAMQuery;
    QcopiaTitoloCanc: TDBISAMQuery;
    QincassiPrecedenti: TDBISAMQuery;
    QcontaRilievi: TDBISAMQuery;
    Stato: TStringField;
    n_polizza: TStringField;
    denominaz: TStringField;
    Decorrenza: TDateField;
    Lordo: TCurrencyField;
    Provvigioni: TCurrencyField;
    Data_pag: TDateField;
    Fraz: TStringField;
    FrazionamentoLkup: TStringField;
    cod_scadenza: TAutoIncField;
    rif_cod_compagnia: TIntegerField;
    CompagniaLkup: TStringField;
    fatta_da: TIntegerField;
    cod_fc_sub_age: TIntegerField;
    tipo_titolo: TStringField;
    TipoTitoloLkup: TStringField;
    netto: TCurrencyField;
    tasse: TCurrencyField;
    rif_cod_polizza: TIntegerField;
    data_pag_ccp: TDateField;
    old_compagnia: TStringField;
    inpdf: TStringField;
    codice_agenzia: TStringField;
    rif_tipo_polizza: TIntegerField;
    tipo_polizza: TStringField;
    provvigioni_sub: TCurrencyField;
    descriz: TStringField;
    QtitolisiglaSubAge: TStringField;
    QtitolisiglaSubColl: TStringField;
    Qcompagnie: TDBISAMQuery;
    DScompagnie: TDataSource;
    QcercaTitoli1011: TDBISAMQuery;
    QcercaPolDaPerfez: TDBISAMQuery;
    Qnew_titolo1011: TDBISAMQuery;
    QQuietanziamento: TDBISAMQuery;
    fdmtblScadenzeTemp: TFDMemTable;
    fdmtblScadenzeTempprogressivo: TAutoIncField;
    fdmtblScadenzeTempdenominaz: TStringField;
    fdmtblScadenzeTempn_polizza: TStringField;
    fdmtblScadenzeTempdecorrenza: TDateField;
    fdmtblScadenzeTempscadenza: TDateField;
    fdmtblScadenzeTempfrazionam: TStringField;
    fdmtblScadenzeTempcompagnia: TStringField;
    fdmtblScadenzeTempramo: TStringField;
    fdmtblScadenzeTemplordo: TCurrencyField;
    fdmtblScadenzeTempprod_nome: TStringField;
    fdmtblScadenzeTempprod_sigla: TStringField;
    fdmtblScadenzeTempoggetto: TStringField;
    fdmtblScadenzeTempcod_polizza: TIntegerField;
    fdmtblScadenzeTempcod_cliente: TIntegerField;
    fdmtblScadenzeTempcod_compagnia: TIntegerField;
    fdmtblScadenzeTempscadenza_annuale: TStringField;
    fdmtblScadenzeTemptelefono: TStringField;
    fdmtbPolizzaScadenzeTempprovvigioni: TCurrencyField;
    fdmtblScadenzeTemptipo_prov: TStringField;
    fdmtblScadenzeTempprovvigioni_sub: TCurrencyField;
    fdmtblScadenzeTempindirizzo: TStringField;
    fdmtblScadenzeTempcitta: TStringField;
    fdmtblScadenzeTempcap: TStringField;
    fdmtblScadenzeTempprovincia: TStringField;
    fdmtblScadenzeTempcod_fisc: TStringField;
    QFindScadenza: TDBISAMQuery;
    FDDataMove: TFDDataMove;
    fdmtblScadenzeTempnetto: TCurrencyField;
    fdmtblScadenzeTempTasse: TCurrencyField;
    fdmtblScadenzeTempPTasse: TCurrencyField;
    fdmtblScadenzeTempStato: TStringField;
    fdmtblScadenzeTempTipo_titolo: TStringField;

    procedure QTitoloEditBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    function dai_provvigioni(cod_polizza, cod_promoter: integer; premio: currency; tipo: string): currency;
  public
    { Public declarations }
    // Interfaccia IQueryElenco
    procedure EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
      DirezioneOrdinamento: integer = 1; TipoFiltro: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 0); override;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); override;

    procedure assegna_sessione;
    procedure update_titolo(imponibile: Currency; premio_lordo: Currency; provvigioni: Currency; imposte: Currency;
      provvigioni_subagente: Currency; cod_scadenza: integer);
    procedure prepara_query_incassi(dal, al: TdateTime);

    function TitoloIsSLP: Boolean;
    function TitoloIsQuietanza: Boolean;
    function TitoloIsAppendice: Boolean;
    function TitoloIsAppendice12: Boolean;
    function TitoloIsAppendiceDIR: Boolean;
    function TitoloIsPagato: Boolean;
    function TitoloIsScadenza: Boolean;
    function TitoloIsPolizza: Boolean;

    function titolo_pagato(rif_cod_compagnia: integer; n_polizza: string; rata: TdateTime; tipo_titolo: string)
      : integer;

    function trova_scadenza(rif_cod_compagnia: integer; n_polizza: string; rata: TdateTime;
      tipo_titolo: string): Boolean;
    function trova_scadenza_storico(rif_cod_compagnia: integer; n_polizza: string; rata: TdateTime;
      tipo_titolo: string): Boolean;

    function incassaTitolo(cod_scadenza: integer; provvigioni: Currency; descriz: string;
      data_pag, data_pag_ccp: TdateTime; stato: string; incassata_da: integer; progressivo: integer;
      tipo_titolo: string): Boolean;

    function dimezza_provvigioni(provvigioni: Currency; decorrenza: TdateTime; tipo_titolo: string; var descriz: string)
      : Currency;

    procedure dispagaTitolo(cod_scadenza: integer; provvigioni: Currency);
    procedure dispagaTitolo1011(cod_scadenza: integer; n_polizza: string);

    function appendice_pagata(polizza: string; decorrenza: TdateTime; tipo_titolo: string;
      var premio: Currency): Boolean;
    function quietanza_con_appendice(n_polizza: string; rata: TdateTime; var app_pagata: Boolean): Boolean;
    // procedure new_titolo(contraente, frazionam, num_polizza, tipo_titolo, valuta, cod_cli: string;
    // lordo, provvigioni, netto, ptasse, tasse, inc_indice: Currency;
    // ramo, rif_cod_compagnia, rif_cliente, incassata_da, rif_cod_polizza: integer;
    // rata, decorrenza, scadenza: TdateTime); overload;

    // procedure new_titolo(num_polizza: string); overload;
    procedure new_titolo(cod_polizza: integer); overload;

    procedure carica_tipo_pagamenti(dove: TStringList);
    procedure caricaTipiTitolo(ATipiTitoloItems: TStringList);
    procedure dispagaScadenza(ACodScadenza: integer);
    procedure dispagaPolizza(ANumPolizza: string; ACodScadenza: integer);
    procedure estremi_incasso(cod_scadenza: integer; importo: Currency;
      data_operazione, data_valuta, data_titolo: TdateTime; mezzo_pagamento, estremi1, estremi2, estremi3, banca,
      descrizione, in_sospeso, note, emesso_da, n_polizza, sospeso_broker: string);
    function registraScadenza(Qpolizza: TDBISAMQuery; const AppRec: TShowAppendiceRec): integer;

    function cancellaTitolo(codScadenza, codCausale: integer): Boolean;
    function ciSonoIncassiPrecedenti(Data: TdateTime): Boolean;
    function contaRilievi: integer;

    procedure CalcolaQuietanziamento(ATipoCopagniaQuietanzamento: TEnTipoCopagniaQuietanzamento;
      ATipoQuietanziamento: TEnTipoQuietanziamento; ADataQuietanziamento: TDate; ACodCompagnia: integer = 1);

    procedure ExportToExcelAndSend(ReportFileName: string);

  end;

var
  DMdatiAgeTitoli: TDMdatiAgeTitoli;

implementation

uses libreria, System.DateUtils, System.StrUtils, MainModule, UCodiciErroriPolizza, date360, libSLP, UPolizzaExceptions,
  System.IOUtils, ServerModule, uniGUIApplication;
{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

procedure TDMdatiAgeTitoli.assegna_sessione;
begin

end;

procedure TDMdatiAgeTitoli.CalcolaQuietanziamento(ATipoCopagniaQuietanzamento: TEnTipoCopagniaQuietanzamento;
  ATipoQuietanziamento: TEnTipoQuietanziamento; ADataQuietanziamento: TDate; ACodCompagnia: integer);
var
  ctrQuietanze, ctrPolizze: integer;
  mese, anno, Fraz, cod_promoter: integer;
  deco, scad: TdateTime;
  premio, netto, tasse, ptasse, prov, prov_sub: currency;
  passa: Boolean;
  tt, tta: string;

  const
    TIPO_TITOLO_QUIETANZA = '20';

  procedure EseguiQuery;
  begin
    QQuietanziamento.Close;
    QQuietanziamento.SQL.Clear;

    QQuietanziamento.SQL.Append
      ('SELECT FRAZIONAM, DECORRENZA, SCADENZA, EXTRACT(MONTH, SCADENZA) AS MESE, PTASSE, LORDO,');
    QQuietanziamento.SQL.Append
      ('COD_POLIZZA, sospeso_dal, sospeso_al, cod_cli, n_polizza, rif_compagnia, cl.denominaz,');
    QQuietanziamento.SQL.Append('cl.indirizzo, cl.citta, cl.cap, cl.provincia, cl.cod_fisc,');

    QQuietanziamento.SQL.Append('cl.promoter, data_morte, pr.nome as nome_promoter, pr.sigla as sigla_promoter,');
    QQuietanziamento.SQL.Append('comp.codice_agenzia, rif_cod_ramo, ID_GEN_SLP_AGE');
    QQuietanziamento.SQL.Append('from polizze pol');
    QQuietanziamento.SQL.Append(' left join clienti cl on cl.cod_cliente = pol.cod_cli');
    QQuietanziamento.SQL.Append('left join produtt pr on pr.cod_produttore = cl.promoter');
    QQuietanziamento.SQL.Append('left join compagni comp on comp.cod_compagnia = pol.rif_compagnia');
    QQuietanziamento.SQL.Append(' where stato = ''V''');

    QQuietanziamento.SQL.Append
      ('and (Extract(MONTH, scadenza) = Extract(MONTH, :DtQuietanza))');
    QQuietanziamento.SQL.Append
      ('or (if (frazionam = ''S'' or frazionam=''2'', if (Extract(MONTH, scadenza) + 6 > 12, mod (Extract(MONTH, scadenza) + 6, 12),' +
      'Extract(MONTH, scadenza) + 6), 0) = Extract(MONTH, :DtQuietanza)');
    // frazionam0 3
    QQuietanziamento.SQL.Append
      ('or if (frazionam = ''T'' or frazionam=''4'', if (Extract(MONTH, scadenza) + 4 > 12, mod (Extract(MONTH, scadenza) + 4, 12),' +
      'Extract(MONTH, scadenza) + 4), 0) = Extract(MONTH, :DtQuietanza)');
    QQuietanziamento.SQL.Append('or if (frazionam = ''T'' or frazionam=''4'', if (Extract(MONTH, scadenza) + 8 > 12,' +
      'mod (Extract(MONTH, scadenza) + 8, 12), Extract(MONTH, scadenza) + 8), 0) = Extract(MONTH, :DtQuietanza)');
    // frazionam = 4
    QQuietanziamento.SQL.Append('or if (frazionam = ''Q'' or frazionam=''3'', if (Extract(MONTH, scadenza) + 3 > 12,' +
      'mod (Extract(MONTH, scadenza) + 3, 12), + 3), 0) = Extract(MONTH, :DtQuietanza)');

    QQuietanziamento.SQL.Append('or if (frazionam = ''Q'' or frazionam=''3'', if (Extract(MONTH, scadenza) + 6 > 12,' +
      'mod (Extract(MONTH, scadenza) + 6, 12),Extract(MONTH, scadenza) + 6), 0) = Extract(MONTH, :DtQuietanza)');

    QQuietanziamento.SQL.Append('or if (frazionam = ''Q'' or frazionam=''3'', if (Extract(MONTH, scadenza) + 9 > 12,' +
      'mod (Extract(MONTH, scadenza) + 9, 12), Extract(MONTH, scadenza) + 9), 0) = Extract(MONTH, :DtQuietanza)');
    // frazionamento 6

    QQuietanziamento.SQL.Append('or if (frazionam = ''B'' or frazionam=''6'' , if (Extract(MONTH, scadenza) + 2 > 12,' +
      'mod (Extract(MONTH, scadenza) + 2, 12), Extract(MONTH, scadenza) + 2), 0) = Extract(MONTH, :DtQuietanza)');

    QQuietanziamento.SQL.Append('or if (frazionam = ''B'' or frazionam=''6'' , if (Extract(MONTH, scadenza) + 4 > 12,' +
      'mod (Extract(MONTH, scadenza) + 4, 12),  Extract(MONTH, scadenza) + 4), 0) = Extract(MONTH, :DtQuietanza)');

    QQuietanziamento.SQL.Append('or if (frazionam = ''B'' or frazionam=''6'', if (Extract(MONTH, scadenza) + 6 > 12,' +
      'mod (Extract(MONTH, scadenza) + 6, 12), Extract(MONTH, scadenza) + 6), 0) = Extract(MONTH, :DtQuietanza)');

    QQuietanziamento.SQL.Append('or if (frazionam = ''B'' or frazionam=''6'', if (Extract(MONTH, scadenza) + 8 > 12,' +
      'mod (Extract(MONTH, scadenza) + 8, 12), Extract(MONTH, scadenza) + 8), 0) = Extract(MONTH, :DtQuietanza)');

    QQuietanziamento.SQL.Append('or if (frazionam = ''B'' or frazionam=''6'', if (Extract(MONTH, scadenza) + 10 > 12,' +
      'mod (Extract(MONTH, scadenza) + 10, 12), Extract(MONTH, scadenza) + 10), 0) = Extract(MONTH, :DtQuietanza)');
    QQuietanziamento.SQL.Append('or frazionam = ''X'')');
    QQuietanziamento.SQL.Append('and decorrenza < :DtQuietanza');
    QQuietanziamento.SQL.Append('and ID_GEN_SLP_AGE = :ID_GEN_SLP_AGE');

    if ACodCompagnia > 0 then
      QQuietanziamento.SQL.Append('and rif_compagnia = :CodCompagnia');

    QQuietanziamento.SQL.Append('order by rif_compagnia');
    QQuietanziamento.ParamByName('DtQuietanza').AsDate := ADataQuietanziamento;
    if ACodCompagnia > 0 then
      QQuietanziamento.ParamByName('CodCompagnia').Asinteger := ACodCompagnia;
    QQuietanziamento.ParamByName('ID_GEN_SLP_AGE').AsInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);
    QQuietanziamento.Open;
    if QQuietanziamento.IsEmpty then
      raise EPolizzaError.Create(CERR_NO_DATI_EXISTS, MSG_NO_DATI_EXISTS);
  end;

  procedure ElaboraSimulazione;
  var
    QAssicuratiPolizza: TDBISAMQuery;

    function getOggettoPolizza: string;
    begin
      result := QAssicuratiPolizza.fieldbyname('MARCA').asString.Trim + ' ' + QAssicuratiPolizza.fieldbyname('TARGA')
        .asString.Trim;
      if result = '' then
        result := QAssicuratiPolizza.fieldbyname('PATENTE').asString.Trim + ' ' +
          QAssicuratiPolizza.fieldbyname('CATEGORIA_PAT').asString.Trim;
      if result = '' then
        result := QAssicuratiPolizza.fieldbyname('DENOMINAZIONE').asString.Trim;
    end;

  begin

    while not QQuietanziamento.eof do
    begin
      // escludi polizze SLP o con frazionamento temporaneo !
      // if (Tpolizze.FieldByName('rif_compagnia').asInteger<>1) and
      // (Tpolizze.FieldByName('frazionam').asString<>'P')  then
      Fraz := dai_frazionamento(QQuietanziamento.fieldbyname('FRAZIONAM').asString);
      // if quietanzio(mese,anno,QQuietanziamento.fieldbyname('decorrenza').asDateTime,
      // QQuietanziamento.fieldbyname('scadenza').asDateTime,fraz) then
      // calcola tutto
      // calcola le date di inzio e fine di validita' della rata di premio
      deco := crea_data(day(QQuietanziamento.fieldbyname('SCADENZA').asDateTime), mese, anno);
      scad := incMonth(deco, (12 div Fraz));
      // calcola il premio ...
      ptasse := QQuietanziamento.fieldbyname('PTASSE').asCurrency;
      // premio:= int((QQuietanziamento.fieldbyname('lordo').asCurrency/fraz)*100)/100;
      premio := roundTo(QQuietanziamento.fieldbyname('LORDO').asCurrency / Fraz, -2);
      tasse  := 0;
      netto  := 0;
      if ptasse > 0 then
      begin
        // tasse:= int((QQuietanziamento.fieldbyname('lordo').asCurrency/100*ptasse)*100)/100;
        tasse := roundTo(premio / 100 * ptasse, -2);
        netto := premio - tasse;
      end;
      cod_promoter := UniMainModule.DMdatiagePolizze.getPromoter(QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger);
      // calcola le provvigioni
      prov     := dai_provvigioni(QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger, 0, 0, TIPO_TITOLO_QUIETANZA);
      prov_sub := dai_provvigioni(QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger, cod_promoter, 0, TIPO_TITOLO_QUIETANZA);

      passa := true;
      if year(QQuietanziamento.fieldbyname('SOSPESO_DAL').asDateTime) >= 2003 then
      begin
        if (deco >= QQuietanziamento.fieldbyname('SOSPESO_DAL').asDateTime) and
          (deco <= QQuietanziamento.fieldbyname('SOSPESO_AL').asDateTime) then
          passa := false;

      end;
      // Potrebbe essere trasformata in
      // if year(QQuietanziamento.fieldbyname('SOSPESO_DAL').asDateTime) >= 2003 and
      // not ((deco >= QQuietanziamento.fieldbyname('SOSPESO_DAL').asDateTime) and
      // (deco <= QQuietanziamento.fieldbyname('SOSPESO_AL').asDateTime)) then
      // begin
      // controlla che non ci sia gi� questa scadenza !
      if passa then
      begin
        // aggancia la scadenza ..
        {
          QclientiT.Close;
          QclientiT.ParamByName('cod_cliente').asInteger:=QQuietanziamento.fieldbyname('COD_CLI').asInteger;
          QclientiT.Open;
        }
        // DMdatiage.Tclienti.Locate('cod_cliente',QQuietanziamento.fieldbyname('COD_CLI').asInteger,[]);

        tt := QQuietanziamento.fieldbyname('N_POLIZZA').asString + ' ';
        tt := tt + QQuietanziamento.fieldbyname('DENOMINAZ').asString + '  dal ';
        // tt:=tt+ DMdatiAge.Tclienti.fieldbyname('denominaz').asString + '  dal ';
        tt := tt + dtoc(deco) + ' al ';
        tt := tt + dtoc(scad) + ' ';
        tt := tt + transform([premio], '99.999,99') + ' fraz. ';
        tt := tt + QQuietanziamento.fieldbyname('FRAZIONAM').asString;

        {
          Qassicurati.close;
          Qassicurati.ParamByName('cod_pol').asInteger:=QQuietanziamento.fieldbyname('cod_polizza').asInteger;
          Qassicurati.Open;
        }
        UniMainModule.DMdatiagePolizze.ApriOggAssicuratoPolizza(QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger);
        QAssicuratiPolizza := UniMainModule.DMdatiagePolizze.QAssicuratiPolizzaEdit;
        fdmtblScadenzeTemp.Append;
        fdmtblScadenzeTemp.fieldbyname('COD_POLIZZA').AsInteger := QQuietanziamento.fieldbyname('COD_POLIZZA')
          .AsInteger;
        fdmtblScadenzeTemp.fieldbyname('COD_CLIENTE').AsInteger := QQuietanziamento.fieldbyname('COD_CLI').AsInteger;
        fdmtblScadenzeTemp.fieldbyname('COD_COMPAGNIA').AsInteger := QQuietanziamento.fieldbyname('RIF_COMPAGNIA')
          .AsInteger;
        fdmtblScadenzeTemp.fieldbyname('DECORRENZA').asDateTime := deco;
        fdmtblScadenzeTemp.fieldbyname('SCADENZA').asDateTime := scad;
        fdmtblScadenzeTemp.fieldbyname('LORDO').asCurrency := premio;
        fdmtblScadenzeTemp.fieldbyname('PROVVIGIONI').asCurrency := prov;
        fdmtblScadenzeTemp.fieldbyname('PROVVIGIONI_SUB').asCurrency := prov_sub;
        // fdmtblScadenzeTemp.fieldbyname('tipo_prov').asString:='';
        fdmtblScadenzeTemp.fieldbyname('N_POLIZZA').asString := QQuietanziamento.fieldbyname('N_POLIZZA').asString;

        {
          QclientiT.close;
          QclientiT.ParamByName('cod_cliente').asInteger:=QQuietanziamento.fieldbyname('COD_CLI').asInteger;
          QclientiT.Open;
        }
        // DMdatiage.Tclienti.Locate('cod_cliente',QQuietanziamento.fieldbyname('COD_CLI').asInteger,[]);

        // Qcompagni.Locate('cod_compagnia',QQuietanziamento.fieldbyname('RIF_COMPAGNIA').asInteger,[]);

        fdmtblScadenzeTemp.fieldbyname('DENOMINAZ').asString := QQuietanziamento.fieldbyname('DENOMINAZ').asString;
        // fdmtblScadenzeTemp.fieldbyname('denominaz').asString:=DMdatiAge.Tclienti.fieldbyname('denominaz').asString;
        fdmtblScadenzeTemp.fieldbyname('FRAZIONAM').asString := QQuietanziamento.fieldbyname('FRAZIONAM').asString;
        fdmtblScadenzeTemp.fieldbyname('compagnia').asString := QQuietanziamento.fieldbyname('codice_agenzia').asString;
        // Qcompagni.fieldbyname('codice_agenzia').asString;
        if MonthOf(deco) = MonthOf(QQuietanziamento.fieldbyname('SCADENZA').asDateTime) then
          fdmtblScadenzeTemp.fieldbyname('SCADENZA_ANNUALE').asString := 'S'
        else
          fdmtblScadenzeTemp.fieldbyname('SCADENZA_ANNUALE').asString := '';

        fdmtblScadenzeTemp.fieldbyname('OGGETTO').asString := getOggettoPolizza;
        {
          if DMdatiAge.Tclienti.fieldbyname('promoter').asInteger>0 then
          begin
          if Qpromoter1.Locate('cod_produttore', DMdatiAge.Tclienti.fieldbyname('promoter').asInteger,[]) then
          begin
          fdmtblScadenzeTemp.fieldbyname('prod_nome').asString:=Qpromoter1.fieldbyname('nome').asString;
          fdmtblScadenzeTemp.fieldbyname('prod_sigla').asString:=Qpromoter1.fieldbyname('sigla').asString;
          end;
          end;
        }
        fdmtblScadenzeTemp.fieldbyname('PROD_NOME').asString := QQuietanziamento.fieldbyname('NOME_PROMOTER').asString;
        fdmtblScadenzeTemp.fieldbyname('PROD_SIGLA').asString := QQuietanziamento.fieldbyname('SIGLA_PROMOTER')
          .asString;

        // metti anche il numero di telefono ...
        // fdmtblScadenzeTemp.fieldbyname('telefono').asString:=dai_telefono(DMdatiAge.Tclienti.fieldbyname('cod_cliente').asInteger);
        fdmtblScadenzeTemp.fieldbyname('TELEFONO').asString := UniMainModule.DMdatiageClienti.getTelefonoMailpref
          (QQuietanziamento.fieldbyname('COD_CLI').AsInteger, ttmTelefono);
        fdmtblScadenzeTemp.fieldbyname('INDIRIZZO').asString := QQuietanziamento.fieldbyname('INDIRIZZO').asString;
        fdmtblScadenzeTemp.fieldbyname('CITTA').asString := QQuietanziamento.fieldbyname('CITTA').asString;
        fdmtblScadenzeTemp.fieldbyname('CAP').asString := QQuietanziamento.fieldbyname('CAP').asString;
        fdmtblScadenzeTemp.fieldbyname('PROVINCIA').asString := QQuietanziamento.fieldbyname('PROVINCIA').asString;
        fdmtblScadenzeTemp.fieldbyname('COD_FISC').asString := QQuietanziamento.fieldbyname('COD_FISC').asString;
        fdmtblScadenzeTemp.post;
        inc(ctrQuietanze);
        // Lsituazione.Caption := 'Elaborati: ' + inttostr(fdmtblScadenzeTemp.RecordCount) + '/' +
        // inttostr(QQuietanziamento.RecordCount);

      end;
      QQuietanziamento.Next;
      inc(ctrPolizze);

    end;
  end;

  procedure ElaboraQuietanziamenti;
  begin
    while not QQuietanziamento.eof do
    begin
      // escludi polizze SLP o con frazionamento temporaneo !
      // if (Tpolizze.FieldByName('rif_compagnia').asInteger<>1) and
      // (Tpolizze.FieldByName('frazionam').asString<>'P')  then
      if true then
      begin
        // verifica che non sia stata annullata per questa data !!!!!!
        if true then // (Tpolizze.fieldbyname('stato').asString='V') then
        begin
          Fraz := dai_frazionamento(QQuietanziamento.fieldbyname('FRAZIONAM').asString);
          if quietanzio(mese, anno, QQuietanziamento.fieldbyname('DECORRENZA').asDateTime,
            QQuietanziamento.fieldbyname('SCADENZA').asDateTime, Fraz) then
          begin
            // calcola tutto
            // calcola le date di inzio e fine di validita' della rata di premio
            deco := crea_data(day(QQuietanziamento.fieldbyname('SCADENZA').asDateTime), mese, anno);
            scad := incMonth(deco, (12 div Fraz));

            // verifica che non sia stata annullata per questa data !!!!!!
            if (yearOf(QQuietanziamento.fieldbyname('DATA_MORTE').asDateTime) < 1980) or
              (deco < QQuietanziamento.fieldbyname('DATA_MORTE').asDateTime) then
            begin

              ptasse := QQuietanziamento.fieldbyname('PTASSE').asCurrency;
              premio := roundTo(QQuietanziamento.fieldbyname('LORDO').asCurrency / Fraz, -2);

              tasse := 0;
              netto := 0;
              // tasse:= roundto(int((Tpolizze.fieldbyname('lordo').asCurrency/100*ptasse)*100)/100,-2);
              // calcola il premio netto e le tasse
              UniMainModule.DMdatiagePolizze.RicalcolaPremio(QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger,
                ptasse, premio, netto, tasse);
              // DMdatiPolizze.QricalcolaPremio.ParamByName('cod_polizza').AsInteger :=
              // QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger;
              // DMdatiPolizze.QricalcolaPremio.Open;
              // if (DMdatiPolizze.QricalcolaPremio.fieldbyname('premiotot').asCurrency > 0) then
              // begin
              // // caso con premio su garanzie singole
              // netto := DMdatiPolizze.QricalcolaPremio.fieldbyname('premiotot').asCurrency;
              // tasse := DMdatiPolizze.QricalcolaPremio.fieldbyname('tot_tasse').asCurrency;
              // end
              // else
              // begin
              // // caso senza garanzie singole
              // netto := roundTo(premio / (1 + ptasse / 100), -2);
              // tasse := premio - netto;
              // end;

              passa := true;
              if year(QQuietanziamento.fieldbyname('SOSPESO_DAL').asDateTime) >= 2003 then
              begin
                if (deco >= QQuietanziamento.fieldbyname('SOSPESO_DAL').asDateTime) and
                  (deco <= QQuietanziamento.fieldbyname('SOSPESO_AL').asDateTime) then
                  passa := false;

              end;
              // calcola le provvigioni
              cod_promoter := UniMainModule.DMdatiagePolizze.getPromoter(QQuietanziamento.fieldbyname('COD_POLIZZA')
                .AsInteger);
              prov := dai_provvigioni(QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger, 0, 0,
                TIPO_TITOLO_QUIETANZA);
              prov_sub := dai_provvigioni(QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger, cod_promoter, 0, TIPO_TITOLO_QUIETANZA);

              // controlla che non ci sia gi� questa scadenza !

              if passa then
              begin
                if not trova_scadenza(ACodCompagnia, QQuietanziamento.fieldbyname('N_POLIZZA').asString, deco,
                  TIPO_TITOLO_QUIETANZA) then
                begin
                  // aggancia la scadenza ..
                  fdmtblScadenzeTemp.Append;
                  fdmtblScadenzeTemp.fieldbyname('COD_POLIZZA').AsInteger :=
                    QQuietanziamento.fieldbyname('COD_POLIZZA').AsInteger;
                  fdmtblScadenzeTemp.fieldbyname('COD_COMPAGNIA').AsInteger :=
                    QQuietanziamento.fieldbyname('RIF_COMPAGNIA').AsInteger;
                  fdmtblScadenzeTemp.fieldbyname('DECORRENZA').asDateTime := deco;
                  fdmtblScadenzeTemp.fieldbyname('DECORRENZA').asDateTime := deco;
                  fdmtblScadenzeTemp.fieldbyname('SCADENZA').asDateTime := scad;
                  fdmtblScadenzeTemp.fieldbyname('LORDO').asCurrency := premio;
                  fdmtblScadenzeTemp.fieldbyname('COD_CLIENTE').AsInteger := QQuietanziamento.fieldbyname('COD_CLI')
                    .AsInteger;
                  fdmtblScadenzeTemp.fieldbyname('NETTO').asCurrency := netto;
                  fdmtblScadenzeTemp.fieldbyname('PROVVIGIONI').asCurrency := prov;
                  fdmtblScadenzeTemp.fieldbyname('PROVVIGIONI_SUB').asCurrency := prov_sub;
                  fdmtblScadenzeTemp.fieldbyname('PTASSE').asCurrency := ptasse;
                  fdmtblScadenzeTemp.fieldbyname('TASSE').asCurrency := tasse;
                  fdmtblScadenzeTemp.fieldbyname('RAMO').AsInteger := QQuietanziamento.fieldbyname('RIF_COD_RAMO')
                    .AsInteger;

                  fdmtblScadenzeTemp.fieldbyname('N_POLIZZA').asString :=
                    QQuietanziamento.fieldbyname('N_POLIZZA').asString;
                  UniMainModule.DMdatiageClienti.PosizionaQuery(QQuietanziamento.fieldbyname('COD_CLI').AsInteger);
                  fdmtblScadenzeTemp.fieldbyname('DENOMINAZ').asString :=
                    UniMainModule.DMdatiageClienti.QeditCliente.fieldbyname('DENOMINAZ').asString;
                  fdmtblScadenzeTemp.fieldbyname('FRAZIONAM').asString :=
                    QQuietanziamento.fieldbyname('FRAZIONAM').asString;
                  fdmtblScadenzeTemp.fieldbyname('STATO').asString := ' ';
                  fdmtblScadenzeTemp.fieldbyname('TIPO_TITOLO').asString := TIPO_TITOLO_QUIETANZA;
                  // fdmtblScadenzeTemp.fieldbyname('').asInteger:=

                  fdmtblScadenzeTemp.post;
                  inc(ctrQuietanze);
                end;
              end;
            end;
          end;

        end;
      end;
      QQuietanziamento.Next;
      inc(ctrPolizze);
      // label4.Caption := 'Polizze esaminate:' + inttostr(ctrPolizze) + ' su ' + inttostr(QQuietanziamento.RecordCount) +
      // '  quietanze emesse ' + inttostr(ctrQuietanze);
    end;
  end;

begin
  fdmtblScadenzeTemp.EmptyDataSet;
  fdmtblScadenzeTemp.Open;
  fdmtblScadenzeTemp.DisableControls;
  ctrPolizze   := 0;
  ctrQuietanze := 0;
  try
    EseguiQuery;

    mese := MonthOf(ADataQuietanziamento);
    anno := yearOf(ADataQuietanziamento);

    case ATipoQuietanziamento of
      tqSimulazione:
        ElaboraSimulazione;
    else
      ElaboraQuietanziamenti;
    end;
  finally
    fdmtblScadenzeTemp.First;
    fdmtblScadenzeTemp.EnableControls;
  end;
end;

function TDMdatiAgeTitoli.dai_provvigioni(cod_polizza, cod_promoter: integer; premio: currency; tipo: string): currency;
begin
  result := 0.0;
end;

procedure TDMdatiAgeTitoli.DataModuleCreate(Sender: TObject);
begin
  inherited;
  fdmtblScadenzeTemp.CreateDataSet;

end;

function TDMdatiAgeTitoli.cancellaTitolo(codScadenza, codCausale: integer): Boolean;
var
  n_polizza, tipo_titolo: string;
  titolo_slp: Boolean;
  decorrenza: TdateTime;
begin
  // cancellazione del titolo corrente con la causale
  n_polizza   := Qtitoli.FieldByName('N_POLIZZA').AsString;
  tipo_titolo := Qtitoli.FieldByName('tipo_titolo').AsString;
  titolo_slp  := TitoloIsSLP;
  decorrenza  := Qtitoli.FieldByName('decorrenza').AsDateTime;

  QcopiaTitoloCanc.Close;
  QcopiaTitoloCanc.ParamByName('data').AsDateTime        := Date;
  QcopiaTitoloCanc.ParamByName('cod_causale').asInteger  := codCausale;
  QcopiaTitoloCanc.ParamByName('cod_scadenza').asInteger := codScadenza;
  QcopiaTitoloCanc.ExecSQL;

  UniMainModule.traccia('DELTITMAN', codScadenza, 'SCADENZE', 'Canc. Quiet. ' + n_polizza);
  if titolo_slp then
    UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' DEQ Np ' + tipo_titolo + ' ' + n_polizza + ' ' +
      FormatDateTime('dd-mm-aaaa', decorrenza), 'CK', '', '', '', 'DEQ', n_polizza, decorrenza);

  Result := True;

  {
    cc_scadenza:=DMdatiAge.Tscadenze1.FieldByName('cod_scadenza').asInteger;
    // passa tutte le comunicazione sul morto !!!
    if not DMdatiAge.Tcom_titoli.Active then DMdatiAge.Tcom_titoli.Open;
    while DMdatiAge.Tcom_titoli.locate('rif_cod_scadenza',c_scadenza,[]) do
    begin
    DMdatiAge.Tcom_titoli.edit;
    DMdatiAge.Tcom_titoli.fieldbyname('rif_cod_scadenza').asInteger:=cc_scadenza;
    DMdatiAge.Tcom_titoli.post;
    end;
    Qscadenze.FlushBuffers;
    Qscadenze.Close;
    Qscadenze.Open;
  }

end;

procedure TDMdatiAgeTitoli.caricaTipiTitolo(ATipiTitoloItems: TStringList);
begin
  if QTipoTitolo.State = dsInactive then
    QTipoTitolo.Open;
  QTipoTitolo.DisableControls;
  try
    QTipoTitolo.First;
    ATipiTitoloItems.Clear;
    while not QTipoTitolo.Eof do
    begin
      ATipiTitoloItems.Append(QTipoTitolo.FieldByName('DESCRIZIONE').AsString + '=' + QTipoTitolo.FieldByName('SIGLA')
        .AsString);
      QTipoTitolo.Next;
    end;
  finally
    QTipoTitolo.EnableControls;
  end;
end;

procedure TDMdatiAgeTitoli.carica_tipo_pagamenti(dove: TStringList);
begin
  // carica i dati
  Qtipo_pagamenti.Open;
  while not Qtipo_pagamenti.Eof do
  begin
    dove.Add(Qtipo_pagamenti.FieldByName('codice_pag').AsString + ' ' + Qtipo_pagamenti.FieldByName('descrizione')
      .AsString);
    Qtipo_pagamenti.Next;
  end;
  Qtipo_pagamenti.Close;
end;

function TDMdatiAgeTitoli.ciSonoIncassiPrecedenti(Data: TdateTime): Boolean;
begin
  QincassiPrecedenti.Close;
  QincassiPrecedenti.ParamByName('data').AsDateTime := Data;
  QincassiPrecedenti.Open;
  Result := QincassiPrecedenti.FieldByName('n_titoli').asInteger > 0;
  QincassiPrecedenti.Close;
end;

function TDMdatiAgeTitoli.contaRilievi: integer;
begin
  QcontaRilievi.Open;
  Result := QcontaRilievi.FieldByName('n_rilievi').asInteger;
  QcontaRilievi.Close;
end;

function TDMdatiAgeTitoli.dimezza_provvigioni(provvigioni: Currency; decorrenza: TdateTime; tipo_titolo: string;
  var descriz: string): Currency;
var
  diff, perc_pro_inso: integer;
  provvig: Currency;
  dimezza: Boolean;
begin
  // se incasso avenuto dopo 50 gg dalla scadenza del titolo: dimezza le provvigioni
  diff := trunc(Date - decorrenza);
  // diff messo a 60 gg il 14/05/2008, prima era a 75 gg
  dimezza := (diff > 60) and (tipo_titolo = '20');
  provvig := provvigioni;
  if dimezza then
  begin
    // la percentuale di riduzione delle provvigioni prendila
    // dal file codicinew campo PERC_PRO_INSO
    perc_pro_inso := UniMainModule.DMdatiAge.get_perc_pro_insolute;
    descriz       := 'PO' + inttostr(trunc(provvigioni * 100));
    // old_provvig   := provvigioni;
    provvig := roundTo(provvigioni / 100 * perc_pro_inso, -2);
  end;
  Result := provvig;

end;

procedure TDMdatiAgeTitoli.dispagaPolizza(ANumPolizza: string; ACodScadenza: integer);
var
  tablesList: TStringList;
  lDatabase: TDBISAMDatabase;
  lDecorrenza: TDate;

  procedure EmettiComunicazione;
  begin
    UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' DISP Np' + ANumPolizza + ' ' + FormatDateTime('dd-mm-yyy',
      lDecorrenza), 'CK', '', '', '', 'DIP', ANumPolizza, lDecorrenza);
  end;
  procedure RefreshTitoli;
  begin
    Qtitoli.DisableControls;
    try
      Qtitoli.Close;
      Qtitoli.Open;
      Qtitoli.Locate('COD_SCADENZA', ACodScadenza, []);
    finally
      Qtitoli.EnableControls;
    end;
  end;

  function cercaPolDaPerfez: Boolean;
  begin
     QcercaPolDaPerfez.Close;
     QcercaPolDaPerfez.ParamByName('n_polizza').AsString:=Qtitoli.FieldByName('n_polizza').AsString;
     QcercaPolDaPerfez.Open;
     Result := QcercaPolDaPerfez.RecordCount>0;
  end;

begin
  // devi differenziare :
  // se la polizza arriva dalle polizze da perfezionare allora
  //                  a) dispaga titolo e cancellalo b) cancella la polizza da polizze.dat
  //                  c) togli alla polizza presente in slp_tpolizze lo stato di perfezionamento
  // se la polizza � stata caricata a mano e anche il titolo relativo allora
  //                  a) dispaga il titolo e basta !!!! avvisa anche l'utente che la polizza, essendo stata
  //                     inserita manualmente rimane nel file polizze.dat fino a quanto non verr� cancellata manualmente
  if cercaPolDaPerfez then
  begin
     // caso normale di polizza emessa e perfezionata
     tablesList := TStringList.Create;
     try
       lDatabase := QdispagaTitolo1011.DBSession.Databases[0];
       tablesList.Append('SLP_TPOLIZZE');
       tablesList.Append('POLIZZE');
       tablesList.Append('SCADENZE');
       if not lDatabase.InTransaction then
         lDatabase.StartTransaction(tablesList);
       try
         if (Qtitoli.FieldByName('COD_SCADENZA').asInteger <> ACodScadenza) then
           Qtitoli.Locate('COD_SCADENZA', ACodScadenza, []);

         lDecorrenza := Qtitoli.FieldByName('decorrenza').AsDateTime;
         QdispagaTitolo1011.Close;
         QdispagaTitolo1011.SQL.Clear;
         QdispagaTitolo1011.SQL.Add('update slp_tpolizze');
         QdispagaTitolo1011.SQL.Add('set status = null, data_perfez = null');
         QdispagaTitolo1011.SQL.Add('where n_polizza=' + QuotedStr(ANumPolizza) + ';');

         QdispagaTitolo1011.SQL.Add('delete from polizze');
         QdispagaTitolo1011.SQL.Add('where n_polizza=' + QuotedStr(ANumPolizza) + ';');

         QdispagaTitolo1011.SQL.Add('delete from scadenze');
         QdispagaTitolo1011.SQL.Add('where cod_scadenza =' + inttostr(ACodScadenza) + ' and n_polizza=' +
           QuotedStr(ANumPolizza));

         // QdispagaTitolo1011.ParamByName('n_polizza').AsString  := ANumPolizza;
         // QdispagaTitolo1011.ParamByName('cod_scadenza').asInteger := ACodScadenza;
         QdispagaTitolo1011.ExecSQL;
         lDatabase.Commit(True);
         RefreshTitoli;
         EmettiComunicazione;
       except
         on EDBISAMEngineError do
           lDatabase.Rollback;
       end;
     finally
       tablesList.Free;
     end;
  end else begin
     // caso in cui la polizza � stata caricata a amano e anche il titolo � stato caricato a mano
     QdispagaTitolo1011.Close;
     QdispagaTitolo1011.SQL.Clear;
     QdispagaTitolo1011.SQL.Add('update scadenze');
     QdispagaTitolo1011.SQL.Add('set stato = null, data_pag = null');
     QdispagaTitolo1011.SQL.Add('where cod_scadenza=' + Qtitoli.FieldByName('COD_SCADENZA').AsString );
     QdispagaTitolo1011.ExecSQL;
     RefreshTitoli;
     EmettiComunicazione;
  end;
end;

procedure TDMdatiAgeTitoli.dispagaScadenza(ACodScadenza: integer);
var
  TipoComunicazione: string;

  procedure EmettiComunicazione;
  begin
    if TitoloIsQuietanza then
      TipoComunicazione := 'DIQ'
    else
      if TitoloIsAppendice then
        TipoComunicazione := 'DIA';
    UniMainModule.comunicaDIR('CKA', datetimetostr(now) + ' DISQ Np' + Qtitoli.FieldByName('N_POLIZZA').AsString + ' ' +
      FormatDateTime('dd-mm-aaaa', Qtitoli.FieldByName('decorrenza').AsDateTime), 'CK', '', '', '', TipoComunicazione,
      Qtitoli.FieldByName('N_POLIZZA').AsString, Qtitoli.FieldByName('decorrenza').AsDateTime);

  end;

  procedure RefreshTitoli;
  begin
    Qtitoli.DisableControls;
    try
      Qtitoli.Close;
      Qtitoli.Open;
      Qtitoli.Locate('COD_SCADENZA', ACodScadenza, []);
    finally
      Qtitoli.EnableControls;
    end;
  end;

begin
  if (Qtitoli.FieldByName('COD_SCADENZA').asInteger <> ACodScadenza) then
    Qtitoli.Locate('COD_SCADENZA', ACodScadenza, []);

  QDispagScadenza.ParamByName('STATO').Clear;
  QDispagScadenza.ParamByName('DATAPAG').Clear;
  if (copy(Qtitoli.FieldByName('DESCRIZ').AsString, 1, 2) = 'PO') then
  begin
    QDispagScadenza.ParamByName('PROVVIGIONI').AsCurrency :=
      roundTo(strtoint(copy(Qtitoli.FieldByName('descriz').AsString, 3, 7)) / 100, -1);
    QDispagScadenza.ParamByName('DESCRIZ').Clear;
  end
  else
  begin
    QDispagScadenza.ParamByName('PROVVIGIONI').AsCurrency := Qtitoli.FieldByName('PROVVIGIONI').AsCurrency;
    QDispagScadenza.ParamByName('DESCRIZ').AsString       := Qtitoli.FieldByName('DESCRIZ').AsString;

  end;
  QDispagScadenza.ParamByName('COD_SCADENZA').asInteger := ACodScadenza;
  QDispagScadenza.ExecSQL;
  RefreshTitoli;
  EmettiComunicazione;
end;

procedure TDMdatiAgeTitoli.dispagaTitolo(cod_scadenza: integer; provvigioni: Currency);
begin
  // dispaga titolo
  QdispagaTitolo.ParamByName('cod_scadenza').asInteger := cod_scadenza;
  QdispagaTitolo.ParamByName('provvigioni').AsCurrency := provvigioni;
  QdispagaTitolo.ExecSQL;
end;

procedure TDMdatiAgeTitoli.dispagaTitolo1011(cod_scadenza: integer; n_polizza: string);
begin
  // dispaga titolo 10 o 11
  QdispagaTitolo1011.ParamByName('cod_scadenza').asInteger := cod_scadenza;
  QdispagaTitolo1011.ParamByName('n_polizza').AsString     := n_polizza;
  QdispagaTitolo1011.ExecSQL;

end;

procedure TDMdatiAgeTitoli.EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
  DirezioneOrdinamento, TipoFiltro, CodCollaboratore, codCompagnia: integer);
var
  strWhere: string;
  i: integer;
  StatoPagamento: TEnStatoPagamento;

  function getWhere: string;
  begin
    Result   := IfThen(strWhere <> '', strWhere, ' AND ');
    strWhere := '';
  end;

  function getStatoPagamentoSQL(AStatoPagamento: TEnStatoPagamento): string;
  begin
    case AStatoPagamento of
      spPagati:
        Result := strWhere + 'STATO = ''P''';
      spPagatiOggi:
        Result := strWhere + 'STATO = ''P'' AND DATA_PAG = ''' + DtoSql(Date) + '''';
      spInsoluti:
        Result := strWhere + 'STATO <> ''P''';
      spInsoluti30gg:
        Result := strWhere + 'STATO <> ''P'' AND DECORRENZA <= ' + QuotedStr(DtoSql(IncDay(Date, -30)));
      spInsoluti45gg:
        Result := strWhere + 'STATO <> ''P'' AND DECORRENZA <= ' + QuotedStr(DtoSql(IncDay(Date, -45)));
    end;

  end;

begin
  strWhere := 'WHERE ';
  Qtitoli.Close;
  Qtitoli.SQL.Clear;
  Qtitoli.SQL.Add('SELECT SCA.STATO STATO,  SCA.N_POLIZZA AS N_POLIZZA, SCA.DENOMINAZ, SCA.DECORRENZA DECORRENZA,');
  Qtitoli.SQL.Add('ROUND(SCA.LORDO,2) LORDO,');

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    if UniMainModule.UtenteSubPromoter > 0 then
      Qtitoli.SQL.Add('ROUND(SCA.PROVVIGIONI / 100*' + floatToStr(UniMainModule.DMdatiAge.get_provv_subCollab) +
        ',2) AS PROVVIGIONI,')
    else
      Qtitoli.SQL.Add('ROUND(SCA.PROVVIGIONI / 100*' + floatToStr(UniMainModule.DMdatiAge.get_provv_collab) +
        ',2) AS PROVVIGIONI,');
  end
  else
    Qtitoli.SQL.Add('ROUND(SCA.PROVVIGIONI, 2) PROVVIGIONI,');

  Qtitoli.SQL.Add
    ('SCA.DATA_PAG "DATA PAG.", SCA.FRAZIONAM FRAZ, SCA.COD_SCADENZA, SCA.DATA_PAG, SCA.RIF_COD_COMPAGNIA,');
  Qtitoli.SQL.Add('POL.FATTA_DA, COD_FC_SUB_AGE,');
  Qtitoli.SQL.Add('SCA.TIPO_TITOLO, SCA.NETTO, SCA.TASSE, RIF_COD_POLIZZA,');
  Qtitoli.SQL.Add('SCA.DATA_PAG_CCP, SCA.OLD_COMPAGNIA, POL.INPDF, COMP.CODICE_AGENZIA,');
  Qtitoli.SQL.Add('POL.RIF_TIPO_POL AS RIF_TIPO_POLIZZA, TPOL.DESCRIZIONE AS TIPO_POLIZZA, PROVVIGIONI_SUB, DESCRIZ, ');
  Qtitoli.SQL.Add('PRO.SIGLA AS SIGLASUBAGE, SPRO.SIGLA AS SIGLASUBCOLL');
  Qtitoli.SQL.Add('FROM SCADENZE SCA LEFT JOIN POLIZZE POL ON COD_POLIZZA = RIF_COD_POLIZZA');
  Qtitoli.SQL.Add('LEFT OUTER JOIN COMPAGNI COMP ON COMP.COD_COMPAGNIA = SCA.RIF_COD_COMPAGNIA');
  Qtitoli.SQL.Add('LEFT JOIN CLIENTI CLI ON CLI.COD_CLIENTE = POL.COD_CLI');
  Qtitoli.SQL.Add('LEFT JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL');
  Qtitoli.SQL.Add('LEFT JOIN PRODUTT PRO  ON PRO.COD_PRODUTTORE = CLI.PROMOTER');
  Qtitoli.SQL.Add('LEFT JOIN PRODUTT SPRO ON SPRO.COD_PRODUTTORE = CLI.SUB_PROMOTER');

  if codCompagnia > 0 then
    Qtitoli.SQL.Add(getWhere + ' SCA.RIF_COD_COMPAGNIA = ' + inttostr(codCompagnia));

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    // filtra solo le sue quietanze
    Qtitoli.SQL.Add(getWhere + ' CLI.PROMOTER = ' + UniMainModule.utentePromoter.ToString);

    if UniMainModule.UtenteSubPromoter > 0 then
      Qtitoli.SQL.Add('AND CLI.SUB_PROMOTER = ' + UniMainModule.UtenteSubPromoter.ToString);
    // vecchio filtro, sostituito il 18/06/2020
    // Qtitoli.SQL.Add(strWhere + 'pol.fatta_da =' + inttostr(UniMainModule.utentePromoter));

    // metti il selettore per escludere i titoli gi� resocontati all'agenzia !!!!
    Qtitoli.SQL.Add('AND ((COD_FC_SUB_AGE IS NULL) OR (COD_FC_SUB_AGE = 0)) ');
  end;

  if AParametersList <> nil then
    for i := 0 to AParametersList.Count - 1 do
    begin
      if AParametersList.Names[i] = 'StatoPagamento' then
      begin
        Qtitoli.SQL.Add(getWhere + getStatoPagamentoSQL
          (TEnStatoPagamento(strtoint(AParametersList.Values['StatoPagamento']))));
      end
      else
        Qtitoli.SQL.Add(getWhere + AParametersList.Names[i] + ' = ' +
          QuotedStr(AParametersList.Values[AParametersList.Names[i]]))
    end;

  Qtitoli.SQL.Add('ORDER BY ' + ordFieldName + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  Qtitoli.Open;

end;

procedure TDMdatiAgeTitoli.estremi_incasso(cod_scadenza: integer; importo: Currency;
  data_operazione, data_valuta, data_titolo: TdateTime; mezzo_pagamento, estremi1, estremi2, estremi3, banca,
  descrizione, in_sospeso, note, emesso_da, n_polizza, sospeso_broker: string);
begin
  // Qins_estremi_pag.Close;
  Qins_estremi_pag.ParamByName('TIPO_MOVIMENTO').AsString := 'E'; // oppure 'U'
  Qins_estremi_pag.ParamByName('DATA_OPERAZIONE').AsDateTime := data_operazione;
  Qins_estremi_pag.ParamByName('DATA_VALUTA').AsDateTime   := data_valuta;
  Qins_estremi_pag.ParamByName('IMPORTO').AsCurrency       := importo;
  Qins_estremi_pag.ParamByName('MEZZO_PAGAMENTO').AsString := mezzo_pagamento;
  Qins_estremi_pag.ParamByName('ESTREMI1').AsString        := estremi1;
  Qins_estremi_pag.ParamByName('ESTREMI2').AsString        := estremi2;
  Qins_estremi_pag.ParamByName('ESTREMI3').AsString        := estremi3;
  Qins_estremi_pag.ParamByName('BANCA').AsString           := banca;
  Qins_estremi_pag.ParamByName('DESCRIZIONE').AsString     := descrizione;
  Qins_estremi_pag.ParamByName('IN_SOSPESO').AsString      := in_sospeso;
  Qins_estremi_pag.ParamByName('NOTE').AsString            := note;
  Qins_estremi_pag.ParamByName('DATA_TITOLO').AsDateTime   := data_titolo;
  Qins_estremi_pag.ParamByName('RIF_SCADENZA').asInteger   := cod_scadenza;
  Qins_estremi_pag.ParamByName('RIF_POLIZZA').asInteger    := 0; // rif_polizza;
  Qins_estremi_pag.ParamByName('RIF_CLIENTE').asInteger    := 0; // rif_cliente;

  Qins_estremi_pag.ParamByName('EMESSO_DA').AsString      := emesso_da;
  Qins_estremi_pag.ParamByName('RIF_COMPAGNIA').asInteger := 0; // rif_compagnia;
  // Qins_estremi_pag.ParamByName('DATA_PRESENTAZIONE').AsDateTime:=
  // Qins_estremi_pag.ParamByName('DA_CASSA
  // Qins_estremi_pag.ParamByName('A_CASSA
  Qins_estremi_pag.ParamByName('OPERATORE').asInteger := UniMainModule.utenteCodice;

  Qins_estremi_pag.ParamByName('N_POLIZZA').AsString := n_polizza;

  Qins_estremi_pag.ParamByName('RIF_PROMOTER').asInteger  := UniMainModule.utentePromoter;
  Qins_estremi_pag.ParamByName('sospeso_broker').AsString := sospeso_broker;
  // Qins_estremi_pag.ParamByName('data_chiusura_sospeso').AsDateTime:=
  // Qins_estremi_pag.ParamByName('descrizione_chiusura_sospeso').AsString:=
  Qins_estremi_pag.ParamByName('DT_LAST_MOD').AsDateTime   := Date;
  Qins_estremi_pag.ParamByName('ID_GEN_SLP_AGE').asInteger := strtoint(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

  Qins_estremi_pag.ExecSQL;

end;

function TDMdatiAgeTitoli.incassaTitolo(cod_scadenza: integer; provvigioni: Currency; descriz: string;
  data_pag, data_pag_ccp: TdateTime; stato: string; incassata_da: integer; progressivo: integer;
  tipo_titolo: string): Boolean;
var desc: string;

  procedure RefreshTitoli;
  begin
    Qtitoli.DisableControls;
    try
      Qtitoli.Close;
      Qtitoli.Open;
      Qtitoli.Locate('COD_SCADENZA', cod_scadenza, []);
    finally
      Qtitoli.EnableControls;
    end;
  end;

begin
  // incassa il titolo indicato

  try
     QincassaTitolo.close;
     QincassaTitolo.ParamByName('descriz').AsString        := Copy(descriz,1,30);
     QincassaTitolo.ParamByName('stato').AsString          := stato;
     QincassaTitolo.ParamByName('provvigioni').AsCurrency  := provvigioni;
     QincassaTitolo.ParamByName('cod_scadenza').asInteger  := cod_scadenza;
     QincassaTitolo.ParamByName('incassata_da').asInteger  := incassata_da;
     QincassaTitolo.ParamByName('progressivo').asInteger   := progressivo;
     QincassaTitolo.ParamByName('data_pag').AsDateTime     := data_pag;
     QincassaTitolo.ParamByName('data_pag_ccp').AsDateTime := data_pag_ccp;
     QincassaTitolo.ParamByName('data_reg_pag').AsDateTime := Date;
     QincassaTitolo.ExecSQL;

     QFindTitolo.Close;
     QFindTitolo.ParamByName('cod_scadenza').AsInteger:=cod_scadenza;
     QFindTitolo.Open;

     desc:=QFindTitolo.FieldByName('n_polizza').AsString +' ' + QFindTitolo.FieldByName('decorrenza').AsString
           + ' ' + QFindTitolo.FieldByName('tipo_titolo').AsString;

     if tipo_titolo = '20' then
       UniMainModule.traccia('REGPAGSCA', cod_scadenza, 'SCADENZE', desc)
     else
       if tipo_titolo = '10' then
         UniMainModule.traccia('REGPAGP10', UniMainModule.codScadenza, 'SCADENZE', desc)
       else
         if tipo_titolo = '11' then
           UniMainModule.traccia('REGPAGP11', UniMainModule.codScadenza, 'SCADENZE', desc)
         else
           if tipo_titolo = '12' then
             UniMainModule.traccia('REGPAGA12', UniMainModule.codScadenza, 'SCADENZE', desc)
           else
             if tipo_titolo = '15' then
               UniMainModule.traccia('REGPAGA15', UniMainModule.codScadenza, 'SCADENZE', desc)
             else
               if tipo_titolo = '50' then
                 UniMainModule.traccia('REGPAGRIL', UniMainModule.codScadenza, 'SCADENZE', desc);
     RefreshTitoli;
     Result := True;
  except
     Result := false;
  end;
end;

procedure TDMdatiAgeTitoli.new_titolo(cod_polizza: integer);
var
  num_polizza: string;
begin

  // Qnew_titolo.ParamByName('DENOMINAZ').AsString := contraente;
  // Qnew_titolo.ParamByName('RAMO').AsInteger     := ramo;
  // Qnew_titolo.ParamByName('DESCRIZ').AsString   := '';
  // Qnew_titolo.ParamByName('FRAZIONAM').AsString := frazionam;
  //
  // Qnew_titolo.ParamByName('LORDO').AsCurrency       := lordo;
  // Qnew_titolo.ParamByName('provvigioni').AsCurrency := provvigioni;
  //
  // Qnew_titolo.ParamByName('RATA').AsDateTime             := rata;
  // Qnew_titolo.ParamByName('COD_CLI').AsString            := cod_cli;
  // Qnew_titolo.ParamByName('N_POLIZZA').AsString          := num_polizza;
  // Qnew_titolo.ParamByName('netto').AsCurrency            := netto;
  // Qnew_titolo.ParamByName('ptasse').AsCurrency           := ptasse;
  // Qnew_titolo.ParamByName('tasse').AsCurrency            := tasse;
  // Qnew_titolo.ParamByName('RIF_COD_COMPAGNIA').AsInteger := rif_cod_compagnia;
  // Qnew_titolo.ParamByName('RIF_COD_POLIZZA').AsInteger   := rif_cod_polizza;
  // Qnew_titolo.ParamByName('inc_indice').AsCurrency       := inc_indice;
  // Qnew_titolo.ParamByName('decorrenza').AsDateTime       := decorrenza;
  // Qnew_titolo.ParamByName('scadenza').AsDateTime         := scadenza;
  // Qnew_titolo.ParamByName('TIPO_TITOLO').AsString        := tipo_titolo;
  // Qnew_titolo.ParamByName('valuta').AsString             := valuta;
  // Qnew_titolo.ParamByName('RIF_CLIENTE').AsInteger       := rif_cliente;
  // Qnew_titolo.ParamByName('INCASSATA_DA').AsInteger      := incassata_da;
  // Qnew_titolo.ParamByName('dt_last_mod').AsDateTime      := now;
  // Qnew_titolo.ParamByName('id_gen_slp_age').AsInteger    := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);
  // Qnew_titolo.ExecSQL;
  if UniMainModule.DMdatiAge.FindPolizza(cod_polizza, num_polizza) then
  begin
    // UniMainModule.DBaccessoBase.StartTransaction;
    if True then // UniMainModule.operazione='INS' then
    begin
      QTitoloEdit.FieldByName('DENOMINAZ').AsString := UniMainModule.DMdatiAge.QPolizze.FieldByName
        ('CONTRAENTE').AsString;
      QTitoloEdit.FieldByName('RAMO').asInteger := UniMainModule.DMdatiAge.QPolizze.FieldByName('RIF_COD_RAMO')
        .asInteger;
      QTitoloEdit.FieldByName('DESCRIZ').AsString   := '';
      QTitoloEdit.FieldByName('FRAZIONAM').AsString :=
        normalizza_fraz_char(UniMainModule.DMdatiAge.QPolizze.FieldByName('FRAZIONAM').AsString);

      QTitoloEdit.FieldByName('LORDO').AsCurrency := roundTo(QTitoloEdit.FieldByName('LORDO').AsCurrency, -2);
      // QTitoloEdit.FieldByName('provvigioni').AsCurrency := provvigioni;
      QTitoloEdit.FieldByName('ptasse').AsCurrency := UniMainModule.DMdatiAge.QPolizze.FieldByName('ptasse').AsCurrency;
      QTitoloEdit.FieldByName('NETTO').AsCurrency :=
        roundTo((QTitoloEdit.FieldByName('NETTO').AsCurrency /
        (1 + (UniMainModule.DMdatiAge.QPolizze.FieldByName('ptasse').AsCurrency / 100))), -2);
      QTitoloEdit.FieldByName('TASSE').AsCurrency := QTitoloEdit.FieldByName('LORDO').AsCurrency -
        QTitoloEdit.FieldByName('NETTO').AsCurrency;

      QTitoloEdit.FieldByName('RATA').AsDateTime       := QTitoloEdit.FieldByName('RATA').AsDateTime;
      QTitoloEdit.FieldByName('decorrenza').AsDateTime := QTitoloEdit.FieldByName('RATA').AsDateTime;
      QTitoloEdit.FieldByName('SCADENZA').AsDateTime   := IncMonth(QTitoloEdit.FieldByName('SCADENZA').AsDateTime,
        12 div fraz_to_num(QTitoloEdit.FieldByName('FRAZIONAM').AsString));

      QTitoloEdit.FieldByName('COD_CLI').AsString := UniMainModule.DMdatiAge.QPolizze.FieldByName('COD_CLI').AsString;
      // ?????????????????????????????????????????????????????????
      { TODO -oAngelo M -cDomande : Come mai mette il cod_cli sia nel cod_cli che nel rif_cli ??? }
      QTitoloEdit.FieldByName('RIF_CLIENTE').asInteger := UniMainModule.DMdatiAge.QPolizze.FieldByName('COD_CLI')
        .asInteger;

      QTitoloEdit.FieldByName('N_POLIZZA').AsString := UniMainModule.DMdatiAge.QPolizze.FieldByName
        ('N_POLIZZA').AsString;

      QTitoloEdit.FieldByName('RIF_COD_COMPAGNIA').asInteger := UniMainModule.DMdatiAge.QPolizze.FieldByName
        ('RIF_COMPAGNIA').asInteger;
      QTitoloEdit.FieldByName('RIF_COD_POLIZZA').asInteger := UniMainModule.DMdatiAge.QPolizze.FieldByName
        ('COD_POLIZZA').asInteger;
      QTitoloEdit.FieldByName('INC_INDICE').AsCurrency := 0;

      if QTitoloEdit.FieldByName('TIPO_TITOLO').AsString = '20' then
        QTitoloEdit.FieldByName('VALUTA').AsString := 'r'
      else
        QTitoloEdit.FieldByName('VALUTA').AsString := 'E';

      if (QTitoloEdit.FieldByName('TIPO_TITOLO').AsString = '15') and (QTitoloEdit.FieldByName('LORDO').AsCurrency = 0)
      then
        QTitoloEdit.FieldByName('TIPO_TITOLO').AsString := '12';

      // QTitoloEdit.FieldByName('INCASSATA_DA').AsInteger   := incassata_da;
      QTitoloEdit.FieldByName('DT_LAST_MOD').AsDateTime   := now;
      QTitoloEdit.FieldByName('ID_GEN_SLP_AGE').asInteger := strtoint(UniMainModule.DMdatiAge.SLPdati_age.cod_int);
    end;
    // QTitoloEdit.Post;
  end;
end;

procedure TDMdatiAgeTitoli.PosizionaQuery(AValue: TValue);
begin
  QTitoloEdit.Close;
  QTitoloEdit.ParamByName('COD_SCADENZA').asInteger := AValue.asInteger;
  QTitoloEdit.Open;
end;

procedure TDMdatiAgeTitoli.update_titolo(imponibile: Currency; premio_lordo: Currency; provvigioni: Currency;
  imposte: Currency; provvigioni_subagente: Currency; cod_scadenza: integer);
begin
  Qupd_scadenze.Close;
  Qupd_scadenze.ParamByName('IMPONIBILE').AsCurrency      := imponibile;
  Qupd_scadenze.ParamByName('PREMIO_LORDO').AsCurrency    := premio_lordo;
  Qupd_scadenze.ParamByName('PROVVIGIONI').AsCurrency     := provvigioni;
  Qupd_scadenze.ParamByName('IMPOSTE').AsCurrency         := imposte;
  Qupd_scadenze.ParamByName('PROVVIGIONI_SUB').AsCurrency := provvigioni_subagente;
  Qupd_scadenze.ParamByName('cod_scadenza').asInteger     := cod_scadenza;
  Qupd_scadenze.ExecSQL;
end;

procedure TDMdatiAgeTitoli.prepara_query_incassi(dal, al: TdateTime);
var
  TXTsql: TStringList;
begin

  TXTsql          := TStringList.Create;
  Qincassi.active := false;
  Qincassi.SQL.Clear;

  TXTsql.Add('select');
  TXTsql.Add
    ('      scadenze.progressivo, scadenze.decorrenza, scadenze.data_pag, scadenze.n_polizza,scadenze.denominaz,');
  TXTsql.Add('      scadenze.lordo lordo, scadenze.provvigioni, ');

  TXTsql.Add('      compagni.nome, compagni.codice_agenzia, clienti.promoter, scadenze.cod_scadenza, ' +
    'compagni.cod_compagnia , tipo_titolo, valuta, data_reg_pag, produtt.sigla as sigla_prod, scadenze.provvigioni_sub, polizze.inpdf');
  TXTsql.Add('   from');
  TXTsql.Add('      scadenze');
  TXTsql.Add('   LEFT OUTER JOIN compagni ON compagni.cod_compagnia = scadenze.rif_cod_compagnia');
  TXTsql.Add('   LEFT OUTER JOIN polizze  ON polizze.cod_polizza = scadenze.rif_cod_polizza');
  TXTsql.Add('   LEFT OUTER JOIN clienti  ON clienti.cod_cliente = polizze.cod_cli');
  TXTsql.Add('   LEFT OUTER JOIN appoggio ON polizze.appoggiata_a = appoggio.cod_appoggio');
  TXTsql.Add('   LEFT OUTER JOIN produtt ON clienti.promoter = produtt.cod_produttore');
  TXTsql.Add('   where');

  // filtro su insolute o meno
  if false then // (RGstampe.ItemIndex=5) or (RGstampe.ItemIndex=2) then
  begin
    TXTsql.Add('      scadenze.stato<>''P''');
    // filtro sul periodo ...
    TXTsql.Add('and scadenze.decorrenza>=''' + DtoSql(dal) + ''' and scadenze.decorrenza<=''' + DtoSql(al) + '''');
  end
  else
  begin
    // TXTsql.Add('      scadenze.stato=''P''');
    // filtro sul periodo ...
    TXTsql.Add(' scadenze.data_pag>=''' + DtoSql(dal) + ''' and scadenze.data_pag<= ''' + DtoSql(al) + '''');
  end;

  TXTsql.Add('   order by scadenze.data_pag, scadenze.progressivo');

  Qincassi.SQL := TXTsql;
  Qincassi.Open;

end;

procedure TDMdatiAgeTitoli.QTitoloEditBeforePost(DataSet: TDataSet);
var
  l_decorrenza: TdateTime;

  function isDuplicateTitolo(NomeTabella: string; var decorrenza: TdateTime): Boolean;
  begin
    QFindTitolo.Close;
    QFindTitolo.SQL.Clear;
    QFindTitolo.SQL.Append('SELECT COUNT(*) AS CTR FROM ' + NomeTabella);
    QFindTitolo.SQL.Append('WHERE N_POLIZZA = :N_POLIZZA');
    QFindTitolo.SQL.Append('AND DECORRENZA = :DECORRENZA');
    QFindTitolo.SQL.Append('AND TIPO_TITOLO = :TIPO_TITOLO');
    QFindTitolo.ParamByName('N_POLIZZA').AsString   := QTitoloEdit.FieldByName('N_POLIZZA').AsString;
    QFindTitolo.ParamByName('DECORRENZA').AsDate    := QTitoloEdit.FieldByName('DECORRENZA').AsDateTime;
    QFindTitolo.ParamByName('TIPO_TITOLO').AsString := QTitoloEdit.FieldByName('TIPO_TITOLO').AsString;
    QFindTitolo.Open;
    Result     := QFindTitolo.FieldByName('CTR').asInteger > 0;
    decorrenza := QTitoloEdit.FieldByName('DECORRENZA').AsDateTime;
    QFindTitolo.Close;
  end;

begin
  if (QTitoloEdit.FieldByName('TIPO_TITOLO').AsString = '20') and (UniMainModule.operazione = 'INS') then
  begin
    if isDuplicateTitolo('SCADENZE', l_decorrenza) then
      raise ETitoloError.Create(MSG_TITOLO_ESISTENTE);

    if isDuplicateTitolo('S_SCADENZE', l_decorrenza) then
      raise ETitoloError.Create(Format(MSG__TITOLO_IN_STORICO, [dtos(l_decorrenza)]));
  end;

end;

function TDMdatiAgeTitoli.TitoloIsAppendice: Boolean;
begin
  Result := Qtitoli.FieldByName('TIPO_TITOLO').AsString = '15';
end;

function TDMdatiAgeTitoli.TitoloIsAppendice12: Boolean;
begin
  Result := Qtitoli.FieldByName('TIPO_TITOLO').AsString = '12';
end;

function TDMdatiAgeTitoli.TitoloIsAppendiceDIR: Boolean;
begin
  Result := (Qtitoli.FieldByName('TIPO_TITOLO').AsString = '15') and
    (alltrim(Qtitoli.FieldByName('OLD_COMPAGNIA').AsString) > '');
end;

function TDMdatiAgeTitoli.TitoloIsPolizza: Boolean;
begin
  Result := (Qtitoli.FieldByName('TIPO_TITOLO').AsString = '11') or
    (Qtitoli.FieldByName('TIPO_TITOLO').AsString = '10');
end;

function TDMdatiAgeTitoli.TitoloIsPagato: Boolean;
begin
  Result := not Qtitoli.FieldByName('DATA_PAG').IsNull;
end;

function TDMdatiAgeTitoli.TitoloIsQuietanza: Boolean;
begin
  Result := Qtitoli.FieldByName('TIPO_TITOLO').AsString >= '20';
end;

function TDMdatiAgeTitoli.TitoloIsScadenza: Boolean;
begin
  Result := Qtitoli.FieldByName('TIPO_TITOLO').asInteger > 11;
end;

function TDMdatiAgeTitoli.TitoloIsSLP: Boolean;
begin
  Result := Qtitoli.FieldByName('RIF_COD_COMPAGNIA').asInteger = 1;
end;

function TDMdatiAgeTitoli.titolo_pagato(rif_cod_compagnia: integer; n_polizza: string; rata: TdateTime;
  tipo_titolo: string): integer;
begin
  // true se il titolo esite ed � pagato
  // -1 = non esiste
  // 0= esiste in scadenze ma � non pagato
  // 1= esiste in scadenze � pagato
  // 2= pagato su fc gi� chiuso (trovato in s_scadenze)
  if trova_scadenza(rif_cod_compagnia, n_polizza, rata, tipo_titolo) then
  begin
    if Qscadenze.FieldByName('stato').AsString = 'P' then
      Result := 1
    else
      Result := 0

  end
  else
  begin
    // cerca nello storico dei pagati !!!
    if trova_scadenza_storico(rif_cod_compagnia, n_polizza, rata, tipo_titolo) then
    begin
      if QscadenzeStorico.FieldByName('stato').AsString = 'P' then
        Result := 2
      else
        Result := 0;
    end
    else
      Result := -1;
  end;

end;

function TDMdatiAgeTitoli.trova_scadenza(rif_cod_compagnia: integer; n_polizza: string; rata: TdateTime;
  tipo_titolo: string): Boolean;
begin
  // true se il titolo indicato esiste tra le scadenze vive
  Qscadenze.Close;
  Qscadenze.ParamByName('rif_cod_compagnia').asInteger := rif_cod_compagnia;
  Qscadenze.ParamByName('n_polizza').AsString          := n_polizza;
  Qscadenze.ParamByName('rata').AsDateTime             := rata;
  Qscadenze.ParamByName('tipo_titolo').AsString        := tipo_titolo;
  Qscadenze.Open;
  Result := True;
  if Qscadenze.isempty then
    Result := false;

end;

function TDMdatiAgeTitoli.trova_scadenza_storico(rif_cod_compagnia: integer; n_polizza: string; rata: TdateTime;
  tipo_titolo: string): Boolean;
begin
  // true se il titolo indicato esiste tra le scadenze storicizzain in s_scadenze.dat
  QscadenzeStorico.Close;
  QscadenzeStorico.ParamByName('rif_cod_compagnia').asInteger := rif_cod_compagnia;
  QscadenzeStorico.ParamByName('n_polizza').AsString := n_polizza;
  QscadenzeStorico.ParamByName('rata').AsDateTime      := rata;
  QscadenzeStorico.ParamByName('tipo_titolo').AsString := tipo_titolo;
  QscadenzeStorico.Open;
  Result := True;
  if QscadenzeStorico.isempty then
    Result := false;
end;

function TDMdatiAgeTitoli.registraScadenza(Qpolizza: TDBISAMQuery; const AppRec: TShowAppendiceRec): integer;

  function getTipoTitolo: string;
  begin
    if AppRec.tipo_titolo > '' then
      Result := AppRec.tipo_titolo
    else
      Result := IfThen(Qpolizza.FieldByName('N_POLIZZA_SOSTITUITA').AsString > '', '11', '10');
  end;

begin
  Qnew_titolo.Close;
  // if (AppRec.tipo_titolo = '12') and (AppRec.NPolizza.isempty) and (AppRec.CodCliente > 0) then
  // mb 11112020
  if (AppRec.tipo_titolo = '12') and (AppRec.tipoApp='CI') and (AppRec.CodCliente > 0) then
  begin
    Qnew_titolo.ParamByName('DENOMINAZ').AsString          := AppRec.denominazione;
    Qnew_titolo.ParamByName('RAMO').asInteger              := 0;
    Qnew_titolo.ParamByName('DESCRIZ').AsString            := 'APP. CAMBIO INDIRIZZO';
    Qnew_titolo.ParamByName('FRAZIONAM').AsString          := '';
    Qnew_titolo.ParamByName('LORDO').AsCurrency            := AppRec.importo;
    Qnew_titolo.ParamByName('RATA').AsDateTime             := AppRec.DataAppendice;
    Qnew_titolo.ParamByName('COD_CLI').AsString            := AppRec.CodCliente.ToString;
    Qnew_titolo.ParamByName('N_POLIZZA').AsString          := Apprec.NPolizza ;
    Qnew_titolo.ParamByName('PROVVIGIONI').AsCurrency      := 0;
    Qnew_titolo.ParamByName('NETTO').AsCurrency            := 0;
    Qnew_titolo.ParamByName('PTASSE').AsCurrency           := 0;
    Qnew_titolo.ParamByName('TASSE').AsCurrency            := 0;
    Qnew_titolo.ParamByName('RIF_COD_COMPAGNIA').asInteger := AppRec.codCompagnia;
    Qnew_titolo.ParamByName('RIF_COD_POLIZZA').asInteger   := AppRec.CodPolizza ;
    Qnew_titolo.ParamByName('INC_INDICE').AsCurrency       := 0;
    Qnew_titolo.ParamByName('DECORRENZA').AsDateTime       := AppRec.DataAppendice;
    Qnew_titolo.ParamByName('SCADENZA').AsDateTime         := AppRec.DataAppendice;
    Qnew_titolo.ParamByName('TIPO_TITOLO').AsString        := AppRec.tipo_titolo;
    Qnew_titolo.ParamByName('VALUTA').AsString             := 'E';
    Qnew_titolo.ParamByName('RIF_CLIENTE').asInteger       := AppRec.CodCliente;
    Qnew_titolo.ParamByName('INCASSATA_DA').asInteger      := 0;
  end
  else
  begin
    Qnew_titolo.ParamByName('DENOMINAZ').AsString          := Qpolizza.FieldByName('CONTRAENTE').AsString;
    Qnew_titolo.ParamByName('RAMO').asInteger              := Qpolizza.FieldByName('RIF_COD_RAMO').asInteger;
    Qnew_titolo.ParamByName('DESCRIZ').AsString            := '';
    Qnew_titolo.ParamByName('FRAZIONAM').AsString          := Qpolizza.FieldByName('FRAZIONAM').AsString;
    Qnew_titolo.ParamByName('LORDO').AsCurrency            := AppRec.importo;
    Qnew_titolo.ParamByName('RATA').AsDateTime             := AppRec.DataAppendice;
    Qnew_titolo.ParamByName('COD_CLI').AsString            := '';
    Qnew_titolo.ParamByName('N_POLIZZA').AsString          := Qpolizza.FieldByName('N_POLIZZA').AsString;
    Qnew_titolo.ParamByName('PROVVIGIONI').AsCurrency      := (AppRec.importo - AppRec.tasse) / 100 * 30;
    Qnew_titolo.ParamByName('NETTO').AsCurrency            := AppRec.importo - AppRec.tasse;
    Qnew_titolo.ParamByName('PTASSE').AsCurrency           := AppRec.ptasse;
    Qnew_titolo.ParamByName('TASSE').AsCurrency            := AppRec.tasse;
    Qnew_titolo.ParamByName('RIF_COD_COMPAGNIA').asInteger := Qpolizza.FieldByName('RIF_COMPAGNIA').asInteger;
    Qnew_titolo.ParamByName('RIF_COD_POLIZZA').asInteger   := Qpolizza.FieldByName('COD_POLIZZA').asInteger;
    Qnew_titolo.ParamByName('INC_INDICE').AsCurrency       := 0;
    Qnew_titolo.ParamByName('DECORRENZA').AsDateTime       := AppRec.DataAppendice;
    Qnew_titolo.ParamByName('SCADENZA').AsDateTime         := AppRec.DataAppendice;
    Qnew_titolo.ParamByName('TIPO_TITOLO').AsString        := getTipoTitolo;
    Qnew_titolo.ParamByName('VALUTA').AsString             := 'E';
    Qnew_titolo.ParamByName('RIF_CLIENTE').asInteger       := Qpolizza.FieldByName('COD_CLI').asInteger;
    Qnew_titolo.ParamByName('INCASSATA_DA').asInteger      := 0;
  end;

  Qnew_titolo.ParamByName('DT_LAST_MOD').AsDateTime   := now;
  Qnew_titolo.ParamByName('ID_GEN_SLP_AGE').asInteger := strtoint(UniMainModule.DMdatiAge.SLPdati_age.cod_int);

  Qnew_titolo.ExecSQL;

  // cerca il codice del titolo appena inserito !!!!
  QAutoincScadenza.Close;
  QAutoincScadenza.Open;
  Result := QAutoincScadenza.FieldByName('CODSCAD').asInteger;
  // cerca in base a data, n_poilzza, tipo_titolo e importo
  // QFindTitoloBis.Close;
  // QFindTitoloBis.ParamByName('N_POLIZZA').AsString   := Qpolizza.FieldByName('N_POLIZZA').AsString;
  // QFindTitoloBis.ParamByName('TIPO_TITOLO').AsString := getTipoTitolo;
  // QFindTitoloBis.ParamByName('RATA').asDateTime      := Qpolizza.FieldByName('DECORRENZA').asDateTime;
  // QFindTitoloBis.ParamByName('LORDO').AsCurrency     := Qpolizza.FieldByName('TOTALE2').AsCurrency;
  // QFindTitoloBis.Open;
  // if not  QFindTitoloBis.IsEmpty then
  // Result := QFindTitoloBis.FieldByName('COD_SCADENZA').AsInteger
  // else
  // Result := -1;
end;

function TDMdatiAgeTitoli.quietanza_con_appendice(n_polizza: string; rata: TdateTime;var app_pagata: boolean): Boolean;
begin
  Qquiet_con_appendice.Close;
  Qquiet_con_appendice.ParamByName('n_polizza').AsString := n_polizza;
  Qquiet_con_appendice.ParamByName('rata').AsDateTime    := rata;
  Qquiet_con_appendice.Open;
  if Qquiet_con_appendice.isempty then
    Result := false
  else begin
    if True then
    app_pagata := (Qquiet_con_appendice.fieldbyname('stato').asString = 'P');
    Result := True;
  end;
end;

function TDMdatiAgeTitoli.appendice_pagata(polizza: string; decorrenza: TdateTime; tipo_titolo: string;
  var premio: Currency): Boolean;
begin
  // verifica se l'appendice indicata risulta pagata in scadenze.dat (foglio cassa no ancora chiuso)
  Qappendice_pagata.Close;
  Qappendice_pagata.ParamByName('polizza').AsString      := polizza;
  Qappendice_pagata.ParamByName('decorrenza').AsDateTime := decorrenza;
  Qappendice_pagata.ParamByName('tipo_titolo').AsString  := tipo_titolo;
  Qappendice_pagata.Open;
  premio := 0;
  Result := Qappendice_pagata.RecordCount > 0;
  if Result then
    premio := Qappendice_pagata.FieldByName('lordo').AsCurrency;
  Qappendice_pagata.Close;
end;

procedure TDMdatiAgeTitoli.ExportToExcelAndSend(ReportFileName: string);
var
  FilesFolderPath: string;
  absFileName: string;
begin
  FilesFolderPath         := TPath.Combine(UniServerModule.FilesFolderPath, 'PDF');
  ReportFileName          := ReportFileName + '_' + FormatDateTime('ddmmyyyhhmmss', now);
  absFileName             := TPath.Combine(FilesFolderPath, ChangeFileExt(ReportFileName, '.csv'));
  FDDataMove.TextFileName := absFileName;
  FDDataMove.execute;
  UniSession.SendFile(absFileName);
  Sleep(2000);
  TFile.delete(absFileName);

end;

end.
