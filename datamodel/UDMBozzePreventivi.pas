unit UDMBozzePreventivi;

interface

uses
  SysUtils, Classes, UDMMaster, UQueryEditIntf, UQueryElencoIntf, Data.DB, dbisamtb, UTipoSalvataggioDocumento, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client,  System.Rtti;

type
  TDMBozzePreventivi = class(TDMMaster, IQueryElenco, IQueryEdit)  //(TDMMaster)
    QBozzePreventivi: TDBISAMQuery;
    QPosizionaBozzaPreventivo: TDBISAMQuery;
    fdmtblStatusPolizza: TFDMemTable;
    QSTATUSPOLIZZAID: TStringField;
    QStatusPolizzaDescrizione: TStringField;
    QCancellaBozzePreventivi: TDBISAMQuery;
    QBozzePreventiviCOD_POLIZZA: TAutoIncField;
    QBozzePreventiviDT_PREVENTIVO: TDateTimeField;
    QBozzePreventiviCONTRAENTE: TStringField;
    QBozzePreventiviDECORRENZA: TDateField;
    QBozzePreventiviDESTIPOPOL: TStringField;
    fdmtblPolizzaQBozzePreventiviSTATUS: TStringField;
    QBozzePreventivilordo: TCurrencyField;
    QBozzePreventividescrStatus: TStringField;
    QBozzePreventividescrutente: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 1); override;

    procedure EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
      DirezioneOrdinamento: integer = 1; TipoFiltro: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 0); override;

    // Interfaccia IQueryEdit
    procedure PosizionaQuery(AValue: TValue); override;

    procedure CancellaBozzePreventivi(ACodPolizzaArray: array of TBookmark);
    function PosizionaBozzaPreventivo(CodPolizza: integer): TTipoSalvataggio;
    function getTipoDocFromDescr(const ADescrDoc: string): string;
  end;

function DMBozzePreventivi: TDMBozzePreventivi;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, System.StrUtils;

function DMBozzePreventivi: TDMBozzePreventivi;
begin
  Result := TDMBozzePreventivi(UniMainModule.GetModuleInstance(TDMBozzePreventivi));
end;

{ TDMBozzePreventivi }

procedure TDMBozzePreventivi.CancellaBozzePreventivi(ACodPolizzaArray: array of TBookmark);
var
  bkPolizza: TBookmark;
  virgola: string;
begin
  // fdmtblBozzePreventiviClone.CloneCursor(QBozzePreventivi, True, False);
  QBozzePreventivi.DisableControls;
  virgola := '';
  QCancellaBozzePreventivi.SQL.Clear;
  QCancellaBozzePreventivi.SQL.Append('DELETE FROM SLP_TPOLIZZE WHERE COD_POLIZZA IN (');
  for bkPolizza in ACodPolizzaArray do
  begin
    QBozzePreventivi.GotoBookmark(bkPolizza);
    QCancellaBozzePreventivi.SQL.Append(virgola + QBozzePreventivi.FieldByName('COD_POLIZZA').AsInteger.ToString);
    virgola := ',';
  end;
  QCancellaBozzePreventivi.SQL.Append(')');
  QCancellaBozzePreventivi.ExecSQL;
  try
    QBozzePreventivi.Close;
    QBozzePreventivi.Open;
  finally
    QBozzePreventivi.Open;
    QBozzePreventivi.EnableControls;
  end;
end;

procedure TDMBozzePreventivi.DataModuleCreate(Sender: TObject);
begin
  inherited;
  fdmtblStatusPolizza.CreateDataSet;
  fdmtblStatusPolizza.AppendRecord(['', 'POLIZZA']);
  fdmtblStatusPolizza.AppendRecord(['B', 'BOZZA']);
  fdmtblStatusPolizza.AppendRecord(['F', 'PREVENTIVO']);
  fdmtblStatusPolizza.AppendRecord(['FD', 'PREVENTIVO DIR']);
  fdmtblStatusPolizza.AppendRecord(['P', 'POLIZZA PERFEZIONATA']);
  fdmtblStatusPolizza.AppendRecord(['PD', 'POLIZZA DIREZIONE']);
end;

procedure TDMBozzePreventivi.EseguiQuery(ordFieldName: string; TipoFiltro: integer; ValToSearch: string;
  StatoPagamento, DirezioneOrdinamento, CodCollaboratore, codCompagnia: integer);
begin
  QBozzePreventivi.Close;
  QBozzePreventivi.SQL.Clear;

  // select POL.*, TPOL.DESCRIZIONE DESTIPOPOL from SLP_TPOLIZZE POL
  // left JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL
  // where STATUS in ('B', 'F')
  // --and ID_GEN_SLP_AGE = :codAgenzia

  // QBozzePreventivi.SQL.Add('SELECT POL.DT_PREVENTIVO, POL.CONTRAENTE, POL.DECORRENZA, TPOL.DESCRIZIONE DESTIPOPOL,');
  // QBozzePreventivi.SQL.Add('POL.COD_POLIZZA, POL.STATUS, POL.DATI5 as DESUSER, POL.LORDO, POL.DESCRIZIONE AS DESCRUTENTE,');
  // QBozzePreventivi.SQL.Add('FROM SLP_TPOLIZZE POL');
  // QBozzePreventivi.SQL.Add('LEFT JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL');
  // QBozzePreventivi.SQL.Add('WHERE STATUS IN ('''', ''B'', ''F'', ''P'', ''FD'')');

  // case TEnTipoPolizza(TipoFiltro) of
  // tpViva:
  // QBozzePreventivi.SQL.Add('and stato = ''V'' ');
  // else
  // QBozzePreventivi.SQL.Add('and stato = ''C'' ');
  // end;
  // QBozzePreventivi.SQL.Add('AND POL.RIF_TIPO_POL = ' + UniMainModule.TipoPolizza.ToString);
  QBozzePreventivi.SQL.Add
    ('SELECT POL.COD_POLIZZA,  POL.DT_PREVENTIVO, POL.CONTRAENTE, POL.DECORRENZA, TPOL.DESCRIZIONE DESTIPOPOL,');
  QBozzePreventivi.SQL.Add('POL.STATUS, POL.LORDO, POL.DESCRIZIONE AS DESCRUTENTE,');
  QBozzePreventivi.SQL.Add('CASE  WHEN STATUS = ''''  THEN ''POLIZZA'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''P'' THEN ''POLIZZA PERFEZIONATA'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''B'' THEN ''BOZZA'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''F'' THEN ''PREVENTIVO'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''FD'' THEN ''PREVENTIVO DIR'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''PD'' THEN ''POLIZZA DIR'' ');
  QBozzePreventivi.SQL.Add('END AS DESCRSTATUS');
  QBozzePreventivi.SQL.Add('FROM SLP_TPOLIZZE POL');
  QBozzePreventivi.SQL.Add('LEFT JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL');
  QBozzePreventivi.SQL.Add('WHERE POL.RIF_TIPO_POL = ' + UniMainModule.TipoPolizza.ToString);
  QBozzePreventivi.SQL.Add('AND (not(pol.mem_polizza is null) or (status=''FD'') )');

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    QBozzePreventivi.SQL.Add(' AND fatta_da = ' + UniMainModule.utentePromoter.ToString);
    if UniMainModule.UtenteSubPromoter > 0 then
      QBozzePreventivi.SQL.Add(' AND sub_promoter = ' + UniMainModule.UtenteSubPromoter.ToString);
  end
  else
    if CodCollaboratore > 0 then
      QBozzePreventivi.SQL.Add('AND fatta_da = ' + CodCollaboratore.ToString);

  if ValToSearch > '' then
    QBozzePreventivi.SQL.Add('AND ' + strOrdCol + ' LIKE ''' + ValToSearch + '%''');

  strOrdCol := ordFieldName;
  QBozzePreventivi.SQL.Add('order by ' + strOrdCol + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  QBozzePreventivi.Open;

end;

procedure TDMBozzePreventivi.EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
  DirezioneOrdinamento, TipoFiltro, CodCollaboratore, codCompagnia: integer);
begin
  QBozzePreventivi.Close;
  QBozzePreventivi.SQL.Clear;

  QBozzePreventivi.SQL.Add
    ('SELECT POL.COD_POLIZZA,  POL.DT_PREVENTIVO, POL.CONTRAENTE, POL.DECORRENZA, TPOL.DESCRIZIONE DESTIPOPOL,');
  QBozzePreventivi.SQL.Add('POL.STATUS, POL.LORDO, POL.DESCR_UTENTE AS DESCRUTENTE,');
  QBozzePreventivi.SQL.Add('CASE  WHEN STATUS = ''''  THEN ''POLIZZA'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''P'' THEN ''POLIZZA PERFEZIONATA'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''B'' THEN ''BOZZA'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''F'' THEN ''PREVENTIVO'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''FD'' THEN ''PREVENTIVO DIR'' ');
  QBozzePreventivi.SQL.Add('      WHEN STATUS = ''PD'' THEN ''POLIZZA DIR'' ');
  QBozzePreventivi.SQL.Add('END AS DESCRSTATUS');
  QBozzePreventivi.SQL.Add('FROM SLP_TPOLIZZE POL');
  QBozzePreventivi.SQL.Add('LEFT JOIN TIPO_POL TPOL ON TPOL.COD_TIPO_POL = POL.RIF_TIPO_POL');
  QBozzePreventivi.SQL.Add('WHERE POL.RIF_TIPO_POL = ' + UniMainModule.TipoPolizza.ToString);
  // STATUS IN ('''', ''B'', ''F'', ''P'', ''FD'')');

  if AParametersList <> nil then
    QBozzePreventivi.SQL.Add('AND ' + AParametersList.Names[0] + ' = ' +
      QuotedStr(AParametersList.Values[AParametersList.Names[0]]));
  // case TEnTipoPolizza(TipoFiltro) of
  // tpViva:
  // QBozzePreventivi.SQL.Add('and stato = ''V'' ');
  // else
  // QBozzePreventivi.SQL.Add('and stato = ''C'' ');
  // end;

  // parte per non mostrare le bozze e i preventivi fatti da slpwin visto che non aggiorna i campi mem_polizza
  QBozzePreventivi.SQL.Add('AND (not(pol.mem_polizza is null) or (status=''FD'') or (status=''PD'') )');

  if UniMainModule.DMdatiAge.is_utente_promoter then
  begin
    QBozzePreventivi.SQL.Add('AND FATTA_DA = ' + UniMainModule.utentePromoter.ToString);
    if UniMainModule.UtenteSubPromoter > 0 then
      QBozzePreventivi.SQL.Add('AND SUB_PROMOTER = ' + UniMainModule.UtenteSubPromoter.ToString);
  end
  else
    if CodCollaboratore > 0 then
      QBozzePreventivi.SQL.Add('AND FATTA_DA = ' + CodCollaboratore.ToString);

  if ValToSearch > '' then
    QBozzePreventivi.SQL.Add('AND ' + strOrdCol + ' LIKE ''' + ValToSearch + '%''');

  strOrdCol := ordFieldName;
  QBozzePreventivi.SQL.Add('ORDER BY ' + strOrdCol + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));

  QBozzePreventivi.Open;

end;

function TDMBozzePreventivi.getTipoDocFromDescr(const ADescrDoc: string): string;
begin
  Result := '';
  if fdmtblStatusPolizza.Locate('DESCRIZIONE', ADescrDoc) then
    Result := fdmtblStatusPolizza.FieldByName('ID').AsString;
end;

function TDMBozzePreventivi.PosizionaBozzaPreventivo(CodPolizza: integer): TTipoSalvataggio;

  function GetTipoSalvataggioFromStatus: TTipoSalvataggio;
  var
    lStatus: string;
  begin
    lStatus := QPosizionaBozzaPreventivo.FieldByName('STATUS').AsString;
    if lStatus = 'B' then
      Result := tsBozza
    else
      if lStatus = 'F' then
        Result := tsPreventivo
      else
        if lStatus = 'FD' then
          Result := tsPreventivoDir
        else
          if lStatus = 'PD' then
            Result := tsPolizzaDir
          else
            Result := tsPolizza;

  end;

begin
  QPosizionaBozzaPreventivo.Close;
  QPosizionaBozzaPreventivo.paramByName('COD_POLIZZA').AsInteger := CodPolizza;
  QPosizionaBozzaPreventivo.Open;
  if not QPosizionaBozzaPreventivo.IsEmpty then
    Result := GetTipoSalvataggioFromStatus
  else
    Result := tsNone;

end;

procedure TDMBozzePreventivi.PosizionaQuery(AValue: TValue);
begin
  inherited;
  //
end;

end.
