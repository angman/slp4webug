unit UDMDatiAgePolizzePerf;

interface

uses
  SysUtils, Classes, UDMMaster, Data.DB, dbisamtb, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UdmdatiAge, math;

type
  TDMDatiAgePolizzePerf = class(TDMMaster)
    Qpol_da_perfez: TDBISAMQuery;
    Qpol_da_perfezcod_polizza: TAutoIncField;
    Qpol_da_perfezContraente: TStringField;
    Qpol_da_perfezDecorrenza: TDateField;
    Qpol_da_perfezScadenza: TDateField;
    Qpol_da_perfezFrazionam: TStringField;
    Qpol_da_perfezN_polizza: TStringField;
    Qpol_da_perfezLordo: TCurrencyField;
    Qpol_da_perfezSub_age: TStringField;
    Qpol_da_perfezData_inserimento: TDateField;
    Qpol_da_perfezData_emissione: TDateField;
    Qpol_da_perfezData_perfez: TDateTimeField;
    Qpol_da_perfezTotale2: TCurrencyField;
    Qpol_da_perfezTotale1: TCurrencyField;
    Qpol_da_perfezStatus: TStringField;
    Qpol_da_perfezData_canc: TDateTimeField;
    Qpol_da_perfezRif_tipo_pol: TIntegerField;
    Qpol_da_perfezfatta_da: TIntegerField;
    Qpol_da_perfezCon_appendice: TStringField;
    Qpol_da_perfezApp_report: TBlobField;
    Qpol_da_perfezN_polizza_sostituita: TStringField;
    Qpol_da_perfezInpdf: TStringField;
    Qpol_da_perfezTipo_polizza: TStringField;
    Qpol_da_perfezSigla: TStringField;
    Qpol_da_perfezProvvigioni: TCurrencyField;
    Qpol_da_perfezPromoterLkup: TStringField;
    Qpol_da_perfezEmissLkup: TStringField;
    Qpol_da_perfezTipoEmis: TStringField;
    Qpol_da_perfezFrazionamentoLkup: TStringField;
    Qpol_da_perfezSubAgeLkup: TStringField;
    Qreg_pagamento_pol: TDBISAMQuery;
    Qins_polizza: TDBISAMQuery;
    Qins_assicurato: TDBISAMQuery;
    Qins_link_pol_assic: TDBISAMQuery;
    Qins_garanzie: TDBISAMQuery;
    Qassicurati: TDBISAMQuery;
    Qgaranzie: TDBISAMQuery;
    Qpolizza: TDBISAMQuery;
    QpolizzaPortaf: TDBISAMQuery;
    QassicuratiPortaf: TDBISAMQuery;
    Qcambia_stato: TDBISAMQuery;
    Qpol_da_perfezsub_promoter: TIntegerField;
    Qpol_da_perfezSiglaSubPromoter: TStringField;
    Qcancella_logico: TDBISAMQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0; codCompagnia: Integer=1); override;
    function cerca_pol_da_perf(cod_polizza: integer): Boolean;
    function cerca_assicurati(cod_polizza: integer): Boolean;
    function cerca_garanzie(cod_polizza: integer): Boolean;
    function incassa_polizza(CodTitolo, CodCollab, prog: integer; data_pag, data_pag_ccp: TDateTime): integer;
    procedure cancellaPolizzaDaPerfez(cod_polizza: Integer);
  end;

function DMDatiAgePolizzePerf: TDMDatiAgePolizzePerf;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, System.StrUtils, libreria;

function DMDatiAgePolizzePerf: TDMDatiAgePolizzePerf;
begin
  Result := TDMDatiAgePolizzePerf(UniMainModule.GetModuleInstance(TDMDatiAgePolizzePerf));
end;

{ TDMDatiAgePolizzePerf }


procedure TDMDatiAgePolizzePerf.cancellaPolizzaDaPerfez(cod_polizza: Integer);
var lRecNumber: Integer;
begin
   lRecNumber := ifThen((Qpol_da_perfez.RecNo -1) > 0, Qpol_da_perfez.RecNo -1, 1);
   qcancella_logico.Close;
   qcancella_logico.ParamByName('cod_polizza').AsInteger:=cod_polizza;
   qcancella_logico.ExecSQL;
   PosizionaElenco(Qpol_da_perfez, lRecNumber);
end;


function TDMDatiAgePolizzePerf.cerca_assicurati(cod_polizza: integer): Boolean;
begin
  // cecrca gli assicurati della polizza
  Qassicurati.Close;
  Qassicurati.ParamByName('cod_polizza').AsInteger := cod_polizza;
  Qassicurati.Open;
  Result := not Qassicurati.IsEmpty;
end;

function TDMDatiAgePolizzePerf.cerca_garanzie(cod_polizza: integer): Boolean;
begin
  // cerca le garanzie della polizza
  Qgaranzie.Close;
  Qgaranzie.ParamByName('cod_polizza').AsInteger := cod_polizza;
  Qgaranzie.Open;
  Result := not Qgaranzie.IsEmpty;
end;

function TDMDatiAgePolizzePerf.cerca_pol_da_perf(cod_polizza: integer): Boolean;
begin
  Qpolizza.Close;
  Qpolizza.ParamByName('cod_polizza').AsInteger := cod_polizza;
  Qpolizza.Open;
  Result := not Qpolizza.IsEmpty;
end;

procedure TDMDatiAgePolizzePerf.DataModuleCreate(Sender: TObject);
begin
  inherited;
  LOG('UdmDatiAgePolizzePerf.DataModuleCreate 0');
  {
    fdmtblStatoPolizza.CreateDataSet;
    fdmtblStatoPolizza.Open;
    fdmtblStatoPolizza.AppendRecord([' ', 'Da perfezionare']);
    fdmtblStatoPolizza.AppendRecord(['P', 'Perfezionate']);
    fdmtblStatoPolizza.AppendRecord(['C', 'Cancellate']);
    fdmtblStatoPolizza.AppendRecord(['R', 'Rese']);
  }
end;

procedure TDMDatiAgePolizzePerf.EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 0; CodCollaboratore: integer = 0; codCompagnia: Integer=1);
begin
  Qpol_da_perfez.Close;
  Qpol_da_perfez.SQL.Clear;
  Qpol_da_perfez.SQL.Append('select cod_polizza, contraente, decorrenza, scadenza, frazionam, tipologia as TipoEmis,');
  Qpol_da_perfez.SQL.Append('n_polizza, lordo, sub_age, data_inserimento, data_emissione, data_perfez,');
  Qpol_da_perfez.SQL.Append('totale2, totale1, status, data_perfez, data_canc, rif_tipo_pol, fatta_da, con_appendice,');
  Qpol_da_perfez.SQL.Append
    ('app_report, n_polizza_sostituita, inpdf , tpol.descrizione as tipo_polizza, tpol.sigla, sub_promoter, pol.n_preventivo');
  if UniMainModule.IsUtentePromoter then
    Qpol_da_perfez.SQL.add(', 0.0 as provvigioni ')
  else
    Qpol_da_perfez.SQL.add(', provvigioni ');

  Qpol_da_perfez.SQL.Append('from slp_tpolizze pol');
  Qpol_da_perfez.SQL.Append('left join tipo_pol tpol on tpol.cod_tipo_pol = pol.rif_tipo_pol');
  if StatoPagamento=5 then
     Qpol_da_perfez.SQL.Append('where n_preventivo > ''''  and (status=''FD'') ')
  else
     Qpol_da_perfez.SQL.Append('where n_polizza > '''' ');

  if UniMainModule.IsUtentePromoter then
  begin
    Qpol_da_perfez.SQL.Append('and fatta_da = ' + UniMainModule.utentePromoter.tostring);
    if UniMainModule.UtenteSubPromoter > 0 then
      Qpol_da_perfez.SQL.Append('and sub_promoter = ' + UniMainModule.UtenteSubPromoter.tostring);
  end
  else
    if CodCollaboratore > 0 then
      Qpol_da_perfez.SQL.Append('and fatta_da = ' + CodCollaboratore.tostring);

  case StatoPagamento of
    0:
      Qpol_da_perfez.SQL.Append('and ((status is null) or (status=''PD''))');
    1:
      Qpol_da_perfez.SQL.Append('and status = ''P'' '); // perfezionate
    2:
      Qpol_da_perfez.SQL.Append('and status = ''R'' '); // rese
    3:
      Qpol_da_perfez.SQL.Append('and status = ''C'' '); // cancellate
    4:
      Qpol_da_perfez.SQL.Append('and (status=''PD'')'); // emissione direzione

  else
    // Qpol_da_perfez.SQL.Append('and ((status is null) or (status=''PD'')) ');
  end;

  Qpol_da_perfez.SQL.Append('order by ' + ordFieldName + IfThen(DirezioneOrdinamento = 1, ' ASC', ' DESC'));
  if ordFieldName = 'decorrenza' then
    Qpol_da_perfez.SQL.Append(', ' + 'N_POLIZZA ASC');
  Qpol_da_perfez.Open;
end;

// ================================================================================================================
function TDMDatiAgePolizzePerf.incassa_polizza(CodTitolo, CodCollab, prog: integer; data_pag, data_pag_ccp: TDateTime): integer;
var
  cod_polizza: integer;

  function registra_polizza: integer;
  begin
    // sposta i dati della polizza in polizze.dat
    Qins_polizza.Close;
    Qins_polizza.ParamByName('RIF_TIPO_POL').AsInteger    := Qpolizza.FieldByName('RIF_TIPO_POL').AsInteger;
    Qins_polizza.ParamByName('CONTRAENTE').asString       := Qpolizza.FieldByName('CONTRAENTE').asString;
    Qins_polizza.ParamByName('DECORRENZA').asDateTime     := Qpolizza.FieldByName('DECORRENZA').asDateTime;
    Qins_polizza.ParamByName('SCADENZA').asDateTime       := Qpolizza.FieldByName('SCADENZA').asDateTime;
    Qins_polizza.ParamByName('data_emissione').asDateTime := Qpolizza.FieldByName('data_emissione').asDateTime;
    Qins_polizza.ParamByName('FRAZIONAM').asString        := Qpolizza.FieldByName('FRAZIONAM').asString;
    Qins_polizza.ParamByName('N_POLIZZA').asString        := Qpolizza.FieldByName('N_POLIZZA').asString;
    Qins_polizza.ParamByName('inpdf').asString            := Qpolizza.FieldByName('inpdf').asString;
    Qins_polizza.ParamByName('LORDO').asCurrency          := Qpolizza.FieldByName('LORDO').asCurrency;
    Qins_polizza.ParamByName('netto').asCurrency          := Qpolizza.FieldByName('netto').asCurrency;
    Qins_polizza.ParamByName('ptasse').asCurrency         := Qpolizza.FieldByName('ptasse').asCurrency;
    Qins_polizza.ParamByName('tasse').asCurrency          := Qpolizza.FieldByName('tasse').asCurrency;
    Qins_polizza.ParamByName('accessori').asCurrency      := Qpolizza.FieldByName('accessori').asCurrency;
    Qins_polizza.ParamByName('valuta').asString           := Qpolizza.FieldByName('valuta').asString;
    Qins_polizza.ParamByName('NOTE').asString             := Qpolizza.FieldByName('NOTE').asString;
    Qins_polizza.ParamByName('RIF_COMPAGNIA').AsInteger   := Qpolizza.FieldByName('RIF_COMPAGNIA').AsInteger;
    Qins_polizza.ParamByName('COD_CLI').AsInteger         := Qpolizza.FieldByName('COD_CLI').AsInteger;
    Qins_polizza.ParamByName('FATTA_DA').AsInteger        := Qpolizza.FieldByName('FATTA_DA').AsInteger;
    Qins_polizza.ParamByName('rif_referente').AsInteger   := Qpolizza.FieldByName('rif_referente').AsInteger;
    Qins_polizza.ParamByName('convenzione').AsInteger     := Qpolizza.FieldByName('convenzione').AsInteger;
    Qins_polizza.ParamByName('N_POLIZZA_SOSTITUITA').asString := Qpolizza.FieldByName('N_POLIZZA_SOSTITUITA').asString;
    Qins_polizza.ParamByName('decorrenza').asDateTime     := Qpolizza.FieldByName('decorrenza').asDateTime;
    Qins_polizza.ParamByName('RIF_COD_RAMO').AsInteger    := Qpolizza.FieldByName('RIF_COD_RAMO').AsInteger;
    Qins_polizza.ParamByName('INTERMEDIATA_DA').AsInteger := Qpolizza.FieldByName('INTERMEDIATA_DA').AsInteger;
    Qins_polizza.ParamByName('sub_promoter').AsInteger    := Qpolizza.FieldByName('sub_promoter').AsInteger;

    Qins_polizza.ParamByName('PROVVIGIONI').asCurrency := Qpolizza.FieldByName('PROVVIGIONI').asCurrency;
    Qins_polizza.ParamByName('PERC_PROVVIGIONI').asCurrency := Qpolizza.FieldByName('PERC_PROVVIGIONI').asCurrency;
    Qins_polizza.ParamByName('TIPO_PROVVIGIONI').asString := Qpolizza.FieldByName('TIPO_PROVVIGIONI').asString;
    Qins_polizza.ParamByName('DATA_EMISSIONE').asDateTime := Qpolizza.FieldByName('DATA_EMISSIONE').asDateTime;
    Qins_polizza.ParamByName('RAGIONE_SOC').asString      := Qpolizza.FieldByName('RAGIONE_SOC').asString;
    Qins_polizza.ParamByName('DATI1').asString            := Qpolizza.FieldByName('DATI1').asString;
    Qins_polizza.ParamByName('DATI2').asString            := Qpolizza.FieldByName('DATI2').asString;
    Qins_polizza.ParamByName('DATI3').asString            := Qpolizza.FieldByName('DATI3').asString;
    Qins_polizza.ParamByName('DATI4').asString            := Qpolizza.FieldByName('DATI4').asString;
    Qins_polizza.ParamByName('DATI5').asString            := Qpolizza.FieldByName('DATI5').asString;
    Qins_polizza.ParamByName('SPECIAL').asString          := Qpolizza.FieldByName('SPECIAL').asString;
    Qins_polizza.ParamByName('ORDINE').asString           := Qpolizza.FieldByName('ORDINE').asString;
    Qins_polizza.ParamByName('DATI_VARI').asString        := Qpolizza.FieldByName('DATI_VARI').asString;

    if Qpolizza.FieldByName('indicizzata').asBoolean then
      Qins_polizza.ParamByName('DATA_INI_INDICE').asDateTime := Qpolizza.FieldByName('DATA_EMISSIONE').asDateTime
    else
      Qins_polizza.ParamByName('DATA_INI_INDICE').asDateTime := 0;
    Qins_polizza.ParamByName('DT_last_mod').asDateTime := date;

    Qins_polizza.ParamByName('ID_GEN_SLP_AGE').AsInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);
    Qins_polizza.execsql;

    QpolizzaPortaf.Close;
    QpolizzaPortaf.ParamByName('n_polizza').asString := Qpolizza.FieldByName('N_POLIZZA').asString;
    QpolizzaPortaf.Open;

    Result := QpolizzaPortaf.FieldByName('cod_polizza').AsInteger;
  end;

  procedure registra_assicurati(CpDaPerf: integer);
  begin
    // registra tutti gli assicurati passandolo tra gli assicurati effettivi
    cerca_assicurati(CpDaPerf);
    while (Qassicurati.FieldByName('cod_polizza').AsInteger = CpDaPerf) and (not Qassicurati.eof) do
    begin

      Qins_assicurato.Close;

      Qins_assicurato.ParamByName('oggi').asDateTime       := date;
      Qins_assicurato.ParamByName('tipo').asString         := Qassicurati.FieldByName('tipo').asString;
      Qins_assicurato.ParamByName('cod_polizza').AsInteger := cod_polizza;
      Qins_assicurato.ParamByName('tipo_veicolo').asString := Qassicurati.FieldByName('tipo_veicolo').asString;
      Qins_assicurato.ParamByName('targa').asString        := Qassicurati.FieldByName('targa').asString;
      Qins_assicurato.ParamByName('modello').asString      := Qassicurati.FieldByName('modello').asString;
      Qins_assicurato.ParamByName('marca').asString        := Qassicurati.FieldByName('marca').asString;
      Qins_assicurato.ParamByName('hp_ql').asString        := Qassicurati.FieldByName('hp_ql').asString;
      Qins_assicurato.ParamByName('patente').asString      := Qassicurati.FieldByName('patente').asString;

      Qins_assicurato.ParamByName('categoria_pat').asString := Qassicurati.FieldByName('categoria_pat').asString;
      Qins_assicurato.ParamByName('data_rilascio').asDateTime := Qassicurati.FieldByName('data_rilascio').asDateTime;
      Qins_assicurato.ParamByName('data_scadenza').asDateTime := Qassicurati.FieldByName('data_scadenza').asDateTime;

      Qins_assicurato.ParamByName('denominazione').asString := Qassicurati.FieldByName('denominazione').asString;
      Qins_assicurato.ParamByName('cod_fisc_iva').asString := Qassicurati.FieldByName('cod_fisc_iva').asString;
      Qins_assicurato.ParamByName('indirizzo').asString := Qassicurati.FieldByName('indirizzo').asString;
      Qins_assicurato.ParamByName('citta').asString     := Qassicurati.FieldByName('citta').asString;
      Qins_assicurato.ParamByName('cap').asString       := Qassicurati.FieldByName('cap').asString;
      Qins_assicurato.ParamByName('prov').asString      := Qassicurati.FieldByName('prov').asString;
      Qins_assicurato.ParamByName('note').asString      := Qassicurati.FieldByName('note').asString;
      Qins_assicurato.ParamByName('cifra').asCurrency   := Qassicurati.FieldByName('premio').asCurrency;
      Qins_assicurato.ParamByName('entrata').asDateTime := date;

      Qins_assicurato.execsql; // open;
      QassicuratiPortaf.Close;
      QassicuratiPortaf.ParamByName('cod_polizza').AsInteger := cod_polizza;
      QassicuratiPortaf.Open;

      Qins_link_pol_assic.Close;
      Qins_link_pol_assic.ParamByName('cod_polizza').AsInteger := cod_polizza;
      Qins_link_pol_assic.ParamByName('cod_assicurati').AsInteger := QassicuratiPortaf.FieldByName('codass').AsInteger;
      Qins_link_pol_assic.execsql;
      QassicuratiPortaf.Close;

      Qassicurati.Next;
    end;

  end;

  procedure registra_garanzie(CpDaPerf: integer);
  begin
    // registra una garanzia di polizza in garnzie.dat
    cerca_garanzie(CpDaPerf);
    Qins_garanzie.Close;

    Qins_garanzie.ParamByName('MASSIMALE').asCurrency   := Qgaranzie.FieldByName('massimale').asCurrency;
    Qins_garanzie.ParamByName('PREMIO').asCurrency      := Qgaranzie.FieldByName('premio').asCurrency;
    Qins_garanzie.ParamByName('DATA_IN').asDateTime     := Qgaranzie.FieldByName('data_in').asDateTime;
    Qins_garanzie.ParamByName('COD_POLIZZA').AsInteger  := cod_polizza;
    Qins_garanzie.ParamByName('RIF_GAR_BASE').AsInteger := Qgaranzie.FieldByName('rif_gar_base').AsInteger;
    Qins_garanzie.ParamByName('FRANCHIGIA').asString    := Qgaranzie.FieldByName('franchigia').asString;
    Qins_garanzie.ParamByName('PERCENTUALE').asCurrency := Qgaranzie.FieldByName('percentuale').asCurrency;
    Qins_garanzie.execsql;
  end;

  function registra_pagamento(CpPerfez: integer): integer;
  var
    tipo_t: string;
  begin
    // registra il pagamento di una poliza
    // creazione del record in scadenze e impostazione dei valori di incasso
    // {
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.Close;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('DENOMINAZ').asString :=
      Qpolizza.FieldByName('contraente').asString;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('RAMO').AsInteger := Qpolizza.FieldByName('rif_cod_ramo')
      .AsInteger;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('DESCRIZ').asString := '';
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('FRAZIONAM').asString :=
      Qpolizza.FieldByName('frazionam').asString;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('LORDO').asCurrency := Qpolizza.FieldByName('totale2')
      .asCurrency;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('provvigioni').asCurrency :=
      roundTo(Qpolizza.FieldByName('provvigioni').asCurrency, -2);

    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('RATA').asDateTime := Qpolizza.FieldByName('decorrenza')
      .asDateTime;

    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('COD_CLI').asString :=
      Qpolizza.FieldByName('cod_cli').asString;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('N_POLIZZA').asString :=
      Qpolizza.FieldByName('N_POLIZZA').asString;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('netto').asCurrency := Qpolizza.FieldByName('imponibile2')
      .asCurrency;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('ptasse').asCurrency := Qpolizza.FieldByName('ptasse')
      .asCurrency;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('tasse').asCurrency := Qpolizza.FieldByName('imposte2')
      .asCurrency;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('RIF_COD_COMPAGNIA').AsInteger :=
      Qpolizza.FieldByName('rif_compagnia').AsInteger;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('RIF_COD_POLIZZA').AsInteger := CpPerfez;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('inc_indice').asCurrency := 0;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('decorrenza').asDateTime := Qpolizza.FieldByName('decorrenza')
      .asDateTime;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('scadenza').asDateTime :=
      Qpolizza.FieldByName('SCAD_PRIMA_RATA').asDateTime;
    if Qpolizza.FieldByName('N_POLIZZA_SOSTITUITA').asString > '' then
      tipo_t := '11'
    else
      tipo_t := '10';
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('TIPO_TITOLO').asString := tipo_t;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('valuta').asString := '';
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('RIF_CLIENTE').AsInteger := Qpolizza.FieldByName('cod_cli')
      .AsInteger;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('INCASSATA_DA').AsInteger := CodCollab;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('DT_LAST_MOD').asDateTime := now;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('id_gen_slp_age').AsInteger :=
      StrToInt(UniMainModule.DMdatiAge.SLPdati_age.cod_int);
    // valorizza i dati identificativi del pagamento della polizza nuova
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('STATO').AsString := 'P';
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('progressivo').AsInteger := prog;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('data_pag').asDateTime := data_pag;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('data_pag_ccp').asDateTime := data_pag_ccp;
    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.ParamByName('data_reg_pag').asDateTime := Date;

    UniMainModule.DMdatiAgeTitoli.Qnew_titolo1011.execsql;

    // cerca il codice del titolo appena inserito !!!!
    // cerca in base a data, n_poilzza, tipo_titolo e importo
    UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.Close;
    UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.ParamByName('n_polizza').asString :=
      Qpolizza.FieldByName('N_POLIZZA').asString;
    UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.ParamByName('tipo_titolo').asString := tipo_t;
    UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.ParamByName('rata').asDateTime := Qpolizza.FieldByName('decorrenza')
      .asDateTime;
    UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.ParamByName('lordo').asCurrency := Qpolizza.FieldByName('totale2')
      .asCurrency;
    UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.Open;
    if UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.RecordCount = 1 then
      Result := UniMainModule.DMdatiAgeTitoli.QFindTitoloBis.FieldByName('cod_scadenza').AsInteger
    else
      Result := -1;

    // }
  end;

  procedure RefreshPolizza;
  var
    cod_pol: integer;
  begin
    Qpol_da_perfez.Prior;
    cod_pol := Qpol_da_perfez.FieldByName('cod_polizza').AsInteger;

    Qpol_da_perfez.DisableControls;
    try
      Qpol_da_perfez.Close;
      Qpol_da_perfez.Open;
      Qpol_da_perfez.Locate('cod_polizza', cod_pol, []);
    finally
      Qpol_da_perfez.EnableControls;
    end;
  end;

begin
  // esegue l'incasso della polizza indicata
  cod_polizza := registra_polizza;

  registra_assicurati(CodTitolo);
  registra_garanzie(CodTitolo);

  // registra l'incasso della polizza
  Result := registra_pagamento(cod_polizza);
  // passa lo stato della polizza a "perfezionato"
  if Result > -1 then
  begin
    Qcambia_stato.Close;
    Qcambia_stato.ParamByName('status').asString        := 'P';
    Qcambia_stato.ParamByName('cod_polizza').AsInteger  := CodTitolo;
    Qcambia_stato.ParamByName('data_perfez').asDateTime := now;
    Qcambia_stato.execsql;
  end;
  RefreshPolizza;

end;
// ================================================================================================================

// initialization
//
// RegisterModuleClass(TDMDatiAgePolizzePerf);

end.
