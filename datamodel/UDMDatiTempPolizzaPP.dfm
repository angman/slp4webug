inherited DMDatiTempPolizzaPP: TDMDatiTempPolizzaPP
  OldCreateOrder = True
  inherited fdmtblPolizza: TFDMemTable
    inherited fdmtblPolizzaNPercAccessori: TIntegerField
      OnChange = fdmtblPolizzaNPercAccessoriChange
    end
    object fdmtblPolizzaindicizzata: TBooleanField
      FieldName = 'indicizzata'
    end
    object fdmtblPolizzadescrizione: TStringField
      FieldName = 'descrizione'
      Size = 40
    end
    object fdmtblPolizzaNUMEROASSICURATI: TIntegerField
      FieldName = 'NUMEROASSICURATI'
    end
  end
  inherited fdmtblAssicurato: TFDMemTable
    BeforePost = fdmtblAssicuratoBeforePost
    AfterPost = fdmtblAssicuratoAfterPost
    AfterDelete = fdmtblAssicuratoAfterDelete
    AfterScroll = fdmtblAssicuratoAfterScroll
    object fdmtblAssicuratoGaranziaA: TBooleanField [34]
      FieldName = 'GaranziaA'
      OnChange = fdmtblAssicuratoGaranziaAChange
    end
    object fdmtblAssicuratoGaranziaB: TBooleanField [35]
      FieldName = 'GaranziaB'
      OnChange = fdmtblAssicuratoGaranziaAChange
    end
    object fdmtblAssicuratoGaranziaC: TBooleanField [36]
      FieldName = 'GaranziaC'
      OnChange = fdmtblAssicuratoGaranziaAChange
    end
    object fdmtblAssicuratoPremioAnnuoLordo: TCurrencyField [37]
      FieldName = 'PremioAnnuoLordo'
      OnChange = fdmtblAssicuratoPremioAnnuoLordoChange
    end
    object fdmtblAssicuratoPremioNettoSLP: TCurrencyField [38]
      FieldName = 'PremioNettoSLP'
    end
    object fdmtblAssicuratoSiglaMassimaleGaranzia: TStringField [39]
      FieldName = 'SiglaMassimaleGaranzia'
      OnChange = fdmtblAssicuratoSiglaMassimaleGaranziaChange
    end
    object fdmtblAssicuratoMassimaleGaranzia: TCurrencyField [40]
      FieldName = 'MassimaleGaranzia'
      OnChange = fdmtblAssicuratoMassimaleGaranziaChange
    end
    object fdmtblAssicuratoMassimaleIndex: TIntegerField [41]
      FieldName = 'MassimaleIndex'
    end
    object fdmtblAssicuratoSelectedGaranzie: TStringField [42]
      FieldName = 'SelectedGaranzie'
      Size = 50
    end
    object fdmtblAssicuratoGaranziaAgenzia: TStringField [43]
      FieldName = 'GaranziaAgenzia'
      OnChange = fdmtblAssicuratoGaranziaAgenziaChange
      Size = 30
    end
    object fdmtblAssicuratoSelGarStream: TBlobField
      FieldName = 'SelGarStream'
    end
  end
  inherited frxPolizzaGen: TfrxReport
    Left = 176
    Top = 200
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxAllegatiPOL: TfrxReport
    Left = 160
    Top = 160
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxGzipCompr: TfrxGZipCompressor
    Left = 32
  end
  inherited frxReport1: TfrxReport
    Left = 280
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object fdmtblSelGar: TFDMemTable
    IndexFieldNames = 'CodAssicurato'
    MasterSource = dsAssicurato
    MasterFields = 'CodAssicurato'
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 276
    Top = 184
    object fdmtblSelGarIdGaranzia: TFDAutoIncField
      FieldName = 'IDGaranzia'
      ReadOnly = True
      AutoIncrementSeed = 0
      AutoIncrementStep = 1
    end
    object fdmtbSelGarSelectedGaranzia: TBooleanField
      FieldName = 'Selected'
    end
    object fdmtblSelGarGarDescr: TStringField
      FieldName = 'GarDescr'
      Size = 35
    end
    object fdmtblSelGarCodAssicurato: TIntegerField
      FieldName = 'CodAssicurato'
    end
  end
  object fdmtblCalcGaranzie: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 116
    Top = 112
  end
  object dsAssicurato: TDataSource
    DataSet = fdmtblAssicurato
    Left = 208
    Top = 104
  end
end
