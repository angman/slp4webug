unit UDMdatiGenerali;

interface

uses
  System.SysUtils, System.Classes, dbisamtb, Data.DB, UDMMaster;

type
  TDMdatiGenerali = class(TDMMaster)
    QinsAccessi: TDBISAMQuery;
    QelencoAgenzie: TDBISAMQuery;
    DBaccessoBase: TDBISAMDatabase;
    SessioneAccessoBase: TDBISAMSession;
    QLog_Operazioni: TDBISAMQuery;
    DBscambio: TDBISAMDatabase;
    Qins_trasfDati: TDBISAMQuery;
    QscambioINVIA: TDBISAMQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    procedure insAllAgeAccessi(indirizzoIP, browser, sistemaOP, utenteInterno, operazione: string;
      ris_operazione: string = ''; ope_message: string = ''; id_agenzia: string = '');

  end;

const
  DataPwd = '27H312ACFF77908DDAC4005589.-+';

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

uses ServerController, USLPmain, IOUtils, Variants;

procedure TDMdatiGenerali.DataModuleCreate(Sender: TObject);
begin
  SessioneAccessoBase.Active := False;
  if UserSession.sviluppo then
  begin
    SessioneAccessoBase.PrivateDir := TPath.Combine(PosizioneDati, 'temp');
    // 'C:\Sviluppo\Sorgenti\Slp4Web_2019\SLPDATI\temp\';
    DBaccessoBase.Directory := PosizioneDati;
    DBscambio.Directory     := PosizioneDati;
  end else begin
      // MB 20042019
		sessioneAccessoBase.PrivateDir :='C:\slp\Slp4Web\Temp\';
		sessioneAccessoBase.RemoteEncryption := true;
		sessioneAccessoBase.RemoteEncryptionPassword := 'astraGalo37k21Hh51zZ';
		sessioneAccessoBase.RemotePort := 12005;
		sessioneAccessoBase.SessionType := stRemote;
		sessioneAccessoBase.RemoteAddress := '127.0.0.1';
		sessioneAccessoBase.RemoteUser := 'SLPcomune';
		sessioneAccessoBase.RemotePassword := '752scambiO421';
  end;

  SessioneAccessoBase.Active := True;
  SessioneAccessoBase.AddPassword(DataPwd);

end;

procedure TDMdatiGenerali.DataModuleDestroy(Sender: TObject);
begin
  DBaccessoBase.close;
end;

procedure TDMdatiGenerali.insAllAgeAccessi(indirizzoIP, browser, sistemaOP, utenteInterno, operazione: string;
  ris_operazione: string = ''; ope_message: string = ''; id_agenzia: string = '');
begin
  QinsAccessi.close;
  QinsAccessi.ParamByName('data').AsDateTime        := Now;
  QinsAccessi.ParamByName('indirizzoIP').AsString   := indirizzoIP;
  QinsAccessi.ParamByName('browser').AsString       := browser;
  QinsAccessi.ParamByName('sistemaOP').AsString     := sistemaOP;
  QinsAccessi.ParamByName('utenteInterno').AsString := utenteInterno;
  QinsAccessi.ParamByName('operazione').AsString    := operazione;
  QinsAccessi.ParamByName('risOperazione').AsString := ris_operazione;
  QinsAccessi.ParamByName('opeMessage').AsString    := ope_message;
  QinsAccessi.ParamByName('idAgenzia').AsString     := UserSession.DMdatiAge.SLPdati_age.cod_int;
  QinsAccessi.ExecSQL;
end;

initialization

RegisterClass(TDMdatiGenerali);

end.
