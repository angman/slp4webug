inherited DMDatiAgeFogliCassa: TDMDatiAgeFogliCassa
  OldCreateOrder = True
  Height = 328
  Width = 398
  object QfogliCassa: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select codice_foglio, periodo_dal, periodo_al, data_chiusura, da' +
        'ta_invio, tipo_invio, '
      '   cod_compagnia, sigla_compagnia, numero_fc,'
      '   versato, trattenuto , codice_foglio'
      'from fogli_cassa'
      '')
    Params = <>
    Left = 58
    Top = 40
  end
  object QAnni: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select distinct extract(year from periodo_dal) as anno'
      'from fogli_cassa'
      'order by Anno')
    Params = <>
    Left = 138
    Top = 48
  end
  object QgetNewNumeroFC: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      ''
      'update compagni'
      'set numero_fc = numero_fc +1'
      'where cod_compagnia = :cod_compagnia ;'
      ''
      'select * -- numero_fc '
      'from compagni'
      'where cod_compagnia = :cod_compagnia '
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
    Left = 58
    Top = 124
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
  end
  object QgetTotaliFC: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      ''
      
        'select sum(lordo) as tot_lordo , sum(provvigioni) as tot_provvig' +
        'ioni'
      '         FROM scadenze '
      
        '         where data_pag >= :chiusuraDal and data_pag <= :chiusur' +
        'aAl'
      '         and stato='#39'P'#39
      '        and  rif_cod_compagnia=:cod_compagnia'
      '')
    Params = <
      item
        DataType = ftUnknown
        Name = 'chiusuraDal'
      end
      item
        DataType = ftUnknown
        Name = 'chiusuraAl'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
    Left = 162
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'chiusuraDal'
      end
      item
        DataType = ftUnknown
        Name = 'chiusuraAl'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
  end
  object QinsFoglioCassa: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    ParamCheck = False
    SQL.Strings = (
      'insert into fogli_cassa'
      '(periodo_dal,'
      'periodo_al,'
      'data_chiusura,'
      'operatore,'
      'tipo_foglio,'
      ' cod_compagnia,'
      ' sigla_compagnia,'
      ' numero_fc,'
      ' id_gen_slp_age,'
      'versato,'
      'trattenuto'
      ')'
      ''
      'VALUES'
      '( :periodo_dal,'
      ':periodo_al,'
      ':data_chiusura,'
      ':operatore,'
      ':tipo_foglio,'
      ':cod_compagnia,'
      ':sigla_compagnia,'
      ':numero_fc,'
      ':id_gen_slp_age,'
      ':versato,'
      ':trattenuto'
      ' )')
    Params = <
      item
        DataType = ftUnknown
        Name = 'periodo_dal'
      end
      item
        DataType = ftUnknown
        Name = 'periodo_al'
      end
      item
        DataType = ftUnknown
        Name = 'data_chiusura'
      end
      item
        DataType = ftUnknown
        Name = 'operatore'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_foglio'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'numero_fc'
      end
      item
        DataType = ftUnknown
        Name = 'id_gen_slp_age'
      end
      item
        DataType = ftUnknown
        Name = 'versato'
      end
      item
        DataType = ftUnknown
        Name = 'trattenuto'
      end>
    Left = 50
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'periodo_dal'
      end
      item
        DataType = ftUnknown
        Name = 'periodo_al'
      end
      item
        DataType = ftUnknown
        Name = 'data_chiusura'
      end
      item
        DataType = ftUnknown
        Name = 'operatore'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_foglio'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'numero_fc'
      end
      item
        DataType = ftUnknown
        Name = 'id_gen_slp_age'
      end
      item
        DataType = ftUnknown
        Name = 'versato'
      end
      item
        DataType = ftUnknown
        Name = 'trattenuto'
      end>
  end
  object QgetCodiceFC: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    ParamCheck = False
    SQL.Strings = (
      'select codice_foglio'
      'from fogli_cassa'
      'where periodo_dal=:periodo_dal'
      'and periodo_al=:periodo_al'
      'and numero_fc = :numero_fc')
    Params = <
      item
        DataType = ftUnknown
        Name = 'periodo_dal'
      end
      item
        DataType = ftUnknown
        Name = 'periodo_al'
      end
      item
        DataType = ftUnknown
        Name = 'numero_fc'
      end>
    Left = 250
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'periodo_dal'
      end
      item
        DataType = ftUnknown
        Name = 'periodo_al'
      end
      item
        DataType = ftUnknown
        Name = 'numero_fc'
      end>
  end
  object QspostaInStorico: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      ''
      'update scadenze'
      'set foglio_cassa=:codice_foglio'
      'where data_pag >= :chiusuraDal and data_pag <= :chiusuraAl'
      'and stato='#39'P'#39' and rif_cod_compagnia=:cod_compagnia ;'
      ''
      ''
      ''
      'insert into s_scadenze'
      #9#9#9#9#9'(OLD_COMPAGNIA,'
      '           RAMO,'
      #9#9#9#9#9' DESCRIZ,'
      #9#9#9#9#9' RATA,'
      #9#9#9#9#9' COD_CLI,'
      '           RIF_CLIENTE,'
      #9#9#9#9#9' DATA_PAG,'
      '           DATA_ANN,'
      #9#9#9#9#9' MODIFICA,'
      #9#9#9#9#9' provvigioni,'
      #9#9#9#9#9' netto,'
      #9#9#9#9#9' ptasse,'
      #9#9#9#9#9' accessori,'
      #9#9#9#9#9' tasse,'
      #9#9#9#9#9' valuta,'
      #9#9#9#9#9' CAUSALE_ANNUL,'
      #9#9#9#9#9' inc_indice,'
      #9#9#9#9#9' PROGRESSIVO,'
      #9#9#9#9#9' INVIO_AVV_SCADENZA,'
      #9#9#9#9#9' INVIO_1_SOLLECITO,'
      #9#9#9#9#9' INVIO_2_SOLLECITO,'
      #9#9#9#9#9' TIPO_TITOLO,'
      #9#9#9#9#9' RIF_COD_POLIZZA,'
      #9#9#9#9#9' RIF_COD_COMPAGNIA,'
      #9#9#9#9#9' DECORRENZA,'
      #9#9#9#9#9' SCADENZA,'
      #9#9#9#9#9' lordo,'
      #9#9#9#9#9' N_POLIZZA,'
      #9#9#9#9#9' denominaz,'
      #9#9#9#9#9' frazionam,'
      #9#9#9#9#9' stato,'
      #9#9#9#9#9' data_pag_ccp,'
      #9#9#9#9#9' data_reg_pag,'
      #9#9#9#9#9' INCASSATA_DA,'
      #9#9#9#9#9' data_importazione,'
      #9#9#9#9#9' codice_importazione,'
      #9#9#9#9#9' foglio_cassa,'
      ''
      #9#9#9#9#9' COD_SCAD_STORICA,'
      '           ID_GEN_SLP_AGE'
      '          )'
      ''
      '          SELECT'
      '           sca.OLD_COMPAGNIA,'
      '           sca.RAMO,'
      #9#9#9#9#9' sca.DESCRIZ,'
      #9#9#9#9#9' sca.RATA,'
      #9#9#9#9#9' sca.COD_CLI,'
      '           sca.RIF_CLIENTE,'
      #9#9#9#9#9' sca.DATA_PAG,'
      '           sca.DATA_ANN,'
      #9#9#9#9#9' sca.MODIFICA,'
      #9#9#9#9#9' sca.provvigioni,'
      #9#9#9#9#9' sca.netto,'
      #9#9#9#9#9' sca.ptasse,'
      #9#9#9#9#9' sca.accessori,'
      #9#9#9#9#9' sca.tasse,'
      #9#9#9#9#9' sca.valuta,'
      #9#9#9#9#9' sca.CAUSALE_ANNUL,'
      #9#9#9#9#9' sca.inc_indice,'
      #9#9#9#9#9' sca.PROGRESSIVO,'
      #9#9#9#9#9' sca.INVIO_AVV_SCADENZA,'
      #9#9#9#9#9' sca.INVIO_1_SOLLECITO,'
      #9#9#9#9#9' sca.INVIO_2_SOLLECITO,'
      #9#9#9#9#9' sca.TIPO_TITOLO,'
      #9#9#9#9#9' sca.RIF_COD_POLIZZA,'
      #9#9#9#9#9' sca.RIF_COD_COMPAGNIA,'
      #9#9#9#9#9' sca.DECORRENZA,'
      #9#9#9#9#9' sca.SCADENZA,'
      #9#9#9#9#9' sca.lordo,'
      #9#9#9#9#9' sca.N_POLIZZA,'
      #9#9#9#9#9' sca.denominaz,'
      #9#9#9#9#9' sca.frazionam,'
      #9#9#9#9#9' sca.stato,'
      #9#9#9#9#9' sca.data_pag_ccp,'
      #9#9#9#9#9' sca.data_reg_pag,'
      #9#9#9#9#9' sca.INCASSATA_DA,'
      #9#9#9#9#9' sca.data_importazione,'
      #9#9#9#9#9' sca.codice_importazione,'
      #9#9#9#9#9' sca.foglio_cassa,'
      ''
      #9#9#9#9#9' sca.COD_SCADENZA,'
      '           sca.ID_GEN_SLP_AGE'
      ''
      '         FROM scadenze sca'
      
        '         where sca.data_pag >= :chiusuraDal and sca.data_pag <= ' +
        ':chiusuraAl'
      '         and sca.stato='#39'P'#39' and sca.foglio_cassa= :codice_foglio'
      '         and sca.rif_cod_compagnia=:cod_compagnia ;'
      ''
      '         delete from scadenze sca'
      
        '         where sca.data_pag >= :chiusuraDal and sca.data_pag <= ' +
        ':chiusuraAl'
      '         and sca.stato='#39'P'#39' and sca.foglio_cassa= :codice_foglio'
      '         and sca.rif_cod_compagnia=:cod_compagnia;')
    Params = <
      item
        DataType = ftInteger
        Name = 'codice_foglio'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraDal'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraAl'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
    Left = 178
    Top = 228
    ParamData = <
      item
        DataType = ftInteger
        Name = 'codice_foglio'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraDal'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraAl'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
  end
  object QaggCodFC_s_scad: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      ''
      '  update s_scadenze'
      '  set n_foglio_cassa = :numero_fc'
      '  where foglio_cassa = :codice_foglio')
    Params = <
      item
        DataType = ftUnknown
        Name = 'numero_fc'
      end
      item
        DataType = ftInteger
        Name = 'codice_foglio'
      end>
    Left = 278
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'numero_fc'
      end
      item
        DataType = ftInteger
        Name = 'codice_foglio'
      end>
  end
end
