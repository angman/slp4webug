inherited DMDatiTempPolizzaCondTP: TDMDatiTempPolizzaCondTP
  OldCreateOrder = True
  inherited fdmtblPolizza: TFDMemTable
    inherited fdmtblPolizzaSiglaMassimale: TStringField
      OnChange = fdmtblPolizzaSiglaMassimaleChange
    end
    object fdmtblPolizzaindicizzata: TBooleanField
      FieldName = 'indicizzata'
      OnChange = fdmtblPolizzaindicizzataChange
    end
    object fdmtblPolizzaUnitaImmobiliari: TIntegerField
      FieldName = 'UnitaImmobiliari'
      OnChange = fdmtblPolizzaUnitaImmobiliariChange
    end
    object fdmtblPolizzaNumeroBox: TIntegerField
      FieldName = 'NumeroBox'
      OnChange = fdmtblPolizzaNumeroBoxChange
    end
  end
  inherited fdmtblAssicurato: TFDMemTable
    object fdmtblAssicuratoPremioAnnuoLordo: TCurrencyField [34]
      FieldName = 'PremioAnnuoLordo'
      OnChange = fdmtblAssicuratoPremioAnnuoLordoChange
    end
  end
  inherited frxPolizzaGen: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxAllegatiPOL: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxReport1: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
end
