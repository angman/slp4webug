object DMDatiTempPolizza: TDMDatiTempPolizza
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 256
  Width = 330
  object fdmtblPolizza: TFDMemTable
    BeforePost = fdmtblPolizzaBeforePost
    AfterPost = fdmtblPolizzaAfterPost
    AfterScroll = fdmtblPolizzaAfterScroll
    OnNewRecord = fdmtblPolizzaNewRecord
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 32
    Top = 40
    object fdmtblPolizzaCodPolizza: TFDAutoIncField
      FieldName = 'CodPolizza'
      ReadOnly = True
      AutoIncrementSeed = 1
      AutoIncrementStep = 1
    end
    object fdmtblPolizzaRifTipoPol: TIntegerField
      FieldName = 'RifTipoPol'
    end
    object fdmtblPolizzaDecorrenza: TDateField
      FieldName = 'Decorrenza'
      OnChange = fdmtblPolizzaDecorrenzaChange
    end
    object fdmtblPolizzaScadenza: TDateField
      FieldName = 'Scadenza'
      OnChange = fdmtblPolizzaScadenzaChange
    end
    object fdmtblPolizzaFrazionamento: TStringField
      DisplayWidth = 1
      FieldName = 'Frazionamento'
      OnChange = fdmtblPolizzaFrazionamentoChange
      Size = 1
    end
    object fdmtblPolizzaNPolizza: TStringField
      FieldName = 'NPolizza'
      Size = 6
    end
    object fdmtblPolizzaLordo: TCurrencyField
      FieldName = 'Lordo'
    end
    object fdmtblPolizzaNetto: TCurrencyField
      FieldName = 'Netto'
    end
    object fdmtblPolizzaPTasse: TCurrencyField
      FieldName = 'PTasse'
    end
    object fdmtblPolizzaAccessori: TCurrencyField
      FieldName = 'Accessori'
    end
    object fdmtblPolizzaValuta: TStringField
      FieldName = 'Valuta'
      Size = 1
    end
    object fdmtblPolizzaTasse: TCurrencyField
      FieldName = 'Tasse'
    end
    object fdmtblPolizzaNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object fdmtblPolizzaRifCompagnia: TIntegerField
      FieldName = 'RifCompagnia'
    end
    object fdmtblPolizzaRifCodCli: TIntegerField
      FieldName = 'RifCodCli'
    end
    object fdmtblPolizzaCollaboratore: TIntegerField
      FieldName = 'Collaboratore'
    end
    object fdmtblPolizzaRifCodRamo: TIntegerField
      FieldName = 'RifCodRamo'
    end
    object fdmtblPolizzaStato: TStringField
      FieldName = 'Stato'
      Size = 1
    end
    object fdmtblPolizzaProvvigioni: TCurrencyField
      FieldName = 'Provvigioni'
    end
    object fdmtblPolizzaPProvvigioni: TCurrencyField
      FieldName = 'PProvvigioni'
    end
    object fdmtblPolizzaClasse: TIntegerField
      FieldName = 'Classe'
    end
    object fdmtblPolizzaAgenzia: TStringField
      FieldName = 'Agenzia'
      Size = 3
    end
    object fdmtblPolizzaAgenziaNome: TStringField
      FieldName = 'AgenziaNome'
      Size = 60
    end
    object fdmtblPolizzaSubAgenzia: TStringField
      FieldName = 'SubAgenzia'
      Size = 3
    end
    object fdmtblPolizzaRatino1Rata: TBooleanField
      FieldName = 'Ratino1Rata'
      Visible = False
      OnChange = fdmtblPolizzaRatino1RataChange
    end
    object fdmtblPolizzaScontoDurata: TBooleanField
      FieldName = 'ScontoDurata'
    end
    object fdmtblPolizzaUtente: TIntegerField
      FieldName = 'Utente'
    end
    object fdmtblPolizzaDataInserimento: TDateField
      FieldName = 'DataInserimento'
    end
    object fdmtblPolizzaNetto1: TCurrencyField
      FieldName = 'Netto1'
    end
    object fdmtblPolizzaAccessori1: TCurrencyField
      FieldName = 'Accessori1'
    end
    object fdmtblPolizzaInteressiFrazionamento1: TCurrencyField
      FieldName = 'InteressiFrazionamento'
    end
    object fdmtblPolizzaImponibile1: TCurrencyField
      FieldName = 'Imponibile1'
    end
    object fdmtblPolizzaImposte1: TCurrencyField
      FieldName = 'Imposte1'
    end
    object fdmtblPolizzaTotale1: TCurrencyField
      FieldName = 'Totale1'
    end
    object fdmtblPolizzaNetto2: TCurrencyField
      FieldName = 'Netto2'
    end
    object fdmtblPolizzaAccessori2: TCurrencyField
      FieldName = 'Accessori2'
    end
    object fdmtblPolizzaRimborsoSost: TCurrencyField
      FieldName = 'RimborsoSost'
    end
    object fdmtblPolizzaImponibile2: TCurrencyField
      FieldName = 'Imponibile2'
    end
    object fdmtblPolizzaImposte2: TCurrencyField
      FieldName = 'Imposte2'
    end
    object fdmtblPolizzaTotale2: TCurrencyField
      FieldName = 'Totale2'
    end
    object fdmtblPolizzaNetto1A: TCurrencyField
      FieldName = 'Netto1A'
    end
    object fdmtblPolizzaAccessori1A: TCurrencyField
      FieldName = 'Accessori1A'
    end
    object fdmtblPolizzaInteressiFrazionamento1A: TCurrencyField
      FieldName = 'InteressiFrazionamentoA'
    end
    object fdmtblPolizzaImponibile1A: TCurrencyField
      FieldName = 'Imponibile1A'
    end
    object fdmtblPolizzaImposte1A: TCurrencyField
      FieldName = 'Imposte1A'
    end
    object fdmtblPolizzaTotale1A: TCurrencyField
      FieldName = 'Totale1A'
    end
    object fdmtblPolizzaNetto2A: TCurrencyField
      FieldName = 'Netto2A'
    end
    object fdmtblPolizzaAccessori2A: TCurrencyField
      FieldName = 'Accessori2A'
    end
    object fdmtblPolizzaRimborsoSostA: TCurrencyField
      FieldName = 'RimborsoSostA'
    end
    object fdmtblPolizzaImposte2A: TCurrencyField
      FieldName = 'Imposte2A'
    end
    object fdmtblPolizzaTotale2A: TCurrencyField
      FieldName = 'Totale2A'
    end
    object fdmtblPolizzaCombinazione: TStringField
      FieldName = 'Combinazione'
      Size = 1
    end
    object fdmtblPolizzaForma: TStringField
      FieldName = 'Forma'
      Size = 1
    end
    object fdmtblPolizzaEstensioneOM: TStringField
      FieldName = 'EstensioneOM'
      Size = 1
    end
    object fdmtblPolizzaNumPolizzaSostituita: TStringField
      FieldName = 'NumPolizzaSostituita'
      Size = 6
    end
    object fdmtblPolizzaScadenzaPolizzaSostituita: TDateField
      FieldName = 'ScadenzaPolizzaSostituita'
    end
    object fdmtblPolizzaScadenzaPrimaRata: TDateField
      FieldName = 'ScadenzaPrimaRata'
    end
    object fdmtblPolizzaPolizzaStatus: TStringField
      FieldName = 'Status'
    end
    object fdmtblPolizzaPolizzaDataPerfezionamento: TDateField
      FieldName = 'DataPerfezionamento'
    end
    object fdmtblPolizzaDataCancellazione: TDateField
      FieldName = 'DataCancellazione'
    end
    object fdmtblPolizzaVolumeAffari: TCurrencyField
      FieldName = 'VolumeAffari'
    end
    object fdmtblPolizzaPercAccessori: TCurrencyField
      FieldName = 'PercAccessori'
      OnChange = fdmtblPolizzaPercAccessoriChange
    end
    object fdmtblPolizzaRagioneSociale: TStringField
      DisplayWidth = 60
      FieldName = 'RagioneSociale'
      Size = 60
    end
    object fdmtblPolizzaDati1: TStringField
      FieldName = 'Dati1'
      Size = 60
    end
    object fdmtblPolizzaDati2: TStringField
      FieldName = 'Dati2'
      Size = 30
    end
    object fdmtblPolizzaDati3: TStringField
      FieldName = 'Dati3'
      Size = 30
    end
    object fdmtblPolizzaDati4: TStringField
      FieldName = 'Dati4'
      Size = 30
    end
    object fdmtblPolizzaEstensioni: TStringField
      FieldName = 'Estensioni'
      Size = 15
    end
    object fdmtblPolizzaSiglaMassimale: TStringField
      FieldName = 'SiglaMassimale'
      Size = 4
    end
    object fdmtblPolizzaTipologia: TStringField
      FieldName = 'Tipologia'
      Size = 1
    end
    object fdmtblPolizzaDatiVari: TMemoField
      FieldName = 'DatiVari'
      BlobType = ftMemo
    end
    object fdmtblPolizzaOrdine: TStringField
      FieldName = 'Ordine'
      Size = 10
    end
    object fdmtblPolizzaSpecial: TStringField
      FieldName = 'Special'
      Size = 30
    end
    object fdmtblPolizzaDataImportazione: TDateTimeField
      FieldName = 'DataImportazione'
    end
    object fdmtblPolizzaCodiceImportazione: TIntegerField
      FieldName = 'CodiceImportazione'
    end
    object fdmtblPolizzaIntermediataDa: TIntegerField
      FieldName = 'IntermediataDa'
    end
    object fdmtblPolizzaConvenzione: TIntegerField
      FieldName = 'Convenzione'
    end
    object fdmtblPolizzaProvvig: TStringField
      FieldName = 'Provvig'
      OnChange = fdmtblPolizzaProvvigChange
      Size = 1
    end
    object fdmtblPolizzaRifReferente: TIntegerField
      FieldName = 'RifReferente'
    end
    object fdmtblPolizzaVarFrp: TMemoField
      FieldName = 'VarFrp'
      BlobType = ftMemo
    end
    object fdmtblPolizzaAppReport: TBlobField
      FieldName = 'AppReport'
    end
    object fdmtblPolizzaConAppendice: TStringField
      FieldName = 'ConAppendice'
      Size = 1
    end
    object fdmtblPolizzaAppNumero: TIntegerField
      FieldName = 'AppNumero'
    end
    object fdmtblPolizzaAppDal: TDateField
      FieldName = 'AppDal'
    end
    object fdmtblPolizzaAppAL: TDateField
      FieldName = 'AppAL'
    end
    object fdmtblPolizzaAppOggetto: TStringField
      FieldName = 'AppOggetto'
      Size = 30
    end
    object fdmtblPolizzaPSconto: TIntegerField
      FieldName = 'PSconto'
    end
    object fdmtblPolizzaInpDf: TStringField
      FieldName = 'InpDf'
      Size = 1
    end
    object fdmtblPolizzaSubPromoter: TIntegerField
      FieldName = 'SubPromoter'
    end
    object fdmtblPolizzaSubPromoterSigla: TStringField
      FieldName = 'SubPromoterSigla'
    end
    object fdmtblPolizzaDati5: TStringField
      FieldName = 'Dati5'
      Size = 60
    end
    object fdmtblPolizzaNSP: TIntegerField
      FieldName = 'NSP'
    end
    object fdmtblPolizzaTipoEmissione: TStringField
      FieldName = 'TipoEmissione'
      Size = 2
    end
    object fdmtblPolizzaDtLastMod: TDateField
      FieldName = 'DtLastMod'
    end
    object fdmtblPolizzaDurataAnni: TIntegerField
      FieldName = 'DurataAnni'
    end
    object fdmtblPolizzaDurataMesi: TIntegerField
      FieldName = 'DurataMesi'
    end
    object fdmtblPolizzaDurataGiorni: TIntegerField
      FieldName = 'DurataGiorni'
    end
    object fdmtblPolizzaCodCliente: TIntegerField
      FieldName = 'CodCliente'
    end
    object fdmtblPolizzaNomeCliente: TStringField
      FieldName = 'NomeCliente'
      Size = 60
    end
    object fdmtblPolizzaCodiceFiscale: TStringField
      FieldName = 'CodiceFiscale'
      Size = 16
    end
    object fdmtblPolizzaLocNascita: TStringField
      FieldName = 'LocNascita'
      Size = 30
    end
    object fdmtblPolizzaDataNascita: TDateField
      FieldName = 'DataNascita'
      OnChange = fdmtblPolizzaDataNascitaChange
    end
    object fdmtblPolizzaIndirizzo: TStringField
      FieldName = 'Indirizzo'
      Size = 50
    end
    object fdmtblPolizzaCap: TStringField
      FieldName = 'Cap'
      Size = 5
    end
    object fdmtblPolizzaCitta: TStringField
      FieldName = 'Citta'
      Size = 30
    end
    object fdmtblPolizzaProvincia: TStringField
      FieldName = 'Provincia'
    end
    object fdmtblPolizzaProfessione: TIntegerField
      FieldName = 'Professione'
    end
    object fdmtblPolizzaDescrizioneAttivita: TStringField
      FieldName = 'DescrizioneAttivita'
      Size = 50
    end
    object fdmtblPolizzaTacitoRinnovo: TBooleanField
      FieldName = 'TacitoRinnovo'
    end
    object fdmtblPolizzaContraente: TStringField
      FieldName = 'Contraente'
      Size = 50
    end
    object fdmtblPolizzaDataEmissione: TDateField
      FieldName = 'DataEmissione'
    end
    object fdmtbPolizzaCollaboratoreDescr: TStringField
      FieldName = 'CollaboratoreDescr'
      Size = 40
    end
    object fdmtblPolizzaPartitaIva: TStringField
      FieldName = 'PartitaIva'
      Size = 11
    end
    object fdmtblPolizzadt_preventivo: TDateTimeField
      FieldName = 'dt_preventivo'
    end
    object fdmtblPolizzaNPercAccessori: TIntegerField
      FieldName = 'NPercAccessori'
    end
    object fdmtblPolizzaNettoAnnuoTot: TCurrencyField
      FieldName = 'NettoAnnuoTot'
    end
    object fdmtblPolizzaSconto: TCurrencyField
      FieldName = 'Sconto'
    end
    object fdmtblPolizzaPremioScontato: TCurrencyField
      FieldName = 'PremioScontato'
    end
    object fdmtblPolizzaImponibile2A: TCurrencyField
      FieldName = 'Imponibile2A'
    end
    object fdmtblPolizzaTotaleAB: TCurrencyField
      FieldName = 'TotaleAB'
    end
    object fdmtblPolizzaArrotondamento: TCurrencyField
      FieldName = 'Arrotondamento'
    end
    object fdmtblPolizzaTelefono: TStringField
      FieldName = 'Telefono'
      Size = 30
    end
    object fdmtblPolizzaEMail: TStringField
      FieldName = 'EMail'
      Size = 50
    end
    object fdmtblPolizzaIdGenSlpAge: TIntegerField
      FieldName = 'IdGenSlpAge'
    end
    object fdmtblPolizzaSavCodPolizza: TIntegerField
      FieldName = 'SavCodPolizza'
    end
    object fdmtblPolizzaprofessioneDescriz: TStringField
      FieldName = 'professioneDescriz'
      Size = 40
    end
    object fdmtblPerAllegati: TStringField
      FieldName = 'PerAllegati'
      Size = 40
    end
    object fdmtblPolizzaTotaleAB0: TCurrencyField
      FieldName = 'TotaleAB0'
    end
    object fdmtblPolizzafatta_da: TIntegerField
      FieldName = 'fatta_da'
    end
    object fdmtblPolizzaintermediataDaSigla: TStringField
      FieldName = 'intermediataDaSigla'
      Size = 3
    end
  end
  object fdmtblGaranzia: TFDMemTable
    OnNewRecord = fdmtblGaranziaNewRecord
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 136
    Top = 40
    object fdmtblGaranziaCodGaranzia: TFDAutoIncField
      FieldName = 'CodGaranzia'
      ReadOnly = True
      AutoIncrementSeed = 1
      AutoIncrementStep = 1
    end
    object fdmtblGaranziaMassimale: TCurrencyField
      FieldName = 'Massimale'
    end
    object fdmtblGaranziaPremio: TCurrencyField
      FieldName = 'Premio'
    end
    object fdmtblGaranziaDataIn: TDateField
      FieldName = 'DataIn'
    end
    object fdmtblGaranziaDataOut: TDateField
      FieldName = 'DataOut'
    end
    object fdmtblGaranziaRifCodPolizza: TIntegerField
      FieldName = 'RifCodPolizza'
    end
    object fdmtblGaranziaValoreAssicurato: TCurrencyField
      FieldName = 'ValoreAssicurato'
    end
    object fdmtblGaranziaValoreIf: TCurrencyField
      FieldName = 'ValoreIf'
    end
    object fdmtblGaranziaRifGarBase: TIntegerField
      FieldName = 'RifGarBase'
    end
    object fdmtblGaranziaFranchigia: TStringField
      FieldName = 'Franchigia'
      Size = 1
    end
    object fdmtblGaranziaRifCodMassimale: TIntegerField
      FieldName = 'RifCodMassimale'
    end
    object fdmtblGaranziaPercentuale: TCurrencyField
      FieldName = 'Percentuale'
    end
    object fdmtblGaranziaIdGenSlpAge: TIntegerField
      FieldName = 'IdGenSlpAge'
    end
    object fdmtblGaranziaSiglaGaranzia: TStringField
      FieldName = 'SiglaGaranzia'
      Size = 5
    end
  end
  object fdmtblAssicurato: TFDMemTable
    OnNewRecord = fdmtblAssicuratoNewRecord
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 232
    Top = 48
    object fdmtblAssicuratoCodAssicurato: TFDAutoIncField
      FieldName = 'CodAssicurato'
      ReadOnly = True
      AutoIncrementSeed = 0
      AutoIncrementStep = 1
    end
    object fdmtblAssicuratoDenominazione: TStringField
      FieldName = 'Denominazione'
      Size = 40
    end
    object fdmtblAssicuratoIndirizzo: TStringField
      FieldName = 'Indirizzo'
      Size = 40
    end
    object fdmtblAssicuratoCitta: TStringField
      FieldName = 'Citta'
      Size = 25
    end
    object fdmtblAssicuratocap: TStringField
      FieldName = 'cap'
      Size = 5
    end
    object fdmtblAssicuratoProvincia: TStringField
      FieldName = 'Provincia'
      Size = 2
    end
    object fdmtblAssicuratoCodFiscIvas: TStringField
      FieldName = 'CodFiscIvas'
      Size = 16
    end
    object fdmtblAssicuratoNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object fdmtblAssicuratoEntrata: TDateField
      FieldName = 'Entrata'
    end
    object fdmtblAssicuratoUscita: TDateField
      FieldName = 'Uscita'
    end
    object fdmtblAssicuratoCausaleUscita: TIntegerField
      FieldName = 'CausaleUscita'
    end
    object fdmtblAssicuratoSostituitoDa: TIntegerField
      FieldName = 'SostituitoDa'
    end
    object fdmtblAssicuratoPatente: TStringField
      FieldName = 'Patente'
      Size = 13
    end
    object fdmtblAssicuratoCategoriaPatente: TStringField
      FieldName = 'CategoriaPatente'
      Size = 10
    end
    object fdmtblAssicuratoDataRilascio: TDateField
      FieldName = 'DataRilascio'
    end
    object fdmtblAssicuratoRilasciataDa: TStringField
      FieldName = 'RilasciataDa'
      Size = 2
    end
    object fdmtblAssicuratoDataScadenza: TDateField
      FieldName = 'DataScadenza'
    end
    object fdmtblAssicuratoTipoVeicolo: TStringField
      FieldName = 'TipoVeicolo'
      Size = 2
    end
    object fdmtblAssicuratoTarga: TStringField
      FieldName = 'Targa'
      Size = 8
    end
    object fdmtblAssicuratoMarca: TStringField
      FieldName = 'Marca'
    end
    object fdmtblAssicuratoHpQl: TStringField
      FieldName = 'HpQl'
      Size = 6
    end
    object fdmtblAssicuratoRifCodTipoAssicurato: TIntegerField
      FieldName = 'RifCodTipoAssicurato'
    end
    object fdmtblAssicuratoTipo: TStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object fdmtblAssicuratoCodPolizza: TIntegerField
      FieldName = 'CodPolizza'
    end
    object fdmtblAssicuratoDataScadRevisione: TDateField
      FieldName = 'DataScadRevisione'
    end
    object fdmtblAssicuratoModello: TStringField
      FieldName = 'Modello'
    end
    object fdmtblAssicuratoClasse: TIntegerField
      FieldName = 'Classe'
    end
    object fdmtblAssicuratoTelaio: TStringField
      FieldName = 'Telaio'
      Size = 15
    end
    object fdmtblAssicuratoCc: TStringField
      FieldName = 'Cc'
      Size = 10
    end
    object fdmtblAssicuratoDataImmatricolazione: TDateField
      FieldName = 'DataImmatricolazione'
    end
    object fdmtblAssicuratoPremio: TCurrencyField
      FieldName = 'Premio'
    end
    object fdmtblAssicuratoMassimale: TCurrencyField
      FieldName = 'Massimale'
    end
    object fdmtblAssicuratoPremioSLP: TCurrencyField
      FieldName = 'PremioSLP'
    end
    object fdmtblAssicuratoOggAggiuntivo: TStringField
      FieldName = 'OggAggiuntivo'
      Size = 60
    end
    object fdmtblAssicuratoIdGenSlpAge: TIntegerField
      FieldName = 'IdGenSlpAge'
    end
  end
  object frxpdfxprt: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    EmbedFontsIfProtected = False
    OpenAfterExport = False
    PrintOptimized = False
    Outline = False
    Background = True
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'SLP Assicurazioni Spa'
    Subject = 'SLP Assicurazioni Spa'
    Creator = 'SLP Assicurazioni Spa'
    ProtectionFlags = [ePrint]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    PDFStandard = psNone
    PDFVersion = pv17
    Left = 45
    Top = 176
  end
  object frxPolizzaGen: TfrxReport
    Version = '6.8'
    DotMatrixReport = False
    EngineOptions.SilentMode = True
    EngineOptions.NewSilentMode = simSilent
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.AllowPreviewEdit = False
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.Compressed = True
    ReportOptions.CreateDate = 43886.402419270830000000
    ReportOptions.LastChange = 43886.402419270830000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    ShowProgress = False
    Left = 184
    Top = 184
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxAllegatiPOL: TfrxReport
    Version = '6.8'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    EngineOptions.SilentMode = True
    EngineOptions.NewSilentMode = simSilent
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.AllowPreviewEdit = False
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.Compressed = True
    ReportOptions.CreateDate = 43886.402419270830000000
    ReportOptions.LastChange = 43886.402419270830000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 168
    Top = 120
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxGzipCompr: TfrxGZipCompressor
    Left = 40
    Top = 112
  end
  object frxReport1: TfrxReport
    Version = '6.8'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    EngineOptions.SilentMode = True
    EngineOptions.NewSilentMode = simSilent
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.AllowPreviewEdit = False
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.Compressed = True
    ReportOptions.CreateDate = 43886.402419270830000000
    ReportOptions.LastChange = 43886.402419270830000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 248
    Top = 128
    Datasets = <>
    Variables = <>
    Style = <>
  end
end
