inherited DMdatiAgeTitoli: TDMdatiAgeTitoli
  OldCreateOrder = True
  Height = 416
  Width = 654
  inherited QGetLastID: TDBISAMQuery
    Left = 26
    Top = 8
  end
  object Qtitoli: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select sca.stato Stato,  sca.n_polizza as n_polizza, sca.denomin' +
        'az, '
      '         sca.decorrenza Decorrenza, round(sca.lordo,2) Lordo,'
      
        '        round(sca.provvigioni, 2) provvigioni, sca.data_pag as D' +
        'ata_pag, '
      
        '        sca.Frazionam Fraz, sca.cod_scadenza, sca.rif_cod_compag' +
        'nia, pol.fatta_da, '
      
        '        cod_fc_sub_age, sca.tipo_titolo, sca.netto, sca.tasse, r' +
        'if_cod_polizza,'
      
        '        sca.data_pag_ccp, sca.old_compagnia, pol.inpdf, comp.cod' +
        'ice_agenzia, '
      
        '        pol.rif_tipo_pol as rif_tipo_polizza, tpol.descrizione a' +
        's tipo_polizza, provvigioni_sub, descriz,'
      '        pro.sigla as siglaSubAge, spro.sigla as siglaSubColl'
      ''
      
        'from scadenze sca left join polizze pol on cod_polizza = rif_cod' +
        '_polizza'
      
        '                   LEFT outer JOIN compagni comp ON comp.cod_com' +
        'pagnia = sca.rif_cod_compagnia'
      
        '                   left join tipo_pol tpol on tpol.cod_tipo_pol ' +
        '= pol.rif_tipo_pol'
      
        '                   LEFT JOIN clienti cli ON cli.cod_cliente = po' +
        'l.cod_cli'
      
        '                   left join produtt pro  on pro.cod_produttore ' +
        '= cli.promoter'
      
        '                   left join produtt spro on spro.cod_produttore' +
        ' = cli.sub_promoter')
    Params = <>
    Left = 56
    Top = 48
    object QtitoliStato: TStringField
      FieldName = 'Stato'
      FixedChar = True
      Size = 1
    end
    object Qtitolin_polizza: TStringField
      FieldName = 'n_polizza'
      Size = 15
    end
    object Qtitolidenominaz: TStringField
      FieldName = 'denominaz'
      Size = 40
    end
    object QtitoliDecorrenza: TDateField
      FieldName = 'Decorrenza'
    end
    object QtitoliLordo: TCurrencyField
      FieldName = 'Lordo'
    end
    object Qtitoliprovvigioni: TCurrencyField
      FieldName = 'provvigioni'
    end
    object QtitoliData_pag: TDateField
      FieldName = 'Data_pag'
    end
    object QtitoliFraz: TStringField
      FieldName = 'Fraz'
      FixedChar = True
      Size = 1
    end
    object QtitoliFrazionamentoLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'FrazionamentoLkup'
      LookupDataSet = DMdatiAge.fdmtblFrazionamento
      LookupKeyFields = 'CodFrazionamento'
      LookupResultField = 'DesFrazionamento'
      KeyFields = 'Fraz'
      Lookup = True
    end
    object Qtitolicod_scadenza: TAutoIncField
      FieldName = 'cod_scadenza'
    end
    object Qtitolirif_cod_compagnia: TIntegerField
      FieldName = 'rif_cod_compagnia'
    end
    object QtitoliCompagniaLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'CompagniaLkup'
      LookupDataSet = DMdatiAge.fdmtblTipoEmiss
      LookupKeyFields = 'CodCompagnia'
      LookupResultField = 'DesTipoEmiss'
      KeyFields = 'rif_cod_compagnia'
      Lookup = True
    end
    object Qtitolifatta_da: TIntegerField
      FieldName = 'fatta_da'
    end
    object Qtitolicod_fc_sub_age: TIntegerField
      FieldName = 'cod_fc_sub_age'
    end
    object Qtitolitipo_titolo: TStringField
      FieldName = 'tipo_titolo'
      Size = 2
    end
    object QtitoliTipoTitoloLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoTitoloLkup'
      LookupDataSet = QTipoTitolo
      LookupKeyFields = 'SIGLA'
      LookupResultField = 'DESCRIZIONE'
      KeyFields = 'tipo_titolo'
      Size = 30
      Lookup = True
    end
    object Qtitolinetto: TCurrencyField
      FieldName = 'netto'
    end
    object Qtitolitasse: TCurrencyField
      FieldName = 'tasse'
    end
    object Qtitolirif_cod_polizza: TIntegerField
      FieldName = 'rif_cod_polizza'
    end
    object Qtitolidata_pag_ccp: TDateField
      FieldName = 'data_pag_ccp'
    end
    object Qtitoliold_compagnia: TStringField
      FieldName = 'old_compagnia'
      Size = 6
    end
    object Qtitoliinpdf: TStringField
      FieldName = 'inpdf'
      Size = 1
    end
    object Qtitolicodice_agenzia: TStringField
      FieldName = 'codice_agenzia'
      Size = 10
    end
    object Qtitolirif_tipo_polizza: TIntegerField
      FieldName = 'rif_tipo_polizza'
    end
    object Qtitolitipo_polizza: TStringField
      FieldName = 'tipo_polizza'
      Size = 30
    end
    object Qtitoliprovvigioni_sub: TCurrencyField
      FieldName = 'provvigioni_sub'
    end
    object Qtitolidescriz: TStringField
      FieldName = 'descriz'
      Size = 30
    end
    object QtitolisiglaSubAge: TStringField
      FieldName = 'siglaSubAge'
      Size = 5
    end
    object QtitolisiglaSubColl: TStringField
      FieldName = 'siglaSubColl'
      Size = 5
    end
  end
  object DStitoli: TDataSource
    DataSet = Qtitoli
    Left = 120
    Top = 48
  end
  object Qincassi: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select      '
      
        '         scadenze.progressivo, scadenze.decorrenza, scadenze.dat' +
        'a_pag, scadenze.n_polizza,scadenze.denominaz,'
      
        '         round(scadenze.lordo,2) lordo, scadenze.provvigioni as ' +
        'provvigioni, appoggio.nome as sappoggio,'
      
        '         compagni.nome, compagni.codice_agenzia, clienti.promote' +
        'r, '
      '        scadenze.cod_scadenza, '
      
        '        compagni.cod_compagnia, tipo_titolo, valuta, data_reg_pa' +
        'g, '
      
        '         produtt.sigla as sigla_prod, scadenze.provvigioni_sub, ' +
        'polizze.inpdf,descriz'
      '         from'
      '            scadenze'
      '      '
      
        '          LEFT OUTER JOIN compagni ON compagni.cod_compagnia = s' +
        'cadenze.rif_cod_compagnia'
      '      '
      
        '         LEFT OUTER JOIN polizze  ON polizze.cod_polizza = scade' +
        'nze.rif_cod_polizza'
      '      '
      
        '         LEFT OUTER JOIN clienti  ON clienti.cod_cliente = poliz' +
        'ze.cod_cli'
      '      '
      
        '           LEFT OUTER JOIN appoggio ON polizze.appoggiata_a = ap' +
        'poggio.cod_appoggio'
      '      '
      
        '           LEFT OUTER JOIN produtt ON clienti.promoter = produtt' +
        '.cod_produttore'
      '        where      scadenze.stato<>'#39'P'#39)
    Params = <>
    Left = 40
    Top = 112
  end
  object Qscadenze: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from scadenze'
      'where '
      'rif_cod_compagnia =:rif_cod_compagnia'
      'and n_polizza = :n_polizza'
      'and rata = :rata'
      'and tipo_titolo = :tipo_titolo')
    Params = <
      item
        DataType = ftUnknown
        Name = 'rif_cod_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
    Left = 40
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rif_cod_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
  end
  object QscadenzeStorico: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from s_scadenze'
      'where '
      'rif_cod_compagnia =:rif_cod_compagnia'
      'and n_polizza = :n_polizza'
      'and rata = :rata'
      'and tipo_titolo = :tipo_titolo')
    Params = <
      item
        DataType = ftUnknown
        Name = 'rif_cod_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
    Left = 40
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rif_cod_compagnia'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
  end
  object Qupd_scadenze: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update scadenze'
      
        'set netto = :imponibile, lordo = :premio_lordo, provvigioni = :p' +
        'rovvigioni, '
      'tasse = :imposte, provvigioni_sub = :provvigioni_suB'
      'where cod_scadenza = :cod_scadenza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'imponibile'
      end
      item
        DataType = ftUnknown
        Name = 'premio_lordo'
      end
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'imposte'
      end
      item
        DataType = ftUnknown
        Name = 'provvigioni_suB'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
    Left = 138
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'imponibile'
      end
      item
        DataType = ftUnknown
        Name = 'premio_lordo'
      end
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'imposte'
      end
      item
        DataType = ftUnknown
        Name = 'provvigioni_suB'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
  end
  object DSTipoTitolo: TDataSource
    DataSet = QTipoTitolo
    Left = 368
    Top = 24
  end
  object QTipoTitolo: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_titoli'
      'order by descrizione')
    Params = <>
    Left = 304
    Top = 24
  end
  object QincassaTitolo: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update scadenze'
      'set  provvigioni = :provvigioni, '
      '       -- provvigioni_sub = :provvigioni_suB,'
      '     descriz= :descriz,'
      '     data_pag= :data_pag,'
      '     data_pag_ccp= :data_pag_ccp,'
      '     data_reg_pag= :data_reg_pag,'
      '     stato = :stato, '
      '     incassata_da= :incassata_da,'
      '     progressivo= :progressivo'
      ''
      'where cod_scadenza = :cod_scadenza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'descriz'
      end
      item
        DataType = ftUnknown
        Name = 'data_pag'
      end
      item
        DataType = ftUnknown
        Name = 'data_pag_ccp'
      end
      item
        DataType = ftUnknown
        Name = 'data_reg_pag'
      end
      item
        DataType = ftUnknown
        Name = 'stato'
      end
      item
        DataType = ftUnknown
        Name = 'incassata_da'
      end
      item
        DataType = ftUnknown
        Name = 'progressivo'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
    Left = 130
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'descriz'
      end
      item
        DataType = ftUnknown
        Name = 'data_pag'
      end
      item
        DataType = ftUnknown
        Name = 'data_pag_ccp'
      end
      item
        DataType = ftUnknown
        Name = 'data_reg_pag'
      end
      item
        DataType = ftUnknown
        Name = 'stato'
      end
      item
        DataType = ftUnknown
        Name = 'incassata_da'
      end
      item
        DataType = ftUnknown
        Name = 'progressivo'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
  end
  object QfogliCassa: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'select codice_foglio, periodo_dal, periodo_al, data_chiusura, da' +
        'ta_invio, tipo_invio, '
      '   cod_compagnia, sigla_compagnia, numero_fc,'
      '   versato, trattenuto , codice_foglio'
      'from fogli_cassa'
      'order by periodo_dal')
    Params = <>
    Left = 360
    Top = 128
  end
  object DSfogliCassa: TDataSource
    DataSet = QfogliCassa
    Left = 424
    Top = 128
  end
  object QgetFoglioCassa: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from fogli_cassa'
      'where codice_foglio = :codice_foglio')
    Params = <
      item
        DataType = ftUnknown
        Name = 'codice_foglio'
      end>
    Left = 320
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'codice_foglio'
      end>
  end
  object QdispagaTitolo: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update scadenze'
      'set data_pag = null,'
      '     data_pag_ccp= null,'
      '     data_reg_pag= null,'
      '     stato = '#39#39', '
      '     incassata_da= null,'
      '     progressivo= null,'
      '     provvigioni=:provvigioni'
      ''
      'where cod_scadenza = :cod_scadenza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
    Left = 154
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'provvigioni'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
  end
  object QdispagaTitolo1011: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update slp_tpolizze'
      'set status = null, data_perfez = null'
      'where n_polizza=:n_polizza;'
      ''
      'delete from polizze'
      'where n_polizza=:n_polizza;'
      ''
      'delete from scadenze'
      'where cod_scadenza = :cod_scadenza and n_polizza=:n_polizza')
    Params = <
      item
        DataType = ftString
        Name = 'n_polizza'
      end
      item
        DataType = ftInteger
        Name = 'cod_scadenza'
      end>
    Left = 186
    Top = 304
    ParamData = <
      item
        DataType = ftString
        Name = 'n_polizza'
      end
      item
        DataType = ftInteger
        Name = 'cod_scadenza'
      end>
  end
  object QspostaInStorico: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      ''
      'update scadenze'
      'set foglio_cassa=:codice_foglio'
      'where data_pag >= :chiusuraDal and data_pag <= :chiusuraAl'
      'and stato='#39'P'#39' and rif_cod_compagnia=:cod_compagnia ;'
      ''
      ''
      ''
      'insert into s_scadenze'
      #9#9#9#9#9'(OLD_COMPAGNIA,'
      '           RAMO,'
      #9#9#9#9#9' DESCRIZ,'
      #9#9#9#9#9' RATA,'
      #9#9#9#9#9' COD_CLI,'
      '           RIF_CLIENTE,'
      #9#9#9#9#9' DATA_PAG,'
      '           DATA_ANN,'
      #9#9#9#9#9' MODIFICA,'
      #9#9#9#9#9' provvigioni,'
      #9#9#9#9#9' netto,'
      #9#9#9#9#9' ptasse,'
      #9#9#9#9#9' accessori,'
      #9#9#9#9#9' tasse,'
      #9#9#9#9#9' valuta,'
      #9#9#9#9#9' CAUSALE_ANNUL,'
      #9#9#9#9#9' inc_indice,'
      #9#9#9#9#9' PROGRESSIVO,'
      #9#9#9#9#9' INVIO_AVV_SCADENZA,'
      #9#9#9#9#9' INVIO_1_SOLLECITO,'
      #9#9#9#9#9' INVIO_2_SOLLECITO,'
      #9#9#9#9#9' TIPO_TITOLO,'
      #9#9#9#9#9' RIF_COD_POLIZZA,'
      #9#9#9#9#9' RIF_COD_COMPAGNIA,'
      #9#9#9#9#9' DECORRENZA,'
      #9#9#9#9#9' SCADENZA,'
      #9#9#9#9#9' lordo,'
      #9#9#9#9#9' N_POLIZZA,'
      #9#9#9#9#9' denominaz,'
      #9#9#9#9#9' frazionam,'
      #9#9#9#9#9' stato,'
      #9#9#9#9#9' data_pag_ccp,'
      #9#9#9#9#9' data_reg_pag,'
      #9#9#9#9#9' INCASSATA_DA,'
      #9#9#9#9#9' data_importazione,'
      #9#9#9#9#9' codice_importazione,'
      #9#9#9#9#9' foglio_cassa,'
      ''
      #9#9#9#9#9' COD_SCAD_STORICA,'
      '           ID_GEN_SLP_AGE'
      '          )'
      ''
      '          SELECT'
      '           sca.OLD_COMPAGNIA,'
      '           sca.RAMO,'
      #9#9#9#9#9' sca.DESCRIZ,'
      #9#9#9#9#9' sca.RATA,'
      #9#9#9#9#9' sca.COD_CLI,'
      '           sca.RIF_CLIENTE,'
      #9#9#9#9#9' sca.DATA_PAG,'
      '           sca.DATA_ANN,'
      #9#9#9#9#9' sca.MODIFICA,'
      #9#9#9#9#9' sca.provvigioni,'
      #9#9#9#9#9' sca.netto,'
      #9#9#9#9#9' sca.ptasse,'
      #9#9#9#9#9' sca.accessori,'
      #9#9#9#9#9' sca.tasse,'
      #9#9#9#9#9' sca.valuta,'
      #9#9#9#9#9' sca.CAUSALE_ANNUL,'
      #9#9#9#9#9' sca.inc_indice,'
      #9#9#9#9#9' sca.PROGRESSIVO,'
      #9#9#9#9#9' sca.INVIO_AVV_SCADENZA,'
      #9#9#9#9#9' sca.INVIO_1_SOLLECITO,'
      #9#9#9#9#9' sca.INVIO_2_SOLLECITO,'
      #9#9#9#9#9' sca.TIPO_TITOLO,'
      #9#9#9#9#9' sca.RIF_COD_POLIZZA,'
      #9#9#9#9#9' sca.RIF_COD_COMPAGNIA,'
      #9#9#9#9#9' sca.DECORRENZA,'
      #9#9#9#9#9' sca.SCADENZA,'
      #9#9#9#9#9' sca.lordo,'
      #9#9#9#9#9' sca.N_POLIZZA,'
      #9#9#9#9#9' sca.denominaz,'
      #9#9#9#9#9' sca.frazionam,'
      #9#9#9#9#9' sca.stato,'
      #9#9#9#9#9' sca.data_pag_ccp,'
      #9#9#9#9#9' sca.data_reg_pag,'
      #9#9#9#9#9' sca.INCASSATA_DA,'
      #9#9#9#9#9' sca.data_importazione,'
      #9#9#9#9#9' sca.codice_importazione,'
      #9#9#9#9#9' sca.foglio_cassa,'
      ''
      #9#9#9#9#9' sca.COD_SCADENZA,'
      '           sca.ID_GEN_SLP_AGE'
      ''
      '         FROM scadenze sca'
      
        '         where sca.data_pag >= :chiusuraDal and sca.data_pag <= ' +
        ':chiusuraAl'
      '         and sca.stato='#39'P'#39' and sca.foglio_cassa= :codice_foglio'
      '         and sca.rif_cod_compagnia=:cod_compagnia ;'
      ''
      '         delete from scadenze sca'
      
        '         where sca.data_pag >= :chiusuraDal and sca.data_pag <= ' +
        ':chiusuraAl'
      '         and sca.stato='#39'P'#39' and sca.foglio_cassa= :codice_foglio'
      '         and sca.rif_cod_compagnia=:cod_compagnia;')
    Params = <
      item
        DataType = ftInteger
        Name = 'codice_foglio'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraDal'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraAl'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
    Left = 322
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'codice_foglio'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraDal'
      end
      item
        DataType = ftDateTime
        Name = 'chiusuraAl'
      end
      item
        DataType = ftUnknown
        Name = 'cod_compagnia'
      end>
  end
  object Qnew_titolo: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into scadenze'
      '('
      'DENOMINAZ,'
      'RAMO,'
      'DESCRIZ,'
      'FRAZIONAM,'
      'LORDO,'
      'provvigioni,'
      'RATA,'
      'COD_CLI,'
      'N_POLIZZA,'
      'netto,'
      'ptasse,'
      'tasse,'
      'RIF_COD_COMPAGNIA,'
      'RIF_COD_POLIZZA,'
      'inc_indice,'
      'decorrenza,'
      'scadenza,'
      'TIPO_TITOLO,'
      'valuta,'
      'RIF_CLIENTE,'
      'INCASSATA_DA,'
      'DT_LAST_MOD,'
      'id_gen_slp_age'
      ')'
      'VALUES'
      '('
      ':DENOMINAZ,'
      ':RAMO,'
      ':DESCRIZ,'
      ':FRAZIONAM,'
      ':LORDO,'
      ':provvigioni,'
      ':RATA,'
      ':COD_CLI,'
      ':N_POLIZZA,'
      ':netto,'
      ':ptasse,'
      ':tasse,'
      ':RIF_COD_COMPAGNIA,'
      ':RIF_COD_POLIZZA,'
      ':inc_indice,'
      ':decorrenza,'
      ':scadenza,'
      ':TIPO_TITOLO,'
      ':valuta,'
      ':RIF_CLIENTE,'
      ':INCASSATA_DA,'
      ':DT_LAST_MOD,'
      ':id_gen_slp_age'
      ')')
    Params = <
      item
        DataType = ftString
        Name = 'DENOMINAZ'
      end
      item
        DataType = ftInteger
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'DESCRIZ'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftDate
        Name = 'RATA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'netto'
      end
      item
        DataType = ftCurrency
        Name = 'ptasse'
      end
      item
        DataType = ftCurrency
        Name = 'tasse'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'inc_indice'
      end
      item
        DataType = ftDate
        Name = 'decorrenza'
      end
      item
        DataType = ftDate
        Name = 'scadenza'
      end
      item
        DataType = ftInteger
        Name = 'TIPO_TITOLO'
      end
      item
        DataType = ftString
        Name = 'valuta'
      end
      item
        DataType = ftInteger
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftInteger
        Name = 'INCASSATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftInteger
        Name = 'id_gen_slp_age'
      end>
    Left = 328
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'DENOMINAZ'
      end
      item
        DataType = ftInteger
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'DESCRIZ'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftDate
        Name = 'RATA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'netto'
      end
      item
        DataType = ftCurrency
        Name = 'ptasse'
      end
      item
        DataType = ftCurrency
        Name = 'tasse'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'inc_indice'
      end
      item
        DataType = ftDate
        Name = 'decorrenza'
      end
      item
        DataType = ftDate
        Name = 'scadenza'
      end
      item
        DataType = ftInteger
        Name = 'TIPO_TITOLO'
      end
      item
        DataType = ftString
        Name = 'valuta'
      end
      item
        DataType = ftInteger
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftInteger
        Name = 'INCASSATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftInteger
        Name = 'id_gen_slp_age'
      end>
  end
  object Qtipo_pagamenti: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from tipo_pagamenti'
      'order by codice_pag')
    Params = <>
    Left = 472
    Top = 16
  end
  object DStipo_pagamenti: TDataSource
    DataSet = Qtipo_pagamenti
    Left = 568
    Top = 16
  end
  object Qins_estremi_pag: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into dati_incasso'
      '('
      ''
      '   TIPO_MOVIMENTO,'
      '   DATA_OPERAZIONE,'
      '   DATA_VALUTA,'
      '   IMPORTO,'
      '   MEZZO_PAGAMENTO,'
      '   ESTREMI1,'
      '   ESTREMI2,'
      '   ESTREMI3,'
      '   BANCA,'
      '   DESCRIZIONE,'
      '   IN_SOSPESO,'
      '   NOTE,'
      '   DATA_TITOLO,'
      ''
      '   RIF_SCADENZA,'
      '   RIF_POLIZZA,'
      '   RIF_CLIENTE,'
      ''
      '   EMESSO_DA,'
      '   RIF_COMPAGNIA,'
      '--   DATA_PRESENTAZIONE,'
      '--   DA_CASSA,'
      '--   A_CASSA,'
      '   OPERATORE,'
      ''
      '   N_POLIZZA,'
      ''
      '   RIF_PROMOTER,'
      '   sospeso_broker,'
      '--   data_chiusura_sospeso,'
      '--   descrizione_chiusura_sospeso,'
      '   DT_LAST_MOD,'
      '  ID_GEN_SLP_AGE'
      ')'
      ''
      'VALUES'
      ''
      '('
      ''
      '   :TIPO_MOVIMENTO,'
      '   :DATA_OPERAZIONE,'
      '   :DATA_VALUTA,'
      '   :IMPORTO,'
      '   :MEZZO_PAGAMENTO,'
      '   :ESTREMI1,'
      '   :ESTREMI2,'
      '   :ESTREMI3,'
      '   :BANCA,'
      '   :DESCRIZIONE,'
      '   :IN_SOSPESO,'
      '   :NOTE,'
      '   :DATA_TITOLO,'
      ''
      '   :RIF_SCADENZA,'
      '   :RIF_POLIZZA,'
      '   :RIF_CLIENTE,'
      ''
      '   :EMESSO_DA,'
      '   :RIF_COMPAGNIA,'
      '--   :DATA_PRESENTAZIONE,'
      '--   :DA_CASSA,'
      '--   :A_CASSA,'
      '   :OPERATORE,'
      ''
      '   :N_POLIZZA,'
      ''
      '   :RIF_PROMOTER,'
      '   :sospeso_broker,'
      '--   :data_chiusura_sospeso,'
      '--   :descrizione_chiusura_sospeso,'
      '   :DT_LAST_MOD,'
      '   :ID_GEN_SLP_AGE'
      ')')
    Params = <
      item
        DataType = ftUnknown
        Name = 'TIPO_MOVIMENTO'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_OPERAZIONE'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_VALUTA'
      end
      item
        DataType = ftUnknown
        Name = 'IMPORTO'
      end
      item
        DataType = ftUnknown
        Name = 'MEZZO_PAGAMENTO'
      end
      item
        DataType = ftUnknown
        Name = 'ESTREMI1'
      end
      item
        DataType = ftUnknown
        Name = 'ESTREMI2'
      end
      item
        DataType = ftUnknown
        Name = 'ESTREMI3'
      end
      item
        DataType = ftUnknown
        Name = 'BANCA'
      end
      item
        DataType = ftUnknown
        Name = 'DESCRIZIONE'
      end
      item
        DataType = ftUnknown
        Name = 'IN_SOSPESO'
      end
      item
        DataType = ftUnknown
        Name = 'NOTE'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_TITOLO'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_SCADENZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftUnknown
        Name = 'EMESSO_DA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftUnknown
        Name = 'OPERATORE'
      end
      item
        DataType = ftUnknown
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_PROMOTER'
      end
      item
        DataType = ftUnknown
        Name = 'sospeso_broker'
      end
      item
        DataType = ftUnknown
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
    Left = 480
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'TIPO_MOVIMENTO'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_OPERAZIONE'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_VALUTA'
      end
      item
        DataType = ftUnknown
        Name = 'IMPORTO'
      end
      item
        DataType = ftUnknown
        Name = 'MEZZO_PAGAMENTO'
      end
      item
        DataType = ftUnknown
        Name = 'ESTREMI1'
      end
      item
        DataType = ftUnknown
        Name = 'ESTREMI2'
      end
      item
        DataType = ftUnknown
        Name = 'ESTREMI3'
      end
      item
        DataType = ftUnknown
        Name = 'BANCA'
      end
      item
        DataType = ftUnknown
        Name = 'DESCRIZIONE'
      end
      item
        DataType = ftUnknown
        Name = 'IN_SOSPESO'
      end
      item
        DataType = ftUnknown
        Name = 'NOTE'
      end
      item
        DataType = ftUnknown
        Name = 'DATA_TITOLO'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_SCADENZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftUnknown
        Name = 'EMESSO_DA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_COMPAGNIA'
      end
      item
        DataType = ftUnknown
        Name = 'OPERATORE'
      end
      item
        DataType = ftUnknown
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftUnknown
        Name = 'RIF_PROMOTER'
      end
      item
        DataType = ftUnknown
        Name = 'sospeso_broker'
      end
      item
        DataType = ftUnknown
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftUnknown
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  object QTitoloEdit: TDBISAMQuery
    BeforePost = QTitoloEditBeforePost
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'SELECT * '
      'FROM SCADENZE'
      'WHERE COD_SCADENZA = :COD_SCADENZA')
    Params = <
      item
        DataType = ftInteger
        Name = 'COD_SCADENZA'
      end>
    Left = 192
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COD_SCADENZA'
      end>
    object QTitoloEditCOD_SCADENZA: TAutoIncField
      FieldName = 'COD_SCADENZA'
    end
    object QTitoloEditDENOMINAZ: TStringField
      FieldName = 'DENOMINAZ'
      Size = 40
    end
    object QTitoloEditOLD_COMPAGNIA: TStringField
      FieldName = 'OLD_COMPAGNIA'
      Size = 6
    end
    object QTitoloEditRAMO: TIntegerField
      FieldName = 'RAMO'
    end
    object QTitoloEditDESCRIZ: TStringField
      FieldName = 'DESCRIZ'
      Size = 30
    end
    object QTitoloEditFRAZIONAM: TStringField
      FieldName = 'FRAZIONAM'
      FixedChar = True
      Size = 1
    end
    object QTitoloEditLORDO: TCurrencyField
      FieldName = 'LORDO'
    end
    object QTitoloEditRATA: TDateField
      FieldName = 'RATA'
    end
    object QTitoloEditCOD_CLI: TStringField
      FieldName = 'COD_CLI'
      Size = 6
    end
    object QTitoloEditN_POLIZZA: TStringField
      FieldName = 'N_POLIZZA'
      Size = 15
    end
    object QTitoloEditSTATO: TStringField
      FieldName = 'STATO'
      FixedChar = True
      Size = 1
    end
    object QTitoloEditDATA_PAG: TDateField
      FieldName = 'DATA_PAG'
    end
    object QTitoloEditFOGLIO_CASSA: TIntegerField
      FieldName = 'FOGLIO_CASSA'
    end
    object QTitoloEditDATA_ANN: TDateField
      FieldName = 'DATA_ANN'
    end
    object QTitoloEditMODIFICA: TDateTimeField
      FieldName = 'MODIFICA'
    end
    object QTitoloEditprovvigioni: TCurrencyField
      FieldName = 'provvigioni'
    end
    object QTitoloEditnetto: TCurrencyField
      FieldName = 'netto'
    end
    object QTitoloEditptasse: TCurrencyField
      FieldName = 'ptasse'
    end
    object QTitoloEditaccessori: TCurrencyField
      FieldName = 'accessori'
    end
    object QTitoloEdittasse: TCurrencyField
      FieldName = 'tasse'
    end
    object QTitoloEditvaluta: TStringField
      FieldName = 'valuta'
      FixedChar = True
      Size = 1
    end
    object QTitoloEditRIF_COD_COMPAGNIA: TIntegerField
      FieldName = 'RIF_COD_COMPAGNIA'
    end
    object QTitoloEditRIF_COD_POLIZZA: TIntegerField
      FieldName = 'RIF_COD_POLIZZA'
    end
    object QTitoloEditCAUSALE_ANNUL: TIntegerField
      FieldName = 'CAUSALE_ANNUL'
    end
    object QTitoloEditinc_indice: TCurrencyField
      FieldName = 'inc_indice'
    end
    object QTitoloEditPROGRESSIVO: TIntegerField
      FieldName = 'PROGRESSIVO'
    end
    object QTitoloEditdecorrenza: TDateField
      FieldName = 'decorrenza'
    end
    object QTitoloEditscadenza: TDateField
      FieldName = 'scadenza'
    end
    object QTitoloEditINVIO_AVV_SCADENZA: TDateField
      FieldName = 'INVIO_AVV_SCADENZA'
    end
    object QTitoloEditINVIO_1_SOLLECITO: TDateField
      FieldName = 'INVIO_1_SOLLECITO'
    end
    object QTitoloEditINVIO_2_SOLLECITO: TDateField
      FieldName = 'INVIO_2_SOLLECITO'
    end
    object QTitoloEditTIPO_TITOLO: TStringField
      FieldName = 'TIPO_TITOLO'
      Size = 2
    end
    object QTitoloEditRIF_CLIENTE: TIntegerField
      FieldName = 'RIF_CLIENTE'
    end
    object QTitoloEditDATA_COPERTURA: TDateField
      FieldName = 'DATA_COPERTURA'
    end
    object QTitoloEditDATA_STAMPA: TDateField
      FieldName = 'DATA_STAMPA'
    end
    object QTitoloEditP_NOTA_NUMERO: TIntegerField
      FieldName = 'P_NOTA_NUMERO'
    end
    object QTitoloEditP_NOTA_DATA: TDateField
      FieldName = 'P_NOTA_DATA'
    end
    object QTitoloEditDATA_REG_PAG: TDateField
      FieldName = 'DATA_REG_PAG'
    end
    object QTitoloEditDATA_PAG_CCP: TDateField
      FieldName = 'DATA_PAG_CCP'
    end
    object QTitoloEditINCASSATA_DA: TIntegerField
      FieldName = 'INCASSATA_DA'
    end
    object QTitoloEditDATA_IMPORTAZIONE: TDateTimeField
      FieldName = 'DATA_IMPORTAZIONE'
    end
    object QTitoloEditCODICE_IMPORTAZIONE: TIntegerField
      FieldName = 'CODICE_IMPORTAZIONE'
    end
    object QTitoloEditprovvigioni_sub: TCurrencyField
      FieldName = 'provvigioni_sub'
    end
    object QTitoloEditCOD_SUB_AGE: TIntegerField
      FieldName = 'COD_SUB_AGE'
    end
    object QTitoloEditCOD_FC_SUB_AGE: TIntegerField
      FieldName = 'COD_FC_SUB_AGE'
    end
    object QTitoloEditDT_LAST_MOD: TDateField
      FieldName = 'DT_LAST_MOD'
    end
    object QTitoloEditID_GEN_SLP_AGE: TIntegerField
      FieldName = 'ID_GEN_SLP_AGE'
    end
    object QTitoloEditN_STAMPE_Q1: TIntegerField
      FieldName = 'N_STAMPE_Q1'
    end
    object QTitoloEditN_STAMPE_Q2: TIntegerField
      FieldName = 'N_STAMPE_Q2'
    end
    object QTitoloEditN_STAMPE_A1: TIntegerField
      FieldName = 'N_STAMPE_A1'
    end
    object QTitoloEditDesFrazionamento: TStringField
      FieldKind = fkLookup
      FieldName = 'DesFrazionamento'
      LookupDataSet = DMdatiAge.fdmtblFrazionamento
      LookupKeyFields = 'CodFrazionamento'
      LookupResultField = 'DesFrazionamento'
      KeyFields = 'FRAZIONAM'
      Size = 25
      Lookup = True
    end
  end
  object QFindTitolo: TDBISAMQuery
    BeforePost = QTitoloEditBeforePost
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'SELECT * FROM SCADENZE'
      'WHERE COD_SCADENZA = :COD_SCADENZA')
    Params = <
      item
        DataType = ftUnknown
        Name = 'COD_SCADENZA'
      end>
    Left = 200
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'COD_SCADENZA'
      end>
  end
  object QFindTitoloBis: TDBISAMQuery
    BeforePost = QTitoloEditBeforePost
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'SELECT * FROM SCADENZE'
      'WHERE n_polizza=:n_polizza'
      'and tipo_titolo=:tipo_titolo'
      'and rata=:rata'
      'and lordo=:lordo')
    Params = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'lordo'
      end>
    Left = 224
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end
      item
        DataType = ftUnknown
        Name = 'lordo'
      end>
  end
  object QDispagScadenza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update scadenze'
      'set stato = :stato,'
      '      Data_pag = :DataPag,'
      '     provvigioni = :provvigioni,'
      '    descriz        = :descriz'
      'where cod_scadenza = :cod_scadenza')
    Params = <
      item
        DataType = ftString
        Name = 'stato'
      end
      item
        DataType = ftDateTime
        Name = 'DataPag'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftString
        Name = 'descriz'
      end
      item
        DataType = ftInteger
        Name = 'cod_scadenza'
      end>
    Left = 226
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'stato'
      end
      item
        DataType = ftDateTime
        Name = 'DataPag'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftString
        Name = 'descriz'
      end
      item
        DataType = ftInteger
        Name = 'cod_scadenza'
      end>
  end
  object QAutoincScadenza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'SELECT DISTINCT LASTAUTOINC('#39'SCADENZE'#39') AS CODSCAD FROM SCADENZE')
    Params = <>
    Left = 416
    Top = 72
  end
  object Qquiet_con_appendice: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from scadenze'
      'where n_polizza=:n_polizza'
      'and decorrenza=:rata and tipo_titolo='#39'15'#39)
    Params = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end>
    Left = 54
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'rata'
      end>
  end
  object Qappendice_pagata: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from scadenze'
      'where '
      'n_polizza=:polizza'
      'and decorrenza=:decorrenza'
      'and tipo_titolo=:tipo_titolo')
    Params = <
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'decorrenza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
    Left = 134
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'decorrenza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
  end
  object QcopiaTitoloCanc: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update scadenze'
      'set data_ann = :data, causale_annul = :cod_causale'
      'where cod_scadenza = :cod_scadenza ;'
      ''
      'insert into scadenze1'
      '('
      'DENOMINAZ, OLD_COMPAGNIA, RAMO, DESCRIZ, FRAZIONAM, LORDO, RATA,'
      'decorrenza, scadenza, COD_CLI, N_POLIZZA, STATO, DATA_PAG,'
      'FOGLIO_CASSA, MODIFICA, provvigioni, provvigioni_sub,'
      'netto, ptasse, accessori, tasse, valuta,'
      'RIF_COD_COMPAGNIA, RIF_COD_POLIZZA,'
      'inc_indice, TIPO_TITOLO, RIF_CLIENTE,'
      'cod_scad_storica,'
      'DATA_ANN,'
      'CAUSALE_ANNUL'
      ')'
      ''
      
        'select  DENOMINAZ, OLD_COMPAGNIA, RAMO, DESCRIZ, FRAZIONAM, LORD' +
        'O, RATA,'
      'decorrenza, scadenza, COD_CLI, N_POLIZZA, STATO, DATA_PAG,'
      'FOGLIO_CASSA, MODIFICA, provvigioni, provvigioni_sub,'
      'netto, ptasse, accessori, tasse, valuta,'
      'RIF_COD_COMPAGNIA, RIF_COD_POLIZZA,'
      'inc_indice, cast(TIPO_TITOLO as smallint), RIF_CLIENTE,'
      'cod_scadenza as cod_scad_storica,'
      'DATA_ANN,'
      'CAUSALE_ANNUL'
      'from scadenze where cod_scadenza=:cod_scadenza ;'
      ''
      'delete from scadenze '
      'where cod_scadenza = :cod_scadenza')
    Params = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'cod_causale'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
    Left = 554
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'cod_causale'
      end
      item
        DataType = ftUnknown
        Name = 'cod_scadenza'
      end>
  end
  object QincassiPrecedenti: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select count(*) as n_titoli'
      'FROM scadenze'
      'where stato='#39'P'#39'  and rif_cod_compagnia=1'
      'and data_pag<:data')
    Params = <
      item
        DataType = ftUnknown
        Name = 'data'
      end>
    Left = 456
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'data'
      end>
  end
  object QcontaRilievi: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select count(*) as n_rilievi'
      'FROM scadenze'
      'where stato='#39'P'#39'  and rif_cod_compagnia=1'
      'and tipo_titolo='#39'50'#39)
    Params = <>
    Left = 496
    Top = 136
    object Stato: TStringField
      FieldName = 'Stato'
      FixedChar = True
      Size = 1
    end
    object n_polizza: TStringField
      FieldName = 'n_polizza'
      Size = 15
    end
    object denominaz: TStringField
      FieldName = 'denominaz'
      Size = 40
    end
    object Decorrenza: TDateField
      FieldName = 'Decorrenza'
    end
    object Lordo: TCurrencyField
      FieldName = 'Lordo'
    end
    object Provvigioni: TCurrencyField
      FieldName = 'provvigioni'
    end
    object Data_pag: TDateField
      FieldName = 'Data_pag'
    end
    object Fraz: TStringField
      FieldName = 'Fraz'
      FixedChar = True
      Size = 1
    end
    object FrazionamentoLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'FrazionamentoLkup'
      LookupDataSet = DMdatiAge.fdmtblFrazionamento
      LookupKeyFields = 'CodFrazionamento'
      LookupResultField = 'DesFrazionamento'
      KeyFields = 'Fraz'
      Lookup = True
    end
    object cod_scadenza: TAutoIncField
      FieldName = 'cod_scadenza'
    end
    object rif_cod_compagnia: TIntegerField
      FieldName = 'rif_cod_compagnia'
    end
    object CompagniaLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'CompagniaLkup'
      LookupDataSet = DMdatiAge.fdmtblTipoEmiss
      LookupKeyFields = 'CodCompagnia'
      LookupResultField = 'DesTipoEmiss'
      KeyFields = 'rif_cod_compagnia'
      Lookup = True
    end
    object fatta_da: TIntegerField
      FieldName = 'fatta_da'
    end
    object cod_fc_sub_age: TIntegerField
      FieldName = 'cod_fc_sub_age'
    end
    object tipo_titolo: TStringField
      FieldName = 'tipo_titolo'
      Size = 2
    end
    object TipoTitoloLkup: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoTitoloLkup'
      LookupDataSet = QTipoTitolo
      LookupKeyFields = 'SIGLA'
      LookupResultField = 'DESCRIZIONE'
      KeyFields = 'tipo_titolo'
      Size = 30
      Lookup = True
    end
    object netto: TCurrencyField
      FieldName = 'netto'
    end
    object tasse: TCurrencyField
      FieldName = 'tasse'
    end
    object rif_cod_polizza: TIntegerField
      FieldName = 'rif_cod_polizza'
    end
    object data_pag_ccp: TDateField
      FieldName = 'data_pag_ccp'
    end
    object old_compagnia: TStringField
      FieldName = 'old_compagnia'
      Size = 6
    end
    object inpdf: TStringField
      FieldName = 'inpdf'
      Size = 1
    end
    object codice_agenzia: TStringField
      FieldName = 'codice_agenzia'
      Size = 10
    end
    object rif_tipo_polizza: TIntegerField
      FieldName = 'rif_tipo_polizza'
    end
    object tipo_polizza: TStringField
      FieldName = 'tipo_polizza'
      Size = 30
    end
    object provvigioni_sub: TCurrencyField
      FieldName = 'provvigioni_sub'
    end
    object descriz: TStringField
      FieldName = 'descriz'
      Size = 30
    end
  end
  object Qcompagnie: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from compagni'
      'order by cod_compagnia')
    Params = <>
    Left = 440
    Top = 296
  end
  object DScompagnie: TDataSource
    DataSet = Qcompagnie
    Left = 512
    Top = 288
  end
  object QcercaTitoli1011: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select n_polizza, rata, tipo_titolo'
      'from scadenze'
      'where '
      'rif_cod_compagnia = 1'
      'and n_polizza = :n_polizza'
      'and tipo_titolo = :tipo_titolo'
      ''
      'UNION'
      ''
      'select n_polizza, rata, tipo_titolo'
      'from s_scadenze'
      'where '
      'rif_cod_compagnia = 1'
      'and n_polizza = :n_polizza'
      'and tipo_titolo = :tipo_titolo')
    Params = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
    Left = 256
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_titolo'
      end>
  end
  object QcercaPolDaPerfez: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      '-- cerca se esiste la polizze perfezionata'
      'select n_polizza, decorrenza'
      'from slp_tpolizze'
      'where n_polizza = :n_polizza'
      'and status='#39'P'#39)
    Params = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end>
    Left = 256
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'n_polizza'
      end>
  end
  object QQuietanziamento: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    Params = <>
    Left = 584
    Top = 152
  end
  object Qnew_titolo1011: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into scadenze'
      '('
      'DENOMINAZ,'
      'RAMO,'
      'DESCRIZ,'
      'FRAZIONAM,'
      'LORDO,'
      'provvigioni,'
      'RATA,'
      'COD_CLI,'
      'N_POLIZZA,'
      'netto,'
      'ptasse,'
      'tasse,'
      'RIF_COD_COMPAGNIA,'
      'RIF_COD_POLIZZA,'
      'inc_indice,'
      'decorrenza,'
      'scadenza,'
      'TIPO_TITOLO,'
      'valuta,'
      'RIF_CLIENTE,'
      'INCASSATA_DA,'
      'DT_LAST_MOD,'
      'id_gen_slp_age,'
      'stato,'
      'progressivo,'
      'data_pag,'
      'data_pag_ccp,'
      'data_reg_pag'
      ')'
      'VALUES'
      '('
      ':DENOMINAZ,'
      ':RAMO,'
      ':DESCRIZ,'
      ':FRAZIONAM,'
      ':LORDO,'
      ':provvigioni,'
      ':RATA,'
      ':COD_CLI,'
      ':N_POLIZZA,'
      ':netto,'
      ':ptasse,'
      ':tasse,'
      ':RIF_COD_COMPAGNIA,'
      ':RIF_COD_POLIZZA,'
      ':inc_indice,'
      ':decorrenza,'
      ':scadenza,'
      ':TIPO_TITOLO,'
      ':valuta,'
      ':RIF_CLIENTE,'
      ':INCASSATA_DA,'
      ':DT_LAST_MOD,'
      ':id_gen_slp_age,'
      ':stato,'
      ':progressivo,'
      ':data_pag,'
      ':data_pag_ccp,'
      ':data_reg_pag'
      ')')
    Params = <
      item
        DataType = ftString
        Name = 'DENOMINAZ'
      end
      item
        DataType = ftInteger
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'DESCRIZ'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftDate
        Name = 'RATA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'netto'
      end
      item
        DataType = ftCurrency
        Name = 'ptasse'
      end
      item
        DataType = ftCurrency
        Name = 'tasse'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'inc_indice'
      end
      item
        DataType = ftDate
        Name = 'decorrenza'
      end
      item
        DataType = ftDate
        Name = 'scadenza'
      end
      item
        DataType = ftInteger
        Name = 'TIPO_TITOLO'
      end
      item
        DataType = ftString
        Name = 'valuta'
      end
      item
        DataType = ftInteger
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftInteger
        Name = 'INCASSATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftInteger
        Name = 'id_gen_slp_age'
      end
      item
        DataType = ftString
        Name = 'stato'
      end
      item
        DataType = ftInteger
        Name = 'progressivo'
      end
      item
        DataType = ftDateTime
        Name = 'data_pag'
      end
      item
        DataType = ftDateTime
        Name = 'data_pag_ccp'
      end
      item
        DataType = ftUnknown
        Name = 'data_reg_pag'
      end>
    Left = 400
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'DENOMINAZ'
      end
      item
        DataType = ftInteger
        Name = 'RAMO'
      end
      item
        DataType = ftString
        Name = 'DESCRIZ'
      end
      item
        DataType = ftString
        Name = 'FRAZIONAM'
      end
      item
        DataType = ftCurrency
        Name = 'LORDO'
      end
      item
        DataType = ftCurrency
        Name = 'provvigioni'
      end
      item
        DataType = ftDate
        Name = 'RATA'
      end
      item
        DataType = ftInteger
        Name = 'COD_CLI'
      end
      item
        DataType = ftString
        Name = 'N_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'netto'
      end
      item
        DataType = ftCurrency
        Name = 'ptasse'
      end
      item
        DataType = ftCurrency
        Name = 'tasse'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_COMPAGNIA'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_POLIZZA'
      end
      item
        DataType = ftCurrency
        Name = 'inc_indice'
      end
      item
        DataType = ftDate
        Name = 'decorrenza'
      end
      item
        DataType = ftDate
        Name = 'scadenza'
      end
      item
        DataType = ftInteger
        Name = 'TIPO_TITOLO'
      end
      item
        DataType = ftString
        Name = 'valuta'
      end
      item
        DataType = ftInteger
        Name = 'RIF_CLIENTE'
      end
      item
        DataType = ftInteger
        Name = 'INCASSATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DT_LAST_MOD'
      end
      item
        DataType = ftInteger
        Name = 'id_gen_slp_age'
      end
      item
        DataType = ftString
        Name = 'stato'
      end
      item
        DataType = ftInteger
        Name = 'progressivo'
      end
      item
        DataType = ftDateTime
        Name = 'data_pag'
      end
      item
        DataType = ftDateTime
        Name = 'data_pag_ccp'
      end
      item
        DataType = ftUnknown
        Name = 'data_reg_pag'
      end>
  end
  object fdmtblScadenzeTemp: TFDMemTable
    Indexes = <
      item
        Active = True
        Selected = True
        Name = 'IxPolizza'
        Fields = 'n_polizza'
      end
      item
        Name = 'IxContraente'
        Fields = 'denominaz'
      end
      item
        Name = 'IxScadenza'
        Fields = 'scadenza'
      end>
    IndexesActive = False
    IndexName = 'IxPolizza'
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 584
    Top = 256
    object fdmtblScadenzeTempprogressivo: TAutoIncField
      FieldName = 'progressivo'
      Origin = 'scadenzeTemp.progressivo'
    end
    object fdmtblScadenzeTempdenominaz: TStringField
      FieldName = 'denominaz'
      Origin = 'scadenzeTemp.denominaz'
      Size = 50
    end
    object fdmtblScadenzeTempn_polizza: TStringField
      FieldName = 'n_polizza'
      Origin = 'scadenzeTemp.n_polizza'
      Size = 15
    end
    object fdmtblScadenzeTempdecorrenza: TDateField
      FieldName = 'decorrenza'
      Origin = 'scadenzeTemp.decorrenza'
    end
    object fdmtblScadenzeTempscadenza: TDateField
      FieldName = 'scadenza'
      Origin = 'scadenzeTemp.scadenza'
    end
    object fdmtblScadenzeTempfrazionam: TStringField
      FieldName = 'frazionam'
      Origin = 'scadenzeTemp.frazionam'
      Size = 1
    end
    object fdmtblScadenzeTempcompagnia: TStringField
      FieldName = 'compagnia'
      Origin = 'scadenzeTemp.compagnia'
      Size = 25
    end
    object fdmtblScadenzeTempramo: TStringField
      FieldName = 'ramo'
      Origin = 'scadenzeTemp.ramo'
      Size = 25
    end
    object fdmtblScadenzeTemplordo: TCurrencyField
      FieldName = 'lordo'
      Origin = 'scadenzeTemp.lordo'
    end
    object fdmtblScadenzeTempnetto: TCurrencyField
      FieldName = 'netto'
    end
    object fdmtblScadenzeTempTasse: TCurrencyField
      FieldName = 'Tasse'
    end
    object fdmtblScadenzeTempPTasse: TCurrencyField
      FieldName = 'PTasse'
    end
    object fdmtblScadenzeTempprod_nome: TStringField
      FieldName = 'prod_nome'
      Origin = 'scadenzeTemp.prod_nome'
    end
    object fdmtblScadenzeTempprod_sigla: TStringField
      FieldName = 'prod_sigla'
      Origin = 'scadenzeTemp.prod_sigla'
      Size = 3
    end
    object fdmtblScadenzeTempoggetto: TStringField
      FieldName = 'oggetto'
      Origin = 'scadenzeTemp.oggetto'
      Size = 40
    end
    object fdmtblScadenzeTempcod_polizza: TIntegerField
      FieldName = 'cod_polizza'
      Origin = 'scadenzeTemp.cod_polizza'
    end
    object fdmtblScadenzeTempcod_cliente: TIntegerField
      FieldName = 'cod_cliente'
      Origin = 'scadenzeTemp.cod_cliente'
    end
    object fdmtblScadenzeTempcod_compagnia: TIntegerField
      FieldName = 'cod_compagnia'
      Origin = 'scadenzeTemp.cod_compagnia'
    end
    object fdmtblScadenzeTempscadenza_annuale: TStringField
      FieldName = 'scadenza_annuale'
      Origin = 'scadenzeTemp.scadenza_annuale'
      Size = 1
    end
    object fdmtblScadenzeTemptelefono: TStringField
      FieldName = 'telefono'
      Origin = 'scadenzeTemp.telefono'
      Size = 30
    end
    object fdmtbPolizzaScadenzeTempprovvigioni: TCurrencyField
      FieldName = 'provvigioni'
      Origin = 'scadenzeTemp.provvigioni'
    end
    object fdmtblScadenzeTemptipo_prov: TStringField
      FieldName = 'tipo_prov'
      Origin = 'scadenzeTemp.tipo_prov'
      Size = 1
    end
    object fdmtblScadenzeTempprovvigioni_sub: TCurrencyField
      FieldName = 'provvigioni_sub'
      Origin = 'scadenzeTemp.provvigioni_sub'
    end
    object fdmtblScadenzeTempindirizzo: TStringField
      FieldName = 'indirizzo'
      Size = 50
    end
    object fdmtblScadenzeTempcitta: TStringField
      FieldName = 'citta'
      Size = 30
    end
    object fdmtblScadenzeTempcap: TStringField
      FieldName = 'cap'
      Size = 5
    end
    object fdmtblScadenzeTempprovincia: TStringField
      FieldName = 'provincia'
      Size = 2
    end
    object fdmtblScadenzeTempcod_fisc: TStringField
      FieldName = 'cod_fisc'
      Size = 16
    end
    object fdmtblScadenzeTempStato: TStringField
      FieldName = 'Stato'
      Size = 1
    end
    object fdmtblScadenzeTempTipo_titolo: TStringField
      FieldName = 'Tipo_titolo'
      Size = 2
    end
  end
  object QFindScadenza: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select cod_scadenza'
      'from scadenze'
      'where N_POLIZZA = :Polizza'
      'and DECORRENZA = :Decorrenza'
      'and Tipo_Titolo = '#39'20'#39)
    Params = <
      item
        DataType = ftUnknown
        Name = 'Polizza'
      end
      item
        DataType = ftUnknown
        Name = 'Decorrenza'
      end>
    Left = 584
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Polizza'
      end
      item
        DataType = ftUnknown
        Name = 'Decorrenza'
      end>
  end
  object FDDataMove: TFDDataMove
    TextDataDef.Fields = <
      item
        DataType = atOther
        FieldSize = 0
        Precision = 0
      end>
    TextFileName = 'Data.txt'
    TextAnalyze = [taDelimSep, taHeader, taFields]
    Mappings = <>
    LogFileName = 'Data.log'
    DestinationKind = skText
    Source = fdmtblScadenzeTemp
    Left = 584
    Top = 320
  end
end
