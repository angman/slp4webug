unit UDMDatiTempPolizzaPM;

interface

uses
  SysUtils, Classes, UDMDatiTempPolizza, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TDMDatiTempPolizzaPM = class(TDMDatiTempPolizza)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function DMDatiTempPolizzaPM: TDMDatiTempPolizzaPM;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule;

function DMDatiTempPolizzaPM: TDMDatiTempPolizzaPM;
begin
  Result := TDMDatiTempPolizzaPM(UniMainModule.GetModuleInstance(TDMDatiTempPolizzaPM));
end;

initialization
  RegisterClass(TDMDatiTempPolizzaPM);

end.
