unit UMDatiTempPolizzaFM;

interface

uses
  SysUtils, Classes, UDMDatiTempPolizza, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, frxClass, frxExportBaseDialog, frxExportPDF, frxGZip;

type
  TDMDatiTempPolizzaFM = class(TDMDatiTempPolizza)
    fdmtblPolizzaIndicizzata: TBooleanField;
    fdmtblPolizzaEStensione12: TBooleanField;
    fdmtblPolizzaEstensione13: TBooleanField;
    fdmtblGaranziaRischioB: TBooleanField;
    fdmtblGaranziaRischioBDecremento: TCurrencyField;
    procedure fdmtblPolizzaNewRecord(DataSet: TDataSet);
    procedure fdmtblPolizzaIndicizzataChange(Sender: TField);
    procedure fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
    procedure fdmtblPolizzaEStensione12Change(Sender: TField);
    procedure fdmtblPolizzaEstensione13Change(Sender: TField);
    procedure fdmtblGaranziaNewRecord(DataSet: TDataSet);
    procedure fdmtblAssicuratoNewRecord(DataSet: TDataSet);
    procedure fdmtblPolizzaNPercAccessoriChange(Sender: TField);
  private
    { Private declarations }
    lEstensione12, lEstensione13: Currency;
  protected
    procedure preparaReport; override;

    procedure MemorizzaTempAssicurato(CodPolizza: Integer); override;
    procedure MemorizzaTempGaranzie(TipoPolizza: Integer); override;
    function per_allegati: string; override;

  public
    { Public declarations }
    procedure cal_premio; override;
    procedure DoControlli(BaseOnly: Boolean = False); override;

  end;

function DMDatiTempPolizzaFM: TDMDatiTempPolizzaFM;

implementation

{$R *.dfm}

uses
  libSLP, UniGUIVars, uniGUIMainModule, MainModule, ServerModule, System.StrUtils,
  UCodiciErroriPolizza, UPolizzaExceptions;

const
  SIGLA_ESTENSIONE_12 = 'A12';
  SIGLA_ESTENSIONE_13 = 'A13';

function DMDatiTempPolizzaFM: TDMDatiTempPolizzaFM;
begin
  Result := TDMDatiTempPolizzaFM(UniMainModule.GetModuleInstance(TDMDatiTempPolizzaFM));
end;

procedure TDMDatiTempPolizzaFM.cal_premio;
var
  premioGaranzia: Currency;
  sig: string;
  // fo: string[1];
  // sco: single;
begin
  // metti il  valore del premio in base ai dati forniti ..
  // conRischioB              := false;
  nettoAnnuoTotNONScontato := 0;
  premioGaranzia           := 0;
  lEstensione12            := 0;
  lEstensione13            := 0;
  // preleva i lpremio dall'archivio garanzie della polizza corrente
  // sig:=padr(inttostr(RGcombinazione.itemindex+1)+'A',' ',5);
  // sig:=padr(inttostr(CBmassimale.itemindex+1)+'A',' ',5);
  // fo  := copy(CBmassimale.Text, 1, 1);
  // sig := padr(copy(CBmassimale.Text, 1, 3), ' ', 5);
  // if cerca_garanzia(tipo_pol, sig) then
  // begin
  // nettoAnnuoTotNONScontato := Qgar_base.fieldbyname('premio').asFloat;
  // end;
  AggiornaNettoPerEstensione(true, siglaMassimale, premioGaranzia);

  // sig:=padr('A12',' ',5);
  // if cerca_garanzia(tipo_pol,sig) then estensioneA12:=nettoAnnuoTotNONScontato/100*Qgar_base.fieldbyname('percentuale').asFloat;
  // sig:=padr('A13',' ',5);
  // if cerca_garanzia(tipo_pol,sig) then estensioneA13:=nettoAnnuoTotNONScontato/100*Qgar_base.fieldbyname('percentuale').asFloat;
  // estensioneA12 := 0;
  // estensioneA13 := 0;

  if fdmtblPolizzaEStensione12.AsBoolean then
  begin
    sig := Copy(siglaMassimale, 1, 1) + SIGLA_ESTENSIONE_12;
    AggiornaNettoPerEstensione(true, sig, lEstensione12);
  end;

  if fdmtblPolizzaEstensione13.AsBoolean then
  begin
    sig := Copy(siglaMassimale, 1, 1) + SIGLA_ESTENSIONE_13;
    AggiornaNettoPerEstensione(true, sig, lEstensione13);
  end;
  fdmtblPolizza.FieldByName('PerAllegati').AsString := per_allegati;
end;

procedure TDMDatiTempPolizzaFM.DoControlli(BaseOnly: Boolean);
begin
  inherited DoControlli(BaseOnly);
  if BaseOnly then
    Exit;

  if not isPersonaFisica(fdmtblPolizzaCodiceFiscale.AsString) then
    raise EPolizzaError.Create(CERR_PERS_GIURIDICA, MSG_PERS_GIURIDICA);
end;

procedure TDMDatiTempPolizzaFM.fdmtblAssicuratoNewRecord(DataSet: TDataSet);
begin
  inherited;
  fdmtblAssicuratoDenominazione.AsString := 'DIMORA SALTUARIA IN ITALIA';
  fdmtblAssicuratoTipo.AsString          := 'A';
end;

procedure TDMDatiTempPolizzaFM.fdmtblGaranziaNewRecord(DataSet: TDataSet);
begin
  inherited;
  fdmtblGaranziaRischioB.AsBoolean            := False;
  fdmtblGaranziaRischioBDecremento.AsCurrency := 0;
end;

procedure TDMDatiTempPolizzaFM.fdmtblPolizzaEStensione12Change(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaFM.fdmtblPolizzaEstensione13Change(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaFM.fdmtblPolizzaIndicizzataChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaFM.fdmtblPolizzaNewRecord(DataSet: TDataSet);
begin
  inherited;
  fdmtblPolizzaIndicizzata.AsBoolean          := False;
  fdmtblPolizzaEStensione12.AsBoolean         := False;
  fdmtblPolizzaEstensione13.AsBoolean         := False;
  fdmtblGaranziaRischioB.AsBoolean            := False;
  fdmtblGaranziaRischioBDecremento.AsCurrency := 0;

end;

procedure TDMDatiTempPolizzaFM.fdmtblPolizzaNPercAccessoriChange(
  Sender: TField);
begin
  inherited;
  //
end;

procedure TDMDatiTempPolizzaFM.fdmtblPolizzaSiglaMassimaleChange(Sender: TField);
begin
  calcolaPremio;
end;

procedure TDMDatiTempPolizzaFM.MemorizzaTempAssicurato(CodPolizza: Integer);
begin
  // non deve fare nulla - ma deve essere implementato vuoto per evitare abstracterror
end;

procedure TDMDatiTempPolizzaFM.MemorizzaTempGaranzie(TipoPolizza: Integer);
begin
  inherited;

  if fdmtblPolizzaEStensione12.AsBoolean then
  begin
    MemorizzaTempGaranzia(CodPolizza, Copy(siglaMassimale, 1, 1) + SIGLA_ESTENSIONE_12, lEstensione12);
  end;

  if fdmtblPolizzaEstensione13.AsBoolean then
  begin
    MemorizzaTempGaranzia(CodPolizza, Copy(siglaMassimale, 1, 1) + SIGLA_ESTENSIONE_13, lEstensione13);
  end;
end;

function TDMDatiTempPolizzaFM.per_allegati: string;
begin
  // genera la stringa da passare a stamp_moduli per sapere quali crocette mettere
  // nel modulo dell'adeguatezza
  inherited;
  Result := Result + '.V1.V2.V3'; // vita privata

  Result := Copy(Result, 2, 40); // togli il punto iniziale ...
end;

procedure TDMDatiTempPolizzaFM.preparaReport;
const
  FD           = ' - ';
  FMT_IMPORTO  = '#.###.###';
  IMPORTO_VOID = ' // // //';
var
  sgMassimale: string;
begin

  stringaTest                         := QuotedStr(' ');
  StringaChecked                      := QuotedStr('X');
  SLPDatiAge                          := UniMainModule.DMdatiAge.SLPdati_age;
  frxPolizzaGen.EngineOptions.TempDir := SLPDatiAge.dati_gen.dir_temp;
  AzzeraCampiReport;

  frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');

  if isPolizza then
  begin
    frxPolizzaGen.Variables['polizza']   := QuotedStr(UniMainModule.NumPolizza);
    frxPolizzaGen.Variables['barcode1']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 1));
    frxPolizzaGen.Variables['barcode2']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 2));
    frxPolizzaGen.Variables['facsimile'] := QuotedStr('N');
  end
  else
    if isPreventivo then
    begin
      frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
      frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
    end;

  frxPolizzaGen.Variables['ramo']        := QuotedStr(UniServerModule.ramoSLP);
  frxPolizzaGen.Variables['agenzia']     := QuotedStr(SLPDatiAge.agenzia);
  frxPolizzaGen.Variables['citta_age']   := QuotedStr(SLPDatiAge.nome);
  frxPolizzaGen.Variables['descrizione'] := QuotedStr(SLPDatiAge.Denominazione);

  frxPolizzaGen.Variables['codsubag']           := QuotedStr(fdmtblPolizzaSubAgenzia.AsString);
  if frxPolizzaGen.Variables.IndexOf('S_SUBA')>=0 then
     frxPolizzaGen.Variables['s_suba']     := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);
  if frxPolizzaGen.Variables.IndexOf('subP')>=0 then
     frxPolizzaGen.Variables['subP'] := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);

  frxPolizzaGen.Variables['POLSOST']        := QuotedStr(fdmtblPolizzaNumPolizzaSostituita.AsString);
  frxPolizzaGen.Variables['SCADSOST']       := QuotedStr(fdmtblPolizzaScadenzaPolizzaSostituita.AsString);
  frxPolizzaGen.Variables['DATA_EMISSIONE'] := QuotedStr(fdmtblPolizzaDataEmissione.AsString);

  frxPolizzaGen.Variables['EFFETTO']  := QuotedStr(fdmtblPolizzaDecorrenza.AsString);
  frxPolizzaGen.Variables['SCAD']     := QuotedStr(fdmtblPolizzaScadenza.AsString);
  frxPolizzaGen.Variables['DURANNI']  := QuotedStr('-' + trim(fdmtblPolizzaDurataAnni.AsString) + '-');
  frxPolizzaGen.Variables['GIORNI']   := QuotedStr(fdmtblPolizzaDurataGiorni.AsString);
  frxPolizzaGen.Variables['fraz'] := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));

  frxPolizzaGen.Variables['SCADRATA'] := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['DATAEM']   := QuotedStr(fdmtblPolizzaDataEmissione.AsString);

  frxPolizzaGen.Variables['CONTRAENTE'] := QuotedStr(fdmtblPolizzaContraente.AsString);

  frxPolizzaGen.Variables['NATO'] := QuotedStr(fdmtblPolizzaLocNascita.AsString);
  frxPolizzaGen.Variables['IL']   := QuotedStr(fdmtblPolizzaDataNascita.AsString);

  frxPolizzaGen.Variables['RESID'] := QuotedStr(fdmtblPolizzaIndirizzo.AsString + ' - ' + fdmtblPolizzaCitta.AsString);
  frxPolizzaGen.Variables['CAP']   := QuotedStr(fdmtblPolizzaCap.AsString);

  frxPolizzaGen.Variables['CF'] := QuotedStr(fdmtblPolizzaCodiceFiscale.AsString);

  frxPolizzaGen.Variables['dimora_saltuaria'] :=
    QuotedStr(fdmtblAssicuratoIndirizzo.AsString + FD + fdmtblAssicuratoCAP.AsString + FD +
    fdmtblAssicuratoCitta.AsString + fdmtblAssicuratoProvincia.AsString);

  getImportoGaranzia(Copy(fdmtblPolizzaSiglaMassimale.AsString, 1, 2));
  frxPolizzaGen.Variables['massimale'] := QuotedStr(zero(fdmtblGaranziaMassimale.AsCurrency));

  frxPolizzaGen.Variables['ADEGNO'] := ifThen(fdmtblPolizzaIndicizzata.AsBoolean, QuotedStr('SI'), QuotedStr('NO'));;
  frxPolizzaGen.Variables['ADEGSI'] := ifThen(fdmtblPolizzaIndicizzata.AsBoolean, QuotedStr('SI'), QuotedStr('NO'));

  frxPolizzaGen.Variables['premioBase'] :=
    QuotedStr(zero(fdmtblGaranziaPremio.AsCurrency - fdmtblPolizzaArrotondamento.AsCurrency));

  frxPolizzaGen.Variables['X1'] := ifThen(fdmtblPolizzaEStensione12.AsBoolean, StringaChecked, stringaTest);
  frxPolizzaGen.Variables['X2'] := ifThen(fdmtblPolizzaEstensione13.AsBoolean, StringaChecked, stringaTest);

  if fdmtblPolizzaEStensione12.AsBoolean then
  begin
    sgMassimale                          := Copy(siglaMassimale, 1, 1) + 'A12';
    frxPolizzaGen.Variables['premioA12'] := QuotedStr(zero(getImportoGaranzia(sgMassimale)));
  end
  else
    frxPolizzaGen.Variables['premioA12'] := QuotedStr(IMPORTO_VOID);

  if fdmtblPolizzaEstensione13.AsBoolean then
  begin
    sgMassimale                          := Copy(siglaMassimale, 1, 1) + 'A13';
    frxPolizzaGen.Variables['premioA13'] := QuotedStr(zero(getImportoGaranzia(sgMassimale)));
  end
  else
    frxPolizzaGen.Variables['premioA13'] := QuotedStr(IMPORTO_VOID);

  // frxPolizzaGen.Variables['premio1'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));

  if fdmtblPolizzaSconto.AsCurrency<>0 then
  begin
    frxPolizzaGen.Variables['perc_sconto'] := QuotedStr(zero(fdmtblPolizzaPSconto.AsCurrency) + '%');
    frxPolizzaGen.Variables['sc1'] := QuotedStr(zero(fdmtblPolizzaSconto.AsCurrency));  //QuotedStr(zero(Opremio.scontoDurataBase));
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
    frxPolizzaGen.Variables['nettoscontato'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
    // frxPolizzaGen.Variables['premio1'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
    frxPolizzaGen.Variables['premio1'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));
  end
  else
  begin
    frxPolizzaGen.Variables['sc1']     := QuotedStr('=======');
    frxPolizzaGen.Variables['nettoscontato'] := QuotedStr('=======');
    frxPolizzaGen.Variables['premio1'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));
  end;


  frxPolizzaGen.Variables['FRAZRATA'] := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['PN1']      := QuotedStr(zero(fdmtblPolizzaNetto1.AsCurrency));
  frxPolizzaGen.Variables['ACCESS1']  := QuotedStr(zero(fdmtblPolizzaAccessori1.AsCurrency));
  frxPolizzaGen.Variables['FRAZ1']    := QuotedStr(zero(fdmtblPolizzaInteressiFrazionamento1.AsCurrency));
  frxPolizzaGen.Variables['IMP1']  := QuotedStr(zero(fdmtblPolizzaImponibile1.AsCurrency));
  frxPolizzaGen.Variables['TAX1']  := QuotedStr(zero(fdmtblPolizzaImposte1.AsCurrency));
  frxPolizzaGen.Variables['PREM1'] := QuotedStr(zero(fdmtblPolizzaTotale1.AsCurrency));

  frxPolizzaGen.Variables['RATFINAL'] := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['PN2']      := QuotedStr(zero(fdmtblPolizzaNetto2.AsCurrency));
  frxPolizzaGen.Variables['ACCESS2']  := QuotedStr(zero(fdmtblPolizzaAccessori2.AsCurrency));
  frxPolizzaGen.Variables['FRAZ2']    := QuotedStr(zero(fdmtblPolizzaRimborsoSost.AsCurrency));
  frxPolizzaGen.Variables['IMP2']     := QuotedStr(zero(fdmtblPolizzaImponibile2.AsCurrency));
  frxPolizzaGen.Variables['TAX2']     := QuotedStr(zero(fdmtblPolizzaImposte2.AsCurrency));
  frxPolizzaGen.Variables['PREM2']    := QuotedStr(zero(fdmtblPolizzaTotale2.AsCurrency));

  // su una polizza non circolazione il STR non lo mettiamo
  // if UniMainModule.DMdatiAge.TacitoRinnovoEnabled and fdmtblPolizzaTacitoRinnovo.AsBoolean then
  //  frxPolizzaGen.Variables['STR'] := QuotedStr('S');
end;

initialization

RegisterClass(TDMDatiTempPolizzaFM);

end.
