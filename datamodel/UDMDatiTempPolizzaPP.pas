unit UDMDatiTempPolizzaPP;

interface

uses
  SysUtils, Classes, UDMDatiTempPolizza, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, frxClass, frxExportBaseDialog, frxExportPDF, UTipoSalvataggioDocumento,
  frxGZip;

type
  TDMDatiTempPolizzaPP = class(TDMDatiTempPolizza)
    fdmtblAssicuratoGaranziaA: TBooleanField;
    fdmtblAssicuratoGaranziaB: TBooleanField;
    fdmtblAssicuratoGaranziaC: TBooleanField;
    fdmtblAssicuratoPremioAnnuoLordo: TCurrencyField;
    fdmtblAssicuratoPremioNettoSLP: TCurrencyField;
    fdmtblAssicuratoMassimaleGaranzia: TCurrencyField;
    fdmtblAssicuratoSelectedGaranzie: TStringField;
    fdmtblAssicuratoGaranziaAgenzia: TStringField;
    fdmtblAssicuratoMassimaleIndex: TIntegerField;
    fdmtblAssicuratoSiglaMassimaleGaranzia: TStringField;
    fdmtblSelGar: TFDMemTable;
    fdmtbSelGarSelectedGaranzia: TBooleanField;
    fdmtblSelGarGarDescr: TStringField;
    fdmtblCalcGaranzie: TFDMemTable;
    fdmtblSelGarIdGaranzia: TFDAutoIncField;
    dsAssicurato: TDataSource;
    fdmtblSelGarCodAssicurato: TIntegerField;
    fdmtblPolizzaindicizzata: TBooleanField;
    fdmtblPolizzadescrizione: TStringField;
    fdmtblPolizzaNUMEROASSICURATI: TIntegerField;
    fdmtblAssicuratoSelGarStream: TBlobField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure fdmtblAssicuratoNewRecord(DataSet: TDataSet);
    procedure fdmtblAssicuratoGaranziaAgenziaChange(Sender: TField);
    procedure fdmtblAssicuratoPremioAnnuoLordoChange(Sender: TField);
    procedure fdmtblAssicuratoMassimaleGaranziaChange(Sender: TField);
    procedure fdmtblAssicuratoBeforePost(DataSet: TDataSet);
    procedure fdmtblAssicuratoSiglaMassimaleGaranziaChange(Sender: TField);
    procedure fdmtblAssicuratoGaranziaAChange(Sender: TField);
    procedure fdmtblAssicuratoAfterScroll(DataSet: TDataSet);
    procedure fdmtblAssicuratoAfterPost(DataSet: TDataSet);
    procedure fdmtblAssicuratoAfterDelete(DataSet: TDataSet);
    procedure fdmtblPolizzaNPercAccessoriChange(Sender: TField);
  private
    { Private declarations }

    giallo, bianco, blu: TStringList;

    AssicuratoNewRec: Boolean;

    procedure AggiornaCheckSelectedGaranzie;
    procedure AggiornaSelectedGaranzie;
  protected
    function dai_premio(PercentualePremio: currency; premioB: currency): currency;
    function calcolaImportoGaranzia(GaranziaChecked: Boolean; MassimaleGaranzia: string;
      ImportoGaranzia: TCurrencyField; liv: Integer): currency;
    procedure preparaReport; override;
    procedure MemorizzaTempAssicurati(CodPolizza: Integer); override;

    procedure MemorizzaTempGaranzia(CodPolizza: Integer; const Sigla: string; PremioCalcolato: currency = 0.0);
      override;
    procedure MemorizzaTempGaranzie(TipoPolizza: Integer); override;
    procedure componiColori;
    function per_allegati: string; override;
    procedure DoCalcolaPremio;
  public
    { Public declarations }
    procedure DoControlli(BaseOnly: Boolean = False); override;
    procedure cal_premio; override;

  end;

function DMDatiTempPolizzaPP: TDMDatiTempPolizzaPP;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIMainModule, MainModule, libreria, UdmdatiAge, ServerModule, System.StrUtils, System.Math, libSLP,
  UCodiciErroriPolizza, System.DateUtils, System.RegularExpressions, System.Types, UPolizzaExceptions;

function DMDatiTempPolizzaPP: TDMDatiTempPolizzaPP;
begin
  Result := TDMDatiTempPolizzaPP(UniMainModule.GetModuleInstance(TDMDatiTempPolizzaPP));
end;

{ TDMDatiTempPolizzaPP }

procedure TDMDatiTempPolizzaPP.cal_premio;
var
  liv: Integer;
begin
  nettoAnnuoTotNONScontato              := 0;
  fdmtblPolizzaNettoAnnuoTot.AsCurrency := 0.0;
  fdmtblCalcGaranzie.CloneCursor(fdmtblAssicurato, True, False);

  fdmtblCalcGaranzie.First;

  while not fdmtblCalcGaranzie.eof do
  begin

    liv := 0;
    fdmtblCalcGaranzie.edit;
    fdmtblCalcGaranzie.FieldByName('PremioNettoSLP').AsCurrency := 0.0;
    fdmtblCalcGaranzie.FieldByName('PremioNettoSLP').AsCurrency :=
      calcolaImportoGaranzia(fdmtblCalcGaranzie.FieldByName('GaranziaA').asboolean,
      fdmtblCalcGaranzie.FieldByName('SiglaMassimaleGaranzia').AsString,
      TCurrencyField(fdmtblCalcGaranzie.FieldByName('PremioAnnuoLordo')), liv);

    if fdmtblCalcGaranzie.FieldByName('GaranziaA').asboolean then
      inc(liv);
    fdmtblCalcGaranzie.FieldByName('PremioNettoSLP').AsCurrency := fdmtblCalcGaranzie.FieldByName('PremioNettoSLP')
      .AsCurrency + calcolaImportoGaranzia(fdmtblCalcGaranzie.FieldByName('GaranziaB').asboolean,
      fdmtblCalcGaranzie.FieldByName('SiglaMassimaleGaranzia').AsString,
      TCurrencyField(fdmtblCalcGaranzie.FieldByName('PremioAnnuoLordo')), liv);

    if fdmtblCalcGaranzie.FieldByName('GaranziaB').asboolean then
      inc(liv);
    fdmtblCalcGaranzie.FieldByName('PremioNettoSLP').AsCurrency := fdmtblCalcGaranzie.FieldByName('PremioNettoSLP')
      .AsCurrency + calcolaImportoGaranzia(fdmtblCalcGaranzie.FieldByName('GaranziaC').asboolean,
      fdmtblCalcGaranzie.FieldByName('SiglaMassimaleGaranzia').AsString,
      TCurrencyField(fdmtblCalcGaranzie.FieldByName('PremioAnnuoLordo')), liv);

    // fdmtblCalcGaranzie.Post;

    fdmtblPolizzaNettoAnnuoTot.AsCurrency := fdmtblPolizzaNettoAnnuoTot.AsCurrency +
      fdmtblCalcGaranzie.FieldByName('PremioNettoSLP').AsCurrency;

    fdmtblCalcGaranzie.Next;
  end;

  nettoAnnuoTotNONScontato := fdmtblPolizzaNettoAnnuoTot.AsCurrency;

  fdmtblPolizza.FieldByName('PerAllegati').AsString := per_allegati;
end;

procedure TDMDatiTempPolizzaPP.AggiornaCheckSelectedGaranzie;

  procedure CeckFromStringa;
  var
    SelGarArr: TStringDynArray;
    i: Integer;
  begin
    fdmtblSelGar.DisableControls;
    try

      SelGarArr := SplitString(fdmtblAssicuratoSelectedGaranzie.AsString, ';');
      for i     := Low(SelGarArr) to High(SelGarArr) do
      begin
        fdmtblSelGar.First;
        if SelGarArr[i] = fdmtblSelGarGarDescr.AsString then
        begin
          if not(fdmtblSelGar.state in [dsInsert, dsEdit]) then
            fdmtblSelGar.edit;
          fdmtbSelGarSelectedGaranzia.asboolean := True;
          Break;
        end;
        fdmtblSelGar.Next;
      end;
    finally
      fdmtblSelGar.EnableControls;
    end;

  end;

  procedure CheckFromStream;
  var
    st: TMemoryStream;
  begin
    st := TMemoryStream.Create;
    try
      fdmtblAssicuratoSelGarStream.SaveToStream(st);
      st.Position := 0;
      fdmtblSelGar.LoadFromStream(st);
    finally
      st.Free;
    end;
  end;

begin
  if fdmtblAssicuratoSelGarStream.IsNull then
    CeckFromStringa
  else
    CheckFromStream;
end;

procedure TDMDatiTempPolizzaPP.AggiornaSelectedGaranzie;
var
  separator: string;
begin
  fdmtblAssicuratoSelectedGaranzie.AsString := '';
  separator                                 := '';
  fdmtblSelGar.DisableControls;
  try
    fdmtblSelGar.First;
    while not fdmtblSelGar.eof do
    begin
      if fdmtbSelGarSelectedGaranzia.asboolean then
      begin
        fdmtblAssicuratoSelectedGaranzie.AsString := fdmtblAssicuratoSelectedGaranzie.AsString + separator +
          fdmtblSelGarGarDescr.AsString;
        separator := ';';
      end;
      fdmtblSelGar.Next;
    end;
  finally
    fdmtblSelGar.EnableControls;
  end;
end;

function TDMDatiTempPolizzaPP.calcolaImportoGaranzia(GaranziaChecked: Boolean; MassimaleGaranzia: string;
  ImportoGaranzia: TCurrencyField; liv: Integer): currency;
var
  sgMassimale: string;
  TotaleNonScontato: currency;
  PercentualePremio, MaxScelto: currency;

  function GetIndexMassimale: Integer;
  var
    StrAr: TArray<string>;
  begin
    Result := 0;
    StrAr  := TRegex.split(MassimaleGaranzia, '\W+');
    if Length(StrAr) > 0 then
      Result := StrToInt(Copy(StrAr[0], 2, 1));
  end;

begin
  Result            := 0.0;
  TotaleNonScontato := 0.0;
  if GaranziaChecked then
  begin
    inc(liv);
    if liv < 2 then
    begin
      sgMassimale := 'M' + intToStr(GetIndexMassimale + (liv - 1) * 10);
      if UniMainModule.DMDatiBasePolizza.cercaGaranzia(UniMainModule.TipoPolizza, sgMassimale) then
      begin
        PercentualePremio := UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('PERCENTUALE').AsCurrency;
        TotaleNonScontato := dai_premio(PercentualePremio, ImportoGaranzia.AsCurrency);
        MaxScelto         := UniMainModule.DMDatiBasePolizza.Qgar_base.FieldByName('massimale').AsCurrency;
        fdmtblCalcGaranzie.FieldByName('Massimale').AsCurrency := MaxScelto;
        Result := TotaleNonScontato;
      end;
    end
    else
    begin
      Result := TotaleNonScontato + IfThen(liv < 2, 0, 20);
    end;
    // if ImportoGaranzia.AsCurrency < 15 then
    // ImportoGaranzia.AsCurrency := 15;
    Result := Max(15, Result);
  end;

end;

function TDMDatiTempPolizzaPP.dai_premio(PercentualePremio: currency; premioB: currency): currency;
// var
// Divisore: Integer;
// i: Integer;
begin
  Result := 0;
  // for i  := Low(MassimaliAr) to High(MassimaliAr) do
  // if MassimaleSelezionato = MassimaliAr[i, 0] then
  // begin
  // Divisore := StrToInt(MassimaliAr[i, 1]);
  // Result   := premioB / 100 * Divisore + 10;
  // Break;
  // end;
  Result := premioB / 100 * PercentualePremio + 10;
  if (Result < 15) then // or (premioB/100*5+10 <15) then
  begin
    // in data 23/06/2016 rimesso blocco: se siamo a premio minimo allora l'unico massimale selezionabile ' di 2.500 euro
    // indice := 0;
    Result := 15;
  end;

end;

procedure TDMDatiTempPolizzaPP.DataModuleCreate(Sender: TObject);
begin
  inherited;
  giallo := TStringList.Create;
  bianco := TStringList.Create;
  blu    := TStringList.Create;

  giallo.Add('Abitazione');
  giallo.Add('Globale fabbricati');
  giallo.Add('RCT / RCO');
  giallo.Add('RC diversi');
  // Giallo.Add('Incendio');
  giallo.Add('Furto / Incendio');
  giallo.Add('RC professionale');
  giallo.Add('Trasporti');

  // Bianco.Add('Furto / incendio');
  bianco.Add('malattia');
  bianco.Add('Infortuni');
  // Bianco.Add('RC professionale');

  // Blu.Add('RC professionale');
  // Blu.Add('Incendio');
  // Blu.Add('Trasporti');
  AssicuratoNewRec := False;
  fdmtblSelGar.CreateDataSet;

end;

procedure TDMDatiTempPolizzaPP.DataModuleDestroy(Sender: TObject);
begin
  giallo.Free;
  bianco.Free;
  blu.Free;

end;

procedure TDMDatiTempPolizzaPP.DoCalcolaPremio;
begin
  if (fdmtblAssicuratoPremioAnnuoLordo.AsCurrency > 0.0) // and (not facsimile)
    and (fdmtblAssicuratoCodFiscIvas.AsString <> '') then
    calcolaPremio;
end;

procedure TDMDatiTempPolizzaPP.DoControlli(BaseOnly: Boolean);
var errore: Boolean;
begin
  inherited DoControlli(BaseOnly);
  if BaseOnly then
    Exit;

  if (fdmtblPolizzaProfessione.AsInteger = 2) then   // caso di MEDICO !!!!!!
  begin
    if (fdmtblAssicuratoGaranziaC.AsBoolean) then errore:=True
    else begin
       fdmtblAssicurato.First;
       errore:=false;
       while not fdmtblAssicurato.eof do
       begin
         if fdmtblAssicurato.FieldByName('GaranziaC').asBoolean then
         begin
            errore:=True;
            Break;
         end;
         fdmtblAssicurato.Next;
       end;
    end;
    if errore then
       raise EPolizzaError.Create(CERR_PROFESSIONE_CONTRAENTE,
             'ATTENZIONE. Vi ricordiamo che La difesa penale non e'' attivabile con Contraenti che esercitano la professione medica.');

  end;

  if (fdmtblAssicuratoCodFiscIvas.AsString <> '') and (fdmtblAssicuratoSelectedGaranzie.AsString = '') then
    // ShowMessage('Bisogna selezionare almeno una garanzia per la polizza '+ Epol1.Text +' !');
    raise EPolizzaError.Create(CERR_GAR_POLIZZA, format(MSG_GAR_POLIZZA, [fdmtblAssicuratoCodFiscIvas.AsString]));

  // if fdmtblAssicuratoSelectedGaranzie.AsString = '' then
  // // ShowMessage('Bisogna indicare il tipo di polizza in garanzia '+ Epol1.Text +' !');
  // raise EPolizzaError.Create(CERR_TIPO_POLIZZA_IN_GAR, format(MSG_TIPO_POLIZZA_IN_GAR,
  // [fdmtblAssicuratoCodFiscIvas.AsString]));

  // if fdmtblAssicuratoCitta.AsString = '' then
  // // ShowMessage('Bisogna indicare la compagnia che ha emesso la polizza in garanzia '+ Epol1.Text +' !');
  // raise EPolizzaError.Create(CERR_COMP_EMITT_IN_GAR, format(MSG_COMP_EMITT_IN_GAR,
  // [fdmtblAssicuratoCodFiscIvas.AsString]));

  // if fdmtblAssicuratoGaranziaAgenzia.AsString = '' then
  // // ShowMessage('Bisogna indicare l''agenzia che ha emesso la polizza in garanzia '+ Epol1.Text +' !');
  // raise EPolizzaError.Create(CERR_AGE_IN_GAR, format(MSG_AGE_IN_GAR, [fdmtblAssicuratoCodFiscIvas.AsString]));

  // if (fdmtblAssicuratoPremioAnnuoLordo.AsCurrency = 0.0) and (fdmtblAssicuratoMassimaleGaranzia.AsString <> '') then
  // // showmessage('E'' obbligatorio inserire il premio della polizza base');
  // raise EPolizzaError.Create(CERR_MANCA_PREMIO_BASE, format(MSG_MANCA_PREMIO_BASE,
  // [fdmtblAssicuratoCodFiscIvas.AsString]));

  if ((year(fdmtblAssicuratoDataRilascio.AsDateTime) < 2000) or (year(fdmtblAssicuratoDataScadenza.AsDateTime) < 2000))
  then
    // showmessage('Contratto 1 - E'' obbligatorio inserire le date di decorrenza/scadenza della polizza in garanzia');
    raise EPolizzaError.Create(CERR_DATE_POLIZZA, format(MSG_DATE_POLIZZA, [fdmtblAssicuratoCodFiscIvas.AsString]));

  if (fdmtblAssicuratoPremioAnnuoLordo.AsCurrency > 0.0) // and (not facsimile)
    and (fdmtblAssicuratoCodFiscIvas.AsString = '') then
    // showmessage('E'' obbligatorio inserire la descrizione delle garanzie della polizza base N� 1');
    raise EPolizzaError.Create(CERR_DESCR_GARANZIE, format(MSG_DESCR_GARANZIE, [fdmtblAssicuratoCodFiscIvas.AsString]));

  if (yearOf(fdmtblAssicuratoDataRilascio.AsDateTime) > 1980) and
    (yearOf(fdmtblAssicuratoDataScadenza.AsDateTime) > 1980) and
    (fdmtblAssicuratoDataRilascio.AsDateTime >= fdmtblAssicuratoDataScadenza.AsDateTime) then
    // showmessage('Le date di effetto e/o scadenza della PRIMA polizza in garanzia non sono coerenti !');
    raise EPolizzaError.Create(CERR_DATE_NON_COERENTI, format(MSG_DATE_NON_COERENTI,
      [fdmtblAssicuratoCodFiscIvas.AsString]));

end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoNewRecord(DataSet: TDataSet);
begin
  inherited;
  InNewRecord := True;

  fdmtblAssicuratoGaranziaAgenzia.OnChange        := nil;
  fdmtblAssicuratoPremioAnnuoLordo.OnChange       := nil;
  fdmtblAssicuratoMassimaleGaranzia.OnChange      := nil;
  fdmtblAssicuratoSiglaMassimaleGaranzia.OnChange := nil;
  fdmtblAssicuratoGaranziaA.OnChange              := nil;
  fdmtblAssicuratoGaranziaB.OnChange              := nil;
  fdmtblAssicuratoGaranziaC.OnChange              := nil;
  try
    fdmtblAssicuratoCodPolizza.AsInteger        := fdmtblPolizzaCodPolizza.AsInteger;
    fdmtblAssicuratoTipo.AsString               := 'C';
    fdmtblAssicuratoGaranziaAgenzia.AsString    := '';
    fdmtblAssicuratoPremioAnnuoLordo.AsCurrency := 0.0;
    fdmtblAssicuratoPremioNettoSLP.AsCurrency   := 0.0;
    fdmtblAssicuratoDataRilascio.Clear;
    fdmtblAssicuratoDataScadenza.Clear;
    fdmtblAssicuratoGaranziaA.asboolean          := False;
    fdmtblAssicuratoGaranziaB.asboolean          := False;
    fdmtblAssicuratoGaranziaC.asboolean          := False;
    fdmtblAssicuratoSelectedGaranzie.AsString    := '';
    fdmtblAssicuratoMassimaleGaranzia.AsCurrency := 0;

  finally
    InNewRecord                                     := False;
    fdmtblAssicuratoPremioAnnuoLordo.OnChange       := fdmtblAssicuratoPremioAnnuoLordoChange;
    fdmtblAssicuratoMassimaleGaranzia.OnChange      := fdmtblAssicuratoMassimaleGaranziaChange;
    fdmtblAssicuratoSiglaMassimaleGaranzia.OnChange := fdmtblAssicuratoSiglaMassimaleGaranziaChange;
    fdmtblAssicuratoGaranziaA.OnChange              := fdmtblAssicuratoGaranziaAChange;
    fdmtblAssicuratoGaranziaB.OnChange              := fdmtblAssicuratoGaranziaAChange;
    fdmtblAssicuratoGaranziaC.OnChange              := fdmtblAssicuratoGaranziaAChange;
  end;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoAfterDelete(DataSet: TDataSet);
begin
  DoCalcolaPremio;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoAfterPost(DataSet: TDataSet);
begin
  DoCalcolaPremio;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoAfterScroll(DataSet: TDataSet);
begin
  AggiornaCheckSelectedGaranzie;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoBeforePost(DataSet: TDataSet);
var
  st: TMemoryStream;
begin
  AggiornaSelectedGaranzie;

  if fdmtblPolizzaProfessione.AsInteger = 0 then
    raise EPolizzaError.Create(CERR_PROFESSIONE_CONTRAENTE, MSG_PROFESSIONE_CONTRAENTE);
  // numero polizza
  if not(fdmtblAssicuratoCodFiscIvas.AsString > '') then
    raise EPolizzaError.Create(CERR_TIPO_POLIZZA_IN_GAR, format(MSG_TIPO_POLIZZA_IN_GAR,
      [fdmtblAssicuratoCodFiscIvas.AsString]));
  // Compagnia emittente
  if (fdmtblAssicuratoCitta.AsString = '') then
    raise EPolizzaError.Create(CERR_COMP_EMITT_IN_GAR, format(MSG_COMP_EMITT_IN_GAR,
      [fdmtblAssicuratoCodFiscIvas.AsString]));
  // Agenzia
  if (fdmtblAssicuratoIndirizzo.AsString = '') then
    raise EPolizzaError.Create(CERR_AGE_IN_GAR, format(MSG_AGE_IN_GAR, [fdmtblAssicuratoCodFiscIvas.AsString]));
  // Premio
  if (fdmtblAssicuratoPremioAnnuoLordo.AsCurrency = 0.0) and (fdmtblAssicuratoSiglaMassimaleGaranzia.AsString <> '')
  then
    raise EPolizzaError.Create(CERR_MANCA_PREMIO_BASE, format(MSG_MANCA_PREMIO_BASE,
      [fdmtblAssicuratoCodFiscIvas.AsString]));

  if (fdmtblAssicuratoPremioAnnuoLordo.AsCurrency > 5000) then
    raise EPolizzaError.Create(CERR_MANCA_PREMIO_BASE, format('Il premio base NON pu� superare i 5.000 euro.',
      [fdmtblAssicuratoCodFiscIvas.AsString]));

  // date
  if (fdmtblAssicuratoPremioAnnuoLordo.AsCurrency = 0.0) and (fdmtblAssicuratoSiglaMassimaleGaranzia.AsString = '')
  then
    raise EPolizzaError.Create(CERR_MANCA_PREMIO_BASE_MASSIMALE, format(MSG_MANCA_PREMIO_BASE_MASSIMALE,
      [fdmtblAssicuratoCodFiscIvas.AsString]));

  if (fdmtblAssicuratoDataRilascio.AsDateTime = MinDateTime) or (fdmtblAssicuratoDataScadenza.AsDateTime = MinDateTime)
  then
    raise EPolizzaError.Create(CERR_DATE_POLIZZA, format(MSG_DATE_POLIZZA, [fdmtblAssicuratoCodFiscIvas.AsString]));

  if fdmtblAssicuratoDataScadenza.AsDateTime <= fdmtblAssicuratoDataRilascio.AsDateTime then
    raise EPolizzaError.Create(CERR_DATE_NON_COERENTI, format(MSG_DATE_NON_COERENTI,
      [fdmtblAssicuratoCodFiscIvas.AsString]));

  if not(fdmtblAssicuratoGaranziaA.asboolean or fdmtblAssicuratoGaranziaB.asboolean or
    fdmtblAssicuratoGaranziaC.asboolean) then
    raise EPolizzaError.Create(CERR_GAR_POLIZZA, format(MSG_GAR_POLIZZA, [fdmtblAssicuratoCodFiscIvas.AsString]));

  if fdmtblAssicuratoSelectedGaranzie.AsString = '' then
    raise EPolizzaError.Create(CERR_DESCR_GARANZIE, format(MSG_DESCR_GARANZIE, [fdmtblAssicuratoCodFiscIvas.AsString]));

  if (fdmtblAssicuratoSiglaMassimaleGaranzia.AsString = '') and (fdmtblAssicuratoPremioAnnuoLordo.AsCurrency > 0.0) then
    raise EPolizzaError.Create(CERR_MASS_GAR_MANCANTE, format(MSG_MASS_GAR_MANCANTE,
      [fdmtblAssicuratoCodFiscIvas.AsString]));

  st := TMemoryStream.Create;
  try
    fdmtblSelGar.SaveToStream(st);
    st.Position := 0;
    fdmtblAssicuratoSelGarStream.LoadFromStream(st);
  finally
    st.Free
  end;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoGaranziaAChange(Sender: TField);
begin
  componiColori;
  DoCalcolaPremio;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoGaranziaAgenziaChange(Sender: TField);
begin
  DoCalcolaPremio;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoMassimaleGaranziaChange(Sender: TField);
begin
  DoCalcolaPremio;

end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoPremioAnnuoLordoChange(Sender: TField);
begin
  DoCalcolaPremio;
end;

procedure TDMDatiTempPolizzaPP.fdmtblAssicuratoSiglaMassimaleGaranziaChange(Sender: TField);
begin
  DoCalcolaPremio;
end;

procedure TDMDatiTempPolizzaPP.fdmtblPolizzaNPercAccessoriChange(
  Sender: TField);
begin
  inherited;
  //
end;

procedure TDMDatiTempPolizzaPP.componiColori;
var
  GarA: Boolean;
  GarB: Boolean;
  GarC: Boolean;
  SelectedGars: TStringList;

  procedure addlist(srcLst: TStringList);
  var
    AString: string;
  begin
    for AString in srcLst do
    begin
      fdmtblSelGar.AppendRecord([nil, False, AString, fdmtblAssicuratoCodAssicurato.AsInteger]);
    end;
  end;

  procedure memorizzaSelGar;
  begin
    SelectedGars.Clear;
    fdmtblSelGar.First;
    while not fdmtblSelGar.eof do
    begin
      if fdmtbSelGarSelectedGaranzia.asboolean then
        SelectedGars.Add(fdmtblSelGarGarDescr.AsString);
      fdmtblSelGar.Next;
    end;

  end;

  procedure ClearGar;
  begin
    memorizzaSelGar;
    fdmtblSelGar.First;
    while not fdmtblSelGar.eof do
      fdmtblSelGar.Delete;

  end;

  procedure RestoreSelectedGars;
  begin
    fdmtblSelGar.First;
    fdmtblSelGar.edit;
    while not fdmtblSelGar.eof do
    begin
      fdmtblSelGar.edit;
      fdmtbSelGarSelectedGaranzia.asboolean := SelectedGars.IndexOf(fdmtblSelGarGarDescr.AsString) > -1;
      fdmtblSelGar.Post;
      fdmtblSelGar.Next;
    end;

  end;

begin
  // prepara le liste per la selezione delle garanzie che si possono selezionare in base
  // al fatto che l'utente abbia scelto la granzia A o B o C o una combinazione di queste
  SelectedGars := TStringList.Create;

  fdmtblSelGar.DisableControls;
  try
    ClearGar;

    fdmtblSelGar.append;
    fdmtblSelGar.edit;

    GarA := fdmtblAssicuratoGaranziaA.asboolean;
    GarB := fdmtblAssicuratoGaranziaB.asboolean;
    GarC := fdmtblAssicuratoGaranziaC.asboolean;

    if (GarA and GarB and GarC) or (GarA and (GarB or GarC)) then // solo il giallo
    begin
      // AItems.AddStrings(giallo);
      addlist(giallo);
    end;

    if (not GarA and (GarB or GarC)) then // il giallo + blu c'e' di sicuro
    begin
      // AItems.AddStrings(giallo);
      addlist(giallo);
      // EEgar1.Items.AddStrings(blu);
    end;
    if (GarA and (not GarB and not GarC)) then // il giallo + bianco c'e' di sicuro
    begin
      // AItems.AddStrings(giallo);
      // AItems.AddStrings(bianco);
      addlist(giallo);
      addlist(bianco);
    end;
    RestoreSelectedGars;
  finally
    fdmtblSelGar.First;
    fdmtblSelGar.EnableControls;
    SelectedGars.Free;
  end;

end;

function TDMDatiTempPolizzaPP.per_allegati: string;
begin
  // genera la stringa da passare a stamp_moduli per sapere quali crocette mettere
  // nel modulo dell'adeguatezza
  inherited;
  Result := Result + '.P1'; // crocetta per perizie

  if fdmtblCalcGaranzie.FieldByName('GaranziaA').asboolean then
    Result := Result + '.P2'; // perizie
  if fdmtblCalcGaranzie.FieldByName('GaranziaB').asboolean then
    Result := Result + '.P3'; // chiamata in causa
  if fdmtblCalcGaranzie.FieldByName('GaranziaC').asboolean then
    Result := Result + '.P4'; // difesa penale

  Result := Copy(Result, 2, 40); // togli il punto iniziale ...
end;

procedure TDMDatiTempPolizzaPP.preparaReport;
var
  // gar_sel1, gar_sel2: string;
  arrotondato: Boolean;
  StringaTest: string;
  StringaChecked: string;
  SLPDatiAge: TSLPdati_age_rec;

  function arrotondaX(cc: currency): currency;
  begin
    Result := cc;
    if arrotondato then
    begin
      Result      := cc - Opremio.arrotondamento;
      arrotondato := False;
    end;
  end;

  function getGaranzieSelezionate: string;
  begin
    Result := '';
    if fdmtblAssicuratoGaranziaA.asboolean then
      Result := 'A.';
    if fdmtblAssicuratoGaranziaB.asboolean then
      Result := Result + 'B.';
    if fdmtblAssicuratoGaranziaC.asboolean then
      Result := Result + 'C';
  end;

  procedure azzeraDatiPolizze;
  begin
      frxPolizzaGen.Variables['X1a'] := QuotedStr('');
      frxPolizzaGen.Variables['X1b'] := QuotedStr('');
      frxPolizzaGen.Variables['X1c'] := QuotedStr('');
      frxPolizzaGen.Variables['NPOL1']   := QuotedStr('==========');
      frxPolizzaGen.Variables['EMESSA1'] := QuotedStr('==========');
      frxPolizzaGen.Variables['AGE1']    := QuotedStr('==========');
      frxPolizzaGen.Variables['PLORDO1'] := QuotedStr(zero(0));
      frxPolizzaGen.Variables['DECO1']   := QuotedStr('');
      frxPolizzaGen.Variables['SCAD1']   := QuotedStr('');
      frxPolizzaGen.Variables['GAR1']    := QuotedStr('==========');
      frxPolizzaGen.Variables['MAX1']    := QuotedStr('=0' + '=');
      frxPolizzaGen.Variables['PNETTO1'] := QuotedStr('=0' + '=');

      frxPolizzaGen.Variables['X2a'] := QuotedStr('');
      frxPolizzaGen.Variables['X2b'] := QuotedStr('');
      frxPolizzaGen.Variables['X2c'] := QuotedStr('');
      frxPolizzaGen.Variables['NPOL2']   := QuotedStr('==========');
      frxPolizzaGen.Variables['EMESSA2'] := QuotedStr('==========');
      frxPolizzaGen.Variables['AGE2']    := QuotedStr('==========');
      frxPolizzaGen.Variables['PLORDO2'] := QuotedStr(zero(0));
      frxPolizzaGen.Variables['DECO2']   := QuotedStr('');
      frxPolizzaGen.Variables['SCAD2']   := QuotedStr('');
      frxPolizzaGen.Variables['GAR2']    := QuotedStr('==========');
      frxPolizzaGen.Variables['MAX2']    := QuotedStr('=0' + '=');
      frxPolizzaGen.Variables['PNETTO2'] := QuotedStr('=0' + '=');

      frxPolizzaGen.Variables['X3a'] := QuotedStr('');
      frxPolizzaGen.Variables['X3b'] := QuotedStr('');
      frxPolizzaGen.Variables['X3c'] := QuotedStr('');
      frxPolizzaGen.Variables['NPOL3']   := QuotedStr('==========');
      frxPolizzaGen.Variables['EMESSA3'] := QuotedStr('==========');
      frxPolizzaGen.Variables['AGE3']    := QuotedStr('==========');
      frxPolizzaGen.Variables['PLORDO3'] := QuotedStr(zero(0));
      frxPolizzaGen.Variables['DECO3']   := QuotedStr('');
      frxPolizzaGen.Variables['SCAD3']   := QuotedStr('');
      frxPolizzaGen.Variables['GAR3']    := QuotedStr('==========');
      frxPolizzaGen.Variables['MAX3']    := QuotedStr('=0' + '=');
      frxPolizzaGen.Variables['PNETTO3'] := QuotedStr('=0' + '=');

      frxPolizzaGen.Variables['X4a'] := QuotedStr('');
      frxPolizzaGen.Variables['X4b'] := QuotedStr('');
      frxPolizzaGen.Variables['X4c'] := QuotedStr('');
      frxPolizzaGen.Variables['NPOL4']   := QuotedStr('==========');
      frxPolizzaGen.Variables['EMESSA4'] := QuotedStr('==========');
      frxPolizzaGen.Variables['AGE4']    := QuotedStr('==========');
      frxPolizzaGen.Variables['PLORDO4'] := QuotedStr(zero(0));
      frxPolizzaGen.Variables['DECO4']   := QuotedStr('');
      frxPolizzaGen.Variables['SCAD4']   := QuotedStr('');
      frxPolizzaGen.Variables['GAR4']    := QuotedStr('==========');
      frxPolizzaGen.Variables['MAX4']    := QuotedStr('=0' + '=');
      frxPolizzaGen.Variables['PNETTO4'] := QuotedStr('=0' + '=');
  end;

  procedure valorizzaDatiPolizza(n: Integer);
  begin
    frxPolizzaGen.Variables['NPOL'+IntToStr(n)]   := QuotedStr(fdmtblAssicuratoCodFiscIvas.AsString);
    frxPolizzaGen.Variables['EMESSA'+IntToStr(n)] := QuotedStr(fdmtblAssicuratoCitta.AsString);
    frxPolizzaGen.Variables['AGE'+IntToStr(n)]    := QuotedStr(fdmtblAssicuratoIndirizzo.AsString);
    frxPolizzaGen.Variables['PLORDO'+IntToStr(n)] := QuotedStr(zero(fdmtblAssicuratoPremioAnnuoLordo.AsCurrency));
    frxPolizzaGen.Variables['DECO'+IntToStr(n)]   := QuotedStr(fdmtblAssicuratoDataRilascio.AsString);
    frxPolizzaGen.Variables['SCAD'+IntToStr(n)]   := QuotedStr(fdmtblAssicuratoDataScadenza.AsString);
    frxPolizzaGen.Variables['GAR'+IntToStr(n)]    := QuotedStr(fdmtblAssicuratoSelectedGaranzie.AsString);

    frxPolizzaGen.Variables['MAX'+IntToStr(n)]    := QuotedStr('=' + zero(fdmtblAssicuratoMassimale.AsCurrency));
    frxPolizzaGen.Variables['pnetto'+IntToStr(n)] :=
      QuotedStr('=' + zero(arrotondaX(fdmtblAssicuratoPremioNettoSLP.AsCurrency)) + '=');

    frxPolizzaGen.Variables['gar_sel'+IntToStr(n)] := QuotedStr(getGaranzieSelezionate);

    if fdmtblAssicuratoGaranziaA.asboolean then
      frxPolizzaGen.Variables['X'+IntToStr(n)+'a'] := QuotedStr('X');
    if fdmtblAssicuratoGaranziaB.asboolean then
      frxPolizzaGen.Variables['X'+IntToStr(n)+'b'] := QuotedStr('X');
    if fdmtblAssicuratoGaranziaC.asboolean then
      frxPolizzaGen.Variables['X'+IntToStr(n)+'c'] := QuotedStr('X');
  end;

begin
  // istanzia i valori
  SLPDatiAge                          := UniMainModule.DMdatiAge.SLPdati_age;
  frxPolizzaGen.EngineOptions.TempDir := SLPDatiAge.dati_gen.dir_temp;

  frxPolizzaGen.Variables['versione']    := QuotedStr(leggiVersione);
  frxPolizzaGen.Variables['premiounico'] := StringaTest;

  arrotondato    := (Opremio.arrotondamento <> 0);
  StringaTest    := QuotedStr(' ');
  StringaChecked := QuotedStr('X');
  AzzeraCampiReport;

  frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
  frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');

  if isPolizza then
  begin
    frxPolizzaGen.Variables['polizza']   := QuotedStr(UniMainModule.NumPolizza);
    frxPolizzaGen.Variables['barcode1']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 1));
    frxPolizzaGen.Variables['barcode2']  := QuotedStr(genera_cbPOL(UniMainModule.NumPolizza, 2));
    frxPolizzaGen.Variables['facsimile'] := QuotedStr('N');
  end
  else
    if isPreventivo then
    begin
      frxPolizzaGen.Variables['polizza']   := QuotedStr('Fac-Simile');
      frxPolizzaGen.Variables['facsimile'] := QuotedStr('S');
    end;

  frxPolizzaGen.Variables['ramo']        := QuotedStr(UniServerModule.ramoSLP);
  frxPolizzaGen.Variables['agenzia']     := QuotedStr(SLPDatiAge.agenzia);
  frxPolizzaGen.Variables['citta_age']   := QuotedStr(SLPDatiAge.nome);
  frxPolizzaGen.Variables['descrizione'] := QuotedStr(SLPDatiAge.Denominazione);
  frxPolizzaGen.Variables['suba']        := QuotedStr(fdmtblPolizzaSubAgenzia.AsString);

  frxPolizzaGen.Variables['s_suba']     := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);
  if frxPolizzaGen.Variables.IndexOf('subP')>=0 then
     frxPolizzaGen.Variables['subP'] := QuotedStr(fdmtblPolizzaSubPromoterSigla.AsString);

  // frxPolizzaGen.Variables['int_da']     := QuotedStr(fdmtblPolizzaintermediataDaSigla.AsString);

  frxPolizzaGen.Variables['Psost']    := QuotedStr(fdmtblPolizzaNumPolizzaSostituita.AsString);
  frxPolizzaGen.Variables['scade_il'] := QuotedStr(fdmtblPolizzaScadenzaPolizzaSostituita.AsString);
  frxPolizzaGen.Variables['effetto']  := QuotedStr(fdmtblPolizzaDecorrenza.AsString);
  frxPolizzaGen.Variables['scadenza'] := QuotedStr(fdmtblPolizzaScadenza.AsString);
  frxPolizzaGen.Variables['durata']   := QuotedStr('-' + fdmtblPolizzaDurataAnni.AsString + '-');
  frxPolizzaGen.Variables['appe']     := QuotedStr(' / / /');

  frxPolizzaGen.Variables['giorni'] := QuotedStr(IfThen(fdmtblPolizzaDurataGiorni.AsString = '0', '',
    fdmtblPolizzaDurataGiorni.AsString));

  frxPolizzaGen.Variables['fraz']      := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['scad1rata'] := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['emissione'] := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  frxPolizzaGen.Variables['fraz']      := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['scad1rata'] := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);

  frxPolizzaGen.Variables['emissione']  := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  frxPolizzaGen.Variables['contraente'] := QuotedStr(fdmtblPolizzaContraente.AsString);
  frxPolizzaGen.Variables['nato_a']     := QuotedStr(fdmtblPolizzaLocNascita.AsString);
  frxPolizzaGen.Variables['nato_il']    := QuotedStr(IfThen(yearOf(fdmtblPolizzaDataNascita.AsDateTime) > 1900,
    fdmtblPolizzaDataNascita.AsString, ''));

  frxPolizzaGen.Variables['residente_in'] :=
    QuotedStr(fdmtblPolizzaIndirizzo.AsString + ' - ' + fdmtblPolizzaCitta.AsString + ' - ' +
    fdmtblPolizzaProvincia.AsString);

  frxPolizzaGen.Variables['cap']         := QuotedStr(fdmtblPolizzaCap.AsString);
  frxPolizzaGen.Variables['professione'] :=
    QuotedStr(UniMainModule.DMdatiAgeClienti.getDescrizProfessione(fdmtblPolizzaProfessione.AsInteger));

  // frxPolizzaGen.Variables['telefono'] := QuotedStr('Numero telefonico');
  { TODO -oMB : inserire il campo per telefono e email nella form di inserimento }
  // QuotedStr(alltrim(alltrim(Ee_mail.text) + ' ' + alltrim(Etelefono.text)));
  frxPolizzaGen.Variables['cf_iva'] := QuotedStr(IfThen(fdmtblPolizzaPartitaIva.AsString > '',
    fdmtblPolizzaPartitaIva.AsString, fdmtblPolizzaCodiceFiscale.AsString));
  // frxPolizzaGen.Variables['e_mail'] := QuotedStr(Ee_mail.text);

  // parte per indice istat
  frxPolizzaGen.Variables['ADEGSI'] := IfThen(fdmtblPolizzaindicizzata.asboolean, QuotedStr('SI'), QuotedStr('NO'));

  frxPolizzaGen.Variables['VARIE'] := StringaTest;

  frxPolizzaGen.Variables['gar_sel1'] := QuotedStr('');
  frxPolizzaGen.Variables['gar_sel2'] := QuotedStr('');

  // compila la parte relativa alle polizze in garanzia
  // prima polizza in garanzia -----------------------------------------------------------
  try
    azzeraDatiPolizze;

    fdmtblAssicurato.DisableControls;
    fdmtblAssicurato.First;

    valorizzaDatiPolizza(1);
    {
    frxPolizzaGen.Variables['NPOL1']   := QuotedStr(fdmtblAssicuratoCodFiscIvas.AsString);
    frxPolizzaGen.Variables['EMESSA1'] := QuotedStr(fdmtblAssicuratoCitta.AsString);
    frxPolizzaGen.Variables['AGE1']    := QuotedStr(fdmtblAssicuratoIndirizzo.AsString);
    frxPolizzaGen.Variables['PLORDO1'] := QuotedStr(zero(fdmtblAssicuratoPremioAnnuoLordo.AsCurrency));
    frxPolizzaGen.Variables['DECO1']   := QuotedStr(fdmtblAssicuratoDataRilascio.AsString);
    frxPolizzaGen.Variables['SCAD1']   := QuotedStr(fdmtblAssicuratoDataScadenza.AsString);
    frxPolizzaGen.Variables['GAR1']    := QuotedStr(fdmtblAssicuratoSelectedGaranzie.AsString);

    // frxPolizzaGen.Variables['MAX1']    := QuotedStr('=' + fdmtblAssicuratoMassimaleGaranzia.AsString + '=');
    frxPolizzaGen.Variables['MAX1']    := QuotedStr('=' + zero(fdmtblAssicuratoMassimale.AsCurrency));
    frxPolizzaGen.Variables['pnetto1'] :=
      QuotedStr('=' + zero(arrotondaX(fdmtblAssicuratoPremioNettoSLP.AsCurrency)) + '=');

    frxPolizzaGen.Variables['gar_sel1'] := QuotedStr(getGaranzieSelezionate);

    if fdmtblAssicuratoGaranziaA.asboolean then
      frxPolizzaGen.Variables['X1a'] := QuotedStr('X');
    if fdmtblAssicuratoGaranziaB.asboolean then
      frxPolizzaGen.Variables['X1b'] := QuotedStr('X');
    if fdmtblAssicuratoGaranziaC.asboolean then
      frxPolizzaGen.Variables['X1c'] := QuotedStr('X');
    }
    fdmtblAssicurato.Next;
    // seconda polizza in garanzia -----------------------------------------------------------
    if not fdmtblAssicurato.eof then
    begin
      valorizzaDatiPolizza(2);
      {
      if fdmtblAssicuratoGaranziaA.asboolean then
        frxPolizzaGen.Variables['X2a'] := QuotedStr('X');
      if fdmtblAssicuratoGaranziaB.asboolean then
        frxPolizzaGen.Variables['X2b'] := QuotedStr('X');
      if fdmtblAssicuratoGaranziaC.asboolean then
        frxPolizzaGen.Variables['X2c'] := QuotedStr('X');

      frxPolizzaGen.Variables['gar_sel2'] := QuotedStr(getGaranzieSelezionate);

      frxPolizzaGen.Variables['NPOL2']   := QuotedStr(fdmtblAssicuratoCodFiscIvas.AsString);
      frxPolizzaGen.Variables['EMESSA2'] := QuotedStr(fdmtblAssicuratoCitta.AsString);
      frxPolizzaGen.Variables['AGE2']    := QuotedStr(fdmtblAssicuratoIndirizzo.AsString);
      frxPolizzaGen.Variables['PLORDO2'] := QuotedStr(zero(fdmtblAssicuratoPremioAnnuoLordo.AsCurrency));
      frxPolizzaGen.Variables['DECO2']   := QuotedStr(fdmtblAssicuratoDataRilascio.AsString);
      frxPolizzaGen.Variables['SCAD2']   := QuotedStr(fdmtblAssicuratoDataScadenza.AsString);
      frxPolizzaGen.Variables['GAR2']    := QuotedStr(fdmtblAssicuratoSelectedGaranzie.AsString);
      frxPolizzaGen.Variables['MAX2']    := QuotedStr('=' + zero(fdmtblAssicuratoMassimale.AsCurrency));
      frxPolizzaGen.Variables['pnetto2'] := QuotedStr('=' + zero(fdmtblAssicuratoPremioNettoSLP.AsCurrency) + '=');
      }

       fdmtblAssicurato.Next;
       // terza polizza in garanzia -----------------------------------------------------------
       if not fdmtblAssicurato.eof then
       begin
         valorizzaDatiPolizza(3);
         fdmtblAssicurato.Next;
         // quarta polizza in garanzia -----------------------------------------------------------
         if not fdmtblAssicurato.eof then
         begin
           valorizzaDatiPolizza(4);
         end;
       end;
    end;
  finally
    fdmtblAssicurato.EnableControls;
    fdmtblAssicurato.First;
  end;

  {
  frxPolizzaGen.Variables['NETTOANNUO'] := QuotedStr(zero(Opremio.nettoTotaleBaseArrotondato));
  if fdmtblPolizzaSconto.AsCurrency <> 0 then
    frxPolizzaGen.Variables['sconto'] :=
      QuotedStr(zero(Opremio.nettoTotaleBaseArrotondato - Opremio.nettoTotaleRicalcolato))

  else
    frxPolizzaGen.Variables['sconto'] := QuotedStr('/////');

  frxPolizzaGen.Variables['perc_sconto'] := QuotedStr('-' + zero(fdmtblPolizzaPSconto.AsCurrency) + '%');
  }

  // parte nuova ============================================
  frxPolizzaGen.Variables['NETTOANNUO'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));
  if fdmtblPolizzaSconto.AsCurrency<>0 then
  begin
    frxPolizzaGen.Variables['sconto'] := QuotedStr(zero(fdmtblPolizzaSconto.AsCurrency));  //QuotedStr(zero(Opremio.scontoDurataBase));
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
    frxPolizzaGen.Variables['perc_sconto'] := QuotedStr(zero(fdmtblPolizzaPSconto.AsCurrency) + '%');
  end
  else
  begin
    frxPolizzaGen.Variables['sconto']     := QuotedStr('=======');
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr('=======');
    frxPolizzaGen.Variables['perc_sconto'] := QuotedStr('==');
  end;
  // fine parte nuova ==========================================================

  {
  // parte nuova ============================================
  frxPolizzaGen.Variables['nettoAnnuoTot'] := QuotedStr(zero(fdmtblPolizzaNetto.AsCurrency));

  // if Opremio.scontoDurataBase <> 0 then
  if fdmtblPolizzaSconto.AsCurrency<>0 then
  begin
    frxPolizzaGen.Variables['sconto'] := QuotedStr(zero(fdmtblPolizzaSconto.AsCurrency));  //QuotedStr(zero(Opremio.scontoDurataBase));
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr(zero(fdmtblPolizzaNettoAnnuoTot.AsCurrency));
    frxPolizzaGen.Variables['perc_sconto'] := QuotedStr(zero(fdmtblPolizzaPSconto.AsCurrency) + '%');
  end
  else
  begin
    frxPolizzaGen.Variables['sconto']     := QuotedStr('=======');
    frxPolizzaGen.Variables['p_scontato'] := QuotedStr('=======');
    frxPolizzaGen.Variables['perc_sconto'] := QuotedStr('==');
  end;
  // fine parte nuova ==========================================================
  }
  // parte finale con i premi
  frxPolizzaGen.Variables['rate_succ']   := QuotedStr(dai_frazionamentoE(fdmtblPolizzaFrazionamento.AsString));
  frxPolizzaGen.Variables['netto1']      := QuotedStr(zero(fdmtblPolizzanetto1.AsCurrency));
  frxPolizzaGen.Variables['acc1']        := QuotedStr(zero(fdmtblPolizzaAccessori1.AsCurrency));
  frxPolizzaGen.Variables['int_fraz']    := QuotedStr(zero(fdmtblPolizzaInteressiFrazionamento1.AsCurrency));
  frxPolizzaGen.Variables['imponibile1'] := QuotedStr(zero(fdmtblPolizzaImponibile1.AsCurrency));
  frxPolizzaGen.Variables['tasse1']      := QuotedStr(zero(fdmtblPolizzaImposte1.AsCurrency));
  frxPolizzaGen.Variables['totale1']     := QuotedStr(zero(fdmtblPolizzaTotale1.AsCurrency));
  frxPolizzaGen.Variables['alla_firma']  := QuotedStr(fdmtblPolizzaScadenzaPrimaRata.AsString);
  frxPolizzaGen.Variables['netto2']      := QuotedStr(zero(fdmtblPolizzanetto2.AsCurrency));
  frxPolizzaGen.Variables['acc2']        := QuotedStr(zero(fdmtblPolizzaaccessori2.AsCurrency));
  frxPolizzaGen.Variables['rimborso']    := QuotedStr(zero(fdmtblPolizzaRimborsoSost.AsCurrency));
  frxPolizzaGen.Variables['imponibile2'] := QuotedStr(zero(fdmtblPolizzaimponibile2.AsCurrency));
  frxPolizzaGen.Variables['tasse2']      := QuotedStr(zero(fdmtblPolizzaimposte2.AsCurrency));
  frxPolizzaGen.Variables['totale2']     := QuotedStr(zero(fdmtblPolizzatotale2.AsCurrency));
  frxPolizzaGen.Variables['emissione']   := QuotedStr(fdmtblPolizzaDataEmissione.AsString);
  if UniMainModule.DMdatiAge.TacitoRinnovoEnabled and fdmtblPolizzaTacitoRinnovo.AsBoolean then
    frxPolizzaGen.Variables['STR'] := QuotedStr('S');
end;

procedure TDMDatiTempPolizzaPP.MemorizzaTempGaranzie(TipoPolizza: Integer);
begin
  inherited;
  try
    fdmtblAssicurato.DisableControls;
    fdmtblAssicurato.First;
    while not fdmtblAssicurato.eof do
    begin
      MemorizzaTempGaranzia(CodPolizza, siglaMassimale);
      fdmtblAssicurato.Next;
    end;
  finally
    fdmtblAssicurato.EnableControls;
  end;
end;

procedure TDMDatiTempPolizzaPP.MemorizzaTempAssicurati(CodPolizza: Integer);
begin
  // inherited;
  // fdmtblAssicurato.DisableControls;
  // fdmtblAssicurato.First;
  // CodPolizza := fdmtblPolizzaCodPolizza.AsInteger;
  //
  // while not fdmtblAssicurato.Eof do
  // begin
  // siglaMassimale := Copy(fdmtblAssicuratoSiglaMassimaleGaranzia.AsString, 1, 3);
  // MemorizzaTempAssicurato(CodPolizza, siglaMassimale, i);
  // end;
end;

// procedure TDMDatiTempPolizzaPP.MemorizzaTempAssicurato(CodPolizza: Integer; const Sigla: string);
// var
// MassimaleGaranzia: string;
// begin
// if fdmtblPolizza.fieldByName('PREMIONETTOSLPGARANZIA' + Ix.toString).AsCurrency <> 0.0 then
// begin
// fdmtblAssicurato.Append;
// fdmtblAssicuratoDenominazione.AsString := fdmtblPolizza.fieldByName('SELECTEDGARANZIA' + Ix.toString).AsString;
// // QInsAssicurato.ParamByName('TIPO').asString := 'C'; // Contratto
//
// fdmtblAssicuratoCodFiscIvas.AsString := fdmtblPolizza.fieldByName('NPOLIZZAGARANZIA' + Ix.toString).AsString;
// fdmtblAssicuratoCitta.AsString       := fdmtblPolizza.fieldByName('COMPEMITTGARANZIA' + Ix.toString).AsString;
// fdmtblAssicuratoIndirizzo.AsString   := fdmtblPolizza.fieldByName('AGENZIAGARANZIA' + Ix.toString).AsString;
// fdmtblAssicuratoPremio.AsCurrency := fdmtblPolizza.fieldByName('PREMIOANNUOLORDOGARANZIA' + Ix.toString).AsCurrency;
// fdmtblAssicuratoDataRilascio.AsDateTime := fdmtblPolizza.fieldByName('DECORRENZAGARANZIA' + Ix.toString).AsDateTime;
// fdmtblAssicuratoDataScadenza.AsDateTime := fdmtblPolizza.fieldByName('SCADENZAGARANZIA' + Ix.toString).AsDateTime;
// fdmtblAssicuratoModello.AsString        := fdmtblPolizza.fieldByName('SELECTEDGARANZIA' + Ix.toString).AsString;
// fdmtblAssicuratoTipo.AsString           := 'C';
//
// fdmtblAssicuratoNote.Clear;
// fdmtblAssicuratoEntrata.Clear;
// fdmtblAssicuratoUscita.Clear;
// fdmtblAssicuratoCausaleUscita.Clear;
// fdmtblAssicuratoSostituitoDa.Clear;
// fdmtblAssicuratoPatente.Clear;
// fdmtblAssicuratoCategoriaPatente.Clear;
// fdmtblAssicuratoDataRilascio.Clear;
// fdmtblAssicuratoRilasciataDa.Clear;
// fdmtblAssicuratoDataScadenza.Clear;
// fdmtblAssicuratoTipoVeicolo.Clear;
// fdmtblAssicuratoTarga.Clear;
// fdmtblAssicuratoMarca.Clear;
// fdmtblAssicuratoHpQl.Clear;
// fdmtblAssicuratoDataScadRevisione.Clear;
// fdmtblAssicuratoClasse.Clear;
// fdmtblAssicuratoTelaio.Clear;
// fdmtblAssicuratoCc.Clear;
// fdmtblAssicuratoDataImmatricolazione.Clear;
// fdmtblAssicuratoPremio.Clear;
// QInsAssicurato.ParamByName('RIF_COD_TIPO_ASSICURATO').AsInteger :=
// DataSetAssicurati.FieldByName('RIFCODTIPOASSICURATO').AsInteger;
// QInsAssicurato.ParamByName('TIPO').AsString := DataSetAssicurati.FieldByName('TIPO').AsString;
// QInsAssicurato.ParamByName('COD_POLIZZA').AsString :=  DataSetAssicurati.FieldByName('CITTA').AsString;

// MassimaleGaranzia := fdmtblPolizza.fieldByName('MASSIMALEGARANZIA' + Ix.toString).AsString;
// MassimaleGaranzia := StringReplace(MassimaleGaranzia, '.', '', [rfReplaceAll]);
//
// fdmtblAssicuratoMassimale.AsCurrency := StrToCurr(MassimaleGaranzia);
//
// fdmtblAssicuratoPremioSLP.AsCurrency := fdmtblPolizza.fieldByName('PREMIONETTOSLPGARANZIA' + Ix.toString)
// .AsCurrency;
// fdmtblAssicuratoIdGenSlpAge.AsInteger := StrToInt(UniMainModule.DMdatiAge.SLPdati_age.agenzia);
//
// fdmtblAssicurato.Post;
// end;
// end;

procedure TDMDatiTempPolizzaPP.MemorizzaTempGaranzia(CodPolizza: Integer; const Sigla: string;
  PremioCalcolato: currency = 0.0);
var
  MassimaleGaranzia: string;
begin
  fdmtblGaranzia.append;
  fdmtblGaranziaSiglaGaranzia.AsString  := Sigla;
  fdmtblGaranziaMassimale.AsCurrency    := fdmtblAssicuratoMassimaleGaranzia.AsCurrency;
  fdmtblGaranziaPremio.AsCurrency       := fdmtblAssicuratoPremioNettoSLP.AsCurrency;
  fdmtblGaranziaDataIn.AsDateTime       := Date;
  fdmtblGaranziaRifCodPolizza.AsInteger := CodPolizza;
  { TODO 1 -oAngelo -cBug : Occorre memorizzarre il cod_gar_base }
  // fdmtblGaranziaRifGarBase.AsInteger := UniMainModule.DMDatiBasePolizza.Qgar_base.fieldByName('COD_GAR_BASE').AsInteger;
  fdmtblGaranziaFranchigia.AsString    := 'N';
  fdmtblGaranziaPercentuale.AsCurrency := 0;
  fdmtblGaranzia.Post;

end;

initialization

RegisterClass(TDMDatiTempPolizzaPP);

end.
