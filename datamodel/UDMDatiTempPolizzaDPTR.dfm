inherited DMDatiTempPolizzaDPTR: TDMDatiTempPolizzaDPTR
  OldCreateOrder = True
  inherited fdmtblPolizza: TFDMemTable
    inherited fdmtblPolizzaForma: TStringField
      OnChange = fdmtblPolizzaFormaChange
    end
    inherited fdmtblPolizzaSiglaMassimale: TStringField
      OnChange = fdmtblPolizzaSiglaMassimaleChange
    end
    object fdmtblPolizzaRimborsoSpesePatPunti: TBooleanField [99]
      FieldName = 'RimborsoSpesePatPunti'
      OnChange = fdmtblPolizzaRimborsoSpesePatPuntiChange
    end
    object fdmtblPolizzaSiglaRimborsoSpesePatPunti: TStringField [100]
      FieldName = 'SiglaRimborsoSpesePatPunti'
      OnChange = fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange
    end
    object fdmtblPolizzaAppControversie: TBooleanField [101]
      FieldName = 'AppControversie'
      OnChange = fdmtblPolizzaAppControversieChange
    end
    object fdmtblPolizzaProblematicheRCA: TBooleanField [102]
      FieldName = 'ProblematicheRCA'
      OnChange = fdmtblPolizzaProblematicheRCAChange
    end
    object fdmtblPolizzaPerizieParte: TBooleanField [103]
      FieldName = 'PerizieParte'
      OnChange = fdmtblPolizzaPerizieParteChange
    end
    object fdmtblPolizzaMancatoIntRCA: TBooleanField [104]
      FieldName = 'MancatoIntRCA'
      OnChange = fdmtblPolizzaMancatoIntRCAChange
    end
    object fdmtblPolizzaProtezioneFamiglia: TBooleanField [105]
      FieldName = 'ProtezioneFamiglia'
      OnChange = fdmtblPolizzaProtezioneFamigliaChange
    end
    object fdmtblPolizzaNumeroAssicurati: TIntegerField [106]
      FieldName = 'NumeroAssicurati'
      OnChange = fdmtblPolizzaNumeroAssicuratiChange
    end
    object fdmtblPolizzaEstPatente: TBooleanField [107]
      FieldName = 'EstPatente'
      OnChange = fdmtblPolizzaEstPatenteChange
    end
    object fdmtblPolizzaIndicizzata: TBooleanField [108]
      FieldName = 'Indicizzata'
      OnChange = fdmtblPolizzaIndicizzataChange
    end
    object fdmtblPolizzaTipoAssicurato: TStringField [109]
      FieldName = 'TipoAssicurato'
      OnChange = fdmtblPolizzaTipoAssicuratoChange
      Size = 1
    end
    object fdmtblPolizzaNome1: TStringField [110]
      FieldName = 'Nome1'
      Required = True
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaNome2: TStringField [111]
      FieldName = 'Nome2'
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaNome3: TStringField [112]
      FieldName = 'Nome3'
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaNome4: TStringField [113]
      FieldName = 'Nome4'
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaPatente1: TStringField [114]
      FieldName = 'Patente1'
      OnChange = fdmtblPolizzaPatente1Change
      Size = 13
    end
    object fdmtblPolizzaPatente2: TStringField [115]
      FieldName = 'Patente2'
      Size = 13
    end
    object fdmtblPolizzaPatente3: TStringField [116]
      FieldName = 'Patente3'
      Size = 13
    end
    object fdmtblPolizzaPatente4: TStringField [117]
      FieldName = 'Patente4'
      Size = 13
    end
    object fdmtblPolizzaCatPatente1: TStringField [118]
      FieldName = 'CatPatente1'
      Size = 2
    end
    object fdmtblPolizzaCatPatente2: TStringField [119]
      FieldName = 'CatPatente2'
      Size = 2
    end
    object fdmtblPolizzaCatPatente3: TStringField [120]
      FieldName = 'CatPatente3'
      Size = 2
    end
    object fdmtblPolizzaCatPatente4: TStringField [121]
      FieldName = 'CatPatente4'
      Size = 2
    end
    inherited fdmtblPolizzaNPercAccessori: TIntegerField
      OnChange = fdmtblPolizzaNPercAccessoriChange
    end
  end
  inherited fdmtblAssicurato: TFDMemTable
    inherited fdmtblAssicuratoCategoriaPatente: TStringField
      OnChange = fdmtblAssicuratoCategoriaPatenteChange
      Size = 2
    end
  end
  inherited frxPolizzaGen: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxAllegatiPOL: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxReport1: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
end
