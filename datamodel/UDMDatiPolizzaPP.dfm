inherited DMDatiPolizzaPP: TDMDatiPolizzaPP
  inherited QGetLastID: TDBISAMQuery
    Top = 96
  end
  inherited tblTemp: TDBISAMTable
    Left = 144
    Top = 16
  end
  inherited QInsPolizza: TDBISAMQuery
    SQL.Strings = (
      'INSERT INTO SLP_TPOLIZZE'
      '('
      '  RIF_TIPO_POL,'
      ' SIGLA_POL,'
      '  CONTRAENTE,'
      '  DECORRENZA,'
      '  SCADENZA,'
      '  FRAZIONAM,'
      '  N_POLIZZA,'
      '  LORDO,'
      '  NETTO,'
      '  PTASSE,'
      '  ACCESSORI,'
      '  VALUTA,'
      '  TASSE,'
      '  NOTE,'
      '  RIF_COMPAGNIA,'
      '  COD_CLI,'
      '  FATTA_DA,'
      '  RIF_COD_RAMO,'
      '  STATO,'
      '  PROVVIGIONI,'
      '  PERC_PROVVIGIONI,'
      '  CLASSE,'
      '  RAMO,'
      '  AGENZIA,'
      '  SUB_AGE,'
      '  RATINO_1_RATA,'
      '  SCONTO_DURATA,'
      '  UTENTE,'
      '  DATA_INSERIMENTO,'
      '  NUM_ASSICURATI,'
      '  NETTO1,'
      '  ACCESSORI1,'
      '  INT_FRAZ,'
      '  IMPONIBILE1,'
      '  IMPOSTE1,'
      '  TOTALE1,'
      '  NETTO2,'
      '  ACCESSORI2,'
      '  RIMBORSO_SOST,'
      '  IMPONIBILE2,'
      '  IMPOSTE2,'
      '  TOTALE2,'
      '  COMBINAZIONE,'
      '  FORMA,'
      '  ESTENSIONE_OM,'
      '  N_POLIZZA_SOSTITUITA,'
      '  SCAD_POL_SOSTITUITA,'
      '  SCAD_PRIMA_RATA,'
      '  INDICIZZATA,'
      '  STATUS,'
      '  DATA_PERFEZ,'
      '  DATA_CANC,'
      '  VOLUME_AFFARI,'
      '  NUMERO_ASSICURATI,'
      '  TIPO_PROVVIGIONI,'
      '  PERC_ACCESSORI,'
      '  RAGIONE_SOC,'
      '  DATI1,'
      '  DATI2,'
      '  DATI3,'
      '  DATI4,'
      '  ESTENSIONI,'
      '  SIGLA_POL,'
      '  TIPO_PROV,'
      '  TIPOLOGIA,'
      '  DATA_EMISSIONE,'
      '  DATI_VARI,'
      '  ORDINE,'
      '  SPECIAL,'
      '  CODICE_IMPORTAZIONE,'
      '  INTERMEDIATA_DA,'
      '  INTERMEDIATA_DA_SIGLA,'
      '  CONVENZIONE,'
      '  RIF_REFERENTE,'
      '  VARFRP,'
      '  CON_APPENDICE,'
      '  APP_NUMERO,'
      '  APP_DAL,'
      '  APP_AL,'
      '  APP_OGGETTO,'
      '  P_SCONTO,'
      '  INPDF,'
      '  SUB_PROMOTER,'
      '  DATI5,'
      '  N_S_P,'
      '  TIPO_EMISSIONE,'
      '  DT_LAST_MOD,'
      '  DT_PREVENTIVO,'
      '  TELEFONO,'
      '  EMAIL,'
      '  perAllegati,'
      '  ID_GEN_SLP_AGE,'
      ' descrizione'
      ')'
      'VALUES ('
      '  :RIF_TIPO_POL,'
      '  :SIGLA_POL,'
      '  :CONTRAENTE,'
      '  :DECORRENZA,'
      '  :SCADENZA,'
      '  :FRAZIONAM,'
      '  :N_POLIZZA,'
      '  :LORDO,'
      '  :NETTO,'
      '  :PTASSE,'
      '  :ACCESSORI,'
      '  :VALUTA,'
      '  :TASSE,'
      '  :NOTE,'
      '  :RIF_COMPAGNIA,'
      '  :COD_CLI,'
      '  :FATTA_DA,'
      '  :RIF_COD_RAMO,'
      '  :STATO,'
      '  :PROVVIGIONI,'
      '  :PERC_PROVVIGIONI,'
      '  :CLASSE,'
      '  :RAMO,'
      '  :AGENZIA,'
      '  :SUB_AGE,'
      '  :RATINO_1_RATA,'
      '  :SCONTO_DURATA,'
      '  :UTENTE,'
      '  :DATA_INSERIMENTO,'
      '  :NUM_ASSICURATI,'
      '  :NETTO1,'
      '  :ACCESSORI1,'
      '  :INT_FRAZ,'
      '  :IMPONIBILE1,'
      '  :IMPOSTE1,'
      '  :TOTALE1,'
      '  :NETTO2,'
      '  :ACCESSORI2,'
      '  :RIMBORSO_SOST,'
      '  :IMPONIBILE2,'
      '  :IMPOSTE2,'
      '  :TOTALE2,'
      '  :COMBINAZIONE,'
      '  :FORMA,'
      '  :ESTENSIONE_OM,'
      '  :N_POLIZZA_SOSTITUITA,'
      '  :SCAD_POL_SOSTITUITA,'
      '  :SCAD_PRIMA_RATA,'
      '  :INDICIZZATA,'
      '  :STATUS,'
      '  :DATA_PERFEZ,'
      '  :DATA_CANC,'
      '  :VOLUME_AFFARI,'
      '  :NUMERO_ASSICURATI,'
      '  :TIPO_PROVVIGIONI,'
      '  :PERC_ACCESSORI,'
      '  :RAGIONE_SOC,'
      '  :DATI1,'
      '  :DATI2,'
      '  :DATI3,'
      '  :DATI4,'
      '  :ESTENSIONI,'
      '  :SIGLA_POL,'
      '  :TIPO_PROV,'
      '  :TIPOLOGIA,'
      '  :DATA_EMISSIONE,'
      '  :DATI_VARI,'
      '  :ORDINE,'
      '  :SPECIAL,'
      '  :CODICE_IMPORTAZIONE,'
      '  :INTERMEDIATA_DA,'
      '  :INTERMEDIATA_DA_SIGLA,'
      '  :CONVENZIONE,'
      '  :RIF_REFERENTE,'
      '  :VARFRP,'
      '  :CON_APPENDICE,'
      '  :APP_NUMERO,'
      '  :APP_DAL,'
      '  :APP_AL,'
      '  :APP_OGGETTO,'
      '  :P_SCONTO,'
      '  :INPDF,'
      '  :SUB_PROMOTER,'
      '  :DATI5,'
      '  :N_S_P,'
      '  :TIPO_EMISSIONE,'
      '  :DT_LAST_MOD,'
      '  :DT_PREVENTIVO,'
      '  :TELEFONO,'
      '  :EMAIL,'
      '  :perAllegati,'
      '  :ID_GEN_SLP_AGE,'
      '  :descrizione'
      ')'
      '')
  end
  inherited QInsAssicurato: TDBISAMQuery
    SQL.Strings = (
      'INSERT INTO SLP_TASSICURATI'
      '('
      ' -- COD_ASSICURATI,'
      '  DENOMINAZIONE,'
      '  INDIRIZZO,'
      '  CITTA,'
      '  CAP,'
      '  PROV,'
      '  COD_FISC_IVA,'
      '  NOTE,'
      '  ENTRATA,'
      '  USCITA,'
      '  CAUSALE_USCITA,'
      '  SOSTITUITO_DA,'
      '  PATENTE,'
      '  CATEGORIA_PAT,'
      '  DATA_RILASCIO,'
      '  RILASCIATA_DA,'
      '  DATA_SCADENZA,'
      '  TIPO_VEICOLO,'
      '  TARGA,'
      '  MARCA,'
      '  HP_QL,'
      '  RIF_COD_TIPO_ASSICURATO,'
      '  TIPO,'
      '  COD_POLIZZA,'
      '  DATA_SCAD_REVISIONE,'
      '  MODELLO,'
      '  CLASSE,'
      '  TELAIO,'
      '  CC,'
      '  DATA_IMMATR,'
      '  PREMIO,'
      '  MASSIMALE,'
      '  PREMIO_SLP,'
      '  OGG_AGGIUNTIVO,'
      ' GARANZIE,'
      '  ID_GEN_SLP_AGE'
      ''
      ')'
      'VALUES'
      '('
      ' -- :COD_ASSICURATI,'
      '  :DENOMINAZIONE,'
      '  :INDIRIZZO,'
      '  :CITTA,'
      '  :CAP,'
      '  :PROV,'
      '  :COD_FISC_IVA,'
      '  :NOTE,'
      '  :ENTRATA,'
      '  :USCITA,'
      '  :CAUSALE_USCITA,'
      '  :SOSTITUITO_DA,'
      '  :PATENTE,'
      '  :CATEGORIA_PAT,'
      '  :DATA_RILASCIO,'
      '  :RILASCIATA_DA,'
      '  :DATA_SCADENZA,'
      '  :TIPO_VEICOLO,'
      '  :TARGA,'
      '  :MARCA,'
      '  :HP_QL,'
      '  :RIF_COD_TIPO_ASSICURATO,'
      '  :TIPO,'
      ' LASTAUTOINC('#39'SLP_TPOLIZZE'#39'),'
      '  :DATA_SCAD_REVISIONE,'
      '  :MODELLO,'
      '  :CLASSE,'
      '  :TELAIO,'
      '  :CC,'
      '  :DATA_IMMATR,'
      '  :PREMIO,'
      '  :MASSIMALE,'
      '  :PREMIO_SLP,'
      '  :OGG_AGGIUNTIVO,'
      '  :GARANZIE,'
      '  :ID_GEN_SLP_AGE'
      ')')
    Params = <
      item
        DataType = ftString
        Name = 'DENOMINAZIONE'
      end
      item
        DataType = ftString
        Name = 'INDIRIZZO'
      end
      item
        DataType = ftString
        Name = 'CITTA'
      end
      item
        DataType = ftFixedChar
        Name = 'CAP'
      end
      item
        DataType = ftFixedChar
        Name = 'PROV'
      end
      item
        DataType = ftString
        Name = 'COD_FISC_IVA'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftDate
        Name = 'ENTRATA'
      end
      item
        DataType = ftDate
        Name = 'USCITA'
      end
      item
        DataType = ftInteger
        Name = 'CAUSALE_USCITA'
      end
      item
        DataType = ftInteger
        Name = 'SOSTITUITO_DA'
      end
      item
        DataType = ftString
        Name = 'PATENTE'
      end
      item
        DataType = ftString
        Name = 'CATEGORIA_PAT'
      end
      item
        DataType = ftDate
        Name = 'DATA_RILASCIO'
      end
      item
        DataType = ftString
        Name = 'RILASCIATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCADENZA'
      end
      item
        DataType = ftString
        Name = 'TIPO_VEICOLO'
      end
      item
        DataType = ftString
        Name = 'TARGA'
      end
      item
        DataType = ftString
        Name = 'MARCA'
      end
      item
        DataType = ftString
        Name = 'HP_QL'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_TIPO_ASSICURATO'
      end
      item
        DataType = ftFixedChar
        Name = 'TIPO'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCAD_REVISIONE'
      end
      item
        DataType = ftString
        Name = 'MODELLO'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftString
        Name = 'TELAIO'
      end
      item
        DataType = ftString
        Name = 'CC'
      end
      item
        DataType = ftDate
        Name = 'DATA_IMMATR'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO'
      end
      item
        DataType = ftCurrency
        Name = 'MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO_SLP'
      end
      item
        DataType = ftString
        Name = 'OGG_AGGIUNTIVO'
      end
      item
        DataType = ftBlob
        Name = 'GARANZIE'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
    ParamData = <
      item
        DataType = ftString
        Name = 'DENOMINAZIONE'
      end
      item
        DataType = ftString
        Name = 'INDIRIZZO'
      end
      item
        DataType = ftString
        Name = 'CITTA'
      end
      item
        DataType = ftFixedChar
        Name = 'CAP'
      end
      item
        DataType = ftFixedChar
        Name = 'PROV'
      end
      item
        DataType = ftString
        Name = 'COD_FISC_IVA'
      end
      item
        DataType = ftMemo
        Name = 'NOTE'
      end
      item
        DataType = ftDate
        Name = 'ENTRATA'
      end
      item
        DataType = ftDate
        Name = 'USCITA'
      end
      item
        DataType = ftInteger
        Name = 'CAUSALE_USCITA'
      end
      item
        DataType = ftInteger
        Name = 'SOSTITUITO_DA'
      end
      item
        DataType = ftString
        Name = 'PATENTE'
      end
      item
        DataType = ftString
        Name = 'CATEGORIA_PAT'
      end
      item
        DataType = ftDate
        Name = 'DATA_RILASCIO'
      end
      item
        DataType = ftString
        Name = 'RILASCIATA_DA'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCADENZA'
      end
      item
        DataType = ftString
        Name = 'TIPO_VEICOLO'
      end
      item
        DataType = ftString
        Name = 'TARGA'
      end
      item
        DataType = ftString
        Name = 'MARCA'
      end
      item
        DataType = ftString
        Name = 'HP_QL'
      end
      item
        DataType = ftInteger
        Name = 'RIF_COD_TIPO_ASSICURATO'
      end
      item
        DataType = ftFixedChar
        Name = 'TIPO'
      end
      item
        DataType = ftDate
        Name = 'DATA_SCAD_REVISIONE'
      end
      item
        DataType = ftString
        Name = 'MODELLO'
      end
      item
        DataType = ftInteger
        Name = 'CLASSE'
      end
      item
        DataType = ftString
        Name = 'TELAIO'
      end
      item
        DataType = ftString
        Name = 'CC'
      end
      item
        DataType = ftDate
        Name = 'DATA_IMMATR'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO'
      end
      item
        DataType = ftCurrency
        Name = 'MASSIMALE'
      end
      item
        DataType = ftCurrency
        Name = 'PREMIO_SLP'
      end
      item
        DataType = ftString
        Name = 'OGG_AGGIUNTIVO'
      end
      item
        DataType = ftBlob
        Name = 'GARANZIE'
      end
      item
        DataType = ftInteger
        Name = 'ID_GEN_SLP_AGE'
      end>
  end
  inherited QDelPolizza: TDBISAMQuery
    Top = 96
  end
end
