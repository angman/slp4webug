{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N-,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$WARN SYMBOL_DEPRECATED ON}
{$WARN SYMBOL_LIBRARY ON}
{$WARN SYMBOL_PLATFORM ON}
{$WARN SYMBOL_EXPERIMENTAL ON}
{$WARN UNIT_LIBRARY ON}
{$WARN UNIT_PLATFORM ON}
{$WARN UNIT_DEPRECATED ON}
{$WARN UNIT_EXPERIMENTAL ON}
{$WARN HRESULT_COMPAT ON}
{$WARN HIDING_MEMBER ON}
{$WARN HIDDEN_VIRTUAL ON}
{$WARN GARBAGE ON}
{$WARN BOUNDS_ERROR ON}
{$WARN ZERO_NIL_COMPAT ON}
{$WARN STRING_CONST_TRUNCED ON}
{$WARN FOR_LOOP_VAR_VARPAR ON}
{$WARN TYPED_CONST_VARPAR ON}
{$WARN ASG_TO_TYPED_CONST ON}
{$WARN CASE_LABEL_RANGE ON}
{$WARN FOR_VARIABLE ON}
{$WARN CONSTRUCTING_ABSTRACT ON}
{$WARN COMPARISON_FALSE ON}
{$WARN COMPARISON_TRUE ON}
{$WARN COMPARING_SIGNED_UNSIGNED ON}
{$WARN COMBINING_SIGNED_UNSIGNED ON}
{$WARN UNSUPPORTED_CONSTRUCT ON}
{$WARN FILE_OPEN ON}
{$WARN FILE_OPEN_UNITSRC ON}
{$WARN BAD_GLOBAL_SYMBOL ON}
{$WARN DUPLICATE_CTOR_DTOR ON}
{$WARN INVALID_DIRECTIVE ON}
{$WARN PACKAGE_NO_LINK ON}
{$WARN PACKAGED_THREADVAR ON}
{$WARN IMPLICIT_IMPORT ON}
{$WARN HPPEMIT_IGNORED ON}
{$WARN NO_RETVAL ON}
{$WARN USE_BEFORE_DEF ON}
{$WARN FOR_LOOP_VAR_UNDEF ON}
{$WARN UNIT_NAME_MISMATCH ON}
{$WARN NO_CFG_FILE_FOUND ON}
{$WARN IMPLICIT_VARIANTS ON}
{$WARN UNICODE_TO_LOCALE ON}
{$WARN LOCALE_TO_UNICODE ON}
{$WARN IMAGEBASE_MULTIPLE ON}
{$WARN SUSPICIOUS_TYPECAST ON}
{$WARN PRIVATE_PROPACCESSOR ON}
{$WARN UNSAFE_TYPE OFF}
{$WARN UNSAFE_CODE OFF}
{$WARN UNSAFE_CAST OFF}
{$WARN OPTION_TRUNCATED ON}
{$WARN WIDECHAR_REDUCED ON}
{$WARN DUPLICATES_IGNORED ON}
{$WARN UNIT_INIT_SEQ ON}
{$WARN LOCAL_PINVOKE ON}
{$WARN MESSAGE_DIRECTIVE ON}
{$WARN TYPEINFO_IMPLICITLY_ADDED ON}
{$WARN RLINK_WARNING ON}
{$WARN IMPLICIT_STRING_CAST ON}
{$WARN IMPLICIT_STRING_CAST_LOSS ON}
{$WARN EXPLICIT_STRING_CAST OFF}
{$WARN EXPLICIT_STRING_CAST_LOSS OFF}
{$WARN CVT_WCHAR_TO_ACHAR ON}
{$WARN CVT_NARROWING_STRING_LOST ON}
{$WARN CVT_ACHAR_TO_WCHAR ON}
{$WARN CVT_WIDENING_STRING_LOST ON}
{$WARN NON_PORTABLE_TYPECAST ON}
{$WARN XML_WHITESPACE_NOT_ALLOWED ON}
{$WARN XML_UNKNOWN_ENTITY ON}
{$WARN XML_INVALID_NAME_START ON}
{$WARN XML_INVALID_NAME ON}
{$WARN XML_EXPECTED_CHARACTER ON}
{$WARN XML_CREF_NO_RESOLVE ON}
{$WARN XML_NO_PARM ON}
{$WARN XML_NO_MATCHING_PARM ON}
{$WARN IMMUTABLE_STRINGS OFF}
unit UDMdatiAge;

interface

uses
  System.SysUtils, System.Classes, Data.DB, dbisamtb, IWApplication, UDMMaster, System.Rtti,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, frxClass;

type

  TDati_gen_rec = record
    dir_exe: string;
    dir_dati: string;
    dir_doc: string;
    dir_temp: string;
    dir_loc: string;
    dir_bak: string;
    dir_img: string;
    dir_pdf: string;
    srvIP: string;
    srvPort1, srvPort2: integer;
    remotePassword, remoteUser, remoteDB, serverName, passwordUtenteDB, utenteDB: string;
    tipoI: string[3];
    // stato_finestra: TWindowState ;
    localUserName: string;
    sativ: string;
    // contiene i vari flag che disattivano opzioni del programma
    conLogSuDBremoto: boolean;
    data_controllo: TdateTime;
    percorso_base: string;
  end;

  TSLPdati_age_rec = record
    agenzia: string;
    sub_age: string;
    cod_int: string;
    cod_intZ: string;
    citta: string;
    nome: string;
    Denominazione: string;
    utente: string;
    // utenteCodice: Integer;
    percorso_age: string;
    // TacitoRinnovoEnabled: Boolean;
    dati_gen: TDati_gen_rec;
  end;

  TDMdatiAge = class(TDMMaster)
    DBagenti4web: TDBISAMDatabase;
    Qutenti: TDBISAMQuery;
    Qget_codice: TDBISAMQuery;
    Qget_codicenew: TDBISAMQuery;
    Qins_codicenew: TDBISAMQuery;
    Qmod_codicenew: TDBISAMQuery;
    SessionAgenzia: TDBISAMSession;
    QClientiProfessioni: TDBISAMQuery;
    dsClientiProfessioni: TDataSource;
    QRami: TDBISAMQuery;
    DSPolizze: TDataSource;
    DSCompagnie: TDataSource;
    QCompagnie: TDBISAMQuery;
    DSRami: TDataSource;
    QPolizze: TDBISAMQuery;
    QTipoPolizze: TDBISAMQuery;
    DSTipoPolizze: TDataSource;
    DSConvenzioni: TDataSource;
    QConvenzioni: TDBISAMQuery;
    QCausali: TDBISAMQuery;
    DSCausali: TDataSource;
    DSReferenti: TDataSource;
    QReferenti: TDBISAMQuery;
    QAppoggio: TDBISAMQuery;
    DSAppoggio: TDataSource;
    QTitoliNumPolizza: TDBISAMQuery;

    QLog_Ope: TDBISAMQuery;
    QSelPromoter: TDBISAMQuery;
    DSSelPromoter: TDataSource;
    QPromoter_From_Polizza: TDBISAMQuery;
    QProdut: TDBISAMQuery;
    DSProdut: TDataSource;
    QnumeroFC: TDBISAMQuery;
    QDati_agenzia: TDBISAMQuery;
    QcompagniSLP: TDBISAMQuery;
    QgetCompagnia: TDBISAMQuery;
    QUtenteAutorizzazioni: TDBISAMQuery;
    QAgenziaEdit: TDBISAMQuery;
    QTipoPolStampaPol: TDBISAMQuery;
    Qdurata_max: TDBISAMQuery;
    QAliquote: TDBISAMQuery;
    QCodici: TDBISAMQuery;
    Qtipo_veicolo: TDBISAMQuery;
    QTipo_pol: TDBISAMQuery;
    fdmtblFrazionamento: TFDMemTable;
    dsFrazionamento: TDataSource;
    fdmtblFrazionamentoCodFrazionamento: TStringField;
    Qpol_da_perfezFrazionamentoDesFrazionamento: TStringField;
    fdmtblTipoEmiss: TFDMemTable;
    fdmtblPolizzaTipoEmissCodEmmiss: TStringField;
    fdmtblPolizzaTipoEmissDesTipoEmiss: TStringField;
    dsTipoEmiss: TDataSource;
    fdmtblTipoEmissCodCompagnia: TIntegerField;
    QtipoPolizzeAll: TDBISAMQuery;
    DStipoPolizzeAll: TDataSource;
    QCapItalia: TDBISAMQuery;
    QinsStampa: TDBISAMQuery;
    QStaccaNumeroPolizza: TDBISAMQuery;
    QUpdNumeroPolizza: TDBISAMQuery;
    QModSLP: TDBISAMQuery;
    QgetLastCodDoc: TDBISAMQuery;
    Qmod_slp: TDBISAMQuery;
    Qmoduli: TDBISAMQuery;
    QgetDescrizioneVeicolo: TDBISAMQuery;
    QproduttAgente: TDBISAMQuery;
    QcercaProdutt: TDBISAMQuery;
    QsubProdut: TDBISAMQuery;
    DSsubProdut: TDataSource;
    QintermediataDa: TDBISAMQuery;
    DSintermediataDa: TDataSource;
    Qfind_durata: TDBISAMQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DBagenti4webAfterConnect(Sender: TObject);
    procedure SessionAgenziaPassword(Sender: TObject; var Continue: boolean);
    procedure QAgenziaEditBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FChiudi_sessione: boolean;
    FSLPdati_age: TSLPdati_age_rec;
    FTacitoRinnovoEnabled: boolean;

    procedure configuraServerDB;

    function getDurataMax: integer;
    function leggiAliquote(ACodAgenzia: integer): Currency;
    function GetDati_gen: TDati_gen_rec;
    procedure SetDati_gen(const Value: TDati_gen_rec);
    procedure setEncriptionPassword(const Value: string);
    procedure SetTacitoRinnovoEnabled(const Value: boolean);
    function getTacitoRinnovoEnabled: boolean;

  public
    { Public declarations }

    procedure decodeCF(cf: string; var nato_il: TdateTime; var nato_a: string);
    function isDirezione: boolean;
    function isAssitorino: boolean;
    function isExtensionEnabled(const ExtensionDescr: string): boolean;
    function is_utente_promoter: boolean;
    procedure leggiAccessoriProvvigioni(ACodAgenzia: integer; var PercAccessoriAGe: Currency;
      var ProvvigioneAge: Currency; CodCompagnia: integer = 1; tipo: char = 'R'; durata: integer = 1);

    // procedure connettiDB;
    function get_codiceX(nome_campo: string; var valore: integer): boolean; overload;
    function get_codiceDef(nome_campo: string; DefValue: integer): integer;
    procedure get_codice(cosa: string; var valore: TdateTime); overload;
    procedure get_codice(cosa: string; var valore: integer); overload;
    procedure get_codice(cosa: string; var valore: string); overload;
    procedure set_codice(cosa: string; valore: TdateTime); overload;
    procedure set_codice(cosa: string; valore: integer); overload;
    procedure set_codice(cosa: string; valore: string); overload;

    function getCodice(IDCampo: string; AFieldType: TFieldType): TValue;
    procedure setCodice(IDCampo: string; valore: TValue);
    procedure InsCodice(IDCampo: string; valore: TValue);

    function get_perc_pro_insolute: integer;
    procedure caricaTipoVeicolo(lista: Tstrings);
    procedure ClearRuiData;
    function GetSativ: string;
    procedure caricaPromoter(items: Tstringlist);
    procedure carica_compagnie(items: Tstringlist);
    function get_n_fc: string;
    function get_intervallo_chiusura_contabile: integer;
    function get_dati_pagamentoFC: string;
    function getNomePromoter(ACodPromoter: integer): string;
    function getDesFrazionamentoPolizza(AFrazionamento: string): string;
    function FindPolizza(const N_polizza: string): boolean; overload;
    function FindPolizza(const cod_polizza: integer; var num_polizza: string): boolean; overload;
    procedure PosizionaQuery(AValue: TValue); override;

    function get_provv_collab: Currency;
    function get_provv_SubCollab: Currency;

    procedure LoadModelloReport(frxPolizzaGen: TfrxReport);
    function MemorizzaReportPolizza(AStream: TStream; NPolizza, tipor: string; dataEmissione: TdateTime;
      bVisible: boolean): integer;
    function staccaNumeroPolizza(sigla: string; bUpdateNumero: boolean = true): integer;

    function VerificaAutorizzazione(AArchivio: string = ''; AOperazione: string = ''): boolean;
    function VerAutGenAgenzia(AOperazione: string = ''): boolean;

    function get_mod_slp_gen(cod_stampa: integer; var blobs: TStream): boolean;
    function get_mod_slp_genX(cod_stampa: integer; var blobs: TMemoryStream): boolean;

    function getDescrizioneVeicolo(sigla: string): string;

    function esiste_coll(cod_coll: Integer): Boolean;

    function dai_descrizione_ph(sigla: string): string;

    function find_durata(durata: Integer; titpo: string): Boolean;

    property Chiudi_sessione: boolean read FChiudi_sessione;
    property SLPdati_age: TSLPdati_age_rec read FSLPdati_age write FSLPdati_age;
    property dati_gen: TDati_gen_rec read GetDati_gen write SetDati_gen;
    property DurataMax: integer read getDurataMax;
    property EncryptionPassword: string write setEncriptionPassword;
    property TacitoRinnovoEnabled: boolean read getTacitoRinnovoEnabled;

  end;

implementation

{ %CLASSGROUP 'Vcl.Controls.TControl' }

{$R *.dfm}

uses libreria, IOUtils, System.Variants, ServerModule, MainModule, UCodiciErroriPolizza, System.StrUtils,
  System.TypInfo, UPolizzaExceptions;

procedure TDMdatiAge.DataModuleCreate(Sender: TObject);
begin
  SessionAgenzia.Close;
  DBagenti4web.Close;

  // configuraServerDB;

  // dati_gen.percorso_base := 'C:\slp\dati\St';
  FSLPdati_age.dati_gen.percorso_base := TPath.Combine(UniServerModule.PosizioneDati, 'dati');
  DBagenti4web.Directory              := TPath.Combine(UniServerModule.PosizioneDati, 'dati');
  SessionAgenzia.PrivateDir           := TPath.Combine(UniServerModule.PosizioneDati, 'temp');
  // SessionAgenzia.AutoSessionName      := True;

  FChiudi_sessione := false;
  fdmtblFrazionamento.CreateDataSet;
  fdmtblFrazionamento.AppendRecord(['A', 'Annuale']);
  fdmtblFrazionamento.AppendRecord(['B', 'Bimestrale']);
  fdmtblFrazionamento.AppendRecord(['M', 'Mensile']);
  fdmtblFrazionamento.AppendRecord(['T', 'Trimestrale']);
  fdmtblFrazionamento.AppendRecord(['Q', 'Quadrimestrale']);
  fdmtblFrazionamento.AppendRecord(['S', 'Semestrale']);
  fdmtblFrazionamento.AppendRecord(['P', 'temporaneo']);
  fdmtblFrazionamento.AppendRecord(['U', 'Unico']);
  fdmtblFrazionamento.Open;

  fdmtblTipoEmiss.CreateDataSet;
  fdmtblTipoEmiss.Open;
  fdmtblTipoEmiss.AppendRecord(['A', 'AGE']);
  fdmtblTipoEmiss.AppendRecord(['D', 'SLP', 1]);

end;

procedure TDMdatiAge.configuraServerDB;
begin
  // SessionAgenzia.active := False;
{$IFNDEF  SVILUPPO}
  // autenticazione su server remoto (2)
  // MB INI autenticazione su server remoto (2)
  SessionAgenzia.PrivateDir   := 'C:\slp\Slp4Web\Temp\';
  DBagenti4web.Connected      := false;
  DBagenti4web.RemoteDatabase := SLPdati_age.dati_gen.remoteDB;

  SessionAgenzia.RemoteEncryption := true;
  // SessionAgenzia.RemoteEncryptionPassword := 'astraGalo37k21Hh51zZ';
  SessionAgenzia.RemoteEncryptionPassword := UniServerModule.EncriptionPassword;

  if SLPdati_age.dati_gen.srvPort1 = 12007 then
  begin
    Engine.serverName := 'DBSRVR1';
  end;

  if SLPdati_age.dati_gen.srvPort1 > 0 then
  begin
    Engine.ServerMainPort     := SLPdati_age.dati_gen.srvPort1;
    SessionAgenzia.RemotePort := SLPdati_age.dati_gen.srvPort1;
  end;
  if SLPdati_age.dati_gen.srvPort2 > 0 then
  begin
    Engine.ServerAdminPort := SLPdati_age.dati_gen.srvPort2;
  end;

  SessionAgenzia.SessionType    := stRemote;
  SessionAgenzia.RemoteAddress  := '127.0.0.1'; // dati_gen.srvIP;
  SessionAgenzia.remoteUser     := SLPdati_age.dati_gen.utenteDB;
  SessionAgenzia.remotePassword := SLPdati_age.dati_gen.passwordUtenteDB;
  // MB END
{$ELSE}
  SessionAgenzia.PrivateDir := TPath.Combine(UniServerModule.PosizioneDati, 'temp');
  DBagenti4web.Directory    := TPath.Combine(UniServerModule.PosizioneDati, 'dati');
{$ENDIF}
  SessionAgenzia.active := true;
  // UniMainModule.SessioneAccesso.AddPassword('27H312ACFF77908DDAC4005589.-+');
  SessionAgenzia.AddPassword(UniServerModule.remotePassword);

  QDati_agenzia.Open;
  if QDati_agenzia.FieldByName('STAMPA_INTESTAZIONE_IN_POLIZZA').asBoolean then
    FSLPdati_age.Denominazione := QDati_agenzia.FieldByName('denominazione').asString;

end;


function TDMdatiAge.dai_descrizione_ph(sigla: string): string;
   function cerca(cosa: string): string;
   begin
      // cerca in mod_slp la descrizione e se non la trovi ricostruiscila
      // salvo che per chi comincia per P per tutti gli altri il campo TIPO = 'S'
      UniMainModule.DMdatiAge.Qmoduli.Close;
      if Copy(sigla,1,2)='P-' then
         UniMainModule.DMdatiAge.Qmoduli.ParamByName('tipo').AsString := 'P'
      else
         UniMainModule.DMdatiAge.Qmoduli.ParamByName('tipo').AsString := 'S';
      UniMainModule.DMdatiAge.Qmoduli.ParamByName('codice_slp').AsString := cosa;
      UniMainModule.DMdatiAge.Qmoduli.Open;
      if UniMainModule.DMdatiAge.Qmoduli.IsEmpty then
         Result:=''
      else
         Result:=Copy(UniMainModule.DMdatiAge.Qmoduli.FieldByName('descrizione').AsString,1,40);
   end;
begin
   // lunghezza massima descrizione 40 caratteri
   if Copy(sigla,1,2)='P-' then Result:= Cerca(Copy(sigla,3,5))   // P- sigla polizza
   else if Copy(sigla,1,3)='ALL' then Result:= Cerca(Copy(sigla,1,4))  // ALL3 oppure ALL4
   else if Copy(sigla,1,4)='ADEG' then Result:= Cerca(Copy(sigla,1,4))  //
   else if Copy(sigla,1,3)='SET' then Result:= Cerca(sigla)  //  SET  + sigla polizza
   else if Copy(sigla,1,3)='DIP' then Result:= Cerca(sigla)  //  DIP  + sigla polizza
   else if Copy(sigla,1,3)='PRI' then Result:= Cerca(Copy(sigla,1,3))  //  PRI
   else if Copy(sigla,1,2)='NI' then Result:= Cerca(sigla)  //  NI + sigla polizza = vecchia nota informativa
   else if Copy(sigla,1,2)='FI' then Result:= 'Fascicolo infor.'
   else if Copy(sigla,1,4)='PREC' then Result:= Cerca(sigla)
   else if Copy(sigla,1,4)='COER' then Result:= Cerca(Copy(sigla,1,4))
   else if Copy(sigla,1,2)='IP' then Result:= 'Informativa al Contraente'
   else Result:='';
end;


procedure TDMdatiAge.DataModuleDestroy(Sender: TObject);
begin
  if DBagenti4web.InTransaction then
    DBagenti4web.Rollback;
  SessionAgenzia.Close;
  DBagenti4web.Close;
end;

procedure TDMdatiAge.DBagenti4webAfterConnect(Sender: TObject);
var
  l_Dati_gen: TDati_gen_rec;
begin
  l_Dati_gen       := SLPdati_age.dati_gen;
  l_Dati_gen.sativ := GetSativ;
  dati_gen         := l_Dati_gen;

end;

procedure TDMdatiAge.decodeCF(cf: string; var nato_il: TdateTime; var nato_a: string);
var
  // t1, t2: string;
  gg, mm, aa: word;

const
  CodiciMesi: String = 'ABCDEHLMPRST';

begin
  // procedura che dato un codice fiscale restituisce il luogo di nascita e la data...

  aa := strToint(copy(cf, 7, 2));
  if aa < 10 then
    inc(aa, 2000)
  else
    inc(aa, 1900);
  gg := strToint(copy(cf, 10, 2));
  if gg > 31 then
    dec(gg, 40);
  mm      := pos(copy(cf, 9, 1), CodiciMesi);
  nato_il := encodedate(aa, mm, gg);
  nato_a  := '';
  QCapItalia.Close;
  QCapItalia.ParamByName('IDCODFISCALE').asString := copy(cf, 12, 4);
  QCapItalia.Open;
  if not QCapItalia.IsEmpty then
    nato_a := QCapItalia.FieldByName('NOME_COMUNE').asString;
  QCapItalia.Close;
end;

function TDMdatiAge.esiste_coll(cod_coll: Integer): Boolean;
begin
   // controlla se il codice di collaboratore passato esiste nell'archivio dei collaboratori
   QcercaProdutt.Close;
   QcercaProdutt.ParamByName('cod_produttore').AsInteger:=cod_coll;
   QcercaProdutt.Open;
   Result := QcercaProdutt.RecordCount>0;
end;


function TDMdatiAge.FindPolizza(const cod_polizza: integer; var num_polizza: string): boolean;
begin
  if QPolizze.state = dsInactive then
    QPolizze.Open;
  result      := QPolizze.Locate('COD_POLIZZA', cod_polizza, []);
  num_polizza := QPolizze.FieldByName('n_polizza').asString;
end;

function TDMdatiAge.find_durata(durata: Integer; titpo: string): Boolean;
begin
   // controlla se esiste la durata inserita
   Qfind_durata.Close;
   Qfind_durata.ParamByName('durata').AsInteger:=durata;
   Qfind_durata.ParamByName('tipo').AsString:=titpo;
   Qfind_durata.Open;
   result := Qfind_durata.RecordCount>0 ;
end;


function TDMdatiAge.FindPolizza(const N_polizza: string): boolean;
begin
  if QPolizze.state = dsInactive then
    QPolizze.Open;
  result := QPolizze.Locate('N_POLIZZA', N_polizza, []);

end;

function TDMdatiAge.GetDati_gen: TDati_gen_rec;
begin
  result := SLPdati_age.dati_gen;
end;

function TDMdatiAge.VerAutGenAgenzia(AOperazione: string): boolean;
begin
  result := false;
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := AOperazione;
  Qget_codicenew.Open;
  if not Qget_codicenew.IsEmpty then
  begin
    // se l'autorizzazione � gestita alla vecchia maniera con il campo valore
    if Qget_codicenew.FieldByName('valore').asString > '' then
    begin
      if trim(Qget_codicenew.FieldByName('valore').asString) = 'S' then
        result := true
      else
        result := false;
    end
    else // gestita con il nuovo metodo
      result := Qget_codicenew.FieldByName('VAL_BOOLEAN').asBoolean;
  end;
  Qget_codicenew.Close;
end;

function TDMdatiAge.VerificaAutorizzazione(AArchivio, AOperazione: string): boolean;
begin
  if QUtenteAutorizzazioni.state = dsInactive then
  begin
    QUtenteAutorizzazioni.Close;
    QUtenteAutorizzazioni.ParamByName('UTENTE').asInteger := UniMainModule.utenteCodice;
    QUtenteAutorizzazioni.Open;
  end;
  result := QUtenteAutorizzazioni.Locate('Archivio;Operazione', varArrayOf([AArchivio, AOperazione]), []) and
    QUtenteAutorizzazioni.FieldByName('Attivo').asBoolean;
end;

function TDMdatiAge.isAssitorino: boolean;
begin
  result := (SLPdati_age.cod_int = ' 1857') or (SLPdati_age.cod_int = '  400');
end;

function TDMdatiAge.isDirezione: boolean;
begin
  { TODO -oAngelo -cRefactoring : implementare }
  result := false;
end;

function TDMdatiAge.isExtensionEnabled(const ExtensionDescr: string): boolean;
var
  appendiceStr: string;
begin
  get_codice(ExtensionDescr, appendiceStr);
  result := (appendiceStr <> '') and (appendiceStr[1] = 'S');
end;

function TDMdatiAge.is_utente_promoter: boolean;
begin
  // true se l'utente corrente � anche un promoter e deve avere la visibilit� bloccata ai suoi clienti
  result := ((UniMainModule.utentePromoter > 0) and (UniMainModule.utenteLivello < 11));
end;

function TDMdatiAge.leggiAliquote(ACodAgenzia: integer): Currency;
begin
  QAliquote.Close;
  QAliquote.SQL.Clear;
  QAliquote.SQL.Append('select * from aliquote');
  QAliquote.SQL.Append('where ID_GEN_SLP_AGE = :codAgenzia');
  QAliquote.SQL.Append('and rif_cod_compagnia = :codCompagnia');
  QAliquote.SQL.Append('and tipo = :tipo');
  QAliquote.SQL.Append('and durata = :durata');

  QAliquote.ParamByName('ID_GEN_SLP_AGE').asInteger     := ACodAgenzia;
  QAliquote.ParamByName('RIF_COD_COMPAGNIA ').asInteger := 1;
  QAliquote.ParamByName('TIPO').asString                := 'R';
  QAliquote.ParamByName('DURATA').asInteger             := 1;
  QAliquote.Open;
  result := QAliquote.FieldByName('ACCESSORI_AGE').asCurrency;
  QAliquote.Close;

end;

procedure TDMdatiAge.LoadModelloReport;
var
  blobs: TStream;
begin
  QModSLP.Close;
  QModSLP.ParamByName('TIPO').asString      := 'P';
  QModSLP.ParamByName('SIGLA_POL').asString := UniMainModule.SiglaPolizza;
  QModSLP.Open;
  frxPolizzaGen.Clear;
  if not QModSLP.IsEmpty then
  begin
    blobs := QModSLP.CreateBlobStream(QModSLP.FieldByName('TESTO'), bmRead);
    frxPolizzaGen.LoadFromStream(blobs);
    blobs.Free;
  end
  else
    raise EPolizzaError.Create(CERR_MOD_REPORT_POLIZZA, Format(MSG_MOD_REPORT_POLIZZA, [UniMainModule.SiglaPolizza]));

end;

function TDMdatiAge.MemorizzaReportPolizza(AStream: TStream; NPolizza, tipor: string; dataEmissione: TdateTime;
  bVisible: boolean): integer;
var
  lDataBase: TDBISAMDatabase;
  tablesList: Tstringlist;

begin
  AStream.Position := 0;
  result           := 0;
  tablesList       := Tstringlist.Create;
  try
    lDataBase := QinsStampa.DBSession.Databases[0];
    tablesList.Append('PRINTHOUSE');
    if not lDataBase.InTransaction then
      lDataBase.StartTransaction(tablesList);
    try
      QinsStampa.Close;
      QinsStampa.ParamByName('TIPO_DOC').asString       := tipor;
      QinsStampa.ParamByName('DATA_STAMPA').AsDateTime  := Date;
      QinsStampa.ParamByName('DATA_STAMPA1').AsDateTime := now;
      QinsStampa.ParamByName('N_POLIZZA').asString      := NPolizza;
      QinsStampa.ParamByName('DOC').LoadFromStream(AStream, ftBlob);
      QinsStampa.ParamByName('VISIBILE').asString        := IfThen(bVisible, 'S', 'N');
      QinsStampa.ParamByName('DESCRIZIONE').asString     := dai_descrizione_ph(tipor) ;
      QinsStampa.ParamByName('DATA_DOC').AsDateTime      := dataEmissione;
      QinsStampa.ParamByName('ID_GEN_SLP_AGE').asInteger := strToint(SLPdati_age.cod_int);
      QinsStampa.execSql;
      QgetLastCodDoc.Close;
      QgetLastCodDoc.Open;
      result := QgetLastCodDoc.FieldByName('CODDOC').asInteger;
      QgetLastCodDoc.Close;
      lDataBase.Commit(true);
    except
      on EDBISAMEngineError do
        lDataBase.Rollback;

    end;
  finally
    tablesList.Free;
  end;
end;

procedure TDMdatiAge.leggiAccessoriProvvigioni(ACodAgenzia: integer; var PercAccessoriAGe: Currency;
  var ProvvigioneAge: Currency; CodCompagnia: integer = 1; tipo: char = 'R'; durata: integer = 1);
begin
  QAliquote.Close;
  // QAliquote.SQL.Clear;
  // QAliquote.SQL.Append('select * from aliquote');
  // QAliquote.SQL.Append('where ID_GEN_SLP_AGE = :codAgenzia');
  // QAliquote.SQL.Append('and rif_cod_compagnia = :codCompagnia');
  // QAliquote.SQL.Append('and tipo = :tipo');
  // QAliquote.SQL.Append('and durata = :durata');

  QAliquote.ParamByName('CODAGENZIA').asInteger   := ACodAgenzia;
  QAliquote.ParamByName('CODCOMPAGNIA').asInteger := CodCompagnia;
  QAliquote.ParamByName('TIPO').asString          := tipo;
  QAliquote.ParamByName('DURATA').asInteger       := durata;
  QAliquote.Open;

  if not QAliquote.IsEmpty then
  begin
    PercAccessoriAGe := QAliquote.FieldByName('ACCESSORI_AGE').asCurrency;
    ProvvigioneAge   := QAliquote.FieldByName('PROVVIGIONE').asCurrency;
  end
  else
  begin
    PercAccessoriAGe := 0.0;
    ProvvigioneAge   := 0.0;
  end;

  // if result = 0 then
  // begin
  //
  // // possibile durata balorda
  // if durata in [4, 6, 7, 8, 9, 10] then
  // // calcolo da v 1.0.6.2 per evitare porcate da agenti che hanno durata max 5 anni
  // if durata = 4 then
  // begin
  // // if DMdatiage.Taliquote.locate('rif_cod_compagnia;tipo;durata;tipo_aliquota',vararrayof([1,tipo,3,rischio]),[]) then
  // if QAliquote.Locate('rif_cod_compagnia;tipo;durata', varArrayOf([1, tipo, 3]), []) then
  // result := QAliquote.FieldByName('provvigione').asCurrency;
  // end
  // else
  // if durata in [6, 7, 8, 9, 10] then
  // // if DMdatiage.Taliquote.locate('rif_cod_compagnia;tipo;durata;tipo_aliquota',vararrayof([1,tipo,5,rischio]),[]) then
  // if QAliquote.Locate('rif_cod_compagnia;tipo;durata', varArrayOf([1, tipo, 5]), []) then
  // result := QAliquote.FieldByName('provvigione').asCurrency;
  //
  // end;

  QAliquote.Close;

end;

procedure TDMdatiAge.PosizionaQuery(AValue: TValue);
begin
  QAgenziaEdit.Close;
  QAgenziaEdit.ParamByName('ID_GEN_SLP_AGE').asInteger := AValue.asInteger;
  QAgenziaEdit.Open;
end;

procedure TDMdatiAge.QAgenziaEditBeforePost(DataSet: TDataSet);
begin
  if (QAgenziaEdit.FieldByName('TIPO').asString = 'S') and
    ((QAgenziaEdit.FieldByName('n_rui').asString = '') or (QAgenziaEdit.FieldByName('data_iscrizione_rui').IsNull)) then
    raise EPolizzaError.Create(CERR_AGE_DATI_RUI_MISSED, MSG_AGE_DATI_RUI_MISSED);

end;

procedure TDMdatiAge.SessionAgenziaPassword(Sender: TObject; var Continue: boolean);
begin
  SessionAgenzia.AddPassword(UniServerModule.EncriptionPassword);
  Continue := true;
end;

procedure TDMdatiAge.SetDati_gen(const Value: TDati_gen_rec);
begin
  FSLPdati_age.dati_gen := Value;
end;

procedure TDMdatiAge.caricaPromoter(items: Tstringlist);
begin
  // carica i promoter
  QSelPromoter.Open;
  QSelPromoter.First;
  items.Clear;
  items.Add('nessuno=0');
  while not QSelPromoter.Eof do
  begin
    if QSelPromoter.FieldByName('nome').asString > '' then
      items.Add(QSelPromoter.FieldByName('nome').asString + '=' + QSelPromoter.FieldByName('cod_produttore').asString);
    QSelPromoter.Next;
  end;
  QSelPromoter.Close;
end;

procedure TDMdatiAge.carica_compagnie(items: Tstringlist);
begin
  // carica le compagnie in CBompagnie
  QCompagnie.Open;
  QCompagnie.First;
  items.Clear;
  items.Add('Tutte le compagnie=0');
  while not QCompagnie.Eof do
  begin
    items.Add(QCompagnie.FieldByName('nome').asString + '=' + QCompagnie.FieldByName('cod_compagnia').asString);
    QCompagnie.Next;
  end;
  QCompagnie.Close;
end;

procedure TDMdatiAge.ClearRuiData;
begin
  QAgenziaEdit.FieldByName('N_RUI').Clear;
  QAgenziaEdit.FieldByName('DATA_ISCRIZIONE_RUI').Clear;
end;

function TDMdatiAge.get_codiceX(nome_campo: string; var valore: integer): boolean;
begin
  // funzione che gestisce l'accesso al file codici
  Qget_codice.SQL.Clear;
  Qget_codice.SQL.Add('update codici');
  Qget_codice.SQL.Add('set  ' + nome_campo + ' = ' + nome_campo + ' + 1');
  Qget_codice.SQL.Add('commit flush;');
  Qget_codice.SQL.Add('select ' + nome_campo + ' from codici');
  try
    Qget_codice.Open;
    valore := Qget_codice.FieldValues[nome_campo];
  except
    valore := -1;
  end;
  Qget_codice.Close;
  result := true;
end;

function TDMdatiAge.get_perc_pro_insolute: integer;
begin
  // recupera la percentuale delle provvigioni orginali che l'agenzia pu�
  // trattenere se incassa dopo 60 giorni dalla data di scadenza
  // se il valore non � stato istanziato, impostalo a 50%
  get_codice('PERC_PRO_INSO', result);
  if result = 0 then
    result := 50;
end;

function TDMdatiAge.get_provv_collab: Currency;
begin
  // prendi la percentuale delleprovvigioni dell'agente che il collaboratore corrente
  // deve trattenere
  result := 0;
  QProdut.Open;
  if QProdut.Locate('cod_produttore', UniMainModule.utentePromoter, []) then
  begin
    if is_utente_promoter then
      result := QProdut.FieldByName('perc_provv_age').asCurrency;
  end;
end;

function TDMdatiAge.get_provv_SubCollab: Currency;
begin
  // prendi la percentuale delleprovvigioni dell'agente che il collaboratore corrente
  // deve trattenere
  result := 0;
  QProdut.Open;
  if QProdut.Locate('cod_produttore', UniMainModule.utenteSubPromoter, []) then
  begin
    if is_utente_promoter then
      result := QProdut.FieldByName('perc_provv_age').asCurrency;
  end;
end;

procedure TDMdatiAge.InsCodice(IDCampo: string; valore: TValue);
var
  BooleanVal: boolean;
  IntegerVal: integer;
  StringVal: string;
  CrurrencyVal: Currency;
  DateVal: TDate;
begin

  Qins_codicenew.ParamByName('VAL_BOOLEAN').Clear;
  Qins_codicenew.ParamByName('VAL_DATE').Clear;
  Qins_codicenew.ParamByName('VAL_INTEGER').Clear;
  Qins_codicenew.ParamByName('VAL_CURRENCY').Clear;
  Qins_codicenew.ParamByName('VAL_STRING').Clear;
  Qins_codicenew.ParamByName('VALORE').Clear;

  Qins_codicenew.ParamByName('ID_CAMPO').asString := IDCampo;

  if valore.TryAsType<boolean>(BooleanVal) then
    Qins_codicenew.ParamByName('VAL_BOOLEAN').asBoolean := BooleanVal;

  if valore.TryAsType<integer>(IntegerVal) then
    Qins_codicenew.ParamByName('VAL_INTEGER').asInteger := IntegerVal;

  if valore.TryAsType<Currency>(CrurrencyVal) then
    Qins_codicenew.ParamByName('VAL_CURRENCY').asCurrency := CrurrencyVal;

  if valore.TryAsType<TDate>(DateVal) then
    Qins_codicenew.ParamByName('VAL_DATE').asCurrency := DateVal;

  if valore.TryAsType<String>(StringVal) then
    Qins_codicenew.ParamByName('VAL_STRING').asString := StringVal;

  Qins_codicenew.execSql;

end;

function TDMdatiAge.getCodice(IDCampo: string; AFieldType: TFieldType): TValue;
begin
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := IDCampo;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
  begin
    case AFieldType of
      ftBoolean:
        result := default (boolean);
      ftInteger:
        result := default (integer);
      ftCurrency:
        result := default (Currency);
      ftDate:
        result := default (TDate);
      ftDateTime:
        result := default (TdateTime);
      ftString:
        result := default (string);
    end;
    InsCodice(IDCampo, result);
  end
  else
  begin
    if not Qget_codicenew.FieldByName('VAL_BOOLEAN').IsNull then
      result := Qget_codicenew.FieldByName('VAL_BOOLEAN').asBoolean;

    if not Qget_codicenew.FieldByName('VAL_INTEGER').IsNull then
      result := Qget_codicenew.FieldByName('VAL_INTEGER').asInteger;

    if not Qget_codicenew.FieldByName('VAL_CURRENCY').IsNull then
      result := Qget_codicenew.FieldByName('VAL_CURRENCY').asCurrency;

    if not Qget_codicenew.FieldByName('VAL_DATE').IsNull then
      result := Qget_codicenew.FieldByName('VAL_DATE').AsDateTime;

    if not Qget_codicenew.FieldByName('VAL_STRING').IsNull then
      result := Qget_codicenew.FieldByName('VAL_STRING').asString;
  end;

end;

procedure TDMdatiAge.setCodice(IDCampo: string; valore: TValue);
begin
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := IDCampo;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
    InsCodice(IDCampo, valore);
end;

procedure TDMdatiAge.get_codice(cosa: string; var valore: TdateTime);
begin
  // funzione che gestisce l'accesso al file codiciNew
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := cosa;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
  begin
    Qins_codicenew.Close;
    Qins_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qins_codicenew.ParamByName('valore').asString   := '20000101';
    Qins_codicenew.execSql;
    valore := stod('20000101');
  end
  else
  begin
    valore := stod(trim(Qget_codicenew.FieldByName('valore').asString));
  end;
end;

procedure TDMdatiAge.get_codice(cosa: string; var valore: integer);
begin
  // funzione che gestisce l'accesso al file codiciNew
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := cosa;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
  begin
    Qins_codicenew.Close;
    Qins_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qins_codicenew.ParamByName('valore').asString   := '0';
    Qins_codicenew.ParamByName('VAL_BOOLEAN').Clear;
    Qins_codicenew.ParamByName('VAL_DATE').Clear;
    Qins_codicenew.ParamByName('VAL_INTEGER').Clear;
    Qins_codicenew.ParamByName('VAL_CURRENCY').Clear;
    Qins_codicenew.ParamByName('VAL_STRING').Clear;

    Qins_codicenew.execSql;
    valore := 0;
  end
  else
  begin
    if (length(trim(Qget_codicenew.FieldByName('valore').asString)) = 0) then
      valore := 0
    else
      valore := strToint(trim(Qget_codicenew.FieldByName('valore').asString));
  end;
end;

function TDMdatiAge.getDesFrazionamentoPolizza(AFrazionamento: string): string;
var
  Fraz: char;
begin
  if AFrazionamento = '' then
    result := ''
  else
  begin
    Fraz := AFrazionamento[1];
    case Fraz of
      'A':
        result := 'Annuale';
      'S':
        result := 'Semestrale';
      'T':
        result := 'Trimestrale';
      'Q':
        result := 'Quadrimestrale';
      'B':
        result := 'Bimestrale';
      'M':
        result := 'Mensile';
      'P':
        result := 'temporaneo';
      'U':
        result := 'Unico';
    else
      result := '';
    end;
  end;
end;

function TDMdatiAge.getDurataMax: integer;
begin
  Qdurata_max.Open;
  result := Qdurata_max.FieldByName('dmax').asInteger;
  Qdurata_max.Close;
end;

function TDMdatiAge.getNomePromoter(ACodPromoter: integer): string;
begin
  result := '';
  if QSelPromoter.state = dsInactive then
    QSelPromoter.Open;

  if QSelPromoter.Locate('COD_PRODUTTORE', ACodPromoter, []) then
    result := QSelPromoter.FieldByName('NOME').asString;
end;

function TDMdatiAge.GetSativ: string;
begin
  QCodici.Open;
  result := QCodici.FieldByName('PSWAGENZIA').asString;
end;

function TDMdatiAge.getTacitoRinnovoEnabled: boolean;
var
  TacitoRinnovo: string;
begin
  get_codice('SENZA_T_R', TacitoRinnovo);
  result := copy(TacitoRinnovo, 1, 1) = 'S';
end;

procedure TDMdatiAge.get_codice(cosa: string; var valore: string);
begin
  // funzione che gestisce l'accesso al file codiciNew
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := cosa;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
  begin
    Qins_codicenew.Close;
    Qins_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qins_codicenew.ParamByName('valore').asString   := '';
    Qins_codicenew.ParamByName('VAL_BOOLEAN').Clear;
    Qins_codicenew.ParamByName('VAL_DATE').Clear;
    Qins_codicenew.ParamByName('VAL_INTEGER').Clear;
    Qins_codicenew.ParamByName('VAL_CURRENCY').Clear;
    Qins_codicenew.ParamByName('VAL_STRING').Clear;

    Qins_codicenew.execSql;
    valore := '';
  end
  else
  begin
    valore := trim(Qget_codicenew.FieldByName('valore').asString);
  end;
end;

function TDMdatiAge.get_codiceDef(nome_campo: string; DefValue: integer): integer;
// var
// resval: integer;
begin
  Qget_codice.SQL.Clear;
  Qget_codice.SQL.Add('select valore from codicinew');
  Qget_codice.SQL.Add('where ID_CAMPO = :idCampo');
  Qget_codice.ParamByName('IDCAMPO').asString := nome_campo;
  Qget_codice.Open;
  try
    if not Qget_codice.IsEmpty then

      result := Qget_codice.FieldByName(nome_campo).asInteger
    else
      result := DefValue;
  finally
    Qget_codice.Close;
  end;

end;

procedure TDMdatiAge.set_codice(cosa: string; valore: TdateTime);
begin
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := cosa;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
  begin
    Qins_codicenew.Close;
    Qins_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qins_codicenew.ParamByName('valore').asString   := dtos(valore);
    Qins_codicenew.ParamByName('VAL_BOOLEAN').Clear;
    Qins_codicenew.ParamByName('VAL_DATE').Clear;
    Qins_codicenew.ParamByName('VAL_INTEGER').Clear;
    Qins_codicenew.ParamByName('VAL_CURRENCY').Clear;
    Qins_codicenew.ParamByName('VAL_STRING').Clear;

    Qins_codicenew.execSql;
  end
  else
  begin
    Qmod_codicenew.Close;
    Qmod_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qmod_codicenew.ParamByName('valore').asString   := dtos(valore);
    Qmod_codicenew.execSql;
  end;
end;

procedure TDMdatiAge.set_codice(cosa: string; valore: integer);
begin
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := cosa;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
  begin
    Qins_codicenew.Close;
    Qins_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qins_codicenew.ParamByName('valore').asString   := IntToStr(valore);
    Qins_codicenew.ParamByName('VAL_BOOLEAN').Clear;
    Qins_codicenew.ParamByName('VAL_DATE').Clear;
    Qins_codicenew.ParamByName('VAL_INTEGER').Clear;
    Qins_codicenew.ParamByName('VAL_CURRENCY').Clear;
    Qins_codicenew.ParamByName('VAL_STRING').Clear;

    Qins_codicenew.execSql;
  end
  else
  begin
    Qmod_codicenew.Close;
    Qmod_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qmod_codicenew.ParamByName('valore').asString   := IntToStr(valore);
    Qmod_codicenew.execSql;
  end;
end;

procedure TDMdatiAge.setEncriptionPassword(const Value: string);
begin
  Session.active := true;
  Session.AddPassword(Value);
end;

procedure TDMdatiAge.SetTacitoRinnovoEnabled(const Value: boolean);
begin
  FTacitoRinnovoEnabled := Value;
end;

procedure TDMdatiAge.set_codice(cosa: string; valore: string);
begin
  Qget_codicenew.Close;
  Qget_codicenew.ParamByName('ID_CAMPO').asString := cosa;
  Qget_codicenew.Open;

  if Qget_codicenew.IsEmpty then
  begin
    Qins_codicenew.Close;
    Qins_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qins_codicenew.ParamByName('valore').asString   := valore;
    Qins_codicenew.ParamByName('VAL_BOOLEAN').Clear;
    Qins_codicenew.ParamByName('VAL_DATE').Clear;
    Qins_codicenew.ParamByName('VAL_INTEGER').Clear;
    Qins_codicenew.ParamByName('VAL_CURRENCY').Clear;
    Qins_codicenew.ParamByName('VAL_STRING').Clear;

    Qins_codicenew.execSql;
  end
  else
  begin
    Qmod_codicenew.Close;
    Qmod_codicenew.ParamByName('ID_CAMPO').asString := cosa;
    Qmod_codicenew.ParamByName('valore').asString   := valore;
    Qmod_codicenew.execSql;
  end;
end;

function TDMdatiAge.staccaNumeroPolizza(sigla: string; bUpdateNumero: boolean): integer;

  procedure UpdateNumeroPolizza;
  var
    tablesList: Tstringlist;
    lDataBase: TDBISAMDatabase;
  begin
    if bUpdateNumero then
    begin
      tablesList := Tstringlist.Create;
      try
        lDataBase := QUpdNumeroPolizza.DBSession.Databases[0];
        tablesList.Append('IND_POLIZZE');
        if not lDataBase.InTransaction then
          lDataBase.StartTransaction(tablesList);
        try
          QUpdNumeroPolizza.ParamByName('NUMEROPOLIZZA').asString := QStaccaNumeroPolizza.FieldByName('numero')
            .asString;
          QUpdNumeroPolizza.execSql;
          lDataBase.Commit(true);
          result := QStaccaNumeroPolizza.FieldByName('numero').asInteger;
        except
          on EDBISAMEngineError do
            lDataBase.Rollback;
        end;
      finally
        tablesList.Free;
      end;
    end;
  end;

begin
  // Log('prima di QStaccaNumeroPolizza.open linea 1118' );
  QStaccaNumeroPolizza.Close;
  QStaccaNumeroPolizza.ParamByName('sigla').asString := sigla;
  QStaccaNumeroPolizza.Open;
  // Log('dopo di QStaccaNumeroPolizza.open linea 1118' );
  if QStaccaNumeroPolizza.IsEmpty then
  begin
    // if QStaccaNumeroPolizza.FieldByName('numero').asString = '' then
    // MessageDlg('Numerazione per la stampa della polizza finita; richiedere una nuova numerazione in direzione!',
    // mtInformation, [mbOK], 0);
    result := -1;
    raise EPolizzaError.Create(CERR_NUM_POL_ENDED, MSG_NUM_POL_ENDED);
  end;

  try
    // CheckNumeroPolizzaDuplicato(QStaccaNumeroPolizza.FieldByName('numero').asString);
    // Log('prima di UpdateNumeroPolizza 1134' );
    UpdateNumeroPolizza;
    // Log('dopo di UpdateNumeroPolizza 1134' );
    result := QStaccaNumeroPolizza.FieldByName('numero').asInteger;
  finally
    QStaccaNumeroPolizza.Close;
  end;
end;

function TDMdatiAge.get_n_fc: string;
var
  n_fc: integer;
begin
  if not QCompagnie.active then
    QCompagnie.Open;
  QnumeroFC.Open;
  n_fc   := QnumeroFC.FieldByName('NUMERO_FC').asInteger + 1;
  result := SLPdati_age.agenzia + '.' + IntToStr(n_fc).PadLeft(4, '0');
end;

function TDMdatiAge.get_intervallo_chiusura_contabile: integer;
begin
  if not QDati_agenzia.active then
    QDati_agenzia.Open;

  if QDati_agenzia.FieldByName('numero_chiusure').asInteger = 1 then
    result := 30
  else
    if QDati_agenzia.FieldByName('numero_chiusure').asInteger = 2 then
      result := 15
    else
      if QDati_agenzia.FieldByName('numero_chiusure').asInteger = 3 then
        result := 10
      else
        result := 0;
end;

function TDMdatiAge.get_dati_pagamentoFC: string;
begin
  // carica i dati relativi
  QcompagniSLP.Open;
  result := QcompagniSLP.FieldByName('PAGFC_banca').asString + ', IBAN ' + QcompagniSLP.FieldByName('PAGFC_IBAN')
    .asString;
    // modifica del 13/11/2020 per obbligare gi agenti a pagare su generali
    // .asString + ' oppure    C.C. postale IBAN IT24  B076 0101 000 0000 10778108';

  QcompagniSLP.Close;
end;

procedure TDMdatiAge.caricaTipoVeicolo(lista: Tstrings);
var
  tt: string;
begin
  // carica i tipi di veicolo
  lista.Clear;
  Qtipo_veicolo.Open;
  Qtipo_veicolo.First;
  while not Qtipo_veicolo.Eof do
  begin
    tt := Qtipo_veicolo.FieldByName('SIGLA').asString + ' = ' + Qtipo_veicolo.FieldByName('DESCRIZIONE').asString;
    lista.Add(tt);
    Qtipo_veicolo.Next;
  end;
  Qtipo_veicolo.Close;
end;

{ TODO 5 -oAngelo -cRefactoring : Dal Momento che la query � nella unit MainModule sarebbe meglio spostare li tutta la funzione }
function TDMdatiAge.get_mod_slp_gen(cod_stampa: integer; var blobs: TStream): boolean;
begin
  // 02/09/2019 versione 1.6.3.9
  // introdotta la centralizzazione del campo blob del file mod_slp
  // aggiunto file mod_slp_gen nell'area SCAMBIO per far scaricare i blob con i moduli di stampa sempre
  // dallo stesso file per tutte le agenzie in modo da rendere + veloce le modifiche e ridurre l'occupazione di spazio

  // per sicurezza prevedi l'attivazione di questa funzione con un tag nel file codici_new

  // nel file mod_slp locale prendere il campo rif_mod_slp_gen che punta al cod_stampa del file mod_slp_gen a visibilit� unica per tutte le agenzie

  Qmod_slp.Close;
  Qmod_slp.ParamByName('COD_STAMPA').asInteger := cod_stampa;
  Qmod_slp.Open;

  if VerAutGenAgenzia('MOD_SLP_GEN') then
  begin
    // parte nuova con accesso a file unificato
    UniMainModule.Qmod_slp_gen.Close;
    UniMainModule.Qmod_slp_gen.ParamByName('COD_STAMPA').asInteger := Qmod_slp.FieldByName('RIF_MOD_SLP_GEN').asInteger;
    UniMainModule.Qmod_slp_gen.Open;
    result := true;
    if not UniMainModule.Qmod_slp_gen.IsEmpty then
      blobs := UniMainModule.Qmod_slp_gen.CreateBlobStream(UniMainModule.Qmod_slp_gen.FieldByName('TESTO'), bmRead)
    else
      result := false;
  end
  else
  begin
    // parte vecchia con accesso a file locale ad ogni agenzia
    result := true;
    if not Qmod_slp.IsEmpty then
    begin
      if not Qmod_slp.FieldByName('TESTO').IsNull then
      begin
        blobs := Qmod_slp.CreateBlobStream(Qmod_slp.FieldByName('TESTO'), bmRead);
      end
      else
        result := false;
    end
    else
      result := false;
  end;
end;

function TDMdatiAge.get_mod_slp_genX(cod_stampa: integer; var blobs: TMemoryStream): boolean;
begin
  // 02/09/2019 versione 1.6.3.9
  // introdotta la centralizzazione del campo blob del file mod_slp
  // aggiunto file mod_slp_gen nell'area SCAMBIO per far scaricare i blob con i moduli di stampa sempre
  // dallo stesso file per tutte le agenzie in modo da rendere + veloce le modifiche e ridurre l'occupazione di spazio

  // per sicurezza prevedi l'attivazione di questa funzione con un tag nel file codici_new

  // nel file mod_slp locale prendere il campo rif_mod_slp_gen che punta al cod_stampa del file mod_slp_gen a visibilit� unica per tutte le agenzie

  Qmod_slp.Close;
  Qmod_slp.ParamByName('COD_STAMPA').asInteger := cod_stampa;
  Qmod_slp.Open;

  {
    if VerAutGenAgenzia('MOD_SLP_GEN') then
    begin
    // parte nuova con accesso a file unificato
    UniMainModule.Qmod_slp_gen.Close;
    UniMainModule.Qmod_slp_gen.ParamByName('COD_STAMPA').asInteger := Qmod_slp.FieldByName('RIF_MOD_SLP_GEN').asInteger;
    UniMainModule.Qmod_slp_gen.Open;
    result := true;
    if not UniMainModule.Qmod_slp_gen.IsEmpty then
    blobs := UniMainModule.Qmod_slp_gen.CreateBlobStream(UniMainModule.Qmod_slp_gen.FieldByName('TESTO'), bmRead)
    else
    result := false;
    end
    else }
  begin
    // parte vecchia con accesso a file locale ad ogni agenzia
    result := true;
    if not Qmod_slp.IsEmpty then
    begin
      if not Qmod_slp.FieldByName('TESTO').IsNull then
      begin
        // blobs := Qmod_slp.CreateBlobStream(Qmod_slp.FieldByName('TESTO'), bmRead);
        TBlobField(Qmod_slp.FieldByName('TESTO')).SaveToStream(blobs);
        blobs.Position := 0; // reset to start of stream....
      end
      else
        result := false;
    end
    else
      result := false;
  end;
end;

function TDMdatiAge.getDescrizioneVeicolo(sigla: string): string;
begin
  QgetDescrizioneVeicolo.Close;
  QgetDescrizioneVeicolo.ParamByName('sigla').asString := sigla;
  QgetDescrizioneVeicolo.Open;
  result := QgetDescrizioneVeicolo.FieldByName('descrizione').asString;
  if not(result > '') then
    result := sigla;
end;

end.
