inherited DMdatiAgeClienti: TDMdatiAgeClienti
  OldCreateOrder = True
  Height = 421
  Width = 486
  inherited QGetLastID: TDBISAMQuery
    Left = 34
    Top = 72
  end
  inherited tblTemp: TDBISAMTable
    Left = 16
    Top = 240
  end
  object QAssicurati: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select'
      '      CASE'
      
        '      WHEN a4.tipo='#39'C'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))'
      
        '                                               || '#39' - '#39' ||  '#39'Pol' +
        'izza: '#39' || a4.cod_fisc_iva || '#39' - '#39' || cast(a4.data_rilascio as ' +
        'char(10)) || '#39' - '#39' ||'
      
        '                                               cast(data_scadenz' +
        'a as char(10)) || '#39' - '#39' || citta || '#39' - '#39' || denominazione || '#39' ' +
        '- Age: '#39' || indirizzo || '#39' - Premio: '#39' || cast(cifra as char(10)' +
        ')'
      
        '      WHEN a4.tipo='#39'V'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))'
      
        '                                               || '#39' - '#39' ||  '#39'Vei' +
        'colo: '#39' || a4.targa || '#39' - '#39' || a4.marca || '#39' - '#39' || a4.hp_ql ||' +
        ' '#39' - '#39' || a4.modello'
      ''
      
        '      WHEN a4.tipo='#39'P'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))'
      
        '                                               || '#39' - '#39' ||  '#39'Pat' +
        'ente: '#39' || a4.patente || '#39' - Cat.: '#39' || a4.categoria_pat ||  '#39' -' +
        ' Ril. il '#39' ||'
      
        '                                               cast(a4.data_rila' +
        'scio as char(10)) || '#39' - Scad.: '#39' || cast(data_scadenza as char(' +
        '10)) || '#39' - '#39' ||  '#39'Assicurato: '#39'  || a4.denominazione'
      ''
      
        '      WHEN a4.tipo='#39'N'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))'
      
        '                                                || '#39' - '#39' ||  '#39'No' +
        'me: '#39' || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39' ' +
        '- '#39' || a4.citta || '#39' - '#39' ||'
      
        '                                                a4.cap || '#39' - '#39' ' +
        '|| a4.prov || '#39' - '#39' || a4.cod_fisc_iva'
      
        '      WHEN a4.tipo='#39'A'#39' THEN '#39'Entrata:'#39' || cast(extract(DAY, a4.e' +
        'ntrata) as char(2)) || '#39'/'#39' ||cast(extract(MONTH, a4.entrata) as ' +
        'char(2)) || '#39'/'#39' ||cast(extract(YEAR, a4.entrata) as char(4))'
      
        '                                               || '#39' - '#39' ||  '#39'Var' +
        'ie: '#39'  || a4.denominazione   || '#39' - Ind.: '#39' || a4.indirizzo || '#39 +
        ' - '#39' || a4.citta || '#39' - '#39' || a4.cap || '#39' - '#39' || a4.prov'
      '      END AS "In garanzia Tit.",'
      '      cod_assicurati'
      ''
      'FROM   assicurati a4'
      'WHERE  a4.cod_polizza = :rif_cod_polizza'
      '               --a4.cod_assicurati = p2.rif_cod_assicurato'
      '               and a4.uscita is null')
    Params = <
      item
        DataType = ftUnknown
        Name = 'rif_cod_polizza'
      end>
    Left = 32
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rif_cod_polizza'
      end>
  end
  object DSAssicurati: TDataSource
    DataSet = QAssicurati
    Left = 32
    Top = 192
  end
  object Qclienti: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select cod_cliente, denominaz, referente, indirizzo, citta, '
      '          pr.sigla , spr.sigla as siglaSubColl'
      'from clienti cl '
      
        '             left join produtt pr on pr.COD_PRODUTTORE=cl.promot' +
        'er'
      
        '             left join produtt spr on spr.COD_PRODUTTORE=cl.sub_' +
        'promoter'
      'where denominaz > '#39#39
      'and not Stato = -99'
      'order by denominaz'
      'top 5')
    Params = <>
    Left = 37
    Top = 16
  end
  object DSclienti: TDataSource
    DataSet = Qclienti
    Left = 85
    Top = 16
  end
  object DSClienteTitoli: TDataSource
    DataSet = QClienteTitoli
    Left = 213
    Top = 64
  end
  object QClienteTitoli: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from titoli')
    Params = <>
    Left = 213
    Top = 16
  end
  object QeditCliente: TDBISAMQuery
    AfterOpen = QeditClienteAfterOpen
    BeforePost = QeditClienteBeforePost
    AfterPost = QeditClienteAfterPost
    AfterCancel = QeditClienteAfterCancel
    OnNewRecord = QeditClienteNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from clienti'
      'where cod_cliente = :cod_cliente')
    Params = <
      item
        DataType = ftInteger
        Name = 'cod_cliente'
      end>
    Left = 317
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'cod_cliente'
      end>
  end
  object DSeditCliente: TDataSource
    DataSet = QeditCliente
    Left = 317
    Top = 64
  end
  object Qlivello_cli: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from livelli_cli')
    Params = <>
    Left = 208
    Top = 128
  end
  object DSlivello_cli: TDataSource
    DataSet = Qlivello_cli
    Left = 208
    Top = 200
  end
  object QFindReferente: TDBISAMQuery
    BeforePost = QeditClienteBeforePost
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select cl.*'
      'from clienti cl'
      'where cod_cliente = :rif_referente')
    Params = <
      item
        DataType = ftUnknown
        Name = 'rif_referente'
      end>
    Left = 317
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'rif_referente'
      end>
  end
  object QClienteHasPolizze: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select count(cod_polizza) as Num_polizze from polizze'
      'where cod_cli = :CodCli'
      'union '
      'select count(cod_polizza) as Num_polizze from slp_tpolizze'
      'where cod_cli = :CodCli '
      'and status is null')
    Params = <
      item
        DataType = ftInteger
        Name = 'CodCli'
      end
      item
        DataType = ftInteger
        Name = 'CodCli'
      end>
    Left = 109
    Top = 120
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CodCli'
      end
      item
        DataType = ftInteger
        Name = 'CodCli'
      end>
  end
  object QCancellaCliente: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'update clienti'
      'set  Stato = -99'
      'where cod_cliente = :cod_cliente')
    Params = <
      item
        DataType = ftInteger
        Name = 'cod_cliente'
      end>
    Left = 317
    Top = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'cod_cliente'
      end>
  end
  object QUpdCliente: TDBISAMQuery
    AfterOpen = QeditClienteAfterOpen
    BeforePost = QeditClienteBeforePost
    OnNewRecord = QeditClienteNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'update clienti'
      'set profession = :professione,'
      '      descrizione_attivita = :descrAttivita'
      'where cod_cliente=:codcliente')
    Params = <
      item
        DataType = ftInteger
        Name = 'professione'
      end
      item
        DataType = ftString
        Name = 'descrAttivita'
      end
      item
        DataType = ftInteger
        Name = 'codcliente'
      end>
    Left = 117
    Top = 192
    ParamData = <
      item
        DataType = ftInteger
        Name = 'professione'
      end
      item
        DataType = ftString
        Name = 'descrAttivita'
      end
      item
        DataType = ftInteger
        Name = 'codcliente'
      end>
  end
  object Qprofessioni: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from profess'
      'where cod_professione = :cod_professione')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_professione'
      end>
    Left = 149
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_professione'
      end>
  end
  object QProfessioniAll: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from profess'
      'order by descriz')
    Params = <>
    Left = 109
    Top = 72
  end
  object QTelefoniMail: TDBISAMQuery
    BeforePost = QTelefoniMailBeforePost
    AfterPost = QTelefoniMailAfterPost
    OnNewRecord = QTelefoniMailNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from telefoni'
      'where categoria = '#39'C'#39
      'and cod_cli = :cod_cli')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    Left = 109
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    object QTelefoniMailNumero: TStringField
      FieldName = 'Numero'
      Size = 30
    end
    object QTelefoniMailDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 40
    end
    object QTelefoniMailCategoria: TStringField
      FieldName = 'Categoria'
      Size = 1
    end
    object QTelefoniMailTitolo: TStringField
      FieldName = 'Titolo'
      Size = 1
    end
    object QTelefoniMailcod_cli: TIntegerField
      FieldName = 'cod_cli'
    end
    object QTelefoniMailrif_operatore: TIntegerField
      FieldName = 'rif_operatore'
    end
    object QTelefoniMailDescrTitolo: TStringField
      FieldKind = fkLookup
      FieldName = 'DescrTitolo'
      LookupDataSet = fdmtblTipo
      LookupKeyFields = 'TipoTelMail'
      LookupResultField = 'TelMailDescr'
      KeyFields = 'Titolo'
      Lookup = True
    end
    object QTelefoniMailvalore_default: TStringField
      FieldName = 'valore_default'
      Size = 1
    end
    object QTelefoniMailcod_telefoni: TIntegerField
      FieldName = 'cod_telefoni'
    end
    object QTelefoniMailIDGENSLPAGE: TIntegerField
      FieldName = 'ID_GEN_SLP_AGE'
    end
  end
  object QDocuments: TDBISAMQuery
    BeforePost = QDocumentsBeforePost
    OnNewRecord = QDocumentsNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from documenti'
      'where rif_cod_cli = :cod_cli')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    Left = 197
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
  end
  object fdmtblTipo: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 352
    Top = 200
    object fdmtblTipoTipoTelMail: TIntegerField
      FieldName = 'TipoTelMail'
    end
    object fdmtblTipoTelMailDescr: TStringField
      FieldName = 'TelMailDescr'
      Size = 30
    end
  end
  object QTipoDocumento: TDBISAMQuery
    OnNewRecord = QDocumentsNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select cod_tipo_doc, descrizione'
      'from tipo_doc'
      '')
    Params = <>
    Left = 197
    Top = 344
  end
  object QContattoCliente: TDBISAMQuery
    OnNewRecord = QContattoClienteNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select contatti.*'
      'from contatti'
      'where cod_cli = :cod_cli')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    Left = 293
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    object QContattoClienteDATA: TDateField
      FieldName = 'DATA'
    end
    object QContattoClienteTIPO: TIntegerField
      FieldName = 'TIPO'
    end
    object QContattoClienteDescrTipoCont: TStringField
      FieldKind = fkLookup
      FieldName = 'DescrTipoCont'
      LookupDataSet = QTipoContatto
      LookupKeyFields = 'cod_tipo_cont'
      LookupResultField = 'descriz'
      KeyFields = 'TIPO'
      Size = 25
      Lookup = True
    end
    object QContattoClienteMOTIVO: TStringField
      FieldName = 'MOTIVO'
      Size = 25
    end
    object QContattoClienteFATTO_DA: TStringField
      FieldName = 'FATTO_DA'
      Size = 3
    end
    object QContattoClientetesto: TMemoField
      FieldName = 'testo'
      BlobType = ftMemo
    end
    object QContattoClientecategoria: TStringField
      FieldName = 'categoria'
      Size = 1
    end
    object QContattoClientemodifica: TDateField
      FieldName = 'modifica'
    end
    object QContattoClientereferente: TStringField
      FieldName = 'referente'
      Size = 25
    end
    object QContattoClienterif_operatore: TIntegerField
      FieldName = 'rif_operatore'
    end
    object QContattoClienteID_GEN_SLP_AGE: TIntegerField
      FieldName = 'ID_GEN_SLP_AGE'
    end
    object QContattoClienteCOD_CLI: TIntegerField
      FieldName = 'COD_CLI'
    end
  end
  object QTipoContatto: TDBISAMQuery
    OnNewRecord = QDocumentsNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select cod_tipo_cont, descriz'
      'from tipo_cont')
    Params = <>
    Left = 381
    Top = 288
  end
  object QMailPref: TDBISAMQuery
    BeforePost = QTelefoniMailBeforePost
    OnNewRecord = QTelefoniMailNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select * from telefoni'
      'where cod_cli = :cod_cli'
      'and categoria = '#39'C'#39
      'and titolo = :titolo'
      'order by valore_default desc'
      'top 1')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end
      item
        DataType = ftUnknown
        Name = 'titolo'
      end>
    Left = 277
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end
      item
        DataType = ftUnknown
        Name = 'titolo'
      end>
  end
  object QUpdTelefoniMail: TDBISAMQuery
    BeforePost = QTelefoniMailBeforePost
    OnNewRecord = QTelefoniMailNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update telefoni'
      'set valore_default = '#39'N'#39
      'where categoria = '#39'C'#39
      'and cod_cli = :cod_cli'
      'and valore_default = '#39'S'#39
      'and titolo = :Tipo'
      'and cod_telefoni <> :cod_telefoni')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end
      item
        DataType = ftUnknown
        Name = 'Tipo'
      end
      item
        DataType = ftUnknown
        Name = 'cod_telefoni'
      end>
    Left = 197
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end
      item
        DataType = ftUnknown
        Name = 'Tipo'
      end
      item
        DataType = ftUnknown
        Name = 'cod_telefoni'
      end>
  end
  object QTelefoni: TDBISAMQuery
    BeforePost = QTelefoniMailBeforePost
    AfterPost = QTelefoniMailAfterPost
    OnNewRecord = QTelefoniMailNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from telefoni'
      'where categoria = '#39'C'#39
      'and titolo = '#39'0'#39
      'and cod_cli = :cod_cli'
      'order by valore_default desc')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    Left = 13
    Top = 288
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
  end
  object QMail: TDBISAMQuery
    BeforePost = QTelefoniMailBeforePost
    AfterPost = QTelefoniMailAfterPost
    OnNewRecord = QTelefoniMailNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from telefoni'
      'where categoria = '#39'C'#39
      'and titolo = '#39'2'#39
      'and cod_cli = :cod_cli'
      'order by valore_default desc')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    Left = 13
    Top = 336
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
  end
  object dsTelefoni: TDataSource
    DataSet = QTelefoni
    Left = 80
    Top = 296
  end
  object dsMail: TDataSource
    DataSet = QMail
    Left = 80
    Top = 344
  end
  object Qins_cli_new: TDBISAMQuery
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      '-- START TRANSACTION;'
      'INSERT INTO clienti (stato, DT_LAST_MOD, ID_GEN_SLP_AGE) '
      'VALUES (0 , :data , :ID_AGE);'
      '-- COMMIT FLUSH;'
      
        'select distinct lastautoinc('#39'clienti'#39') as LAST_CODCLIENTE from c' +
        'lienti')
    Params = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'ID_AGE'
      end>
    Left = 408
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'ID_AGE'
      end>
  end
  object QcontrattiCliente: TDBISAMQuery
    OnNewRecord = QContattoClienteNewRecord
    DatabaseName = 'DBagenti4web'
    SessionName = 'sessionAgenzia_2'
    EngineVersion = '4.37 Build 3'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from polizze'
      'where cod_cli = :cod_cli'
      'and stato='#39'V'#39)
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    Left = 389
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_cli'
      end>
    object DateField1: TDateField
      FieldName = 'DATA'
    end
    object IntegerField1: TIntegerField
      FieldName = 'TIPO'
    end
    object StringField1: TStringField
      FieldKind = fkLookup
      FieldName = 'DescrTipoCont'
      LookupDataSet = QTipoContatto
      LookupKeyFields = 'cod_tipo_cont'
      LookupResultField = 'descriz'
      KeyFields = 'TIPO'
      Size = 25
      Lookup = True
    end
    object StringField2: TStringField
      FieldName = 'MOTIVO'
      Size = 25
    end
    object StringField3: TStringField
      FieldName = 'FATTO_DA'
      Size = 3
    end
    object MemoField1: TMemoField
      FieldName = 'testo'
      BlobType = ftMemo
    end
    object StringField4: TStringField
      FieldName = 'categoria'
      Size = 1
    end
    object DateField2: TDateField
      FieldName = 'modifica'
    end
    object StringField5: TStringField
      FieldName = 'referente'
      Size = 25
    end
    object IntegerField2: TIntegerField
      FieldName = 'rif_operatore'
    end
    object IntegerField3: TIntegerField
      FieldName = 'ID_GEN_SLP_AGE'
    end
    object IntegerField4: TIntegerField
      FieldName = 'COD_CLI'
    end
  end
end
