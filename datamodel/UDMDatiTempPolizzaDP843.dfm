inherited DMDatiTempPolizzaDP843: TDMDatiTempPolizzaDP843
  OldCreateOrder = True
  Height = 316
  Width = 387
  inherited fdmtblPolizza: TFDMemTable
    inherited fdmtblPolizzaForma: TStringField
      OnChange = fdmtblPolizzaFormaChange
    end
    inherited fdmtblPolizzaSiglaMassimale: TStringField
      OnChange = fdmtblPolizzaSiglaMassimaleChange
    end
    object fdmtblPolizzaTipoAssicurato: TStringField [100]
      FieldName = 'TipoAssicurato'
      OnChange = fdmtblPolizzaTipoAssicuratoChange
      Size = 1
    end
    object fdmtblPolizzaSiglaRimborsoSpesePatPunti: TStringField [101]
      FieldName = 'SiglaRimborsoSpesePatPunti'
      OnChange = fdmtblPolizzaSiglaRimborsoSpesePatPuntiChange
    end
    object fdmtblPolizzaAppControversie: TBooleanField [102]
      FieldName = 'AppControversie'
      OnChange = fdmtblPolizzaAppControversieChange
    end
    object fdmtblPolizzaProblematicheRCA: TBooleanField [103]
      FieldName = 'ProblematicheRCA'
    end
    object fdmtblPolizzaPerizieParte: TBooleanField [104]
      FieldName = 'PerizieParte'
      OnChange = fdmtblPolizzaPerizieParteChange
    end
    object fdmtblPolizzaMancatoIntRCA: TBooleanField [105]
      FieldName = 'MancatoIntRCA'
    end
    object fdmtblPolizzaProtezioneFamiglia: TBooleanField [106]
      FieldName = 'ProtezioneFamiglia'
      OnChange = fdmtblPolizzaProtezioneFamigliaChange
    end
    object fdmtblPolizzaNumeroAssicurati: TIntegerField [107]
      FieldName = 'NumeroAssicurati'
      OnChange = fdmtblPolizzaNumeroAssicuratiChange
    end
    object fdmtblPolizzaEstPatente: TBooleanField [108]
      FieldName = 'EstPatente'
    end
    object fdmtblPolizzaIndicizzata: TBooleanField [109]
      FieldName = 'Indicizzata'
    end
    object fdmtblPolizzaNome1: TStringField [110]
      FieldName = 'Nome1'
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaNome2: TStringField [111]
      FieldName = 'Nome2'
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaNome3: TStringField [112]
      FieldName = 'Nome3'
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaNome4: TStringField [113]
      FieldName = 'Nome4'
      OnChange = fdmtblPolizzaNome1Change
      Size = 40
    end
    object fdmtblPolizzaPatente1: TStringField [114]
      FieldName = 'Patente1'
      OnChange = fdmtblPolizzaPatente1Change
      Size = 13
    end
    object fdmtblPolizzaPatente2: TStringField [115]
      FieldName = 'Patente2'
      Size = 13
    end
    object fdmtblPolizzaPatente3: TStringField [116]
      FieldName = 'Patente3'
      Size = 13
    end
    object fdmtblPolizzaPatente4: TStringField [117]
      FieldName = 'Patente4'
      Size = 13
    end
    object fdmtblPolizzaCatPatente1: TStringField [118]
      FieldName = 'CatPatente1'
      Size = 2
    end
    object fdmtblPolizzaCatPatente2: TStringField [119]
      FieldName = 'CatPatente2'
      Size = 2
    end
    object fdmtblPolizzaCatPatente3: TStringField [120]
      FieldName = 'CatPatente3'
      Size = 2
    end
    object fdmtblPolizzaCatPatente4: TStringField [121]
      FieldName = 'CatPatente4'
      Size = 2
    end
    object fdmtblPolizzaRimborsoSpesePatPunti: TBooleanField [126]
      FieldName = 'RimborsoSpesePatPunti'
    end
    object fdmtblPolizzaRecuperoDanni: TBooleanField [127]
      FieldName = 'RecuperoDanni'
      OnChange = fdmtblPolizzaRecuperoDanniChange
    end
    object fdmtblPolizzaLegaleDomiciliatario: TBooleanField [128]
      FieldName = 'LegaleDomiciliatario'
      OnChange = fdmtblPolizzaLegaleDomiciliatarioChange
    end
    object fdmtblPolizzaRecuperoDanniFam: TBooleanField [129]
      FieldName = 'RecuperoDanniFam'
      OnChange = fdmtblPolizzaRecuperoDanniFamChange
    end
    object fdmtblPolizzaDifPenVitaPrivata: TBooleanField [130]
      FieldName = 'DifPenVPrivata'
      OnChange = fdmtblPolizzaDifPenVitaPrivataChange
    end
    object fdmtblPolizzaPatenteHaCAP: TBooleanField [131]
      FieldName = 'PatenteHaCAP'
    end
    object fdmtblPolizzaPatenteHaCAPCQC: TBooleanField [132]
      FieldName = 'PatenteHaCAPCQC'
    end
    object fdmtblPolizzaPatenteHaCAPCQCADR: TBooleanField [133]
      FieldName = 'PatenteHaCAPCQCADR'
    end
    inherited fdmtblPolizzaNPercAccessori: TIntegerField
      OnChange = fdmtblPolizzaNPercAccessoriChange
    end
  end
  inherited fdmtblAssicurato: TFDMemTable
    inherited fdmtblAssicuratoCodAssicurato: TFDAutoIncField
      AutoIncrementSeed = 1
    end
    inherited fdmtblAssicuratoCategoriaPatente: TStringField
      OnChange = fdmtblAssicuratoCategoriaPatenteChange
    end
  end
  inherited frxPolizzaGen: TfrxReport
    Left = 160
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxAllegatiPOL: TfrxReport
    Datasets = <>
    Variables = <>
    Style = <>
  end
  inherited frxReport1: TfrxReport
    Top = 120
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxRichObject1: TfrxRichObject
    Left = 252
    Top = 192
  end
end
