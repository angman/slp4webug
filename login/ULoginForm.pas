unit ULoginForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniGUIBaseClasses, uniLabel, uniBitBtn,
  uniColorButton, uniButton, uniEdit, uniSpeedButton, uniMemo, uniHTMLMemo, uniPanel, uniHTMLFrame,
  uniCheckBox, uniMultiItem, uniComboBox, Vcl.Imaging.jpeg, uniImage, JvGIF;

type
  TUniLoginForm1 = class(TUniLoginForm)
    unlblSlp4Web: TUniLabel;
    unhtmlfrmCredenziali: TUniHTMLFrame;
    unhtmlfrmSLP: TUniHTMLFrame;
    UniFieldContainer1: TUniFieldContainer;
    unedtNomeUtente: TUniEdit;
    unedtPassword: TUniEdit;
    UniCbxRicordami: TUniCheckBox;
    UniFieldContainer2: TUniFieldContainer;
    cbbUtenti: TUniComboBox;
    unedtOpPassword: TUniEdit;
    btnConferma: TUniButton;
    btnAccedi: TUniButton;
    btnCancel: TUniButton;
    UniImage1: TUniImage;
    procedure UniLoginFormCreate(Sender: TObject);
    procedure btnAccediClick(Sender: TObject);
    procedure btnConfermaClick(Sender: TObject);
    procedure unedtOpPasswordKeyPress(Sender: TObject; var Key: Char);
    procedure unedtPasswordKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

function UniLoginForm1: TUniLoginForm1;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication;

function UniLoginForm1: TUniLoginForm1;
begin
  Result := TUniLoginForm1(UniMainModule.GetFormInstance(TUniLoginForm1));
end;

procedure TUniLoginForm1.btnAccediClick(Sender: TObject);

  procedure MemorizzaCredenziali;
  var
    UniApp: TUniGuiApplication;
    CookiesScad: TDate;

  begin
    if UniCbxRicordami.Checked then
    begin
      UniApp      := TUniGuiApplication(UniApplication);
      CookiesScad := Date + 30;
      // mCookie := THTTPCookie.Create('nomeUtente', EnomeUtente.Text, '/', Date + 30);
      // WebApplication.Response.Cookies.Add(mCookie);
      UniApp.Cookies.SetCookie('nomeUtente', unedtNomeUtente.Text, CookiesScad);
      // mCookie := THTTPCookie.Create('password', Epassword.Text, '/', Date + 30);
      // WebApplication.Response.Cookies.Add(mCookie);
      UniApp.Cookies.SetCookie('password', unedtPassword.Text, CookiesScad);

      // mCookie := THTTPCookie.Create('rememberMe', 'True', '/', Date + 30);
      // WebApplication.Response.Cookies.Add(mCookie);
      UniApp.Cookies.SetCookie('rememberMe', UniCbxRicordami.Checked.ToString, CookiesScad);
    end;

  end;

begin
  if UniMainModule.LoginAgenzia(unedtNomeUtente.Text, unedtPassword.Text, cbbUtenti.Items) then
  begin
    MemorizzaCredenziali;

    unedtNomeUtente.Enabled := False;
    unedtPassword.Enabled   := False;
    btnAccedi.Enabled       := False;
    btnAccedi.Default       := False;

    cbbUtenti.Enabled       := true;
    unedtOpPassword.Enabled := true;
    btnConferma.Enabled     := true;
    ActiveControl           := cbbUtenti;
    btnConferma.Default     := true;
  end else begin
    ShowMessage(UniMainModule.MSGerrore);
  end;

end;

procedure TUniLoginForm1.btnConfermaClick(Sender: TObject);
begin
  if UniMainModule.LoginUtente(cbbUtenti.Text, unedtOpPassword.Text) then
  begin
    ModalResult := mrOk;
  end else begin
    ShowMessage(UniMainModule.MSGerrore);

  end;
end;

procedure TUniLoginForm1.unedtOpPasswordKeyPress(Sender: TObject;
  var Key: Char);
begin
   if Key=#13 then btnConfermaClick(Sender);

end;

procedure TUniLoginForm1.unedtPasswordKeyPress(Sender: TObject; var Key: Char);
begin
   if Key=#13 then btnAccediClick(Sender);
end;

procedure TUniLoginForm1.UniLoginFormCreate(Sender: TObject);
var
  UniApp: TUniGuiApplication;

begin
{$IFDEF  SVILUPPO}
  unedtNomeUtente.Text := 'COSSATO-824';
  unedtPassword.Text   := '81133400';

{$ELSE}

  UniApp                  := TUniGuiApplication(UniApplication);
  unedtNomeUtente.Text    := UniApp.Cookies.GetCookie('nomeUtente');
  unedtPassword.Text      := UniApp.Cookies.GetCookie('password');
  UniCbxRicordami.Checked := UniApp.Cookies.GetCookie('rememberMe') = true.ToString;

{$ENDIF}
{
  // tolto il 10/12/2020 per i problemi tra cookies e hyperserver
  if UniApplication.Parameters.Values['user'] > '' then
    unedtNomeUtente.Text := UniApplication.Parameters.Values['user'];
  if UniApplication.Parameters.Values['pass'] > '' then
    unedtPassword.Text := UniApplication.Parameters.Values['pass'];
}
end;

initialization

RegisterAppFormClass(TUniLoginForm1);

end.
