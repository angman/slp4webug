{$DEFINE UNIGUI_VCL} // Comment out this line to turn this project into an ISAPI module

{$IFNDEF UNIGUI_VCL}
library
{$else}
program
{$ENDIF}
  SLP4WebUG;



// program SLP4WebUG;

uses
  {$IFNDEF UNIGUI_VCL}
  uniGUIISAPI,
  {$ENDIF }
  Forms,
  ServerModule in 'ServerModule.pas' {UniServerModule: TUniGUIServerModule},
  Main in 'Main.pas' {MainForm: TUniForm},
  ULoginForm in 'login\ULoginForm.pas' {UniLoginForm1: TUniLoginForm},
  UDMMaster in 'datamodel\UDMMaster.pas' {DMMaster: TDataModule},
  UQueryElencoIntf in 'Interfacce\UQueryElencoIntf.pas',
  UQueryEditIntf in 'Interfacce\UQueryEditIntf.pas',
  libreria in 'libs\libreria.pas',
  UDMdatiAgeTitoli in 'datamodel\UDMdatiAgeTitoli.pas' {DMdatiAgeTitoli: TDataModule},
  UDMdatiAgeClienti in 'datamodel\UDMdatiAgeClienti.pas' {DMdatiAgeClienti: TDataModule},
  UDMDatiAgeUtenti in 'datamodel\UDMDatiAgeUtenti.pas' {DMDatiAgeUtenti: TDataModule},
  UDMAgeCollaboratori in 'datamodel\UDMAgeCollaboratori.pas' {DMAgeCollaboratori: TDataModule},
  UDMDatiReport in 'datamodel\UDMDatiReport.pas' {DMDatiReport: TDataModule},
  UTipoPol in 'oggetti\UTipoPol.pas',
  UToPremio in 'oggetti\UToPremio.pas',
  date360 in 'libs\date360.pas',
  UMasterElenco in 'master\UMasterElenco.pas' {FMasterElenco: TUniForm},
  UMasterEdit in 'master\UMasterEdit.pas' {FMasterEdit: TUniForm},
  UClienti in 'Archivi\UClienti.pas' {FClienti: TUniForm},
  UClienteEdit in 'Archivi\UClienteEdit.pas' {FClienteEdit: TUniForm},
  UCollaboratori in 'Archivi\UCollaboratori.pas' {FCollaboratori: TUniForm},
  UdmdatiAge in 'datamodel\UdmdatiAge.pas' {DMdatiAge: TDataModule},
  UStampaBasePolizza in 'Archivi\UStampaBasePolizza.pas' {FStampaBasePolizza: TUniForm},
  UDMDatiTempPolizza in 'datamodel\UDMDatiTempPolizza.pas' {DMDatiTempPolizza: TDataModule},
  USceltaTipoPolizza in 'Archivi\USceltaTipoPolizza.pas' {FSceltaTipoPolizza: TUniForm},
  UDMDatiTempPolizzaDPTR in 'datamodel\UDMDatiTempPolizzaDPTR.pas' {DMDatiTempPolizzaDPTR: TDataModule},
  UDMDatiPolizzaDPTR in 'datamodel\UDMDatiPolizzaDPTR.pas' {DMDatiPolizzaDPTR: TDataModule},
  UStampaPolizzaDPTR in 'Archivi\UStampaPolizzaDPTR.pas' {FStampaPolizzaDPTR: TUniForm},
  UCollaboratoreEdit in 'Archivi\UCollaboratoreEdit.pas' {FCollaboratoreEdit: TUniForm},
  UAgenziaEdit in 'Archivi\UAgenziaEdit.pas' {FAgenziaEdit: TUniForm},
  UUtenti in 'Archivi\UUtenti.pas' {FUtenti: TUniForm},
  UUtenteEdit in 'Archivi\UUtenteEdit.pas' {FUtenteEdit: TUniForm},
  UPrinthouseSLP in 'Archivi\UPrinthouseSLP.pas' {FPrintHouseSLP: TUniForm},
  UContratti in 'Archivi\UContratti.pas' {FContratti: TUniForm},
  UContrattoEdit in 'Archivi\UContrattoEdit.pas' {FContrattoEdit: TUniForm},
  oggetti in 'oggetti\oggetti.pas',
  UStampePDFViewer in 'Archivi\UStampePDFViewer.pas' {FPDFViewer: TUniForm},
  UFogliCassa in 'Archivi\UFogliCassa.pas' {FFogliCassa: TUniForm},
  UdmdatiAgeFogliCassa in 'datamodel\UdmdatiAgeFogliCassa.pas' {DMDatiAgeFogliCassa: TDataModule},
  UTitoli in 'Archivi\UTitoli.pas' {FTitoli: TUniForm},
  UPerfezionaPolizze in 'Archivi\UPerfezionaPolizze.pas' {FperfezionaPolizze: TUniForm},
  UDMDatiAgePolizzePerf in 'datamodel\UDMDatiAgePolizzePerf.pas' {DMDatiAgePolizzePerf: TDataModule},
  MainModule in 'MainModule.pas' {UniMainModule: TUniGUIMainModule},
  UTitoloEdit in 'Archivi\UTitoloEdit.pas' {FTitoloEdit: TUniForm},
  UStampeSLP in 'Archivi\UStampeSLP.pas' {FStampeSLP: TUniForm},
  UconfermaDatiIncasso in 'Archivi\UconfermaDatiIncasso.pas' {FconfermaDatiIncasso: TUniForm},
  libSLP in 'libs\libSLP.pas',
  UEmettiFoglioCassa in 'Archivi\UEmettiFoglioCassa.pas' {FEmettiFoglioCassa: TUniForm},
  UMandanti in 'Archivi\UMandanti.pas' {FMandanti: TUniForm},
  UDMDatiMandante in 'datamodel\UDMDatiMandante.pas' {DMMandanti: TDataModule},
  URiprendiBozza in 'Archivi\URiprendiBozza.pas' {FRiprendiBozza: TUniForm},
  UDMBozzePreventivi in 'datamodel\UDMBozzePreventivi.pas' {DMBozzePreventivi: TDataModule},
  UTipoSalvataggioDocumento in 'oggetti\UTipoSalvataggioDocumento.pas',
  UMandanteEdit in 'Archivi\UMandanteEdit.pas' {FMasterEdit1: TUniForm},
  UStampaPolizzaPP in 'Archivi\UStampaPolizzaPP.pas' {FStampaPolizzaPP: TUniForm},
  UDMDatiTempPolizzaPP in 'datamodel\UDMDatiTempPolizzaPP.pas' {DMDatiTempPolizzaPP: TDataModule},
  UStampaPolizzaFM in 'Archivi\UStampaPolizzaFM.pas' {FStampaPolizzaFM: TUniForm},
  UStampaPolizzaPM in 'Archivi\UStampaPolizzaPM.pas' {FStampaPolizzaPM: TUniForm},
  UDMDatiPolizzaPP in 'datamodel\UDMDatiPolizzaPP.pas' {DMDatiPolizzaPP: TDataModule},
  UDMDatiPolizzaFM in 'datamodel\UDMDatiPolizzaFM.pas' {DMDatiPolizzaFM: TDataModule},
  UMDatiTempPolizzaFM in 'datamodel\UMDatiTempPolizzaFM.pas' {DMDatiTempPolizzaFM: TDataModule},
  UStampaPolizzaTP in 'Archivi\UStampaPolizzaTP.pas' {FStampaPolizzaTP: TUniForm},
  UDMDatiTempPolizzaTP in 'datamodel\UDMDatiTempPolizzaTP.pas' {DMDatiTempPolizzaTP: TDataModule},
  UDMDatiPolizzaTP in 'datamodel\UDMDatiPolizzaTP.pas' {DMDatiPolizzaTP: TDataModule},
  UVediTempTables in 'Archivi\UVediTempTables.pas' {FmVediTempTables: TUniForm},
  UStampaPolizzaOM in 'Archivi\UStampaPolizzaOM.pas' {FStampaPolizzaOM: TUniForm},
  UDMDatiPolizzaOM in 'datamodel\UDMDatiPolizzaOM.pas' {DMDatiPolizzaOM: TDataModule},
  UStampaPolizzaDPPP in 'Archivi\UStampaPolizzaDPPP.pas' {FStampaPolizzaDPPP: TUniForm},
  UDMDatiPolizzaDPPP in 'datamodel\UDMDatiPolizzaDPPP.pas' {DMDatiPolizzaDPPP: TDataModule},
  UDMDatiTempPolizzaDPPP in 'datamodel\UDMDatiTempPolizzaDPPP.pas' {DMDatiTempPolizzaDPPP: TDataModule},
  UStampaPolizzaDPGF in 'Archivi\UStampaPolizzaDPGF.pas' {FStampaPolizzaDPGF: TUniForm},
  UDMDatiPolizzaDPGF in 'datamodel\UDMDatiPolizzaDPGF.pas' {DMDatiPolizzaDPGF: TDataModule},
  UDatiTempPolizzaPM in 'datamodel\UDatiTempPolizzaPM.pas' {DMDatiTempPolizzaPM: TDataModule},
  UDatiPolizzaPM in 'datamodel\UDatiPolizzaPM.pas' {DMDatiPolizzaPM: TDataModule},
  UAskDocSaveType in 'Archivi\UAskDocSaveType.pas' {FAskDocSaveType: TUniForm},
  UDMdatiAgePolizze in 'datamodel\UDMdatiAgePolizze.pas' {DMdatiAgePolizze: TDataModule},
  URistampaAppendice in 'Archivi\URistampaAppendice.pas' {FRistampaAppendice: TUniForm},
  UDMDatiTempPolizzaOM in 'datamodel\UDMDatiTempPolizzaOM.pas' {DMDatiTempPolizzaOM: TDataModule},
  UMasterPage in 'master\UMasterPage.pas' {FMasterPage: TUniForm},
  UcausaliAnnullamento in 'Archivi\UcausaliAnnullamento.pas' {FcausaliAnnullamento: TUniForm},
  UstampeStatistiche in 'Archivi\UstampeStatistiche.pas' {FstampeStatistiche: TUniForm},
  UDMDatiTempPolizzaDPGF in 'datamodel\UDMDatiTempPolizzaDPGF.pas' {DMDatiTempPolizzaDPGF: TDataModule},
  Uchiedi_data in 'Archivi\Uchiedi_data.pas' {Fchiedi_data: TUniForm},
  UDMDatiBasePolizza in 'datamodel\UDMDatiBasePolizza.pas' {DMDatiBasePolizza: TDataModule},
  UAvvisoAgenzia in 'Archivi\UAvvisoAgenzia.pas' {FAvviso: TUniForm},
  UDMDatiAgeAvvisi in 'datamodel\UDMDatiAgeAvvisi.pas' {DMDatiAgeAvvisi: TDataModule},
  UElencoAvvisi in 'Archivi\UElencoAvvisi.pas' {FElencoAvvisi: TUniForm},
  UCodiciErroriPolizza in 'oggetti\UCodiciErroriPolizza.pas',
  UStampaQuietanzeSLP in 'Archivi\UStampaQuietanzeSLP.pas' {FStampaQuietanzeSLP: TUniForm},
  UPolizzaExceptions in 'oggetti\UPolizzaExceptions.pas',
  UModuliDisponibili in 'Archivi\UModuliDisponibili.pas' {FModuliDisponibili: TUniForm},
  UShowAppendiceRec in 'oggetti\UShowAppendiceRec.pas',
  UPosizionaQueryElencoIntf in 'Interfacce\UPosizionaQueryElencoIntf.pas',
  UDatiPolSostituitaRec in 'oggetti\UDatiPolSostituitaRec.pas',
  UDatiPolizzaSostituita in 'Archivi\UDatiPolizzaSostituita.pas' {FDatiPolizzaSostituita: TUniForm},
  UDMDatiTempPolizzaCondTP in 'datamodel\UDMDatiTempPolizzaCondTP.pas' {DMDatiTempPolizzaCondTP: TDataModule},
  UDMDatiPolizzaCondTP in 'datamodel\UDMDatiPolizzaCondTP.pas' {DMDatiPolizzaCondTP: TDataModule},
  UStampaPolizzaDP843 in 'Archivi\UStampaPolizzaDP843.pas' {FStampaPolizzaDP843: TUniForm},
  UDMDatiTempPolizzaDP843 in 'datamodel\UDMDatiTempPolizzaDP843.pas' {DMDatiTempPolizzaDP843: TDataModule},
  UDMDatiPolizzaDP843 in 'datamodel\UDMDatiPolizzaDP843.pas' {DMDatiPolizzaDP843: TDataModule},
  UStoricoIncassi in 'Archivi\UStoricoIncassi.pas' {FStoricoIncassi: TUniForm},
  UdmdatiStoricoIncassi in 'datamodel\UdmdatiStoricoIncassi.pas' {DMDatiAgeStoricoIncassi: TDataModule},
  UDocumentoEdit in 'Archivi\UDocumentoEdit.pas' {FEditDocumento: TUniForm},
  UEditContattoCliente in 'Archivi\UEditContattoCliente.pas' {FEditContattoCliente: TUniForm},
  USimulaQuietanziamento in 'Archivi\USimulaQuietanziamento.pas' {FSimulaQuietanziamento: TUniForm},
  UStampaPolizzaCondTP in 'Archivi\UStampaPolizzaCondTP.pas';

{$R *.res}
{$IFNDEF UNIGUI_VCL}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;
{$ENDIF}

begin
{$IFDEF UNIGUI_VCL}
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  TUniServerModule.Create(Application);
  Application.Run;
{$ENDIF}

end.
