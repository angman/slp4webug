unit UPosizionaQueryElencoIntf;

interface

uses System.Classes, Data.DB;

type
  IPosizionaQueryElenco = interface
    ['{A234EC43-D97C-466B-AECF-A380C3B42DDC}']
    function PosizionaQueryElenco(AQueryElenco, AQueryEdit: TDataset): boolean; overload;
  end;

implementation

end.
