/// <summary>
/// Interfaccia che deve essere implementata nel datamodule per le form di elenco
/// </summary>
unit UQueryElencoIntf;

interface

uses System.Classes;

type
  IQueryElenco = Interface
    ['{78593746-0FEB-4FEA-AD78-3BF16C58AF00}']

    procedure EseguiQuery(ordFieldName: string; TipoFiltro: integer = 0; ValToSearch: string = '';
      StatoPagamento: integer = 0; DirezioneOrdinamento: integer = 1; CodCollaboratore: integer = 0;
      codCompagnia: integer = 0); overload;

    procedure EseguiQuery(ordFieldName: string; AParametersList: TStrings; ValToSearch: String;
      DirezioneOrdinamento: integer = 1; TipoFiltro: integer = 0; CodCollaboratore: integer = 0;
      codCompagnia: integer = 0); overload;

    procedure CambiaOrdinamento(ATipoOrdinamento: integer; ATipoFiltro: integer = 0);
    procedure CercaDato(ATipoOrdinamento: integer; ATipoFiltro: integer; AValueToSearch: string);
    procedure TogliFiltro;
  end;

implementation

end.
