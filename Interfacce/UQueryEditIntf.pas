unit UQueryEditIntf;

interface

uses  System.Classes, System.RTTI;

type
  ///	<summary>
  ///	  Interfaccia che deve essere implementata nel datamodule per le form di edit
  ///	</summary>
  IQueryEdit = Interface
  ['{1805D497-6C4D-45DE-9669-476D39A0622F}']
    procedure PosizionaQuery(AValue: TValue);
  end;

implementation

end.
