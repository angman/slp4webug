object UniMainModule: TUniMainModule
  OldCreateOrder = False
  OnCreate = UniGUIMainModuleCreate
  OnDestroy = UniGUIMainModuleDestroy
  MonitoredKeys.Keys = <>
  ConstrainForms = True
  OnSessionTimeout = UniGUIMainModuleSessionTimeout
  OnBrowserClose = UniGUIMainModuleBrowserClose
  Height = 411
  Width = 533
  object QinsAccessi: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert'
      'into Accessi'
      
        '(TSLogin, agenzia, codage, CodUtente, SiglaUtente, NomeUtente, C' +
        'odCollaboratore, SiglaCollaboratore, NomeCollaboratore, IpConnes' +
        'sione, Browser, SistemaOperativo, UtenteInterno, Operazione)'
      'VALUES'
      
        '(:TSLogin, :agenzia, :codage, :CodUtente, :SiglaUtente, :NomeUte' +
        'nte, :CodCollaboratore, :SiglaCollaboratore, :NomeCollaboratore,' +
        ' :IpConnessione, :Browser, :SistemaOperativo, :UtenteInterno, :O' +
        'perazione)')
    Params = <
      item
        DataType = ftDateTime
        Name = 'TSLogin'
      end
      item
        DataType = ftString
        Name = 'agenzia'
      end
      item
        DataType = ftString
        Name = 'codage'
      end
      item
        DataType = ftInteger
        Name = 'CodUtente'
      end
      item
        DataType = ftUnknown
        Name = 'SiglaUtente'
      end
      item
        DataType = ftString
        Name = 'NomeUtente'
      end
      item
        DataType = ftInteger
        Name = 'CodCollaboratore'
      end
      item
        DataType = ftUnknown
        Name = 'SiglaCollaboratore'
      end
      item
        DataType = ftString
        Name = 'NomeCollaboratore'
      end
      item
        DataType = ftString
        Name = 'IpConnessione'
      end
      item
        DataType = ftString
        Name = 'browser'
      end
      item
        DataType = ftString
        Name = 'SistemaOperativo'
      end
      item
        DataType = ftString
        Name = 'utenteInterno'
      end
      item
        DataType = ftString
        Name = 'operazione'
      end>
    Left = 345
    Top = 24
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'TSLogin'
      end
      item
        DataType = ftString
        Name = 'agenzia'
      end
      item
        DataType = ftString
        Name = 'codage'
      end
      item
        DataType = ftInteger
        Name = 'CodUtente'
      end
      item
        DataType = ftUnknown
        Name = 'SiglaUtente'
      end
      item
        DataType = ftString
        Name = 'NomeUtente'
      end
      item
        DataType = ftInteger
        Name = 'CodCollaboratore'
      end
      item
        DataType = ftUnknown
        Name = 'SiglaCollaboratore'
      end
      item
        DataType = ftString
        Name = 'NomeCollaboratore'
      end
      item
        DataType = ftString
        Name = 'IpConnessione'
      end
      item
        DataType = ftString
        Name = 'browser'
      end
      item
        DataType = ftString
        Name = 'SistemaOperativo'
      end
      item
        DataType = ftString
        Name = 'utenteInterno'
      end
      item
        DataType = ftString
        Name = 'operazione'
      end>
  end
  object QelencoAgenzie: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'select *'
      'from allagenzie')
    Params = <>
    Left = 249
    Top = 24
  end
  object DBaccessoBase: TDBISAMDatabase
    EngineVersion = '4.37 Build 3'
    DatabaseName = 'DBaccessoBase'
    Directory = 'c:\SLP\Slp4Web\SLPDATI'
    SessionName = 'SessionLogin_1'
    Left = 155
    Top = 24
  end
  object SessionLogin: TDBISAMSession
    EngineVersion = '4.37 Build 3'
    AutoSessionName = True
    PrivateDir = 'C:\Users\MarcoOK\AppData\Local\Temp\'
    OnPassword = SessionLoginPassword
    RemoteEncryptionPassword = 'elevatesoft'
    RemoteAddress = '127.0.0.1'
    Left = 48
    Top = 26
  end
  object QLog_Operazioni: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert '
      'into log_operazioni'
      
        '(utente, datatime, operazione,  val_preced, rif_operatore, rif_c' +
        'hiave, chiave_sec, idAgenzia, ip, agenzia)'
      'VALUES'
      
        '(:utente, :datatime, :operazione, :val_preced, :rif_operatore, :' +
        'rif_chiave, :chiave_sec, :idAgenzia, :ip, :agenzia)')
    Params = <
      item
        DataType = ftFixedChar
        Name = 'utente'
      end
      item
        DataType = ftDateTime
        Name = 'datatime'
      end
      item
        DataType = ftFixedChar
        Name = 'operazione'
      end
      item
        DataType = ftString
        Name = 'val_preced'
      end
      item
        DataType = ftInteger
        Name = 'rif_operatore'
      end
      item
        DataType = ftInteger
        Name = 'rif_chiave'
      end
      item
        DataType = ftString
        Name = 'chiave_sec'
      end
      item
        DataType = ftString
        Name = 'idAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end>
    Left = 289
    Top = 160
    ParamData = <
      item
        DataType = ftFixedChar
        Name = 'utente'
      end
      item
        DataType = ftDateTime
        Name = 'datatime'
      end
      item
        DataType = ftFixedChar
        Name = 'operazione'
      end
      item
        DataType = ftString
        Name = 'val_preced'
      end
      item
        DataType = ftInteger
        Name = 'rif_operatore'
      end
      item
        DataType = ftInteger
        Name = 'rif_chiave'
      end
      item
        DataType = ftString
        Name = 'chiave_sec'
      end
      item
        DataType = ftString
        Name = 'idAgenzia'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end>
  end
  object DBscambio: TDBISAMDatabase
    EngineVersion = '4.37 Build 3'
    DatabaseName = 'DBscambio'
    Directory = 'C:\slp\dati\St\SCAMBIO'
    SessionName = 'SessionLogin_1'
    Left = 160
    Top = 120
  end
  object Qins_trasfDati: TDBISAMQuery
    DatabaseName = 'DBscambio'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert into trasfDati'
      
        '(agenzia, cod_int, tipo, data, data_invio, testo, dati, versione' +
        ','
      
        ' varie, ip, polizza, tipo_riga, scadenza, rif_operatore, utente_' +
        'sigla)'
      'values'
      
        '(:agenzia, :cod_int, :tipo, :data, :data_invio, :testo, :dati, :' +
        'versione,'
      
        ' :varie, :ip, :polizza, :tipo_riga, :scadenza,:rif_operatore, :u' +
        'tente_sigla)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'scadenza'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'utente_sigla'
      end>
    Left = 136
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'scadenza'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'utente_sigla'
      end>
  end
  object QscambioINVIA: TDBISAMQuery
    DatabaseName = 'DBscambio'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'INSERT INTO trasfDati'
      
        '(agenzia, sub_age, cod_int, tipo, data_invio, testo, dati, versi' +
        'one, direzione_com, varie, rif_operatore, sigla_utente, tipo_rig' +
        'a, polizza, scadenza, ip)'
      
        'VALUES (:agenzia, :sub_age, :cod_int, :tipo, :data_invio, :testo' +
        ', :dati, :versione, :direzione_com, :varie, :rif_operatore, :sig' +
        'la_utente, :tipo_riga, :polizza, :data_doc, :ip)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'sub_age'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'direzione_com'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_utente'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'data_doc'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end>
    Left = 351
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'agenzia'
      end
      item
        DataType = ftUnknown
        Name = 'sub_age'
      end
      item
        DataType = ftUnknown
        Name = 'cod_int'
      end
      item
        DataType = ftUnknown
        Name = 'tipo'
      end
      item
        DataType = ftUnknown
        Name = 'data_invio'
      end
      item
        DataType = ftUnknown
        Name = 'testo'
      end
      item
        DataType = ftUnknown
        Name = 'dati'
      end
      item
        DataType = ftUnknown
        Name = 'versione'
      end
      item
        DataType = ftUnknown
        Name = 'direzione_com'
      end
      item
        DataType = ftUnknown
        Name = 'varie'
      end
      item
        DataType = ftUnknown
        Name = 'rif_operatore'
      end
      item
        DataType = ftUnknown
        Name = 'sigla_utente'
      end
      item
        DataType = ftUnknown
        Name = 'tipo_riga'
      end
      item
        DataType = ftUnknown
        Name = 'polizza'
      end
      item
        DataType = ftUnknown
        Name = 'data_doc'
      end
      item
        DataType = ftUnknown
        Name = 'ip'
      end>
  end
  object Qmod_slp_gen: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      '-- introdotto in data 02/09/2019 versione 1.6.3.9'
      '-- restituisce la riga con i dati relativi alla stampa voluta'
      
        '-- usato per centralizzare il file che contiene i cambi blob del' +
        ' file'
      '-- mod_slp presente nei singoli DB delle agenzie'
      'select *'
      'from mod_slp_gen'
      'where cod_stampa=:cod_stampa')
    Params = <
      item
        DataType = ftUnknown
        Name = 'cod_stampa'
      end>
    ReadOnly = True
    Left = 269
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'cod_stampa'
      end>
  end
  object QAutoincAccessi: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      
        'SELECT DISTINCT LASTAUTOINC('#39'ACCESSI'#39') AS IDSESSIONE FROM ACCESS' +
        'I')
    Params = <>
    Left = 344
    Top = 80
  end
  object QUpdAccessi: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'update Accessi'
      'set TSLOGOUT = Current_Timestamp,'
      '      OPERAZIONE = :OPERAZIONE'
      'where IDACCESSO = :IDACCESSO')
    Params = <
      item
        DataType = ftString
        Name = 'OPERAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'IDACCESSO'
      end>
    Left = 417
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'OPERAZIONE'
      end
      item
        DataType = ftInteger
        Name = 'IDACCESSO'
      end>
  end
  object QInsLoginFalliti: TDBISAMQuery
    DatabaseName = 'DBaccessoBase'
    SessionName = 'SessionLogin_1'
    EngineVersion = '4.37 Build 3'
    SQL.Strings = (
      'insert'
      'into LoginFalliti'
      
        '(TSLogin, Utente, password, IpComputer, Browser, SistemaOperativ' +
        'o, Tipo)'
      'VALUES'
      
        '(current_timestamp, :utente, :password, :IpComputer, :Browser, :' +
        'SistemaOperativo, :Tipo)')
    Params = <
      item
        DataType = ftUnknown
        Name = 'utente'
      end
      item
        DataType = ftUnknown
        Name = 'password'
      end
      item
        DataType = ftUnknown
        Name = 'IpComputer'
      end
      item
        DataType = ftString
        Name = 'browser'
      end
      item
        DataType = ftString
        Name = 'SistemaOperativo'
      end
      item
        DataType = ftUnknown
        Name = 'Tipo'
      end>
    Left = 441
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'utente'
      end
      item
        DataType = ftUnknown
        Name = 'password'
      end
      item
        DataType = ftUnknown
        Name = 'IpComputer'
      end
      item
        DataType = ftString
        Name = 'browser'
      end
      item
        DataType = ftString
        Name = 'SistemaOperativo'
      end
      item
        DataType = ftUnknown
        Name = 'Tipo'
      end>
  end
end
