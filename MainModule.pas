unit MainModule;

interface

uses
  uniGUIMainModule, SysUtils, Classes, dbisamtb, Data.DB, UdmdatiAge, UDMdatiAgePolizze, UDMdatiAgeTitoli,
  UDMdatiAgeClienti, UDMDatiAgeUtenti, UDMMaster, UDMAgeCollaboratori, UDMDatiTempPolizza,
  UDMDatiBasePolizza, UDMDatiReport, UdmdatiAgeFogliCassa, UDMDatiAgePolizzePerf, UDMDatiMandante, UDMBozzePreventivi,
  UDMDatiAgeAvvisi, UdmdatiStoricoIncassi, uniGUIApplication, System.StrUtils, UClienteEdit;

type
  ELoginError = class(Exception);

  TDMTempPolizzaClassRef = class of TDMDatiTempPolizza;
  TDMBasePolizzaClassRef = class of TDMDatiBasePolizza;

  TUniMainModule = class(TUniGUIMainModule)
    QinsAccessi: TDBISAMQuery;
    QelencoAgenzie: TDBISAMQuery;
    DBaccessoBase: TDBISAMDatabase;
    SessionLogin: TDBISAMSession;
    QLog_Operazioni: TDBISAMQuery;
    DBscambio: TDBISAMDatabase;
    Qins_trasfDati: TDBISAMQuery;
    QscambioINVIA: TDBISAMQuery;
    Qmod_slp_gen: TDBISAMQuery;
    QAutoincAccessi: TDBISAMQuery;
    QUpdAccessi: TDBISAMQuery;
    QInsLoginFalliti: TDBISAMQuery;
    procedure UniGUIMainModuleCreate(Sender: TObject);
    procedure UniGUIMainModuleDestroy(Sender: TObject);
    procedure SessionLoginPassword(Sender: TObject; var Continue: Boolean);
    procedure UniGUIMainModuleSessionTimeout(ASession: TObject; var ExtendTimeOut: Integer);
    procedure UniGUIMainModuleBrowserClose(Sender: TObject);
  private
    { Private declarations }
    SessionTimeout: Boolean;

    FDMdatiAge: TDMdatiAge;
    FDMdatiAgePolizze: TDMdatiAgePolizze;
    FDMdatiAgeTitoli: TDMdatiAgeTitoli;
    FDMdatiAgePolizzePerf: TDMdatiAgePolizzePerf;
    FDMdatiAgeClienti: TDMdatiAgeClienti;
    FDMAgeCollaboratori: TDMAgeCollaboratori;
    FDMDatiReport: TDMDatiReport;
    FDMDatiAgeFogliCassa: TDMDatiAgeFogliCassa;
    FDMdatiAgeUtenti: TDMdatiAgeUtenti;
    FDMDatiTempPolizza: TDMDatiTempPolizza;
    FDMDatiBasePolizza: TDMDatiBasePolizza;
    FDMMandanti: TDMMandanti;
    FDMBozzePreventivi: TDMBozzePreventivi;
    FDMdatiAgeAvvisi: TDMdatiAgeAvvisi;
    FDMdatiStoricoIncassi: TDMDatiStoricoIncassi;

    FSiglaPolizza: string;
    FTipoPolizza: Integer;
    FDMTempPolizzaClassName: string;
    DMTempPolizzaClassRef: TDMTempPolizzaClassRef;
    FDMBasePolizzaClassName: string;
    DMBasePolizzaClassRef: TDMBasePolizzaClassRef;
    FutenteCodice: Integer;
    FUtenteNome: string;
    FutenteSigla: string;
    FUtentePassword: string;
    FUtenteSoloEmissioneSLP: string;
    FUtenteSoloFacSimileSLP: string;
    FUtenteStampaQuietanze: string;
    FutenteNoIncasso: string;
    FutenteNoAppendici: string;

    FcodCompagnia: Integer;
    FArchivio: string;
    FposizioneMenu: string;
    FOperazione: string;
    FnumPolizza: string;
    FutenteLivello: smallint;
    FutentePromoter: Integer;
    FcodScadenza: Integer;
    FNomePromoter: string;
    FIdConnessione: Integer;
    FSiglaPromoter: string;
    FNlinkSubPromoter: Integer;
    FSiglaSubPromoter: string;
    FutenteSubPromoter: Integer;
    FNomeSubPromoter: string;
    FContraenteIsSocieta: Boolean;

    function GetDMAgeCollaboratori: TDMAgeCollaboratori;
    function GetDMdatiAge: TDMdatiAge;
    function GetDMdatiAgeClienti: TDMdatiAgeClienti;
    function GetDMdatiAgePolizze: TDMdatiAgePolizze;
    function GetDMdatiAgeTitoli: TDMdatiAgeTitoli;
    function GetDMdatiAgeUtenti: TDMdatiAgeUtenti;
    function GetDMDatiBasePolizza: TDMDatiBasePolizza;
    function GetDMdatiReport: TDMDatiReport;
    function GetDMDatiTempPolizza: TDMDatiTempPolizza;
    function GetIsUserAdmin: Boolean;
    procedure SetDMBasePolizzaClassName(const Value: string);
    procedure SetDMTempPolizzaClassName(const Value: string);

    procedure caricaUtenti(ListaUtenti: TStrings);
    procedure caricaParametriAgenzia;
    procedure configuraServerDB;
    function GetDMDatiAgeFogliCassa: TDMDatiAgeFogliCassa;
    function GetIsUtentePromoter: Boolean;
    function GetDMdatiAgePolizzePerf: TDMdatiAgePolizzePerf;
    function GetDMDatiMandante: TDMMandanti;
    function GetDMBozzePreventivi: TDMBozzePreventivi;
    function GetDMDatiAgeAvvisi: TDMdatiAgeAvvisi;
    function GetDMDatiStoricoIncassi: TDMDatiStoricoIncassi;
    function GetIsSubAgenzia: Boolean;

  public
    { Public declarations }
    dataChiediData: TDateTime;
    cod_cliente_preparazione: Integer;
    MSGerrore: string;

    function LoginAgenzia(UtenteAgenzia, Password: string; ListaUtenti: TStrings): Boolean;
    function LoginUtente(Utente, Password: string): Boolean;

    function aggiornaPosizione(AArchivio, AOperazione, APosizione: string; conraise: boolean = True): boolean;
    procedure aggiungiPosizioneMenu(dove: string);
    procedure togliPosizioneMenu;

    procedure registraAccesso;
    procedure registraAccessoFallito(const AUtente, APassword, ATipo: string);

    procedure traccia(operazione: string; chiave: Integer; archivio: string; chiave_sec: string = '';
      ope_message: string = ''; ris_operazione: string = '');

    function VerificaAutorizzazione(AArchivio: string = ''; AOperazione: string = ''; RaiseException: Boolean = false): Boolean;

    procedure comunicaDIR(operazione, varie, tipo, nomefile, userfilec, mes, tipo_riga, polizza: string;
      data_doc: TDateTime);

    property DMdatiAge: TDMdatiAge read GetDMdatiAge;
    property DMdatiAgePolizze: TDMdatiAgePolizze read GetDMdatiAgePolizze;
    property DMdatiAgePolizzePerf: TDMdatiAgePolizzePerf read GetDMdatiAgePolizzePerf;
    property DMdatiAgeTitoli: TDMdatiAgeTitoli read GetDMdatiAgeTitoli;
    property DMdatiAgeClienti: TDMdatiAgeClienti read GetDMdatiAgeClienti;
    property DMdatiAgeCollaboratori: TDMAgeCollaboratori read GetDMAgeCollaboratori;
    property DMdatiAgeUtenti: TDMdatiAgeUtenti read GetDMdatiAgeUtenti;
    property DMDatiReport: TDMDatiReport read GetDMdatiReport;
    property DMDatiAgeFogliCassa: TDMDatiAgeFogliCassa read GetDMDatiAgeFogliCassa;
    property DMDatiTempPolizza: TDMDatiTempPolizza read GetDMDatiTempPolizza;
    property DMDatiBasePolizza: TDMDatiBasePolizza read GetDMDatiBasePolizza;
    property DMDatiMandante: TDMMandanti read GetDMDatiMandante;
    property DMBozzePreventivi: TDMBozzePreventivi read GetDMBozzePreventivi;
    property DMDatiAgeAvvisi: TDMdatiAgeAvvisi read GetDMDatiAgeAvvisi;
    property DMDatiStoricoIncassi: TDMDatiStoricoIncassi read GetDMDatiStoricoIncassi;

    property IsUserAdmin: Boolean read GetIsUserAdmin;
    property IsUtentePromoter: Boolean read GetIsUtentePromoter;
    property IsSubAgenzia: Boolean read GetIsSubAgenzia;

    property SiglaPolizza: string read FSiglaPolizza write FSiglaPolizza;
    property TipoPolizza: Integer read FTipoPolizza write FTipoPolizza;
    property DMTempPolizzaClassName: string read FDMTempPolizzaClassName write SetDMTempPolizzaClassName;
    property DMBasePolizzaClassName: string read FDMBasePolizzaClassName write SetDMBasePolizzaClassName;

    property archivio: string read FArchivio write FArchivio;
    property operazione: string read FOperazione write FOperazione;
    property UtenteSigla: string read FutenteSigla write FutenteSigla;
    property UtenteCodice: Integer read FutenteCodice write FutenteCodice;
    property UtenteNome: string read FUtenteNome write FUtenteNome;
    property UtentePassword: string read FUtentePassword write FUtentePassword;
    property UtenteSoloEmissioneSLP: string read FUtenteSoloEmissioneSLP write FUtenteSoloEmissioneSLP;
    property UtenteSoloFacSimileSLP: string read FUtenteSoloFacSimileSLP write FUtenteSoloFacSimileSLP;
    property UtenteStampaQuietanze: string read FUtenteStampaQuietanze write FUtenteStampaQuietanze;
    property UtenteNoIncasso: string read FutenteNoIncasso write FutenteNoIncasso;
    property UtenteNoAppendici: string read FutenteNoAppendici write FutenteNoAppendici;
    property UtenteLivello: smallint read FutenteLivello write FutenteLivello;
    property UtentePromoter: Integer read FutentePromoter write FutentePromoter;
    property SiglaPromoter: string read FSiglaPromoter write FSiglaPromoter;
    property NomePromoter: string read FNomePromoter write FNomePromoter;

    // parte per l'identificazione dell'eventuale subPromoter
    property UtenteSubPromoter: Integer read FutenteSubPromoter write FutenteSubPromoter;
    property SiglaSubPromoter: string read FSiglaSubPromoter write FSiglaSubPromoter;
    property NomeSubPromoter: string read FNomeSubPromoter write FNomeSubPromoter;
    property LinkSubPromoter: Integer read FNlinkSubPromoter write FNlinkSubPromoter;

    property CodCompagnia: Integer read FcodCompagnia write FcodCompagnia;
    property CodScadenza: Integer read FcodScadenza write FcodScadenza;
    property NumPolizza: string read FnumPolizza write FnumPolizza;
    property Posizionemenu: string read FposizioneMenu write FposizioneMenu;
    property ContraenteIsSocieta: Boolean read FContraenteIsSocieta write FContraenteIsSocieta;

  end;

function UniMainModule: TUniMainModule;

implementation

{$R *.dfm}

uses
  UniGUIVars, ServerModule, System.IOUtils, libreria, libSLP, UCodiciErroriPolizza, UPolizzaExceptions;

function UniMainModule: TUniMainModule;
begin
  Result := TUniMainModule(UniApplication.UniMainModule)
end;

function TUniMainModule.aggiornaPosizione(AArchivio, AOperazione, APosizione: string; conraise: boolean): Boolean;
begin
  archivio   := AArchivio;
  operazione := AOperazione;
  Result:=true;
  if VerificaAutorizzazione(AArchivio, AOperazione, conraise) then
  begin
    togliPosizioneMenu;
    aggiungiPosizioneMenu(APosizione);
  end else Result:=false;

end;

procedure TUniMainModule.aggiungiPosizioneMenu(dove: string);
begin
  Posizionemenu := Posizionemenu + ' \ ' + dove;

end;

procedure TUniMainModule.caricaParametriAgenzia;
var
  SLPDatiAge: TSLPdati_age_rec;
begin
  SLPDatiAge.agenzia  := QelencoAgenzie.FieldByName('agenzia').AsString;
  SLPDatiAge.cod_int  := QelencoAgenzie.FieldByName('cod_age').AsString;
  SLPDatiAge.cod_intZ := strZero(Alltrim(QelencoAgenzie.FieldByName('cod_age').AsString), 5);
  SLPDatiAge.nome     := QelencoAgenzie.FieldByName('nome_agenzia').AsString;
  SLPDatiAge.citta    := QelencoAgenzie.FieldByName('citta').AsString;

  SLPDatiAge.dati_gen.RemotePassword := QelencoAgenzie.FieldByName('password').AsString;
  SLPDatiAge.dati_gen.RemoteUser     := QelencoAgenzie.FieldByName('nome_utente').AsString;

  SLPDatiAge.dati_gen.srvIP    := QelencoAgenzie.FieldByName('srvIP').AsString;
  SLPDatiAge.dati_gen.srvPort1 := QelencoAgenzie.FieldByName('srvPort1').AsInteger;
  SLPDatiAge.dati_gen.srvPort2 := QelencoAgenzie.FieldByName('srvPort2').AsInteger;

  SLPDatiAge.dati_gen.remoteDB         := QelencoAgenzie.FieldByName('nomeDB').AsString;
  SLPDatiAge.dati_gen.utenteDB         := QelencoAgenzie.FieldByName('utenteDB').AsString;
  SLPDatiAge.dati_gen.ServerName       := QelencoAgenzie.FieldByName('serverName').AsString;
  SLPDatiAge.dati_gen.passwordUtenteDB := QelencoAgenzie.FieldByName('passwordUtenteDB').AsString;

  // if UniMainModule.sviluppo then
  // SLPDatiAge.dati_gen.dir_temp := TPath.Combine(PosizioneDati, 'temp')
  // // 'C:\sviluppo\sorgenti\SLP4WEB\SLPDATI\temp\'
  // else
  // SLPDatiAge.dati_gen.dir_temp    := 'C:\slp\Slp4Web\Temp\';
  SLPDatiAge.dati_gen.dir_temp := TPath.Combine(UniServerModule.PosizioneDati, 'temp');
  DMdatiAge.SLPdati_age        := SLPDatiAge;

  log('MainModule Thread_id: ' + TThread.CurrentThread.ThreadID.ToString + ' Sessionname: ' +
    DMdatiAge.SessionAgenzia.SessionName);
end;

procedure TUniMainModule.caricaUtenti(ListaUtenti: TStrings);
begin
  DMdatiAge.EncryptionPassword := UniServerModule.EncriptionPassword;
  ListaUtenti.Clear;
  DMdatiAge.Qutenti.Open;
  while not DMdatiAge.Qutenti.Eof do
  begin
    ListaUtenti.Append(DMdatiAge.Qutenti.FieldByName('nome').AsString);
    DMdatiAge.Qutenti.Next;
  end;
  DMdatiAge.Qutenti.close;

end;

procedure TUniMainModule.comunicaDIR(operazione, varie, tipo, nomefile, userfilec, mes, tipo_riga, polizza: string;
  data_doc: TDateTime);
begin
  QscambioINVIA.close;
  QscambioINVIA.ParamByName('agenzia').AsString      := DMdatiAge.SLPdati_age.agenzia;
  QscambioINVIA.ParamByName('sub_age').AsString      := DMdatiAge.SLPdati_age.sub_age;
  QscambioINVIA.ParamByName('cod_int').AsString      := DMdatiAge.SLPdati_age.cod_intZ;
  QscambioINVIA.ParamByName('tipo').AsString         := operazione;
  QscambioINVIA.ParamByName('data_invio').AsDateTime := Now;
  QscambioINVIA.ParamByName('testo').AsString        := mes;
  // AnsiReplaceText(mes, '%', ' ');
  QscambioINVIA.ParamByName('dati').AsBlob       := '';
  QscambioINVIA.ParamByName('versione').AsString := '';

  QscambioINVIA.ParamByName('sigla_utente').AsString   := UtenteSigla;
  QscambioINVIA.ParamByName('rif_operatore').AsInteger := UtenteCodice;
  // if sdimain.is_direzione3 then DMdatiAge.QscambioINVIA.ParamByName('direzione_com').AsString:='DD'
  // else DMdatiAge.QscambioINVIA.ParamByName('direzione_com').AsString:='AD';
  QscambioINVIA.ParamByName('direzione_com').AsString := 'AW';
  QscambioINVIA.ParamByName('varie').AsString         := varie;
  QscambioINVIA.ParamByName('tipo_riga').AsString     := tipo_riga;
  QscambioINVIA.ParamByName('data_doc').AsDateTime    := data_doc;
  QscambioINVIA.ParamByName('polizza').AsString       := polizza;
  QscambioINVIA.ParamByName('ip').AsString            := TUniGUIApplication(UniApplication).RemoteAddress;
  QscambioINVIA.ExecSQL;

end;

procedure TUniMainModule.configuraServerDB;
begin
  DMdatiAge.SessionAgenzia.active := False;
{$IFNDEF  SVILUPPO}
  // autenticazione su server remoto (2)
  // MB INI autenticazione su server remoto (2)
  DMdatiAge.SessionAgenzia.PrivateDir   := 'C:\slp\Slp4Web\Temp\';
  DMdatiAge.DBagenti4web.Connected      := False;
  DMdatiAge.DBagenti4web.RemoteDatabase := DMdatiAge.SLPdati_age.dati_gen.remoteDB;

  DMdatiAge.SessionAgenzia.RemoteEncryption := true;
  // DMdatiAge.SessionAgenzia.RemoteEncryptionPassword := 'astraGalo37k21Hh51zZ';
  DMdatiAge.SessionAgenzia.RemoteEncryptionPassword := UniServerModule.EncriptionPassword;

  if DMdatiAge.SLPdati_age.dati_gen.srvPort1 = 12007 then
  begin
    Engine.ServerName := 'DBSRVR1';
  end;

  if DMdatiAge.SLPdati_age.dati_gen.srvPort1 > 0 then
  begin
    Engine.ServerMainPort               := DMdatiAge.SLPdati_age.dati_gen.srvPort1;
    DMdatiAge.SessionAgenzia.RemotePort := DMdatiAge.SLPdati_age.dati_gen.srvPort1;
  end;
  if DMdatiAge.SLPdati_age.dati_gen.srvPort2 > 0 then
  begin
    Engine.ServerAdminPort := DMdatiAge.SLPdati_age.dati_gen.srvPort2;
  end;

  DMdatiAge.SessionAgenzia.SessionType    := stRemote;
  DMdatiAge.SessionAgenzia.RemoteAddress  := '127.0.0.1'; // dati_gen.srvIP;
  DMdatiAge.SessionAgenzia.RemoteUser     := DMdatiAge.SLPdati_age.dati_gen.utenteDB;
  DMdatiAge.SessionAgenzia.RemotePassword := DMdatiAge.SLPdati_age.dati_gen.passwordUtenteDB;
  // MB END
{$ELSE}
  DMdatiAge.SessionAgenzia.PrivateDir := TPath.Combine(UniServerModule.PosizioneDati, 'temp');
  DMdatiAge.DBagenti4web.Directory    := TPath.Combine(UniServerModule.PosizioneDati, 'dati');
{$ENDIF}
  DMdatiAge.SessionAgenzia.active := true;
  // UniMainModule.DMdatiAge.SessioneAccesso.AddPassword('27H312ACFF77908DDAC4005589.-+');
  DMdatiAge.SessionAgenzia.AddPassword(UniServerModule.RemotePassword);

end;

function TUniMainModule.GetDMAgeCollaboratori: TDMAgeCollaboratori;
begin
  if not assigned(FDMAgeCollaboratori) then
    FDMAgeCollaboratori := TDMAgeCollaboratori.Create(Self);
  Result                := FDMAgeCollaboratori;

end;

function TUniMainModule.GetDMBozzePreventivi: TDMBozzePreventivi;
begin
  if not assigned(FDMBozzePreventivi) then
    FDMBozzePreventivi := TDMBozzePreventivi.Create(Self);
  Result       := FDMBozzePreventivi;

end;

function TUniMainModule.GetDMdatiAge: TDMdatiAge;
begin
  if not assigned(FDMdatiAge) then
    FDMdatiAge := TDMdatiAge.Create(Self);
  Result       := FDMdatiAge;

end;

function TUniMainModule.GetDMDatiAgeAvvisi: TDMdatiAgeAvvisi;
begin

  if not assigned(FDMdatiAgeAvvisi) then
    FDMdatiAgeAvvisi := TDMdatiAgeAvvisi.Create(Self);
  Result             := FDMdatiAgeAvvisi;

end;

function TUniMainModule.GetDMDatiStoricoIncassi: TDMdatiStoricoIncassi;
begin

  if not assigned(FDMdatiStoricoIncassi) then
    FDMdatiStoricoIncassi := TDMDatiStoricoIncassi.Create(Self);
  Result             := FDMdatiStoricoIncassi;

end;

function TUniMainModule.GetDMdatiAgeClienti: TDMdatiAgeClienti;
begin
  if not assigned(FDMdatiAgeClienti) then
    FDMdatiAgeClienti := TDMdatiAgeClienti.Create(Self);
  Result              := FDMdatiAgeClienti;

end;

function TUniMainModule.GetDMDatiAgeFogliCassa: TDMDatiAgeFogliCassa;
begin

  if not assigned(FDMDatiAgeFogliCassa) then
    FDMDatiAgeFogliCassa := TDMDatiAgeFogliCassa.Create(Self);
  Result                 := FDMDatiAgeFogliCassa;
end;

function TUniMainModule.GetDMdatiAgePolizze: TDMdatiAgePolizze;
begin
  if not assigned(FDMdatiAgePolizze) then
    FDMdatiAgePolizze := TDMdatiAgePolizze.Create(Self);
  Result              := FDMdatiAgePolizze;

end;

function TUniMainModule.GetDMdatiAgePolizzePerf: TDMdatiAgePolizzePerf;
begin
  if not assigned(FDMdatiAgePolizzePerf) then
    FDMdatiAgePolizzePerf := TDMdatiAgePolizzePerf.Create(Self);
  Result                  := FDMdatiAgePolizzePerf;

end;

function TUniMainModule.GetDMdatiAgeTitoli: TDMdatiAgeTitoli;
begin
  if not assigned(FDMdatiAgeTitoli) then
    FDMdatiAgeTitoli := TDMdatiAgeTitoli.Create(Self);
  Result             := FDMdatiAgeTitoli;

end;

function TUniMainModule.GetDMdatiAgeUtenti: TDMdatiAgeUtenti;
begin
  if not assigned(FDMdatiAgeUtenti) then
    FDMdatiAgeUtenti := TDMdatiAgeUtenti.Create(Self);
  Result             := FDMdatiAgeUtenti;

end;

function TUniMainModule.GetDMDatiBasePolizza: TDMDatiBasePolizza;
begin
  if not assigned(FDMDatiBasePolizza) then
    FDMDatiBasePolizza := TDMDatiBasePolizza.Create(Self);
  Result               := FDMDatiBasePolizza;

end;

function TUniMainModule.GetDMDatiMandante: TDMMandanti;
begin
  if not assigned(FDMMandanti) then
    FDMMandanti := TDMMandanti.Create(Self);
  Result          := FDMMandanti;

end;

function TUniMainModule.GetDMdatiReport: TDMDatiReport;
begin
  if not assigned(FDMDatiReport) then
    FDMDatiReport := TDMDatiReport.Create(Self);
  Result          := FDMDatiReport;

end;

function TUniMainModule.GetDMDatiTempPolizza: TDMDatiTempPolizza;
begin
  Result := FDMDatiTempPolizza;

end;

function TUniMainModule.GetIsSubAgenzia: Boolean;
begin
  Result := Trim(UniMainModule.DMdatiAge.SLPdati_age.sub_age) > '';
end;

function TUniMainModule.GetIsUserAdmin: Boolean;
begin
  Result := UtenteLivello = 11;

end;

function TUniMainModule.GetIsUtentePromoter: Boolean;
begin
  Result := ((UtentePromoter > 0) and (UtenteLivello < 11));
end;

procedure TUniMainModule.registraAccesso;
var
  SLPDatiAge: TSLPdati_age_rec;
  ClientInfo: TUniClientInfoRec;
  tablesList: Tstringlist;

begin
  tablesList := Tstringlist.Create;
  try
    tablesList.Append('ACCESSI');
    DBaccessoBase.StartTransaction(tablesList);
    ClientInfo := TUniGUIApplication(UniApplication).ClientInfoRec;

    SLPDatiAge                                             := UniMainModule.DMdatiAge.SLPdati_age;
    QinsAccessi.ParamByName('TSLOGIN').AsDateTime          := Now;
    QinsAccessi.ParamByName('IPCONNESSIONE').AsString      := TUniGUIApplication(UniApplication).RemoteAddress;
    QinsAccessi.ParamByName('CODUTENTE').AsInteger         := UtenteCodice;
    QinsAccessi.ParamByName('SIGLAUTENTE').AsString        := UtenteSigla;
    QinsAccessi.ParamByName('NOMEUTENTE').AsString         := UtenteNome;
    QinsAccessi.ParamByName('CODCOLLABORATORE').AsInteger  := UtentePromoter;
    QinsAccessi.ParamByName('SIGLACOLLABORATORE').AsString := SiglaPromoter;
    QinsAccessi.ParamByName('NOMECOLLABORATORE').AsString  := NomePromoter;
    QinsAccessi.ParamByName('BROWSER').AsString            := ClientInfo.BrowserType;
    QinsAccessi.ParamByName('SISTEMAOPERATIVO').AsString   := ClientInfo.OSType;
    QinsAccessi.ParamByName('UTENTEINTERNO').AsString      := SLPDatiAge.Utente;
    QinsAccessi.ParamByName('OPERAZIONE').AsString         := 'LOGIN';
    QinsAccessi.ParamByName('AGENZIA').AsString            := SLPDatiAge.agenzia;
    QinsAccessi.ParamByName('CODAGE').AsString             := SLPDatiAge.cod_int;
    QinsAccessi.ExecSQL;
    QAutoincAccessi.close;
    QAutoincAccessi.Open;
    FIdConnessione := QAutoincAccessi.FieldByName('IDSESSIONE').AsInteger;
    QAutoincAccessi.close;
    DBaccessoBase.Commit(true);
  finally
    tablesList.Free;
  end;

end;

procedure TUniMainModule.registraAccessoFallito(const AUtente, APassword, ATipo: string);
var
  ClientInfo: TUniClientInfoRec;

begin
  ClientInfo := TUniGUIApplication(UniApplication).ClientInfoRec;

  QInsLoginFalliti.ParamByName('IPCOMPUTER').AsString := TUniGUIApplication(UniApplication).RemoteAddress;
  QInsLoginFalliti.ParamByName('UTENTE').AsString     := AUtente;
  QInsLoginFalliti.ParamByName('PASSWORD').AsString   := APassword;
  QInsLoginFalliti.ParamByName('BROWSER').AsString    := ClientInfo.BrowserType;
  QInsLoginFalliti.ParamByName('SISTEMAOPERATIVO').AsString := ClientInfo.OSType;
  QInsLoginFalliti.ParamByName('TIPO').AsString := ATipo;
  QInsLoginFalliti.ExecSQL;
end;

function TUniMainModule.LoginAgenzia(UtenteAgenzia, Password: string; ListaUtenti: TStrings): Boolean;
begin
  Result := False;

{$IFNDEF  SVILUPPO}
  // caso di accesso a server remoto
  Engine.ServerName            := 'DBSRVR';
  // DBaccessoBase.RemoteDatabase := 'DBcomune';
  DBaccessoBase.RemoteDatabase := 'DBscambio';
  DBscambio.RemoteDatabase     := 'DBscambio';

{$ELSE}
  DBaccessoBase.Directory := UniServerModule.PosizioneDati;
  DBscambio.Directory     := UniServerModule.PosizioneDati;
{$ENDIF}
  DBaccessoBase.Open;
  DBscambio.Open;
  QelencoAgenzie.Open;

  if QelencoAgenzie.Locate('nome_utente', UtenteAgenzia, []) then
  begin
    if QelencoAgenzie.FieldByName('password').AsString = Password then
    begin
      caricaParametriAgenzia;
      configuraServerDB;

      caricaUtenti(ListaUtenti);
      Result := true;
    end
    else
    begin
      registraAccessoFallito(UtenteAgenzia, Password, 'AGENZIA');
      // raise ELoginError.Create(UniServerModule.MSG_PASSWORD_ERRATA);
      MSGerrore:=UniServerModule.MSG_PASSWORD_ERRATA;
    end;
  end
  else
  begin
    registraAccessoFallito(UtenteAgenzia, Password, 'AGENZIA');
    // raise ELoginError.Create(UniServerModule.MSG_UTENTE_SCONOSCIUTO);
    MSGerrore:=UniServerModule.MSG_UTENTE_SCONOSCIUTO;
  end;

end;

function TUniMainModule.LoginUtente(Utente, Password: string): Boolean;
var
  SLPDatiAge: TSLPdati_age_rec;

begin
  // conferma l'accesso
  // verifica nome utente e password dell'utente dell'agenzia
  Result                       := False;
  DMdatiAge.EncryptionPassword := UniServerModule.RemotePassword;
  DMdatiAge.Qutenti.Open;
  if DMdatiAge.Qutenti.Locate('nome', Utente, []) then
  begin
    // verifica la password
    if uppercase(DMdatiAge.Qutenti.FieldByName('mpassword').AsString) = uppercase(Password) then
    begin
      // caricaParametriAgenzia;
      // configuraServerDB;

      SLPDatiAge        := DMdatiAge.SLPdati_age;
      SLPDatiAge.Utente := Utente;
      // SLPDatiAge.utenteCodice := DMdatiAge.Qutenti.FieldByName('PROGRESSIVO').AsInteger;
      DMdatiAge.SLPdati_age := SLPDatiAge;

      // mette in sessione le informazionei dell'utente
      UtenteNome             := DMdatiAge.Qutenti.FieldByName('NOME').AsString;
      UtenteSigla            := DMdatiAge.Qutenti.FieldByName('SIGLA').AsString;
      UtenteCodice           := DMdatiAge.Qutenti.FieldByName('PROGRESSIVO').AsInteger;
      UtenteLivello          := DMdatiAge.Qutenti.FieldByName('LIVELLO').AsInteger;
      UtenteSoloEmissioneSLP := DMdatiAge.Qutenti.FieldByName('SOLO_EMISSIONE_SLP').AsString;
      UtenteSoloFacSimileSLP := DMdatiAge.Qutenti.FieldByName('SOLO_FAC_SIMILE_SLP').AsString;
      UtenteStampaQuietanze  := DMdatiAge.Qutenti.FieldByName('STAMPA_QUIETANZE').AsString;
      UtenteNoIncasso        := DMdatiAge.Qutenti.FieldByName('NO_INCASSO').AsString;
      UtenteNoAppendici      := DMdatiAge.Qutenti.FieldByName('NO_APPENDICI').AsString;

      // controlla se questo utente � linkato ad altro utente
      if (DMdatiAge.Qutenti.FieldByName('LinkCollaboratore').AsInteger > 0) and
         (DMdatiAge.esiste_coll(DMdatiAge.Qutenti.FieldByName('LinkCollaboratore').AsInteger)) then
      begin
        // questo � un collaboratore di secondo livello, sub produttore
        UtenteSubPromoter := DMdatiAge.Qutenti.FieldByName('PROMOTER').AsInteger;
        SiglaSubPromoter  := DMdatiAge.Qutenti.FieldByName('SIGLACOLLABORATORE').AsString;
        NomeSubPromoter   := DMdatiAge.Qutenti.FieldByName('NOMECOLLABORATORE').AsString;
        LinkSubPromoter   := DMdatiAge.Qutenti.FieldByName('LinkCollaboratore').AsInteger;

        // posizionati sul collaboratore di primo livello linkato che � il vero sub agente
        // DMdatiAge.Qutenti1.Open;
        DMdatiAge.Qutenti.First;
        DMdatiAge.Qutenti.Locate('CodCollaboratore', LinkSubPromoter, []);
        UtentePromoter := DMdatiAge.Qutenti.FieldByName('PROMOTER').AsInteger;
        SiglaPromoter  := DMdatiAge.Qutenti.FieldByName('SIGLACOLLABORATORE').AsString;
        NomePromoter   := DMdatiAge.Qutenti.FieldByName('NOMECOLLABORATORE').AsString;
        UtentePassword := DMdatiAge.Qutenti.FieldByName('MPASSWORD').AsString;
      end
      else
      begin
        UtenteSubPromoter := 0;
        SiglaSubPromoter  := '';
        NomeSubPromoter   := '';
        LinkSubPromoter   := 0;

        UtentePromoter := DMdatiAge.Qutenti.FieldByName('PROMOTER').AsInteger;
        SiglaPromoter  := DMdatiAge.Qutenti.FieldByName('SIGLACOLLABORATORE').AsString;
        NomePromoter   := DMdatiAge.Qutenti.FieldByName('NOMECOLLABORATORE').AsString;
        UtentePassword := DMdatiAge.Qutenti.FieldByName('MPASSWORD').AsString;
      end;

      registraAccesso;
      Result := true;
    end
    else
    begin
      registraAccessoFallito(Utente, uppercase(Password), 'UTENTE');
      // raise ELoginError.Create(UniServerModule.MSG_PASSWORD_ERRATA);
      MSGerrore:=UniServerModule.MSG_PASSWORD_ERRATA;
    end;
  end
  else
  begin
    registraAccessoFallito(Utente, uppercase(Password), 'UTENTE');
    // raise ELoginError.Create(UniServerModule.MSG_UTENTE_SCONOSCIUTO);
    MSGerrore:=UniServerModule.MSG_UTENTE_SCONOSCIUTO;
  end;

end;

procedure TUniMainModule.SessionLoginPassword(Sender: TObject; var Continue: Boolean);
begin
  SessionLogin.AddPassword(UniServerModule.EncriptionPassword);
  Continue := true;
end;

procedure TUniMainModule.SetDMBasePolizzaClassName(const Value: string);
begin
  FDMBasePolizzaClassName := Value;
  if FDMBasePolizzaClassName <> '' then
  begin
    DMBasePolizzaClassRef := TDMBasePolizzaClassRef(FindClass(FDMBasePolizzaClassName));
    // if DMBasePolizzaClassRef = nil then
    // raise Exception.Create('Classe non trovata: ' + FDMBasePolizzaClassName);
    if assigned(DMDatiBasePolizza) then
      if assigned(DMBasePolizzaClassRef) then
      begin
        if (DMDatiBasePolizza.ClassType <> DMBasePolizzaClassRef) then
        begin
          DMDatiBasePolizza.Free;
          FDMDatiBasePolizza := DMBasePolizzaClassRef.Create(Self);
        end;
      end
      else
        FDMDatiBasePolizza := DMBasePolizzaClassRef.Create(Self);
  end;
end;

procedure TUniMainModule.SetDMTempPolizzaClassName(const Value: string);
begin
  FDMTempPolizzaClassName := Value;
  if FDMTempPolizzaClassName <> '' then
  begin
    DMTempPolizzaClassRef := TDMTempPolizzaClassRef(FindClass(FDMTempPolizzaClassName));
    // if DMTempPolizzaClassRef = nil then
    // raise Exception.Create('Classe non trovata: ' + FDMTempPolizzaClassName);
    if assigned(DMTempPolizzaClassRef) then
      if assigned(DMDatiTempPolizza) then
      begin
        if (DMDatiTempPolizza.ClassType <> DMTempPolizzaClassRef) then
        begin
          DMDatiTempPolizza.Free;
          FDMDatiTempPolizza := DMTempPolizzaClassRef.Create(Self);
        end;
      end
      else
        FDMDatiTempPolizza := DMTempPolizzaClassRef.Create(Self);
  end;

end;

procedure TUniMainModule.togliPosizioneMenu;
begin
  Posizionemenu := Copy(Posizionemenu, 1, Rpos(' \ ', Posizionemenu));

end;

procedure TUniMainModule.traccia(operazione: string; chiave: Integer;
  archivio, chiave_sec, ope_message, ris_operazione: string);
begin
  if not UniServerModule.IsIpDirezione then
  begin
      if not DMdatiAge.QLog_Ope.active then
        DMdatiAge.QLog_Ope.Open;
      try
        DMdatiAge.QLog_Ope.Append;
        DMdatiAge.QLog_Ope.FieldByName('OPERAZIONE').AsString     := operazione;
        DMdatiAge.QLog_Ope.FieldByName('UTENTE').AsString         := UtenteSigla;
        DMdatiAge.QLog_Ope.FieldByName('DATATIME').AsDateTime     := Now;
        DMdatiAge.QLog_Ope.FieldByName('NOME_ARCHIVIO').AsString  := archivio;
        DMdatiAge.QLog_Ope.FieldByName('RIF_OPERATORE').AsInteger := UtenteCodice;
        DMdatiAge.QLog_Ope.FieldByName('RIF_CHIAVE').AsInteger    := chiave;
        DMdatiAge.QLog_Ope.FieldByName('CHIAVE_SEC').AsString     := chiave_sec;
        DMdatiAge.QLog_Ope.FieldByName('id_gen_slp_age').AsString := DMdatiAge.SLPdati_age.cod_int;
      finally
        DMdatiAge.QLog_Ope.Post;
      end;
  end;

  try
     try
       QLog_Operazioni.close;
       QLog_Operazioni.ParamByName('UTENTE').AsString         := UtenteSigla;
       QLog_Operazioni.ParamByName('OPERAZIONE').AsString     := operazione;
       QLog_Operazioni.ParamByName('DATATIME').AsDateTime     := Now;
       QLog_Operazioni.ParamByName('val_preced').AsString     := 'W';
       QLog_Operazioni.ParamByName('RIF_OPERATORE').AsInteger := UtenteCodice;
       QLog_Operazioni.ParamByName('RIF_CHIAVE').AsInteger    := chiave;
       QLog_Operazioni.ParamByName('CHIAVE_SEC').AsString     := chiave_sec;
       // QLog_Operazioni.ParamByName('risOperazione').AsString := ris_operazione;
       // QLog_Operazioni.ParamByName('opeMessage').AsString := ope_message;
       QLog_Operazioni.ParamByName('idAgenzia').AsString := DMdatiAge.SLPdati_age.cod_int;
       QLog_Operazioni.ParamByName('Agenzia').AsString := DMdatiAge.SLPdati_age.agenzia;
       QLog_Operazioni.ParamByName('ip').AsString := TUniGUIApplication(UniApplication).RemoteAddress;
     finally
       QLog_Operazioni.ExecSQL;
     end;
  except

  end;
end;

procedure TUniMainModule.UniGUIMainModuleBrowserClose(Sender: TObject);
begin
  if assigned(DMdatiAge) then
    DMdatiAge.Free;
end;

procedure TUniMainModule.UniGUIMainModuleCreate(Sender: TObject);
begin
  SessionTimeout          := false;
  SessionLogin.active     := False;
  SessionLogin.PrivateDir := TPath.Combine(UniServerModule.PosizioneDati, 'temp');
  TFclienteEdit.resetInstanceNumber;

{$IFDEF  SVILUPPO}
  // 'C:\Sviluppo\Sorgenti\Slp4Web_2019\SLPDATI\temp\';
  DBaccessoBase.Directory := UniServerModule.PosizioneDati;
  DBscambio.Directory     := UniServerModule.PosizioneDati;
{$ELSE}
  // MB 20042019
  SessionLogin.RemoteEncryption         := true;
  SessionLogin.RemoteEncryptionPassword := UniServerModule.EncriptionPassword;
  SessionLogin.RemotePort               := UniServerModule.RemotePort;
  SessionLogin.SessionType              := stRemote;
  SessionLogin.RemoteAddress            := UniServerModule.RemoteAddress;
  SessionLogin.RemoteUser               := UniServerModule.RemoteUser;
  SessionLogin.RemotePassword           := UniServerModule.RemotePassword;
{$ENDIF}
  SessionLogin.active := true;
  SessionLogin.AddPassword(UniServerModule.EncriptionPassword);

end;

procedure TUniMainModule.UniGUIMainModuleDestroy(Sender: TObject);
begin
  QUpdAccessi.ParamByName('IDACCESSO').AsInteger := FIdConnessione;
  QUpdAccessi.ParamByName('OPERAZIONE').AsString := IfThen(SessionTimeout, 'TIMEOUT', 'LOGOUT');
  QUpdAccessi.ExecSQL;
  SessionLogin.close;
  DBaccessoBase.close;
end;

procedure TUniMainModule.UniGUIMainModuleSessionTimeout(ASession: TObject; var ExtendTimeOut: Integer);
begin
  SessionTimeout := true;
  TFclienteEdit.resetInstanceNumber;

end;

function TUniMainModule.VerificaAutorizzazione(AArchivio, AOperazione: string; RaiseException: Boolean): Boolean;
begin
  if AArchivio = '' then
    AArchivio := archivio;
  if AOperazione = '' then
    AOperazione := operazione;
  Result        := DMdatiAge.VerificaAutorizzazione(AArchivio, AOperazione);
  if not result and RaiseException then
     raise Exception.Create(UniServerModule.OPERAZIONE_NON_AUTORIZZATA);
end;

initialization

RegisterMainModuleClass(TUniMainModule);

end.
