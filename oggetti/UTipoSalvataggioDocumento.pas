unit UTipoSalvataggioDocumento;

interface

type
  TTipoSalvataggio = (tsNone, tsBozza, tsPreventivo, tsPolizza, tsNuovaBozza, tsNuovoPreventivo, tsPreventivoDir, tsPolizzaDir);

  TTipoOggettoAssicurato = (toPatente, toVeicolo, toPersona, toContratto, toAltro);

function getStatus(TipoSalvataggio: TTipoSalvataggio): string;

implementation

function getStatus(TipoSalvataggio: TTipoSalvataggio): string;
begin
  case TipoSalvataggio of
    tsBozza, tsNuovaBozza:
      Result := 'B';
    tsPreventivo, tsNuovoPreventivo:
      Result := 'F';
  else
    Result := '';

  end;
end;

end.
