unit UToPremio;

interface

uses
  System.Classes;

type
  ToPremio = class
  private
    t_AccessoriRata, t_Arrotondamento, t_InteressiFraz, t_LordoRata,

      t_NettoRata, t_NettoTotale, t_NettoTotaleRicalcolato, t_Premio_rimborsato, t_TasseRata, t_paccessori,
      t_Ptasse: currency;
    t_Durata, t_frazionamento, t_durataRatino: smallInt;
    p_ScontoDurata, t_ScontoDurata: currency;
    t_imponibileRata: currency;

    t_nettoRataB: currency;
    t_accessoriRataB: currency;
    t_interessiFrazB: currency;
    t_imponibileRataB: currency;
    t_tasseRataB: currency;
    t_lordoRataB: currency;

    function GetAccessoriRate: currency;
    function GetInteressiFraz: currency;
    function GetLordoRata: currency;
    function GetNettoRata: currency;
    function GetNettoTotale: currency;
    function GetNettoTotaleRicalcolato: currency;
    function GetTasseRata: currency;

    function GetNettoRataB: currency;
    function GetAccessoriRataB: currency;
    function GetInteressiFrazB: currency;
    function GetImponibileRataB: currency;
    function GetTasseRataB: currency;
    function GetLordoRataB: currency;

    procedure SetAccessoriRata(const Value: currency);
    procedure SetInteressiFraz(const Value: currency);
    procedure SetLordoRata(const Value: currency);
    procedure SetNettoRata(const Value: currency);
    procedure SetNettoTotale(const Value: currency);
    procedure SetNettoTotaleRicalcolato(const Value: currency);
    procedure SetTasseRata(const Value: currency);

    procedure SetNettoRataB(const Value: currency);
    procedure SetAccessoriRataB(const Value: currency);
    procedure SetInteressiFrazB(const Value: currency);
    procedure SetImponibileRataB(const Value: currency);
    procedure SetTasseRataB(const Value: currency);
    procedure SetLordoRataB(const Value: currency);
    function GetImponibileRata: currency;
    procedure SetImponibileRata(const Value: currency);

  public
    calcola_interessi: boolean;
    tipo_arrotondamento: currency;
    durata: smallInt;
    frazionamento: smallInt;
    arrotondamento: currency;
    durataRatino: smallInt;
    ratino_su_prima_rata: boolean;
    premio_rimborsato: currency;
    paccessori: currency;
    ptasse: currency;
    verifica: TStringList;
    perc_sconto_durata: single;
    nettoTotaleBase, nettoTotaleBaseArrotondato, scontoDurataBase: currency;
    numero_assicurati: integer;

    constructor create; overload;
    constructor create(o_nettoTotale, o_ptasse, o_paccessori: currency; o_frazionamento, o_durata: smallInt); overload;
    destructor Destroy; override;

    property nettoTotale: currency read GetNettoTotale write SetNettoTotale;

    // premio netto annuale di polizza arrotondato se polizza annuale, arrotond + scontato se polizza poliennale
    property nettoTotaleRicalcolato: currency read GetNettoTotaleRicalcolato write SetNettoTotaleRicalcolato;
    // valori per le ultime due righe dello stampato di polizza

    property nettoRata: currency read GetNettoRata write SetNettoRata;
    property accessoriRata: currency read GetAccessoriRate write SetAccessoriRata;
    property interessiFraz: currency read GetInteressiFraz write SetInteressiFraz;
    property imponibileRata: currency read GetImponibileRata write SetImponibileRata;
    property tasseRata: currency read GetTasseRata write SetTasseRata;
    property lordoRata: currency read GetLordoRata write SetLordoRata;

    property nettoRataB: currency read GetNettoRataB write SetNettoRataB;
    property accessoriRataB: currency read GetAccessoriRataB write SetAccessoriRataB;
    property interessiFrazB: currency read GetInteressiFrazB write SetInteressiFrazB;
    property imponibileRataB: currency read GetImponibileRataB write SetImponibileRataB;
    property tasseRataB: currency read GetTasseRataB write SetTasseRataB;
    property lordoRataB: currency read GetLordoRataB write SetLordoRataB;

    procedure exec;

  end;

implementation

uses
  System.AnsiStrings, System.Math, System.SysUtils, date360;

function arrotonda(cifra: single): single;
var
  a, dif: single;
begin
  a := frac(cifra);

  // arrotonda ai 50 cent
  if a > 0.55 then
    dif := 1 - a
  else
    if a < 0.05 then
      dif := -a
    else
      dif := 0.50 - a;
  result  := cifra + dif;

  // arrotonda ai 50 cent con discriminante 0.25
  if (a < 0.25) then
    dif := 0;
  if (a >= 0.25) and (a < 0.75) then
    dif := 0.50;
  if (a >= 0.75) then
    dif := 1;

  cifra  := trunc(cifra);
  result := cifra + dif;

  {
    // arrotonda all'euro + vicino ...
    if a>0.50 then dif:=1
    else dif:=0;
    result:=int(cifra)+dif;
  }
end;

{ ToPremio }

constructor ToPremio.create;
begin
  // imposta tutti i valori iniziali
  calcola_interessi   := true;
  tipo_arrotondamento := 0.50;
  premio_rimborsato   := 0;
  numero_assicurati   := 1;

  verifica := TStringList.create;
end;

constructor ToPremio.create(o_nettoTotale, o_ptasse, o_paccessori: currency; o_frazionamento, o_durata: smallInt);
begin
  // imposta tutti i valori iniziali
  calcola_interessi   := true;
  tipo_arrotondamento := 0.50;
  nettoTotale         := o_nettoTotale;
  ptasse              := o_ptasse;
  frazionamento       := o_frazionamento;
  paccessori          := o_paccessori;
  durata              := o_durata;
  verifica            := TStringList.create;
  premio_rimborsato   := 0;
  perc_sconto_durata  := 0;
  numero_assicurati   := 1;
end;

destructor ToPremio.Destroy;
begin
  verifica.free;
end;

procedure ToPremio.exec;
var
  dif: currency;
  function dai_lordo(pnetto: currency): currency;
  begin
    result := pnetto + (pnetto / 100 * ptasse);
  end;

  function dai_netto(plordo: currency): currency;
  begin
    result := plordo / (1 + (ptasse / 100));
  end;

begin
  // calcola il premio netto annuale con l'eventuale sconto
  verifica.Clear;
  verifica.Add('Netto totale iniziale :' + CurrToStr(nettoTotale));
  scontoDurataBase := 0;
  if durata > 1 then
  begin
    if durata = 2 then
      p_ScontoDurata := 5;
    if durata = 3 then
      p_ScontoDurata := 7;
    if durata = 5 then
      p_ScontoDurata   := 10;
    perc_sconto_durata := p_ScontoDurata;
    t_ScontoDurata     := nettoTotale / 100 * p_ScontoDurata;
    scontoDurataBase   := t_ScontoDurata;
    t_NettoTotale      := nettoTotale - t_ScontoDurata;
  end
  else
  begin
    p_ScontoDurata     := 0;
    perc_sconto_durata := 0;
    t_ScontoDurata     := 0;
    t_NettoTotale      := nettoTotale;
  end;

  verifica.Add('Netto totale iniziale scontato:' + CurrToStr(t_NettoTotale));
  // calcola tutti i valori NON arrotondati
  t_NettoRata      := roundTo(t_NettoTotale / frazionamento, -2);
  t_AccessoriRata  := roundTo(t_NettoRata / 100 * paccessori, -2);
  t_InteressiFraz  := roundTo(t_NettoRata / 100 * dai_interessi(frazionamento), -2);
  t_imponibileRata := t_NettoRata + t_AccessoriRata + t_InteressiFraz;
  t_TasseRata      := roundTo(t_imponibileRata / 100 * ptasse, -2);
  t_LordoRata      := t_imponibileRata + t_TasseRata;

  verifica.Add('Netto rata iniziale :' + CurrToStr(t_NettoRata));
  verifica.Add('Accessori rata iniziale :' + CurrToStr(t_AccessoriRata));
  verifica.Add('Inter. fraz. rata iniziale :' + CurrToStr(t_InteressiFraz));
  verifica.Add('Imponibile rata iniziale :' + CurrToStr(t_imponibileRata));
  verifica.Add('Tasse rata iniziale :' + CurrToStr(t_TasseRata));
  verifica.Add('Lordo rata iniziale :' + CurrToStr(t_LordoRata));

  // calcola l'arrotondamento da applicare per avere un premio di rata lordo arrotondato ai .50
  t_LordoRata      := arrotonda(t_LordoRata);
  t_imponibileRata := roundTo(dai_netto(t_LordoRata), -2);
  t_TasseRata      := t_LordoRata - t_imponibileRata;
  t_NettoRata      := t_imponibileRata;

  if t_InteressiFraz > 0 then
    t_NettoRata := roundTo((t_imponibileRata * 100) / (100 + dai_interessi(frazionamento)), -2);
  if t_AccessoriRata > 0 then
    t_NettoRata := roundTo((t_NettoRata * 100) / (100 + paccessori), -2);

  t_AccessoriRata := roundTo(t_NettoRata / 100 * paccessori, -2);
  t_InteressiFraz := roundTo((t_NettoRata + t_AccessoriRata) / 100 * dai_interessi(frazionamento), -2);

  dif := t_imponibileRata - (t_InteressiFraz + t_AccessoriRata + t_NettoRata);
  if dif <> 0 then
    t_NettoRata := t_NettoRata + dif;

  nettoTotaleRicalcolato := t_NettoRata * frazionamento;

  if durata > 1 then
  begin
    nettoTotaleBaseArrotondato := (nettoTotaleRicalcolato * 100) / (100 - p_ScontoDurata);
    arrotondamento             := nettoTotaleBase - nettoTotaleBaseArrotondato;
  end
  else
  begin
    nettoTotaleBaseArrotondato := nettoTotaleRicalcolato;
    arrotondamento             := nettoTotale - nettoTotaleBaseArrotondato;
  end;

  verifica.Add('  ==== DOPO ARROTONDAMENTO ======  ');
  verifica.Add('Netto rata arrotondato :' + CurrToStr(t_NettoRata));
  verifica.Add('Accessori rata arrotondato :' + CurrToStr(t_AccessoriRata));
  verifica.Add('Inter. fraz. rata arrotondato :' + CurrToStr(t_InteressiFraz));
  verifica.Add('Imponibile rata arrotondato :' + CurrToStr(t_imponibileRata));
  verifica.Add('Tasse rata arrotondato :' + CurrToStr(t_TasseRata));
  verifica.Add('Lordo rata arrotondato :' + CurrToStr(t_LordoRata));
  verifica.Add('-- Netto Totale RICALCOLATO = :' + CurrToStr(nettoTotaleRicalcolato));
  verifica.Add('Arrotondamento :' + CurrToStr(arrotondamento));
  verifica.Add('nettoTotale :' + CurrToStr(nettoTotale));
  if durataRatino > 0 then
  begin
    // esiste un ratino
    // calcolare la parte di premio relativa al ratino indicato
    // calcola l'arrotondamento da applicare per avere un premio di rata lordo con ratino arrotondato ai .1

    if premio_rimborsato > 0 then
    begin
      t_lordoRataB := roundTo(t_LordoRata / (360 / frazionamento) * durataRatino, -1);
      if ratino_su_prima_rata then
        t_lordoRataB := t_lordoRataB + t_LordoRata;

      t_imponibileRataB := roundTo(dai_netto(t_lordoRataB), -2);
      t_imponibileRataB := t_imponibileRataB - premio_rimborsato;
      t_tasseRataB      := roundTo(t_imponibileRataB / 100 * ptasse, -2);
      t_lordoRataB      := t_imponibileRataB + t_tasseRataB;

      t_nettoRataB := t_imponibileRataB + premio_rimborsato;
      {
      if t_interessiFrazB > 0 then
        t_nettoRataB := roundTo((t_imponibileRataB * 100) / (100 + dai_interessi(frazionamento)), -2);
      if t_accessoriRataB > 0 then
        t_nettoRataB := roundTo((t_nettoRataB * 100) / (100 + paccessori), -2);

      t_accessoriRataB := roundTo(t_nettoRataB / 100 * paccessori, -2);
      t_interessiFrazB := roundTo((t_nettoRataB + t_accessoriRataB) / 100 * dai_interessi(frazionamento), -2);
      }
      // mb 01112020  per correggere errore quanto sostituzione con rimborso e ratino
      if t_interessiFrazB > 0 then
        t_nettoRataB := roundTo(((t_imponibileRataB  + premio_rimborsato) * 100) / (100 + dai_interessi(frazionamento)), -2);
      if t_accessoriRataB > 0 then
        t_nettoRataB := roundTo((t_nettoRataB * 100) / (100 + paccessori), -2);


      dif := t_imponibileRataB - (t_interessiFrazB + t_accessoriRataB + t_nettoRataB);
      // tolto il 24/10/2014 per far stampare guisto il premio netto in caso di polizza sostituita
      // con restituzione di premio e ratino
      // if dif<>0 then t_NettoRataB:=t_NettoRataB+dif;

    end
    else
    begin
      t_lordoRataB := roundTo(t_LordoRata / (360 / frazionamento) * durataRatino, -1);
      if ratino_su_prima_rata then
        t_lordoRataB    := t_lordoRataB + t_LordoRata;
      t_imponibileRataB := roundTo(dai_netto(t_lordoRataB), -2);
      t_tasseRataB      := t_lordoRataB - t_imponibileRataB;
      t_nettoRataB      := t_imponibileRataB;
      if t_interessiFrazB > 0 then
        t_nettoRataB := roundTo((t_imponibileRataB * 100) / (100 + dai_interessi(frazionamento)), -2);
      if t_accessoriRataB > 0 then
        t_nettoRataB := roundTo((t_nettoRataB * 100) / (100 + paccessori), -2);

      t_accessoriRataB := roundTo(t_nettoRataB / 100 * paccessori, -2);
      t_interessiFrazB := roundTo((t_nettoRataB + t_accessoriRataB) / 100 * dai_interessi(frazionamento), -2);

      dif := t_imponibileRataB - (t_interessiFrazB + t_accessoriRataB + t_nettoRataB);
      if dif <> 0 then
        t_nettoRataB := t_nettoRataB + dif;
    end;

  end
  else
  begin
    t_nettoRataB     := t_NettoRata + t_InteressiFraz;
    t_interessiFrazB := t_InteressiFraz;
    t_accessoriRataB := t_AccessoriRata;

    if premio_rimborsato > 0 then
    begin
      t_imponibileRataB := t_imponibileRata - premio_rimborsato;
      t_tasseRataB      := roundTo(t_imponibileRataB / 100 * ptasse, -2);
      t_lordoRataB      := t_imponibileRataB + t_tasseRataB;
    end
    else
    begin
      t_imponibileRataB := t_imponibileRata;
      t_tasseRataB      := t_TasseRata;
      t_lordoRataB      := t_LordoRata;
    end;

  end;

  verifica.Add('  ==== SECONDA RiGA DI IMPORTI (RATEI) ======  ');
  verifica.Add('BB Netto rata iniziale :' + CurrToStr(t_nettoRataB));
  verifica.Add('BB Accessori rata iniziale :' + CurrToStr(t_accessoriRataB));
  verifica.Add('BB Inter. fraz. rata iniziale :' + CurrToStr(t_interessiFrazB));
  verifica.Add('BB Imponibile rata iniziale :' + CurrToStr(t_imponibileRataB));
  verifica.Add('BB Tasse rata iniziale :' + CurrToStr(t_tasseRataB));
  verifica.Add('BB Lordo rata iniziale :' + CurrToStr(t_lordoRataB));

end;

function ToPremio.GetAccessoriRate: currency;
begin
  result := t_AccessoriRata;
end;

function ToPremio.GetInteressiFraz: currency;
begin
  result := t_InteressiFraz;
end;

function ToPremio.GetLordoRata: currency;
begin
  result := t_LordoRata;
end;

function ToPremio.GetNettoRata: currency;
begin
  result := t_NettoRata;
end;

function ToPremio.GetNettoTotale: currency;
begin
  result := t_NettoTotale;
end;

function ToPremio.GetNettoTotaleRicalcolato: currency;
begin
  result := t_NettoTotaleRicalcolato;
end;

function ToPremio.GetTasseRata: currency;
begin
  result := t_TasseRata;
end;

procedure ToPremio.SetAccessoriRata(const Value: currency);
begin
  t_AccessoriRata := Value;
end;

procedure ToPremio.SetInteressiFraz(const Value: currency);
begin
  t_InteressiFraz := Value;
end;

procedure ToPremio.SetLordoRata(const Value: currency);
begin
  t_LordoRata := Value;
end;

procedure ToPremio.SetNettoRata(const Value: currency);
begin

end;

procedure ToPremio.SetNettoTotale(const Value: currency);
begin
  t_NettoTotale := Value;
end;

procedure ToPremio.SetNettoTotaleRicalcolato(const Value: currency);
begin
  t_NettoTotaleRicalcolato := Value;
end;

procedure ToPremio.SetTasseRata(const Value: currency);
begin
  t_TasseRata := Value;
end;

function ToPremio.GetAccessoriRataB: currency;
begin
  result := t_accessoriRataB
end;

function ToPremio.GetInteressiFrazB: currency;
begin
  result := t_interessiFrazB;
end;

function ToPremio.GetImponibileRata: currency;
begin
  result := t_imponibileRata;
end;

function ToPremio.GetImponibileRataB: currency;
begin
  result := t_imponibileRataB;
end;

function ToPremio.GetLordoRataB: currency;
begin
  result := t_lordoRataB;
end;

function ToPremio.GetNettoRataB: currency;
begin
  result := t_nettoRataB;
end;

function ToPremio.GetTasseRataB: currency;
begin
  result := t_tasseRataB;
end;

procedure ToPremio.SetAccessoriRataB(const Value: currency);
begin
  t_accessoriRataB := Value;
end;

procedure ToPremio.SetImponibileRata(const Value: currency);
begin
  t_imponibileRata := Value;
end;

procedure ToPremio.SetImponibileRataB(const Value: currency);
begin
  t_imponibileRataB := Value;
end;

procedure ToPremio.SetInteressiFrazB(const Value: currency);
begin
  t_interessiFrazB := Value;
end;

procedure ToPremio.SetLordoRataB(const Value: currency);
begin
  t_lordoRataB := Value;
end;

procedure ToPremio.SetNettoRataB(const Value: currency);
begin
  t_nettoRataB := Value;
end;

procedure ToPremio.SetTasseRataB(const Value: currency);
begin
  t_tasseRataB := Value;
end;

end.
