unit UDatiPolSostituitaRec;

interface
type
  TDatiPolizzaSostituita = record
    numeroPolizzaSostituita: string;
    nuovaDataEffetto: TDateTime;
    percentualeTasse: currency;
    ScadenzaUltimoPagamentoDa: TDateTime;
    ScadenzaUltimoPagamentoA: TDateTime;
    lastPremio: currency;
    scadenza: TDateTime;
    decorrenza: TDateTime;
    frazionamento: string;
    premioDaRimborsare: currency;
  end;

implementation


end.
