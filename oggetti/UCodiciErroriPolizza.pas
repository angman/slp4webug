unit UCodiciErroriPolizza;

interface

uses System.SysUtils;

const
  sigla_ORSA    = '1OA'; // orsa 2016
  sigla_DPPP    = '1DA'; // D. PENALE 2016 PAT. PUNTI

  sigla_MEDICO     = '1P3'; // PROFESSIONE MEDICO 2018
  sigla_PARAMEDICO = '1TN'; // TUT PLUS PARAMEDICO 2018   TJ era paramedico 2010
  sigla_CONDOMINIO = '1TF'; // CONDOMINIO 2018        TW era CONDOMINIO 2013
  sigla_FAMIGLIA   = '1N1'; // famiglia plus 2018   N9 era famiglia plus 2010
  sigla_DP_VP      = '1Q1'; // difesa penale circolazione + vita privata
  sigla_TUT_PERS = '1Q3';  // polizza tutela personale circolazione + dp nucleo familiare
  sigla_NEW_DP_GAFATA = '1DF';  // polizza GA Fata 2020 agenzia 910  Medesani
  sigla_NEW_DP_TRATTORI = '1DE';  // polizza DP trattori fatta per Lanza ag. 100

  sigla_PERIZIE = '1JY'; // PERIZIE DI PARTE 2017
  sigle_CIRCOLAZ = '1OA.1OB.1DA.DB.1DE.1DF.1Q3.';

  sigle_CIRCOLAZ_PERIZIE = '1OA.1OB.1OC.1DA.DB.1DF.1DE.1Q3.'+sigla_PERIZIE;   // rimesso GAFATA il 11/12/2019

  sigle_TUTTE = sigle_CIRCOLAZ_PERIZIE +'.'+ sigla_ORSA +'.'+
                sigla_DPPP +'.'+ sigla_MEDICO +'.'+ sigla_PARAMEDICO +'.'+
                sigla_CONDOMINIO +'.'+ sigla_FAMIGLIA + '.' + sigla_DP_VP+ '.'
                + '.' + sigla_NEW_DP_GAFATA + '.' + sigla_NEW_DP_TRATTORI + '.';

  sigle_CIRCOLAZ_VITA_PRIV = '1OA.OB.1OC.1DA.DB.1DF.1DE.'+ sigla_DP_VP;
  sigla_Modulo_privacy     = 'PRI';

const

  MSG_RECORD_ALREADY_LOCKED = 'Il record non � accessibile in quanto' + sLineBreak + '� utilizzato da un altro utente';

  CERR_POLIZZA_BASE = 1000;

  CERR_DATA_EFFETTO_TOO_OLD = CERR_POLIZZA_BASE + 1;
  MSG_DATA_EFFETTO_TOO_OLD  = 'Attenzione! Non � possibile emettere polizze con effetto cos� vecchio !';

  CERR_POL_SOST_DATA_EFFETTO_TOO_OLD = CERR_POLIZZA_BASE + 2;
  MSG_POL_SOST_DATA_EFFETTO_TOO_OLD  =
    'Attenzione! Non � possibile emettere polizze SOSTITUITE con effetto cos� vecchio !';

  CERR_DATA_EFFETTO_TOO_FORWARD = CERR_POLIZZA_BASE + 3;
  MSG_DATA_EFFETTO_TOO_FORWARD  = 'Attenzione! Non � possibile emettere polizze con effetto cos� spostato in avanti !';

  CERR_DATA_EMISS_GT_DATA_EFFETTO = CERR_POLIZZA_BASE + 4;
  MSG_DATA_EMISS_GT_DATA_EFFETTO  =
    'La data di emissione deve essere uguale o precedente a quella di effetto della polizza!';

  CERR_DATA_EMISS_LT_DATA_EFFETTO = CERR_POLIZZA_BASE + 5;
  MSG_DATA_EMISS_LT_DATA_EFFETTO  =
    'La data di emissione non pu� precedere l''effetto della polizza di pi� di 35 giorni!';

  CERR_DATA_EMISS_TOO_FORWARD = CERR_POLIZZA_BASE + 6;
  MSG_DATA_EMISS_TOO_FORWARD  = 'La data di emissione non pu� essere cos� spostata in avanti!';

  CERR_DATA_SCAD_ANTICIP_POLSOST = CERR_POLIZZA_BASE + 7;
  MSG_DATA_SCAD_ANTICIP_POLSOST  = 'Non � possibile anticipare la scadenza della polizza sostituita!';

  CERR_DATA_EFFETTO_GT_DATA_SCADENZA = CERR_POLIZZA_BASE + 8;
  MSG_DATA_EFFETTO_GT_DATA_SCADENZA  = 'La data di decorrenza deve essere precedente a quella di scadenza !';

  CERR_DURATA_MINIMA_POL = CERR_POLIZZA_BASE + 9;
  MSG_DURATA_MINIMA_POL  = 'La durata minima di una polizza � di un anno !';

  CERR_DURATA_POL_GT_MAX = CERR_POLIZZA_BASE + 10;
  MSG_DURATA_POL_GT_MAX  = 'La durata della polizza � superiore alla durata massima utilizzabile da mandato';

  CERR_RATINO_POL_LT_60GG = CERR_POLIZZA_BASE + 11;
  MSG_RATINO_POL_LT_60GG = 'Attenzione, la polizza ha un ratino inferiore a 60 giorni. Viene selezionata ' + sLineBreak
    + 'automaticamente l''opzione che incorpora l''incasso del ratino e della prima rata. ' + sLineBreak +
    'Nel caso in cui si volesse procedere all''incasso del solo ratino ricordarsi di avvisare ' + sLineBreak +
    'l''ufficio portafoglio della SLP affinch� venga emessa e spedita in agenzia la relativa quietanza.';

  CERR_DURATA_ANNI_TOO_LONG = CERR_POLIZZA_BASE + 12;
  MSG_DURATA_ANNI_TOO_LONG  = 'Durata troppo lunga per questo Contraente.';

  CERR_CONTRAENTE_TOO_YOUNG = CERR_POLIZZA_BASE + 13;
  MSG_CONTRAENTE_TOO_YOUNG  = 'Non � possibile intestare una polizza ad un Contraente con meno di 16 anni.';

  CERR_POL_CONTRAENTE_MISSED = CERR_POLIZZA_BASE + 14;
  MSG_POL_CONTRAENTE_MISSED  = 'Bisogna indicare il Contraente della polizza';

  CERR_DATA_EFFETTO_TOO_LOW = CERR_POLIZZA_BASE + 15;
  MSG_DATA_EFFETTO_TOO_LOW  = 'La data di effetto deve maggiore del 01/01/2014';

  CERR_DATA_EMISSIONE_TOO_LOW = CERR_POLIZZA_BASE + 16;
  MSG_DATA_EMISSIONE_TOO_LOW  = 'Bisogna indicare la data di emissione';

  CERR_TASSA_ZERO = CERR_POLIZZA_BASE + 17;
  MSG_TASSA_ZERO  = 'Tasse a zero, polizza non stampabile';

  CERR_PREMIO_LT_ZERO = CERR_POLIZZA_BASE + 18;
  MSG_PREMIO_LT_ZERO  = 'Attenzione! Il premio indicato � negativo ! Correggere !';

  CERR_TARGA_VEICOLO_MISSED = CERR_POLIZZA_BASE + 19;
  MSG_TARGA_VEICOLO_MISSED  = 'Attenzione ! Bisogna indicare la targa del veicolo.';

  CERR_HPQL_VEICOLO_MISSED = CERR_POLIZZA_BASE + 20;
  MSG_HPQL_VEICOLO_MISSED  = 'Attenzione ! Bisogna indicare i quintali del veicolo.';

  CERR_FORMA_B_NEEDED = CERR_POLIZZA_BASE + 21;
  MSG_FORMA_B_NEEDED  = 'Attenzione! Con veicoli superiore a 35 ql � necessario selezionare la forma B.';

  CERR_VEICOLO_DURATA_TOO_LONG = CERR_POLIZZA_BASE + 22;
  MSG_VEICOLO_DURATA_TOO_LONG  = 'Le polizze con durata superiore ai 5 anni non possono avere in garanzia un veicolo.';

  CERR_ESTENSIONE_FAM_MISSED = CERR_POLIZZA_BASE + 23;
  MSG_ESTENSIONE_FAM_MISSED  =
    'Se si attiva l''estensione per il nucleo familiare BISOGNA inserire almeno una persona !';

  CERR_FORMA_B_NOT_ALLOWED = CERR_POLIZZA_BASE + 24;
  MSG_FORMA_B_NOT_ALLOWED  = 'Attenzione ! Con la forma B non si pu� inserire l''estensione Famiglia.';

  CERR_NOME_FAMILIARE_DUP = CERR_POLIZZA_BASE + 25;
  MSG_NOME_FAMILIARE_DUP  = 'Attenzione !! Il nome dell''assicurato n. %d' + sLineBreak +
    '� gi� indicato per l''assicurato n. %d';

  CERR_PATENTE_FAMILIARE_DUP = CERR_POLIZZA_BASE + 26;
  MSG_PATENTE_FAMILIARE_DUP  = 'Attenzione !! La patente dell''assicurato n. %d' + sLineBreak +
    '� gi� indicata con l''assicurato n. %d';

  CERR_FAMILIARE_EQ_CONTRAENTE = CERR_POLIZZA_BASE + 27;
  MSG_FAMILIARE_EQ_CONTRAENTE  = 'Attenzione !! il nome dell''assicurato n. %d' + sLineBreak +
    'corrisponde a quello del contraente';

  CERR_TARGA_TELAIO_MISSED = CERR_POLIZZA_BASE + 28;
  MSG_TARGA_TELAIO_MISSED  = 'Nelle polizze aventi in garanzia un veicolo bisogna NECESSARIAMENTE ' + sLineBreak +
    'indicare la targa del veicolo!';

  CERR_TIPOVEICOLO_NOT_ALLOWED = CERR_POLIZZA_BASE + 29;
  MSG_TIPOVEICOLO_NOT_ALLOWED  = 'Non � possibile usare la forma B con AutoVeicoli, CicloMotori o MotoCicli !!!';

  CERR_PAT_FAMILIARE_EQ_CONTRAENTE = CERR_POLIZZA_BASE + 30;
  MSG_PAT_FAMILIARE_EQ_CONTRAENTE  = 'Attenzione !! La patente dell''assicurato n. %d' + sLineBreak +
    'corrisponde a quella del contraente';

  CERR_NUM_ASSICURATI = CERR_POLIZZA_BASE + 31;
  MSG_NUM_ASSICURATI  = 'Il massimo numero di familiari assicurati � %d';

  CERR_NUM_ASSICURATI_ZERO = CERR_POLIZZA_BASE + 32;
  MSG_NUM_ASSICURATI_ZERO  = 'Deve esistere almeno un familiare assicurato';

  CERR_TITOLO_ESISTENTE = CERR_POLIZZA_BASE + 33;
  MSG_TITOLO_ESISTENTE  = 'Esiste gi� un titolo con lo stesso numero e la stessa data. Inserimento impossibile !';

  CERR_TITOLO_IN_STORICO = CERR_POLIZZA_BASE + 34;
  MSG__TITOLO_IN_STORICO = 'Esiste gi� lo stesso titolo nello storico degli incassi con data di incasso : %s' +
    sLineBreak + '. Inserimento impos. Inserimento impossibile !';
  CERR_TITOLO_PAGATO = CERR_POLIZZA_BASE + 35;
  MSG_TITOLO_PAGATO  = 'Titolo gi� pagato';

  CERR_DT_FC_MONTHS_DIFF = CERR_POLIZZA_BASE + 36;
  MSG_DT_FC_MONTHS_DIFF  = 'Le date di inizio e fine del periodo del foglio cassa' + sLineBreak +
    'DEVONO essere dello stesso mese!';

  CERR_DT_END_FC_TOO_FORWARD = CERR_POLIZZA_BASE + 37;
  MSG_DT_END_FC_TOO_FORWARD  = 'La data di fine periodo non pu� essere' + sLineBreak +
    'cos� spostata in avanti nel tempo!';

  CERR_DT_END_FC_LESS_DT_START = CERR_POLIZZA_BASE + 38;
  MSG_DT_END_FC_LESS_DT_START  = 'La data di fine periodo deve essere maggiore' + sLineBreak +
    'o uguale a quella di inizio periodo!';

  CERR_NO_DATI_EXISTS = CERR_POLIZZA_BASE + 39;
  MSG_NO_DATI_EXISTS  = 'Per il periodo inserito non sono stati trovati dati da mostrare.';

  CERR_CLI_NO_SURNAME = CERR_POLIZZA_BASE + 40;
  MSG_CLI_NO_SURNAME  = 'Attenzione, � obbligatorio la ragione sociale del cliente !';

  CERR_CLI_ADDRESS_INCOMPLETE = CERR_POLIZZA_BASE + 41;
  MSG_CLI_ADDRESS_INCOMPLETE = 'Attenzione, � obbligatorio inserire l''indirizzo in tutte le sue parti:' + sLineBreak +
    'Indirizzo + citt� + CAP + provincia !';

  CERR_CLI_CF_PIVA_MISSED = CERR_POLIZZA_BASE + 42;
  MSG_CLI_CF_PIVA_MISSED  = 'Attenzione, il cod. fiscale o la partita iva sono obbligatori !';

  CERR_CLI_NO_SURNAME_NAME = CERR_POLIZZA_BASE + 43;
  MSG_CLI_NO_SURNAME_NAME  = 'Attenzione, il cognome ed il nome sono entrambe oobligatori per le persone fisiche !';

  CERR_CLI_CODFISC_ERR = CERR_POLIZZA_BASE + 44;
  MSG_CLI_CODFISC_ERR  = 'Il Codice Fiscale inserito � errato.';

  CERR_AGE_DATI_RUI_MISSED = CERR_POLIZZA_BASE + 45;
  MSG_AGE_DATI_RUI_MISSED  = 'I dati di iscrizione al RUI sono obbligatori per le societ�';

  CERR_PREUNI_FRAZ_NO_ANN = CERR_POLIZZA_BASE + 46;
  MSG_PREUNI_FRAZ_NO_ANN  = 'Attenzione ! In una polizza a premio unico' + sLineBreak +
    'il frazionamento DEVE essere ANNUALE !';

  CERR_PREUNI_DUR_GT_3 = CERR_POLIZZA_BASE + 47;
  MSG_PREUNI_DUR_GT_3 = 'ATTENZIONE ! La durata massima di una polizza' + sLineBreak + 'a premio unico � di TRE anni.';

  CERR_CLI_NOT_ERASABLE = CERR_POLIZZA_BASE + 48;
  MSG_CLI_NOT_ERASABLE  = 'Il cliente non � cancellabile' + sLineBreak +
    'in quanto contraente di polizze ancora attive !';

  CERR_POL_TP_FORMA_A = CERR_POLIZZA_BASE + 49;
  MSG_POL_TP_FORMA_A  = 'Attenzione, se si sceglie la forma A per una patente di categoria C/D' + sLineBreak +
    'eventuali sinistri avvenuti conducendo veicoli superiori ai 35 ql. non saranno in garanzia!';

  CERR_POL_TP_FORMA_B = CERR_POLIZZA_BASE + 50;
  MSG_POL_TP_FORMA_B  = 'Attenzione, se si sceglie la forma B la patente deve esse di categoria C o D!';

  // Errori polizza perizie di parte
  CERR_PROFESSIONE_CONTRAENTE = CERR_POLIZZA_BASE + 51;
  MSG_PROFESSIONE_CONTRAENTE  = 'Bisogna OBBLIGATORIAMENTE indicare l''attivit� / professione del Contraente !';

  CERR_GAR_POLIZZA = CERR_POLIZZA_BASE + 52;
  MSG_GAR_POLIZZA  = 'Bisogna selezionare almeno una garanzia per la polizza %s !';

  CERR_TIPO_POLIZZA_IN_GAR = CERR_POLIZZA_BASE + 53;
  MSG_TIPO_POLIZZA_IN_GAR  = 'Bisogna indicare il tipo di polizza in garanzia %s !';

  CERR_COMP_EMITT_IN_GAR = CERR_POLIZZA_BASE + 54;
  MSG_COMP_EMITT_IN_GAR  = 'Bisogna indicare la compagnia che ha emesso la polizza in garanzia %s !';

  CERR_AGE_IN_GAR = CERR_POLIZZA_BASE + 55;
  MSG_AGE_IN_GAR  = 'Bisogna indicare l''agenzia che ha emesso la polizza in garanzia %s !';

  CERR_MANCA_PREMIO_BASE = CERR_POLIZZA_BASE + 56;
  MSG_MANCA_PREMIO_BASE  = 'E'' obbligatorio inserire il premio della polizza base nella polizza %s';

  CERR_DATE_POLIZZA = CERR_POLIZZA_BASE + 57;
  MSG_DATE_POLIZZA  = 'Contratto %s - E'' obbligatorio inserire le date' + sLineBreak +
    'di decorrenza/scadenza della polizza in garanzia';

  CERR_DATE_NON_COERENTI = CERR_POLIZZA_BASE + 58;
  MSG_DATE_NON_COERENTI  = 'Le date di effetto e/o scadenza della polizza %s' + sLineBreak +
    'in garanzia non sono coerenti !';

  CERR_DESCR_GARANZIE = CERR_POLIZZA_BASE + 59;
  MSG_DESCR_GARANZIE  = 'E'' obbligatorio inserire la descrizione delle garanzie della polizza base %s';

  CERR_MASS_GAR_MANCANTE = CERR_POLIZZA_BASE + 60;
  MSG_MASS_GAR_MANCANTE  = 'Selezionare il massimale della polizza';

  // Errori numerazione polizza
  CERR_NUM_POL_ENDED = CERR_POLIZZA_BASE + 61;
  MSG_NUM_POL_ENDED  = 'Numerazione per la stampa della polizza terminata;' + sLineBreak +
    'richiedere una nuova numerazione in direzione!';

  // Errore caricamento modello report polizza
  CERR_MOD_REPORT_POLIZZA = CERR_POLIZZA_BASE + 62;
  MSG_MOD_REPORT_POLIZZA  = 'Modello report tipo polizza = %s non trovato';

  CERR_FAMILIARI_MANCANTI = CERR_POLIZZA_BASE + 63;
  MSG_FAMILIARI_MANCANTI  = 'Dati familiari mancanti o incompleti';

  CERR_NUM_POLIZZA_DUPLICATE = CERR_POLIZZA_BASE + 64;
  MSG_NUM_POLIZZA_DUPLICATE  = 'Numero Polizza: %s gi� utilizzato';

  CERR_PERS_GIURIDICA = CERR_POLIZZA_BASE + 65;
  MSG_PERS_GIURIDICA  = 'Il Contraente di questa polizza non pu� essere una persona giuridica !';

  // Polizza paramedico
  CERR_ATT_CONTRAENTE = CERR_POLIZZA_BASE + 66;
  MSG_ATT_CONTRAENTE  = 'Bisogna indicare il tipo di attivit� svolta dal Contraente.';

  CERR_SEDE_ATT_CONTRAENTE = CERR_POLIZZA_BASE + 67;
  MSG_SEDE_ATT_CONTRAENTE  = 'Bisogna indicare la sede dell''attivit�.';

  // Polizze circolazione stradale
  CERR_ASS_PERS_GIU = CERR_POLIZZA_BASE + 68;
  MSG_ASS_PERS_GIU  = 'L''assicurato non pu� essere una persona giuridica.';

  // Allegati
  CERR_DESCR_ALLEGATO = CERR_POLIZZA_BASE + 69;
  MSG_DESCR_ALLEGATO  = 'Descrizione allegato obbligatoria';

  CERR_TIPO_ALLEGATO = CERR_POLIZZA_BASE + 70;
  MSG_TIPO_ALLEGATO  = 'Tipo allegato obbligatorio';

  CERR_DT_VAL_ALLEGATO = CERR_POLIZZA_BASE + 71;
  MSG_DT_VAL_ALLEGATO  = 'Data inizio validit� obbligatoria';

  CERR_DT_VAL_ALLEGATO_LESS_CURRENT = CERR_POLIZZA_BASE + 72;
  MSG_DT_VAL_ALLEGATO_LESS_CURRENT  = 'Data inizio antecedente a quella odierna';

  CERR_ALLEGATO_NOT_FOUND = CERR_POLIZZA_BASE + 73;
  MSG_ALLEGATO_NOT_FOUND  = 'Non � stato caricato nessun allegato';

  // circolazione stradale
  CERR_NOME_ASSICURATO_MISSED = CERR_POLIZZA_BASE + 74;
  MSG_NOME_ASSICURATO_MISSED  = 'Attenzione ! Bisogna indicare il nome dell''assicurato.';

  CERR_CODFISC_ASSICURATO_MISSED = CERR_POLIZZA_BASE + 75;
  MSG_CODFISC_ASSICURATO_MISSED  = 'Attenzione ! Bisogna indicare il codice fiscale dell''assicurato.';

  // Errori DMDatiAgePolizza
  CERR_COMPANY_MISSED = CERR_POLIZZA_BASE + 75;
  MSG_COMPANY_MISSED  = 'E'' necessario indicare la compagnia !';

  CERR_NPOLIZZA_MISSED = CERR_POLIZZA_BASE + 76;
  MSG_NPOLIZZA_MISSED  = 'E'' necessario indicare il numero di contratto !';

  CERR_CONTRAENTE_MISSED = CERR_POLIZZA_BASE + 77;
  MSG_CONTRAENTE_MISSED  = 'E'' necessario indicare il Contraente !';

  CERR_DATE_MISSED = CERR_POLIZZA_BASE + 78;
  MSG_DATE_MISSED  = 'E'' necessario indicare la decorrenza e la scadenza !';

  CERR_FRAZIONAMENTO_MISSED = CERR_POLIZZA_BASE + 79;
  MSG_FRAZIONAMENTO_MISSED  = 'E'' necessario indicare il frazionamento della polizza !';

  // DMAgeCollaboratori
  CERR_COLLAB_SIGLA_MISSED = CERR_POLIZZA_BASE + 80;
  MSG_COLLAB_SIGLA_MISSED  = 'Bisogna indicare una sigla per il collaboratore; la sigla, ' + sLineBreak +
    'di tre caratteri/numeri, sar� stampata sulle polizze perfezionate dal collaboratore';

  CERR_NRUI_MISSED = CERR_POLIZZA_BASE + 81;
  MSG_NRUI_MISSED  = 'Bisogna indicare il numero RUI';

  // Messaggi relativi agli avvisi
  MSG_APP_DISABLED = 'L''applicazione � momentaneamente disattivata';
  MSG_FOR_YOU      = 'Ci sono comunicazioni da visionare';

  // Messaggi relativi alla stampa statistiche
  MSG_DATA_A_LT_DATA_DA = 'La data A non pu� essere minore della data Da';
  MSG_NO_DATA_FOUND     = 'Nessun dato estratto con l''attuale impostazione dei filtri';

  // Messaggi relativi alla stampa delle quietanze
  MSG_STAMPA_QUIET_DA_SUBAGE = 'Operazione non eseguibile da una sub-agenzia.';
  CERR_QUIETANZE_NOT_FOUND   = CERR_POLIZZA_BASE + 82;
  MSG_QUIETANZE_NOT_FOUND   = 'Non Esistono quietanze nel periodo richiesto';

    CERR_MANCA_PREMIO_BASE_MASSIMALE = CERR_POLIZZA_BASE + 56;
  MSG_MANCA_PREMIO_BASE_MASSIMALE  = 'E'' obbligatorio inserire il premio' + sLineBreak + 'della polizza base nella polizza %s'
  + sLineBreak + 'e il massimale';
  CERR_MAXTELPREF_GT_ONE = CERR_POLIZZA_BASE + 57;
  MSG_MAXTELPREF_GT_ONE  = 'E'' consentito solo un %s preferenziale';

  CERR_INVALID_MAIL = CERR_POLIZZA_BASE + 58;
  MSG_INVALID_MAIL  = 'Mail non valida';

implementation

end.
