unit UTipoPol;

interface

uses dbisamtb;

type
  Ttipo_pol = class
  private
    FCOD_TIPO_POL: integer;
    FDESCRIZIONE: string;
    FSIGLA: string;
    FMODIFICA: TdateTime;
    FNOTE: string;
    Fcodice: String;
    FRIF_COD_REL_RAMO_MANDA: integer;
    FRIF_COD_RAMO: integer;
    FLR: string;
    FRIF_COD_COMPAGNIA: integer;
    FPERC_TASSE: currency;
    FCOMODO: String;
    Faccessori_max: currency;
    Faccessori_min: currency;
    FSTAMPA_SLP: String;
    FCODICE_PERSONALE: String;
    FPERC_PROVV: currency;
    FPERC_AGGIO_INCASSO: currency;
    FPERC_ACCESSORI: currency;
    FACCESSORI_AGE: currency;
    Fsolo_per: String;
    Ftipologia_polizza: String;
  public

    constructor create(xcod_tipo_pol: integer; RifQuery: TDBISAMquery);

    property COD_TIPO_POL: integer read FCOD_TIPO_POL write FCOD_TIPO_POL;
    property DESCRIZIONE: string read FDESCRIZIONE write FDESCRIZIONE;
    property SIGLA: string read FSIGLA write FSIGLA;
    property MODIFICA: TdateTime read FMODIFICA write FMODIFICA;
    property NOTE: string read FNOTE write FNOTE;
    property codice: String read Fcodice write Fcodice;
    property RIF_COD_REL_RAMO_MANDA: integer read FRIF_COD_REL_RAMO_MANDA write FRIF_COD_REL_RAMO_MANDA;
    property RIF_COD_RAMO: integer read FRIF_COD_RAMO write FRIF_COD_RAMO;
    property LR: string read FLR write FLR;
    property RIF_COD_COMPAGNIA: integer read FRIF_COD_COMPAGNIA write FRIF_COD_COMPAGNIA;
    property PERC_TASSE: currency read FPERC_TASSE write FPERC_TASSE;
    property COMODO: String read FCOMODO write FCOMODO;
    property accessori_max: currency read Faccessori_max write Faccessori_max;
    property accessori_min: currency read Faccessori_min write Faccessori_min;
    property STAMPA_SLP: String read FSTAMPA_SLP write FSTAMPA_SLP;
    property CODICE_PERSONALE: String read FCODICE_PERSONALE write FCODICE_PERSONALE;
    property PERC_PROVV: currency read FPERC_PROVV write FPERC_PROVV;
    property PERC_AGGIO_INCASSO: currency read FPERC_AGGIO_INCASSO write FPERC_AGGIO_INCASSO;
    property PERC_ACCESSORI: currency read FPERC_ACCESSORI write FPERC_ACCESSORI;
    property ACCESSORI_AGE: currency read FACCESSORI_AGE write FACCESSORI_AGE;
    property solo_per: String read Fsolo_per write Fsolo_per;
    property tipologia_polizza: String read Ftipologia_polizza write Ftipologia_polizza;

  end;

implementation

constructor Ttipo_pol.create(xcod_tipo_pol: integer; RifQuery: TDBISAMquery);
begin
  RifQuery.Close;
  RifQuery.ParamByName('cod_tipo_pol').asInteger := xcod_tipo_pol;
  RifQuery.Open;
  COD_TIPO_POL           := RifQuery.fieldbyname('COD_TIPO_POL').asInteger;
  DESCRIZIONE            := RifQuery.fieldbyname('descrizione').asstring;
  SIGLA                  := RifQuery.fieldbyname('sigla').asstring;
  MODIFICA               := RifQuery.fieldbyname('modifica').asdateTime;
  NOTE                   := RifQuery.fieldbyname('note').asstring;
  codice                 := RifQuery.fieldbyname('codice').asstring;
  RIF_COD_REL_RAMO_MANDA := RifQuery.fieldbyname('RIF_COD_REL_RAMO_MANDA').asInteger;
  RIF_COD_RAMO           := RifQuery.fieldbyname('rif_cod_ramo').asInteger;
  LR                     := RifQuery.fieldbyname('LR').asstring;
  RIF_COD_COMPAGNIA      := RifQuery.fieldbyname('RIF_COD_COMPAGNIA').asInteger;
  PERC_TASSE             := RifQuery.fieldbyname('PERC_TASSE').ascurrency;
  COMODO                 := RifQuery.fieldbyname('comodo').asstring;
  accessori_max          := RifQuery.fieldbyname('accessori_max').ascurrency;
  accessori_min          := RifQuery.fieldbyname('accessori_min').ascurrency;
  STAMPA_SLP             := RifQuery.fieldbyname('stampa_slp4web').asstring;
  CODICE_PERSONALE       := RifQuery.fieldbyname('codice_personale').asstring;
  PERC_PROVV             := RifQuery.fieldbyname('perc_provv').ascurrency;
  PERC_AGGIO_INCASSO     := RifQuery.fieldbyname('PERC_AGGIO_INCASSO').ascurrency;
  PERC_ACCESSORI         := RifQuery.fieldbyname('PERC_ACCESSORI').ascurrency;
  ACCESSORI_AGE          := RifQuery.fieldbyname('ACCESSORI_AGE').ascurrency;
  solo_per               := RifQuery.fieldbyname('solo_per').asstring;
  // tipologia_polizza:= RifQuery.fieldbyname('tipologia_polizza').asString;

end;

end.
