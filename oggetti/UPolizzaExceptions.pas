unit UPolizzaExceptions;

interface

uses
  System.SysUtils;

type
  ESLP4WebException = class(Exception);

  ELoginError         = class(ESLP4WebException);
  EAuthorizationError = class(ESLP4WebException);

  EPolizzaError = class(ESLP4WebException)
  private
    FErrorCode: Integer;
  public
    constructor Create(AErrorCode: Integer; AErrorMessage: string); overload;
    constructor Create(AErrorMessage: string); overload;
    property ErrorCode: Integer read FErrorCode;
  end;

  EPolDataEffettoError = class(EPolizzaError);

  EPolDataEmissioneError = class(EPolizzaError);

  EPolDataScadenzaError    = class(EPolizzaError);
  EPolDataScadPolSostError = class(EPolizzaError);
  EPolDurataAnniTooLong    = class(EPolizzaError);
  EPolContraenteTooYoung   = class(EPolizzaError);

  ENumPolizzaDuplicate = class(EPolizzaError);

implementation

{ EPolizzaError }

constructor EPolizzaError.Create(AErrorCode: Integer; AErrorMessage: string);
begin
  inherited Create(AErrorMessage);
  FErrorCode := AErrorCode;
end;

constructor EPolizzaError.Create(AErrorMessage: string);
begin
  inherited Create(AErrorMessage);
end;

end.
