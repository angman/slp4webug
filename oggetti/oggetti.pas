unit oggetti;

interface

type

  Tpremio = class(Tobject)
  private
    Flordo, Fptasse, Ftasse, Fnetto, Fssn, FpSsn, Faccessori, Fimponibile: currency;
    function readAccessori: currency;
    function readImponibile: currency;
    function readLordo: currency;
    function readNetto: currency;
    function readptasse: currency;
    function readSSN: currency;
    function readpSSN: currency;
    function readTasse: currency;
    procedure setAccessori(const Value: currency);
    procedure setImponibile(const Value: currency);
    procedure setLordo(const Value: currency);
    procedure setNetto(const Value: currency);
    procedure setptasse(const Value: currency);
    procedure setpSSN(const Value: currency);
    procedure setSSN(const Value: currency);
    procedure setTasse(const Value: currency);

    procedure calcola;

  published
    property lordo: currency read readLordo write setLordo;
    property ptasse: currency read readptasse write setptasse;
    property pSSN: currency read readpSSN write setpSSN;
    property tasse: currency read readTasse write setTasse;
    property netto: currency read readNetto write setNetto;
    property ssn: currency read readSSN write setSSN;
    property accessori: currency read readAccessori write setAccessori;
    property imponibile: currency read readImponibile write setImponibile;
  public

    // lordo, ptasse, tasse, netto, ssn, accessori, imponibile: currency;
    partenza: string[1]; // L = parti da lordo   N = parti da netto
    constructor Create;
    destructor free;
  end;

  Tscadenze = class(Tobject)
  private
    F_COD_SCADENZA: Integer;
    F_DENOMINAZ: String;
    F_OLD_COMPAGNIA: String;
    F_RAMO: Integer;
    F_DESCRIZ: String;
    F_FRAZIONAM: String;
    F_LORDO: currency;
    F_RATA: TdateTime;
    F_COD_CLI: String;
    F_N_POLIZZA: String;
    F_STATO: String;
    F_DATA_PAG: TdateTime;
    F_FOGLIO_CASSA: Integer;
    F_DATA_ANN: TdateTime;
    F_MODIFICA: TdateTime;
    F_provvigioni: currency;
    F_netto: currency;
    F_ptasse: currency;
    F_accessori: currency;
    F_tasse: currency;
    F_valuta: String;
    F_RIF_COD_COMPAGNIA: Integer;
    F_RIF_COD_POLIZZA: Integer;
    F_CAUSALE_ANNUL: Integer;
    F_inc_indice: currency;
    F_PROGRESSIVO: Integer;
    F_decorrenza: TdateTime;
    F_scadenza: TdateTime;
    F_INVIO_AVV_SCADENZA: TdateTime;
    F_INVIO_1_SOLLECITO: TdateTime;
    F_INVIO_2_SOLLECITO: TdateTime;
    F_TIPO_TITOLO: String;
    F_RIF_CLIENTE: Integer;
    F_DATA_COPERTURA: TdateTime;
    F_DATA_STAMPA: TdateTime;
    F_P_NOTA_NUMERO: Integer;
    F_P_NOTA_DATA: TdateTime;
    F_DATA_REG_PAG: TdateTime;
    F_DATA_PAG_CCP: TdateTime;
    F_INCASSATA_DA: Integer;
    F_DATA_IMPORTAZIONE: TdateTime;
    F_CODICE_IMPORTAZIONE: Integer;
    F_provvigioni_sub: currency;
    F_COD_SUB_AGE: Integer;
    F_COD_FC_SUB_AGE: Integer;
    F_DT_LAST_MOD: TdateTime;
    F_ID_TIT_CMP: String;
    F_ID_GEN_SLP_AGE: Integer;

    function read_COD_SCADENZA: Integer;
    function read_DENOMINAZ: String;
    function read_OLD_COMPAGNIA: String;
    function read_RAMO: Integer;
    function read_DESCRIZ: String;
    function read_FRAZIONAM: String;
    function read_LORDO: currency;
    function read_RATA: TdateTime;
    function read_COD_CLI: String;
    function read_N_POLIZZA: String;
    function read_STATO: String;
    function read_DATA_PAG: TdateTime;
    function read_FOGLIO_CASSA: Integer;
    function read_DATA_ANN: TdateTime;
    function read_MODIFICA: TdateTime;
    function read_provvigioni: currency;
    function read_netto: currency;
    function read_ptasse: currency;
    function read_accessori: currency;
    function read_tasse: currency;
    function read_valuta: String;
    function read_RIF_COD_COMPAGNIA: Integer;
    function read_RIF_COD_POLIZZA: Integer;
    function read_CAUSALE_ANNUL: Integer;
    function read_inc_indice: currency;
    function read_PROGRESSIVO: Integer;
    function read_decorrenza: TdateTime;
    function read_scadenza: TdateTime;
    function read_INVIO_AVV_SCADENZA: TdateTime;
    function read_INVIO_1_SOLLECITO: TdateTime;
    function read_INVIO_2_SOLLECITO: TdateTime;
    function read_TIPO_TITOLO: String;
    function read_RIF_CLIENTE: Integer;
    function read_DATA_COPERTURA: TdateTime;
    function read_DATA_STAMPA: TdateTime;
    function read_P_NOTA_NUMERO: Integer;
    function read_P_NOTA_DATA: TdateTime;
    function read_DATA_REG_PAG: TdateTime;
    function read_DATA_PAG_CCP: TdateTime;
    function read_INCASSATA_DA: Integer;
    function read_DATA_IMPORTAZIONE: TdateTime;
    function read_CODICE_IMPORTAZIONE: Integer;
    function read_provvigioni_sub: currency;
    function read_COD_SUB_AGE: Integer;
    function read_COD_FC_SUB_AGE: Integer;
    function read_DT_LAST_MOD: TdateTime;
    function read_ID_TIT_CMP: String;
    function read_ID_GEN_SLP_AGE: Integer;
    // procedure set_COD_SCADENZA(const Value: Integer);
    procedure set_DENOMINAZ(const Value: String);
    procedure set_OLD_COMPAGNIA(const Value: String);
    procedure set_RAMO(const Value: Integer);
    procedure set_DESCRIZ(const Value: String);
    procedure set_FRAZIONAM(const Value: String);
    procedure set_LORDO(const Value: currency);
    procedure set_RATA(const Value: TdateTime);
    procedure set_COD_CLI(const Value: String);
    procedure set_N_POLIZZA(const Value: String);
    procedure set_STATO(const Value: String);
    procedure set_DATA_PAG(const Value: TdateTime);
    procedure set_FOGLIO_CASSA(const Value: Integer);
    procedure set_DATA_ANN(const Value: TdateTime);
    procedure set_MODIFICA(const Value: TdateTime);
    procedure set_provvigioni(const Value: currency);
    procedure set_netto(const Value: currency);
    procedure set_ptasse(const Value: currency);
    procedure set_accessori(const Value: currency);
    procedure set_tasse(const Value: currency);
    procedure set_valuta(const Value: String);
    procedure set_RIF_COD_COMPAGNIA(const Value: Integer);
    procedure set_RIF_COD_POLIZZA(const Value: Integer);
    procedure set_CAUSALE_ANNUL(const Value: Integer);
    procedure set_inc_indice(const Value: currency);
    procedure set_PROGRESSIVO(const Value: Integer);
    procedure set_decorrenza(const Value: TdateTime);
    procedure set_scadenza(const Value: TdateTime);
    procedure set_INVIO_AVV_SCADENZA(const Value: TdateTime);
    procedure set_INVIO_1_SOLLECITO(const Value: TdateTime);
    procedure set_INVIO_2_SOLLECITO(const Value: TdateTime);
    procedure set_TIPO_TITOLO(const Value: String);
    procedure set_RIF_CLIENTE(const Value: Integer);
    procedure set_DATA_COPERTURA(const Value: TdateTime);
    procedure set_DATA_STAMPA(const Value: TdateTime);
    procedure set_P_NOTA_NUMERO(const Value: Integer);
    procedure set_P_NOTA_DATA(const Value: TdateTime);
    procedure set_DATA_REG_PAG(const Value: TdateTime);
    procedure set_DATA_PAG_CCP(const Value: TdateTime);
    procedure set_INCASSATA_DA(const Value: Integer);
    procedure set_DATA_IMPORTAZIONE(const Value: TdateTime);
    procedure set_CODICE_IMPORTAZIONE(const Value: Integer);
    procedure set_provvigioni_sub(const Value: currency);
    procedure set_COD_SUB_AGE(const Value: Integer);
    procedure set_COD_FC_SUB_AGE(const Value: Integer);
    procedure set_DT_LAST_MOD(const Value: TdateTime);
    procedure set_ID_TIT_CMP(const Value: String);
    procedure set_ID_GEN_SLP_AGE(const Value: Integer);

  public
    constructor Create;
    destructor free;

    property COD_SCADENZA: Integer read read_COD_SCADENZA;
    property DENOMINAZ: String read read_DENOMINAZ write set_DENOMINAZ;
    property OLD_COMPAGNIA: String read read_OLD_COMPAGNIA write set_OLD_COMPAGNIA;
    property RAMO: Integer read read_RAMO write set_RAMO;
    property DESCRIZ: String read read_DESCRIZ write set_DESCRIZ;
    property FRAZIONAM: String read read_FRAZIONAM write set_FRAZIONAM;
    property lordo: currency read read_LORDO write set_LORDO;
    property RATA: TdateTime read read_RATA write set_RATA;
    property COD_CLI: String read read_COD_CLI write set_COD_CLI;
    property N_POLIZZA: String read read_N_POLIZZA write set_N_POLIZZA;
    property STATO: String read read_STATO write set_STATO;
    property DATA_PAG: TdateTime read read_DATA_PAG write set_DATA_PAG;
    property FOGLIO_CASSA: Integer read read_FOGLIO_CASSA write set_FOGLIO_CASSA;
    property DATA_ANN: TdateTime read read_DATA_ANN write set_DATA_ANN;
    property MODIFICA: TdateTime read read_MODIFICA write set_MODIFICA;
    property provvigioni: currency read read_provvigioni write set_provvigioni;
    property netto: currency read read_netto write set_netto;
    property ptasse: currency read read_ptasse write set_ptasse;
    property accessori: currency read read_accessori write set_accessori;
    property tasse: currency read read_tasse write set_tasse;
    property valuta: String read read_valuta write set_valuta;
    property RIF_COD_COMPAGNIA: Integer read read_RIF_COD_COMPAGNIA write set_RIF_COD_COMPAGNIA;
    property RIF_COD_POLIZZA: Integer read read_RIF_COD_POLIZZA write set_RIF_COD_POLIZZA;
    property CAUSALE_ANNUL: Integer read read_CAUSALE_ANNUL write set_CAUSALE_ANNUL;
    property inc_indice: currency read read_inc_indice write set_inc_indice;
    property PROGRESSIVO: Integer read read_PROGRESSIVO write set_PROGRESSIVO;
    property decorrenza: TdateTime read read_decorrenza write set_decorrenza;
    property scadenza: TdateTime read read_scadenza write set_scadenza;
    property INVIO_AVV_SCADENZA: TdateTime read read_INVIO_AVV_SCADENZA write set_INVIO_AVV_SCADENZA;
    property INVIO_1_SOLLECITO: TdateTime read read_INVIO_1_SOLLECITO write set_INVIO_1_SOLLECITO;
    property INVIO_2_SOLLECITO: TdateTime read read_INVIO_2_SOLLECITO write set_INVIO_2_SOLLECITO;
    property TIPO_TITOLO: String read read_TIPO_TITOLO write set_TIPO_TITOLO;
    property RIF_CLIENTE: Integer read read_RIF_CLIENTE write set_RIF_CLIENTE;
    property DATA_COPERTURA: TdateTime read read_DATA_COPERTURA write set_DATA_COPERTURA;
    property DATA_STAMPA: TdateTime read read_DATA_STAMPA write set_DATA_STAMPA;
    property P_NOTA_NUMERO: Integer read read_P_NOTA_NUMERO write set_P_NOTA_NUMERO;
    property P_NOTA_DATA: TdateTime read read_P_NOTA_DATA write set_P_NOTA_DATA;
    property DATA_REG_PAG: TdateTime read read_DATA_REG_PAG write set_DATA_REG_PAG;
    property DATA_PAG_CCP: TdateTime read read_DATA_PAG_CCP write set_DATA_PAG_CCP;
    property INCASSATA_DA: Integer read read_INCASSATA_DA write set_INCASSATA_DA;
    property DATA_IMPORTAZIONE: TdateTime read read_DATA_IMPORTAZIONE write set_DATA_IMPORTAZIONE;
    property CODICE_IMPORTAZIONE: Integer read read_CODICE_IMPORTAZIONE write set_CODICE_IMPORTAZIONE;
    property provvigioni_sub: currency read read_provvigioni_sub write set_provvigioni_sub;
    property COD_SUB_AGE: Integer read read_COD_SUB_AGE write set_COD_SUB_AGE;
    property COD_FC_SUB_AGE: Integer read read_COD_FC_SUB_AGE write set_COD_FC_SUB_AGE;
    property DT_LAST_MOD: TdateTime read read_DT_LAST_MOD write set_DT_LAST_MOD;
    property ID_TIT_CMP: String read read_ID_TIT_CMP write set_ID_TIT_CMP;
    property ID_GEN_SLP_AGE: Integer read read_ID_GEN_SLP_AGE write set_ID_GEN_SLP_AGE;

  end;

implementation

{ Tpremio  ======================================================================================================= }

constructor Tpremio.Create;
begin
  inherited Create;
  Fssn       := 0;
  Faccessori := 0;
end;

destructor Tpremio.free;
begin
  inherited destroy;
end;

procedure Tpremio.calcola;
begin
  // calcola i valori in base al valore della direzione
  if partenza = 'L' then
  begin
    // parti dal premio lordo e scomponi
    Fnetto      := (Flordo / (1 + (Fptasse + FpSsn) / 100)) - Faccessori;
    Fimponibile := Fnetto + Faccessori;
    Fssn        := 0;
    if FpSsn = 0 then
      Ftasse := Flordo - Fimponibile
    else
    begin
      Ftasse := Fnetto / 100 * Fptasse;
      Fssn   := Fnetto / 100 * FpSsn;
    end;
  end
  else
  begin
    // parti dal premio netto e calcola il lordo
    Fssn        := Fnetto / 100 * FpSsn;
    Ftasse      := (Fnetto + Faccessori) / 100 * Fptasse;
    Fimponibile := (Fnetto + Faccessori);
    Flordo      := Fimponibile + Ftasse + Fssn;
  end;
end;

function Tpremio.readAccessori: currency;
begin
  result := Faccessori;
end;

function Tpremio.readImponibile: currency;
begin
  result := Fimponibile;
end;

function Tpremio.readLordo: currency;
begin
  result := Flordo;
end;

function Tpremio.readNetto: currency;
begin
  result := Fnetto;
end;

function Tpremio.readptasse: currency;
begin
  result := Fptasse;
end;

function Tpremio.readSSN: currency;
begin
  result := Fssn;
end;

function Tpremio.readTasse: currency;
begin
  result := Ftasse;
end;

procedure Tpremio.setAccessori(const Value: currency);
begin
  Faccessori := Value;
end;

procedure Tpremio.setImponibile(const Value: currency);
begin
  Fimponibile := Value;
end;

procedure Tpremio.setLordo(const Value: currency);
begin
  // if Flordo <> value then
  // begin
  Flordo   := Value;
  partenza := 'L';
  calcola;
  // end;
end;

procedure Tpremio.setNetto(const Value: currency);
begin
  // if Fnetto <> value then
  // begin
  Fnetto   := Value;
  partenza := 'N';
  calcola;
  // end;
end;

procedure Tpremio.setptasse(const Value: currency);
begin
  Fptasse := Value;
end;

procedure Tpremio.setSSN(const Value: currency);
begin
  Fssn := Value;
end;

procedure Tpremio.setTasse(const Value: currency);
begin
  Ftasse := Value;
end;

function Tpremio.readpSSN: currency;
begin
  result := FpSsn;
end;

procedure Tpremio.setpSSN(const Value: currency);
begin
  FpSsn := Value;
end;

{ Tscadenze }

constructor Tscadenze.Create;
begin

end;

destructor Tscadenze.free;
begin

end;

// ========================  implementazione function e e procedure della classe ===================

// procedure Tscadenze.set_COD_SCADENZA(const Value: Integer);
// begin
// F_COD_SCADENZA := value;
// end;

function Tscadenze.read_COD_SCADENZA: Integer;
begin
  result := F_COD_SCADENZA;
end;

procedure Tscadenze.set_DENOMINAZ(const Value: String);
begin
  F_DENOMINAZ := Value;
end;

function Tscadenze.read_DENOMINAZ: String;
begin
  result := F_DENOMINAZ;
end;

procedure Tscadenze.set_OLD_COMPAGNIA(const Value: String);
begin
  F_OLD_COMPAGNIA := Value;
end;

function Tscadenze.read_OLD_COMPAGNIA: String;
begin
  result := F_OLD_COMPAGNIA;
end;

procedure Tscadenze.set_RAMO(const Value: Integer);
begin
  F_RAMO := Value;
end;

function Tscadenze.read_RAMO: Integer;
begin
  result := F_RAMO;
end;

procedure Tscadenze.set_DESCRIZ(const Value: String);
begin
  F_DESCRIZ := Value;
end;

function Tscadenze.read_DESCRIZ: String;
begin
  result := F_DESCRIZ;
end;

procedure Tscadenze.set_FRAZIONAM(const Value: String);
begin
  F_FRAZIONAM := Value;
end;

function Tscadenze.read_FRAZIONAM: String;
begin
  result := F_FRAZIONAM;
end;

procedure Tscadenze.set_LORDO(const Value: currency);
begin
  F_LORDO := Value;
end;

function Tscadenze.read_LORDO: currency;
begin
  result := F_LORDO;
end;

procedure Tscadenze.set_RATA(const Value: TdateTime);
begin
  F_RATA := Value;
end;

function Tscadenze.read_RATA: TdateTime;
begin
  result := F_RATA;
end;

procedure Tscadenze.set_COD_CLI(const Value: String);
begin
  F_COD_CLI := Value;
end;

function Tscadenze.read_COD_CLI: String;
begin
  result := F_COD_CLI;
end;

procedure Tscadenze.set_N_POLIZZA(const Value: String);
begin
  F_N_POLIZZA := Value;
end;

function Tscadenze.read_N_POLIZZA: String;
begin
  result := F_N_POLIZZA;
end;

procedure Tscadenze.set_STATO(const Value: String);
begin
  F_STATO := Value;
end;

function Tscadenze.read_STATO: String;
begin
  result := F_STATO;
end;

procedure Tscadenze.set_DATA_PAG(const Value: TdateTime);
begin
  F_DATA_PAG := Value;
end;

function Tscadenze.read_DATA_PAG: TdateTime;
begin
  result := F_DATA_PAG;
end;

procedure Tscadenze.set_FOGLIO_CASSA(const Value: Integer);
begin
  F_FOGLIO_CASSA := Value;
end;

function Tscadenze.read_FOGLIO_CASSA: Integer;
begin
  result := F_FOGLIO_CASSA;
end;

procedure Tscadenze.set_DATA_ANN(const Value: TdateTime);
begin
  F_DATA_ANN := Value;
end;

function Tscadenze.read_DATA_ANN: TdateTime;
begin
  result := F_DATA_ANN;
end;

procedure Tscadenze.set_MODIFICA(const Value: TdateTime);
begin
  F_MODIFICA := Value;
end;

function Tscadenze.read_MODIFICA: TdateTime;
begin
  result := F_MODIFICA;
end;

procedure Tscadenze.set_provvigioni(const Value: currency);
begin
  F_provvigioni := Value;
end;

function Tscadenze.read_provvigioni: currency;
begin
  result := F_provvigioni;
end;

procedure Tscadenze.set_netto(const Value: currency);
begin
  F_netto := Value;
end;

function Tscadenze.read_netto: currency;
begin
  result := F_netto;
end;

procedure Tscadenze.set_ptasse(const Value: currency);
begin
  F_ptasse := Value;
end;

function Tscadenze.read_ptasse: currency;
begin
  result := F_ptasse;
end;

procedure Tscadenze.set_accessori(const Value: currency);
begin
  F_accessori := Value;
end;

function Tscadenze.read_accessori: currency;
begin
  result := F_accessori;
end;

procedure Tscadenze.set_tasse(const Value: currency);
begin
  F_tasse := Value;
end;

function Tscadenze.read_tasse: currency;
begin
  result := F_tasse;
end;

procedure Tscadenze.set_valuta(const Value: String);
begin
  F_valuta := Value;
end;

function Tscadenze.read_valuta: String;
begin
  result := F_valuta;
end;

procedure Tscadenze.set_RIF_COD_COMPAGNIA(const Value: Integer);
begin
  F_RIF_COD_COMPAGNIA := Value;
end;

function Tscadenze.read_RIF_COD_COMPAGNIA: Integer;
begin
  result := F_RIF_COD_COMPAGNIA;
end;

procedure Tscadenze.set_RIF_COD_POLIZZA(const Value: Integer);
begin
  F_RIF_COD_POLIZZA := Value;
end;

function Tscadenze.read_RIF_COD_POLIZZA: Integer;
begin
  result := F_RIF_COD_POLIZZA;
end;

procedure Tscadenze.set_CAUSALE_ANNUL(const Value: Integer);
begin
  F_CAUSALE_ANNUL := Value;
end;

function Tscadenze.read_CAUSALE_ANNUL: Integer;
begin
  result := F_CAUSALE_ANNUL;
end;

procedure Tscadenze.set_inc_indice(const Value: currency);
begin
  F_inc_indice := Value;
end;

function Tscadenze.read_inc_indice: currency;
begin
  result := F_inc_indice;
end;

procedure Tscadenze.set_PROGRESSIVO(const Value: Integer);
begin
  F_PROGRESSIVO := Value;
end;

function Tscadenze.read_PROGRESSIVO: Integer;
begin
  result := F_PROGRESSIVO;
end;

procedure Tscadenze.set_decorrenza(const Value: TdateTime);
begin
  F_decorrenza := Value;
end;

function Tscadenze.read_decorrenza: TdateTime;
begin
  result := F_decorrenza;
end;

procedure Tscadenze.set_scadenza(const Value: TdateTime);
begin
  F_scadenza := Value;
end;

function Tscadenze.read_scadenza: TdateTime;
begin
  result := F_scadenza;
end;

procedure Tscadenze.set_INVIO_AVV_SCADENZA(const Value: TdateTime);
begin
  F_INVIO_AVV_SCADENZA := Value;
end;

function Tscadenze.read_INVIO_AVV_SCADENZA: TdateTime;
begin
  result := F_INVIO_AVV_SCADENZA;
end;

procedure Tscadenze.set_INVIO_1_SOLLECITO(const Value: TdateTime);
begin
  F_INVIO_1_SOLLECITO := Value;
end;

function Tscadenze.read_INVIO_1_SOLLECITO: TdateTime;
begin
  result := F_INVIO_1_SOLLECITO;
end;

procedure Tscadenze.set_INVIO_2_SOLLECITO(const Value: TdateTime);
begin
  F_INVIO_2_SOLLECITO := Value;
end;

function Tscadenze.read_INVIO_2_SOLLECITO: TdateTime;
begin
  result := F_INVIO_2_SOLLECITO;
end;

procedure Tscadenze.set_TIPO_TITOLO(const Value: String);
begin
  F_TIPO_TITOLO := Value;
end;

function Tscadenze.read_TIPO_TITOLO: String;
begin
  result := F_TIPO_TITOLO;
end;

procedure Tscadenze.set_RIF_CLIENTE(const Value: Integer);
begin
  F_RIF_CLIENTE := Value;
end;

function Tscadenze.read_RIF_CLIENTE: Integer;
begin
  result := F_RIF_CLIENTE;
end;

procedure Tscadenze.set_DATA_COPERTURA(const Value: TdateTime);
begin
  F_DATA_COPERTURA := Value;
end;

function Tscadenze.read_DATA_COPERTURA: TdateTime;
begin
  result := F_DATA_COPERTURA;
end;

procedure Tscadenze.set_DATA_STAMPA(const Value: TdateTime);
begin
  F_DATA_STAMPA := Value;
end;

function Tscadenze.read_DATA_STAMPA: TdateTime;
begin
  result := F_DATA_STAMPA;
end;

procedure Tscadenze.set_P_NOTA_NUMERO(const Value: Integer);
begin
  F_P_NOTA_NUMERO := Value;
end;

function Tscadenze.read_P_NOTA_NUMERO: Integer;
begin
  result := F_P_NOTA_NUMERO;
end;

procedure Tscadenze.set_P_NOTA_DATA(const Value: TdateTime);
begin
  F_P_NOTA_DATA := Value;
end;

function Tscadenze.read_P_NOTA_DATA: TdateTime;
begin
  result := F_P_NOTA_DATA;
end;

procedure Tscadenze.set_DATA_REG_PAG(const Value: TdateTime);
begin
  F_DATA_REG_PAG := Value;
end;

function Tscadenze.read_DATA_REG_PAG: TdateTime;
begin
  result := F_DATA_REG_PAG;
end;

procedure Tscadenze.set_DATA_PAG_CCP(const Value: TdateTime);
begin
  F_DATA_PAG_CCP := Value;
end;

function Tscadenze.read_DATA_PAG_CCP: TdateTime;
begin
  result := F_DATA_PAG_CCP;
end;

procedure Tscadenze.set_INCASSATA_DA(const Value: Integer);
begin
  F_INCASSATA_DA := Value;
end;

function Tscadenze.read_INCASSATA_DA: Integer;
begin
  result := F_INCASSATA_DA;
end;

procedure Tscadenze.set_DATA_IMPORTAZIONE(const Value: TdateTime);
begin
  F_DATA_IMPORTAZIONE := Value;
end;

function Tscadenze.read_DATA_IMPORTAZIONE: TdateTime;
begin
  result := F_DATA_IMPORTAZIONE;
end;

procedure Tscadenze.set_CODICE_IMPORTAZIONE(const Value: Integer);
begin
  F_CODICE_IMPORTAZIONE := Value;
end;

function Tscadenze.read_CODICE_IMPORTAZIONE: Integer;
begin
  result := F_CODICE_IMPORTAZIONE;
end;

procedure Tscadenze.set_provvigioni_sub(const Value: currency);
begin
  F_provvigioni_sub := Value;
end;

function Tscadenze.read_provvigioni_sub: currency;
begin
  result := F_provvigioni_sub;
end;

procedure Tscadenze.set_COD_SUB_AGE(const Value: Integer);
begin
  F_COD_SUB_AGE := Value;
end;

function Tscadenze.read_COD_SUB_AGE: Integer;
begin
  result := F_COD_SUB_AGE;
end;

procedure Tscadenze.set_COD_FC_SUB_AGE(const Value: Integer);
begin
  F_COD_FC_SUB_AGE := Value;
end;

function Tscadenze.read_COD_FC_SUB_AGE: Integer;
begin
  result := F_COD_FC_SUB_AGE;
end;

procedure Tscadenze.set_DT_LAST_MOD(const Value: TdateTime);
begin
  F_DT_LAST_MOD := Value;
end;

function Tscadenze.read_DT_LAST_MOD: TdateTime;
begin
  result := F_DT_LAST_MOD;
end;

procedure Tscadenze.set_ID_TIT_CMP(const Value: String);
begin
  F_ID_TIT_CMP := Value;
end;

function Tscadenze.read_ID_TIT_CMP: String;
begin
  result := F_ID_TIT_CMP;
end;

procedure Tscadenze.set_ID_GEN_SLP_AGE(const Value: Integer);
begin
  F_ID_GEN_SLP_AGE := Value;
end;

function Tscadenze.read_ID_GEN_SLP_AGE: Integer;
begin
  result := F_ID_GEN_SLP_AGE;
end;

end.
