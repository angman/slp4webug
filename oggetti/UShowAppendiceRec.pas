unit UShowAppendiceRec;

interface

type
  TShowAppendiceRec = record
    CodPolizza: integer;
    NAppendice: integer;
    CodCliente: integer;
    DataAppendice: TDate;
    NPolizza: string;
    PTasse: Currency;
    Tasse: Currency;
    DtDalAppendice: TDate;
    DtAlAppendice: TDate;
    ImportoModificato: Boolean;
    bCanPrint: Boolean;
    Importo: Currency;
    NextRata: Currency;
    tipo_titolo: string[2];
    CodCompagnia: integer;
    denominazione: string[40];
    tipoApp: string[2];  // CI =cambio indirizzo
  end;

implementation

end.
