inherited MainForm: TMainForm
  ClientHeight = 162
  ClientWidth = 878
  Caption = 'SLP4Web'
  WindowState = wsMaximized
  Menu = UmnMainMenu
  OnAfterShow = UniFormAfterShow
  OnScreenResize = UniFormScreenResize
  ExplicitWidth = 894
  ExplicitHeight = 221
  PixelsPerInch = 96
  TextHeight = 13
  object pnlMenuButtons: TUniSimplePanel [0]
    Left = 0
    Top = 25
    Width = 878
    Height = 56
    Hint = ''
    ParentColor = False
    Align = alTop
    AlignmentControl = uniAlignmentClient
    ParentAlignmentControl = False
    TabOrder = 1
    Layout = 'hbox'
    LayoutAttribs.Pack = 'start'
    LayoutAttribs.Padding = '3'
    LayoutAttribs.Columns = 7
    object btnClienti: TUniSpeedButton
      Left = 21
      Top = 6
      Width = 106
      Height = 43
      Action = actClienti
      ParentFont = False
      Font.Style = [fsBold]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 1
    end
    object btnUscita: TUniSpeedButton
      Left = 128
      Top = 6
      Width = 106
      Height = 43
      Action = actContrattiInVigore
      ParentFont = False
      Font.Style = [fsBold]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 2
    end
    object btnStampaPolizze: TUniSpeedButton
      Left = 235
      Top = 6
      Width = 106
      Height = 43
      Action = actStampaPolizza
      ParentFont = False
      Font.Style = [fsBold]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 3
    end
    object btnPerfezionaPolizza: TUniSpeedButton
      Left = 342
      Top = 6
      Width = 106
      Height = 43
      Action = actPerfezionaPolizza
      ParentFont = False
      Font.Style = [fsBold]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 4
    end
    object btnTitoli: TUniSpeedButton
      Left = 449
      Top = 6
      Width = 106
      Height = 43
      Action = actTitoli
      ParentFont = False
      Font.Style = [fsBold]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 5
    end
    object btnFogliCassa: TUniSpeedButton
      Left = 556
      Top = 6
      Width = 106
      Height = 43
      Action = actFogliCassa
      ParentFont = False
      Font.Style = [fsBold]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 6
    end
    object btnEsci: TUniSpeedButton
      Left = 663
      Top = 6
      Width = 106
      Height = 43
      Action = actUscita
      ParentFont = False
      Font.Color = clRed
      Font.Style = [fsBold, fsUnderline]
      ParentColor = False
      Color = clWindow
      LayoutConfig.Flex = 1
      TabOrder = 7
    end
  end
  inherited pnlHead: TUniSimplePanel
    Width = 878
    ExplicitWidth = 878
  end
  object pnlGlobal: TUniSimplePanel
    Left = 0
    Top = 81
    Width = 878
    Height = 81
    Hint = ''
    ParentColor = False
    Align = alClient
    TabOrder = 2
  end
  object UmnMainMenu: TUniMainMenu
    Left = 776
    Top = 40
    object mnuFile: TUniMenuItem
      Caption = 'File'
    end
    object mnuArchivi: TUniMenuItem
      Caption = 'Archivi'
      object mnuClienti: TUniMenuItem
        Action = actClienti
      end
      object mnuMandanti: TUniMenuItem
        Action = actMandanti
      end
      object mnuAgentDipCollab: TUniMenuItem
        Action = actAgentDipCollab
      end
      object mnuDatiAgenzia: TUniMenuItem
        Action = actDatiAgenzia
      end
    end
    object mnuContratti: TUniMenuItem
      Caption = 'Contratti'
      object mnuContrattiInVigore: TUniMenuItem
        Action = actContrattiInVigore
      end
    end
    object mnuTitFogliCassa: TUniMenuItem
      Caption = 'Titoli / fogli cassa'
      object mnuTitoli: TUniMenuItem
        Action = actTitoli
      end
      object mnuFogliCassa: TUniMenuItem
        Action = actFogliCassa
      end
      object mnuEstremiIncassi: TUniMenuItem
        Action = actEstremiIncassi
      end
      object mnuStampaQuietanziamenti: TUniMenuItem
        Action = actStampaQuietanziamenti
      end
      object mnuN1: TUniMenuItem
        Caption = '-'
      end
      object mnuSimulaQuietanziamento: TUniMenuItem
        Action = actSimulaQuietanzamenti
      end
      object mnuCalcolaQuietanziamentoSLP: TUniMenuItem
        Action = actCalcolaQuietanziamentoSLP
        Visible = False
      end
      object mnuCalcolaQuietanziamentoNoSLP: TUniMenuItem
        Action = actCalcolaQuietanziamentoNoSLP
      end
    end
    object mnuDocumenti: TUniMenuItem
      Caption = 'Documenti / Modulistica'
      object mnuPolizzeDisponibili: TUniMenuItem
        Action = actPolizzeDisponibili
      end
      object mnuInviatiDaSLP: TUniMenuItem
        Action = actInviatiDaSLP
      end
      object mnuPrintHouse: TUniMenuItem
        Action = actPrintHouse
        Visible = False
      end
    end
    object mnuServizi: TUniMenuItem
      Caption = 'Servizi'
      object mnuUtenti: TUniMenuItem
        Action = actUtenti
      end
      object Stampe1: TUniMenuItem
        Action = actStampe
        Caption = 'Stampe / Statistiche'
      end
    end
    object mnuSLP: TUniMenuItem
      Caption = 'SLP'
      object mnuMessaggi: TUniMenuItem
        Action = actMessaggi
      end
    end
    object mnuUscita: TUniMenuItem
      Action = actUscita
    end
  end
  object actlstOperazioni: TActionList
    Left = 824
    Top = 40
    object actUscita: TAction
      Category = 'Uscita'
      Caption = '&Uscita'
      OnExecute = actUscitaExecute
    end
    object actClienti: TAction
      Category = 'Archivi'
      Caption = '&Clienti'
      OnExecute = actClientiExecute
    end
    object actMandanti: TAction
      Category = 'Archivi'
      Caption = 'Mandanti'
      OnExecute = actMandantiExecute
    end
    object actAgentDipCollab: TAction
      Category = 'Archivi'
      Caption = 'Agenti dipendenti collaboratori'
      OnExecute = actAgentDipCollabExecute
    end
    object actDatiAgenzia: TAction
      Category = 'Archivi'
      Caption = 'Dati agenzia'
      OnExecute = actDatiAgenziaExecute
    end
    object actContrattiInVigore: TAction
      Category = 'Contratti'
      Caption = 'Contratti in &vigore'
      OnExecute = actContrattiInVigoreExecute
    end
    object actTitoli: TAction
      Category = 'TitoliFogliCassa'
      Caption = '&Titoli'
      OnExecute = actTitoliExecute
    end
    object actFogliCassa: TAction
      Category = 'TitoliFogliCassa'
      Caption = '&Fogli cassa'
      OnExecute = actFogliCassaExecute
    end
    object actEstremiIncassi: TAction
      Category = 'TitoliFogliCassa'
      Caption = 'Storico Incassi'
      OnExecute = actEstremiIncassiExecute
    end
    object actPolizzeDisponibili: TAction
      Category = 'Documenti'
      Caption = 'Polizze disponibili'
      OnExecute = actPolizzeDisponibiliExecute
    end
    object actInviatiDaSLP: TAction
      Category = 'Documenti'
      Caption = 'Documenti inviati da SLP'
      OnExecute = actInviatiDaSLPExecute
    end
    object actPrintHouse: TAction
      Category = 'Documenti'
      Caption = 'Print house'
      OnExecute = actPrintHouseExecute
    end
    object actUtenti: TAction
      Category = 'Servizi'
      Caption = 'Utenti'
      OnExecute = actUtentiExecute
    end
    object actStampaPolizza: TAction
      Category = 'Polizze'
      Caption = 'P&repara polizza'
      OnExecute = actStampaPolizzaExecute
    end
    object actPerfezionaPolizza: TAction
      Category = 'Polizze'
      Caption = '&Perf. Polizza'
      OnExecute = actPerfezionaPolizzaExecute
    end
    object actInviatiDaSLPOnLogin: TAction
      Category = 'Documenti'
      Caption = 'actInviatiDaSLPOnLogin'
      OnExecute = actInviatiDaSLPOnLoginExecute
    end
    object actStampe: TAction
      Category = 'Servizi'
      Caption = 'Stampe Statistiche'
      OnExecute = actStampeExecute
    end
    object actMessaggi: TAction
      Category = 'Messaggi'
      Caption = 'Messaggi'
      OnExecute = actMessaggiExecute
    end
    object actStampaQuietanzamenti: TAction
      Category = 'TitoliFogliCassa'
      Caption = 'Stampa quietanzamenti SLP'
    end
    object actStampaQuietanziamenti: TAction
      Category = 'Documenti'
      Caption = 'Stampa Quietanziamenti SLP'
      OnExecute = actStampaQuietanziamentiExecute
    end
    object actCalcolaQuietanziamentoSLP: TAction
      Category = 'Quietanziamento'
      Caption = 'Calcola Quietanziamento SLP'
      OnExecute = actCalcolaQuietanziamentoSLPExecute
    end
    object actSimulaQuietanzamenti: TAction
      Category = 'Quietanziamento'
      Caption = 'Simula quietanzamenti'
      OnExecute = actSimulaQuietanzamentiExecute
    end
    object actCalcolaQuietanziamentoNoSLP: TAction
      Category = 'Quietanziamento'
      Caption = 'Calcola quietanziamento non SLP'
      OnExecute = actCalcolaQuietanziamentoNoSLPExecute
    end
  end
end
