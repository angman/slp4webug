object UniServerModule: TUniServerModule
  OldCreateOrder = False
  TempFolder = 'temp\'
  Title = 'New Application'
  SuppressErrors = []
  Bindings = <>
  ServerMessages.UnavailableErrMsg = 'Errore di comunicazione'
  ServerMessages.LoadingMessage = 'Caricamento ...'
  ServerMessages.ExceptionTemplate.Strings = (
    '<html>'
    '<body bgcolor="#dfe8f6">'
    
      '<p style="text-align:center;color:#A05050">An Exception has occu' +
      'red in application:</p>'
    '<p style="text-align:center;color:#0000A0">[###message###]</p>'
    
      '<p style="text-align:center;color:#A05050"><a href="[###url###]"' +
      '>Rilancia l'#39'applicazione</a></p>'
    '</body>'
    '</html>')
  ServerMessages.InvalidSessionTemplate.Strings = (
    '<html>'
    '<body bgcolor="#dfe8f6">'
    '<p style="text-align:center;color:#0000A0">[###message###]</p>'
    
      '<p style="text-align:center;color:#A05050"><a href="[###url###]"' +
      '>Riavvia l'#39'applicazione</a></p>'
    '</body>'
    '</html>')
  ServerMessages.TerminateTemplate.Strings = (
    '<html>'
    '<body bgcolor="#dfe8f6">'
    
      '<p style="text-align:center;color:red;font-size:40px">[###messag' +
      'e###]</p>'
    
      '<p style="text-align:center;color:#A05050"><a href="[###url###]"' +
      '>Riavvia l'#39'applicazione</a></p>'
    '</body>'
    '</html>')
  ServerMessages.InvalidSessionMessage = 'Sessione invalida o timeout'
  ServerMessages.TerminateMessage = 'Sessione web terminata'
  ServerLimits.MaxSessions = 350
  ServerLimits.MaxRequests = 100
  ServerLimits.MaxConnections = 700
  SSL.SSLOptions.RootCertFile = 'root.pem'
  SSL.SSLOptions.CertFile = 'cert.pem'
  SSL.SSLOptions.KeyFile = 'key.pem'
  SSL.SSLOptions.Method = sslvTLSv1_2
  SSL.SSLOptions.SSLVersions = [sslvTLSv1_2]
  SSL.SSLOptions.Mode = sslmUnassigned
  SSL.SSLOptions.VerifyMode = []
  SSL.SSLOptions.VerifyDepth = 0
  ConnectionFailureRecovery.ErrorMessage = 'Errore di connessione'
  ConnectionFailureRecovery.RetryMessage = 'Riconnessione ...'
  OnBeforeInit = UniGUIServerModuleBeforeInit
  OnException = UniGUIServerModuleException
  Height = 150
  Width = 215
end
