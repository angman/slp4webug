unit UMasterElenco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, System.Actions, Vcl.ActnList, Vcl.Menus, uniMainMenu, uniLabel,
  uniButton, uniBitBtn, uniSpeedButton, uniGUIBaseClasses, uniPanel, uniBasicGrid, uniDBGrid,
  Data.DB, UMasterEdit, uniCheckBox, uniEdit, uniMultiItem, uniComboBox, UQueryElencoIntf, UDMMaster,
  UDMdatiAgeClienti, UMasterPage, Main;

type

  EFormEditClassNotFound = Exception;

  TEditPageClass = class of TFMasterEdit;

  TOnSelectedItem = procedure(ASelectedItem: Integer; const ASelectedDescription: string) of object;

  TFMasterElenco = class(TFMasterPage)
    pnlOperationButtons: TUniSimplePanel;
    pnlButtonContainer: TUniSimplePanel;
    btnInsert: TUniSpeedButton;
    btnModify: TUniSpeedButton;
    btnChiudi: TUniSpeedButton;
    btnSeleziona: TUniSpeedButton;
    actInsert: TAction;
    actModify: TAction;
    actChiudi: TAction;
    actSelezionaDaElenco: TAction;
    pnlGlobal: TUniSimplePanel;
    dsGrid: TDataSource;
    pnlGrid: TUniContainerPanel;
    grdElenco: TUniDBGrid;
    pnlhdrSearch: TUniHiddenPanel;
    actlstOperazioni: TActionList;
    UniSimplePanel1: TUniSimplePanel;
    lblPosizione: TUniLabel;
    btnVisualizza: TUniSpeedButton;
    procedure actInsertExecute(Sender: TObject);
    procedure uncbxApplyFilterChange(Sender: TObject);
    procedure cbbTipoChangeValue(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
    procedure grdElencoClearFilters(Sender: TObject);
    procedure grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn; const Value: Variant);
    procedure actModifyExecute(Sender: TObject);
    procedure grdElencoBodyDblClick(Sender: TObject);
    procedure actUscitaExecute(Sender: TObject);
    procedure actChiudiExecute(Sender: TObject);
    procedure actSelezionaDaElencoExecute(Sender: TObject);
  private
    { Private declarations }
    FEditPageClass: TEditPageClass;
    FDMMaster: IQueryElenco;
    FEditPageClassName: string;
    FSelectedCode: Integer;
    FSelectedDescription: string;
    FOnSelectedItem: TOnSelectedItem;

    procedure CreateAndShowEditForm;
    procedure setEditPageClassName(const Value: string);
    procedure SetOnSelectedItem(const Value: TOnSelectedItem);
  protected
    property EditPageClass: TEditPageClass read FEditPageClass;
    property SelectedCode: Integer read FSelectedCode write FSelectedCode;
    property SelectedDescription: string read FSelectedDescription write FSelectedDescription;

  public
    { Public declarations }
    constructor Create(AOwner: TComponent; ADataModule: TDMMaster); overload;
    function ImpostaParent(AParent: TUniControl): TFMasterElenco;

    property DMMaster: IQueryElenco read FDMMaster write FDMMaster;
    property EditPageClassName: string read FEditPageClassName write setEditPageClassName;
    property OnSelectedItem: TOnSelectedItem read FOnSelectedItem write SetOnSelectedItem;

  end;

function FMasterElenco: TFMasterElenco;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, dbisamtb, ServerModule, UClienti, UQueryEditIntf, UCodiciErroriPolizza,
  System.StrUtils;

function FMasterElenco: TFMasterElenco;
begin
  Result := TFMasterElenco(UniMainModule.GetFormInstance(TFMasterElenco));
end;

function TFMasterElenco.ImpostaParent(AParent: TUniControl): TFMasterElenco;
begin
  Parent := AParent;
  Result := Self;
end;

procedure TFMasterElenco.actChiudiExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFMasterElenco.actInsertExecute(Sender: TObject);
begin
  UniMainModule.operazione := 'INS';
  CreateAndShowEditForm;

end;

procedure TFMasterElenco.actModifyExecute(Sender: TObject);
begin
  if Assigned(FEditPageClass) then
  begin
    UniMainModule.operazione := 'MOD';
    (DMMaster as IQueryEdit).PosizionaQuery(FSelectedCode);
    CreateAndShowEditForm;
  end;
end;

procedure TFMasterElenco.actSelezionaDaElencoExecute(Sender: TObject);
begin
  if Assigned(FOnSelectedItem) then
    FOnSelectedItem(SelectedCode, FSelectedDescription);
  ModalResult := mrOk;
end;

procedure TFMasterElenco.actUscitaExecute(Sender: TObject);
begin
  if Assigned(OnSelectedItem) then
    ModalResult := mrCancel
  else
    inherited;
end;

procedure TFMasterElenco.cbbTipoChangeValue(Sender: TObject);
begin
  // DMMaster.EseguiQuery(CbbOrdinamento.ItemIndex, CbbTipo.ItemIndex);
end;

constructor TFMasterElenco.Create(AOwner: TComponent; ADataModule: TDMMaster);
begin
  assert((ADataModule <> nil) and (ADataModule is TDMMaster));
  FDMMaster := (ADataModule as IQueryElenco);

end;

procedure TFMasterElenco.CreateAndShowEditForm;
begin
  with FEditPageClass.Create(UniApplication) do
  begin
    try
      FDMMaster := Self.FDMMaster;
      DSoEdit.DataSet.Edit;
      DatasetElenco := grdElenco.DataSource.DataSet;
      ShowModal;
    except
      on e: Exception do
        if EDBISAMEngineError(e).ErrorCode = 10258 then
        begin
          ShowMessage(MSG_RECORD_ALREADY_LOCKED);
          Close;
        end;

    end;
  end;

end;

procedure TFMasterElenco.grdElencoBodyDblClick(Sender: TObject);
begin
  if Assigned(FOnSelectedItem) then
    actSelezionaDaElenco.Execute
  else
    actModify.Execute;
end;

procedure TFMasterElenco.grdElencoClearFilters(Sender: TObject);
begin
  inherited;
  dsGrid.DataSet.DisableControls;
  try
    dsGrid.DataSet.Filtered := False;
    dsGrid.DataSet.Filter   := '';
  finally
    dsGrid.DataSet.EnableControls
  end;

end;

procedure TFMasterElenco.grdElencoColumnFilter(Sender: TUniDBGrid; const Column: TUniDBGridColumn;
  const Value: Variant);

  function getColumnName(FieldName: string): string;
  begin
    if grdElenco.DataSource.DataSet.FieldByName(FieldName).FieldKind = fkLookup then
      Result := grdElenco.DataSource.DataSet.FieldByName(FieldName).KeyFields
    else
      Result := FieldName;
  end;

begin
  inherited;
  dsGrid.DataSet.DisableControls;
  try
    dsGrid.DataSet.Filtered      := False;
    dsGrid.DataSet.FilterOptions := [foCaseInsensitive];
    if VarIsNumeric(Value) then
    begin
      if Value > -1 then
      begin
        dsGrid.DataSet.Filter   := getColumnName(Column.FieldName) + ' LIKE ''' + VarToStr(Value) + '%''';
        dsGrid.DataSet.Filtered := True;
      end;
    end
    else
    begin
      dsGrid.DataSet.Filter   := getColumnName(Column.FieldName) + ' LIKE ''' + Value + '%''';
      dsGrid.DataSet.Filtered := Value <> '';
    end;

  finally
    dsGrid.DataSet.EnableControls
  end;

end;

procedure TFMasterElenco.grdElencoColumnSort(Column: TUniDBGridColumn; Direction: Boolean);
begin
  inherited;
  FDMMaster.EseguiQuery(Column.FieldName, 0, '', 0, Integer(Direction));
end;

procedure TFMasterElenco.setEditPageClassName(const Value: string);
begin
  FEditPageClassName := Value;
  FEditPageClass     := TEditPageClass(findClass(FEditPageClassName));
  if FEditPageClass = nil then
    raise EFormEditClassNotFound.Create('Class ' + FEditPageClassName + ' Non trovata');
end;

procedure TFMasterElenco.SetOnSelectedItem(const Value: TOnSelectedItem);
begin
  FOnSelectedItem              := Value;
  actSelezionaDaElenco.Visible := Assigned(Value);
  btnSeleziona.Visible         := actSelezionaDaElenco.Visible;
  // actInsert.Visible  := False;
  // actModify.Visible := False;
end;

procedure TFMasterElenco.uncbxApplyFilterChange(Sender: TObject);
begin
  // if not uncbxApplyFilter.checked then
  // unedtSearch.Text := '';
  // FDMMaster.EseguiQuery(cbbOrdinamento.ItemIndex, cbbTipo.ItemIndex, Trim(unedtSearch.Text));

end;

procedure TFMasterElenco.UniFormCreate(Sender: TObject);
begin
  inherited;
  // CbbOrdinamento.ItemIndex := 0;
  // CbbTipo.ItemIndex        := 0;
  lblPosizione.Caption := lblPosizione.Caption + UniMainModule.posizioneMenu;

  btnInsert.Enabled := UniMainModule.VerificaAutorizzazione(UniMainModule.archivio, 'INS');
  btnModify.Enabled := UniMainModule.VerificaAutorizzazione(UniMainModule.archivio, 'MOD');

end;

end.
