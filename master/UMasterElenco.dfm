inherited FMasterElenco: TFMasterElenco
  ClientHeight = 444
  ClientWidth = 935
  Caption = 'FMasterElenco'
  ExplicitWidth = 951
  ExplicitHeight = 483
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlHead: TUniSimplePanel
    Width = 935
    ExplicitWidth = 935
  end
  object pnlGlobal: TUniSimplePanel
    Left = 0
    Top = 25
    Width = 935
    Height = 419
    Hint = ''
    ParentColor = False
    Align = alClient
    AlignmentControl = uniAlignmentClient
    ParentAlignmentControl = False
    TabOrder = 1
    Layout = 'vbox'
    object pnlOperationButtons: TUniSimplePanel
      Left = 0
      Top = 0
      Width = 935
      Height = 60
      Hint = ''
      ParentColor = False
      Align = alTop
      ParentAlignmentControl = False
      TabOrder = 0
      LayoutConfig.Flex = 1
      LayoutConfig.Height = '8%'
      object pnlButtonContainer: TUniSimplePanel
        Left = 0
        Top = 20
        Width = 935
        Height = 40
        Hint = ''
        Constraints.MaxWidth = 590
        Constraints.MinWidth = 295
        ParentColor = False
        Align = alClient
        AlignmentControl = uniAlignmentClient
        ParentAlignmentControl = False
        TabOrder = 1
        Layout = 'hbox'
        object btnInsert: TUniSpeedButton
          Left = 10
          Top = 0
          Width = 106
          Height = 43
          Action = actInsert
          ParentFont = False
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 1
        end
        object btnModify: TUniSpeedButton
          Left = 122
          Top = 0
          Width = 106
          Height = 43
          Action = actModify
          ParentFont = False
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 2
        end
        object btnVisualizza: TUniSpeedButton
          Left = 234
          Top = 0
          Width = 106
          Height = 43
          Hint = ''
          Visible = False
          Caption = 'Visualizza'
          ParentFont = False
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 5
        end
        object btnSeleziona: TUniSpeedButton
          Left = 346
          Top = 0
          Width = 106
          Height = 43
          Visible = False
          Action = actSelezionaDaElenco
          ParentFont = False
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 4
        end
        object btnChiudi: TUniSpeedButton
          Left = 458
          Top = 0
          Width = 106
          Height = 43
          Action = actChiudi
          ParentFont = False
          Font.Color = clRed
          Font.Style = [fsBold, fsUnderline]
          ParentColor = False
          Color = clWindow
          LayoutConfig.Flex = 1
          TabOrder = 0
        end
      end
      object UniSimplePanel1: TUniSimplePanel
        Left = 0
        Top = 0
        Width = 935
        Height = 20
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 2
        object lblPosizione: TUniLabel
          Left = 6
          Top = 4
          Width = 95
          Height = 13
          Hint = ''
          Caption = 'Ti trovi nell'#39'area:'
          ParentFont = False
          Font.Style = [fsBold]
          TabOrder = 1
        end
      end
    end
    object pnlGrid: TUniContainerPanel
      Left = 0
      Top = 60
      Width = 935
      Height = 359
      Hint = ''
      ParentColor = False
      Align = alClient
      TabOrder = 2
      LayoutConfig.Flex = 2
      LayoutConfig.Height = '92%'
      object grdElenco: TUniDBGrid
        Left = 0
        Top = 0
        Width = 935
        Height = 359
        Hint = ''
        DataSource = dsGrid
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgFilterClearButton, dgCancelOnExit, dgAutoRefreshRow, dgDontShowSelected, dgRowNumbers]
        ReadOnly = True
        LoadMask.Message = 'Loading data...'
        Align = alClient
        ParentFont = False
        TabOrder = 0
        OnBodyDblClick = grdElencoBodyDblClick
        OnColumnSort = grdElencoColumnSort
        OnClearFilters = grdElencoClearFilters
        OnColumnFilter = grdElencoColumnFilter
      end
      object pnlhdrSearch: TUniHiddenPanel
        Left = 152
        Top = 6
        Width = 217
        Height = 256
        Hint = ''
        Visible = True
      end
    end
  end
  object actlstOperazioni: TActionList
    Left = 616
    Top = 136
    object actInsert: TAction
      Category = 'ItemOperation'
      Caption = 'Inserisci'
      OnExecute = actInsertExecute
    end
    object actModify: TAction
      Category = 'ItemOperation'
      Caption = 'Modifica'
      OnExecute = actModifyExecute
    end
    object actChiudi: TAction
      Category = 'ItemOperation'
      Caption = 'Chiudi'
      OnExecute = actChiudiExecute
    end
    object actSelezionaDaElenco: TAction
      Category = 'ItemOperation'
      Caption = 'Seleziona'
      OnExecute = actSelezionaDaElencoExecute
    end
  end
  object dsGrid: TDataSource
    Left = 656
    Top = 240
  end
end
