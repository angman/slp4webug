unit UMasterEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, UMasterPage, uniLabel, uniGUIBaseClasses, uniPanel, System.Actions,
  Vcl.ActnList, Data.DB, uniButton, uniBitBtn, uniSpeedButton, uniBasicGrid, uniDBGrid, System.Rtti,
  uniImageList;

type
  TFMasterEdit = class(TFMasterPage)
    pnlEditButtons: TUniSimplePanel;
    btnConferma: TUniSpeedButton;
    btnAnnulla: TUniSpeedButton;
    actlstEditOperation: TActionList;
    dsoEdit: TDataSource;
    actConferma: TAction;
    actAnnulla: TAction;
    untvmglstIcons: TUniNativeImageList;
    procedure UniFormCreate(Sender: TObject);
    procedure actConfermaExecute(Sender: TObject);
    procedure actAnnullaExecute(Sender: TObject);
    procedure dsoEditStateChange(Sender: TObject);
  private
    FDatasetElenco: TDataset;
    FRowSelect: TValue;
    { Private declarations }
  protected
    procedure SetAllControlsReadOnly(AContainer: TUniContainer);

  public
    { Public declarations }
    property DatasetElenco: TDataset read FDatasetElenco write FDatasetElenco;
    property RowSelect: TValue read FRowSelect write FRowSelect;

  end;

function FMasterEdit: TFMasterEdit;

implementation

{$R *.dfm}

uses
  uniGUIApplication, MainModule, uniGUIMainModule, UDMMaster;

function FMasterEdit: TFMasterEdit;
begin
  Result := TFMasterEdit(UniMainModule.GetFormInstance(TFMasterEdit));
end;

procedure TFMasterEdit.actAnnullaExecute(Sender: TObject);
begin
  if dsoEdit.DataSet.State in [dsInsert, DSEdit] then
    dsoEdit.DataSet.Cancel;
  ModalResult := mrCancel;
end;

procedure TFMasterEdit.actConfermaExecute(Sender: TObject);
begin
  try
    if dsoEdit.DataSet.State in [dsInsert, DSEdit] then
    begin
      if dsoEdit.DataSet.FindField('DT_LAST_MOD') <> nil then
        dsoEdit.DataSet.FieldByName('DT_LAST_MOD').AsDateTime := Now;

      if Assigned(FDatasetElenco) then
        TDMMaster(FDatasetElenco.Owner).PosizionaQueryElenco(FDatasetElenco, dsoEdit.DataSet)
      else
        dsoEdit.dataset.Post;
    end;
    ModalResult := mrOk;
  except
    on E: Exception do
      raise;
  end;

end;

procedure TFMasterEdit.dsoEditStateChange(Sender: TObject);
begin
  if dsoEdit.DataSet <> nil then
    btnConferma.Enabled := dsoEdit.DataSet.State in [dsInsert, DSEdit];
end;

procedure TFMasterEdit.SetAllControlsReadOnly(AContainer: TUniContainer);
var
  i: Integer;

begin
  for i := 0 to AContainer.ControlCount - 1 do
  begin
    if (AContainer.Controls[i].InheritsFrom(TUniContainer)) then
      SetAllControlsReadOnly(AContainer.Controls[i] as TUniContainer)
    else
      if AContainer.Controls[i].InheritsFrom(TUniBitBtn) then
        (AContainer.Controls[i] as TUniBitBtn).Enabled := False
      else
        if AContainer.Controls[i].InheritsFrom(TUniSpeedButton) then
          (AContainer.Controls[i] as TUniSpeedButton).Enabled := False
        else
          if AContainer.Controls[i].InheritsFrom(TUniFormControl) then
          begin
            TUniFormControl(AContainer.Controls[i]).ReadOnly := True;
            TUniFormControl(AContainer.Controls[i]).Color := clYellow;
          end;
  end;
end;

procedure TFMasterEdit.UniFormCreate(Sender: TObject);
begin
  inherited;
  if UniMainModule.operazione = 'INS' then
  begin
    if dsoEdit.DataSet.State = dsInactive then
      dsoEdit.DataSet.Open;
    dsoEdit.DataSet.Append;
  end;

end;

end.
