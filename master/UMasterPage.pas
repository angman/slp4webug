unit UMasterPage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniPanel, uniLabel, uniGUIBaseClasses;

type
  TFMasterPage = class(TUniForm)
    pnlHead: TUniSimplePanel;
    unlblAgenzia: TUniLabel;
    unlblCitta: TUniLabel;
    unlblNomeAgenzia: TUniLabel;
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses
  uniGUIApplication, MainModule;

{$R *.dfm}

procedure TFMasterPage.UniFormCreate(Sender: TObject);
begin
  unlblAgenzia.Caption     := 'Codice: ' + UniMainModule.DMdatiAge.SLPdati_age.agenzia;
  unlblNomeAgenzia.Caption := 'Agenzia: ' + UniMainModule.DMdatiAge.SLPdati_age.nome + ' - Utente:' +
    UniMainModule.DMdatiAge.SLPdati_age.utente;
  unlblCitta.Caption := 'Citt�: ' + UniMainModule.DMdatiAge.SLPdati_age.citta;

end;

end.
