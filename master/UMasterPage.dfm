object FMasterPage: TFMasterPage
  Left = 0
  Top = 0
  ClientHeight = 242
  ClientWidth = 912
  Caption = 'FMasterPage'
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pnlHead: TUniSimplePanel
    Left = 0
    Top = 0
    Width = 912
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    Layout = 'column'
    object unlblAgenzia: TUniLabel
      Left = 21
      Top = 6
      Width = 72
      Height = 13
      Hint = ''
      Caption = 'unlblAgenzia'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 1
    end
    object unlblCitta: TUniLabel
      Left = 112
      Top = 6
      Width = 54
      Height = 13
      Hint = ''
      Caption = 'unlblCitta'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 2
    end
    object unlblNomeAgenzia: TUniLabel
      Left = 241
      Top = 6
      Width = 104
      Height = 13
      Hint = ''
      Caption = 'unlblNomeAgenzia'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 3
    end
  end
end
