unit UFraGaranziePP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniSplitter, uniGUIBaseClasses, uniPanel, uniMultiItem, uniComboBox,
  uniDBComboBox, uniDateTimePicker, uniDBDateTimePicker, uniDBEdit, uniEdit, uniCheckBox,
  uniDBCheckBox, Data.DB, uniGroupBox, uniListBox, uniDBListBox;

type
  TFraGaranziePP = class(TUniFrame)
    pnlGaranzie: TUniPanel;
    dbedtNPolizza: TUniDBEdit;
    dbedtCompagniaEmittente: TUniDBEdit;
    dbedtAgenzia: TUniDBEdit;
    dbedtPremioAnnuoLordo: TUniDBNumberEdit;
    dtpckDecorrenza: TUniDBDateTimePicker;
    dtpckScadenza: TUniDBDateTimePicker;
    ckbx_A: TUniDBCheckBox;
    ckbx_B: TUniDBCheckBox;
    ckbx_C: TUniDBCheckBox;
    dbedtPremioNettoSLP: TUniDBNumberEdit;
    cbbMassimale: TUniDBComboBox;
    ungrpbxGaranzia: TUniGroupBox;
    lstSelectedGaranzie: TUniDBListBox;
    procedure ckbx_AClick(Sender: TObject);
    procedure lstSelectedGaranzieChange(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
{ TFraGaranziePP }

procedure TFraGaranziePP.ckbx_AClick(Sender: TObject);
begin
  lstSelectedGaranzie.Enabled := ckbx_A.Checked or ckbx_B.Checked or ckbx_C.Checked;
end;

procedure TFraGaranziePP.lstSelectedGaranzieChange(Sender: TObject);
begin
  // Place holder
end;

end.
