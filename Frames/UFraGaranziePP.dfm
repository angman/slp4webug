object FraGaranziePP: TFraGaranziePP
  Left = 0
  Top = 0
  Width = 798
  Height = 151
  TabOrder = 0
  object pnlGaranzie: TUniPanel
    Left = 0
    Top = 0
    Width = 798
    Height = 151
    Hint = ''
    Align = alClient
    TabOrder = 0
    BorderStyle = ubsFrameRaised
    Caption = ''
    object ungrpbxGaranzia: TUniGroupBox
      Left = 8
      Top = 3
      Width = 787
      Height = 129
      Hint = ''
      Caption = 'Polizza'
      TabOrder = 1
      object cbbMassimale: TUniDBComboBox
        Left = 632
        Top = 27
        Width = 145
        Hint = ''
        Items.Strings = (
          '2.500'
          '5.000'
          '7.500'
          '10.000'
          '12.500'
          '15.000')
        ItemIndex = 0
        ParentFont = False
        TabOrder = 10
        FieldLabel = 'Massimale scelto'
        FieldLabelAlign = laTop
        IconItems = <>
      end
      object ckbx_A: TUniDBCheckBox
        Left = 576
        Top = 33
        Width = 33
        Height = 17
        Hint = ''
        ValueChecked = 'true'
        ValueUnchecked = 'false'
        Caption = 'A'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 7
        ParentColor = False
        Color = clBtnFace
        OnClick = ckbx_AClick
      end
      object ckbx_B: TUniDBCheckBox
        Left = 576
        Top = 56
        Width = 33
        Height = 17
        Hint = ''
        ValueChecked = 'true'
        ValueUnchecked = 'false'
        Caption = 'B'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 8
        ParentColor = False
        Color = clBtnFace
        OnClick = ckbx_AClick
      end
      object ckbx_C: TUniDBCheckBox
        Left = 576
        Top = 79
        Width = 33
        Height = 17
        Hint = ''
        ValueChecked = 'true'
        ValueUnchecked = 'false'
        Caption = 'C'
        ParentFont = False
        Font.Style = [fsBold]
        TabOrder = 9
        ParentColor = False
        Color = clBtnFace
        OnClick = ckbx_AClick
      end
      object dbedtAgenzia: TUniDBEdit
        Left = 16
        Top = 44
        Width = 241
        Height = 22
        Hint = ''
        CharCase = ecUpperCase
        TabOrder = 3
        FieldLabel = 'Agenzia di'
      end
      object dbedtCompagniaEmittente: TUniDBEdit
        Left = 304
        Top = 18
        Width = 241
        Height = 22
        Hint = ''
        CharCase = ecUpperCase
        TabOrder = 2
        FieldLabel = 'Emessa da Comp'
      end
      object dbedtNPolizza: TUniDBEdit
        Left = 16
        Top = 16
        Width = 241
        Height = 22
        Hint = ''
        CharCase = ecUpperCase
        ParentFont = False
        TabOrder = 1
        FieldLabel = 'Polizza N.'
      end
      object dbedtPremioAnnuoLordo: TUniDBNumberEdit
        Left = 304
        Top = 42
        Width = 241
        Height = 22
        Hint = ''
        ParentFont = False
        TabOrder = 4
        FieldLabel = 'Premio annuo lordo'
        DecimalSeparator = ','
      end
      object dbedtPremioNettoSLP: TUniDBNumberEdit
        Left = 632
        Top = 74
        Width = 145
        Height = 22
        Hint = ''
        ParentFont = False
        TabOrder = 11
        FieldLabel = 'Premio netto SLP'
        FieldLabelAlign = laTop
        DecimalSeparator = ','
      end
      object dtpckDecorrenza: TUniDBDateTimePicker
        Left = 16
        Top = 70
        Width = 241
        Hint = ''
        DateTime = 43874.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 5
        FieldLabel = 'Decorrenza'
      end
      object dtpckScadenza: TUniDBDateTimePicker
        Left = 304
        Top = 70
        Width = 241
        Hint = ''
        DateTime = 43874.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 6
        FieldLabel = 'Scadenza'
      end
      object lstSelectedGaranzie: TUniDBListBox
        Left = 16
        Top = 96
        Width = 241
        Height = 27
        Hint = ''
        Enabled = False
        Items.Strings = (
          'Infortuni'
          'Malattia'
          'Abitazione'
          'Globale fabbricati'
          'Furto / incendio '
          'Rct / Rco'
          'RC diversi')
        TabOrder = 12
        MultiSelect = True
        ShowCheckBoxes = True
        SelectionMode = smMulti
        OnChange = lstSelectedGaranzieChange
      end
    end
  end
end
