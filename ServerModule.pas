unit ServerModule;

interface

uses
  Classes, SysUtils, uniGUIServer, uniGUIMainModule, uniGUIApplication, uIdCustomHTTPServer,
  uniGUITypes, uniGUIBaseClasses, uniGUIClasses, uniImageList, Vcl.ImgList, Vcl.Controls;

type

  TUniServerModule = class(TUniGUIServerModule)
    procedure UniGUIServerModuleBeforeInit(Sender: TObject);
    procedure UniGUIServerModuleException(Sender: TUniGUIMainModule; AException: Exception; var Handled: Boolean);
  private
  { Private declarations }
    const
{$IFDEF  ANGELO}
    const_Local_PosizioneDati = 'c:\SLP\Slp4Web\SLPDATI';
//     const_Local_PosizioneDati = 'Z:\VMShared\Slp4Web_2019\SLPDATI';
{$ELSE}
     const_Local_PosizioneDati  = 'C:\Sviluppo\Sorgenti\SLP4web_UNIG\SLPDATI';
{$ENDIF}
    const_remote_PosizioneDati = 'C:\slp\Slp4Web\Temp\';

    const_Local_Encription_password  = '27H312ACFF77908DDAC4005589.-+';
    const_Remote_encription_password = 'astraGalo37k21Hh51zZ';

    const_RemotePort     = 12005;
    const_RemoteAddress  = '127.0.0.1';
    const_RemoteUser     = 'SLPcomune';
    const_RemotePassword = '752scambiO421';

    const_ip_direzione = '194.184.86.253';
    const_Ramo_slp     = '17';

    function getEncriptionPassword: string;
    function getPosizioneDati: string;
    function getRemoteAddress: string;
    function getRemotePort: integer;
    function getRemoteUser: string;
    function getRemotePassword: string;
    function getMSG_PASSWORD_ERRATA: string;
    function getMSG_UTENTE_SCONOSCIUTO: string;
    function getOPERAZIONE_NON_AUTORIZZATA: string;
    function getRamoSLP: string;

  protected
    procedure FirstInit; override;
  public
  { Public declarations }
    const
    const_MSG_PASSWORD_ERRATA    = 'Autenticazione utente fallita! Password errata.';
    const_MSG_UTENTE_SCONOSCIUTO = 'Autenticazione utente fallita! Utente sconosciuto.';

    const_OPERAZIONE_NON_AUTORIZZATA = 'Utente non autorizzato ad eseguire l''operazione richiesta';

    function IsIpDirezione: Boolean;
    property PosizioneDati: string read getPosizioneDati;
    property EncriptionPassword: string read getEncriptionPassword;
    property RamoSLP: string read getRamoSLP;
    property RemoteAddress: string read getRemoteAddress;
    property RemotePort: integer read getRemotePort;
    property RemoteUser: string read getRemoteUser;
    property RemotePassword: string read getRemotePassword;
    property MSG_PASSWORD_ERRATA: string read getMSG_PASSWORD_ERRATA;
    property MSG_UTENTE_SCONOSCIUTO: string read getMSG_UTENTE_SCONOSCIUTO;
    property OPERAZIONE_NON_AUTORIZZATA: string read getOPERAZIONE_NON_AUTORIZZATA;
  end;

function UniServerModule: TUniServerModule;

implementation

{$R *.dfm}

uses
  UniGUIVars, uniGUIDialogs, UPolizzaExceptions;

function UniServerModule: TUniServerModule;
begin
  Result := TUniServerModule(UniGUIServerInstance);
end;

procedure TUniServerModule.FirstInit;
begin
  InitServerModule(Self);
end;

function TUniServerModule.getEncriptionPassword: string;
begin
{$IFDEF  SVILUPPO}
  Result := const_Local_Encription_password;
{$ELSE}
  Result := const_Remote_encription_password;
{$ENDIF}
end;

function TUniServerModule.getMSG_PASSWORD_ERRATA: string;
begin
  Result := const_MSG_PASSWORD_ERRATA;
end;

function TUniServerModule.getMSG_UTENTE_SCONOSCIUTO: string;
begin
  Result := const_MSG_UTENTE_SCONOSCIUTO;
end;

function TUniServerModule.getOPERAZIONE_NON_AUTORIZZATA: string;
begin
  Result := const_OPERAZIONE_NON_AUTORIZZATA;
end;

function TUniServerModule.getPosizioneDati: string;
begin
{$IFDEF  SVILUPPO}
  Result := const_Local_PosizioneDati;
{$ELSE}
  Result := const_remote_PosizioneDati;
{$ENDIF}
end;

function TUniServerModule.getRamoSLP: string;
begin
  Result := const_Ramo_slp;
end;

function TUniServerModule.getRemoteAddress: string;
begin
  Result := const_RemoteAddress;
end;

function TUniServerModule.getRemotePassword: string;
begin
  Result := const_RemotePassword;
end;

function TUniServerModule.getRemotePort: integer;
begin
  Result := const_RemotePort;
end;

function TUniServerModule.getRemoteUser: string;
begin
  Result := const_RemoteUser;
end;

function TUniServerModule.IsIpDirezione: Boolean;
begin
  Result := UniApplication.RemoteAddress = const_ip_direzione;
end;

procedure TUniServerModule.UniGUIServerModuleBeforeInit(Sender: TObject);
begin
  Self.Title                                  := 'SLP Assicurazioni Spa - slp4web';
  Self.ConnectionFailureRecovery.ErrorMessage := 'Errore di connessione';
  Self.ConnectionFailureRecovery.RetryMessage := 'Nuovo tentativo di connessione ...';
  Self.SessionTimeout                         := 1200000;     // 20 minuti
  Self.AjaxTimeout                            := 1200000;
{$IFNDEF  SVILUPPO}

  // impostazioni SSL disattivate per poter usare Hyperserver:
  // hyperserver usa le dll openSSL a 64 bit ma l'eseguibile deve essere compilato senza impostazioni SSL
  // versione test su server usa porta 12021

//   Self.Port          := 12021;
{
  Self.SSL.Enabled                 := True;
  Self.SSL.SSLOptions.CertFile     := 'cert.pem';
  Self.SSL.SSLOptions.KeyFile      := 'key.pem';
  Self.SSL.SSLOptions.RootCertFile := 'root.pem';
}
  // Self.SSL.SSLOptions.Method       := sslvTLSv1_2;


  // versione release su server vera usa 12017
  Self.Port          := 12017;   // 12017 server produzione   12021 per versione di prova che parte con  slp4web_tesx.htm
  Self.ExtRoot       := 'c:\slp\Slp4Web\Framework\Unigui\[ext]';
  Self.UniRoot       := 'c:\slp\Slp4Web\Framework\Unigui\[uni]';
  Self.UniMobileRoot := 'c:\slp\Slp4Web\Framework\Unigui\[unim]';


{$ENDIF}
end;

procedure TUniServerModule.UniGUIServerModuleException(Sender: TUniGUIMainModule; AException: Exception;
  var Handled: Boolean);
begin
  if AException.InheritsFrom(ESLP4WebException) then
  begin
    ShowMessage(AException.Message);
    Handled := True;
  end;
end;

initialization

RegisterServerModuleClass(TUniServerModule);

end.
