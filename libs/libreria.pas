unit libreria;

interface

uses sysutils, dateutils, System.StrUtils, Winapi.Windows;

function str0(n, l, d: integer): string;
function DtoSql(data: TdateTime): string;
function DtoS(data: TdateTime): string;
function StoD(data: string): TdateTime;
function alltrim(tt: string): string;
function Rpos(ss: string; tt: string): integer;
function fraz_to_num(fraz: string): integer;
procedure Log(const sMsg: string; onDebugString: boolean = false);
//function leggiVersione(const ApplicationName: string): string;
//function ReadVersionInfo(const sAppNamePath: string): TFileVersionInfoA;
function VerificaCodiceFiscale(codicefiscale: string): boolean;

function LeggiVersione: string;

implementation

uses
  System.IOUtils;

function alltrim(tt: string): string;
begin
  Result := Trim(tt);
end;

function str0(n, l, d: integer): string;
begin
  Result := inttostr(n);
  while length(Result) < l do
    Result := '0' + Result;
end;

function DtoSql(data: TdateTime): string;
var
  aa, mm, dd: word;
begin
  // restituisce la data in formato aaaa-mm-dd come richiesto dalle query SQL
  DecodeDate(data, aa, mm, dd);
  Result := inttostr(aa) + '-' + str0(mm, 2, 0) + '-' + str0(dd, 2, 0);
end;

function DtoS(data: TdateTime): string;
var
  aa, mm, dd: word;
begin
  // restituisce la data in formato aaaa-mm-dd come richiesto dalle query SQL
  DecodeDate(data, aa, mm, dd);
  Result := inttostr(aa) + str0(mm, 2, 0) + str0(dd, 2, 0);
end;

function StoD(data: string): TdateTime;
var
  aa, mm, dd: word;
begin
  // dalla string aaaammgg restituisce la data
  aa     := strToInt(copy(data, 1, 4));
  mm     := strToInt(copy(data, 5, 2));
  dd     := strToInt(copy(data, 7, 2));
  Result := encodedate(aa, mm, dd);
end;

function Rpos(ss: string; tt: string): integer;
var
  i, j: integer;
begin
  i := Pos(ss, tt);
  j := i;
  while i > 0 do
  begin
    j := i;
    i := posex(ss, tt, i + 1);
  end;
  Result := j;
end;

function fraz_to_num(fraz: string): integer;
begin
  if fraz = 'A' then
    Result := 1
  else
    if fraz = 'S' then
      Result := 2
    else
      if fraz = 'T' then
        Result := 4
      else
        if fraz = 'Q' then
          Result := 3
        else
          Result := 1;
end;

procedure Log(const sMsg: string; onDebugString: boolean);
var
  fLog: TextFile;
  nfile: string;
begin
{$IFDEF  DEBUG*}
  if onDebugString then
    outputdebugString(PChar(sMsg))
  else
  begin
    nfile := TPath.Combine(ExtractFilePath(ParamStr(0)), 'DBIsamSessionlog.txt');
    AssignFile(fLog, nfile);
    if not FileExists(nfile) then
      rewrite(fLog)
    else
      Append(fLog);
    try
      WriteLn(fLog, datetimeTostr(now) + ': ' + sMsg);
    finally
      CloseFile(fLog);
    end;
  end;

{$ENDIF}
end;

function VerificaCodiceFiscale(codicefiscale: string): boolean;

const
  CodiciMesi: String      = 'ABCDEHLMPRST';
  CodiciControllo: String = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

  function ValoreCarPosPari(C: Char): integer;
  begin
    Result := -1;
    case C of
      '0':
        Result := 0;
      '1':
        Result := 1;
      '2':
        Result := 2;
      '3':
        Result := 3;
      '4':
        Result := 4;
      '5':
        Result := 5;
      '6':
        Result := 6;
      '7':
        Result := 7;
      '8':
        Result := 8;
      '9':
        Result := 9;
      'A':
        Result := 0;
      'B':
        Result := 1;
      'C':
        Result := 2;
      'D':
        Result := 3;
      'E':
        Result := 4;
      'F':
        Result := 5;
      'G':
        Result := 6;
      'H':
        Result := 7;
      'I':
        Result := 8;
      'J':
        Result := 9;
      'K':
        Result := 10;
      'L':
        Result := 11;
      'M':
        Result := 12;
      'N':
        Result := 13;
      'O':
        Result := 14;
      'P':
        Result := 15;
      'Q':
        Result := 16;
      'R':
        Result := 17;
      'S':
        Result := 18;
      'T':
        Result := 19;
      'U':
        Result := 20;
      'V':
        Result := 21;
      'W':
        Result := 22;
      'X':
        Result := 23;
      'Y':
        Result := 24;
      'Z':
        Result := 25;
    end;
  end;

  function ValoreCarPosDispari(C: Char): integer;
  begin
    Result := -1;
    case C of
      '0':
        Result := 1;
      '1':
        Result := 0;
      '2':
        Result := 5;
      '3':
        Result := 7;
      '4':
        Result := 9;
      '5':
        Result := 13;
      '6':
        Result := 15;
      '7':
        Result := 17;
      '8':
        Result := 19;
      '9':
        Result := 21;
      'A':
        Result := 1;
      'B':
        Result := 0;
      'C':
        Result := 5;
      'D':
        Result := 7;
      'E':
        Result := 9;
      'F':
        Result := 13;
      'G':
        Result := 15;
      'H':
        Result := 17;
      'I':
        Result := 19;
      'J':
        Result := 21;
      'K':
        Result := 2;
      'L':
        Result := 4;
      'M':
        Result := 18;
      'N':
        Result := 20;
      'O':
        Result := 11;
      'P':
        Result := 3;
      'Q':
        Result := 6;
      'R':
        Result := 8;
      'S':
        Result := 12;
      'T':
        Result := 14;
      'U':
        Result := 16;
      'V':
        Result := 10;
      'W':
        Result := 22;
      'X':
        Result := 25;
      'Y':
        Result := 24;
      'Z':
        Result := 23;
    end;
  end;

  function CalcolaCodiceControllo(codicefiscale: String): Char;
  var
    i, CodiceControllo: integer;
  begin
    CodiceControllo := 0;
    for i           := 1 to length(codicefiscale) do
    begin
      if Odd(i) then
        Inc(CodiceControllo, ValoreCarPosDispari(codicefiscale[i]))
      else
        Inc(CodiceControllo, ValoreCarPosPari(codicefiscale[i]))
    end;
    CodiceControllo := CodiceControllo mod 26;
    Result          := CodiciControllo[CodiceControllo + 1];
  end;

begin
  codicefiscale := UpperCase(codicefiscale);
  Result        := (CalcolaCodiceControllo(copy(codicefiscale, 1, length(codicefiscale) - 1)) = codicefiscale
    [length(codicefiscale)]);
end;

function LeggiVersione: string;
begin
  Result := '1.0.0.1';
end;
//function leggiVersione(const ApplicationName: string): string;
//var
//  FvI: TFileVersionInfoA;
//begin
//  FvI := MFileVersionInfo(ApplicationName);
//  result:= FvI.FileVersion;
//end;

//function ReadVersionInfo(const sAppNamePath: string): TFileVersionInfoA;
//var
//  rSHFI: TSHFileInfo;
//  iRet: Integer;
//  VerSize: Integer;
//  VerBuf: PChar;
//  VerBufValue: Pointer;
//  VerHandle: Cardinal;
//  VerBufLen: Cardinal;
//  VerKey: string;
//  FixedFileInfo: PVSFixedFileInfo;
//
//  // dwFileType, dwFileSubtype
//  function GetFileSubType(FixedFileInfo: PVSFixedFileInfo): string;
//  begin
//    case FixedFileInfo.dwFileType of
//
//      VFT_UNKNOWN: Result    := 'Unknown';
//      VFT_APP: Result        := 'Application';
//      VFT_DLL: Result        := 'DLL';
//      VFT_STATIC_LIB: Result := 'Static-link Library';
//
//      VFT_DRV:
//        case
//          FixedFileInfo.dwFileSubtype of
//          VFT2_UNKNOWN: Result         := 'Unknown Driver';
//          VFT2_DRV_COMM: Result        := 'Communications Driver';
//          VFT2_DRV_PRINTER: Result     := 'Printer Driver';
//          VFT2_DRV_KEYBOARD: Result    := 'Keyboard Driver';
//          VFT2_DRV_LANGUAGE: Result    := 'Language Driver';
//          VFT2_DRV_DISPLAY: Result     := 'Display Driver';
//          VFT2_DRV_MOUSE: Result       := 'Mouse Driver';
//          VFT2_DRV_NETWORK: Result     := 'Network Driver';
//          VFT2_DRV_SYSTEM: Result      := 'System Driver';
//          VFT2_DRV_INSTALLABLE: Result := 'InstallableDriver';
//          VFT2_DRV_SOUND: Result       := 'Sound Driver';
//        end;
//      VFT_FONT:
//        case FixedFileInfo.dwFileSubtype of
//          VFT2_UNKNOWN: Result       := 'Unknown Font';
//          VFT2_FONT_RASTER: Result   := 'Raster Font';
//          VFT2_FONT_VECTOR: Result   := 'Vector Font';
//          VFT2_FONT_TRUETYPE: Result := 'Truetype Font';
//          else;
//        end;
//      VFT_VXD: Result := 'Virtual Defice Identifier = ' +
//          IntToHex(FixedFileInfo.dwFileSubtype, 8);
//    end;
//  end;
//
//
//  function HasdwFileFlags(FixedFileInfo: PVSFixedFileInfo; Flag: Word): Boolean;
//  begin
//    Result := (FixedFileInfo.dwFileFlagsMask and
//      FixedFileInfo.dwFileFlags and
//      Flag) = Flag;
//  end;
//
//  function GetFixedFileInfo: PVSFixedFileInfo;
//  begin
//    if not VerQueryValue(VerBuf, '', Pointer(Result), VerBufLen) then
//      Result := nil
//  end;
//
//  function GetInfo(const aKey: string): string;
//  begin
//    Result := '';
//    VerKey := Format('\StringFileInfo\%.4x%.4x\%s',
//      [LoWord(Integer(VerBufValue^)),
//      HiWord(Integer(VerBufValue^)), aKey]);
//    if VerQueryValue(VerBuf, PChar(VerKey), VerBufValue, VerBufLen) then
//      Result := StrPas(VerBufValue);
//  end;
//
//  function QueryValue(const aValue: string): string;
//  begin
//    Result := '';
//    // obtain version information about the specified file
//    if GetFileVersionInfo(PChar(sAppNamePath), VerHandle, VerSize, VerBuf) and
//      // return selected version information
//      VerQueryValue(VerBuf, '\VarFileInfo\Translation', VerBufValue, VerBufLen) then
//      Result := GetInfo(aValue);
//  end;
//begin
//  // Initialize the Result
//  with Result do
//  begin
//    FileType         := '';
//    CompanyName      := '';
//    FileDescription  := '';
//    FileVersion      := '';
//    InternalName     := '';
//    LegalCopyRight   := '';
//    LegalTradeMarks  := '';
//    OriginalFileName := '';
//    ProductName      := '';
//    ProductVersion   := '';
//    Comments         := '';
//    SpecialBuildStr  := '';
//    PrivateBuildStr  := '';
//    FileFunction     := '';
//    DebugBuild       := False;
//    Patched          := False;
//    PreRelease       := False;
//    SpecialBuild     := False;
//    PrivateBuild     := False;
//    InfoInferred     := False;
//  end;
//
//  // Get the file type
//  if SHGetFileInfo(PChar(sAppNamePath), 0, rSHFI, SizeOf(rSHFI),
//    SHGFI_TYPENAME) <> 0 then
//  begin
//    Result.FileType := rSHFI.szTypeName;
//  end;
//
//  iRet := SHGetFileInfo(PChar(sAppNamePath), 0, rSHFI, SizeOf(rSHFI), SHGFI_EXETYPE);
//  if iRet <> 0 then
//  begin
//    // determine whether the OS can obtain version information
//    VerSize := GetFileVersionInfoSize(PChar(sAppNamePath), VerHandle);
//    if VerSize > 0 then
//    begin
//      VerBuf := AllocMem(VerSize);
//      try
//        with Result do
//        begin
//          CompanyName      := QueryValue('CompanyName');
//          FileDescription  := QueryValue('FileDescription');
//          FileVersion      := QueryValue('FileVersion');
//          InternalName     := QueryValue('InternalName');
//          LegalCopyRight   := QueryValue('LegalCopyRight');
//          LegalTradeMarks  := QueryValue('LegalTradeMarks');
//          OriginalFileName := QueryValue('OriginalFileName');
//          ProductName      := QueryValue('ProductName');
//          ProductVersion   := QueryValue('ProductVersion');
//          Comments         := QueryValue('Comments');
//          SpecialBuildStr  := QueryValue('SpecialBuild');
//          PrivateBuildStr  := QueryValue('PrivateBuild');
//          // Fill the VS_FIXEDFILEINFO structure
//          FixedFileInfo := GetFixedFileInfo;
//          DebugBuild    := HasdwFileFlags(FixedFileInfo, VS_FF_DEBUG);
//          PreRelease    := HasdwFileFlags(FixedFileInfo, VS_FF_PRERELEASE);
//          PrivateBuild  := HasdwFileFlags(FixedFileInfo, VS_FF_PRIVATEBUILD);
//          SpecialBuild  := HasdwFileFlags(FixedFileInfo, VS_FF_SPECIALBUILD);
//          Patched       := HasdwFileFlags(FixedFileInfo, VS_FF_PATCHED);
//          InfoInferred  := HasdwFileFlags(FixedFileInfo, VS_FF_INFOINFERRED);
//          FileFunction  := GetFileSubType(FixedFileInfo);
//        end;
//      finally
//        FreeMem(VerBuf, VerSize);
//      end
//    end;
//  end
//end;

end.
