unit libSLP;

interface

uses sysutils, System.DateUtils, System.Character, System.StrUtils, JclStrings;

const
  NULL_STRING = '';

function str0(n, l, d: integer): string;
function DtoSql(data: TdateTime): string;
function DtoS(data: TdateTime): string;
function StoD(data: string): TdateTime;
function DToC(rDate: TdateTime): String;
function dai_titolo(cf: string): string;
function right(tt: string; ncar: integer): string;
function left(tt: string; ncar: integer): string;
function PadL(tt: string; con_cosa: Char; lung: integer): string;
function PadR(tt: string; con_cosa: Char; lung: integer): string;
function Ltrim(tt: string): string;
function Rtrim(tt: string): string;
function Alltrim(tt: string): string;
function strzero(tt: string; nzero: integer): string; overload;
function strzero(tt: integer; nzero: integer): string; overload;
function year(tt: TdateTime): Word;
function isdigit(tt: string): Boolean;
function month(tt: TdateTime): Word;
function day(tt: TdateTime): Word;
function replicate(tt: string; numero: integer): string;
function round50(x: currency): currency; // arrototonda ai 50 centesimi pi� vicini
function IsAlpha(sStr: String): Boolean;
function space(numero: integer): string;
function At(sFind, sSource: String): LongInt;
function RAt(sFind, sSource: String): LongInt;
function substr(ss: string; da, quanti: integer): string;
function StrTran(sStr, sFind, sReplace: String; iStart, iNum: LongInt): String;
function Stuff(sSource: String; iStart, iDel: LongInt; sInsert: String): String;
function upper(ss: string): string;
function Proper(sString: String): String;
function Empty(const arg: array of const): Boolean;
function zero(valore: double): string;

{ Date Manipulation Functions }

function CToD(sStr: String): TdateTime; { Char To Date }
function DaysPerMonth(AYear, AMonth: integer): integer;
function DOW(rDate: TdateTime): Word;

{ IIFx() functions }
function IIFB(bCond, bTrue, bFalse: Boolean): Boolean;
function IIFC(bCond: Boolean; cTrue, cFalse: Char): Char;
function IIFD(bCond: Boolean; dTrue, dFalse: TdateTime): TdateTime;
function IIFI(bCond: Boolean; iTrue, iFalse: integer): integer;
function IIFL(bCond: Boolean; liTrue, liFalse: LongInt): LongInt;
function IIFR(bCond: Boolean; rTrue, rFalse: Real): Real;
function IIFS(bCond: Boolean; sTrue, sFalse: String): String;
{ xTransform() functions }
function BTransForm(Value: Boolean; Picture: String): String;
function NTransForm(Value: double; Picture: String): String;
function STransForm(Value: String; Picture: String): String;
function Transform(const Args: Array of Const; Picture: String): String;
function InStr(sFind, sSearch, sDelim: String): Boolean;

// procedure dai_scadenze(decorrenza, scadenza: TdateTime; fraz: word; lista:TstringList);
// function dist360(d1,d2: TdateTime): integer;
// function diffMesi30(d1,d2: TdateTime): integer;
function dai_frazionamento(fraz: string): Word;
function dai_frazionamentoE(fraz: string): String;
// function dai_interessi(fraz: string): single; overload;
// function dai_interessi(fraz: smallint): single; overload;
// function quietanzio(mese,anno: smallint; decorrenza, scadenza: TdateTime; fraz: word): boolean;
// function crea_data(gg,mm,aa: integer): TdateTime;
// function LastOfMonth(gg, mm, aa: smallint): boolean;
// function UltimoGiornoDelMese(mm, aa: smallint): integer;

implementation

var
  ObjCreated: Boolean;
  DateFmtStr: String;

function str0(n, l, d: integer): string;
begin
  result := inttostr(n);
  while length(result) < l do
    result := '0' + result;
end;

function At(sFind, sSource: String): LongInt;
begin
  result := pos(sFind, sSource);
end;

function RAt(sFind, sSource: String): LongInt;
var
  iPos, iTemp: LongInt;
begin
  result := 0;
  iPos   := pos(sFind, sSource);

  if iPos = 0 then
    exit;

  iTemp := 0;
  while iPos > 0 do
  begin
    { Save the current position }
    iTemp := iTemp + iPos;
    { Extract the portion we just counted }
    sSource := copy(sSource, iPos + 1, length(sSource));
    { See if another instance of sFind is there }
    iPos := pos(sFind, sSource);
  end;
  { Return the last found position of sFind we saved above }
  result := iTemp;
end;

function upper(ss: string): string;
begin
  result := UpperCase(ss);
end;

function substr(ss: string; da, quanti: integer): string;
begin
  if quanti = 0 then
    result := copy(ss, da, length(ss))
  else
    result := copy(ss, da, quanti);
end;

(* **********************************************************************
  CToD( <StringDate> ) - converts a string date into its Pascal
  TDateTime type equivalent.
  ********************************************************************** *)
function CToD(sStr: String): TdateTime; { Char To Date }
begin
  result := StrToDate(sStr);
end;

(* **********************************************************************
  DaysPerMonth( <Year>, <Month> ) - returns number of days in <Month>
  for <Year>.  Accounts for leap years using IsLeapYear().
  ('Borrowed' from CALENDAR.PAS which comes with Delphi).
  ********************************************************************** *)
function DaysPerMonth(AYear, AMonth: integer): integer;
const
  MonthDays: array [1 .. 12] of integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  result := MonthDays[AMonth];
  if (AMonth = 2) and IsLeapYear(AYear) then { leap-year Feb is special }
    Inc(result);
end;

(* **********************************************************************
  DOW( <Date> ) - returns integer specifying day of week, where
  Sunday is 1, Monday is two,...Saturday is 7.
  ********************************************************************** *)
function DOW(rDate: TdateTime): Word;
begin
  result := DayOfWeek(rDate);
end;

(* **********************************************************************
  Empty([<xVal]) - Tests a boolean, string, or numeric argument and
  returns True if empty, False if not.  Empty is defined as:

  Booleans:  False
  Strings :  Length = 0 or nothing is in string
  Numerics:  0

  Note that the square brackets ([]) are REQUIRED!
  ********************************************************************** *)
function Empty(const arg: array of const): Boolean;
var
  s: Byte; { Not needed, but needed to allow static functions }

  function BEmpty(bVal: Boolean): Boolean;
  begin
    result := not(bVal);
  end;

  function SEmpty(sVal: String): Boolean;
  begin
    result := (length(Alltrim(sVal)) = 0);
  end;

  function NEmpty(nVal: double): Boolean;
  begin
    result := (nVal = 0);
  end;

begin
  s := 0; { Only done to prevent compiler hints about unused var }
  with arg[s] do
    case VType of
      VTInteger:
        result := NEmpty(VInteger);
      VTExtended:
        result := NEmpty(VExtended^);
      VTString:
        result := SEmpty(VString^);
      VTBoolean:
        result := BEmpty(VBoolean);
      VTChar:
        result := SEmpty(VChar);
      VTPChar:
        result := SEmpty(StrPas(VPChar));
    else
      raise Exception.Create('Invalid argument to Empty function');
    end;
end;

function DtoSql(data: TdateTime): string;
var
  aa, mm, dd: Word;
begin
  // restituisce la data in formato aaaa-mm-dd come richiesto dalle query SQL
  DecodeDate(data, aa, mm, dd);
  result := inttostr(aa) + '-' + str0(mm, 2, 0) + '-' + str0(dd, 2, 0);
end;

function DtoS(data: TdateTime): string;
var
  aa, mm, dd: Word;
begin
  // restituisce la data in formato aaaa-mm-dd come richiesto dalle query SQL
  DecodeDate(data, aa, mm, dd);
  result := inttostr(aa) + str0(mm, 2, 0) + str0(dd, 2, 0);
end;

function year(tt: TdateTime): Word;
begin
  result := YearOf(tt);
end;

function month(tt: TdateTime): Word;
begin
  result := MonthOf(tt);
end;

function day(tt: TdateTime): Word;
begin
  result := dayof(tt);
end;

function StoD(data: string): TdateTime;
var
  aa, mm, dd: Word;
begin
  // dalla string aaaammgg restituisce la data
  aa     := strToInt(copy(data, 1, 4));
  mm     := strToInt(copy(data, 5, 2));
  dd     := strToInt(copy(data, 7, 2));
  result := encodedate(aa, mm, dd);
end;

function DToC(rDate: TdateTime): String; { Date To Char }
begin
  if ObjCreated then
    result := FormatDateTime(DateFmtStr, rDate)
  else
    result := FormatDateTime(sysutils.FormatSettings.ShortDateFormat, rDate);
end;

function right(tt: string; ncar: integer): string;
begin
  result := RightStr(tt, ncar);
end;

function left(tt: string; ncar: integer): string;
begin
  result := RightStr(tt, ncar);
end;

function PadR(tt: string; con_cosa: Char; lung: integer): string;
var
  i: integer;
begin
  i := length(tt);
  while i < lung do
  begin
    tt := tt + con_cosa;
    Inc(i);
  end;
  result := tt;
end;

function PadL(tt: string; con_cosa: Char; lung: integer): string;
var
  i: integer;
begin
  i := length(tt);
  while i < lung do
  begin
    tt := con_cosa + tt;
    Inc(i);
  end;
  result := tt;
end;

function Ltrim(tt: string): string;
begin
  result := StrTrimCharLeft(tt, ' ');
end;

function Rtrim(tt: string): string;
begin
  result := StrTrimCharRight(tt, ' ');
end;

function Alltrim(tt: string): string;
begin
  result := Ltrim(Rtrim(tt));
end;

function strzero(tt: string; nzero: integer): string;
begin
  result := PadL(tt, '0', nzero);
end;

function strzero(tt: integer; nzero: integer): string; overload;
begin
  result := PadL(inttostr(tt), '0', nzero);
end;

function isdigit(tt: string): Boolean;
begin
  result := (pos(copy(tt, 1, 1), '0123456789') > 0);
end;

function replicate(tt: string; numero: integer): string;
var
  ii: integer;
begin
  result   := '';
  for ii   := 1 to numero do
    result := result + tt;
end;

function space(numero: integer): string;
begin
  result := replicate(' ', numero);
end;


// function LastOfMonth(gg, mm, aa: smallint): boolean;
// begin
// // true se la data passata � l'ultimo giorno del mese
// result:=false;
// if gg=31 then result:=true
// else begin
// if (gg=30) and (mm in [4,6,9,11]) then result:=true
// else if mm=2 then begin
// if IsLeapYear(aa) and (gg=29) then result:=true;
// if not IsLeapYear(aa) and (gg=28) then result:=true;
// end;
// end;
// end;
//
//
// function UltimoGiornoDelMese(mm, aa: smallint): integer;
// begin
// // mi restituisce l'ultimo giorno del mese della mese/anno passato
// result:=31;
// if (mm in [4,6,9,11]) then result:=30
// else begin
// if mm=2 then begin
// if IsLeapYear(aa) then result:=29
// else result:=28;
// end;
// end;
// end;
//
//
// function dist360(d1,d2: TdateTime): integer;
// var dt: TdateTime;
// a1, a2, m1, m2, g1, g2: word;
// a0, m0, g0: smallint;
// begin
// // d1 deve sempre essere la data minore
// if d1>d2 then begin
// dt:=d1;
// d1:=d2;
// d2:=dt;
// end;
// // decodifica i dati relativi alle date fornite
// decodedate(d1,a1,m1,g1);
// decodedate(d2,a2,m2,g2);
//
// a0:=a2-a1;
// m0:=m2-m1;
// // normalizza l'utimo giorno del mese a 30 gg
// if lastOfMonth(g1,m1,a1) then g1:=30;
// if lastOfMonth(g2,m2,a2) then g2:=30
// else begin
// // occhi se parliamo del mese di febbraio !!!!!
// if (m2=2) and (g2>=28) then
// begin
// // se � febbraio una che se indico come giorno di scadenza 29 e come giorno
// // di decorrenza 28 deve essere tutto ok
// g2:=30;
// end;
// end;
// g0:=g2-g1;
// result:= a0*360 + m0*30 + g0;
// // function IncMonth(const Date: TDateTime; NumberOfMonths: Integer): TDateTime;
// end;

// procedure dai_scadenze(decorrenza, scadenza: TdateTime; fraz: word; lista:TstringList);
// var finito: boolean;
// sca: TdateTime;
// durata, passo, passo1, durata_mesi: integer;
// begin
// // partendo dalla scaenza fai retro marcia fino ad arrivare alla decorrenza
// // o a un punto precedente (ratino)
// // le date sono caricate in senso inverso: durata  n= giorni di rateo; 1=ultima rata
// finito:=false;
// sca:=scadenza;
// passo:=12 div fraz;
// passo1:=passo;
//
// durata:=dist360(decorrenza,scadenza);
// if (durata<720) and (month(scadenza)<>month(decorrenza)) and (fraz=1) then
// begin
// lista.Add(dateTostr(scadenza));
// end else begin
// // altri casi di ratino !!!!
// if (month(scadenza)<>month(decorrenza)) or ((month(scadenza)=month(decorrenza)) and (day(scadenza)<>day(decorrenza)) )  then
// begin
// lista.Add(dateTostr(scadenza));
// end;
// end;
//
// while not finito do begin
// sca:=incMonth(scadenza,-passo1);
// lista.Add(dateTostr(sca));
// finito := sca<=decorrenza;
// passo1:=passo1+passo;
// end;
// // durata:=dist360(decorrenza,scadenza) div 360;
// durata:=dist360(decorrenza,scadenza);
// durata_mesi:= (durata mod 360) div 30;
// durata:=durata div 360;
// lista.Add(intTostr(durata)+'+'+intTostr(durata_mesi));   // durata anni
//
// if lista.strings[lista.count-2]<>dateToStr(decorrenza) then begin
// // caso con ratino iniziale ...
// lista.strings[lista.count-2]:=dateToStr(decorrenza);
// if lista.Count>=3 then
// passo:=dist360(decorrenza,strTodate(lista.strings[lista.count-3]));
// lista.Add(intTostr(passo));
// end else lista.Add(intTostr(0));
//
// // correttivo per caso di polizza durata anni uno fraz ann
// if (lista.count=3) and (fraz=1) then begin
// lista.Insert(0,datetostr(scadenza));
// lista.strings[1]:=datetostr(scadenza);
// end;
// end;

// function quietanzio(mese,anno: smallint; decorrenza, scadenza: TdateTime; fraz: word): boolean;
// var passo,i ,j: integer;
// meseS, mese1: integer;
// begin
// // restituisce true se nel mese,anno indicato la polizza con decorrenza, scadenza
// // e frazionamento dato anno quietanza ...
// passo:= 12 div fraz;
// meseS:=month(scadenza);
// result:=false;
// for i:=1 to fraz do begin
// if mese=meseS then begin
// result:=true;
// break;
// end;
// mese1:= (mese+passo) mod 12;
// if mese1=0 then mese:=mese+passo
// else mese:=mese1;
// end;
// end;

function dai_frazionamento(fraz: string): Word;
begin
  if pos(fraz, 'ASTQBXP') > 0 then
  begin
    if fraz = 'A' then
      result := 1;
    if fraz = 'S' then
      result := 2;
    if fraz = 'T' then
      result := 4;
    if fraz = 'Q' then
      result := 3;
    if fraz = 'B' then
      result := 6;
    if fraz = 'X' then
      result := 12;
    if fraz = 'P' then
      result := 999;
  end
  else
    if pos(fraz, '123460') > 0 then
    begin
      if fraz = '1' then
        result := 1;
      if fraz = '2' then
        result := 2;
      if fraz = '4' then
        result := 4;
      if fraz = '3' then
        result := 3;
      if fraz = '6' then
        result := 6;
      if fraz = '0' then
        result := 12;
    end
    else
      result := 1;
end;

function dai_frazionamentoE(fraz: string): string;
var
  r: Word;
begin
  r := dai_frazionamento(fraz);
  case r of
    1:
      result := 'Annuale';
    2:
      result := 'Semestrale';
    3:
      result := 'Quadrimestrale';
    4:
      result := 'Trimestrale';
    6:
      result := 'Bimestrale';
    12:
      result := 'Mensile';
  end;
end;

function dai_titolo(cf: string): string;
begin
  try
    if (copy(cf, 1, 1) >= '0') and (copy(cf, 1, 1) <= '9') then
      result := 'Spett.Le'
    else
    begin
      if IsNumber(strToInt(copy(cf, 10, 2))) and (strToInt(copy(cf, 10, 2)) > 40) then
        result := 'Gent.ma Sig.ra'
      else
        result := 'Egr. Sig.';
    end;
  except
    // in caso disperato restituisci solo una stringa vuota
    result := '';
  end;
end;

function round50(x: currency): currency; // arrototonda ai 50 centesimi pi� vicini
var
  difdec, x1: currency;
begin
  x1     := x;
  difdec := (x - int(x)); //
  IF difdec > 0 then
  begin
    if (difdec >= 0.001) AND (difdec <= 0.259999) then
      difdec := 0
    else
      if (difdec >= 0.26) AND (difdec <= 0.749999) then
        difdec := 0.5
      else
        if (difdec >= 0.75) AND (difdec <= 0.999999) then
          difdec := 1;
  END;
  result := int(x1) + difdec;
end;

function IsAlpha(sStr: String): Boolean;
begin
  result := ((length(sStr) > 0) and (sStr[1] in ['a' .. 'z']) or (sStr[1] in ['A' .. 'Z']));
end;

(* **********************************************************************
  InStr(<StrToFind>, <StrToSearch>, <DelimChar>) - Works like Clipper's
  $ (contained in) operator.  Returns True if <StrToFind> is contained
  in <StrToSearch>, False if not.  If you want to look for several
  different values at once, separate them with the character you pass as
  <DelimChar>; if you don't need this, pass #0 or a NULL_STRING.  See
  below for examples.

  InStr('A;B;C', 'ABCDE', ';') returns True
  InStr('ABC', 'ABCDE', '') returns True
  InStr('X;Y;z', 'ABCXYZ', ';') returns False
  InStr('X;Y;Z', 'ABCXYZ', ';') returns True

  Instr('YES_OUI', Upper(Edit1.Text), '_') returns:
  True if user entered 'YES' or 'OUI'
  False if user entered NO (or anything else)

  Please note that the example above for YES_OUI can be a problem if
  the user has been allowed to enter a longer string, such as 'YESTERDAY',
  as it would incorrectly identify the first three letters as a match.  This
  is also a limitation of Clipper's $ operator, so it shouldn't be too hard
  to work with.
  ********************************************************************** *)
function InStr(sFind, sSearch, sDelim: String): Boolean;
var
  sTmp: String;
  iPos: LongInt;
  bStop: Boolean;
begin
  result := False;
  sTmp   := NULL_STRING;
  bStop  := False;
  if ((sDelim = #0) or (sDelim = NULL_STRING)) then
  begin
    result := (At(sFind, sSearch) > 0);
    exit;
  end;

  repeat
    iPos := At(sDelim, sFind);
    if (iPos > 0) or (length(sFind) = 1) then
    begin
      if iPos > 0 then
      begin
        sTmp  := substr(sFind, 1, iPos - 1);
        sFind := substr(sFind, iPos + length(sDelim), 0);
      end
      else
      begin
        sTmp  := sFind;
        bStop := True;
      end;
      if (At(sTmp, sSearch) > 0) then
      begin
        result := True;
        break;
      end;
    end;
  until (bStop);
end;

(* **********************************************************************
  Proper( sValue ) - Converts <sValue> to proper name format.

  Example:

  S := Proper('borland delphi');  { S is now 'Borland Delphi' }

  See also:  Transform()
  ********************************************************************** *)
function Proper(sString: String): String;
var
  i: LongInt;
begin
  if length(sString) < 1 then
    result := NULL_STRING;
  if length(sString) < 2 then
  begin
    result := upper(result);
    exit;
  end;
  result := upper(substr(sString, 1, 1)) + substr(sString, 2, 0);
  for i  := 2 to length(result) - 1 do
    if (InStr(substr(result, i, 1), '.'' -/,&*(+\', NULL_STRING)) or ((i > 2) and (substr(result, i - 2, 3) = 'Mac')) or
      (substr(result, i - 1, 2) = 'Mc') then
      result := substr(result, 1, i) + upper(substr(result, i + 1, 1)) + substr(result, i + 2, 0);
end;

(* **********************************************************************
  Stuff( <Src>, <StartPos>, <NumToDel>, <InsStr> ) - Does something
  similar to a 'find and replace' with a string.  It goes to
  <StartPos> in <Str>, deletes <NumToDel> characters, and then
  inserts <InsStr>.  Passing 0 (zero) as <NumToDel> skips the
  deletion step.
  ********************************************************************** *)
function Stuff(sSource: String; iStart, iDel: LongInt; sInsert: String): String;
var
  liLen: LongInt;
begin
  result := NULL_STRING;
  liLen  := length(sSource);

  { Do we have characters to be deleted?  If so, do it! }
  if iDel > 0 then
    Delete(sSource, iStart, iDel);

  { Same with characters to insert - are there any? }
  if sInsert <> NULL_STRING then
  begin
    if (liLen < (length(sSource) + length(sInsert))) then
      SetLength(sSource, length(sSource) + length(sInsert));
    Insert(sInsert, sSource, iStart);
  end;

  { Return the altered string }
  result := sSource;
end;

(* **********************************************************************
  StrTran( <OrigStr>, <FindStr>, <ReplaceStr>, <StartOccur>, <NumTimes> )
  -  This is a goodie!  StrTran() searches for <FindStr> within
  <OrigStr>, and replaces it with <ReplaceStr>.  The good part is
  that you can specify <StartOccur> to start with any occurrence
  of <FindStr>, and <NumTimes> to only perform the replacement a
  specified number of times.  Passing 0 (zero) for <NumTimes> replaces
  all occurrences of <FindStr> with <ReplaceStr>.

  Examples (sorely needed with this one):

  Str := 'This is a string This is a string This is a string';

  StrTran( Str, 'is', 'was', 1, 1 ) returns
  'This was a string This is a string This is a string'

  StrTran( Str, 'This', 'Str', 1, 0 ) returns
  'Str was a string Str is a string Str is a string'

  StrTran( Str, 'string', 'fuzzy bear', 2, 1) returns
  'Str was a string Str is a fuzzy bear Str is a string'

  Test it on a few strings and have fun!

  See Also:  Stuff()
  ********************************************************************** *)
function StrTran(sStr, sFind, sReplace: String; iStart, iNum: LongInt): String;
var
  iPos: LongInt;
  sTmp: String;
  bAll: Boolean;
  iCtr: LongInt;
  iPass: LongInt;
begin
  bAll   := (iNum = 0);
  iPos   := At(sFind, sStr);
  iPass  := 0;
  result := sStr;
  if iPos = 0 then
    exit;

  if (iStart = 1) then
  begin
    result := Stuff(sStr, iPos, length(sFind), sReplace);
    iPass  := 1;
    if (iNum = 1) then
      exit;
  end;

  { If we're doing all instances of the replacement... }
  if (bAll) and (iStart = 1) then
  begin
    repeat
      { We can simply loop through until we don't find any more... }
      iPos := At(sFind, result);
      if iPos = 0 then
        exit;
      result := Stuff(result, iPos, length(sFind), sReplace);
    until (iPos = 0);
  end
  else
  begin
    { Little harder now...we're not doing all, or we're starting past the
      first instance... }
    sTmp   := sStr;
    result := '';
    if (iStart > 1) then
    begin { We need to find the starting position... }
      for iCtr := 1 to iStart - 1 do
      begin
        iPos   := At(sFind, sTmp);
        result := result + substr(sTmp, 1, iPos + IIFL(length(sFind) > 1, length(sFind), 0));
        sTmp   := substr(sTmp, iPos + IIFL(length(sFind) > 1, length(sFind) + 1, 1), 0);
      end;
    end;
    { Either way we should be where we need to be right now... }
    if (bAll) then
    begin
      repeat
        iPos := At(sFind, sTmp);
        if iPos = 0 then
          break;
        sTmp   := Stuff(sTmp, iPos, length(sFind), sReplace);
        result := result + substr(sTmp, 1, iPos + IIFL(length(sReplace) > 1, length(sReplace), 0));
        sTmp   := substr(sTmp, iPos + length(sReplace) + 1, 0);
      until (iPos = 0);
    end
    else
    begin
      iNum     := iNum - iPass;
      for iCtr := iPass to iNum do
      begin
        iPos := At(sFind, sTmp);
        if iPos = 0 then
          break;
        sTmp   := Stuff(sTmp, iPos, length(sFind), sReplace);
        result := result + substr(sTmp, 1, iPos + IIFL(length(sReplace) > 1, length(sReplace), 0));
        sTmp   := substr(sTmp, iPos + length(sReplace) + 1, 0);
      end;
    end;
    result := result + sTmp;
  end;
end;

function IIFB(bCond, bTrue, bFalse: Boolean): Boolean;
begin
  if (bCond) then
    result := bTrue
  else
    result := bFalse;
end;

(* ********************** Immediate IF Char ***************************
  IIFC(<bCond>, <cTrue>, <cFalse>) - evaluates the boolean condition
  <bCond>.  If it is true, the Char <cTrue> is returned; if not,
  the Char <cFalse> is returned.

  Example:
  {
  Assumes that a usertype can be 'S' for supervisor or
  'U' for user, and that utype is the usertype for this
  individual.
  }
  cVal := IIFC((utype = 1), 'S', 'U');

  This replaces the following:
  if (utype = 1) then
  cVal := 'S'
  else
  cVal := 'U';
  ********************************************************************** *)
function IIFC(bCond: Boolean; cTrue, cFalse: Char): Char;
begin
  if (bCond) then
    result := cTrue
  else
    result := cFalse;
end;

(* ************************ Immediate IF Date ************************
  IIFD(<bCond>, <dTrue>, <dFalse>).  Evaluates <bCond> and returns
  TDateTime <dTrue> if true, TDateTime <dFalse> if false.

  Example:
  dStart := IIFD((reporttype = 'M', FirstOfMonth(Date), Date);

  This replaces the following:
  if (reporttype = 'M') then
  dStart := FirstOfMonth(Date)
  else
  dStart := Date;
  ********************************************************************** *)
function IIFD(bCond: Boolean; dTrue, dFalse: TdateTime): TdateTime;
begin
  if (bCond) then
    result := dTrue
  else
    result := dFalse;
end;

(* ******************** Immediate IF Integer ***************************
  IIFI(<bCond>, <iTrue>, <iFalse>) - Evaluates <bCond>; if true,
  returns the Integer <iTrue>; if false, returns the Integer
  <iFalse>.

  Example:
  iType := IIFI((usertype = 'S', 1, 0);

  Replaces:
  if (usertype = 'S') then
  iType := 1
  else
  iType := 0;
  ********************************************************************** *)
function IIFI(bCond: Boolean; iTrue, iFalse: integer): integer;
begin
  if (bCond) then
    result := iTrue
  else
    result := iFalse;
end;

(* ********************** Immediate IF LongInt **************************
  IIFL(<bCond>, <liTrue>, <liFalse>) - Evaluates <bCond>.  If true,
  returns the LongInt <liTrue>; if false, returns the LongInt
  <liFalse>.

  Example:
  liTotal := IIFL((reporttype = 'S', liGrandTotal, liSubTotal);

  Replaces:
  if (reporttype = 'S') then
  liTotal := liGrandTotal
  else
  liTotal := liSubTotal;
  ********************************************************************** *)
function IIFL(bCond: Boolean; liTrue, liFalse: LongInt): LongInt;
begin
  if (bCond) then
    result := liTrue
  else
    result := liFalse;
end;

(* ****************Immediate IF Real (Floating point)********************
  IIFR(<bCond>, <rTrue>, <rFalse>) - Evaluates <bCond>.  If true, the
  Real <rTrue> is returned; if false, the Real <rFalse> is
  returned.

  Example:
  rMult := IIFR((rExpYrs > 10 and rExp < 1), 2.5, 5.7);
  ********************************************************************** *)
function IIFR(bCond: Boolean; rTrue, rFalse: Real): Real;
begin
  if (bCond) then
    result := rTrue
  else
    result := rFalse;
end;

(* ********************** Immediate IF String **************************
  IIFS(<bCond>, <sTrue>, <sFalse>) - Evaluates <bCond>.  If true, the
  string <sTrue> is returned; if false, the string <sFalse> is
  returned.

  Example:
  sTestResult := 'Your score was ' +
  IIFS((passed), 'passing', 'failing');
  ********************************************************************** *)
function IIFS(bCond: Boolean; sTrue, sFalse: String): String;
begin
  if (bCond) then
    result := sTrue
  else
    result := sFalse;
end;

(* ******************* Transform() Functions ************************** *)
(* *********************************************************************
  BTransForm( <bVal>, <sPict> ) - Converts the Boolean <bVal> to a
  string according to the format specified in <sPict>.  The
  formats available in <sPict> are:

  'Y' - returns 'Y' or 'N' for True and False
  'W', 'T', 'F' - returns 'True' or 'False'
  'L' - returns 'T' and 'F'
  ********************************************************************* *)
function BTransForm(Value: Boolean; Picture: String): String;
begin
  result := NULL_STRING;
  if length(Picture) < 1 then
    exit;
  case Picture[1] of
    'Y', 'y':
      result := IIFS(Value, 'Y', 'N');
    'W', 'w', 'T', 't', 'F', 'f':
      result := IIFS(Value, 'True', 'False');
    'L', 'l':
      result := IIFS(Value, 'T', 'F');
  else
    raise Exception.Create('Invalid PICTURE ' + Picture + ' in BTransForm()');
  end;
end;

(* *********************************************************************
  NTransform( <dVal>, <sPict> ) - Converts the Double value <dVal> into
  a string according to the format specified in <sPict>.  Note that
  Delphi's TDateTime format is actually a Real (Double) value, so
  this function is also capable of formatting dates.  Valid <sPict>
  clauses are:

  '@D'    - converts <dVal> into a date string in the same format
  as DToC()
  '@$'    - converts <dVal> into a string formatted as currency, with
  a leading dollar sign, and leading spaces filled with the
  dollar sign character.  Note that the phrase 'dollar sign'
  refers to the Currency String as configured in Win.Ini or
  the Windows 95 System Registry.

  '@('    - Converts <dVal> according to the remainder of the picture
  clause.  If <dVal> is a negative number, opening and
  closing parentheses are added to the string before it is
  returned.

  '9'     - Returns a string in one of two formats:  If there is a
  decimal point in the picture clause, the return value is
  formatted as '999.00'; otherwise, the return value is
  '999'.

  '$'     - Same as '@$', but leading blanks are NOT filled with the
  dollar sign.

  '*'     - Asterisk - same as '@$', but leading blanks are replaced
  with asterisk characters (useful for printing checks, etc.)
  ********************************************************************* *)
function NTransForm(Value: double; Picture: String): String;
var
  s: String;
begin
  s      := Alltrim(Picture);
  result := NULL_STRING;
  if length(s) < 1 then
    exit;
  case s[1] of
    '$':
      begin
        result := FormatFloat('#,###.00', Value);
        if length(result) < (length(Picture) - 1) then
        begin
          if (length(s) > 1) and (s[2] <> '$') then
            result := FormatSettings.currencyString + PadL(result, #0, length(Picture) - 1)
          else
            result := FormatSettings.currencyString + PadL(result, '$', length(Picture) - 2);
        end
        else
          result := FormatSettings.currencyString + result;
      end;
    '*':
      begin
        result := NTransForm(Value, '9' + substr(Picture, 2, 0));
        if length(result) < (length(Picture) - 1) then
          result := FormatSettings.currencyString + PadL(result, '*', length(Picture) - 1)
        else
          result := FormatSettings.currencyString + result;
      end;
    '9':
      begin
        s := StrTran(s, '9', '#', 1, 0);
        if At('.', s) > 0 then
          s    := substr(s, 1, At('.', s)) + replicate('0', length(substr(s, At('.', s) + 1, 0)));
        result := FormatFloat(s, Value);
      end;
    '@':
      begin
        if length(s) < 2 then
          raise Exception.Create('Cannot use single @ as picture clause');
        case s[2] of
          'D', 'd':
            result := DToC(Value);
          { Little recursion trickery here! }
          '$':
            result := NTransForm(Value, '$' + substr(Picture, 2, 0));
          '(':
            begin
              result := NTransForm(Value, IIFS(length(Picture) > 2, substr(Picture, 3, 0), '9'));
              if Value < 0 then
                // Strip negative sign (-) and add parens
                result := '(' + substr(result, 2, 0) + ')';
            end;
        end;
      end;
  end;
end;

(* *********************************************************************
  STransForm( Value, Picture ) - Formats <Value> according to <Picture>
  and returns the result.

  Valid Picture clauses are:

  '@!'  - converts <Value> to upper case
  '@P'  - converts <Value> to proper case
  '!xxx' - converts '!' to uppercase, 'x' remains the same

  Example:

  S := STransform('system', '@!');          { returns 'SYSTEM' }
  S := STransform('borland delphi', '@P');  { returns 'Borland Delphi' }
  S := STransform('ken', '!xx');            { returns 'Ken'    }
  S := STransform('ken', '!x!');            { returns 'KeN'    }
  ********************************************************************** *)
function STransForm(Value: String; Picture: String): String;
var
  s: String;
  i: LongInt;
begin
  s      := Alltrim(Picture);
  result := NULL_STRING;

  if length(s) < 1 then
    exit;

  case s[1] of
    '@':
      begin
        if length(s) < 2 then
          exit;
        case s[2] of
          '!':
            result := upper(Value);
          'P', 'p':
            result := Proper(Value);
        else
          raise Exception.Create(Picture + ' is not a valid PICTURE!');
        end;
      end;
  else
    for i := 1 to length(s) do
      case s[i] of
        '!':
          result := result + upper(Value[i]);
        'x', 'X':
          result := result + Value[i];
      end;
  end;
end;

(* *********************************************************************
  Transform( [<arg>], <Pict> ) - This is a wrapper function for all
  of the other xTransForm() functions.  This is used just like
  the Clipper equivalent, with the exception that the first
  argument passed, <arg>, _MUST_BE_ enclosed in square brackets;
  this converts the parameter to an 'array of const' (even though
  it's only one argument), which allows a value of any type to be passed.

  Picture clauses allowed in <Pict> are any of the valid types
  for the <arg> parameter.  For example, if <arg> is a Boolean,
  <Pict> can be any of 'Y', 'L', or 'W'; if <arg> is a String,
  then <Pict> can be any of '@!', '@P', or a string of '!xxxxx'.

  Examples:
  S := TransForm([GradePassed], 'W'); { S is 'True' or 'False' }
  S := TransForm(['borland delphi'], '@P'); { S is 'Borland Delphi' }
  S := TransForm([Date], '@D');   { S is todays date as '01/01/96'
  ********************************************************************* *)
function Transform(const Args: Array of Const; Picture: String): String;
var
  s: String[1];
  sErr: String[10];
begin
  sErr := NULL_STRING;

  // Make sure that a picture clause was specified
  if length(Picture) < 1 then
    raise Exception.Create('Missing PICTURE clause in Transform()');

  { None of the Transform() functions accept more than one arg in
    'Args', so we can safely ignore all but the first one }

  with Args[0] do
    case VType of
      VTInteger:
        result := NTransForm(VInteger, Picture);
      VTExtended:
        result := NTransForm(VExtended^, Picture);
      VTBoolean:
        result := BTransForm(VBoolean, Picture);
      VTString:
        result := STransForm(VString^, Picture);
      VTChar:
        begin
          s[0]   := Char(1);
          s[1]   := VChar;
          result := STransForm(s, Picture);
        end;
      vtPointer:
        sErr := 'Pointer';
      VTPChar:
        result := STransForm(String(VPChar), Picture);
      vtObject:
        sErr := 'Object';
      vtClass:
        sErr := 'Class';
      vtWideChar:
        sErr := 'WideChar';
      vtPWideChar:
        result := 'PWideChar';
      vtAnsiString:
        result := STransForm(String(VAnsiString), Picture);
      vtCurrency:
        result := NTransForm(VCurrency^, Picture);
      vtVariant:
        sErr := 'Variant';
    else
      raise Exception.Create('Unknown data type as arg to Transform');
    end;
  if sErr <> NULL_STRING then { Unsupported type passed }
    raise Exception.CreateFmt('%s datatype not supported in' + ' Transform()', [sErr]);
end;

function zero(valore: double): string;
begin
  result := IfThen(valore >= 1, '', '0') + Transform([valore], '9,999.99');
end;


// function dai_interessi(fraz: string): single;
// begin
// if pos(fraz,'ASTQBX')>0 then begin
// if fraz='A' then result:=0;
// if fraz='S' then result:=3;
// if fraz='T' then result:=5;
// if fraz='Q' then result:=4;
// if fraz='B' then result:=0;
// if fraz='X' then result:=0;
// end else if pos(fraz,'123460')>0 then begin
// if fraz='1' then result:=0;
// if fraz='2' then result:=3;
// if fraz='4' then result:=5;
// if fraz='3' then result:=4;
// if fraz='6' then result:=0;
// if fraz='0' then result:=0;
// end else result:=0;
// end;
//
//
// function dai_interessi(fraz: smallint): single;
// begin
// if fraz=1 then result:=0
// else if fraz=2 then result:=3
// else if fraz=4 then result:=5
// else if fraz=3 then result:=4
// else if fraz=6 then result:=0
// else if fraz=0 then result:=0
// else result:=0;
// end;
//
//
// function crea_data(gg,mm,aa: integer): TdateTime;
// var t: integer;
// dx: TdateTime;
// begin
// // restituisci una data valida in base ai dati passati ...
// t:=ultimogiornodelmese(mm,aa);
// if not TryEncodeDate(aa, mm, gg, dx) then
// begin
// // problema sicuramente legato all'ultimo giorno del mese
// gg:=t;
// end;
// dx:=encodeDate(aa,mm,gg);
// result:=dx;
// end;
//
//

//
//
// function diffMesi30(d1,d2: TdateTime): integer;
// begin
// // fornisce il numeri di mesi (di 30 giorni) che intercorrono tra le date passate
// result := dist360(d1, d2) div 30;
// end;
end.
