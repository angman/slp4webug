unit date360;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask; // , clipfn32;

procedure dai_scadenze(decorrenza, scadenza: TdateTime; fraz: word; lista: TstringList);
function dist360(d1, d2: TdateTime): integer;
function diffMesi30(d1, d2: TdateTime): integer;
function dai_interessi(fraz: string): single; overload;
function dai_interessi(fraz: smallint): single; overload;
function quietanzio(mese, anno: smallint; decorrenza, scadenza: TdateTime; fraz: word): boolean;
function crea_data(gg, mm, aa: integer): TdateTime;
function LastOfMonth(gg, mm, aa: smallint): boolean;
function UltimoGiornoDelMese(mm, aa: smallint): integer;
function normalizza_fraz_char(fraz: string): string;
function normalizza_fraz_int(fraz: string): integer;
function dai_scadenza_rata(effetto: TdateTime; fraz: string): TdateTime;

implementation

uses
  System.DateUtils;

function LastOfMonth(gg, mm, aa: smallint): boolean;
begin
  // true se la data passata � l'ultimo giorno del mese
  result := false;
  if gg = 31 then
    result := true
  else
  begin
    if (gg = 30) and (mm in [4, 6, 9, 11]) then
      result := true
    else
      if mm = 2 then
      begin
        if IsLeapYear(aa) and (gg = 29) then
          result := true;
        if not IsLeapYear(aa) and (gg = 28) then
          result := true;
      end;
  end;
end;

function UltimoGiornoDelMese(mm, aa: smallint): integer;
begin
  // mi restituisce l'ultimo giorno del mese della mese/anno passato
  result := 31;
  if (mm in [4, 6, 9, 11]) then
    result := 30
  else
  begin
    if mm = 2 then
    begin
      if IsLeapYear(aa) then
        result := 29
      else
        result := 28;
    end;
  end;
end;

function dai_scadenza_rata(effetto: TdateTime; fraz: string): TdateTime;
begin
  // data una decorrenza ed un frazionamento mi restituisce il periodo di fine copertura
  result := IncMonth(effetto, 12 div normalizza_fraz_int(fraz));
end;

function dist360(d1, d2: TdateTime): integer;
var
  dt: TdateTime;
  a1, a2, m1, m2, g1, g2: word;
  a0, m0, g0: smallint;
begin
  // d1 deve sempre essere la data minore
  if d1 > d2 then
  begin
    dt := d1;
    d1 := d2;
    d2 := dt;
  end;
  // decodifica i dati relativi alle date fornite
  decodedate(d1, a1, m1, g1);
  decodedate(d2, a2, m2, g2);

  a0 := a2 - a1;
  m0 := m2 - m1;
  // normalizza l'utimo giorno del mese a 30 gg
  if LastOfMonth(g1, m1, a1) then
    g1 := 30;
  if LastOfMonth(g2, m2, a2) then
    g2 := 30
  else
  begin
    // occhi se parliamo del mese di febbraio !!!!!
    if (m2 = 2) and (g2 >= 28) then
    begin
      // se � febbraio una che se indico come giorno di scadenza 29 e come giorno
      // di decorrenza 28 deve essere tutto ok
      g2 := 30;
    end;
  end;
  g0     := g2 - g1;
  result := a0 * 360 + m0 * 30 + g0;
  // function IncMonth(const Date: TDateTime; NumberOfMonths: Integer): TDateTime;
end;

procedure dai_scadenze(decorrenza, scadenza: TdateTime; fraz: word; lista: TstringList);
var
  finito: boolean;
  sca: TdateTime;
  durata, passo, passo1, durata_mesi: integer;
begin
  // partendo dalla scadenza fai retro marcia fino ad arrivare alla decorrenza
  // o a un punto precedente (ratino)
  // le date sono caricate in senso inverso: durata  n= giorni di rateo; 1=ultima rata
  finito := false;
  sca    := scadenza;
  passo  := 12 div fraz;
  passo1 := passo;

  durata := dist360(decorrenza, scadenza);
  if (durata < 720) and (MonthOf(scadenza) <> MonthOf(decorrenza)) and (fraz = 1) then
  begin
    lista.Add(dateTostr(scadenza));
  end
  else
  begin
    // altri casi di ratino !!!!
    if (MonthOf(scadenza) <> MonthOf(decorrenza)) or
      ((MonthOf(scadenza) = MonthOf(decorrenza)) and (DayOf(scadenza) <> DayOf(decorrenza))) then
    begin
      lista.Add(dateTostr(scadenza));
    end;
  end;

  while not finito do
  begin
    sca := IncMonth(scadenza, -passo1);
    lista.Add(dateTostr(sca));
    finito := sca <= decorrenza;
    passo1 := passo1 + passo;
  end;
  // durata:=dist360(decorrenza,scadenza) div 360;
  durata      := dist360(decorrenza, scadenza);
  durata_mesi := (durata mod 360) div 30;
  durata      := durata div 360;
  lista.Add(intTostr(durata) + '+' + intTostr(durata_mesi)); // durata anni

  if lista.strings[lista.count - 2] <> dateTostr(decorrenza) then
  begin
    // caso con ratino iniziale ...
    lista.strings[lista.count - 2] := dateTostr(decorrenza);
    if lista.count >= 3 then
      passo := dist360(decorrenza, strTodate(lista.strings[lista.count - 3]));
    lista.Add(intTostr(passo));
  end
  else
    lista.Add(intTostr(0));

  // correttivo per caso di polizza durata anni uno fraz ann
  if (lista.count = 3) and (fraz = 1) then
  begin
    lista.Insert(0, dateTostr(scadenza));
    lista.strings[1] := dateTostr(scadenza);
  end;
end;

function quietanzio(mese, anno: smallint; decorrenza, scadenza: TdateTime; fraz: word): boolean;
var
  passo, i: integer;
  meseS, mese1: integer;
begin
  // restituisce true se nel mese,anno indicato la polizza con decorrenza, scadenza
  // e frazionamento dato anno quietanza ...
  passo  := 12 div fraz;
  meseS  := MonthOf(scadenza);
  result := false;
  for i  := 1 to fraz do
  begin
    if mese = meseS then
    begin
      result := true;
      break;
    end;
    mese1 := (mese + passo) mod 12;
    if mese1 = 0 then
      mese := mese + passo
    else
      mese := mese1;
  end;
end;

function normalizza_fraz_char(fraz: string): string;
begin
  if pos(fraz, 'ASTQBX') > 0 then
  begin
    result := fraz;
  end
  else
    if pos(fraz, '123469') > 0 then
    begin
      if fraz = '1' then
        result := 'A'
      else
        if fraz = '2' then
          result := 'S'
        else
          if fraz = '3' then
            result := 'Q'
          else
            if fraz = '4' then
              result := 'T'
            else
              if fraz = '6' then
                result := 'B'
              else
                if fraz = '9' then
                  result := 'X'
    end
    else
      result := 'R'; // errore !!!!!
end;

function normalizza_fraz_int(fraz: string): integer;
begin
  if pos(fraz, 'ASTQBX') > 0 then
  begin
    if fraz = 'A' then
      result := 1
    else
      if fraz = 'S' then
        result := 2
      else
        if fraz = 'Q' then
          result := 3
        else
          if fraz = 'T' then
            result := 4
          else
            if fraz = 'B' then
              result := 6
            else
              if fraz = 'X' then
                result := 9

  end
  else
    if pos(fraz, '123469') > 0 then
    begin
      result := StrToInt(fraz);
    end
    else
      result := 0; // errore !!!!!
end;

function dai_interessi(fraz: string): single;
begin
  if pos(fraz, 'ASTQBX') > 0 then
  begin
    if fraz = 'A' then
      result := 0;
    if fraz = 'S' then
      result := 3;
    if fraz = 'T' then
      result := 5;
    if fraz = 'Q' then
      result := 4;
    if fraz = 'B' then
      result := 0;
    if fraz = 'X' then
      result := 0;
  end
  else
    if pos(fraz, '123460') > 0 then
    begin
      if fraz = '1' then
        result := 0;
      if fraz = '2' then
        result := 3;
      if fraz = '4' then
        result := 5;
      if fraz = '3' then
        result := 4;
      if fraz = '6' then
        result := 0;
      if fraz = '0' then
        result := 0;
    end
    else
      result := 0;
end;

function dai_interessi(fraz: smallint): single;
begin
  if fraz = 1 then
    result := 0
  else
    if fraz = 2 then
      result := 3
    else
      if fraz = 4 then
        result := 5
      else
        if fraz = 3 then
          result := 4
        else
          if fraz = 6 then
            result := 0
          else
            if fraz = 0 then
              result := 0
            else
              result := 0;
end;

function crea_data(gg, mm, aa: integer): TdateTime;
var
  t: integer;
  dx: TdateTime;
begin
  // restituisci una data valida in base ai dati passati ...
  t := UltimoGiornoDelMese(mm, aa);
  if not TryEncodeDate(aa, mm, gg, dx) then
  begin
    // problema sicuramente legato all'ultimo giorno del mese
    gg := t;
  end;
  dx     := encodeDate(aa, mm, gg);
  result := dx;
end;

function diffMesi30(d1, d2: TdateTime): integer;
begin
  // fornisce il numeri di mesi (di 30 giorni) che intercorrono tra le date passate
  result := dist360(d1, d2) div 30;
end;

end.
